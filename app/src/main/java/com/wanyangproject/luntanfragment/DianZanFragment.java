package com.wanyangproject.luntanfragment;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;

import com.wanyangproject.R;
import com.wanyangproject.entity.TieZiXiangQingEntity;
import com.wanyangproject.luntanAdapter.DianZanAdapter;
import com.wanyangproject.utils.JianTingZhuangTai;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by 甜甜 on 2018/7/19.
 */

public class DianZanFragment extends Fragment {


    @BindView(R.id.recyclerView_dianzan)
    RecyclerView recyclerViewDianzan;

    private DianZanAdapter dianZanAdapter;
    private TieZiXiangQingEntity tieZiXiangQingEntity;

    private JianTingZhuangTai jianTingZhuangTai;

    public void setJianTingZhuangTai(JianTingZhuangTai jianTingZhuangTai) {
        this.jianTingZhuangTai = jianTingZhuangTai;
    }

    public TieZiXiangQingEntity getTieZiXiangQingEntity() {
        return tieZiXiangQingEntity;
    }

    public void setTieZiXiangQingEntity(TieZiXiangQingEntity tieZiXiangQingEntity) {
        this.tieZiXiangQingEntity = tieZiXiangQingEntity;
        initDianZan();
        System.out.println(tieZiXiangQingEntity.getResponse().getLikeArr().size() + "     点赞");
    }






    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.dianzan_fragment, container, false);
        ButterKnife.bind(this, view);//        点赞
        if (jianTingZhuangTai!=null){
            jianTingZhuangTai.zhuangTai();
        }


        return view;
    }












    //    点赞
    private void initDianZan() {
        dianZanAdapter = new DianZanAdapter(getContext(), tieZiXiangQingEntity.getResponse().getLikeArr());
        GridLayoutManager manager = new GridLayoutManager(getContext(),5);
        //设置为横向滑动
        System.out.println(manager + "                00000000000000");
        recyclerViewDianzan.setLayoutManager(manager);
        recyclerViewDianzan.setAdapter(dianZanAdapter);

    }
}
