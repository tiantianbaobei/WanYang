package com.wanyangproject.luntanfragment;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.google.gson.Gson;
import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.scwang.smartrefresh.layout.listener.OnLoadmoreListener;
import com.scwang.smartrefresh.layout.listener.OnRefreshListener;
import com.wanyangproject.R;
import com.wanyangproject.activity.FaTieActivity;
import com.wanyangproject.activity.TieZiXiangQingActivity;
import com.wanyangproject.adapter.LunTanLieBiaoAdapter;
import com.wanyangproject.entity.LunTanDianZanEntity;
import com.wanyangproject.entity.LunTanLieBiaoEntity;
import com.wanyangproject.utils.ContractUtils;
import com.wanyangproject.widget.recyclerview.refresh.OnLoadMoreListener;
import com.zhy.http.okhttp.OkHttpUtils;
import com.zhy.http.okhttp.callback.StringCallback;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import okhttp3.Call;

import static com.wanyangproject.R.id.image_zan;
import static com.wanyangproject.R.id.image_zan_true;
import static com.wanyangproject.R.id.re;

/**
 * Created by 甜甜 on 2018/7/19.
 */

public class QuanBuFragment extends Fragment implements OnRefreshListener, OnLoadmoreListener {

    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    //    @BindView(R.id.image_zan)
//    ImageView imageZan;
//    @BindView(R.id.image_pinglun)
//    ImageView imagePinglun;
//    @BindView(R.id.image_zan1)
//    ImageView imageZan1;
//    @BindView(R.id.image_pinglun1)
//    ImageView imagePinglun1;
//    @BindView(R.id.relative_one)
//    RelativeLayout relativeOne;
//    @BindView(R.id.relative_teo)
//    RelativeLayout relativeTeo;
//    @BindView(R.id.tv_zan_number)
//    TextView tvZanNumber;
//    @BindView(R.id.tv_zan_number1)
//    TextView tvZanNumber1;
//    private NetWork netWork;
    private String yuanquparkId;
    private String fenleiid;
    private String token;
    private LunTanLieBiaoAdapter lunTanLieBiaoAdapter;
    private LunTanLieBiaoEntity lunTanLieBiaoEntity;
    private int page = 0;




    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.quanbu_fragment, container, false);
        ButterKnife.bind(this, view);

        SharedPreferences sharedPreferences = getContext().getSharedPreferences("token", Context.MODE_PRIVATE);
        token = sharedPreferences.getString("token", "token");
        System.out.println(token + "     token");


//        获取园区id接收广播
//        IntentFilter intentFilter = new IntentFilter();
//        intentFilter.addAction("luntanyuanquid");
//        netWork = new NetWork();
//        getContext().registerReceiver(netWork, intentFilter);
        return view;
    }

//
//    class NetWork extends BroadcastReceiver {
//        @Override
//        public void onReceive(Context context, Intent intent) {
//            setLuntanparkId(intent.getStringExtra("luntanparkId"));
//            System.out.println(luntanparkId + "    论坛接收的园区id");
//        }
//    }


//    @Override
//    public void onResume() {
//        super.onResume();
//        //        论坛列表的网络请求
//        initLunTanLieBiaoHttp(yuanquparkId,fenleiid);
//
//    }

    //    论坛列表的网络请求
    public void initLunTanLieBiaoHttp(String luntanparkId,String titleid) {
        yuanquparkId = luntanparkId;
        fenleiid = titleid;
        if (titleid == null || luntanparkId == null) {
            return;
        }
        OkHttpUtils.post()
                .url(ContractUtils.LOGIN_URL + "forum/index")
                .addHeader("token", ContractUtils.getTOKEN(getContext()))
                .addParams("parkId", luntanparkId)
                .addParams("typeId", titleid)
                .build()
                .execute(new StringCallback() {
                    @Override
                    public void onError(Call call, Exception e, int id) {
                        System.out.println(e+"         论坛列表信息eeeeeeee");
                    }

                    @Override
                    public void onResponse(String response, int id) {
                        System.out.println(ContractUtils.getTOKEN(getContext()) + "     论坛列表的token");
                        System.out.println(response + "       论坛列表信息的网络请求");
                        ContractUtils.Code500(getContext(),response);
                        if(response.indexOf("200") != -1){
                            Gson gson = new Gson();
                            lunTanLieBiaoEntity = gson.fromJson(response, LunTanLieBiaoEntity.class);

                            List<LunTanLieBiaoEntity.ResponseBean.ListBean> list = lunTanLieBiaoEntity.getResponse().getList();

                            for (int i = 0; i < list.size(); i++) {
                                LunTanLieBiaoEntity.ResponseBean.ListBean listbean =   list.get(i);
                                for (int j = 0; j < listbean.getImgArr().size(); j++) {
                                    String s = listbean.getImgArr().get(j);
                                    if(s.equals("")){
                                        listbean.getImgArr().remove(s);
                                    }
                                }
                            }

                            for(LunTanLieBiaoEntity.ResponseBean.ListBean bean : list){
                                if(bean.getImgArr().size() == 0){
                                    bean.setItemType(0);
                                }else if(bean.getImgArr().size() == 1){
                                    bean.setItemType(1);
                                }else if(bean.getImgArr().size() == 2){
                                    bean.setItemType(2);
                                }else if(bean.getImgArr().size() >= 3){
                                    bean.setItemType(3);
                                }
                            }


                            lunTanLieBiaoAdapter = new LunTanLieBiaoAdapter(lunTanLieBiaoEntity.getResponse().getList());
                            LinearLayoutManager manager = new LinearLayoutManager(getContext());
                            recyclerView.setLayoutManager(manager);
                            recyclerView.setAdapter(lunTanLieBiaoAdapter);

                            lunTanLieBiaoAdapter.setOnItemClickListener(new BaseQuickAdapter.OnItemClickListener() {
                                @Override
                                public void onItemClick(BaseQuickAdapter adapter, View view, int position) {
//                                点击进入帖子详情
                                    Intent intent = new Intent(getContext(), TieZiXiangQingActivity.class);
                                    intent.putExtra("tieziid",lunTanLieBiaoEntity.getResponse().getList().get(position).getId());
                                    System.out.println(lunTanLieBiaoEntity.getResponse().getList().get(position).getId()+"     发送帖子的id");
                                    intent.putExtra("ispc",lunTanLieBiaoEntity.getResponse().getList().get(position).getIsPc());
                                    startActivity(intent);
                                }
                            });



                            lunTanLieBiaoAdapter.setOnItemChildClickListener(new BaseQuickAdapter.OnItemChildClickListener() {
                                @Override
                                public void onItemChildClick(BaseQuickAdapter adapter, View view, int position) {
//                                点赞的网络请求
                                    initDianZanHttp(position);
                                    ImageView viewByPosition = (ImageView) adapter.getViewByPosition(recyclerView, position, R.id.image_zan);
                                    TextView viewByPosition1 = (TextView) adapter.getViewByPosition(recyclerView, position, R.id.tv_zan_number);
                                    if (lunTanLieBiaoEntity.getResponse().getList().get(position).getIsLike().equals("1")){
                                        if (viewByPosition != null) {
                                            viewByPosition.setImageResource(R.mipmap.dianzan_false);
                                        }
                                        if (viewByPosition1 != null) {
                                            viewByPosition1.setText(Integer.parseInt(lunTanLieBiaoEntity.getResponse().getList().get(position).getLike())-1+"");
                                        }
                                    }else {
                                        if (viewByPosition != null) {
                                            viewByPosition.setImageResource(R.mipmap.dianzan_true);
                                        }
                                        if (viewByPosition1 != null) {
                                            viewByPosition1.setText(Integer.parseInt(lunTanLieBiaoEntity.getResponse().getList().get(position).getLike())+1+"");
                                        }
                                    }
                                }
                            });
                        }else {
                            if (lunTanLieBiaoEntity != null) {

                                lunTanLieBiaoEntity.getResponse().getList().clear();

                                List<LunTanLieBiaoEntity.ResponseBean.ListBean> list = lunTanLieBiaoEntity.getResponse().getList();

                                for (int i = 0; i < list.size(); i++) {
                                    LunTanLieBiaoEntity.ResponseBean.ListBean listbean = list.get(i);
                                    for (int j = 0; j < listbean.getImgArr().size(); j++) {
                                        String s = listbean.getImgArr().get(j);
                                        if (s.equals("")) {
                                            listbean.getImgArr().remove(s);
                                        }
                                    }
                                }

                                for (LunTanLieBiaoEntity.ResponseBean.ListBean bean : list) {
                                    if (bean.getImgArr().size() == 0) {
                                        bean.setItemType(0);
                                    } else if (bean.getImgArr().size() == 1) {
                                        bean.setItemType(1);
                                    } else if (bean.getImgArr().size() == 2) {
                                        bean.setItemType(2);
                                    } else if (bean.getImgArr().size() >= 3) {
                                        bean.setItemType(3);
                                    }
                                }


                                lunTanLieBiaoAdapter = new LunTanLieBiaoAdapter(lunTanLieBiaoEntity.getResponse().getList());
                                LinearLayoutManager manager = new LinearLayoutManager(getContext());
                                recyclerView.setLayoutManager(manager);
                                recyclerView.setAdapter(lunTanLieBiaoAdapter);

                                lunTanLieBiaoAdapter.setOnItemClickListener(new BaseQuickAdapter.OnItemClickListener() {
                                    @Override
                                    public void onItemClick(BaseQuickAdapter adapter, View view, int position) {
//                                点击进入帖子详情
                                        Intent intent = new Intent(getContext(), TieZiXiangQingActivity.class);
                                        intent.putExtra("tieziid", lunTanLieBiaoEntity.getResponse().getList().get(position).getId());
                                        System.out.println(lunTanLieBiaoEntity.getResponse().getList().get(position).getId() + "     发送帖子的id");
                                        startActivity(intent);
                                    }
                                });


                                lunTanLieBiaoAdapter.setOnItemChildClickListener(new BaseQuickAdapter.OnItemChildClickListener() {
                                    @Override
                                    public void onItemChildClick(BaseQuickAdapter adapter, View view, int position) {
//                                点赞的网络请求
                                        initDianZanHttp(position);
                                        ImageView viewByPosition = (ImageView) adapter.getViewByPosition(recyclerView, position, R.id.image_zan);
                                        TextView viewByPosition1 = (TextView) adapter.getViewByPosition(recyclerView, position, R.id.tv_zan_number);
                                        if (lunTanLieBiaoEntity.getResponse().getList().get(position).getIsLike().equals("1")) {
                                            if (viewByPosition != null) {
                                                viewByPosition.setImageResource(R.mipmap.dianzan_false);
                                            }
                                            if (viewByPosition1 != null) {
                                                viewByPosition1.setText(Integer.parseInt(lunTanLieBiaoEntity.getResponse().getList().get(position).getLike()) - 1 + "");
                                            }
                                        } else {
                                            if (viewByPosition != null) {
                                                viewByPosition.setImageResource(R.mipmap.dianzan_true);
                                            }
                                            if (viewByPosition1 != null) {
                                                viewByPosition1.setText(Integer.parseInt(lunTanLieBiaoEntity.getResponse().getList().get(position).getLike()) + 1 + "");
                                            }
                                        }
                                    }
                                });
                            }
                        }
                    }
                });
    }






    //    论坛列表的网络请求
    public void initLunTanLieBiaoHttp1(String luntanparkId, String titleid) {
        yuanquparkId = luntanparkId;
        fenleiid = titleid;
        if (titleid == null || luntanparkId == null) {
            return;
        }
        OkHttpUtils.post()
                .url(ContractUtils.LOGIN_URL + "forum/index")
                .addHeader("token", ContractUtils.getTOKEN(getContext()))
                .addParams("parkId", luntanparkId)
                .addParams("typeId", titleid)
                .build()
                .execute(new StringCallback() {
                    @Override
                    public void onError(Call call, Exception e, int id) {

                    }

                    @Override
                    public void onResponse(String response, int id) {
                        System.out.println(ContractUtils.getTOKEN(getContext()) + "     论坛列表的token");
                        System.out.println(response + "       论坛列表信息的网络请求");
                        ContractUtils.Code500(getContext(),response);
                        if(response.indexOf("200") != -1){
                            Gson gson = new Gson();
                            lunTanLieBiaoEntity = gson.fromJson(response, LunTanLieBiaoEntity.class);
                        }


//                        Toast.makeText(getContext(), "11", Toast.LENGTH_SHORT).show();


                    }
                });
    }








//    点赞的网络请求
//    tiantian
    private void initDianZanHttp(final int position) {
        System.out.println(lunTanLieBiaoEntity.getResponse().getList().get(position).getId()+"   点赞的id");
        OkHttpUtils.post()
                .url(ContractUtils.LOGIN_URL+"forum/toLike")
                .addHeader("token",ContractUtils.getTOKEN(getContext()))
                .addParams("forumId",lunTanLieBiaoEntity.getResponse().getList().get(position).getId())
                .build()
                .execute(new StringCallback() {
                    @Override
                    public void onError(Call call, Exception e, int id) {

                    }

                    @Override
                    public void onResponse(String response, int id) {
                        System.out.println(response+"  点赞的网络请求");
                        ContractUtils.Code500(getContext(),response);
                        Gson gson = new Gson();
                        LunTanDianZanEntity lunTanDianZanEntity = gson.fromJson(response, LunTanDianZanEntity.class);
                        if(lunTanDianZanEntity.getCode() == 200){
//                            lunTanLieBiaoAdapter.setDianZanClick(new LunTanLieBiaoAdapter.DianZanClick() {
//                                @Override
//                                public void dianzanClick(int position) {
//
//                                }
//                            });


                            //        论坛列表的网络请求
                        initLunTanLieBiaoHttp1(yuanquparkId,fenleiid);

                        }
                    }
                });
    }

    @Override
    public void onRefresh(RefreshLayout refreshlayout) {
//        page = 0;
        //        论坛列表的网络请求
        initLunTanLieBiaoHttp1(yuanquparkId,fenleiid);
        if (refreshlayout.isRefreshing()) {
            refreshlayout.finishRefresh();
        }
    }


    @Override
    public void onLoadmore(RefreshLayout refreshlayout) {
//        page++;
        //        论坛列表的网络请求
        initLunTanLieBiaoHttp1(yuanquparkId,fenleiid);
        refreshlayout.finishLoadmore();

    }


//    @OnClick({R.id.tv_zan_number, R.id.tv_zan_number1,R.id.image_zan, R.id.image_pinglun, R.id.image_zan1, R.id.image_pinglun1, R.id.relative_one, R.id.relative_teo})
//    public void onViewClicked(View view) {
//        switch (view.getId()) {
//            case R.id.tv_zan_number:
//                break;
//            case R.id.tv_zan_number1:
//                break;
//            case R.id.image_zan:
//                imageZan.setImageResource(R.mipmap.dianzan_true);
//                int i = Integer.parseInt(tvZanNumber.getText().toString());
//                i++;
//                tvZanNumber.setText(i+"");
//                break;
//            case R.id.image_pinglun:
//                Intent intent1 = new Intent(getContext(), TieZiXiangQingActivity.class);
//                startActivity(intent1);
//                break;
//            case R.id.image_zan1:
//                imageZan1.setImageResource(R.mipmap.dianzan_true);
//                int iii = Integer.parseInt(tvZanNumber1.getText().toString());
//                iii++;
//                tvZanNumber1.setText(iii+"");
//                break;
//            case R.id.image_pinglun1:
//                Intent intent3 = new Intent(getContext(), TieZiXiangQingActivity.class);
//                startActivity(intent3);
//                break;
//            case R.id.relative_one:
//                Intent intent4 = new Intent(getContext(), TieZiXiangQingActivity.class);
//                startActivity(intent4);
//                break;
//            case R.id.relative_teo:
//                Intent intent5 = new Intent(getContext(), TieZiXiangQingActivity.class);
//                startActivity(intent5);
//                break;
//        }
//    }
}

