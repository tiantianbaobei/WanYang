package com.wanyangproject.luntanfragment;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.wanyangproject.R;
import com.wanyangproject.entity.TieZiXiangQingEntity;
import com.wanyangproject.luntanAdapter.PingLunAdapter;
import com.wanyangproject.utils.ContractUtils;
import com.zhy.http.okhttp.OkHttpUtils;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by 甜甜 on 2018/7/19.
 */

public class LunTanPingLunFragment extends Fragment {

    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    private PingLunAdapter pingLunAdapter;
    private TieZiXiangQingEntity tieZiXiangQingEntity;

    public TieZiXiangQingEntity getTieZiXiangQingEntity() {
        return tieZiXiangQingEntity;

    }

    public void setTieZiXiangQingEntity(TieZiXiangQingEntity tieZiXiangQingEntity) {
        this.tieZiXiangQingEntity = tieZiXiangQingEntity;
        System.out.println(tieZiXiangQingEntity.getResponse().getReplyArr().size()+"      pingun");
//        评论
        initPingLun();
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.pinglun_diannao, container, false);
        ButterKnife.bind(this, view);

        return view;
    }

//    评论
    private void initPingLun() {
        pingLunAdapter = new PingLunAdapter(getContext(),tieZiXiangQingEntity.getResponse().getReplyArr());
        LinearLayoutManager manager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(manager);
        recyclerView.setAdapter(pingLunAdapter);


        pingLunAdapter.setHuifuidClick(new PingLunAdapter.HuiFuidClick() {
            @Override
            public void huifuidClick(int position, String id, String name) {
                Intent intent=new Intent();
                intent.putExtra("id",id);
                intent.putExtra("name",name);
                intent.setAction("huifu");
                getContext().sendBroadcast(intent);
            }
        });

    }


}
