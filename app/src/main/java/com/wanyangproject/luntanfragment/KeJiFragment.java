package com.wanyangproject.luntanfragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.wanyangproject.R;
import com.wanyangproject.activity.TieZiXiangQingActivity;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by 甜甜 on 2018/7/19.
 */

public class KeJiFragment extends Fragment {

    @BindView(R.id.image_zan)
    ImageView imageZan;
    @BindView(R.id.image_pinglun)
    ImageView imagePinglun;
    @BindView(R.id.relative)
    RelativeLayout relative;
    @BindView(R.id.tv_zan_number)
    TextView tvZanNumber;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.keji_fragment, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @OnClick({R.id.image_zan, R.id.image_pinglun, R.id.relative,R.id.tv_zan_number})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.image_zan:
                imageZan.setImageResource(R.mipmap.dianzan_true);
                int i = Integer.parseInt(tvZanNumber.getText().toString());
                i++;
                tvZanNumber.setText(i+"");
                break;
            case R.id.image_pinglun:
                Intent intent1 = new Intent(getContext(), TieZiXiangQingActivity.class);
                startActivity(intent1);
                break;
            case R.id.relative:
                Intent intent2 = new Intent(getContext(), TieZiXiangQingActivity.class);
                startActivity(intent2);
                break;
            case R.id.tv_zan_number:

                break;
        }
    }
}
