package com.wanyangproject.entity;

import java.util.List;

/**
 * Created by 甜甜 on 2018/8/13.
 */

public class PingLunEntity {

    /**
     * code : 200
     * msg : 发表成功
     * response : [{"id":"14","content":"哈哈","add_time":"0秒前","upInfo":[],"uinfo":{"userid":"2","avatar":"http://park.hostop.net/static/image/avatar.jpg","nickname":"未填写"},"canReply":"1"}]
     * request : {"content":"哈哈","forumId":"13","upId":"0"}
     * other : []
     */

    private int code;
    private String msg;
    private RequestBean request;
    private List<ResponseBean> response;
    private List<?> other;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public RequestBean getRequest() {
        return request;
    }

    public void setRequest(RequestBean request) {
        this.request = request;
    }

    public List<ResponseBean> getResponse() {
        return response;
    }

    public void setResponse(List<ResponseBean> response) {
        this.response = response;
    }

    public List<?> getOther() {
        return other;
    }

    public void setOther(List<?> other) {
        this.other = other;
    }

    public static class RequestBean {
        /**
         * content : 哈哈
         * forumId : 13
         * upId : 0
         */

        private String content;
        private String forumId;
        private String upId;

        public String getContent() {
            return content;
        }

        public void setContent(String content) {
            this.content = content;
        }

        public String getForumId() {
            return forumId;
        }

        public void setForumId(String forumId) {
            this.forumId = forumId;
        }

        public String getUpId() {
            return upId;
        }

        public void setUpId(String upId) {
            this.upId = upId;
        }
    }

    public static class ResponseBean {
        /**
         * id : 14
         * content : 哈哈
         * add_time : 0秒前
         * upInfo : []
         * uinfo : {"userid":"2","avatar":"http://park.hostop.net/static/image/avatar.jpg","nickname":"未填写"}
         * canReply : 1
         */

        private String id;
        private String content;
        private String add_time;
        private UinfoBean uinfo;
        private String canReply;
        private List<?> upInfo;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getContent() {
            return content;
        }

        public void setContent(String content) {
            this.content = content;
        }

        public String getAdd_time() {
            return add_time;
        }

        public void setAdd_time(String add_time) {
            this.add_time = add_time;
        }

        public UinfoBean getUinfo() {
            return uinfo;
        }

        public void setUinfo(UinfoBean uinfo) {
            this.uinfo = uinfo;
        }

        public String getCanReply() {
            return canReply;
        }

        public void setCanReply(String canReply) {
            this.canReply = canReply;
        }

        public List<?> getUpInfo() {
            return upInfo;
        }

        public void setUpInfo(List<?> upInfo) {
            this.upInfo = upInfo;
        }

        public static class UinfoBean {
            /**
             * userid : 2
             * avatar : http://park.hostop.net/static/image/avatar.jpg
             * nickname : 未填写
             */

            private String userid;
            private String avatar;
            private String nickname;

            public String getUserid() {
                return userid;
            }

            public void setUserid(String userid) {
                this.userid = userid;
            }

            public String getAvatar() {
                return avatar;
            }

            public void setAvatar(String avatar) {
                this.avatar = avatar;
            }

            public String getNickname() {
                return nickname;
            }

            public void setNickname(String nickname) {
                this.nickname = nickname;
            }
        }
    }
}
