package com.wanyangproject.entity;

import java.util.List;

/**
 * Created by 甜甜 on 2018/8/21.
 */

public class QiuZhiZhaoPinLieBiaoEntity {
    /**
     * code : 200
     * msg :
     * response : [{"id":"52","title":"","money":"薪资面议","address":"","entername":"","enterId":"51","parkId":"19"},{"id":"46","title":"测试921","money":"薪资面议","address":"测试地址","entername":"测试企业名称","enterId":"632","parkId":"14"},{"id":"37","title":"客户代表","money":"薪资面议","address":"山东省济南市历下区创星汇科技园","entername":"测试企业名称","enterId":"632","parkId":"19"}]
     * request : {"parkId":"1","typeId":"12"}
     * other : []
     */

    private int code;
    private String msg;
    private RequestBean request;
    private List<ResponseBean> response;
    private List<?> other;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public RequestBean getRequest() {
        return request;
    }

    public void setRequest(RequestBean request) {
        this.request = request;
    }

    public List<ResponseBean> getResponse() {
        return response;
    }

    public void setResponse(List<ResponseBean> response) {
        this.response = response;
    }

    public List<?> getOther() {
        return other;
    }

    public void setOther(List<?> other) {
        this.other = other;
    }

    public static class RequestBean {
        /**
         * parkId : 1
         * typeId : 12
         */

        private String parkId;
        private String typeId;

        public String getParkId() {
            return parkId;
        }

        public void setParkId(String parkId) {
            this.parkId = parkId;
        }

        public String getTypeId() {
            return typeId;
        }

        public void setTypeId(String typeId) {
            this.typeId = typeId;
        }
    }

    public static class ResponseBean {
        /**
         * id : 52
         * title :
         * money : 薪资面议
         * address :
         * entername :
         * enterId : 51
         * parkId : 19
         */

        private String id;
        private String title;
        private String money;
        private String address;
        private String entername;
        private String enterId;
        private String parkId;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getMoney() {
            return money;
        }

        public void setMoney(String money) {
            this.money = money;
        }

        public String getAddress() {
            return address;
        }

        public void setAddress(String address) {
            this.address = address;
        }

        public String getEntername() {
            return entername;
        }

        public void setEntername(String entername) {
            this.entername = entername;
        }

        public String getEnterId() {
            return enterId;
        }

        public void setEnterId(String enterId) {
            this.enterId = enterId;
        }

        public String getParkId() {
            return parkId;
        }

        public void setParkId(String parkId) {
            this.parkId = parkId;
        }
    }


//
//    /**
//     * code : 200
//     * msg :
//     * response : [{"id":"23","title":"服务员","money":"薪资面议","address":"济南市槐荫区匡山汽车大世界","enterName":"温州嘉创印务有限公司"},{"id":"1","title":"营业员","money":"薪资面议","address":"济南市槐荫区匡山汽车大世界","enterName":"温州嘉创印务有限公司"}]
//     * request : {"parkId":"1","typeID":"4"}
//     * other : []
//     */
//
//    private int code;
//    private String msg;
//    private RequestBean request;
//    private List<ResponseBean> response;
//    private List<?> other;
//
//    public int getCode() {
//        return code;
//    }
//
//    public void setCode(int code) {
//        this.code = code;
//    }
//
//    public String getMsg() {
//        return msg;
//    }
//
//    public void setMsg(String msg) {
//        this.msg = msg;
//    }
//
//    public RequestBean getRequest() {
//        return request;
//    }
//
//    public void setRequest(RequestBean request) {
//        this.request = request;
//    }
//
//    public List<ResponseBean> getResponse() {
//        return response;
//    }
//
//    public void setResponse(List<ResponseBean> response) {
//        this.response = response;
//    }
//
//    public List<?> getOther() {
//        return other;
//    }
//
//    public void setOther(List<?> other) {
//        this.other = other;
//    }
//
//    public static class RequestBean {
//        /**
//         * parkId : 1
//         * typeID : 4
//         */
//
//        private String parkId;
//        private String typeID;
//
//        public String getParkId() {
//            return parkId;
//        }
//
//        public void setParkId(String parkId) {
//            this.parkId = parkId;
//        }
//
//        public String getTypeID() {
//            return typeID;
//        }
//
//        public void setTypeID(String typeID) {
//            this.typeID = typeID;
//        }
//    }
//
//    public static class ResponseBean {
//        /**
//         * id : 23
//         * title : 服务员
//         * money : 薪资面议
//         * address : 济南市槐荫区匡山汽车大世界
//         * enterName : 温州嘉创印务有限公司
//         */
//
//        private String id;
//        private String title;
//        private String money;
//        private String address;
//        private String enterName;
//
//        public String getId() {
//            return id;
//        }
//
//        public void setId(String id) {
//            this.id = id;
//        }
//
//        public String getTitle() {
//            return title;
//        }
//
//        public void setTitle(String title) {
//            this.title = title;
//        }
//
//        public String getMoney() {
//            return money;
//        }
//
//        public void setMoney(String money) {
//            this.money = money;
//        }
//
//        public String getAddress() {
//            return address;
//        }
//
//        public void setAddress(String address) {
//            this.address = address;
//        }
//
//        public String getEnterName() {
//            return enterName;
//        }
//
//        public void setEnterName(String enterName) {
//            this.enterName = enterName;
//        }
//    }
}
