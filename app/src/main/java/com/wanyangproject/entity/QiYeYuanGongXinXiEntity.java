package com.wanyangproject.entity;

import java.util.List;

/**
 * Created by 甜甜 on 2018/9/5.
 */

public class QiYeYuanGongXinXiEntity {

    /**
     * code : 200
     * msg :
     * response : [{"parkPersonId":"296","personName":"陈正群","phone":"13588904546","sex":"男","avatar":"http://park.hostop.netstatic/image/logo.jpg","entryTime":"2018-04-03 14:12:01","dormitoryName":"暂无","typeId":"1"},{"parkPersonId":"304","personName":"吕荣彬","phone":"13606778817","sex":"男","avatar":"http://park.hostop.netstatic/image/logo.jpg","entryTime":"2018-04-03 14:12:01","dormitoryName":"暂无","typeId":"1"},{"parkPersonId":"332","personName":"吕蒙正","phone":"13868552666","sex":"男","avatar":"http://park.hostop.netstatic/image/logo.jpg","entryTime":"2018-04-03 14:12:01","dormitoryName":"暂无","typeId":"1"},{"parkPersonId":"363","personName":"林海","phone":"13732019232","sex":"男","avatar":"http://park.hostop.netstatic/image/logo.jpg","entryTime":"2018-04-03 14:12:01","dormitoryName":"暂无","typeId":"1"},{"parkPersonId":"412","personName":"温从克","phone":"158884755371","sex":"男","avatar":"http://park.hostop.netstatic/image/logo.jpg","entryTime":"2018-04-03 14:12:01","dormitoryName":"暂无","typeId":"1"}]
     * request : {"personName":""}
     * other : []
     */

    private int code;
    private String msg;
    private RequestBean request;
    private List<ResponseBean> response;
    private List<?> other;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public RequestBean getRequest() {
        return request;
    }

    public void setRequest(RequestBean request) {
        this.request = request;
    }

    public List<ResponseBean> getResponse() {
        return response;
    }

    public void setResponse(List<ResponseBean> response) {
        this.response = response;
    }

    public List<?> getOther() {
        return other;
    }

    public void setOther(List<?> other) {
        this.other = other;
    }

    public static class RequestBean {
        /**
         * personName :
         */

        private String personName;

        public String getPersonName() {
            return personName;
        }

        public void setPersonName(String personName) {
            this.personName = personName;
        }
    }

    public static class ResponseBean {
        /**
         * parkPersonId : 296
         * personName : 陈正群
         * phone : 13588904546
         * sex : 男
         * avatar : http://park.hostop.netstatic/image/logo.jpg
         * entryTime : 2018-04-03 14:12:01
         * dormitoryName : 暂无
         * typeId : 1
         */

        private String parkPersonId;
        private String personName;
        private String phone;
        private String sex;
        private String avatar;
        private String entryTime;
        private String dormitoryName;
        private String typeId;
        private boolean zhuangtai=false;
        private boolean zhuangtai1=false;


        public boolean isZhuangtai1() {
            return zhuangtai1;
        }

        public void setZhuangtai1(boolean zhuangtai1) {
            this.zhuangtai1 = zhuangtai1;
        }

        public boolean isZhuangtai() {
            return zhuangtai;
        }

        public void setZhuangtai(boolean zhuangtai) {
            this.zhuangtai = zhuangtai;
        }

        public String getParkPersonId() {
            return parkPersonId;
        }

        public void setParkPersonId(String parkPersonId) {
            this.parkPersonId = parkPersonId;
        }

        public String getPersonName() {
            return personName;
        }

        public void setPersonName(String personName) {
            this.personName = personName;
        }

        public String getPhone() {
            return phone;
        }

        public void setPhone(String phone) {
            this.phone = phone;
        }

        public String getSex() {
            return sex;
        }

        public void setSex(String sex) {
            this.sex = sex;
        }

        public String getAvatar() {
            return avatar;
        }

        public void setAvatar(String avatar) {
            this.avatar = avatar;
        }

        public String getEntryTime() {
            return entryTime;
        }

        public void setEntryTime(String entryTime) {
            this.entryTime = entryTime;
        }

        public String getDormitoryName() {
            return dormitoryName;
        }

        public void setDormitoryName(String dormitoryName) {
            this.dormitoryName = dormitoryName;
        }

        public String getTypeId() {
            return typeId;
        }

        public void setTypeId(String typeId) {
            this.typeId = typeId;
        }
    }
}
