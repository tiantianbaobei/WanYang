package com.wanyangproject.entity;

import java.util.List;

/**
 * Created by 甜甜 on 2018/9/11.
 */

public class FangKeGuanLiEntity {

    /**
     * code : 200
     * msg :
     * response : [{"id":"208","parkId":"1","phone":"15875585555","nums":"3","name":"哈哈","daodaTime":"2018","chepai":"","add_time":"1536634526","add_ip":"124.133.211.40","special":"27db68d0696a386185fb043def14da62d","photo":"upload/qrcode/2018-09-11/1907f44dba7bff7e3e3695d71dbfaf9c31.png","visit_time":"","visit_nums":"","visit_userid":"","type":"1","userid":"2"}]
     * request : {"parkId":"1","page":"1"}
     * other : []
     */

    private int code;
    private String msg;
    private RequestBean request;
    private List<ResponseBean> response;
    private List<?> other;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public RequestBean getRequest() {
        return request;
    }

    public void setRequest(RequestBean request) {
        this.request = request;
    }

    public List<ResponseBean> getResponse() {
        return response;
    }

    public void setResponse(List<ResponseBean> response) {
        this.response = response;
    }

    public List<?> getOther() {
        return other;
    }

    public void setOther(List<?> other) {
        this.other = other;
    }

    public static class RequestBean {
        /**
         * parkId : 1
         * page : 1
         */

        private String parkId;
        private String page;

        public String getParkId() {
            return parkId;
        }

        public void setParkId(String parkId) {
            this.parkId = parkId;
        }

        public String getPage() {
            return page;
        }

        public void setPage(String page) {
            this.page = page;
        }
    }

    public static class ResponseBean {
        /**
         * id : 208
         * parkId : 1
         * phone : 15875585555
         * nums : 3
         * name : 哈哈
         * daodaTime : 2018
         * chepai :
         * add_time : 1536634526
         * add_ip : 124.133.211.40
         * special : 27db68d0696a386185fb043def14da62d
         * photo : upload/qrcode/2018-09-11/1907f44dba7bff7e3e3695d71dbfaf9c31.png
         * visit_time :
         * visit_nums :
         * visit_userid :
         * type : 1
         * userid : 2
         */

        private String id;
        private String parkId;
        private String phone;
        private String nums;
        private String name;
        private String daodaTime;
        private String chepai;
        private String add_time;
        private String add_ip;
        private String special;
        private String photo;
        private String visit_time;
        private String visit_nums;
        private String visit_userid;
        private String type;
        private String userid;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getParkId() {
            return parkId;
        }

        public void setParkId(String parkId) {
            this.parkId = parkId;
        }

        public String getPhone() {
            return phone;
        }

        public void setPhone(String phone) {
            this.phone = phone;
        }

        public String getNums() {
            return nums;
        }

        public void setNums(String nums) {
            this.nums = nums;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getDaodaTime() {
            return daodaTime;
        }

        public void setDaodaTime(String daodaTime) {
            this.daodaTime = daodaTime;
        }

        public String getChepai() {
            return chepai;
        }

        public void setChepai(String chepai) {
            this.chepai = chepai;
        }

        public String getAdd_time() {
            return add_time;
        }

        public void setAdd_time(String add_time) {
            this.add_time = add_time;
        }

        public String getAdd_ip() {
            return add_ip;
        }

        public void setAdd_ip(String add_ip) {
            this.add_ip = add_ip;
        }

        public String getSpecial() {
            return special;
        }

        public void setSpecial(String special) {
            this.special = special;
        }

        public String getPhoto() {
            return photo;
        }

        public void setPhoto(String photo) {
            this.photo = photo;
        }

        public String getVisit_time() {
            return visit_time;
        }

        public void setVisit_time(String visit_time) {
            this.visit_time = visit_time;
        }

        public String getVisit_nums() {
            return visit_nums;
        }

        public void setVisit_nums(String visit_nums) {
            this.visit_nums = visit_nums;
        }

        public String getVisit_userid() {
            return visit_userid;
        }

        public void setVisit_userid(String visit_userid) {
            this.visit_userid = visit_userid;
        }

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public String getUserid() {
            return userid;
        }

        public void setUserid(String userid) {
            this.userid = userid;
        }
    }
}
