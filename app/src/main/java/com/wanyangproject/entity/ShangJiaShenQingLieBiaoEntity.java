package com.wanyangproject.entity;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by 甜甜 on 2018/9/1.
 */

public class ShangJiaShenQingLieBiaoEntity {

    /**
     * code : 200
     * msg :
     * response : [{"id":"10","user_id":"8","pk_id":"1","name":"测试返回值","addres":"北京","Applicant_name":"哈哈","Applicant_phone":"13176688711","class":"2","introduce":"测试一下","legal_name":"石福森","number_Positive":"http://park.hostop.net/upload/20180901/f8ace828840bf47fcf7585ac149a67ec.png","number_side":"http://park.hostop.net/upload/20180901/7061cd64c050717de032f736968e18fb.jpg","license":"http://park.hostop.net/upload/20180901/d18c87a5816f5334fcf169aacd121478.jpg","add_time":"1535811920","type":"2","operation_time":"","Refusal":""}]
     * request : {"parkId":"1"}
     * other : []
     */

    private int code;
    private String msg;
    private RequestBean request;
    private List<ResponseBean> response;
    private List<?> other;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public RequestBean getRequest() {
        return request;
    }

    public void setRequest(RequestBean request) {
        this.request = request;
    }

    public List<ResponseBean> getResponse() {
        return response;
    }

    public void setResponse(List<ResponseBean> response) {
        this.response = response;
    }

    public List<?> getOther() {
        return other;
    }

    public void setOther(List<?> other) {
        this.other = other;
    }

    public static class RequestBean {
        /**
         * parkId : 1
         */

        private String parkId;

        public String getParkId() {
            return parkId;
        }

        public void setParkId(String parkId) {
            this.parkId = parkId;
        }
    }

    public static class ResponseBean {
        /**
         * id : 10
         * user_id : 8
         * pk_id : 1
         * name : 测试返回值
         * addres : 北京
         * Applicant_name : 哈哈
         * Applicant_phone : 13176688711
         * class : 2
         * introduce : 测试一下
         * legal_name : 石福森
         * number_Positive : http://park.hostop.net/upload/20180901/f8ace828840bf47fcf7585ac149a67ec.png
         * number_side : http://park.hostop.net/upload/20180901/7061cd64c050717de032f736968e18fb.jpg
         * license : http://park.hostop.net/upload/20180901/d18c87a5816f5334fcf169aacd121478.jpg
         * add_time : 1535811920
         * type : 2
         * operation_time :
         * Refusal :
         */

        private String id;
        private String user_id;
        private String pk_id;
        private String name;
        private String addres;
        private String Applicant_name;
        private String Applicant_phone;
        @SerializedName("class")
        private String classX;
        private String introduce;
        private String legal_name;
        private String number_Positive;
        private String number_side;
        private String license;
        private String add_time;
        private String type;
        private String operation_time;
        private String Refusal;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getUser_id() {
            return user_id;
        }

        public void setUser_id(String user_id) {
            this.user_id = user_id;
        }

        public String getPk_id() {
            return pk_id;
        }

        public void setPk_id(String pk_id) {
            this.pk_id = pk_id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getAddres() {
            return addres;
        }

        public void setAddres(String addres) {
            this.addres = addres;
        }

        public String getApplicant_name() {
            return Applicant_name;
        }

        public void setApplicant_name(String Applicant_name) {
            this.Applicant_name = Applicant_name;
        }

        public String getApplicant_phone() {
            return Applicant_phone;
        }

        public void setApplicant_phone(String Applicant_phone) {
            this.Applicant_phone = Applicant_phone;
        }

        public String getClassX() {
            return classX;
        }

        public void setClassX(String classX) {
            this.classX = classX;
        }

        public String getIntroduce() {
            return introduce;
        }

        public void setIntroduce(String introduce) {
            this.introduce = introduce;
        }

        public String getLegal_name() {
            return legal_name;
        }

        public void setLegal_name(String legal_name) {
            this.legal_name = legal_name;
        }

        public String getNumber_Positive() {
            return number_Positive;
        }

        public void setNumber_Positive(String number_Positive) {
            this.number_Positive = number_Positive;
        }

        public String getNumber_side() {
            return number_side;
        }

        public void setNumber_side(String number_side) {
            this.number_side = number_side;
        }

        public String getLicense() {
            return license;
        }

        public void setLicense(String license) {
            this.license = license;
        }

        public String getAdd_time() {
            return add_time;
        }

        public void setAdd_time(String add_time) {
            this.add_time = add_time;
        }

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public String getOperation_time() {
            return operation_time;
        }

        public void setOperation_time(String operation_time) {
            this.operation_time = operation_time;
        }

        public String getRefusal() {
            return Refusal;
        }

        public void setRefusal(String Refusal) {
            this.Refusal = Refusal;
        }
    }
}
