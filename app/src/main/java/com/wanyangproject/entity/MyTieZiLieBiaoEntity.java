package com.wanyangproject.entity;

import java.util.List;

/**
 * Created by 甜甜 on 2018/9/12.
 */

public class MyTieZiLieBiaoEntity {
    /**
     * code : 200
     * msg :
     * response : {"list":[{"id":"157","title":"","add_time":"2018-09-25 11:32","uinfo":{"userid":"14","avatar":"http://park.hostop.net/upload/20180926/5431e80a62c256e334d0130994957e0b.jpg","nickname":"2221","username":"13522212291"},"imgArr":["http://park.hostop.net/upload/20180925/2e9dfe56c163f39c6faef3cfb028097a.jpg"],"reply":"1","like":"1","isLike":"0","status":"1","setTop":"0"},{"id":"25","title":"","add_time":"2018-08-28 10:45","uinfo":{"userid":"2","avatar":"http://park.hostop.net/upload/20180927/05515109a51791e52393f6e4daa4e5ee.jpg","nickname":"呵呵呵","username":"17600904682"},"imgArr":["http://park.hostop.net/upload/20180828/5d2f58bcea15b58052b20c7b1ffb34dc.jpg","http://park.hostop.net/upload/20180828/bb91f70215d68008327d41f98635dcde.jpg","http://park.hostop.net/upload/20180828/258924a445e8088b9b26fe7cf05f4bfe.jpg"],"reply":"3","like":"6","isLike":"1","status":"1","setTop":"0"}],"nextPage":"1"}
     * request : []
     * other : []
     */

    private int code;
    private String msg;
    private ResponseBean response;
    private List<?> request;
    private List<?> other;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public ResponseBean getResponse() {
        return response;
    }

    public void setResponse(ResponseBean response) {
        this.response = response;
    }

    public List<?> getRequest() {
        return request;
    }

    public void setRequest(List<?> request) {
        this.request = request;
    }

    public List<?> getOther() {
        return other;
    }

    public void setOther(List<?> other) {
        this.other = other;
    }

    public static class ResponseBean {
        /**
         * list : [{"id":"157","title":"","add_time":"2018-09-25 11:32","uinfo":{"userid":"14","avatar":"http://park.hostop.net/upload/20180926/5431e80a62c256e334d0130994957e0b.jpg","nickname":"2221","username":"13522212291"},"imgArr":["http://park.hostop.net/upload/20180925/2e9dfe56c163f39c6faef3cfb028097a.jpg"],"reply":"1","like":"1","isLike":"0","status":"1","setTop":"0"},{"id":"25","title":"","add_time":"2018-08-28 10:45","uinfo":{"userid":"2","avatar":"http://park.hostop.net/upload/20180927/05515109a51791e52393f6e4daa4e5ee.jpg","nickname":"呵呵呵","username":"17600904682"},"imgArr":["http://park.hostop.net/upload/20180828/5d2f58bcea15b58052b20c7b1ffb34dc.jpg","http://park.hostop.net/upload/20180828/bb91f70215d68008327d41f98635dcde.jpg","http://park.hostop.net/upload/20180828/258924a445e8088b9b26fe7cf05f4bfe.jpg"],"reply":"3","like":"6","isLike":"1","status":"1","setTop":"0"}]
         * nextPage : 1
         */

        private String nextPage;
        private List<ListBean> list;

        public String getNextPage() {
            return nextPage;
        }

        public void setNextPage(String nextPage) {
            this.nextPage = nextPage;
        }

        public List<ListBean> getList() {
            return list;
        }

        public void setList(List<ListBean> list) {
            this.list = list;
        }

        public static class ListBean {
            /**
             * id : 157
             * title :
             * add_time : 2018-09-25 11:32
             * uinfo : {"userid":"14","avatar":"http://park.hostop.net/upload/20180926/5431e80a62c256e334d0130994957e0b.jpg","nickname":"2221","username":"13522212291"}
             * imgArr : ["http://park.hostop.net/upload/20180925/2e9dfe56c163f39c6faef3cfb028097a.jpg"]
             * reply : 1
             * like : 1
             * isLike : 0
             * status : 1
             * setTop : 0
             */

            private String id;
            private String title;
            private String add_time;
            private UinfoBean uinfo;
            private String reply;
            private String like;
            private String isLike;
            private String status;
            private String setTop;
            private List<String> imgArr;
            private String isPc;

            public String getIsPc() {
                return isPc;
            }

            public void setIsPc(String isPc) {
                this.isPc = isPc;
            }

            public String getId() {
                return id;
            }

            public void setId(String id) {
                this.id = id;
            }

            public String getTitle() {
                return title;
            }

            public void setTitle(String title) {
                this.title = title;
            }

            public String getAdd_time() {
                return add_time;
            }

            public void setAdd_time(String add_time) {
                this.add_time = add_time;
            }

            public UinfoBean getUinfo() {
                return uinfo;
            }

            public void setUinfo(UinfoBean uinfo) {
                this.uinfo = uinfo;
            }

            public String getReply() {
                return reply;
            }

            public void setReply(String reply) {
                this.reply = reply;
            }

            public String getLike() {
                return like;
            }

            public void setLike(String like) {
                this.like = like;
            }

            public String getIsLike() {
                return isLike;
            }

            public void setIsLike(String isLike) {
                this.isLike = isLike;
            }

            public String getStatus() {
                return status;
            }

            public void setStatus(String status) {
                this.status = status;
            }

            public String getSetTop() {
                return setTop;
            }

            public void setSetTop(String setTop) {
                this.setTop = setTop;
            }

            public List<String> getImgArr() {
                return imgArr;
            }

            public void setImgArr(List<String> imgArr) {
                this.imgArr = imgArr;
            }

            public static class UinfoBean {
                /**
                 * userid : 14
                 * avatar : http://park.hostop.net/upload/20180926/5431e80a62c256e334d0130994957e0b.jpg
                 * nickname : 2221
                 * username : 13522212291
                 */

                private String userid;
                private String avatar;
                private String nickname;
                private String username;

                public String getUserid() {
                    return userid;
                }

                public void setUserid(String userid) {
                    this.userid = userid;
                }

                public String getAvatar() {
                    return avatar;
                }

                public void setAvatar(String avatar) {
                    this.avatar = avatar;
                }

                public String getNickname() {
                    return nickname;
                }

                public void setNickname(String nickname) {
                    this.nickname = nickname;
                }

                public String getUsername() {
                    return username;
                }

                public void setUsername(String username) {
                    this.username = username;
                }
            }
        }
    }


//    /**
//     * code : 200
//     * msg :
//     * response : {"list":[{"id":"71","title":"发布1338","add_time":"10天前","uinfo":{"userid":"10","avatar":"http://park.hostop.net/upload/20180906/00001e33c45c72fa1a7d3088e3034c28.png","nickname":"后来","username":"18410710712"},"imgArr":[""],"reply":"2","like":"1","isLike":"0","status":"1","setTop":"1"},{"id":"77","title":"星期五下午","add_time":"7天前","uinfo":{"userid":"14","avatar":"http://park.hostop.net/upload/20180906/2a64a6307e1d0ce8781a7a3445890f51.jpg","nickname":"2221","username":"13522212291"},"imgArr":[""],"reply":"10","like":"3","isLike":"0","status":"1","setTop":"0"}],"nextPage":"1"}
//     * request : []
//     * other : []
//     */
//
//    private int code;
//    private String msg;
//    private ResponseBean response;
//    private List<?> request;
//    private List<?> other;
//
//    public int getCode() {
//        return code;
//    }
//
//    public void setCode(int code) {
//        this.code = code;
//    }
//
//    public String getMsg() {
//        return msg;
//    }
//
//    public void setMsg(String msg) {
//        this.msg = msg;
//    }
//
//    public ResponseBean getResponse() {
//        return response;
//    }
//
//    public void setResponse(ResponseBean response) {
//        this.response = response;
//    }
//
//    public List<?> getRequest() {
//        return request;
//    }
//
//    public void setRequest(List<?> request) {
//        this.request = request;
//    }
//
//    public List<?> getOther() {
//        return other;
//    }
//
//    public void setOther(List<?> other) {
//        this.other = other;
//    }
//
//    public static class ResponseBean {
//        /**
//         * list : [{"id":"71","title":"发布1338","add_time":"10天前","uinfo":{"userid":"10","avatar":"http://park.hostop.net/upload/20180906/00001e33c45c72fa1a7d3088e3034c28.png","nickname":"后来","username":"18410710712"},"imgArr":[""],"reply":"2","like":"1","isLike":"0","status":"1","setTop":"1"},{"id":"77","title":"星期五下午","add_time":"7天前","uinfo":{"userid":"14","avatar":"http://park.hostop.net/upload/20180906/2a64a6307e1d0ce8781a7a3445890f51.jpg","nickname":"2221","username":"13522212291"},"imgArr":[""],"reply":"10","like":"3","isLike":"0","status":"1","setTop":"0"}]
//         * nextPage : 1
//         */
//
//        private String nextPage;
//        private List<ListBean> list;
//
//        public String getNextPage() {
//            return nextPage;
//        }
//
//        public void setNextPage(String nextPage) {
//            this.nextPage = nextPage;
//        }
//
//        public List<ListBean> getList() {
//            return list;
//        }
//
//        public void setList(List<ListBean> list) {
//            this.list = list;
//        }
//
//        public static class ListBean {
//            /**
//             * id : 71
//             * title : 发布1338
//             * add_time : 10天前
//             * uinfo : {"userid":"10","avatar":"http://park.hostop.net/upload/20180906/00001e33c45c72fa1a7d3088e3034c28.png","nickname":"后来","username":"18410710712"}
//             * imgArr : [""]
//             * reply : 2
//             * like : 1
//             * isLike : 0
//             * status : 1
//             * setTop : 1
//             */
//
//            private String id;
//            private String title;
//            private String add_time;
//            private UinfoBean uinfo;
//            private String reply;
//            private String like;
//            private String isLike;
//            private String status;
//            private String setTop;
//            private List<String> imgArr;
//
//            public String getId() {
//                return id;
//            }
//
//            public void setId(String id) {
//                this.id = id;
//            }
//
//            public String getTitle() {
//                return title;
//            }
//
//            public void setTitle(String title) {
//                this.title = title;
//            }
//
//            public String getAdd_time() {
//                return add_time;
//            }
//
//            public void setAdd_time(String add_time) {
//                this.add_time = add_time;
//            }
//
//            public UinfoBean getUinfo() {
//                return uinfo;
//            }
//
//            public void setUinfo(UinfoBean uinfo) {
//                this.uinfo = uinfo;
//            }
//
//            public String getReply() {
//                return reply;
//            }
//
//            public void setReply(String reply) {
//                this.reply = reply;
//            }
//
//            public String getLike() {
//                return like;
//            }
//
//            public void setLike(String like) {
//                this.like = like;
//            }
//
//            public String getIsLike() {
//                return isLike;
//            }
//
//            public void setIsLike(String isLike) {
//                this.isLike = isLike;
//            }
//
//            public String getStatus() {
//                return status;
//            }
//
//            public void setStatus(String status) {
//                this.status = status;
//            }
//
//            public String getSetTop() {
//                return setTop;
//            }
//
//            public void setSetTop(String setTop) {
//                this.setTop = setTop;
//            }
//
//            public List<String> getImgArr() {
//                return imgArr;
//            }
//
//            public void setImgArr(List<String> imgArr) {
//                this.imgArr = imgArr;
//            }
//
//            public static class UinfoBean {
//                /**
//                 * userid : 10
//                 * avatar : http://park.hostop.net/upload/20180906/00001e33c45c72fa1a7d3088e3034c28.png
//                 * nickname : 后来
//                 * username : 18410710712
//                 */
//
//                private String userid;
//                private String avatar;
//                private String nickname;
//                private String username;
//
//                public String getUserid() {
//                    return userid;
//                }
//
//                public void setUserid(String userid) {
//                    this.userid = userid;
//                }
//
//                public String getAvatar() {
//                    return avatar;
//                }
//
//                public void setAvatar(String avatar) {
//                    this.avatar = avatar;
//                }
//
//                public String getNickname() {
//                    return nickname;
//                }
//
//                public void setNickname(String nickname) {
//                    this.nickname = nickname;
//                }
//
//                public String getUsername() {
//                    return username;
//                }
//
//                public void setUsername(String username) {
//                    this.username = username;
//                }
//            }
//        }
//    }
}
