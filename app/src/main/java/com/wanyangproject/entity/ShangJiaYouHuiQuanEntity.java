package com.wanyangproject.entity;

import java.util.List;

/**
 * Created by 甜甜 on 2018/8/22.
 */

public class ShangJiaYouHuiQuanEntity {
    /**
     * code : 200
     * msg :
     * response : [{"id":"203","service":"测试1011","name":"呵呵呵","money":"1","permanent":"2","use_time":"","nember":"1","yi":"0","status":"3"}]
     * request : {"phone":"17600904682","parkId":"1"}
     * other : []
     */

    private int code;
    private String msg;
    private RequestBean request;
    private List<ResponseBean> response;
    private List<?> other;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public RequestBean getRequest() {
        return request;
    }

    public void setRequest(RequestBean request) {
        this.request = request;
    }

    public List<ResponseBean> getResponse() {
        return response;
    }

    public void setResponse(List<ResponseBean> response) {
        this.response = response;
    }

    public List<?> getOther() {
        return other;
    }

    public void setOther(List<?> other) {
        this.other = other;
    }

    public static class RequestBean {
        /**
         * phone : 17600904682
         * parkId : 1
         */

        private String phone;
        private String parkId;

        public String getPhone() {
            return phone;
        }

        public void setPhone(String phone) {
            this.phone = phone;
        }

        public String getParkId() {
            return parkId;
        }

        public void setParkId(String parkId) {
            this.parkId = parkId;
        }
    }

    public static class ResponseBean {
        /**
         * id : 203
         * service : 测试1011
         * name : 呵呵呵
         * money : 1
         * permanent : 2
         * use_time :
         * nember : 1
         * yi : 0
         * status : 3
         */

        private String id;
        private String service;
        private String name;
        private String money;
        private String permanent;
        private String use_time;
        private String nember;
        private String yi;
        private String status;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getService() {
            return service;
        }

        public void setService(String service) {
            this.service = service;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getMoney() {
            return money;
        }

        public void setMoney(String money) {
            this.money = money;
        }

        public String getPermanent() {
            return permanent;
        }

        public void setPermanent(String permanent) {
            this.permanent = permanent;
        }

        public String getUse_time() {
            return use_time;
        }

        public void setUse_time(String use_time) {
            this.use_time = use_time;
        }

        public String getNember() {
            return nember;
        }

        public void setNember(String nember) {
            this.nember = nember;
        }

        public String getYi() {
            return yi;
        }

        public void setYi(String yi) {
            this.yi = yi;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }
    }


//
//    /**
//     * code : 200
//     * msg :
//     * response : [{"id":"4","name":"11","money":"2","permanent":"1","use_time":"2018-08-01 - 2018-08-20","nember":"4","yi":"1","status":"2"}]
//     * request : {"phone":"15822075533","parkId":"1"}
//     * other : []
//     */
//
//    private int code;
//    private String msg;
//    private RequestBean request;
//    private List<ResponseBean> response;
//    private List<?> other;
//
//    public int getCode() {
//        return code;
//    }
//
//    public void setCode(int code) {
//        this.code = code;
//    }
//
//    public String getMsg() {
//        return msg;
//    }
//
//    public void setMsg(String msg) {
//        this.msg = msg;
//    }
//
//    public RequestBean getRequest() {
//        return request;
//    }
//
//    public void setRequest(RequestBean request) {
//        this.request = request;
//    }
//
//    public List<ResponseBean> getResponse() {
//        return response;
//    }
//
//    public void setResponse(List<ResponseBean> response) {
//        this.response = response;
//    }
//
//    public List<?> getOther() {
//        return other;
//    }
//
//    public void setOther(List<?> other) {
//        this.other = other;
//    }
//
//    public static class RequestBean {
//        /**
//         * phone : 15822075533
//         * parkId : 1
//         */
//
//        private String phone;
//        private String parkId;
//
//        public String getPhone() {
//            return phone;
//        }
//
//        public void setPhone(String phone) {
//            this.phone = phone;
//        }
//
//        public String getParkId() {
//            return parkId;
//        }
//
//        public void setParkId(String parkId) {
//            this.parkId = parkId;
//        }
//    }
//
//    public static class ResponseBean {
//        /**
//         * id : 4
//         * name : 11
//         * money : 2
//         * permanent : 1
//         * use_time : 2018-08-01 - 2018-08-20
//         * nember : 4
//         * yi : 1
//         * status : 2
//         */
//
//        private String id;
//        private String name;
//        private String money;
//        private String permanent;
//        private String use_time;
//        private String nember;
//        private String yi;
//        private String status;
//
//        public String getId() {
//            return id;
//        }
//
//        public void setId(String id) {
//            this.id = id;
//        }
//
//        public String getName() {
//            return name;
//        }
//
//        public void setName(String name) {
//            this.name = name;
//        }
//
//        public String getMoney() {
//            return money;
//        }
//
//        public void setMoney(String money) {
//            this.money = money;
//        }
//
//        public String getPermanent() {
//            return permanent;
//        }
//
//        public void setPermanent(String permanent) {
//            this.permanent = permanent;
//        }
//
//        public String getUse_time() {
//            return use_time;
//        }
//
//        public void setUse_time(String use_time) {
//            this.use_time = use_time;
//        }
//
//        public String getNember() {
//            return nember;
//        }
//
//        public void setNember(String nember) {
//            this.nember = nember;
//        }
//
//        public String getYi() {
//            return yi;
//        }
//
//        public void setYi(String yi) {
//            this.yi = yi;
//        }
//
//        public String getStatus() {
//            return status;
//        }
//
//        public void setStatus(String status) {
//            this.status = status;
//        }
//    }
}
