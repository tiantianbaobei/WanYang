package com.wanyangproject.entity;

import java.util.List;

/**
 * Created by 甜甜 on 2018/8/24.
 */

public class QiYeFuWuEntity {
    /**
     * code : 200
     * msg :
     * response : {"content":"企业服务--   面向园区企业提供优质的企业服务，切实有效的解决园区企业各类需求，为园区企业健康发展保驾护航。--   全面落实\u201c产业集聚、产城融合、资源共享、产融互动\u201d运营模式，引导中小微制造业企业入园集聚，推动区域块状特色产业向现代产业集群发展。--   秉持\u201c让企业有个家，我才是企业家\u201d的理念，为园区企业提供家一般的温暖。--    为中小微制造业企业嵌入全球供应链，走向\u201c一带一路\u201d，参与全球竞争提供平台支撑。----","data":[{"id":"166","title":"营业执照变更","desc":"","type":"1"},{"id":"167","title":"环评立项报告申请","desc":"","type":"1"},{"id":"168","title":"企业工商年报","desc":"","type":"1"},{"id":"169","title":"住所（营业场所）产权证明","desc":"","type":"1"},{"id":"170","title":"test","desc":"1","type":"1"},{"id":"182","title":"无偿使用证明","desc":"D-12营业执照地址变更无偿使用证明","type":"1"},{"id":"183","title":"员工子女上学","desc":"","type":"1"},{"id":"184","title":"变压器申报报装","desc":"","type":"1"},{"id":"186","title":"优惠券申请（测试）","desc":"","type":"2"},{"id":"196","title":"测试企服服务","desc":"","type":"2"}]}
     * request : {"parkId":"1","typeId":"2"}
     * other : []
     */

    private int code;
    private String msg;
    private ResponseBean response;
    private RequestBean request;
    private List<?> other;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public ResponseBean getResponse() {
        return response;
    }

    public void setResponse(ResponseBean response) {
        this.response = response;
    }

    public RequestBean getRequest() {
        return request;
    }

    public void setRequest(RequestBean request) {
        this.request = request;
    }

    public List<?> getOther() {
        return other;
    }

    public void setOther(List<?> other) {
        this.other = other;
    }

    public static class ResponseBean {
        /**
         * content : 企业服务--   面向园区企业提供优质的企业服务，切实有效的解决园区企业各类需求，为园区企业健康发展保驾护航。--   全面落实“产业集聚、产城融合、资源共享、产融互动”运营模式，引导中小微制造业企业入园集聚，推动区域块状特色产业向现代产业集群发展。--   秉持“让企业有个家，我才是企业家”的理念，为园区企业提供家一般的温暖。--    为中小微制造业企业嵌入全球供应链，走向“一带一路”，参与全球竞争提供平台支撑。----
         * data : [{"id":"166","title":"营业执照变更","desc":"","type":"1"},{"id":"167","title":"环评立项报告申请","desc":"","type":"1"},{"id":"168","title":"企业工商年报","desc":"","type":"1"},{"id":"169","title":"住所（营业场所）产权证明","desc":"","type":"1"},{"id":"170","title":"test","desc":"1","type":"1"},{"id":"182","title":"无偿使用证明","desc":"D-12营业执照地址变更无偿使用证明","type":"1"},{"id":"183","title":"员工子女上学","desc":"","type":"1"},{"id":"184","title":"变压器申报报装","desc":"","type":"1"},{"id":"186","title":"优惠券申请（测试）","desc":"","type":"2"},{"id":"196","title":"测试企服服务","desc":"","type":"2"}]
         */

        private String content;
        private List<DataBean> data;

        public String getContent() {
            return content;
        }

        public void setContent(String content) {
            this.content = content;
        }

        public List<DataBean> getData() {
            return data;
        }

        public void setData(List<DataBean> data) {
            this.data = data;
        }

        public static class DataBean {
            /**
             * id : 166
             * title : 营业执照变更
             * desc :
             * type : 1
             */

            private String id;
            private String title;
            private String desc;
            private String type;

            public String getId() {
                return id;
            }

            public void setId(String id) {
                this.id = id;
            }

            public String getTitle() {
                return title;
            }

            public void setTitle(String title) {
                this.title = title;
            }

            public String getDesc() {
                return desc;
            }

            public void setDesc(String desc) {
                this.desc = desc;
            }

            public String getType() {
                return type;
            }

            public void setType(String type) {
                this.type = type;
            }
        }
    }

    public static class RequestBean {
        /**
         * parkId : 1
         * typeId : 2
         */

        private String parkId;
        private String typeId;

        public String getParkId() {
            return parkId;
        }

        public void setParkId(String parkId) {
            this.parkId = parkId;
        }

        public String getTypeId() {
            return typeId;
        }

        public void setTypeId(String typeId) {
            this.typeId = typeId;
        }
    }


//
//    /**
//     * code : 200
//     * msg :
//     * response : {"content":"企业服务专业电路安装维修二、厨房卫生间下水管道马桶除臭\u2014专利下水管道防溢器\u2014\u2014彻底解决马桶管道溢水、下水管道返臭、地漏返臭等困扰卫生间的臭气异味困扰，还你一个清洁的健康的家庭。三、水管龙头管道维修：水管、水龙头、水管、水龙头、三角阀、花洒套培装、洗脸盆、淋浴房、卫浴镜、浴缸、五金挂件等安装维修。四、管道疏通加清洗：洗菜池、洗脸池下水管疏通、马桶、地漏疏通，疏通后较重要的是要管道清洗，保持干净联系我时，请说是在58同城看到的，谢谢！","data":[{"title":"商家优惠活动","introduction":"商家可在平台发布优惠活动","id":"2","type":"1"},{"title":"商家优惠活动","introduction":"商家可在平台发布优惠活动","id":"1","type":"1"},{"id":"168","type":"2","title":"企业工商年报","desc":""}]}
//     * request : {"parkId":"1","typeId":"2"}
//     * other : []
//     */
//
//    private int code;
//    private String msg;
//    private ResponseBean response;
//    private RequestBean request;
//    private List<?> other;
//
//    public int getCode() {
//        return code;
//    }
//
//    public void setCode(int code) {
//        this.code = code;
//    }
//
//    public String getMsg() {
//        return msg;
//    }
//
//    public void setMsg(String msg) {
//        this.msg = msg;
//    }
//
//    public ResponseBean getResponse() {
//        return response;
//    }
//
//    public void setResponse(ResponseBean response) {
//        this.response = response;
//    }
//
//    public RequestBean getRequest() {
//        return request;
//    }
//
//    public void setRequest(RequestBean request) {
//        this.request = request;
//    }
//
//    public List<?> getOther() {
//        return other;
//    }
//
//    public void setOther(List<?> other) {
//        this.other = other;
//    }
//
//    public static class ResponseBean {
//        /**
//         * content : 企业服务专业电路安装维修二、厨房卫生间下水管道马桶除臭—专利下水管道防溢器——彻底解决马桶管道溢水、下水管道返臭、地漏返臭等困扰卫生间的臭气异味困扰，还你一个清洁的健康的家庭。三、水管龙头管道维修：水管、水龙头、水管、水龙头、三角阀、花洒套培装、洗脸盆、淋浴房、卫浴镜、浴缸、五金挂件等安装维修。四、管道疏通加清洗：洗菜池、洗脸池下水管疏通、马桶、地漏疏通，疏通后较重要的是要管道清洗，保持干净联系我时，请说是在58同城看到的，谢谢！
//         * data : [{"title":"商家优惠活动","introduction":"商家可在平台发布优惠活动","id":"2","type":"1"},{"title":"商家优惠活动","introduction":"商家可在平台发布优惠活动","id":"1","type":"1"},{"id":"168","type":"2","title":"企业工商年报","desc":""}]
//         */
//
//        private String content;
//        private List<DataBean> data;
//
//        public String getContent() {
//            return content;
//        }
//
//        public void setContent(String content) {
//            this.content = content;
//        }
//
//        public List<DataBean> getData() {
//            return data;
//        }
//
//        public void setData(List<DataBean> data) {
//            this.data = data;
//        }
//
//        public static class DataBean {
//            /**
//             * title : 商家优惠活动
//             * introduction : 商家可在平台发布优惠活动
//             * id : 2
//             * type : 1
//             * desc :
//             */
//
//            private String title;
//            private String introduction;
//            private String id;
//            private String type;
//            private String desc;
//
//            public String getTitle() {
//                return title;
//            }
//
//            public void setTitle(String title) {
//                this.title = title;
//            }
//
//            public String getIntroduction() {
//                return introduction;
//            }
//
//            public void setIntroduction(String introduction) {
//                this.introduction = introduction;
//            }
//
//            public String getId() {
//                return id;
//            }
//
//            public void setId(String id) {
//                this.id = id;
//            }
//
//            public String getType() {
//                return type;
//            }
//
//            public void setType(String type) {
//                this.type = type;
//            }
//
//            public String getDesc() {
//                return desc;
//            }
//
//            public void setDesc(String desc) {
//                this.desc = desc;
//            }
//        }
//    }
//
//    public static class RequestBean {
//        /**
//         * parkId : 1
//         * typeId : 2
//         */
//
//        private String parkId;
//        private String typeId;
//
//        public String getParkId() {
//            return parkId;
//        }
//
//        public void setParkId(String parkId) {
//            this.parkId = parkId;
//        }
//
//        public String getTypeId() {
//            return typeId;
//        }
//
//        public void setTypeId(String typeId) {
//            this.typeId = typeId;
//        }
//    }
}
