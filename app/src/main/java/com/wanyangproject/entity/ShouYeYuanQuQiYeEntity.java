package com.wanyangproject.entity;

import java.util.List;

/**
 * Created by 甜甜 on 2018/8/15.
 */

public class ShouYeYuanQuQiYeEntity {
    /**
     * code : 200
     * msg :
     * response : {"total":"332","pageSize":"20","list":{"P":[{"enterId":"45","enterName":"平阳马太贵金属有限公司","logoUrl":"http://auth.vyzcc.com/restfiles/appfiles/201708111515/2.jpg","parkId":"1","address":"温州平阳众创城","initials":"P"},{"enterId":"50","enterName":"平阳县香皇工艺品有限公司","logoUrl":"http://auth.vyzcc.com/restfiles/appfiles/201708111519/3.jpg","parkId":"1","address":"温州平阳众创城","initials":"P"},{"enterId":"54","enterName":"平阳县冠威汽配有限公司","logoUrl":"http://auth.vyzcc.com/restfiles/appfiles/201708111519/3.jpg","parkId":"1","address":"温州平阳众创城","initials":"P"},{"enterId":"55","enterName":"平阳县铭德电子科技有限公司","logoUrl":"http://auth.vyzcc.com/restfiles/appfiles/201708111519/3.jpg","parkId":"1","address":"温州平阳众创城","initials":"P"},{"enterId":"56","enterName":"平阳县璐希洁家居有限公司","logoUrl":"http://auth.vyzcc.com/restfiles/appfiles/201708111519/3.jpg","parkId":"1","address":"温州平阳众创城","initials":"P"},{"enterId":"59","enterName":"平阳县华洋纸业有限公司","logoUrl":"http://auth.vyzcc.com/restfiles/appfiles/201708111521/5.jpg","parkId":"1","address":"温州平阳众创城","initials":"P"},{"enterId":"64","enterName":"平阳县爱婴堂科技有限公司","logoUrl":"http://auth.vyzcc.com/restfiles/appfiles/201708111521/5.jpg","parkId":"1","address":"温州平阳众创城","initials":"P"},{"enterId":"65","enterName":"平阳晶诺模具有限公司","logoUrl":"http://auth.vyzcc.com/restfiles/appfiles/201708111522/6.jpg","parkId":"1","address":"温州平阳众创城","initials":"P"}],"W":[{"enterId":"43","enterName":"温州瑞伟电机有限公司","logoUrl":"http://park.hostop.net/upload/static/logo.png","parkId":"1","address":"温州平阳众创城","initials":"W"},{"enterId":"44","enterName":"温州嘉创印务有限公司","logoUrl":"http://auth.vyzcc.com/restfiles/appfiles/201708111515/2.jpg","parkId":"1","address":"温州平阳众创城","initials":"W"},{"enterId":"46","enterName":"温州鑫美和汽车配件有限公司","logoUrl":"http://auth.vyzcc.com/restfiles/appfiles/201708111515/2.jpg","parkId":"1","address":"温州平阳众创城","initials":"W"},{"enterId":"48","enterName":"温州新漳机车部件有限公司","logoUrl":"http://auth.vyzcc.com/restfiles/appfiles/201708111515/2.jpg","parkId":"1","address":"温州平阳众创城","initials":"W"},{"enterId":"49","enterName":"温州市龙芯电子有限公司","logoUrl":"http://auth.vyzcc.com/restfiles/appfiles/201708111515/2.jpg","parkId":"1","address":"温州平阳众创城","initials":"W"},{"enterId":"51","enterName":"温州金久热交换器科技有限公司","logoUrl":"http://auth.vyzcc.com/restfiles/appfiles/201708111519/3.jpg","parkId":"1","address":"温州平阳众创城","initials":"W"},{"enterId":"52","enterName":"温州绿泰纸塑包装有限公司","logoUrl":"http://auth.vyzcc.com/restfiles/appfiles/201708111519/3.jpg","parkId":"1","address":"温州平阳众创城","initials":"W"},{"enterId":"53","enterName":"温州邦宝机械有限公司","logoUrl":"http://auth.vyzcc.com/restfiles/appfiles/201708111519/3.jpg","parkId":"1","address":"温州平阳众创城","initials":"W"},{"enterId":"57","enterName":"温州驰骋制罐有限公司","logoUrl":"http://auth.vyzcc.com/restfiles/appfiles/201708111519/3.jpg","parkId":"1","address":"温州平阳众创城","initials":"W"},{"enterId":"60","enterName":"温州源美包装材料有限公司","logoUrl":"http://auth.vyzcc.com/restfiles/appfiles/201708111521/5.jpg","parkId":"1","address":"温州平阳众创城","initials":"W"},{"enterId":"61","enterName":"温州国涵纸业有限公司","logoUrl":"http://auth.vyzcc.com/restfiles/appfiles/201708111521/5.jpg","parkId":"1","address":"温州平阳众创城","initials":"W"},{"enterId":"62","enterName":"温州韵瑞电器有限公司","logoUrl":"http://auth.vyzcc.com/restfiles/appfiles/201708111521/5.jpg","parkId":"1","address":"温州平阳众创城","initials":"W"}]}}
     * request : {"parkId":"1","pageIndex":"0","enterName":""}
     * other : []
     */

    private int code;
    private String msg;
    private ResponseBean response;
    private RequestBean request;
    private List<?> other;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public ResponseBean getResponse() {
        return response;
    }

    public void setResponse(ResponseBean response) {
        this.response = response;
    }

    public RequestBean getRequest() {
        return request;
    }

    public void setRequest(RequestBean request) {
        this.request = request;
    }

    public List<?> getOther() {
        return other;
    }

    public void setOther(List<?> other) {
        this.other = other;
    }

    public static class ResponseBean {
        /**
         * total : 332
         * pageSize : 20
         * list : {"P":[{"enterId":"45","enterName":"平阳马太贵金属有限公司","logoUrl":"http://auth.vyzcc.com/restfiles/appfiles/201708111515/2.jpg","parkId":"1","address":"温州平阳众创城","initials":"P"},{"enterId":"50","enterName":"平阳县香皇工艺品有限公司","logoUrl":"http://auth.vyzcc.com/restfiles/appfiles/201708111519/3.jpg","parkId":"1","address":"温州平阳众创城","initials":"P"},{"enterId":"54","enterName":"平阳县冠威汽配有限公司","logoUrl":"http://auth.vyzcc.com/restfiles/appfiles/201708111519/3.jpg","parkId":"1","address":"温州平阳众创城","initials":"P"},{"enterId":"55","enterName":"平阳县铭德电子科技有限公司","logoUrl":"http://auth.vyzcc.com/restfiles/appfiles/201708111519/3.jpg","parkId":"1","address":"温州平阳众创城","initials":"P"},{"enterId":"56","enterName":"平阳县璐希洁家居有限公司","logoUrl":"http://auth.vyzcc.com/restfiles/appfiles/201708111519/3.jpg","parkId":"1","address":"温州平阳众创城","initials":"P"},{"enterId":"59","enterName":"平阳县华洋纸业有限公司","logoUrl":"http://auth.vyzcc.com/restfiles/appfiles/201708111521/5.jpg","parkId":"1","address":"温州平阳众创城","initials":"P"},{"enterId":"64","enterName":"平阳县爱婴堂科技有限公司","logoUrl":"http://auth.vyzcc.com/restfiles/appfiles/201708111521/5.jpg","parkId":"1","address":"温州平阳众创城","initials":"P"},{"enterId":"65","enterName":"平阳晶诺模具有限公司","logoUrl":"http://auth.vyzcc.com/restfiles/appfiles/201708111522/6.jpg","parkId":"1","address":"温州平阳众创城","initials":"P"}],"W":[{"enterId":"43","enterName":"温州瑞伟电机有限公司","logoUrl":"http://park.hostop.net/upload/static/logo.png","parkId":"1","address":"温州平阳众创城","initials":"W"},{"enterId":"44","enterName":"温州嘉创印务有限公司","logoUrl":"http://auth.vyzcc.com/restfiles/appfiles/201708111515/2.jpg","parkId":"1","address":"温州平阳众创城","initials":"W"},{"enterId":"46","enterName":"温州鑫美和汽车配件有限公司","logoUrl":"http://auth.vyzcc.com/restfiles/appfiles/201708111515/2.jpg","parkId":"1","address":"温州平阳众创城","initials":"W"},{"enterId":"48","enterName":"温州新漳机车部件有限公司","logoUrl":"http://auth.vyzcc.com/restfiles/appfiles/201708111515/2.jpg","parkId":"1","address":"温州平阳众创城","initials":"W"},{"enterId":"49","enterName":"温州市龙芯电子有限公司","logoUrl":"http://auth.vyzcc.com/restfiles/appfiles/201708111515/2.jpg","parkId":"1","address":"温州平阳众创城","initials":"W"},{"enterId":"51","enterName":"温州金久热交换器科技有限公司","logoUrl":"http://auth.vyzcc.com/restfiles/appfiles/201708111519/3.jpg","parkId":"1","address":"温州平阳众创城","initials":"W"},{"enterId":"52","enterName":"温州绿泰纸塑包装有限公司","logoUrl":"http://auth.vyzcc.com/restfiles/appfiles/201708111519/3.jpg","parkId":"1","address":"温州平阳众创城","initials":"W"},{"enterId":"53","enterName":"温州邦宝机械有限公司","logoUrl":"http://auth.vyzcc.com/restfiles/appfiles/201708111519/3.jpg","parkId":"1","address":"温州平阳众创城","initials":"W"},{"enterId":"57","enterName":"温州驰骋制罐有限公司","logoUrl":"http://auth.vyzcc.com/restfiles/appfiles/201708111519/3.jpg","parkId":"1","address":"温州平阳众创城","initials":"W"},{"enterId":"60","enterName":"温州源美包装材料有限公司","logoUrl":"http://auth.vyzcc.com/restfiles/appfiles/201708111521/5.jpg","parkId":"1","address":"温州平阳众创城","initials":"W"},{"enterId":"61","enterName":"温州国涵纸业有限公司","logoUrl":"http://auth.vyzcc.com/restfiles/appfiles/201708111521/5.jpg","parkId":"1","address":"温州平阳众创城","initials":"W"},{"enterId":"62","enterName":"温州韵瑞电器有限公司","logoUrl":"http://auth.vyzcc.com/restfiles/appfiles/201708111521/5.jpg","parkId":"1","address":"温州平阳众创城","initials":"W"}]}
         */

        private String total;
        private String pageSize;
        private ListBean list;

        public String getTotal() {
            return total;
        }

        public void setTotal(String total) {
            this.total = total;
        }

        public String getPageSize() {
            return pageSize;
        }

        public void setPageSize(String pageSize) {
            this.pageSize = pageSize;
        }

        public ListBean getList() {
            return list;
        }

        public void setList(ListBean list) {
            this.list = list;
        }

        public static class ListBean {
            private List<PBean> P;
            private List<WBean> W;

            public List<PBean> getP() {
                return P;
            }

            public void setP(List<PBean> P) {
                this.P = P;
            }

            public List<WBean> getW() {
                return W;
            }

            public void setW(List<WBean> W) {
                this.W = W;
            }

            public static class PBean {
                /**
                 * enterId : 45
                 * enterName : 平阳马太贵金属有限公司
                 * logoUrl : http://auth.vyzcc.com/restfiles/appfiles/201708111515/2.jpg
                 * parkId : 1
                 * address : 温州平阳众创城
                 * initials : P
                 */

                private String enterId;
                private String enterName;
                private String logoUrl;
                private String parkId;
                private String address;
                private String initials;

                public String getEnterId() {
                    return enterId;
                }

                public void setEnterId(String enterId) {
                    this.enterId = enterId;
                }

                public String getEnterName() {
                    return enterName;
                }

                public void setEnterName(String enterName) {
                    this.enterName = enterName;
                }

                public String getLogoUrl() {
                    return logoUrl;
                }

                public void setLogoUrl(String logoUrl) {
                    this.logoUrl = logoUrl;
                }

                public String getParkId() {
                    return parkId;
                }

                public void setParkId(String parkId) {
                    this.parkId = parkId;
                }

                public String getAddress() {
                    return address;
                }

                public void setAddress(String address) {
                    this.address = address;
                }

                public String getInitials() {
                    return initials;
                }

                public void setInitials(String initials) {
                    this.initials = initials;
                }
            }

            public static class WBean {
                /**
                 * enterId : 43
                 * enterName : 温州瑞伟电机有限公司
                 * logoUrl : http://park.hostop.net/upload/static/logo.png
                 * parkId : 1
                 * address : 温州平阳众创城
                 * initials : W
                 */

                private String enterId;
                private String enterName;
                private String logoUrl;
                private String parkId;
                private String address;
                private String initials;

                public String getEnterId() {
                    return enterId;
                }

                public void setEnterId(String enterId) {
                    this.enterId = enterId;
                }

                public String getEnterName() {
                    return enterName;
                }

                public void setEnterName(String enterName) {
                    this.enterName = enterName;
                }

                public String getLogoUrl() {
                    return logoUrl;
                }

                public void setLogoUrl(String logoUrl) {
                    this.logoUrl = logoUrl;
                }

                public String getParkId() {
                    return parkId;
                }

                public void setParkId(String parkId) {
                    this.parkId = parkId;
                }

                public String getAddress() {
                    return address;
                }

                public void setAddress(String address) {
                    this.address = address;
                }

                public String getInitials() {
                    return initials;
                }

                public void setInitials(String initials) {
                    this.initials = initials;
                }
            }
        }
    }

    public static class RequestBean {
        /**
         * parkId : 1
         * pageIndex : 0
         * enterName :
         */

        private String parkId;
        private String pageIndex;
        private String enterName;

        public String getParkId() {
            return parkId;
        }

        public void setParkId(String parkId) {
            this.parkId = parkId;
        }

        public String getPageIndex() {
            return pageIndex;
        }

        public void setPageIndex(String pageIndex) {
            this.pageIndex = pageIndex;
        }

        public String getEnterName() {
            return enterName;
        }

        public void setEnterName(String enterName) {
            this.enterName = enterName;
        }
    }


//    /**
//     * code : 200
//     * msg :
//     * response : {"total":"332","pageSize":"20","list":[{"enterId":"43","enterName":"温州瑞伟电机有限公司","logoUrl":"http://park.hostop.net/upload/static/logo.png","parkId":"1","address":"温州平阳众创城"},{"enterId":"44","enterName":"温州嘉创印务有限公司","logoUrl":"http://auth.vyzcc.com/restfiles/appfiles/201708111515/2.jpg","parkId":"1","address":"温州平阳众创城"},{"enterId":"45","enterName":"平阳马太贵金属有限公司","logoUrl":"http://auth.vyzcc.com/restfiles/appfiles/201708111515/2.jpg","parkId":"1","address":"温州平阳众创城"},{"enterId":"46","enterName":"温州鑫美和汽车配件有限公司","logoUrl":"http://auth.vyzcc.com/restfiles/appfiles/201708111515/2.jpg","parkId":"1","address":"温州平阳众创城"},{"enterId":"48","enterName":"温州新漳机车部件有限公司","logoUrl":"http://auth.vyzcc.com/restfiles/appfiles/201708111515/2.jpg","parkId":"1","address":"温州平阳众创城"},{"enterId":"49","enterName":"温州市龙芯电子有限公司","logoUrl":"http://auth.vyzcc.com/restfiles/appfiles/201708111515/2.jpg","parkId":"1","address":"温州平阳众创城"},{"enterId":"50","enterName":"平阳县香皇工艺品有限公司","logoUrl":"http://auth.vyzcc.com/restfiles/appfiles/201708111519/3.jpg","parkId":"1","address":"温州平阳众创城"},{"enterId":"51","enterName":"温州金久热交换器科技有限公司","logoUrl":"http://auth.vyzcc.com/restfiles/appfiles/201708111519/3.jpg","parkId":"1","address":"温州平阳众创城"},{"enterId":"52","enterName":"温州绿泰纸塑包装有限公司","logoUrl":"http://auth.vyzcc.com/restfiles/appfiles/201708111519/3.jpg","parkId":"1","address":"温州平阳众创城"},{"enterId":"53","enterName":"温州邦宝机械有限公司","logoUrl":"http://auth.vyzcc.com/restfiles/appfiles/201708111519/3.jpg","parkId":"1","address":"温州平阳众创城"},{"enterId":"54","enterName":"平阳县冠威汽配有限公司","logoUrl":"http://auth.vyzcc.com/restfiles/appfiles/201708111519/3.jpg","parkId":"1","address":"温州平阳众创城"},{"enterId":"55","enterName":"平阳县铭德电子科技有限公司","logoUrl":"http://auth.vyzcc.com/restfiles/appfiles/201708111519/3.jpg","parkId":"1","address":"温州平阳众创城"},{"enterId":"56","enterName":"平阳县璐希洁家居有限公司","logoUrl":"http://auth.vyzcc.com/restfiles/appfiles/201708111519/3.jpg","parkId":"1","address":"温州平阳众创城"},{"enterId":"57","enterName":"温州驰骋制罐有限公司","logoUrl":"http://auth.vyzcc.com/restfiles/appfiles/201708111519/3.jpg","parkId":"1","address":"温州平阳众创城"},{"enterId":"59","enterName":"平阳县华洋纸业有限公司","logoUrl":"http://auth.vyzcc.com/restfiles/appfiles/201708111521/5.jpg","parkId":"1","address":"温州平阳众创城"},{"enterId":"60","enterName":"温州源美包装材料有限公司","logoUrl":"http://auth.vyzcc.com/restfiles/appfiles/201708111521/5.jpg","parkId":"1","address":"温州平阳众创城"},{"enterId":"61","enterName":"温州国涵纸业有限公司","logoUrl":"http://auth.vyzcc.com/restfiles/appfiles/201708111521/5.jpg","parkId":"1","address":"温州平阳众创城"},{"enterId":"62","enterName":"温州韵瑞电器有限公司","logoUrl":"http://auth.vyzcc.com/restfiles/appfiles/201708111521/5.jpg","parkId":"1","address":"温州平阳众创城"},{"enterId":"64","enterName":"平阳县爱婴堂科技有限公司","logoUrl":"http://auth.vyzcc.com/restfiles/appfiles/201708111521/5.jpg","parkId":"1","address":"温州平阳众创城"},{"enterId":"65","enterName":"平阳晶诺模具有限公司","logoUrl":"http://auth.vyzcc.com/restfiles/appfiles/201708111522/6.jpg","parkId":"1","address":"温州平阳众创城"}]}
//     * request : {"parkId":"1"}
//     * other : []
//     */
//
//    private int code;
//    private String msg;
//    private ResponseBean response;
//    private RequestBean request;
//    private List<?> other;
//
//    public int getCode() {
//        return code;
//    }
//
//    public void setCode(int code) {
//        this.code = code;
//    }
//
//    public String getMsg() {
//        return msg;
//    }
//
//    public void setMsg(String msg) {
//        this.msg = msg;
//    }
//
//    public ResponseBean getResponse() {
//        return response;
//    }
//
//    public void setResponse(ResponseBean response) {
//        this.response = response;
//    }
//
//    public RequestBean getRequest() {
//        return request;
//    }
//
//    public void setRequest(RequestBean request) {
//        this.request = request;
//    }
//
//    public List<?> getOther() {
//        return other;
//    }
//
//    public void setOther(List<?> other) {
//        this.other = other;
//    }
//
//    public static class ResponseBean {
//        /**
//         * total : 332
//         * pageSize : 20
//         * list : [{"enterId":"43","enterName":"温州瑞伟电机有限公司","logoUrl":"http://park.hostop.net/upload/static/logo.png","parkId":"1","address":"温州平阳众创城"},{"enterId":"44","enterName":"温州嘉创印务有限公司","logoUrl":"http://auth.vyzcc.com/restfiles/appfiles/201708111515/2.jpg","parkId":"1","address":"温州平阳众创城"},{"enterId":"45","enterName":"平阳马太贵金属有限公司","logoUrl":"http://auth.vyzcc.com/restfiles/appfiles/201708111515/2.jpg","parkId":"1","address":"温州平阳众创城"},{"enterId":"46","enterName":"温州鑫美和汽车配件有限公司","logoUrl":"http://auth.vyzcc.com/restfiles/appfiles/201708111515/2.jpg","parkId":"1","address":"温州平阳众创城"},{"enterId":"48","enterName":"温州新漳机车部件有限公司","logoUrl":"http://auth.vyzcc.com/restfiles/appfiles/201708111515/2.jpg","parkId":"1","address":"温州平阳众创城"},{"enterId":"49","enterName":"温州市龙芯电子有限公司","logoUrl":"http://auth.vyzcc.com/restfiles/appfiles/201708111515/2.jpg","parkId":"1","address":"温州平阳众创城"},{"enterId":"50","enterName":"平阳县香皇工艺品有限公司","logoUrl":"http://auth.vyzcc.com/restfiles/appfiles/201708111519/3.jpg","parkId":"1","address":"温州平阳众创城"},{"enterId":"51","enterName":"温州金久热交换器科技有限公司","logoUrl":"http://auth.vyzcc.com/restfiles/appfiles/201708111519/3.jpg","parkId":"1","address":"温州平阳众创城"},{"enterId":"52","enterName":"温州绿泰纸塑包装有限公司","logoUrl":"http://auth.vyzcc.com/restfiles/appfiles/201708111519/3.jpg","parkId":"1","address":"温州平阳众创城"},{"enterId":"53","enterName":"温州邦宝机械有限公司","logoUrl":"http://auth.vyzcc.com/restfiles/appfiles/201708111519/3.jpg","parkId":"1","address":"温州平阳众创城"},{"enterId":"54","enterName":"平阳县冠威汽配有限公司","logoUrl":"http://auth.vyzcc.com/restfiles/appfiles/201708111519/3.jpg","parkId":"1","address":"温州平阳众创城"},{"enterId":"55","enterName":"平阳县铭德电子科技有限公司","logoUrl":"http://auth.vyzcc.com/restfiles/appfiles/201708111519/3.jpg","parkId":"1","address":"温州平阳众创城"},{"enterId":"56","enterName":"平阳县璐希洁家居有限公司","logoUrl":"http://auth.vyzcc.com/restfiles/appfiles/201708111519/3.jpg","parkId":"1","address":"温州平阳众创城"},{"enterId":"57","enterName":"温州驰骋制罐有限公司","logoUrl":"http://auth.vyzcc.com/restfiles/appfiles/201708111519/3.jpg","parkId":"1","address":"温州平阳众创城"},{"enterId":"59","enterName":"平阳县华洋纸业有限公司","logoUrl":"http://auth.vyzcc.com/restfiles/appfiles/201708111521/5.jpg","parkId":"1","address":"温州平阳众创城"},{"enterId":"60","enterName":"温州源美包装材料有限公司","logoUrl":"http://auth.vyzcc.com/restfiles/appfiles/201708111521/5.jpg","parkId":"1","address":"温州平阳众创城"},{"enterId":"61","enterName":"温州国涵纸业有限公司","logoUrl":"http://auth.vyzcc.com/restfiles/appfiles/201708111521/5.jpg","parkId":"1","address":"温州平阳众创城"},{"enterId":"62","enterName":"温州韵瑞电器有限公司","logoUrl":"http://auth.vyzcc.com/restfiles/appfiles/201708111521/5.jpg","parkId":"1","address":"温州平阳众创城"},{"enterId":"64","enterName":"平阳县爱婴堂科技有限公司","logoUrl":"http://auth.vyzcc.com/restfiles/appfiles/201708111521/5.jpg","parkId":"1","address":"温州平阳众创城"},{"enterId":"65","enterName":"平阳晶诺模具有限公司","logoUrl":"http://auth.vyzcc.com/restfiles/appfiles/201708111522/6.jpg","parkId":"1","address":"温州平阳众创城"}]
//         */
//
//        private String total;
//        private String pageSize;
//        private List<ListBean> list;
//
//        public String getTotal() {
//            return total;
//        }
//
//        public void setTotal(String total) {
//            this.total = total;
//        }
//
//        public String getPageSize() {
//            return pageSize;
//        }
//
//        public void setPageSize(String pageSize) {
//            this.pageSize = pageSize;
//        }
//
//        public List<ListBean> getList() {
//            return list;
//        }
//
//        public void setList(List<ListBean> list) {
//            this.list = list;
//        }
//
//        public static class ListBean {
//            /**
//             * enterId : 43
//             * enterName : 温州瑞伟电机有限公司
//             * logoUrl : http://park.hostop.net/upload/static/logo.png
//             * parkId : 1
//             * address : 温州平阳众创城
//             */
//
//            private String enterId;
//            private String enterName;
//            private String logoUrl;
//            private String parkId;
//            private String address;
//
//            public String getEnterId() {
//                return enterId;
//            }
//
//            public void setEnterId(String enterId) {
//                this.enterId = enterId;
//            }
//
//            public String getEnterName() {
//                return enterName;
//            }
//
//            public void setEnterName(String enterName) {
//                this.enterName = enterName;
//            }
//
//            public String getLogoUrl() {
//                return logoUrl;
//            }
//
//            public void setLogoUrl(String logoUrl) {
//                this.logoUrl = logoUrl;
//            }
//
//            public String getParkId() {
//                return parkId;
//            }
//
//            public void setParkId(String parkId) {
//                this.parkId = parkId;
//            }
//
//            public String getAddress() {
//                return address;
//            }
//
//            public void setAddress(String address) {
//                this.address = address;
//            }
//        }
//    }
//
//    public static class RequestBean {
//        /**
//         * parkId : 1
//         */
//
//        private String parkId;
//
//        public String getParkId() {
//            return parkId;
//        }
//
//        public void setParkId(String parkId) {
//            this.parkId = parkId;
//        }
//    }
}
