package com.wanyangproject.entity;

import java.util.List;

/**
 * Created by 甜甜 on 2018/8/24.
 */

public class WuYeFuWuLieBiaoEntity {

    /**
     * code : 200
     * msg :
     * response : {"content":"专业电路安装维修二、厨房卫生间下水管道马桶除臭\u2014专利下水管道防溢器\u2014\u2014彻底解决马桶管道溢水、下水管道返臭、地漏返臭等困扰卫生间的臭气异味困扰，还你一个清洁的健康的家庭。三、水管龙头管道维修：水管、水龙头、水管、水龙头、三角阀、花洒套培装、洗脸盆、淋浴房、卫浴镜、浴缸、五金挂件等安装维修。四、管道疏通加清洗：洗菜池、洗脸池下水管疏通、马桶、地漏疏通，疏通后较重要的是要管道清洗，保持干净联系我时，请说是在58同城看到的，谢谢！","data":[{"id":"24","title":"保洁整理","desc":"进行保洁整理一类的内容"},{"id":"23","title":"空调滴水","desc":"维修各种空调漏水"},{"id":"22","title":"电路维修","desc":"维修各种电路，接线 不通"},{"id":"21","title":"门窗损坏","desc":"维修各类门窗"}]}
     * request : {"parkId":"1","typeId":"1"}
     * other : []
     */

    private int code;
    private String msg;
    private ResponseBean response;
    private RequestBean request;
    private List<?> other;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public ResponseBean getResponse() {
        return response;
    }

    public void setResponse(ResponseBean response) {
        this.response = response;
    }

    public RequestBean getRequest() {
        return request;
    }

    public void setRequest(RequestBean request) {
        this.request = request;
    }

    public List<?> getOther() {
        return other;
    }

    public void setOther(List<?> other) {
        this.other = other;
    }

    public static class ResponseBean {
        /**
         * content : 专业电路安装维修二、厨房卫生间下水管道马桶除臭—专利下水管道防溢器——彻底解决马桶管道溢水、下水管道返臭、地漏返臭等困扰卫生间的臭气异味困扰，还你一个清洁的健康的家庭。三、水管龙头管道维修：水管、水龙头、水管、水龙头、三角阀、花洒套培装、洗脸盆、淋浴房、卫浴镜、浴缸、五金挂件等安装维修。四、管道疏通加清洗：洗菜池、洗脸池下水管疏通、马桶、地漏疏通，疏通后较重要的是要管道清洗，保持干净联系我时，请说是在58同城看到的，谢谢！
         * data : [{"id":"24","title":"保洁整理","desc":"进行保洁整理一类的内容"},{"id":"23","title":"空调滴水","desc":"维修各种空调漏水"},{"id":"22","title":"电路维修","desc":"维修各种电路，接线 不通"},{"id":"21","title":"门窗损坏","desc":"维修各类门窗"}]
         */

        private String content;
        private List<DataBean> data;
        public static List<DataBean> leixing;

        public String getContent() {
            return content;
        }

        public void setContent(String content) {
            this.content = content;
        }

        public List<DataBean> getData() {
            return data;
        }

        public void setData(List<DataBean> data) {
            this.data = data;
        }

        public static class DataBean {
            /**
             * id : 24
             * title : 保洁整理
             * desc : 进行保洁整理一类的内容
             */

            private String id;
            private String title;
            private String desc;

            public String getId() {
                return id;
            }

            public void setId(String id) {
                this.id = id;
            }

            public String getTitle() {
                return title;
            }

            public void setTitle(String title) {
                this.title = title;
            }

            public String getDesc() {
                return desc;
            }

            public void setDesc(String desc) {
                this.desc = desc;
            }
        }
    }

    public static class RequestBean {
        /**
         * parkId : 1
         * typeId : 1
         */

        private String parkId;
        private String typeId;

        public String getParkId() {
            return parkId;
        }

        public void setParkId(String parkId) {
            this.parkId = parkId;
        }

        public String getTypeId() {
            return typeId;
        }

        public void setTypeId(String typeId) {
            this.typeId = typeId;
        }
    }
}
