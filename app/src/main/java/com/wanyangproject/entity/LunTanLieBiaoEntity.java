package com.wanyangproject.entity;

import com.chad.library.adapter.base.entity.MultiItemEntity;

import java.util.List;

/**
 * Created by 甜甜 on 2018/8/9.
 */

public class LunTanLieBiaoEntity {
    /**
     * code : 200
     * msg :
     * response : {"list":[{"id":"13","title":"测试","add_time":"20小时前","uinfo":{"userid":"2","avatar":"http://park.hostop.net/static/image/avatar.jpg","nickname":"未填写"},"imgArr":["http://park.hostop.net//upload/20180813/8fc3a20488f6436aa1a86bd9474784df.jpg","http://park.hostop.net//upload/20180813/30039c7a1a83855f3fd1aa38f5d6a6db.jpg","http://park.hostop.net//upload/20180813/7859b4190e49484ecb94b4f1c80ca18d.jpg",""],"reply":"1","like":"1","isLike":"1"},{"id":"10","title":"今天天气不错啊","add_time":"4天前","uinfo":{"userid":"2","avatar":"http://park.hostop.net/static/image/avatar.jpg","nickname":"未填写"},"imgArr":[""],"reply":"3","like":"1","isLike":"1"},{"id":"9","title":"iOS.  Luntan.   Ce   Shi","add_time":"5天前","uinfo":{"userid":"2","avatar":"http://park.hostop.net/static/image/avatar.jpg","nickname":"未填写"},"imgArr":[""],"reply":"1","like":"0","isLike":"0"}],"nextPage":"1"}
     * request : {"parkId":"1","typeId":"0"}
     * other : []
     */

    private int code;
    private String msg;
    private ResponseBean response;
    private RequestBean request;
    private List<?> other;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public ResponseBean getResponse() {
        return response;
    }

    public void setResponse(ResponseBean response) {
        this.response = response;
    }

    public RequestBean getRequest() {
        return request;
    }

    public void setRequest(RequestBean request) {
        this.request = request;
    }

    public List<?> getOther() {
        return other;
    }

    public void setOther(List<?> other) {
        this.other = other;
    }

    public static class ResponseBean {
        /**
         * list : [{"id":"13","title":"测试","add_time":"20小时前","uinfo":{"userid":"2","avatar":"http://park.hostop.net/static/image/avatar.jpg","nickname":"未填写"},"imgArr":["http://park.hostop.net//upload/20180813/8fc3a20488f6436aa1a86bd9474784df.jpg","http://park.hostop.net//upload/20180813/30039c7a1a83855f3fd1aa38f5d6a6db.jpg","http://park.hostop.net//upload/20180813/7859b4190e49484ecb94b4f1c80ca18d.jpg",""],"reply":"1","like":"1","isLike":"1"},{"id":"10","title":"今天天气不错啊","add_time":"4天前","uinfo":{"userid":"2","avatar":"http://park.hostop.net/static/image/avatar.jpg","nickname":"未填写"},"imgArr":[""],"reply":"3","like":"1","isLike":"1"},{"id":"9","title":"iOS.  Luntan.   Ce   Shi","add_time":"5天前","uinfo":{"userid":"2","avatar":"http://park.hostop.net/static/image/avatar.jpg","nickname":"未填写"},"imgArr":[""],"reply":"1","like":"0","isLike":"0"}]
         * nextPage : 1
         */

        private String nextPage;
        private List<ListBean> list;

        public String getNextPage() {
            return nextPage;
        }

        public void setNextPage(String nextPage) {
            this.nextPage = nextPage;
        }

        public List<ListBean> getList() {
            return list;
        }

        public void setList(List<ListBean> list) {
            this.list = list;
        }

        public static class ListBean implements MultiItemEntity{
            /**
             * id : 13
             * title : 测试
             * add_time : 20小时前
             * uinfo : {"userid":"2","avatar":"http://park.hostop.net/static/image/avatar.jpg","nickname":"未填写"}
             * imgArr : ["http://park.hostop.net//upload/20180813/8fc3a20488f6436aa1a86bd9474784df.jpg","http://park.hostop.net//upload/20180813/30039c7a1a83855f3fd1aa38f5d6a6db.jpg","http://park.hostop.net//upload/20180813/7859b4190e49484ecb94b4f1c80ca18d.jpg",""]
             * reply : 1
             * like : 1
             * isLike : 1
             */

            private String id;
            private String title;
            private String content;
            private String add_time;
            private UinfoBean uinfo;
            private String reply;
            private String like;
            private String isLike;
            private List<String> imgArr;
            private String isPc;


            public String getIsPc() {
                return isPc;
            }

            public void setIsPc(String isPc) {
                this.isPc = isPc;
            }

            public static final int LING = 0;
            public static final int ONE = 1;
            public static final int TWO = 2;
            public static final int THREE = 3;
            public static final int SEX = 6;
            public static final int NINE = 9;


            private int itemType;

            public void setItemType(int itemType) {
                this.itemType = itemType;
            }

            @Override
            public int getItemType() {
                return itemType;
            }

            public String getContent() {
                return content;
            }

            public void setContent(String content) {
                this.content = content;
            }

            public String getId() {
                return id;
            }

            public void setId(String id) {
                this.id = id;
            }

            public String getTitle() {
                return title;
            }

            public void setTitle(String title) {
                this.title = title;
            }

            public String getAdd_time() {
                return add_time;
            }

            public void setAdd_time(String add_time) {
                this.add_time = add_time;
            }

            public UinfoBean getUinfo() {
                return uinfo;
            }

            public void setUinfo(UinfoBean uinfo) {
                this.uinfo = uinfo;
            }

            public String getReply() {
                return reply;
            }

            public void setReply(String reply) {
                this.reply = reply;
            }

            public String getLike() {
                return like;
            }

            public void setLike(String like) {
                this.like = like;
            }

            public String getIsLike() {
                return isLike;
            }

            public void setIsLike(String isLike) {
                this.isLike = isLike;
            }

            public List<String> getImgArr() {
                return imgArr;
            }

            public void setImgArr(List<String> imgArr) {
                this.imgArr = imgArr;
            }

            public static class UinfoBean {
                /**
                 * userid : 2
                 * avatar : http://park.hostop.net/static/image/avatar.jpg
                 * nickname : 未填写
                 */

                private String userid;
                private String avatar;
                private String nickname;

                public String getUserid() {
                    return userid;
                }

                public void setUserid(String userid) {
                    this.userid = userid;
                }

                public String getAvatar() {
                    return avatar;
                }

                public void setAvatar(String avatar) {
                    this.avatar = avatar;
                }

                public String getNickname() {
                    return nickname;
                }

                public void setNickname(String nickname) {
                    this.nickname = nickname;
                }
            }
        }
    }

    public static class RequestBean {
        /**
         * parkId : 1
         * typeId : 0
         */

        private String parkId;
        private String typeId;

        public String getParkId() {
            return parkId;
        }

        public void setParkId(String parkId) {
            this.parkId = parkId;
        }

        public String getTypeId() {
            return typeId;
        }

        public void setTypeId(String typeId) {
            this.typeId = typeId;
        }
    }


//    /**
//     * code : 200
//     * msg :
//     * response : {"list":[{"id":"9","title":"iOS.  Luntan.   Ce   Shi","add_time":"12小时前","uinfo":{"userid":"2","avatar":"http://park.hostop.net/static/image/avatar.jpg","nickname":"未填写"},"imgArr":[""],"reply":"0","like":"0"}],"nextPage":"1"}
//     * request : {"parkId":"1","typeId":"0"}
//     * other : []
//     */
//
//    private int code;
//    private String msg;
//    private ResponseBean response;
//    private RequestBean request;
//    private List<?> other;
//
//    public int getCode() {
//        return code;
//    }
//
//    public void setCode(int code) {
//        this.code = code;
//    }
//
//    public String getMsg() {
//        return msg;
//    }
//
//    public void setMsg(String msg) {
//        this.msg = msg;
//    }
//
//    public ResponseBean getResponse() {
//        return response;
//    }
//
//    public void setResponse(ResponseBean response) {
//        this.response = response;
//    }
//
//    public RequestBean getRequest() {
//        return request;
//    }
//
//    public void setRequest(RequestBean request) {
//        this.request = request;
//    }
//
//    public List<?> getOther() {
//        return other;
//    }
//
//    public void setOther(List<?> other) {
//        this.other = other;
//    }
//
//    public static class ResponseBean {
//        /**
//         * list : [{"id":"9","title":"iOS.  Luntan.   Ce   Shi","add_time":"12小时前","uinfo":{"userid":"2","avatar":"http://park.hostop.net/static/image/avatar.jpg","nickname":"未填写"},"imgArr":[""],"reply":"0","like":"0"}]
//         * nextPage : 1
//         */
//
//        private String nextPage;
//        private List<ListBean> list;
//
//        public String getNextPage() {
//            return nextPage;
//        }
//
//        public void setNextPage(String nextPage) {
//            this.nextPage = nextPage;
//        }
//
//        public List<ListBean> getList() {
//            return list;
//        }
//
//        public void setList(List<ListBean> list) {
//            this.list = list;
//        }
//
//        public static class ListBean implements MultiItemEntity{
//            /**
//             * id : 9
//             * title : iOS.  Luntan.   Ce   Shi
//             * add_time : 12小时前
//             * uinfo : {"userid":"2","avatar":"http://park.hostop.net/static/image/avatar.jpg","nickname":"未填写"}
//             * imgArr : [""]
//             * reply : 0
//             * like : 0
//             */
//
//
//
//            public static final int LING = 0;
//            public static final int ONE = 1;
//            public static final int TWO = 2;
//            public static final int THREE = 3;
//
//
//            private int itemType;
//
//            public void setItemType(int itemType) {
//                this.itemType = itemType;
//            }
//
//            @Override
//            public int getItemType() {
//                return itemType;
//            }
//
//            private String id;
//            private String title;
//            private String add_time;
//            private UinfoBean uinfo;
//            private String reply;
//            private String like;
//            private List<String> imgArr;
//
//            public String getId() {
//                return id;
//            }
//
//            public void setId(String id) {
//                this.id = id;
//            }
//
//            public String getTitle() {
//                return title;
//            }
//
//            public void setTitle(String title) {
//                this.title = title;
//            }
//
//            public String getAdd_time() {
//                return add_time;
//            }
//
//            public void setAdd_time(String add_time) {
//                this.add_time = add_time;
//            }
//
//            public UinfoBean getUinfo() {
//                return uinfo;
//            }
//
//            public void setUinfo(UinfoBean uinfo) {
//                this.uinfo = uinfo;
//            }
//
//            public String getReply() {
//                return reply;
//            }
//
//            public void setReply(String reply) {
//                this.reply = reply;
//            }
//
//            public String getLike() {
//                return like;
//            }
//
//            public void setLike(String like) {
//                this.like = like;
//            }
//
//            public List<String> getImgArr() {
//                return imgArr;
//            }
//
//            public void setImgArr(List<String> imgArr) {
//                this.imgArr = imgArr;
//            }
//
//
//            public static class UinfoBean {
//                /**
//                 * userid : 2
//                 * avatar : http://park.hostop.net/static/image/avatar.jpg
//                 * nickname : 未填写
//                 */
//
//
//
//
//                private String userid;
//                private String avatar;
//                private String nickname;
//
//                public String getUserid() {
//                    return userid;
//                }
//
//                public void setUserid(String userid) {
//                    this.userid = userid;
//                }
//
//                public String getAvatar() {
//                    return avatar;
//                }
//
//                public void setAvatar(String avatar) {
//                    this.avatar = avatar;
//                }
//
//                public String getNickname() {
//                    return nickname;
//                }
//
//                public void setNickname(String nickname) {
//                    this.nickname = nickname;
//                }
//            }
//        }
//    }
//
//    public static class RequestBean {
//        /**
//         * parkId : 1
//         * typeId : 0
//         */
//
//        private String parkId;
//        private String typeId;
//
//        public String getParkId() {
//            return parkId;
//        }
//
//        public void setParkId(String parkId) {
//            this.parkId = parkId;
//        }
//
//        public String getTypeId() {
//            return typeId;
//        }
//
//        public void setTypeId(String typeId) {
//            this.typeId = typeId;
//        }
//    }
}
