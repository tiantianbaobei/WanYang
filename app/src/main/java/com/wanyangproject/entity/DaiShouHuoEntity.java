package com.wanyangproject.entity;

import java.util.List;

/**
 * Created by 甜甜 on 2018/8/21.
 */

public class DaiShouHuoEntity {
    /**
     * code : 200
     * msg :
     * response : [{"order_sn":"pk2sn20180820184834","shop_id":"1","money":"40.0","type":"11","shop_name":"商家","shop_cost":"5","shop_portrait":"park.hostop.net//upload/20180823/bab664273f521cea285761a6e79569f6.png","goods":[{"num":"3","pic":"0.01","goods_name":"测试","master":"park.hostop.net//upload/20180804/2da84761de43d358bfd2f89627fdaf42.png","id":"4"},{"num":"2","pic":"10","goods_name":"商品2","master":"park.hostop.net//upload/20180804/963d0fc2a816b184851bb41116478e85.png","id":"2"}]},{"order_sn":"pk2sn20180820170835","shop_id":"1","money":"30.01","type":"11","shop_name":"商家","shop_cost":"5","shop_portrait":"park.hostop.net//upload/20180823/bab664273f521cea285761a6e79569f6.png","goods":[{"num":"3","pic":"0.01","goods_name":"测试","master":"park.hostop.net//upload/20180804/2da84761de43d358bfd2f89627fdaf42.png","id":"4"},{"num":"2","pic":"10","goods_name":"商品2","master":"park.hostop.net//upload/20180804/963d0fc2a816b184851bb41116478e85.png","id":"2"}]},{"order_sn":"pk2sn20180820165825","shop_id":"1","money":"50.0","type":"11","shop_name":"商家","shop_cost":"5","shop_portrait":"park.hostop.net//upload/20180823/bab664273f521cea285761a6e79569f6.png","goods":[{"num":"3","pic":"0.01","goods_name":"测试","master":"park.hostop.net//upload/20180804/2da84761de43d358bfd2f89627fdaf42.png","id":"4"},{"num":"2","pic":"10","goods_name":"商品2","master":"park.hostop.net//upload/20180804/963d0fc2a816b184851bb41116478e85.png","id":"2"}]},{"order_sn":"pk2sn20180820165110","shop_id":"1","money":"40.0","type":"11","shop_name":"商家","shop_cost":"5","shop_portrait":"park.hostop.net//upload/20180823/bab664273f521cea285761a6e79569f6.png","goods":[{"num":"3","pic":"0.01","goods_name":"测试","master":"park.hostop.net//upload/20180804/2da84761de43d358bfd2f89627fdaf42.png","id":"4"},{"num":"2","pic":"10","goods_name":"商品2","master":"park.hostop.net//upload/20180804/963d0fc2a816b184851bb41116478e85.png","id":"2"}]},{"order_sn":"pk2sn20180820164332","shop_id":"1","money":"40.0","type":"1","shop_name":"商家","shop_cost":"5","shop_portrait":"park.hostop.net//upload/20180823/bab664273f521cea285761a6e79569f6.png","goods":[{"num":"3","pic":"0.01","goods_name":"测试","master":"park.hostop.net//upload/20180804/2da84761de43d358bfd2f89627fdaf42.png","id":"4"},{"num":"2","pic":"10","goods_name":"商品2","master":"park.hostop.net//upload/20180804/963d0fc2a816b184851bb41116478e85.png","id":"2"}]},{"order_sn":"pk2sn20180820163537","shop_id":"1","money":"40.0","type":"1","shop_name":"商家","shop_cost":"5","shop_portrait":"park.hostop.net//upload/20180823/bab664273f521cea285761a6e79569f6.png","goods":[{"num":"3","pic":"0.01","goods_name":"测试","master":"park.hostop.net//upload/20180804/2da84761de43d358bfd2f89627fdaf42.png","id":"4"},{"num":"2","pic":"10","goods_name":"商品2","master":"park.hostop.net//upload/20180804/963d0fc2a816b184851bb41116478e85.png","id":"2"}]},{"order_sn":"pk2sn20180820161705","shop_id":"1","money":"40.0","type":"1","shop_name":"商家","shop_cost":"5","shop_portrait":"park.hostop.net//upload/20180823/bab664273f521cea285761a6e79569f6.png","goods":[{"num":"3","pic":"0.01","goods_name":"测试","master":"park.hostop.net//upload/20180804/2da84761de43d358bfd2f89627fdaf42.png","id":"4"},{"num":"2","pic":"10","goods_name":"商品2","master":"park.hostop.net//upload/20180804/963d0fc2a816b184851bb41116478e85.png","id":"2"}]},{"order_sn":"pk2sn20180820161111","shop_id":"1","money":"40.0","type":"1","shop_name":"商家","shop_cost":"5","shop_portrait":"park.hostop.net//upload/20180823/bab664273f521cea285761a6e79569f6.png","goods":[{"num":"3","pic":"0.01","goods_name":"测试","master":"park.hostop.net//upload/20180804/2da84761de43d358bfd2f89627fdaf42.png","id":"4"},{"num":"2","pic":"10","goods_name":"商品2","master":"park.hostop.net//upload/20180804/963d0fc2a816b184851bb41116478e85.png","id":"2"}]},{"order_sn":"pk2sn20180820160839","shop_id":"1","money":"40.0","type":"1","shop_name":"商家","shop_cost":"5","shop_portrait":"park.hostop.net//upload/20180823/bab664273f521cea285761a6e79569f6.png","goods":[{"num":"3","pic":"0.01","goods_name":"测试","master":"park.hostop.net//upload/20180804/2da84761de43d358bfd2f89627fdaf42.png","id":"4"},{"num":"2","pic":"10","goods_name":"商品2","master":"park.hostop.net//upload/20180804/963d0fc2a816b184851bb41116478e85.png","id":"2"}]},{"order_sn":"pk2sn20180820155925","shop_id":"1","money":"40.0","type":"1","shop_name":"商家","shop_cost":"5","shop_portrait":"park.hostop.net//upload/20180823/bab664273f521cea285761a6e79569f6.png","goods":[{"num":"3","pic":"0.01","goods_name":"测试","master":"park.hostop.net//upload/20180804/2da84761de43d358bfd2f89627fdaf42.png","id":"4"},{"num":"2","pic":"10","goods_name":"商品2","master":"park.hostop.net//upload/20180804/963d0fc2a816b184851bb41116478e85.png","id":"2"}]}]
     * request : {"parkId":"1"}
     * other : []
     */

    private int code;
    private String msg;
    private RequestBean request;
    private List<ResponseBean> response;
    private List<?> other;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public RequestBean getRequest() {
        return request;
    }

    public void setRequest(RequestBean request) {
        this.request = request;
    }

    public List<ResponseBean> getResponse() {
        return response;
    }

    public void setResponse(List<ResponseBean> response) {
        this.response = response;
    }

    public List<?> getOther() {
        return other;
    }

    public void setOther(List<?> other) {
        this.other = other;
    }

    public static class RequestBean {
        /**
         * parkId : 1
         */

        private String parkId;

        public String getParkId() {
            return parkId;
        }

        public void setParkId(String parkId) {
            this.parkId = parkId;
        }
    }

    public static class ResponseBean {
        /**
         * order_sn : pk2sn20180820184834
         * shop_id : 1
         * money : 40.0
         * type : 11
         * shop_name : 商家
         * shop_cost : 5
         * shop_portrait : park.hostop.net//upload/20180823/bab664273f521cea285761a6e79569f6.png
         * goods : [{"num":"3","pic":"0.01","goods_name":"测试","master":"park.hostop.net//upload/20180804/2da84761de43d358bfd2f89627fdaf42.png","id":"4"},{"num":"2","pic":"10","goods_name":"商品2","master":"park.hostop.net//upload/20180804/963d0fc2a816b184851bb41116478e85.png","id":"2"}]
         */

        private String order_sn;
        private String shop_id;
        private String money;
        private String type;
        private String shop_name;
        private String shop_cost;
        private String shop_portrait;
        private List<GoodsBean> goods;

        public String getOrder_sn() {
            return order_sn;
        }

        public void setOrder_sn(String order_sn) {
            this.order_sn = order_sn;
        }

        public String getShop_id() {
            return shop_id;
        }

        public void setShop_id(String shop_id) {
            this.shop_id = shop_id;
        }

        public String getMoney() {
            return money;
        }

        public void setMoney(String money) {
            this.money = money;
        }

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public String getShop_name() {
            return shop_name;
        }

        public void setShop_name(String shop_name) {
            this.shop_name = shop_name;
        }

        public String getShop_cost() {
            return shop_cost;
        }

        public void setShop_cost(String shop_cost) {
            this.shop_cost = shop_cost;
        }

        public String getShop_portrait() {
            return shop_portrait;
        }

        public void setShop_portrait(String shop_portrait) {
            this.shop_portrait = shop_portrait;
        }

        public List<GoodsBean> getGoods() {
            return goods;
        }

        public void setGoods(List<GoodsBean> goods) {
            this.goods = goods;
        }

        public static class GoodsBean {
            /**
             * num : 3
             * pic : 0.01
             * goods_name : 测试
             * master : park.hostop.net//upload/20180804/2da84761de43d358bfd2f89627fdaf42.png
             * id : 4
             */

            private String num;
            private String pic;
            private String goods_name;
            private String master;
            private String id;

            public String getNum() {
                return num;
            }

            public void setNum(String num) {
                this.num = num;
            }

            public String getPic() {
                return pic;
            }

            public void setPic(String pic) {
                this.pic = pic;
            }

            public String getGoods_name() {
                return goods_name;
            }

            public void setGoods_name(String goods_name) {
                this.goods_name = goods_name;
            }

            public String getMaster() {
                return master;
            }

            public void setMaster(String master) {
                this.master = master;
            }

            public String getId() {
                return id;
            }

            public void setId(String id) {
                this.id = id;
            }
        }
    }


//
//    /**
//     * code : 200
//     * msg :
//     * response : [{"order_sn":"pk2sn20180818111432","shop_id":"1","money":"0.01","type":"2","shop_name":"彪1123","shop_cost":"10","shop_portrait":"park.hostop.net/static/image/0.jpg","goods":[{"num":"3","pic":"0.01","goods_name":"测试","master":"park.hostop.net//upload/20180804/2da84761de43d358bfd2f89627fdaf42.png"},{"num":"2","pic":"10","goods_name":"商品2","master":"park.hostop.net//upload/20180804/963d0fc2a816b184851bb41116478e85.png"}]},{"order_sn":"pk2sn20180818110322","shop_id":"1","money":"0.01","type":"2","shop_name":"彪1123","shop_cost":"10","shop_portrait":"park.hostop.net/static/image/0.jpg","goods":[{"num":"3","pic":"0.01","goods_name":"测试","master":"park.hostop.net//upload/20180804/2da84761de43d358bfd2f89627fdaf42.png"},{"num":"2","pic":"10","goods_name":"商品2","master":"park.hostop.net//upload/20180804/963d0fc2a816b184851bb41116478e85.png"}]},{"order_sn":"pk2sn20180818105405","shop_id":"1","money":"0.01","type":"2","shop_name":"彪1123","shop_cost":"10","shop_portrait":"park.hostop.net/static/image/0.jpg","goods":[{"num":"3","pic":"0.01","goods_name":"测试","master":"park.hostop.net//upload/20180804/2da84761de43d358bfd2f89627fdaf42.png"},{"num":"2","pic":"10","goods_name":"商品2","master":"park.hostop.net//upload/20180804/963d0fc2a816b184851bb41116478e85.png"}]},{"order_sn":"pk2sn20180818104437","shop_id":"1","money":"0.01","type":"2","shop_name":"彪1123","shop_cost":"10","shop_portrait":"park.hostop.net/static/image/0.jpg","goods":[{"num":"3","pic":"0.01","goods_name":"测试","master":"park.hostop.net//upload/20180804/2da84761de43d358bfd2f89627fdaf42.png"},{"num":"2","pic":"10","goods_name":"商品2","master":"park.hostop.net//upload/20180804/963d0fc2a816b184851bb41116478e85.png"}]},{"order_sn":"pk2sn20180818102253","shop_id":"1","money":"0.01","type":"2","shop_name":"彪1123","shop_cost":"10","shop_portrait":"park.hostop.net/static/image/0.jpg","goods":[{"num":"3","pic":"0.01","goods_name":"测试","master":"park.hostop.net//upload/20180804/2da84761de43d358bfd2f89627fdaf42.png"},{"num":"2","pic":"10","goods_name":"商品2","master":"park.hostop.net//upload/20180804/963d0fc2a816b184851bb41116478e85.png"}]}]
//     * request : {"parkId":"1","type":"2"}
//     * other : []
//     */
//
//    private int code;
//    private String msg;
//    private RequestBean request;
//    private List<ResponseBean> response;
//    private List<?> other;
//
//    public int getCode() {
//        return code;
//    }
//
//    public void setCode(int code) {
//        this.code = code;
//    }
//
//    public String getMsg() {
//        return msg;
//    }
//
//    public void setMsg(String msg) {
//        this.msg = msg;
//    }
//
//    public RequestBean getRequest() {
//        return request;
//    }
//
//    public void setRequest(RequestBean request) {
//        this.request = request;
//    }
//
//    public List<ResponseBean> getResponse() {
//        return response;
//    }
//
//    public void setResponse(List<ResponseBean> response) {
//        this.response = response;
//    }
//
//    public List<?> getOther() {
//        return other;
//    }
//
//    public void setOther(List<?> other) {
//        this.other = other;
//    }
//
//    public static class RequestBean {
//        /**
//         * parkId : 1
//         * type : 2
//         */
//
//        private String parkId;
//        private String type;
//
//        public String getParkId() {
//            return parkId;
//        }
//
//        public void setParkId(String parkId) {
//            this.parkId = parkId;
//        }
//
//        public String getType() {
//            return type;
//        }
//
//        public void setType(String type) {
//            this.type = type;
//        }
//    }
//
//    public static class ResponseBean {
//        /**
//         * order_sn : pk2sn20180818111432
//         * shop_id : 1
//         * money : 0.01
//         * type : 2
//         * shop_name : 彪1123
//         * shop_cost : 10
//         * shop_portrait : park.hostop.net/static/image/0.jpg
//         * goods : [{"num":"3","pic":"0.01","goods_name":"测试","master":"park.hostop.net//upload/20180804/2da84761de43d358bfd2f89627fdaf42.png"},{"num":"2","pic":"10","goods_name":"商品2","master":"park.hostop.net//upload/20180804/963d0fc2a816b184851bb41116478e85.png"}]
//         */
//
//        private String order_sn;
//        private String shop_id;
//        private String money;
//        private String type;
//        private String shop_name;
//        private String shop_cost;
//        private String shop_portrait;
//        private List<GoodsBean> goods;
//
//        public String getOrder_sn() {
//            return order_sn;
//        }
//
//        public void setOrder_sn(String order_sn) {
//            this.order_sn = order_sn;
//        }
//
//        public String getShop_id() {
//            return shop_id;
//        }
//
//        public void setShop_id(String shop_id) {
//            this.shop_id = shop_id;
//        }
//
//        public String getMoney() {
//            return money;
//        }
//
//        public void setMoney(String money) {
//            this.money = money;
//        }
//
//        public String getType() {
//            return type;
//        }
//
//        public void setType(String type) {
//            this.type = type;
//        }
//
//        public String getShop_name() {
//            return shop_name;
//        }
//
//        public void setShop_name(String shop_name) {
//            this.shop_name = shop_name;
//        }
//
//        public String getShop_cost() {
//            return shop_cost;
//        }
//
//        public void setShop_cost(String shop_cost) {
//            this.shop_cost = shop_cost;
//        }
//
//        public String getShop_portrait() {
//            return shop_portrait;
//        }
//
//        public void setShop_portrait(String shop_portrait) {
//            this.shop_portrait = shop_portrait;
//        }
//
//        public List<GoodsBean> getGoods() {
//            return goods;
//        }
//
//        public void setGoods(List<GoodsBean> goods) {
//            this.goods = goods;
//        }
//
//        public static class GoodsBean {
//            /**
//             * num : 3
//             * pic : 0.01
//             * goods_name : 测试
//             * master : park.hostop.net//upload/20180804/2da84761de43d358bfd2f89627fdaf42.png
//             */
//
//            private String num;
//            private String pic;
//            private String goods_name;
//            private String master;
//
//            public String getNum() {
//                return num;
//            }
//
//            public void setNum(String num) {
//                this.num = num;
//            }
//
//            public String getPic() {
//                return pic;
//            }
//
//            public void setPic(String pic) {
//                this.pic = pic;
//            }
//
//            public String getGoods_name() {
//                return goods_name;
//            }
//
//            public void setGoods_name(String goods_name) {
//                this.goods_name = goods_name;
//            }
//
//            public String getMaster() {
//                return master;
//            }
//
//            public void setMaster(String master) {
//                this.master = master;
//            }
//        }
//    }
}
