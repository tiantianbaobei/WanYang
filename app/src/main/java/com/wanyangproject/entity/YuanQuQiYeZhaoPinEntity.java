package com.wanyangproject.entity;

import java.util.List;

/**
 * Created by 甜甜 on 2018/9/21.
 */

public class YuanQuQiYeZhaoPinEntity {

    /**
     * code : 200
     * msg :
     * response : {"data":{"id":"46","title":"测试921","money":"薪资面议","yaoqiu":"测试要求","address":"测试地址","fuli":"测试福利","phone":"测试手机号","content":" 123","seeNums":"216","add_time":"26天前","isLike":"1"},"enterInfo":{"info":{"enterId":"638","enterName":"浦江鸿欣锁厂","parkName":"金华众创城","legalPerson":"张林贤","contact":"13600698117","logoUrl":"http://park.hostop.net/static/image/wanyang.png","intro":"","lat":"","lon":"","desc":"ces&nbsp;","address":"","guimo":"200","hangye":"suochang","lng":""},"recruit":[{"id":"46","title":"测试921","money":"薪资面议","address":"测试地址","entername":"浦江鸿欣锁厂","enterId":"638","parkId":"19","desc":"123..."}]}}
     * request : {"id":"46"}
     * other : []
     */

    private int code;
    private String msg;
    private ResponseBean response;
    private RequestBean request;
    private List<?> other;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public ResponseBean getResponse() {
        return response;
    }

    public void setResponse(ResponseBean response) {
        this.response = response;
    }

    public RequestBean getRequest() {
        return request;
    }

    public void setRequest(RequestBean request) {
        this.request = request;
    }

    public List<?> getOther() {
        return other;
    }

    public void setOther(List<?> other) {
        this.other = other;
    }

    public static class ResponseBean {
        /**
         * data : {"id":"46","title":"测试921","money":"薪资面议","yaoqiu":"测试要求","address":"测试地址","fuli":"测试福利","phone":"测试手机号","content":" 123","seeNums":"216","add_time":"26天前","isLike":"1"}
         * enterInfo : {"info":{"enterId":"638","enterName":"浦江鸿欣锁厂","parkName":"金华众创城","legalPerson":"张林贤","contact":"13600698117","logoUrl":"http://park.hostop.net/static/image/wanyang.png","intro":"","lat":"","lon":"","desc":"ces&nbsp;","address":"","guimo":"200","hangye":"suochang","lng":""},"recruit":[{"id":"46","title":"测试921","money":"薪资面议","address":"测试地址","entername":"浦江鸿欣锁厂","enterId":"638","parkId":"19","desc":"123..."}]}
         */

        private DataBean data;
        private EnterInfoBean enterInfo;

        public DataBean getData() {
            return data;
        }

        public void setData(DataBean data) {
            this.data = data;
        }

        public EnterInfoBean getEnterInfo() {
            return enterInfo;
        }

        public void setEnterInfo(EnterInfoBean enterInfo) {
            this.enterInfo = enterInfo;
        }

        public static class DataBean {
            /**
             * id : 46
             * title : 测试921
             * money : 薪资面议
             * yaoqiu : 测试要求
             * address : 测试地址
             * fuli : 测试福利
             * phone : 测试手机号
             * content :  123
             * seeNums : 216
             * add_time : 26天前
             * isLike : 1
             */

            private String id;
            private String title;
            private String money;
            private String yaoqiu;
            private String address;
            private String fuli;
            private String phone;
            private String content;
            private String seeNums;
            private String add_time;
            private String isLike;

            public String getId() {
                return id;
            }

            public void setId(String id) {
                this.id = id;
            }

            public String getTitle() {
                return title;
            }

            public void setTitle(String title) {
                this.title = title;
            }

            public String getMoney() {
                return money;
            }

            public void setMoney(String money) {
                this.money = money;
            }

            public String getYaoqiu() {
                return yaoqiu;
            }

            public void setYaoqiu(String yaoqiu) {
                this.yaoqiu = yaoqiu;
            }

            public String getAddress() {
                return address;
            }

            public void setAddress(String address) {
                this.address = address;
            }

            public String getFuli() {
                return fuli;
            }

            public void setFuli(String fuli) {
                this.fuli = fuli;
            }

            public String getPhone() {
                return phone;
            }

            public void setPhone(String phone) {
                this.phone = phone;
            }

            public String getContent() {
                return content;
            }

            public void setContent(String content) {
                this.content = content;
            }

            public String getSeeNums() {
                return seeNums;
            }

            public void setSeeNums(String seeNums) {
                this.seeNums = seeNums;
            }

            public String getAdd_time() {
                return add_time;
            }

            public void setAdd_time(String add_time) {
                this.add_time = add_time;
            }

            public String getIsLike() {
                return isLike;
            }

            public void setIsLike(String isLike) {
                this.isLike = isLike;
            }
        }

        public static class EnterInfoBean {
            /**
             * info : {"enterId":"638","enterName":"浦江鸿欣锁厂","parkName":"金华众创城","legalPerson":"张林贤","contact":"13600698117","logoUrl":"http://park.hostop.net/static/image/wanyang.png","intro":"","lat":"","lon":"","desc":"ces&nbsp;","address":"","guimo":"200","hangye":"suochang","lng":""}
             * recruit : [{"id":"46","title":"测试921","money":"薪资面议","address":"测试地址","entername":"浦江鸿欣锁厂","enterId":"638","parkId":"19","desc":"123..."}]
             */

            private InfoBean info;
            private List<RecruitBean> recruit;

            public InfoBean getInfo() {
                return info;
            }

            public void setInfo(InfoBean info) {
                this.info = info;
            }

            public List<RecruitBean> getRecruit() {
                return recruit;
            }

            public void setRecruit(List<RecruitBean> recruit) {
                this.recruit = recruit;
            }

            public static class InfoBean {
                /**
                 * enterId : 638
                 * enterName : 浦江鸿欣锁厂
                 * parkName : 金华众创城
                 * legalPerson : 张林贤
                 * contact : 13600698117
                 * logoUrl : http://park.hostop.net/static/image/wanyang.png
                 * intro :
                 * lat :
                 * lon :
                 * desc : ces&nbsp;
                 * address :
                 * guimo : 200
                 * hangye : suochang
                 * lng :
                 */

                private String enterId;
                private String enterName;
                private String parkName;
                private String legalPerson;
                private String contact;
                private String logoUrl;
                private String intro;
                private String lat;
                private String lon;
                private String desc;
                private String address;
                private String guimo;
                private String hangye;
                private String lng;

                public String getEnterId() {
                    return enterId;
                }

                public void setEnterId(String enterId) {
                    this.enterId = enterId;
                }

                public String getEnterName() {
                    return enterName;
                }

                public void setEnterName(String enterName) {
                    this.enterName = enterName;
                }

                public String getParkName() {
                    return parkName;
                }

                public void setParkName(String parkName) {
                    this.parkName = parkName;
                }

                public String getLegalPerson() {
                    return legalPerson;
                }

                public void setLegalPerson(String legalPerson) {
                    this.legalPerson = legalPerson;
                }

                public String getContact() {
                    return contact;
                }

                public void setContact(String contact) {
                    this.contact = contact;
                }

                public String getLogoUrl() {
                    return logoUrl;
                }

                public void setLogoUrl(String logoUrl) {
                    this.logoUrl = logoUrl;
                }

                public String getIntro() {
                    return intro;
                }

                public void setIntro(String intro) {
                    this.intro = intro;
                }

                public String getLat() {
                    return lat;
                }

                public void setLat(String lat) {
                    this.lat = lat;
                }

                public String getLon() {
                    return lon;
                }

                public void setLon(String lon) {
                    this.lon = lon;
                }

                public String getDesc() {
                    return desc;
                }

                public void setDesc(String desc) {
                    this.desc = desc;
                }

                public String getAddress() {
                    return address;
                }

                public void setAddress(String address) {
                    this.address = address;
                }

                public String getGuimo() {
                    return guimo;
                }

                public void setGuimo(String guimo) {
                    this.guimo = guimo;
                }

                public String getHangye() {
                    return hangye;
                }

                public void setHangye(String hangye) {
                    this.hangye = hangye;
                }

                public String getLng() {
                    return lng;
                }

                public void setLng(String lng) {
                    this.lng = lng;
                }
            }

            public static class RecruitBean {
                /**
                 * id : 46
                 * title : 测试921
                 * money : 薪资面议
                 * address : 测试地址
                 * entername : 浦江鸿欣锁厂
                 * enterId : 638
                 * parkId : 19
                 * desc : 123...
                 */

                private String id;
                private String title;
                private String money;
                private String address;
                private String entername;
                private String enterId;
                private String parkId;
                private String desc;

                public String getId() {
                    return id;
                }

                public void setId(String id) {
                    this.id = id;
                }

                public String getTitle() {
                    return title;
                }

                public void setTitle(String title) {
                    this.title = title;
                }

                public String getMoney() {
                    return money;
                }

                public void setMoney(String money) {
                    this.money = money;
                }

                public String getAddress() {
                    return address;
                }

                public void setAddress(String address) {
                    this.address = address;
                }

                public String getEntername() {
                    return entername;
                }

                public void setEntername(String entername) {
                    this.entername = entername;
                }

                public String getEnterId() {
                    return enterId;
                }

                public void setEnterId(String enterId) {
                    this.enterId = enterId;
                }

                public String getParkId() {
                    return parkId;
                }

                public void setParkId(String parkId) {
                    this.parkId = parkId;
                }

                public String getDesc() {
                    return desc;
                }

                public void setDesc(String desc) {
                    this.desc = desc;
                }
            }
        }
    }

    public static class RequestBean {
        /**
         * id : 46
         */

        private String id;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }
    }
}
