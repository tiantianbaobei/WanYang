package com.wanyangproject.entity;

import com.chad.library.adapter.base.entity.MultiItemEntity;

import java.util.List;

/**
 * Created by 甜甜 on 2018/8/17.
 */

public class ChanYeJinRongEntity {
    /**
     * code : 200
     * msg :
     * response : [{"id":"2","title":"网商银行属于正规银行吗？会不会有风险","desc":"00年前出现的金融传销类骗局\u2014\u2014庞氏骗局，如今是穿了新衣漂洋过海来骗你，教你三招识别骗局不上当。9张图350个名单，让你...","imgArr":["http://park.hostop.net/upload/20180901/afdfa3e27b60a49f64d2f316353e5e38.jpg","http://park.hostop.net/upload/20180901/b90fc628e0f1d27e74a149b0cc61c854.jpg","http://park.hostop.net/upload/20180901/3dd26c6d5a425f806f87b1223d5d70f9.jpg"],"add_time":"15天前"},{"id":"1","title":"茶叶金融的未来发展趋势如何？（揭秘茶圈的骗局）","desc":"一、茶叶金融的定义茶叶金融，是出现于茶业领域的新业态（数年）。其借助机场大师、无良奸商的炒作与投机者的贪婪，通过互联网搭...","imgArr":["http://park.hostop.net/upload/20180901/9616892dad2b115e379abc16b77d7c64.jpg"],"add_time":"15天前"}]
     * request : {"parkId":"1","nTypeId":"2"}
     * other : []
     */

    private int code;
    private String msg;
    private RequestBean request;
    private List<ResponseBean> response;
    private List<?> other;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public RequestBean getRequest() {
        return request;
    }

    public void setRequest(RequestBean request) {
        this.request = request;
    }

    public List<ResponseBean> getResponse() {
        return response;
    }

    public void setResponse(List<ResponseBean> response) {
        this.response = response;
    }

    public List<?> getOther() {
        return other;
    }

    public void setOther(List<?> other) {
        this.other = other;
    }

    public static class RequestBean {
        /**
         * parkId : 1
         * nTypeId : 2
         */

        private String parkId;
        private String nTypeId;

        public String getParkId() {
            return parkId;
        }

        public void setParkId(String parkId) {
            this.parkId = parkId;
        }

        public String getNTypeId() {
            return nTypeId;
        }

        public void setNTypeId(String nTypeId) {
            this.nTypeId = nTypeId;
        }
    }

    public static class ResponseBean implements MultiItemEntity {
        /**
         * id : 2
         * title : 网商银行属于正规银行吗？会不会有风险
         * desc : 00年前出现的金融传销类骗局——庞氏骗局，如今是穿了新衣漂洋过海来骗你，教你三招识别骗局不上当。9张图350个名单，让你...
         * imgArr : ["http://park.hostop.net/upload/20180901/afdfa3e27b60a49f64d2f316353e5e38.jpg","http://park.hostop.net/upload/20180901/b90fc628e0f1d27e74a149b0cc61c854.jpg","http://park.hostop.net/upload/20180901/3dd26c6d5a425f806f87b1223d5d70f9.jpg"]
         * add_time : 15天前
         */

        private String id;
        private String title;
        private String desc;
        private String add_time;
        private List<String> imgArr;









        public static final int LING = 0;
        public static final int ONE = 1;
        public static final int TWO = 2;
        public static final int THREE = 3;
        public static final int SEX = 6;
        public static final int NINE = 9;


        private int itemType;

        public int getItemType() {
            return itemType;
        }

        public void setItemType(int itemType) {
            this.itemType = itemType;
        }

        //





        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getDesc() {
            return desc;
        }

        public void setDesc(String desc) {
            this.desc = desc;
        }

        public String getAdd_time() {
            return add_time;
        }

        public void setAdd_time(String add_time) {
            this.add_time = add_time;
        }

        public List<String> getImgArr() {
            return imgArr;
        }

        public void setImgArr(List<String> imgArr) {
            this.imgArr = imgArr;
        }
    }


//    /**
//     * code : 200
//     * msg :
//     * response : [{"id":"7","title":"123123123123","desc":"基本制造内的内容基本制造内的内容基本制造内的内容基本制造内的内容基本制造内的内容基本制造内的内容...","imgArr":"http://park.hostop.net/upload/20180816/8085d87ae42fa0ec15bc0a8f79ff4484.jpg","add_time":"11小时前"},{"id":"6","title":"高级制造的文章","desc":"基本制造内的内容基本制造内的内容基本制造内的内容基本制造内的内容基本制造内的内容基本制造内的内容...","imgArr":["http://park.hostop.net/upload/20180816/8085d87ae42fa0ec15bc0a8f79ff4484.jpg","http://park.hostop.net/upload/20180816/c3e609af9e6c578b7d9f2a72d0a7f531.jpg","http://park.hostop.net/upload/20180816/8085d87ae42fa0ec15bc0a8f79ff4484.jpg"],"add_time":"15天前"},{"id":"5","title":"基本制造内的内容","desc":"基本制造内的内容基本制造内的内容基本制造内的内容基本制造内的内容基本制造内的内容基本制造内的内容...","imgArr":"http://park.hostop.net/upload/20180816/8085d87ae42fa0ec15bc0a8f79ff4484.jpg","add_time":"16天前"}]
//     * request : {"parkId":"1","nTypeId":"2"}
//     * other : []
//     */
//
//    private int code;
//    private String msg;
//    private RequestBean request;
//    private List<ResponseBean> response;
//    private List<?> other;
//
//    public int getCode() {
//        return code;
//    }
//
//    public void setCode(int code) {
//        this.code = code;
//    }
//
//    public String getMsg() {
//        return msg;
//    }
//
//    public void setMsg(String msg) {
//        this.msg = msg;
//    }
//
//    public RequestBean getRequest() {
//        return request;
//    }
//
//    public void setRequest(RequestBean request) {
//        this.request = request;
//    }
//
//    public List<ResponseBean> getResponse() {
//        return response;
//    }
//
//    public void setResponse(List<ResponseBean> response) {
//        this.response = response;
//    }
//
//    public List<?> getOther() {
//        return other;
//    }
//
//    public void setOther(List<?> other) {
//        this.other = other;
//    }
//
//    public static class RequestBean {
//        /**
//         * parkId : 1
//         * nTypeId : 2
//         */
//
//        private String parkId;
//        private String nTypeId;
//
//        public String getParkId() {
//            return parkId;
//        }
//
//        public void setParkId(String parkId) {
//            this.parkId = parkId;
//        }
//
//        public String getNTypeId() {
//            return nTypeId;
//        }
//
//        public void setNTypeId(String nTypeId) {
//            this.nTypeId = nTypeId;
//        }
//    }
//
//    public static class ResponseBean implements MultiItemEntity {
//        /**
//         * id : 7
//         * title : 123123123123
//         * desc : 基本制造内的内容基本制造内的内容基本制造内的内容基本制造内的内容基本制造内的内容基本制造内的内容...
//         * imgArr : http://park.hostop.net/upload/20180816/8085d87ae42fa0ec15bc0a8f79ff4484.jpg
//         * add_time : 11小时前
//         */
//
//        private String id;
//        private String title;
//        private String desc;
//        private String imgArr;
//        private String add_time;
//
//
//
//        public static final int LING = 0;
//        public static final int ONE = 1;
//        public static final int TWO = 2;
//        public static final int THREE = 3;
//        public static final int SEX = 6;
//        public static final int NINE = 9;
//
//
//        private int itemType;
//
//        public void setItemType(int itemType) {
//            this.itemType = itemType;
//        }
//
//        @Override
//        public int getItemType() {
//            return itemType;
//        }
//
//
//        public String getId() {
//            return id;
//        }
//
//        public void setId(String id) {
//            this.id = id;
//        }
//
//        public String getTitle() {
//            return title;
//        }
//
//        public void setTitle(String title) {
//            this.title = title;
//        }
//
//        public String getDesc() {
//            return desc;
//        }
//
//        public void setDesc(String desc) {
//            this.desc = desc;
//        }
//
//        public String getImgArr() {
//            return imgArr;
//        }
//
//        public void setImgArr(String imgArr) {
//            this.imgArr = imgArr;
//        }
//
//        public String getAdd_time() {
//            return add_time;
//        }
//
//        public void setAdd_time(String add_time) {
//            this.add_time = add_time;
//        }
//    }

}
