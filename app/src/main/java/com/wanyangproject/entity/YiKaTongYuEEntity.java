package com.wanyangproject.entity;

import java.util.List;

/**
 * Created by 甜甜 on 2018/9/8.
 */

public class YiKaTongYuEEntity {
    /**
     * code : 200
     * msg :
     * response : {"parkPersonId":"324","parkPersonName":"王振云","cardNumbers":["1242492294"],"balance":"0"}
     * request : []
     * other : []
     */

    private int code;
    private String msg;
    private ResponseBean response;
    private List<?> request;
    private List<?> other;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public ResponseBean getResponse() {
        return response;
    }

    public void setResponse(ResponseBean response) {
        this.response = response;
    }

    public List<?> getRequest() {
        return request;
    }

    public void setRequest(List<?> request) {
        this.request = request;
    }

    public List<?> getOther() {
        return other;
    }

    public void setOther(List<?> other) {
        this.other = other;
    }

    public static class ResponseBean {
        /**
         * parkPersonId : 324
         * parkPersonName : 王振云
         * cardNumbers : ["1242492294"]
         * balance : 0
         */

        private String parkPersonId;
        private String parkPersonName;
        private String balance;
        private List<String> cardNumbers;

        public String getParkPersonId() {
            return parkPersonId;
        }

        public void setParkPersonId(String parkPersonId) {
            this.parkPersonId = parkPersonId;
        }

        public String getParkPersonName() {
            return parkPersonName;
        }

        public void setParkPersonName(String parkPersonName) {
            this.parkPersonName = parkPersonName;
        }

        public String getBalance() {
            return balance;
        }

        public void setBalance(String balance) {
            this.balance = balance;
        }

        public List<String> getCardNumbers() {
            return cardNumbers;
        }

        public void setCardNumbers(List<String> cardNumbers) {
            this.cardNumbers = cardNumbers;
        }
    }


//
//    /**
//     * code : 200
//     * msg :
//     * response : {"parkPersonId":"","parkPersonName":"","cardNumber":"","balance":"0"}
//     * request : []
//     * other : []
//     */
//
//    private int code;
//    private String msg;
//    private ResponseBean response;
//    private List<?> request;
//    private List<?> other;
//
//    public int getCode() {
//        return code;
//    }
//
//    public void setCode(int code) {
//        this.code = code;
//    }
//
//    public String getMsg() {
//        return msg;
//    }
//
//    public void setMsg(String msg) {
//        this.msg = msg;
//    }
//
//    public ResponseBean getResponse() {
//        return response;
//    }
//
//    public void setResponse(ResponseBean response) {
//        this.response = response;
//    }
//
//    public List<?> getRequest() {
//        return request;
//    }
//
//    public void setRequest(List<?> request) {
//        this.request = request;
//    }
//
//    public List<?> getOther() {
//        return other;
//    }
//
//    public void setOther(List<?> other) {
//        this.other = other;
//    }
//
//    public static class ResponseBean {
//        /**
//         * parkPersonId :
//         * parkPersonName :
//         * cardNumber :
//         * balance : 0
//         */
//
//        private String parkPersonId;
//        private String parkPersonName;
//        private String cardNumber;
//        private String balance;
//
//        public String getParkPersonId() {
//            return parkPersonId;
//        }
//
//        public void setParkPersonId(String parkPersonId) {
//            this.parkPersonId = parkPersonId;
//        }
//
//        public String getParkPersonName() {
//            return parkPersonName;
//        }
//
//        public void setParkPersonName(String parkPersonName) {
//            this.parkPersonName = parkPersonName;
//        }
//
//        public String getCardNumber() {
//            return cardNumber;
//        }
//
//        public void setCardNumber(String cardNumber) {
//            this.cardNumber = cardNumber;
//        }
//
//        public String getBalance() {
//            return balance;
//        }
//
//        public void setBalance(String balance) {
//            this.balance = balance;
//        }
//    }
}
