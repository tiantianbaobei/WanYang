package com.wanyangproject.entity;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by 甜甜 on 2018/10/8.
 */

public class WeiXinBangDingEntity {

    /**
     * code : 200
     * msg :
     * response : {"userid":"2","username":"17600904682","realname":"","avatar":"http://park.hostop.net/upload/20180927/05515109a51791e52393f6e4daa4e5ee.jpg","nickname":"呵呵呵","uuid":"","parkId":"1","parkInfo":{"parkId":"1","parkName":"温州平阳众创城"},"status":"1","typeId":"1","birth":"1379692800","typeId2":"3","token":"dacc80f9a7661ef91eae3a309b8e98ab","new":"0"}
     * request : {"phone":"17600904682","code":"662468","authId":"252","wxkey":"of9-E0g7hKgksAz85N4A_XB3q2z8"}
     * other : []
     */

    private int code;
    private String msg;
    private ResponseBean response;
    private RequestBean request;
    private List<?> other;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public ResponseBean getResponse() {
        return response;
    }

    public void setResponse(ResponseBean response) {
        this.response = response;
    }

    public RequestBean getRequest() {
        return request;
    }

    public void setRequest(RequestBean request) {
        this.request = request;
    }

    public List<?> getOther() {
        return other;
    }

    public void setOther(List<?> other) {
        this.other = other;
    }

    public static class ResponseBean {
        /**
         * userid : 2
         * username : 17600904682
         * realname :
         * avatar : http://park.hostop.net/upload/20180927/05515109a51791e52393f6e4daa4e5ee.jpg
         * nickname : 呵呵呵
         * uuid :
         * parkId : 1
         * parkInfo : {"parkId":"1","parkName":"温州平阳众创城"}
         * status : 1
         * typeId : 1
         * birth : 1379692800
         * typeId2 : 3
         * token : dacc80f9a7661ef91eae3a309b8e98ab
         * new : 0
         */

        private String userid;
        private String username;
        private String realname;
        private String avatar;
        private String nickname;
        private String uuid;
        private String parkId;
        private ParkInfoBean parkInfo;
        private String status;
        private String typeId;
        private String birth;
        private String typeId2;
        private String token;
        @SerializedName("new")
        private String newX;

        public String getUserid() {
            return userid;
        }

        public void setUserid(String userid) {
            this.userid = userid;
        }

        public String getUsername() {
            return username;
        }

        public void setUsername(String username) {
            this.username = username;
        }

        public String getRealname() {
            return realname;
        }

        public void setRealname(String realname) {
            this.realname = realname;
        }

        public String getAvatar() {
            return avatar;
        }

        public void setAvatar(String avatar) {
            this.avatar = avatar;
        }

        public String getNickname() {
            return nickname;
        }

        public void setNickname(String nickname) {
            this.nickname = nickname;
        }

        public String getUuid() {
            return uuid;
        }

        public void setUuid(String uuid) {
            this.uuid = uuid;
        }

        public String getParkId() {
            return parkId;
        }

        public void setParkId(String parkId) {
            this.parkId = parkId;
        }

        public ParkInfoBean getParkInfo() {
            return parkInfo;
        }

        public void setParkInfo(ParkInfoBean parkInfo) {
            this.parkInfo = parkInfo;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getTypeId() {
            return typeId;
        }

        public void setTypeId(String typeId) {
            this.typeId = typeId;
        }

        public String getBirth() {
            return birth;
        }

        public void setBirth(String birth) {
            this.birth = birth;
        }

        public String getTypeId2() {
            return typeId2;
        }

        public void setTypeId2(String typeId2) {
            this.typeId2 = typeId2;
        }

        public String getToken() {
            return token;
        }

        public void setToken(String token) {
            this.token = token;
        }

        public String getNewX() {
            return newX;
        }

        public void setNewX(String newX) {
            this.newX = newX;
        }

        public static class ParkInfoBean {
            /**
             * parkId : 1
             * parkName : 温州平阳众创城
             */

            private String parkId;
            private String parkName;

            public String getParkId() {
                return parkId;
            }

            public void setParkId(String parkId) {
                this.parkId = parkId;
            }

            public String getParkName() {
                return parkName;
            }

            public void setParkName(String parkName) {
                this.parkName = parkName;
            }
        }
    }

    public static class RequestBean {
        /**
         * phone : 17600904682
         * code : 662468
         * authId : 252
         * wxkey : of9-E0g7hKgksAz85N4A_XB3q2z8
         */

        private String phone;
        private String code;
        private String authId;
        private String wxkey;

        public String getPhone() {
            return phone;
        }

        public void setPhone(String phone) {
            this.phone = phone;
        }

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        public String getAuthId() {
            return authId;
        }

        public void setAuthId(String authId) {
            this.authId = authId;
        }

        public String getWxkey() {
            return wxkey;
        }

        public void setWxkey(String wxkey) {
            this.wxkey = wxkey;
        }
    }
}
