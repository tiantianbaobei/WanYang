package com.wanyangproject.entity;

import java.util.List;

/**
 * Created by 甜甜 on 2018/9/2.
 */

public class YouHuiQuanLieBiaoEntity {
    /**
     * code : 200
     * msg :
     * response : {"condition":"优惠券测试","ShopPreferences":[{"title":"国庆福利大放送","introduction":"国庆福利大放送","id":"25"},{"title":"中秋店庆优惠券","introduction":"中秋节超市促销活动方案 - 中秋节超市促销活动方案 ","id":"22"},{"title":"优惠券申请","introduction":"优惠券申请","id":"21"},{"title":"商家优惠活动","introduction":"商家可在平台发布优惠活动","id":"2"}]}
     * request : {"parkId":"1"}
     * other : []
     */

    private int code;
    private String msg;
    private ResponseBean response;
    private RequestBean request;
    private List<?> other;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public ResponseBean getResponse() {
        return response;
    }

    public void setResponse(ResponseBean response) {
        this.response = response;
    }

    public RequestBean getRequest() {
        return request;
    }

    public void setRequest(RequestBean request) {
        this.request = request;
    }

    public List<?> getOther() {
        return other;
    }

    public void setOther(List<?> other) {
        this.other = other;
    }

    public static class ResponseBean {
        /**
         * condition : 优惠券测试
         * ShopPreferences : [{"title":"国庆福利大放送","introduction":"国庆福利大放送","id":"25"},{"title":"中秋店庆优惠券","introduction":"中秋节超市促销活动方案 - 中秋节超市促销活动方案 ","id":"22"},{"title":"优惠券申请","introduction":"优惠券申请","id":"21"},{"title":"商家优惠活动","introduction":"商家可在平台发布优惠活动","id":"2"}]
         */

        private String condition;
        private List<ShopPreferencesBean> ShopPreferences;

        public String getCondition() {
            return condition;
        }

        public void setCondition(String condition) {
            this.condition = condition;
        }

        public List<ShopPreferencesBean> getShopPreferences() {
            return ShopPreferences;
        }

        public void setShopPreferences(List<ShopPreferencesBean> ShopPreferences) {
            this.ShopPreferences = ShopPreferences;
        }

        public static class ShopPreferencesBean {
            /**
             * title : 国庆福利大放送
             * introduction : 国庆福利大放送
             * id : 25
             */

            private String title;
            private String introduction;
            private String id;

            public String getTitle() {
                return title;
            }

            public void setTitle(String title) {
                this.title = title;
            }

            public String getIntroduction() {
                return introduction;
            }

            public void setIntroduction(String introduction) {
                this.introduction = introduction;
            }

            public String getId() {
                return id;
            }

            public void setId(String id) {
                this.id = id;
            }
        }
    }

    public static class RequestBean {
        /**
         * parkId : 1
         */

        private String parkId;

        public String getParkId() {
            return parkId;
        }

        public void setParkId(String parkId) {
            this.parkId = parkId;
        }
    }


//    /**
//     * code : 200
//     * msg :
//     * response : {"ShopPreferences":[{"title":"商家优惠活动","introduction":"商家可在平台发布优惠活动","id":"2"},{"title":"商家优惠活动","introduction":"商家可在平台发布优惠活动","id":"1"}]}
//     * request : {"parkId":"1"}
//     * other : []
//     */
//
//    private int code;
//    private String msg;
//    private ResponseBean response;
//    private RequestBean request;
//    private List<?> other;
//
//    public int getCode() {
//        return code;
//    }
//
//    public void setCode(int code) {
//        this.code = code;
//    }
//
//    public String getMsg() {
//        return msg;
//    }
//
//    public void setMsg(String msg) {
//        this.msg = msg;
//    }
//
//    public ResponseBean getResponse() {
//        return response;
//    }
//
//    public void setResponse(ResponseBean response) {
//        this.response = response;
//    }
//
//    public RequestBean getRequest() {
//        return request;
//    }
//
//    public void setRequest(RequestBean request) {
//        this.request = request;
//    }
//
//    public List<?> getOther() {
//        return other;
//    }
//
//    public void setOther(List<?> other) {
//        this.other = other;
//    }
//
//    public static class ResponseBean {
//        private List<ShopPreferencesBean> ShopPreferences;
//
//        public List<ShopPreferencesBean> getShopPreferences() {
//            return ShopPreferences;
//        }
//
//        public void setShopPreferences(List<ShopPreferencesBean> ShopPreferences) {
//            this.ShopPreferences = ShopPreferences;
//        }
//
//        public static class ShopPreferencesBean {
//            /**
//             * title : 商家优惠活动
//             * introduction : 商家可在平台发布优惠活动
//             * id : 2
//             */
//
//            private String title;
//            private String introduction;
//            private String id;
//
//            public String getTitle() {
//                return title;
//            }
//
//            public void setTitle(String title) {
//                this.title = title;
//            }
//
//            public String getIntroduction() {
//                return introduction;
//            }
//
//            public void setIntroduction(String introduction) {
//                this.introduction = introduction;
//            }
//
//            public String getId() {
//                return id;
//            }
//
//            public void setId(String id) {
//                this.id = id;
//            }
//        }
//    }
//
//    public static class RequestBean {
//        /**
//         * parkId : 1
//         */
//
//        private String parkId;
//
//        public String getParkId() {
//            return parkId;
//        }
//
//        public void setParkId(String parkId) {
//            this.parkId = parkId;
//        }
//    }
}
