package com.wanyangproject.entity;

import java.util.List;

/**
 * Created by 甜甜 on 2018/9/30.
 */

public class WenZhangChanYeJinRongEntity {

    /**
     * code : 200
     * msg :
     * response : {"id":"","title":"","content":"","imgArr":"http://park.hostop.net","add_time":"1970-01-01 08:00:00","isshoucang":"1"}
     * request : {"id":"26","nTypeId":"1"}
     * other : []
     */

    private int code;
    private String msg;
    private ResponseBean response;
    private RequestBean request;
    private List<?> other;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public ResponseBean getResponse() {
        return response;
    }

    public void setResponse(ResponseBean response) {
        this.response = response;
    }

    public RequestBean getRequest() {
        return request;
    }

    public void setRequest(RequestBean request) {
        this.request = request;
    }

    public List<?> getOther() {
        return other;
    }

    public void setOther(List<?> other) {
        this.other = other;
    }

    public static class ResponseBean {
        /**
         * id :
         * title :
         * content :
         * imgArr : http://park.hostop.net
         * add_time : 1970-01-01 08:00:00
         * isshoucang : 1
         */

        private String id;
        private String title;
        private String content;
        private String imgArr;
        private String add_time;
        private String isshoucang;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getContent() {
            return content;
        }

        public void setContent(String content) {
            this.content = content;
        }

        public String getImgArr() {
            return imgArr;
        }

        public void setImgArr(String imgArr) {
            this.imgArr = imgArr;
        }

        public String getAdd_time() {
            return add_time;
        }

        public void setAdd_time(String add_time) {
            this.add_time = add_time;
        }

        public String getIsshoucang() {
            return isshoucang;
        }

        public void setIsshoucang(String isshoucang) {
            this.isshoucang = isshoucang;
        }
    }

    public static class RequestBean {
        /**
         * id : 26
         * nTypeId : 1
         */

        private String id;
        private String nTypeId;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getNTypeId() {
            return nTypeId;
        }

        public void setNTypeId(String nTypeId) {
            this.nTypeId = nTypeId;
        }
    }
}
