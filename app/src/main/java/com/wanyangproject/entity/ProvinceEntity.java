package com.wanyangproject.entity;

/**
 * Created by 甜甜 on 2018/7/19.
 */

public class ProvinceEntity {

    /**
     * id : 1
     * name : 北京
     */

    private int id;
    private String name;
    public ProvinceEntity(String name){
        this.name=name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
