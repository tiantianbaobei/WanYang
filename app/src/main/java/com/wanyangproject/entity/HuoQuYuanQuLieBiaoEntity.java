package com.wanyangproject.entity;

import java.util.List;

/**
 * Created by 甜甜 on 2018/8/6.
 */

public class HuoQuYuanQuLieBiaoEntity {
    /**
     * code : 200
     * msg :
     * response : [{"parkId":"1","parkName":"温州平阳众创城","lat":"117","lng":"36.65","isChecked":"0","userTypeId":"0"},{"parkId":"14","parkName":"龙湾经开小微园","lat":"117","lng":"36.65","isChecked":"0","userTypeId":"0"},{"parkId":"19","parkName":"金华万洋众创城","lat":"117","lng":"36.65","isChecked":"0","userTypeId":"0"}]
     * request : []
     * other : []
     */

    private int code;
    private String msg;
    private List<ResponseBean> response;
    private List<?> request;
    private List<?> other;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public List<ResponseBean> getResponse() {
        return response;
    }

    public void setResponse(List<ResponseBean> response) {
        this.response = response;
    }

    public List<?> getRequest() {
        return request;
    }

    public void setRequest(List<?> request) {
        this.request = request;
    }

    public List<?> getOther() {
        return other;
    }

    public void setOther(List<?> other) {
        this.other = other;
    }

    public static class ResponseBean {
        /**
         * parkId : 1
         * parkName : 温州平阳众创城
         * lat : 117
         * lng : 36.65
         * isChecked : 0
         * userTypeId : 0
         */

        private String parkId;
        private String parkName;
        private String lat;
        private String lng;
        private String isChecked;
        private String userTypeId;

        public String getParkId() {
            return parkId;
        }

        public void setParkId(String parkId) {
            this.parkId = parkId;
        }

        public String getParkName() {
            return parkName;
        }

        public void setParkName(String parkName) {
            this.parkName = parkName;
        }

        public String getLat() {
            return lat;
        }

        public void setLat(String lat) {
            this.lat = lat;
        }

        public String getLng() {
            return lng;
        }

        public void setLng(String lng) {
            this.lng = lng;
        }

        public String getIsChecked() {
            return isChecked;
        }

        public void setIsChecked(String isChecked) {
            this.isChecked = isChecked;
        }

        public String getUserTypeId() {
            return userTypeId;
        }

        public void setUserTypeId(String userTypeId) {
            this.userTypeId = userTypeId;
        }
    }


//    /**
//     * code : 200
//     * msg : [{"parkId":1,"parkName":"温州平阳众创城","lat":117,"lng":36.65,"isChecked":0},{"parkId":14,"parkName":"龙湾经开小微园","lat":117,"lng":36.65,"isChecked":0},{"parkId":19,"parkName":"金华万洋众创城","lat":117,"lng":36.65,"isChecked":0}]
//     * response : []
//     * request : []
//     * other : []
//     */
//
//    private int code;
//    private List<MsgBean> msg;
//    public static List<MsgBean> mmm;
//    private List<?> response;
//    private List<?> request;
//    private List<?> other;
//
//
//
//    public int getCode() {
//        return code;
//    }
//
//    public void setCode(int code) {
//        this.code = code;
//    }
//
//    public List<MsgBean> getMsg() {
//        return msg;
//    }
//
//    public void setMsg(List<MsgBean> msg) {
//        this.msg = msg;
//    }
//
//    public List<?> getResponse() {
//        return response;
//    }
//
//    public void setResponse(List<?> response) {
//        this.response = response;
//    }
//
//    public List<?> getRequest() {
//        return request;
//    }
//
//    public void setRequest(List<?> request) {
//        this.request = request;
//    }
//
//    public List<?> getOther() {
//        return other;
//    }
//
//    public void setOther(List<?> other) {
//        this.other = other;
//    }
//
//    public static class MsgBean {
//        /**
//         * parkId : 1
//         * parkName : 温州平阳众创城
//         * lat : 117
//         * lng : 36.65
//         * isChecked : 0
//         */
//
//        private String parkId;
//        private String parkName;
//        private int lat;
//        private double lng;
//        private int isChecked;
//
//        public String getParkId() {
//            return parkId;
//        }
//
//        public void setParkId(String parkId) {
//            this.parkId = parkId;
//        }
//
//        public String getParkName() {
//            return parkName;
//        }
//
//        public void setParkName(String parkName) {
//            this.parkName = parkName;
//        }
//
//        public int getLat() {
//            return lat;
//        }
//
//        public void setLat(int lat) {
//            this.lat = lat;
//        }
//
//        public double getLng() {
//            return lng;
//        }
//
//        public void setLng(double lng) {
//            this.lng = lng;
//        }
//
//        public int getIsChecked() {
//            return isChecked;
//        }
//
//        public void setIsChecked(int isChecked) {
//            this.isChecked = isChecked;
//        }
//    }
}
