package com.wanyangproject.entity;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by 甜甜 on 2018/9/16.
 */

public class YiKaTongZhiFuEntity {

    /**
     * code : 200
     * msg :
     * response : {"prepayid":"wx16150226532859319cfa256f3711727191","appid":"wx363ae6178db5e958","partnerid":"1511703941","package":"Sign=WXPay","noncestr":"AbkwIOzrQcPLjJ4xN2TUgMp0sd1E9Y87","timestamp":"1537081346","sign":"F9BE3355D7EBFFA1F830B5BFB4B75FC2","type":"2"}
     * request : {"parkId":"1","type":"2","state":"1","money":"50"}
     * other : []
     */

    private int code;
    private String msg;
    private ResponseBean response;
    private RequestBean request;
    private List<?> other;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public ResponseBean getResponse() {
        return response;
    }

    public void setResponse(ResponseBean response) {
        this.response = response;
    }

    public RequestBean getRequest() {
        return request;
    }

    public void setRequest(RequestBean request) {
        this.request = request;
    }

    public List<?> getOther() {
        return other;
    }

    public void setOther(List<?> other) {
        this.other = other;
    }

    public static class ResponseBean {
        /**
         * prepayid : wx16150226532859319cfa256f3711727191
         * appid : wx363ae6178db5e958
         * partnerid : 1511703941
         * package : Sign=WXPay
         * noncestr : AbkwIOzrQcPLjJ4xN2TUgMp0sd1E9Y87
         * timestamp : 1537081346
         * sign : F9BE3355D7EBFFA1F830B5BFB4B75FC2
         * type : 2
         */

        private String prepayid;
        private String appid;
        private String partnerid;
        @SerializedName("package")
        private String packageX;
        private String noncestr;
        private String timestamp;
        private String sign;
        private String type;

        public String getPrepayid() {
            return prepayid;
        }

        public void setPrepayid(String prepayid) {
            this.prepayid = prepayid;
        }

        public String getAppid() {
            return appid;
        }

        public void setAppid(String appid) {
            this.appid = appid;
        }

        public String getPartnerid() {
            return partnerid;
        }

        public void setPartnerid(String partnerid) {
            this.partnerid = partnerid;
        }

        public String getPackageX() {
            return packageX;
        }

        public void setPackageX(String packageX) {
            this.packageX = packageX;
        }

        public String getNoncestr() {
            return noncestr;
        }

        public void setNoncestr(String noncestr) {
            this.noncestr = noncestr;
        }

        public String getTimestamp() {
            return timestamp;
        }

        public void setTimestamp(String timestamp) {
            this.timestamp = timestamp;
        }

        public String getSign() {
            return sign;
        }

        public void setSign(String sign) {
            this.sign = sign;
        }

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }
    }

    public static class RequestBean {
        /**
         * parkId : 1
         * type : 2
         * state : 1
         * money : 50
         */

        private String parkId;
        private String type;
        private String state;
        private String money;

        public String getParkId() {
            return parkId;
        }

        public void setParkId(String parkId) {
            this.parkId = parkId;
        }

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public String getState() {
            return state;
        }

        public void setState(String state) {
            this.state = state;
        }

        public String getMoney() {
            return money;
        }

        public void setMoney(String money) {
            this.money = money;
        }
    }
}
