package com.wanyangproject.entity;

import java.util.List;

/**
 * Created by 甜甜 on 2018/8/16.
 */

public class ShouYeTuiJianQiYeEntity {

    /**
     * code : 200
     * msg :
     * response : [{"enterId":"44","parkId":"1","enterName":"温州嘉创印务有限公司"},{"enterId":"43","parkId":"1","enterName":"温州瑞伟电机有限公司"}]
     * request : {"parkId":"1"}
     * other : []
     */

    private int code;
    private String msg;
    private RequestBean request;
    private List<ResponseBean> response;
    private List<?> other;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public RequestBean getRequest() {
        return request;
    }

    public void setRequest(RequestBean request) {
        this.request = request;
    }

    public List<ResponseBean> getResponse() {
        return response;
    }

    public void setResponse(List<ResponseBean> response) {
        this.response = response;
    }

    public List<?> getOther() {
        return other;
    }

    public void setOther(List<?> other) {
        this.other = other;
    }

    public static class RequestBean {
        /**
         * parkId : 1
         */

        private String parkId;

        public String getParkId() {
            return parkId;
        }

        public void setParkId(String parkId) {
            this.parkId = parkId;
        }
    }

    public static class ResponseBean {
        /**
         * enterId : 44
         * parkId : 1
         * enterName : 温州嘉创印务有限公司
         */

        private String enterId;
        private String parkId;
        private String enterName;

        public String getEnterId() {
            return enterId;
        }

        public void setEnterId(String enterId) {
            this.enterId = enterId;
        }

        public String getParkId() {
            return parkId;
        }

        public void setParkId(String parkId) {
            this.parkId = parkId;
        }

        public String getEnterName() {
            return enterName;
        }

        public void setEnterName(String enterName) {
            this.enterName = enterName;
        }
    }
}
