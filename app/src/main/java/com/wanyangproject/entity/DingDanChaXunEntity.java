package com.wanyangproject.entity;

import java.util.List;

/**
 * Created by 甜甜 on 2018/8/24.
 */

public class DingDanChaXunEntity {

    /**
     * code : 200
     * msg :
     * response : {"zong":"0","shu":"0","pei":"0","pay":"0","wei":"0"}
     * request : {"parkId":"1","phone":"17600904682"}
     * other : []
     */

    private int code;
    private String msg;
    private ResponseBean response;
    private RequestBean request;
    private List<?> other;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public ResponseBean getResponse() {
        return response;
    }

    public void setResponse(ResponseBean response) {
        this.response = response;
    }

    public RequestBean getRequest() {
        return request;
    }

    public void setRequest(RequestBean request) {
        this.request = request;
    }

    public List<?> getOther() {
        return other;
    }

    public void setOther(List<?> other) {
        this.other = other;
    }

    public static class ResponseBean {
        /**
         * zong : 0
         * shu : 0
         * pei : 0
         * pay : 0
         * wei : 0
         */

        private String zong;
        private String shu;
        private String pei;
        private String pay;
        private String wei;

        public String getZong() {
            return zong;
        }

        public void setZong(String zong) {
            this.zong = zong;
        }

        public String getShu() {
            return shu;
        }

        public void setShu(String shu) {
            this.shu = shu;
        }

        public String getPei() {
            return pei;
        }

        public void setPei(String pei) {
            this.pei = pei;
        }

        public String getPay() {
            return pay;
        }

        public void setPay(String pay) {
            this.pay = pay;
        }

        public String getWei() {
            return wei;
        }

        public void setWei(String wei) {
            this.wei = wei;
        }
    }

    public static class RequestBean {
        /**
         * parkId : 1
         * phone : 17600904682
         */

        private String parkId;
        private String phone;

        public String getParkId() {
            return parkId;
        }

        public void setParkId(String parkId) {
            this.parkId = parkId;
        }

        public String getPhone() {
            return phone;
        }

        public void setPhone(String phone) {
            this.phone = phone;
        }
    }
}
