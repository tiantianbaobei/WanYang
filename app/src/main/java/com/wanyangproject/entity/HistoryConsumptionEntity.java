package com.wanyangproject.entity;

import java.util.List;

public class HistoryConsumptionEntity {

    private int code;
    private String msg;
    private Response response;
    private Request request;
    private List<?> other;
    public void setCode(int code) {
        this.code = code;
    }
    public int getCode() {
        return code;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }
    public String getMsg() {
        return msg;
    }

    public void setResponse(Response response) {
        this.response = response;
    }
    public Response getResponse() {
        return response;
    }

    public void setRequest(Request request) {
        this.request = request;
    }
    public Request getRequest() {
        return request;
    }

    public void setOther(List<?> other) {
        this.other = other;
    }
    public List<?> getOther() {
        return other;
    }

    public static class Response {

        private String code;
        private String message;
        private Data data;
        public void setCode(String code) {
            this.code = code;
        }
        public String getCode() {
            return code;
        }

        public void setMessage(String message) {
            this.message = message;
        }
        public String getMessage() {
            return message;
        }

        public void setData(Data data) {
            this.data = data;
        }
        public Data getData() {
            return data;
        }

        public static class Data {
            private String total;
            private String pageSize;
            private List<Consumption> list;
            public void setTotal(String total) {
                this.total = total;
            }
            public String getTotal() {
                return total;
            }

            public void setPageSize(String pageSize) {
                this.pageSize = pageSize;
            }
            public String getPageSize() {
                return pageSize;
            }

            public void setList(List<Consumption> list) {
                this.list = list;
            }
            public List<Consumption> getList() {
                return list;
            }


            public static class Consumption {

                private String month;
                private String cost;
                public void setMonth(String month) {
                    this.month = month;
                }
                public String getMonth() {
                    return month;
                }

                public void setCost(String cost) {
                    this.cost = cost;
                }
                public String getCost() {
                    return cost;
                }

            }

        }
    }

    /**
     * Copyright 2018 bejson.com
     */

    public static class Request {
        private String parkId;
        private String type;
        private String electricityUserId;
        public void setParkId(String parkId) {
            this.parkId = parkId;
        }
        public String getParkId() {
            return parkId;
        }

        public void setType(String type) {
            this.type = type;
        }
        public String getType() {
            return type;
        }

        public void setElectricityUserId(String electricityUserId) {
            this.electricityUserId = electricityUserId;
        }
        public String getElectricityUserId() {
            return electricityUserId;
        }

    }

}
