package com.wanyangproject.entity;

import java.util.List;

/**
 * Created by 甜甜 on 2018/9/9.
 */

public class ZhengCeFaGuiXiangQingEntity {
    /**
     * code : 200
     * msg :
     * response : {"id":"8","title":"biaotibiaoti","content":"<a href=\"wanyang=园区生活\" target=\"_self\">wanyang=园区生活<\/a>","imgArr":["http://park.hostop.net/upload/20180905/df3a0b76eda3204aba048dd1295b5413.jpg"],"add_time":"12天前"}
     * request : {"id":"8"}
     * other : []
     */

    private int code;
    private String msg;
    private ResponseBean response;
    private RequestBean request;
    private List<?> other;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public ResponseBean getResponse() {
        return response;
    }

    public void setResponse(ResponseBean response) {
        this.response = response;
    }

    public RequestBean getRequest() {
        return request;
    }

    public void setRequest(RequestBean request) {
        this.request = request;
    }

    public List<?> getOther() {
        return other;
    }

    public void setOther(List<?> other) {
        this.other = other;
    }

    public static class ResponseBean {
        /**
         * id : 8
         * title : biaotibiaoti
         * content : <a href="wanyang=园区生活" target="_self">wanyang=园区生活</a>
         * imgArr : ["http://park.hostop.net/upload/20180905/df3a0b76eda3204aba048dd1295b5413.jpg"]
         * add_time : 12天前
         */

        private String id;
        private String title;
        private String content;
        private String add_time;
        private List<String> imgArr;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getContent() {
            return content;
        }

        public void setContent(String content) {
            this.content = content;
        }

        public String getAdd_time() {
            return add_time;
        }

        public void setAdd_time(String add_time) {
            this.add_time = add_time;
        }

        public List<String> getImgArr() {
            return imgArr;
        }

        public void setImgArr(List<String> imgArr) {
            this.imgArr = imgArr;
        }
    }

    public static class RequestBean {
        /**
         * id : 8
         */

        private String id;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }
    }


//
//    /**
//     * code : 200
//     * msg :
//     * response : {"id":"","title":"","content":"","imgArr":"http://park.hostop.net","add_time":"1970-01-01 08:00:00"}
//     * request : {"id":"9"}
//     * other : []
//     */
//
//    private int code;
//    private String msg;
//    private ResponseBean response;
//    private RequestBean request;
//    private List<?> other;
//
//    public int getCode() {
//        return code;
//    }
//
//    public void setCode(int code) {
//        this.code = code;
//    }
//
//    public String getMsg() {
//        return msg;
//    }
//
//    public void setMsg(String msg) {
//        this.msg = msg;
//    }
//
//    public ResponseBean getResponse() {
//        return response;
//    }
//
//    public void setResponse(ResponseBean response) {
//        this.response = response;
//    }
//
//    public RequestBean getRequest() {
//        return request;
//    }
//
//    public void setRequest(RequestBean request) {
//        this.request = request;
//    }
//
//    public List<?> getOther() {
//        return other;
//    }
//
//    public void setOther(List<?> other) {
//        this.other = other;
//    }
//
//    public static class ResponseBean {
//        /**
//         * id :
//         * title :
//         * content :
//         * imgArr : http://park.hostop.net
//         * add_time : 1970-01-01 08:00:00
//         */
//
//        private String id;
//        private String title;
//        private String content;
//        private String imgArr;
//        private String add_time;
//
//        public String getId() {
//            return id;
//        }
//
//        public void setId(String id) {
//            this.id = id;
//        }
//
//        public String getTitle() {
//            return title;
//        }
//
//        public void setTitle(String title) {
//            this.title = title;
//        }
//
//        public String getContent() {
//            return content;
//        }
//
//        public void setContent(String content) {
//            this.content = content;
//        }
//
//        public String getImgArr() {
//            return imgArr;
//        }
//
//        public void setImgArr(String imgArr) {
//            this.imgArr = imgArr;
//        }
//
//        public String getAdd_time() {
//            return add_time;
//        }
//
//        public void setAdd_time(String add_time) {
//            this.add_time = add_time;
//        }
//    }
//
//    public static class RequestBean {
//        /**
//         * id : 9
//         */
//
//        private String id;
//
//        public String getId() {
//            return id;
//        }
//
//        public void setId(String id) {
//            this.id = id;
//        }
//    }
}
