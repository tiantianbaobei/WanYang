package com.wanyangproject.entity;

import java.util.List;

/**
 * Created by 甜甜 on 2018/8/26.
 */

public class ShouCangEntity {
    /**
     * code : 200
     * msg :
     * response : {"mes":"取消收藏成功！"}
     * request : {"parkId":"1","type":"1","id":"1"}
     * other : []
     */

    private int code;
    private String msg;
    private ResponseBean response;
    private RequestBean request;
    private List<?> other;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public ResponseBean getResponse() {
        return response;
    }

    public void setResponse(ResponseBean response) {
        this.response = response;
    }

    public RequestBean getRequest() {
        return request;
    }

    public void setRequest(RequestBean request) {
        this.request = request;
    }

    public List<?> getOther() {
        return other;
    }

    public void setOther(List<?> other) {
        this.other = other;
    }

    public static class ResponseBean {
        /**
         * mes : 取消收藏成功！
         */

        private String mes;

        public String getMes() {
            return mes;
        }

        public void setMes(String mes) {
            this.mes = mes;
        }
    }

    public static class RequestBean {
        /**
         * parkId : 1
         * type : 1
         * id : 1
         */

        private String parkId;
        private String type;
        private String id;

        public String getParkId() {
            return parkId;
        }

        public void setParkId(String parkId) {
            this.parkId = parkId;
        }

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }
    }


    //    /**
//     * code : 200
//     * msg :
//     * response : {"list":[{"id":"3","Distribution":"5","service":"5","type":"2","goods_id":"","user_id":"4","shop_id":"1","content":"案例","order_sn":"pk4sn20180820170936","add_time":"2018-08-11 12:50:56"}],"shu":"1","ping":"5.0 "}
//     * request : {"parkId":"1","phone":"15822075533"}
//     * other : []
//     */
//
//    private int code;
//    private String msg;
//    private ResponseBean response;
//    private RequestBean request;
//    private List<?> other;
//
//    public int getCode() {
//        return code;
//    }
//
//    public void setCode(int code) {
//        this.code = code;
//    }
//
//    public String getMsg() {
//        return msg;
//    }
//
//    public void setMsg(String msg) {
//        this.msg = msg;
//    }
//
//    public ResponseBean getResponse() {
//        return response;
//    }
//
//    public void setResponse(ResponseBean response) {
//        this.response = response;
//    }
//
//    public RequestBean getRequest() {
//        return request;
//    }
//
//    public void setRequest(RequestBean request) {
//        this.request = request;
//    }
//
//    public List<?> getOther() {
//        return other;
//    }
//
//    public void setOther(List<?> other) {
//        this.other = other;
//    }
//
//    public static class ResponseBean {
//        /**
//         * list : [{"id":"3","Distribution":"5","service":"5","type":"2","goods_id":"","user_id":"4","shop_id":"1","content":"案例","order_sn":"pk4sn20180820170936","add_time":"2018-08-11 12:50:56"}]
//         * shu : 1
//         * ping : 5.0
//         */
//
//        private String shu;
//        private String ping;
//        private List<ListBean> list;
//
//        public String getShu() {
//            return shu;
//        }
//
//        public void setShu(String shu) {
//            this.shu = shu;
//        }
//
//        public String getPing() {
//            return ping;
//        }
//
//        public void setPing(String ping) {
//            this.ping = ping;
//        }
//
//        public List<ListBean> getList() {
//            return list;
//        }
//
//        public void setList(List<ListBean> list) {
//            this.list = list;
//        }
//
//        public static class ListBean {
//            /**
//             * id : 3
//             * Distribution : 5
//             * service : 5
//             * type : 2
//             * goods_id :
//             * user_id : 4
//             * shop_id : 1
//             * content : 案例
//             * order_sn : pk4sn20180820170936
//             * add_time : 2018-08-11 12:50:56
//             */
//
//            private String id;
//            private String Distribution;
//            private String service;
//            private String type;
//            private String goods_id;
//            private String user_id;
//            private String shop_id;
//            private String content;
//            private String order_sn;
//            private String add_time;
//
//            public String getId() {
//                return id;
//            }
//
//            public void setId(String id) {
//                this.id = id;
//            }
//
//            public String getDistribution() {
//                return Distribution;
//            }
//
//            public void setDistribution(String Distribution) {
//                this.Distribution = Distribution;
//            }
//
//            public String getService() {
//                return service;
//            }
//
//            public void setService(String service) {
//                this.service = service;
//            }
//
//            public String getType() {
//                return type;
//            }
//
//            public void setType(String type) {
//                this.type = type;
//            }
//
//            public String getGoods_id() {
//                return goods_id;
//            }
//
//            public void setGoods_id(String goods_id) {
//                this.goods_id = goods_id;
//            }
//
//            public String getUser_id() {
//                return user_id;
//            }
//
//            public void setUser_id(String user_id) {
//                this.user_id = user_id;
//            }
//
//            public String getShop_id() {
//                return shop_id;
//            }
//
//            public void setShop_id(String shop_id) {
//                this.shop_id = shop_id;
//            }
//
//            public String getContent() {
//                return content;
//            }
//
//            public void setContent(String content) {
//                this.content = content;
//            }
//
//            public String getOrder_sn() {
//                return order_sn;
//            }
//
//            public void setOrder_sn(String order_sn) {
//                this.order_sn = order_sn;
//            }
//
//            public String getAdd_time() {
//                return add_time;
//            }
//
//            public void setAdd_time(String add_time) {
//                this.add_time = add_time;
//            }
//        }
//    }
//
//    public static class RequestBean {
//        /**
//         * parkId : 1
//         * phone : 15822075533
//         */
//
//        private String parkId;
//        private String phone;
//
//        public String getParkId() {
//            return parkId;
//        }
//
//        public void setParkId(String parkId) {
//            this.parkId = parkId;
//        }
//
//        public String getPhone() {
//            return phone;
//        }
//
//        public void setPhone(String phone) {
//            this.phone = phone;
//        }
//    }
}
