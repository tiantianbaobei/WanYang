package com.wanyangproject.entity;

import java.util.List;

/**
 * Created by 甜甜 on 2018/8/29.
 */

public class BuChongEntity {

    /**
     * code : 200
     * msg :
     * response : []
     * request : {"fuwuId":"12","content":"测试"}
     * other : []
     */

    private int code;
    private String msg;
    private RequestBean request;
    private List<?> response;
    private List<?> other;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public RequestBean getRequest() {
        return request;
    }

    public void setRequest(RequestBean request) {
        this.request = request;
    }

    public List<?> getResponse() {
        return response;
    }

    public void setResponse(List<?> response) {
        this.response = response;
    }

    public List<?> getOther() {
        return other;
    }

    public void setOther(List<?> other) {
        this.other = other;
    }

    public static class RequestBean {
        /**
         * fuwuId : 12
         * content : 测试
         */

        private String fuwuId;
        private String content;

        public String getFuwuId() {
            return fuwuId;
        }

        public void setFuwuId(String fuwuId) {
            this.fuwuId = fuwuId;
        }

        public String getContent() {
            return content;
        }

        public void setContent(String content) {
            this.content = content;
        }
    }
}
