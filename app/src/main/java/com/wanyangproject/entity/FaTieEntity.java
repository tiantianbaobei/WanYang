package com.wanyangproject.entity;

import java.util.List;

/**
 * Created by 甜甜 on 2018/8/10.
 */

public class FaTieEntity {
    /**
     * code : 200
     * msg : 发表成功
     * response : {"id":"50"}
     * request : {"parkId":"14","typeId":"9","content":"哈哈","imgArr":"http://park.hostop.net/upload/20180831/489eb68f32c89a0d4b0914cee37c7905.jpg"}
     * other : []
     */

    private int code;
    private String msg;
    private ResponseBean response;
    private RequestBean request;
    private List<?> other;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public ResponseBean getResponse() {
        return response;
    }

    public void setResponse(ResponseBean response) {
        this.response = response;
    }

    public RequestBean getRequest() {
        return request;
    }

    public void setRequest(RequestBean request) {
        this.request = request;
    }

    public List<?> getOther() {
        return other;
    }

    public void setOther(List<?> other) {
        this.other = other;
    }

    public static class ResponseBean {
        /**
         * id : 50
         */

        private String id;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }
    }

    public static class RequestBean {
        /**
         * parkId : 14
         * typeId : 9
         * content : 哈哈
         * imgArr : http://park.hostop.net/upload/20180831/489eb68f32c89a0d4b0914cee37c7905.jpg
         */

        private String parkId;
        private String typeId;
        private String content;
        private String imgArr;

        public String getParkId() {
            return parkId;
        }

        public void setParkId(String parkId) {
            this.parkId = parkId;
        }

        public String getTypeId() {
            return typeId;
        }

        public void setTypeId(String typeId) {
            this.typeId = typeId;
        }

        public String getContent() {
            return content;
        }

        public void setContent(String content) {
            this.content = content;
        }

        public String getImgArr() {
            return imgArr;
        }

        public void setImgArr(String imgArr) {
            this.imgArr = imgArr;
        }
    }


//    /**
//     * code : 200
//     * msg : 发表成功
//     * response : []
//     * request : {"parkId":"1","typeId":"0","content":"今天天气不错啊"}
//     * other : []
//     */
//
//    private int code;
//    private String msg;
//    private RequestBean request;
//    private List<?> response;
//    private List<?> other;
//
//    public int getCode() {
//        return code;
//    }
//
//    public void setCode(int code) {
//        this.code = code;
//    }
//
//    public String getMsg() {
//        return msg;
//    }
//
//    public void setMsg(String msg) {
//        this.msg = msg;
//    }
//
//    public RequestBean getRequest() {
//        return request;
//    }
//
//    public void setRequest(RequestBean request) {
//        this.request = request;
//    }
//
//    public List<?> getResponse() {
//        return response;
//    }
//
//    public void setResponse(List<?> response) {
//        this.response = response;
//    }
//
//    public List<?> getOther() {
//        return other;
//    }
//
//    public void setOther(List<?> other) {
//        this.other = other;
//    }
//
//    public static class RequestBean {
//        /**
//         * parkId : 1
//         * typeId : 0
//         * content : 今天天气不错啊
//         */
//
//        private String parkId;
//        private String typeId;
//        private String content;
//
//        public String getParkId() {
//            return parkId;
//        }
//
//        public void setParkId(String parkId) {
//            this.parkId = parkId;
//        }
//
//        public String getTypeId() {
//            return typeId;
//        }
//
//        public void setTypeId(String typeId) {
//            this.typeId = typeId;
//        }
//
//        public String getContent() {
//            return content;
//        }
//
//        public void setContent(String content) {
//            this.content = content;
//        }
//    }
}
