package com.wanyangproject.entity;

import java.util.List;

/**
 * Created by 甜甜 on 2018/9/23.
 */

public class TiJiaoDingDanXiangQingEntity {

    /**
     * code : 200
     * msg :
     * response : {"order_sn":"pk3sn20180923152212"}
     * request : {"goods":"[{\"id\":\"30\",\"num\":\"1\"}]","money":"0.01","parkId":"1","shopId":"4","adders_id":"29","Remarks":""}
     * other : []
     */

    private int code;
    private String msg;
    private ResponseBean response;
    private RequestBean request;
    private List<?> other;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public ResponseBean getResponse() {
        return response;
    }

    public void setResponse(ResponseBean response) {
        this.response = response;
    }

    public RequestBean getRequest() {
        return request;
    }

    public void setRequest(RequestBean request) {
        this.request = request;
    }

    public List<?> getOther() {
        return other;
    }

    public void setOther(List<?> other) {
        this.other = other;
    }

    public static class ResponseBean {
        /**
         * order_sn : pk3sn20180923152212
         */

        private String order_sn;

        public String getOrder_sn() {
            return order_sn;
        }

        public void setOrder_sn(String order_sn) {
            this.order_sn = order_sn;
        }
    }

    public static class RequestBean {
        /**
         * goods : [{"id":"30","num":"1"}]
         * money : 0.01
         * parkId : 1
         * shopId : 4
         * adders_id : 29
         * Remarks :
         */

        private String goods;
        private String money;
        private String parkId;
        private String shopId;
        private String adders_id;
        private String Remarks;

        public String getGoods() {
            return goods;
        }

        public void setGoods(String goods) {
            this.goods = goods;
        }

        public String getMoney() {
            return money;
        }

        public void setMoney(String money) {
            this.money = money;
        }

        public String getParkId() {
            return parkId;
        }

        public void setParkId(String parkId) {
            this.parkId = parkId;
        }

        public String getShopId() {
            return shopId;
        }

        public void setShopId(String shopId) {
            this.shopId = shopId;
        }

        public String getAdders_id() {
            return adders_id;
        }

        public void setAdders_id(String adders_id) {
            this.adders_id = adders_id;
        }

        public String getRemarks() {
            return Remarks;
        }

        public void setRemarks(String Remarks) {
            this.Remarks = Remarks;
        }
    }
}
