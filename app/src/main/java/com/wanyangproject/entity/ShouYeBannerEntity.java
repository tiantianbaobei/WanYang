package com.wanyangproject.entity;

import java.util.List;

/**
 * Created by 甜甜 on 2018/8/26.
 */

public class ShouYeBannerEntity {
    /**
     * code : 200
     * msg :
     * response : [{"id":"10","title":"","image":"http://park.hostop.net/upload/20180908/9a4f3b4ec1850623194421a27dc30328.jpg","add_time":"2天前","hrefType":"3","content":""},{"id":"8","title":"","image":"http://park.hostop.net/upload/20180906/1f5b2c9ae44040f2c49a083fd32a517f.jpg","add_time":"3天前","hrefType":"0","content":""},{"id":"2","title":"","image":"http://park.hostop.net/upload/20180906/4d3a4db8e349a4ec7a096ade2d2aea81.jpg","add_time":"3天前","hrefType":"0","content":""},{"id":"1","title":"","image":"http://park.hostop.net/upload/20180906/3c20de4965161f35609ccdaf1fbee65f.jpg","add_time":"3天前","hrefType":"0","content":""}]
     * request : {"parkId":"1"}
     * other : []
     */

    private int code;
    private String msg;
    private RequestBean request;
    private List<ResponseBean> response;
    private List<?> other;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public RequestBean getRequest() {
        return request;
    }

    public void setRequest(RequestBean request) {
        this.request = request;
    }

    public List<ResponseBean> getResponse() {
        return response;
    }

    public void setResponse(List<ResponseBean> response) {
        this.response = response;
    }

    public List<?> getOther() {
        return other;
    }

    public void setOther(List<?> other) {
        this.other = other;
    }

    public static class RequestBean {
        /**
         * parkId : 1
         */

        private String parkId;

        public String getParkId() {
            return parkId;
        }

        public void setParkId(String parkId) {
            this.parkId = parkId;
        }
    }

    public static class ResponseBean {
        /**
         * id : 10
         * title :
         * image : http://park.hostop.net/upload/20180908/9a4f3b4ec1850623194421a27dc30328.jpg
         * add_time : 2天前
         * hrefType : 3
         * content :
         */

        private String id;
        private String title;
        private String image;
        private String add_time;
        private String hrefType;
        private String content;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getImage() {
            return image;
        }

        public void setImage(String image) {
            this.image = image;
        }

        public String getAdd_time() {
            return add_time;
        }

        public void setAdd_time(String add_time) {
            this.add_time = add_time;
        }

        public String getHrefType() {
            return hrefType;
        }

        public void setHrefType(String hrefType) {
            this.hrefType = hrefType;
        }

        public String getContent() {
            return content;
        }

        public void setContent(String content) {
            this.content = content;
        }
    }


//
//    /**
//     * code : 200
//     * msg :
//     * response : [{"id":"2","title":"在党的旗帜下奋进强军新时代","image":"http://park.hostop.net/upload/20180816/47c1d7310b9bff1194b85a614b58cc7d.jpg","add_time":"2天前","typeId":"1"},{"id":"1","title":"最新 今天晚上出现50年难遇流星雨","image":"http://park.hostop.net/upload/20180816/8085d87ae42fa0ec15bc0a8f79ff4484.jpg","add_time":"10天前","typeId":"1"}]
//     * request : {"parkId":"1"}
//     * other : []
//     */
//
//    private int code;
//    private String msg;
//    private RequestBean request;
//    private List<ResponseBean> response;
//    private List<?> other;
//
//    public int getCode() {
//        return code;
//    }
//
//    public void setCode(int code) {
//        this.code = code;
//    }
//
//    public String getMsg() {
//        return msg;
//    }
//
//    public void setMsg(String msg) {
//        this.msg = msg;
//    }
//
//    public RequestBean getRequest() {
//        return request;
//    }
//
//    public void setRequest(RequestBean request) {
//        this.request = request;
//    }
//
//    public List<ResponseBean> getResponse() {
//        return response;
//    }
//
//    public void setResponse(List<ResponseBean> response) {
//        this.response = response;
//    }
//
//    public List<?> getOther() {
//        return other;
//    }
//
//    public void setOther(List<?> other) {
//        this.other = other;
//    }
//
//    public static class RequestBean {
//        /**
//         * parkId : 1
//         */
//
//        private String parkId;
//
//        public String getParkId() {
//            return parkId;
//        }
//
//        public void setParkId(String parkId) {
//            this.parkId = parkId;
//        }
//    }
//
//    public static class ResponseBean {
//        /**
//         * id : 2
//         * title : 在党的旗帜下奋进强军新时代
//         * image : http://park.hostop.net/upload/20180816/47c1d7310b9bff1194b85a614b58cc7d.jpg
//         * add_time : 2天前
//         * typeId : 1
//         */
//
//        private String id;
//        private String title;
//        private String image;
//        private String add_time;
//        private String typeId;
//
//        public String getId() {
//            return id;
//        }
//
//        public void setId(String id) {
//            this.id = id;
//        }
//
//        public String getTitle() {
//            return title;
//        }
//
//        public void setTitle(String title) {
//            this.title = title;
//        }
//
//        public String getImage() {
//            return image;
//        }
//
//        public void setImage(String image) {
//            this.image = image;
//        }
//
//        public String getAdd_time() {
//            return add_time;
//        }
//
//        public void setAdd_time(String add_time) {
//            this.add_time = add_time;
//        }
//
//        public String getTypeId() {
//            return typeId;
//        }
//
//        public void setTypeId(String typeId) {
//            this.typeId = typeId;
//        }
//    }
}
