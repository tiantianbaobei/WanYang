package com.wanyangproject.entity;

import java.util.List;

/**
 * Created by 甜甜 on 2018/9/4.
 */

public class WoDeDianZanEntity {
    /**
     * code : 200
     * msg :
     * response : [{"id":"250","title":"地铁、公交、摩托、步行\u2026\u2026上千万北京青年的通勤之路","content":"\r\n\t地铁、公交、摩托、步行，还有打车和共享单车，多种...","add_time":"1544583451","uinfo":{"userid":"135","avatar":"http://park.vyzcc.com/static/image/logo.png","nickname":"wy523796","username":"18410710711"},"imgArr":["http://park.vyzcc.com/upload/20181211/987ba9c6d263c3100926096956eaf349.jpg"],"reply":"1","like":"2","isLike":"0","status":"1","setTop":"0","isPc":"1"}]
     * request : []
     * other : []
     */

    private int code;
    private String msg;
    private List<ResponseBean> response;
    private List<?> request;
    private List<?> other;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public List<ResponseBean> getResponse() {
        return response;
    }

    public void setResponse(List<ResponseBean> response) {
        this.response = response;
    }

    public List<?> getRequest() {
        return request;
    }

    public void setRequest(List<?> request) {
        this.request = request;
    }

    public List<?> getOther() {
        return other;
    }

    public void setOther(List<?> other) {
        this.other = other;
    }

    public static class ResponseBean {
        /**
         * id : 250
         * title : 地铁、公交、摩托、步行……上千万北京青年的通勤之路
         * content :
         地铁、公交、摩托、步行，还有打车和共享单车，多种...
         * add_time : 1544583451
         * uinfo : {"userid":"135","avatar":"http://park.vyzcc.com/static/image/logo.png","nickname":"wy523796","username":"18410710711"}
         * imgArr : ["http://park.vyzcc.com/upload/20181211/987ba9c6d263c3100926096956eaf349.jpg"]
         * reply : 1
         * like : 2
         * isLike : 0
         * status : 1
         * setTop : 0
         * isPc : 1
         */

        private String id;
        private String title;
        private String content;
        private String add_time;
        private UinfoBean uinfo;
        private String reply;
        private String like;
        private String isLike;
        private String status;
        private String setTop;
        private String isPc;
        private List<String> imgArr;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getContent() {
            return content;
        }

        public void setContent(String content) {
            this.content = content;
        }

        public String getAdd_time() {
            return add_time;
        }

        public void setAdd_time(String add_time) {
            this.add_time = add_time;
        }

        public UinfoBean getUinfo() {
            return uinfo;
        }

        public void setUinfo(UinfoBean uinfo) {
            this.uinfo = uinfo;
        }

        public String getReply() {
            return reply;
        }

        public void setReply(String reply) {
            this.reply = reply;
        }

        public String getLike() {
            return like;
        }

        public void setLike(String like) {
            this.like = like;
        }

        public String getIsLike() {
            return isLike;
        }

        public void setIsLike(String isLike) {
            this.isLike = isLike;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getSetTop() {
            return setTop;
        }

        public void setSetTop(String setTop) {
            this.setTop = setTop;
        }

        public String getIsPc() {
            return isPc;
        }

        public void setIsPc(String isPc) {
            this.isPc = isPc;
        }

        public List<String> getImgArr() {
            return imgArr;
        }

        public void setImgArr(List<String> imgArr) {
            this.imgArr = imgArr;
        }

        public static class UinfoBean {
            /**
             * userid : 135
             * avatar : http://park.vyzcc.com/static/image/logo.png
             * nickname : wy523796
             * username : 18410710711
             */

            private String userid;
            private String avatar;
            private String nickname;
            private String username;

            public String getUserid() {
                return userid;
            }

            public void setUserid(String userid) {
                this.userid = userid;
            }

            public String getAvatar() {
                return avatar;
            }

            public void setAvatar(String avatar) {
                this.avatar = avatar;
            }

            public String getNickname() {
                return nickname;
            }

            public void setNickname(String nickname) {
                this.nickname = nickname;
            }

            public String getUsername() {
                return username;
            }

            public void setUsername(String username) {
                this.username = username;
            }
        }
    }


//    /**
//     * code : 200
//     * msg :
//     * response : [{"id":"1","title":"","content":"测试919","add_time":"1534590970","uinfo":{"userid":"3","avatar":"http://park.hostop.net/upload/20180929/053b0cfdbfda0a5cb9c2a5dd10963ff0.jpg","nickname":"哈哈","username":"18364197153"},"imgArr":[""],"reply":"1","like":"2","isLike":"0","status":"1","setTop":"0"},{"id":"69","title":"","content":"我是184用户","add_time":"1537197330","uinfo":{"userid":"0","username":"nobody","nickname":"已注销","avatar":""},"imgArr":["http://park.hostop.net/upload/20180904/e258223320ac5172f81ea6d82f73fe80.png"],"reply":"0","like":"1","isLike":"0","status":"1","setTop":"0"},{"id":"115","title":"","content":"大家都知道，奈特十字韧带断裂已...","add_time":"1537197337","uinfo":{"userid":"0","username":"nobody","nickname":"已注销","avatar":""},"imgArr":[""],"reply":"3","like":"3","isLike":"0","status":"0","setTop":"0"},{"id":"135","title":"","content":"测试919","add_time":"1539762968","uinfo":{"userid":"3","avatar":"http://park.hostop.net/upload/20180929/053b0cfdbfda0a5cb9c2a5dd10963ff0.jpg","nickname":"哈哈","username":"18364197153"},"imgArr":[""],"reply":"2","like":"7","isLike":"0","status":"1","setTop":"0"},{"id":"137","title":"","content":"测试918","add_time":"1540281513","uinfo":{"userid":"0","username":"nobody","nickname":"已注销","avatar":""},"imgArr":[""],"reply":"6","like":"7","isLike":"0","status":"1","setTop":"0"},{"id":"154","title":"","content":"测试返回值","add_time":"1540782348","uinfo":{"userid":"8","avatar":"http://park.hostop.net/upload/20180926/834c03e184ffab1742a750f8e4acc4a1.jpg","nickname":"手讯","username":"13176688711"},"imgArr":["http://park.hostop.net/upload/20180921/a7452a7e2e222dbd9b504c15e390dc19.jpg"],"reply":"4","like":"9","isLike":"0","status":"1","setTop":"0"}]
//     * request : []
//     * other : []
//     */
//
//    private int code;
//    private String msg;
//    private List<ResponseBean> response;
//    private List<?> request;
//    private List<?> other;
//
//    public int getCode() {
//        return code;
//    }
//
//    public void setCode(int code) {
//        this.code = code;
//    }
//
//    public String getMsg() {
//        return msg;
//    }
//
//    public void setMsg(String msg) {
//        this.msg = msg;
//    }
//
//    public List<ResponseBean> getResponse() {
//        return response;
//    }
//
//    public void setResponse(List<ResponseBean> response) {
//        this.response = response;
//    }
//
//    public List<?> getRequest() {
//        return request;
//    }
//
//    public void setRequest(List<?> request) {
//        this.request = request;
//    }
//
//    public List<?> getOther() {
//        return other;
//    }
//
//    public void setOther(List<?> other) {
//        this.other = other;
//    }
//
//    public static class ResponseBean {
//        /**
//         * id : 1
//         * title :
//         * content : 测试919
//         * add_time : 1534590970
//         * uinfo : {"userid":"3","avatar":"http://park.hostop.net/upload/20180929/053b0cfdbfda0a5cb9c2a5dd10963ff0.jpg","nickname":"哈哈","username":"18364197153"}
//         * imgArr : [""]
//         * reply : 1
//         * like : 2
//         * isLike : 0
//         * status : 1
//         * setTop : 0
//         */
//
//        private String id;
//        private String title;
//        private String content;
//        private String add_time;
//        private UinfoBean uinfo;
//        private String reply;
//        private String like;
//        private String isLike;
//        private String status;
//        private String setTop;
//        private List<String> imgArr;
//
//        public String getId() {
//            return id;
//        }
//
//        public void setId(String id) {
//            this.id = id;
//        }
//
//        public String getTitle() {
//            return title;
//        }
//
//        public void setTitle(String title) {
//            this.title = title;
//        }
//
//        public String getContent() {
//            return content;
//        }
//
//        public void setContent(String content) {
//            this.content = content;
//        }
//
//        public String getAdd_time() {
//            return add_time;
//        }
//
//        public void setAdd_time(String add_time) {
//            this.add_time = add_time;
//        }
//
//        public UinfoBean getUinfo() {
//            return uinfo;
//        }
//
//        public void setUinfo(UinfoBean uinfo) {
//            this.uinfo = uinfo;
//        }
//
//        public String getReply() {
//            return reply;
//        }
//
//        public void setReply(String reply) {
//            this.reply = reply;
//        }
//
//        public String getLike() {
//            return like;
//        }
//
//        public void setLike(String like) {
//            this.like = like;
//        }
//
//        public String getIsLike() {
//            return isLike;
//        }
//
//        public void setIsLike(String isLike) {
//            this.isLike = isLike;
//        }
//
//        public String getStatus() {
//            return status;
//        }
//
//        public void setStatus(String status) {
//            this.status = status;
//        }
//
//        public String getSetTop() {
//            return setTop;
//        }
//
//        public void setSetTop(String setTop) {
//            this.setTop = setTop;
//        }
//
//        public List<String> getImgArr() {
//            return imgArr;
//        }
//
//        public void setImgArr(List<String> imgArr) {
//            this.imgArr = imgArr;
//        }
//
//        public static class UinfoBean {
//            /**
//             * userid : 3
//             * avatar : http://park.hostop.net/upload/20180929/053b0cfdbfda0a5cb9c2a5dd10963ff0.jpg
//             * nickname : 哈哈
//             * username : 18364197153
//             */
//
//            private String userid;
//            private String avatar;
//            private String nickname;
//            private String username;
//
//            public String getUserid() {
//                return userid;
//            }
//
//            public void setUserid(String userid) {
//                this.userid = userid;
//            }
//
//            public String getAvatar() {
//                return avatar;
//            }
//
//            public void setAvatar(String avatar) {
//                this.avatar = avatar;
//            }
//
//            public String getNickname() {
//                return nickname;
//            }
//
//            public void setNickname(String nickname) {
//                this.nickname = nickname;
//            }
//
//            public String getUsername() {
//                return username;
//            }
//
//            public void setUsername(String username) {
//                this.username = username;
//            }
//        }
//    }


















//    /**
//     * code : 200
//     * msg :
//     * response : [{"id":"1","title":"测试919","add_time":"1534590970","uinfo":{"userid":"3","avatar":"http://park.hostop.net/upload/20180905/559d73f4ff48ab25dc395fd68d5c391b.jpg","nickname":"哈哈","username":"18364197153"},"imgArr":[""],"reply":"0","like":"2","isLike":"0","status":"1","setTop":"0"},{"id":"28","title":"监控","add_time":"1537197327","uinfo":{"userid":"2","avatar":"http://park.hostop.net/upload/20180915/6755fdb2ff9a3f6cdb253300aecc5f69.jpg","nickname":"呵呵呵","username":"17600904682"},"imgArr":[""],"reply":"0","like":"1","isLike":"0","status":"1","setTop":"0"},{"id":"69","title":"我是184用户","add_time":"1537197330","uinfo":{"userid":"10","avatar":"http://park.hostop.net/upload/20180906/00001e33c45c72fa1a7d3088e3034c28.png","nickname":"后来","username":"18410710712"},"imgArr":["http://park.hostop.net/upload/20180904/e258223320ac5172f81ea6d82f73fe80.png"],"reply":"0","like":"1","isLike":"0","status":"1","setTop":"0"},{"id":"115","title":"大家都知道，奈特十字韧带断裂已...","add_time":"1537197337","uinfo":{"userid":"6","avatar":"http://park.hostop.net/upload/20180918/ab04c4709880c285df0a11759b50d72f.png","nickname":"迷你屋","username":"18301437927"},"imgArr":[""],"reply":"4","like":"3","isLike":"0","status":"1","setTop":"0"},{"id":"110","title":"啊啊","add_time":"1537197339","uinfo":{"userid":"2","avatar":"http://park.hostop.net/upload/20180915/6755fdb2ff9a3f6cdb253300aecc5f69.jpg","nickname":"呵呵呵","username":"17600904682"},"imgArr":["http://park.hostop.net/upload/20180915/3fc239d3511419bd9d9bfc1265674e24.jpg"],"reply":"0","like":"1","isLike":"0","status":"1","setTop":"0"}]
//     * request : []
//     * other : []
//     */
//
//    private int code;
//    private String msg;
//    private List<ResponseBean> response;
//    private List<?> request;
//    private List<?> other;
//
//    public int getCode() {
//        return code;
//    }
//
//    public void setCode(int code) {
//        this.code = code;
//    }
//
//    public String getMsg() {
//        return msg;
//    }
//
//    public void setMsg(String msg) {
//        this.msg = msg;
//    }
//
//    public List<ResponseBean> getResponse() {
//        return response;
//    }
//
//    public void setResponse(List<ResponseBean> response) {
//        this.response = response;
//    }
//
//    public List<?> getRequest() {
//        return request;
//    }
//
//    public void setRequest(List<?> request) {
//        this.request = request;
//    }
//
//    public List<?> getOther() {
//        return other;
//    }
//
//    public void setOther(List<?> other) {
//        this.other = other;
//    }
//
//    public static class ResponseBean {
//        /**
//         * id : 1
//         * title : 测试919
//         * add_time : 1534590970
//         * uinfo : {"userid":"3","avatar":"http://park.hostop.net/upload/20180905/559d73f4ff48ab25dc395fd68d5c391b.jpg","nickname":"哈哈","username":"18364197153"}
//         * imgArr : [""]
//         * reply : 0
//         * like : 2
//         * isLike : 0
//         * status : 1
//         * setTop : 0
//         */
//
//        private String id;
//        private String title;
//        private String add_time;
//        private UinfoBean uinfo;
//        private String reply;
//        private String like;
//        private String isLike;
//        private String status;
//        private String setTop;
//        private List<String> imgArr;
//
//        public String getId() {
//            return id;
//        }
//
//        public void setId(String id) {
//            this.id = id;
//        }
//
//        public String getTitle() {
//            return title;
//        }
//
//        public void setTitle(String title) {
//            this.title = title;
//        }
//
//        public String getAdd_time() {
//            return add_time;
//        }
//
//        public void setAdd_time(String add_time) {
//            this.add_time = add_time;
//        }
//
//        public UinfoBean getUinfo() {
//            return uinfo;
//        }
//
//        public void setUinfo(UinfoBean uinfo) {
//            this.uinfo = uinfo;
//        }
//
//        public String getReply() {
//            return reply;
//        }
//
//        public void setReply(String reply) {
//            this.reply = reply;
//        }
//
//        public String getLike() {
//            return like;
//        }
//
//        public void setLike(String like) {
//            this.like = like;
//        }
//
//        public String getIsLike() {
//            return isLike;
//        }
//
//        public void setIsLike(String isLike) {
//            this.isLike = isLike;
//        }
//
//        public String getStatus() {
//            return status;
//        }
//
//        public void setStatus(String status) {
//            this.status = status;
//        }
//
//        public String getSetTop() {
//            return setTop;
//        }
//
//        public void setSetTop(String setTop) {
//            this.setTop = setTop;
//        }
//
//        public List<String> getImgArr() {
//            return imgArr;
//        }
//
//        public void setImgArr(List<String> imgArr) {
//            this.imgArr = imgArr;
//        }
//
//        public static class UinfoBean {
//            /**
//             * userid : 3
//             * avatar : http://park.hostop.net/upload/20180905/559d73f4ff48ab25dc395fd68d5c391b.jpg
//             * nickname : 哈哈
//             * username : 18364197153
//             */
//
//            private String userid;
//            private String avatar;
//            private String nickname;
//            private String username;
//
//            public String getUserid() {
//                return userid;
//            }
//
//            public void setUserid(String userid) {
//                this.userid = userid;
//            }
//
//            public String getAvatar() {
//                return avatar;
//            }
//
//            public void setAvatar(String avatar) {
//                this.avatar = avatar;
//            }
//
//            public String getNickname() {
//                return nickname;
//            }
//
//            public void setNickname(String nickname) {
//                this.nickname = nickname;
//            }
//
//            public String getUsername() {
//                return username;
//            }
//
//            public void setUsername(String username) {
//                this.username = username;
//            }
//        }
//    }































//
//    /**
//     * code : 200
//     * msg :
//     * response : {"list":[{"id":"72","title":"发帖1339手动审核","add_time":"2小时前","uinfo":{"userid":"10","avatar":"http://park.hostop.net/static/image/logo.png","nickname":"未填写","username":"18410710712"},"imgArr":["http://park.hostop.net/upload/20180904/08ca2c7e603e25368fe133b1879d2dbb.png"],"reply":"6","like":"4","isLike":"1"},{"id":"70","title":"悲伤的天使184","add_time":"4小时前","uinfo":{"userid":"10","avatar":"http://park.hostop.net/static/image/logo.png","nickname":"未填写","username":"18410710712"},"imgArr":[""],"reply":"4","like":"4","isLike":"1"}],"nextPage":"1"}
//     * request : []
//     * other : []
//     */
//
//    private int code;
//    private String msg;
//    private ResponseBean response;
//    private List<?> request;
//    private List<?> other;
//
//    public int getCode() {
//        return code;
//    }
//
//    public void setCode(int code) {
//        this.code = code;
//    }
//
//    public String getMsg() {
//        return msg;
//    }
//
//    public void setMsg(String msg) {
//        this.msg = msg;
//    }
//
//    public ResponseBean getResponse() {
//        return response;
//    }
//
//    public void setResponse(ResponseBean response) {
//        this.response = response;
//    }
//
//    public List<?> getRequest() {
//        return request;
//    }
//
//    public void setRequest(List<?> request) {
//        this.request = request;
//    }
//
//    public List<?> getOther() {
//        return other;
//    }
//
//    public void setOther(List<?> other) {
//        this.other = other;
//    }
//
//    public static class ResponseBean {
//        /**
//         * list : [{"id":"72","title":"发帖1339手动审核","add_time":"2小时前","uinfo":{"userid":"10","avatar":"http://park.hostop.net/static/image/logo.png","nickname":"未填写","username":"18410710712"},"imgArr":["http://park.hostop.net/upload/20180904/08ca2c7e603e25368fe133b1879d2dbb.png"],"reply":"6","like":"4","isLike":"1"},{"id":"70","title":"悲伤的天使184","add_time":"4小时前","uinfo":{"userid":"10","avatar":"http://park.hostop.net/static/image/logo.png","nickname":"未填写","username":"18410710712"},"imgArr":[""],"reply":"4","like":"4","isLike":"1"}]
//         * nextPage : 1
//         */
//
//        private String nextPage;
//        private List<ListBean> list;
//
//        public String getNextPage() {
//            return nextPage;
//        }
//
//        public void setNextPage(String nextPage) {
//            this.nextPage = nextPage;
//        }
//
//        public List<ListBean> getList() {
//            return list;
//        }
//
//        public void setList(List<ListBean> list) {
//            this.list = list;
//        }
//
//        public static class ListBean {
//            /**
//             * id : 72
//             * title : 发帖1339手动审核
//             * add_time : 2小时前
//             * uinfo : {"userid":"10","avatar":"http://park.hostop.net/static/image/logo.png","nickname":"未填写","username":"18410710712"}
//             * imgArr : ["http://park.hostop.net/upload/20180904/08ca2c7e603e25368fe133b1879d2dbb.png"]
//             * reply : 6
//             * like : 4
//             * isLike : 1
//             */
//
//            private String id;
//            private String title;
//            private String add_time;
//            private UinfoBean uinfo;
//            private String reply;
//            private String like;
//            private String isLike;
//            private List<String> imgArr;
//
//            public String getId() {
//                return id;
//            }
//
//            public void setId(String id) {
//                this.id = id;
//            }
//
//            public String getTitle() {
//                return title;
//            }
//
//            public void setTitle(String title) {
//                this.title = title;
//            }
//
//            public String getAdd_time() {
//                return add_time;
//            }
//
//            public void setAdd_time(String add_time) {
//                this.add_time = add_time;
//            }
//
//            public UinfoBean getUinfo() {
//                return uinfo;
//            }
//
//            public void setUinfo(UinfoBean uinfo) {
//                this.uinfo = uinfo;
//            }
//
//            public String getReply() {
//                return reply;
//            }
//
//            public void setReply(String reply) {
//                this.reply = reply;
//            }
//
//            public String getLike() {
//                return like;
//            }
//
//            public void setLike(String like) {
//                this.like = like;
//            }
//
//            public String getIsLike() {
//                return isLike;
//            }
//
//            public void setIsLike(String isLike) {
//                this.isLike = isLike;
//            }
//
//            public List<String> getImgArr() {
//                return imgArr;
//            }
//
//            public void setImgArr(List<String> imgArr) {
//                this.imgArr = imgArr;
//            }
//
//            public static class UinfoBean {
//                /**
//                 * userid : 10
//                 * avatar : http://park.hostop.net/static/image/logo.png
//                 * nickname : 未填写
//                 * username : 18410710712
//                 */
//
//                private String userid;
//                private String avatar;
//                private String nickname;
//                private String username;
//
//                public String getUserid() {
//                    return userid;
//                }
//
//                public void setUserid(String userid) {
//                    this.userid = userid;
//                }
//
//                public String getAvatar() {
//                    return avatar;
//                }
//
//                public void setAvatar(String avatar) {
//                    this.avatar = avatar;
//                }
//
//                public String getNickname() {
//                    return nickname;
//                }
//
//                public void setNickname(String nickname) {
//                    this.nickname = nickname;
//                }
//
//                public String getUsername() {
//                    return username;
//                }
//
//                public void setUsername(String username) {
//                    this.username = username;
//                }
//            }
//        }
//    }
}
