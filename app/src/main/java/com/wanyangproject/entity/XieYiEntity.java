package com.wanyangproject.entity;

import java.util.List;

/**
 * Created by 甜甜 on 2018/9/21.
 */

public class XieYiEntity {

    /**
     * code : 200
     * msg :
     * response : {"content":"<p>万洋集团自身定位中小微企业的服务商和众创平台的运营商，按照\u201c产业聚集、产城融合、资源共享、、产融互动\u201d模式，专门打造承接企业转移的平台，园区生活解决中小微企业在发展过程中生产场地受限的瓶颈。<\/p>"}
     * request : []
     * other : []
     */

    private int code;
    private String msg;
    private ResponseBean response;
    private List<?> request;
    private List<?> other;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public ResponseBean getResponse() {
        return response;
    }

    public void setResponse(ResponseBean response) {
        this.response = response;
    }

    public List<?> getRequest() {
        return request;
    }

    public void setRequest(List<?> request) {
        this.request = request;
    }

    public List<?> getOther() {
        return other;
    }

    public void setOther(List<?> other) {
        this.other = other;
    }

    public static class ResponseBean {
        /**
         * content : <p>万洋集团自身定位中小微企业的服务商和众创平台的运营商，按照“产业聚集、产城融合、资源共享、、产融互动”模式，专门打造承接企业转移的平台，园区生活解决中小微企业在发展过程中生产场地受限的瓶颈。</p>
         */

        private String content;

        public String getContent() {
            return content;
        }

        public void setContent(String content) {
            this.content = content;
        }
    }
}
