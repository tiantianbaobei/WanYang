package com.wanyangproject.entity;

import java.util.List;

/**
 * Created by 甜甜 on 2018/8/27.
 */

public class WeiXinDengLuEntity {
    /**
     * code : 200
     * msg :
     * response : {"userid":"2","username":"17600904682","realname":"","avatar":"http://park.hostop.net/upload/20180927/05515109a51791e52393f6e4daa4e5ee.jpg","nickname":"呵呵呵","uuid":"","parkId":"1","parkInfo":{"parkId":"1","parkName":"温州平阳众创城"},"status":"1","typeId":"1","birth":"1379692800","typeId2":"3","token":"87b6b7e370c29e31a1725db05edb4082","toBind":"0"}
     * request : {"wxkey":"of9-E0g7hKgksAz85N4A_XB3q2z8","uuid":"123"}
     * other : []
     */

    private int code;
    private String msg;
    private ResponseBean response;
    private RequestBean request;
    private List<?> other;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public ResponseBean getResponse() {
        return response;
    }

    public void setResponse(ResponseBean response) {
        this.response = response;
    }

    public RequestBean getRequest() {
        return request;
    }

    public void setRequest(RequestBean request) {
        this.request = request;
    }

    public List<?> getOther() {
        return other;
    }

    public void setOther(List<?> other) {
        this.other = other;
    }

    public static class ResponseBean {
        /**
         * userid : 2
         * username : 17600904682
         * realname :
         * avatar : http://park.hostop.net/upload/20180927/05515109a51791e52393f6e4daa4e5ee.jpg
         * nickname : 呵呵呵
         * uuid :
         * parkId : 1
         * parkInfo : {"parkId":"1","parkName":"温州平阳众创城"}
         * status : 1
         * typeId : 1
         * birth : 1379692800
         * typeId2 : 3
         * token : 87b6b7e370c29e31a1725db05edb4082
         * toBind : 0
         */

        private String userid;
        private String username;
        private String realname;
        private String avatar;
        private String nickname;
        private String uuid;
        private String parkId;
        private ParkInfoBean parkInfo;
        private String status;
        private String typeId;
        private String birth;
        private String typeId2;
        private String token;
        private String toBind;

        public String getUserid() {
            return userid;
        }

        public void setUserid(String userid) {
            this.userid = userid;
        }

        public String getUsername() {
            return username;
        }

        public void setUsername(String username) {
            this.username = username;
        }

        public String getRealname() {
            return realname;
        }

        public void setRealname(String realname) {
            this.realname = realname;
        }

        public String getAvatar() {
            return avatar;
        }

        public void setAvatar(String avatar) {
            this.avatar = avatar;
        }

        public String getNickname() {
            return nickname;
        }

        public void setNickname(String nickname) {
            this.nickname = nickname;
        }

        public String getUuid() {
            return uuid;
        }

        public void setUuid(String uuid) {
            this.uuid = uuid;
        }

        public String getParkId() {
            return parkId;
        }

        public void setParkId(String parkId) {
            this.parkId = parkId;
        }

        public ParkInfoBean getParkInfo() {
            return parkInfo;
        }

        public void setParkInfo(ParkInfoBean parkInfo) {
            this.parkInfo = parkInfo;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getTypeId() {
            return typeId;
        }

        public void setTypeId(String typeId) {
            this.typeId = typeId;
        }

        public String getBirth() {
            return birth;
        }

        public void setBirth(String birth) {
            this.birth = birth;
        }

        public String getTypeId2() {
            return typeId2;
        }

        public void setTypeId2(String typeId2) {
            this.typeId2 = typeId2;
        }

        public String getToken() {
            return token;
        }

        public void setToken(String token) {
            this.token = token;
        }

        public String getToBind() {
            return toBind;
        }

        public void setToBind(String toBind) {
            this.toBind = toBind;
        }

        public static class ParkInfoBean {
            /**
             * parkId : 1
             * parkName : 温州平阳众创城
             */

            private String parkId;
            private String parkName;

            public String getParkId() {
                return parkId;
            }

            public void setParkId(String parkId) {
                this.parkId = parkId;
            }

            public String getParkName() {
                return parkName;
            }

            public void setParkName(String parkName) {
                this.parkName = parkName;
            }
        }
    }

    public static class RequestBean {
        /**
         * wxkey : of9-E0g7hKgksAz85N4A_XB3q2z8
         * uuid : 123
         */

        private String wxkey;
        private String uuid;

        public String getWxkey() {
            return wxkey;
        }

        public void setWxkey(String wxkey) {
            this.wxkey = wxkey;
        }

        public String getUuid() {
            return uuid;
        }

        public void setUuid(String uuid) {
            this.uuid = uuid;
        }
    }


//    /**
//     * code : 200
//     * msg :
//     * response : {"userid":"2","username":"17600904682","avatar":"http://park.hostop.net/static/image/logo.png","nickname":"未填写","realname":"","uuid":"8AE5A3FB-9B39-4E56-85F1-A5117A2DF693","parkId":"0","parkInfo":{"parkId":"1","parkName":"温州平阳众创城"},"status":"1","token":"9e899ab9b844d8120440b526e784b19f"}
//     * request : {"phone":"17600904682","code":"823331","authId":"174","wxkey":"of9-E0g7hKgksAz85N4A_XB3q2z8"}
//     * other : []
//     */
//
//    private int code;
//    private String msg;
//    private ResponseBean response;
//    private RequestBean request;
//    private List<?> other;
//
//    public int getCode() {
//        return code;
//    }
//
//    public void setCode(int code) {
//        this.code = code;
//    }
//
//    public String getMsg() {
//        return msg;
//    }
//
//    public void setMsg(String msg) {
//        this.msg = msg;
//    }
//
//    public ResponseBean getResponse() {
//        return response;
//    }
//
//    public void setResponse(ResponseBean response) {
//        this.response = response;
//    }
//
//    public RequestBean getRequest() {
//        return request;
//    }
//
//    public void setRequest(RequestBean request) {
//        this.request = request;
//    }
//
//    public List<?> getOther() {
//        return other;
//    }
//
//    public void setOther(List<?> other) {
//        this.other = other;
//    }
//
//    public static class ResponseBean {
//        /**
//         * userid : 2
//         * username : 17600904682
//         * avatar : http://park.hostop.net/static/image/logo.png
//         * nickname : 未填写
//         * realname :
//         * uuid : 8AE5A3FB-9B39-4E56-85F1-A5117A2DF693
//         * parkId : 0
//         * parkInfo : {"parkId":"1","parkName":"温州平阳众创城"}
//         * status : 1
//         * token : 9e899ab9b844d8120440b526e784b19f
//         */
//
//        private String userid;
//        private String username;
//        private String avatar;
//        private String nickname;
//        private String realname;
//        private String uuid;
//        private String parkId;
//        private ParkInfoBean parkInfo;
//        private String status;
//        private String token;
//
//        public String getUserid() {
//            return userid;
//        }
//
//        public void setUserid(String userid) {
//            this.userid = userid;
//        }
//
//        public String getUsername() {
//            return username;
//        }
//
//        public void setUsername(String username) {
//            this.username = username;
//        }
//
//        public String getAvatar() {
//            return avatar;
//        }
//
//        public void setAvatar(String avatar) {
//            this.avatar = avatar;
//        }
//
//        public String getNickname() {
//            return nickname;
//        }
//
//        public void setNickname(String nickname) {
//            this.nickname = nickname;
//        }
//
//        public String getRealname() {
//            return realname;
//        }
//
//        public void setRealname(String realname) {
//            this.realname = realname;
//        }
//
//        public String getUuid() {
//            return uuid;
//        }
//
//        public void setUuid(String uuid) {
//            this.uuid = uuid;
//        }
//
//        public String getParkId() {
//            return parkId;
//        }
//
//        public void setParkId(String parkId) {
//            this.parkId = parkId;
//        }
//
//        public ParkInfoBean getParkInfo() {
//            return parkInfo;
//        }
//
//        public void setParkInfo(ParkInfoBean parkInfo) {
//            this.parkInfo = parkInfo;
//        }
//
//        public String getStatus() {
//            return status;
//        }
//
//        public void setStatus(String status) {
//            this.status = status;
//        }
//
//        public String getToken() {
//            return token;
//        }
//
//        public void setToken(String token) {
//            this.token = token;
//        }
//
//        public static class ParkInfoBean {
//            /**
//             * parkId : 1
//             * parkName : 温州平阳众创城
//             */
//
//            private String parkId;
//            private String parkName;
//
//            public String getParkId() {
//                return parkId;
//            }
//
//            public void setParkId(String parkId) {
//                this.parkId = parkId;
//            }
//
//            public String getParkName() {
//                return parkName;
//            }
//
//            public void setParkName(String parkName) {
//                this.parkName = parkName;
//            }
//        }
//    }
//
//    public static class RequestBean {
//        /**
//         * phone : 17600904682
//         * code : 823331
//         * authId : 174
//         * wxkey : of9-E0g7hKgksAz85N4A_XB3q2z8
//         */
//
//        private String phone;
//        private String code;
//        private String authId;
//        private String wxkey;
//
//        public String getPhone() {
//            return phone;
//        }
//
//        public void setPhone(String phone) {
//            this.phone = phone;
//        }
//
//        public String getCode() {
//            return code;
//        }
//
//        public void setCode(String code) {
//            this.code = code;
//        }
//
//        public String getAuthId() {
//            return authId;
//        }
//
//        public void setAuthId(String authId) {
//            this.authId = authId;
//        }
//
//        public String getWxkey() {
//            return wxkey;
//        }
//
//        public void setWxkey(String wxkey) {
//            this.wxkey = wxkey;
//        }
//    }

























//
//    /**
//     * code : 200
//     * msg :
//     * response : {"userid":"0","username":"nobody","nickname":"已注销","avatar":"","uuid":"","token":"c9f8a78e2b0a32823469fca71c6d5e9a"}
//     * request : {"wxkey":"of9-E0g7hKgksAz85N4A_XB3q2z8","uuid":""}
//     * other : []
//     */
//
//    private int code;
//    private String msg;
//    private ResponseBean response;
//    private RequestBean request;
//    private List<?> other;
//
//    public int getCode() {
//        return code;
//    }
//
//    public void setCode(int code) {
//        this.code = code;
//    }
//
//    public String getMsg() {
//        return msg;
//    }
//
//    public void setMsg(String msg) {
//        this.msg = msg;
//    }
//
//    public ResponseBean getResponse() {
//        return response;
//    }
//
//    public void setResponse(ResponseBean response) {
//        this.response = response;
//    }
//
//    public RequestBean getRequest() {
//        return request;
//    }
//
//    public void setRequest(RequestBean request) {
//        this.request = request;
//    }
//
//    public List<?> getOther() {
//        return other;
//    }
//
//    public void setOther(List<?> other) {
//        this.other = other;
//    }
//
//    public static class ResponseBean {
//        /**
//         * userid : 0
//         * username : nobody
//         * nickname : 已注销
//         * avatar :
//         * uuid :
//         * token : c9f8a78e2b0a32823469fca71c6d5e9a
//         */
//
//        private String userid;
//        private String username;
//        private String nickname;
//        private String avatar;
//        private String uuid;
//        private String token;
//
//        public String getUserid() {
//            return userid;
//        }
//
//        public void setUserid(String userid) {
//            this.userid = userid;
//        }
//
//        public String getUsername() {
//            return username;
//        }
//
//        public void setUsername(String username) {
//            this.username = username;
//        }
//
//        public String getNickname() {
//            return nickname;
//        }
//
//        public void setNickname(String nickname) {
//            this.nickname = nickname;
//        }
//
//        public String getAvatar() {
//            return avatar;
//        }
//
//        public void setAvatar(String avatar) {
//            this.avatar = avatar;
//        }
//
//        public String getUuid() {
//            return uuid;
//        }
//
//        public void setUuid(String uuid) {
//            this.uuid = uuid;
//        }
//
//        public String getToken() {
//            return token;
//        }
//
//        public void setToken(String token) {
//            this.token = token;
//        }
//    }
//
//    public static class RequestBean {
//        /**
//         * wxkey : of9-E0g7hKgksAz85N4A_XB3q2z8
//         * uuid :
//         */
//
//        private String wxkey;
//        private String uuid;
//
//        public String getWxkey() {
//            return wxkey;
//        }
//
//        public void setWxkey(String wxkey) {
//            this.wxkey = wxkey;
//        }
//
//        public String getUuid() {
//            return uuid;
//        }
//
//        public void setUuid(String uuid) {
//            this.uuid = uuid;
//        }
//    }
}
