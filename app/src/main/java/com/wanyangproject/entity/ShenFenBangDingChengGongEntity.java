package com.wanyangproject.entity;

import java.util.List;

/**
 * Created by 甜甜 on 2018/10/8.
 */

public class ShenFenBangDingChengGongEntity {

    /**
     * code : 200
     * msg :
     * response : {"userid":"3","username":"18364197153","realname":"","avatar":"http://park.hostop.net/upload/20180929/053b0cfdbfda0a5cb9c2a5dd10963ff0.jpg","nickname":"哈哈","uuid":"0","parkId":"1","parkInfo":{"parkId":"1","parkName":"温州平阳众创城"},"status":"1","typeId":"1","birth":"842630400","typeId2":"0"}
     * request : {"parkId":"1","idCard":"370911199501364569"}
     * other : []
     */

    private int code;
    private String msg;
    private ResponseBean response;
    private RequestBean request;
    private List<?> other;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public ResponseBean getResponse() {
        return response;
    }

    public void setResponse(ResponseBean response) {
        this.response = response;
    }

    public RequestBean getRequest() {
        return request;
    }

    public void setRequest(RequestBean request) {
        this.request = request;
    }

    public List<?> getOther() {
        return other;
    }

    public void setOther(List<?> other) {
        this.other = other;
    }

    public static class ResponseBean {
        /**
         * userid : 3
         * username : 18364197153
         * realname :
         * avatar : http://park.hostop.net/upload/20180929/053b0cfdbfda0a5cb9c2a5dd10963ff0.jpg
         * nickname : 哈哈
         * uuid : 0
         * parkId : 1
         * parkInfo : {"parkId":"1","parkName":"温州平阳众创城"}
         * status : 1
         * typeId : 1
         * birth : 842630400
         * typeId2 : 0
         */

        private String userid;
        private String username;
        private String realname;
        private String avatar;
        private String nickname;
        private String uuid;
        private String parkId;
        private ParkInfoBean parkInfo;
        private String status;
        private String typeId;
        private String birth;
        private String typeId2;

        public String getUserid() {
            return userid;
        }

        public void setUserid(String userid) {
            this.userid = userid;
        }

        public String getUsername() {
            return username;
        }

        public void setUsername(String username) {
            this.username = username;
        }

        public String getRealname() {
            return realname;
        }

        public void setRealname(String realname) {
            this.realname = realname;
        }

        public String getAvatar() {
            return avatar;
        }

        public void setAvatar(String avatar) {
            this.avatar = avatar;
        }

        public String getNickname() {
            return nickname;
        }

        public void setNickname(String nickname) {
            this.nickname = nickname;
        }

        public String getUuid() {
            return uuid;
        }

        public void setUuid(String uuid) {
            this.uuid = uuid;
        }

        public String getParkId() {
            return parkId;
        }

        public void setParkId(String parkId) {
            this.parkId = parkId;
        }

        public ParkInfoBean getParkInfo() {
            return parkInfo;
        }

        public void setParkInfo(ParkInfoBean parkInfo) {
            this.parkInfo = parkInfo;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getTypeId() {
            return typeId;
        }

        public void setTypeId(String typeId) {
            this.typeId = typeId;
        }

        public String getBirth() {
            return birth;
        }

        public void setBirth(String birth) {
            this.birth = birth;
        }

        public String getTypeId2() {
            return typeId2;
        }

        public void setTypeId2(String typeId2) {
            this.typeId2 = typeId2;
        }

        public static class ParkInfoBean {
            /**
             * parkId : 1
             * parkName : 温州平阳众创城
             */

            private String parkId;
            private String parkName;

            public String getParkId() {
                return parkId;
            }

            public void setParkId(String parkId) {
                this.parkId = parkId;
            }

            public String getParkName() {
                return parkName;
            }

            public void setParkName(String parkName) {
                this.parkName = parkName;
            }
        }
    }

    public static class RequestBean {
        /**
         * parkId : 1
         * idCard : 370911199501364569
         */

        private String parkId;
        private String idCard;

        public String getParkId() {
            return parkId;
        }

        public void setParkId(String parkId) {
            this.parkId = parkId;
        }

        public String getIdCard() {
            return idCard;
        }

        public void setIdCard(String idCard) {
            this.idCard = idCard;
        }
    }
}
