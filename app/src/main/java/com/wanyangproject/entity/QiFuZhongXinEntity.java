package com.wanyangproject.entity;

import com.chad.library.adapter.base.entity.MultiItemEntity;

import java.util.List;

/**
 * Created by 甜甜 on 2018/8/24.
 */

public class QiFuZhongXinEntity {
    /**
     * code : 200
     * msg :
     * response : {"upTjArr":[{"id":"8","title":"","image":"http://park.hostop.net/upload/20180906/1f5b2c9ae44040f2c49a083fd32a517f.jpg","add_time":"15天前","hrefType":"0","content":""},{"id":"7","title":"","image":"http://park.hostop.net/upload/20180906/61892d4b8a1724c5480510ce9ddaa189.jpg","add_time":"15天前","hrefType":"9","content":""},{"id":"6","title":"","image":"http://park.hostop.net/upload/20180906/3c20de4965161f35609ccdaf1fbee65f.jpg","add_time":"15天前","hrefType":"1","content":""},{"id":"5","title":"","image":"http://park.hostop.net/upload/20180906/3c20de4965161f35609ccdaf1fbee65f.jpg","add_time":"15天前","hrefType":"2","content":""}],"bannerArr":[{"id":"14","title":"","image":"http://park.hostop.net/upload/20180910/89126b24889519507d19f9adf5ad8ecb.jpg","add_time":"12天前","hrefType":"5","content":""},{"id":"6","title":"","image":"http://park.hostop.net/upload/20180906/3c20de4965161f35609ccdaf1fbee65f.jpg","add_time":"15天前","hrefType":"5","content":""}],"wordArr":[{"id":"5","title":"企业生活","logo":"","imgArr":[],"desc":"企业生活...","add_time":"19天前"},{"id":"3","title":"企业大健康","logo":"http://park.hostop.net/upload/20180903/a15e0bd070be4358eea00b2b634f5396.png","imgArr":["http://park.hostop.net/upload/20180903/a15e0bd070be4358eea00b2b634f5396.png"],"desc":"企业大健康...","add_time":"19天前"}],"category":[{"id":"21","title":"企业咨询","logo":"http://park.hostop.net/upload/20180910/caf8dfbef2d1b9446a0d421189f960d9.jpg","add_time":"11天前"},{"id":"5","title":"企业需知","logo":"http://park.hostop.net/upload/20180903/4b8088ecea55536ae8af11f31bd0637a.png","add_time":"19天前"}],"info":{"content":"<p>企服中心详情介绍一下<\/p><p><img src=\"http://park.hostop.net/upload/20180829/e67d6d8685520bb89f7c3dd2e61e030e.png\" alt=\"undefined\" style=\"width:50%\"><\/p>"},"tjIndex":[{"id":"16","title":"测试","logo":"","imgArr":[],"desc":"123456...","add_time":"6小时前"},{"id":"15","title":"园区招商分类资讯","logo":"","imgArr":[],"desc":"测试...","add_time":"8小时前"},{"id":"14","title":"测试跳转","logo":"","imgArr":[],"desc":"跳转...","add_time":"16小时前"},{"id":"5","title":"企业生活","logo":"","imgArr":[],"desc":"企业生活...","add_time":"19天前"},{"id":"3","title":"企业大健康","logo":"http://park.hostop.net/upload/20180903/a15e0bd070be4358eea00b2b634f5396.png","imgArr":["http://park.hostop.net/upload/20180903/a15e0bd070be4358eea00b2b634f5396.png"],"desc":"企业大健康...","add_time":"19天前"}],"background":""}
     * request : {"parkId":"1","typeId":"2"}
     * other : []
     */

    private int code;
    private String msg;
    private ResponseBean response;
    private RequestBean request;
    private List<?> other;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public ResponseBean getResponse() {
        return response;
    }

    public void setResponse(ResponseBean response) {
        this.response = response;
    }

    public RequestBean getRequest() {
        return request;
    }

    public void setRequest(RequestBean request) {
        this.request = request;
    }

    public List<?> getOther() {
        return other;
    }

    public void setOther(List<?> other) {
        this.other = other;
    }

    public static class ResponseBean {
        /**
         * upTjArr : [{"id":"8","title":"","image":"http://park.hostop.net/upload/20180906/1f5b2c9ae44040f2c49a083fd32a517f.jpg","add_time":"15天前","hrefType":"0","content":""},{"id":"7","title":"","image":"http://park.hostop.net/upload/20180906/61892d4b8a1724c5480510ce9ddaa189.jpg","add_time":"15天前","hrefType":"9","content":""},{"id":"6","title":"","image":"http://park.hostop.net/upload/20180906/3c20de4965161f35609ccdaf1fbee65f.jpg","add_time":"15天前","hrefType":"1","content":""},{"id":"5","title":"","image":"http://park.hostop.net/upload/20180906/3c20de4965161f35609ccdaf1fbee65f.jpg","add_time":"15天前","hrefType":"2","content":""}]
         * bannerArr : [{"id":"14","title":"","image":"http://park.hostop.net/upload/20180910/89126b24889519507d19f9adf5ad8ecb.jpg","add_time":"12天前","hrefType":"5","content":""},{"id":"6","title":"","image":"http://park.hostop.net/upload/20180906/3c20de4965161f35609ccdaf1fbee65f.jpg","add_time":"15天前","hrefType":"5","content":""}]
         * wordArr : [{"id":"5","title":"企业生活","logo":"","imgArr":[],"desc":"企业生活...","add_time":"19天前"},{"id":"3","title":"企业大健康","logo":"http://park.hostop.net/upload/20180903/a15e0bd070be4358eea00b2b634f5396.png","imgArr":["http://park.hostop.net/upload/20180903/a15e0bd070be4358eea00b2b634f5396.png"],"desc":"企业大健康...","add_time":"19天前"}]
         * category : [{"id":"21","title":"企业咨询","logo":"http://park.hostop.net/upload/20180910/caf8dfbef2d1b9446a0d421189f960d9.jpg","add_time":"11天前"},{"id":"5","title":"企业需知","logo":"http://park.hostop.net/upload/20180903/4b8088ecea55536ae8af11f31bd0637a.png","add_time":"19天前"}]
         * info : {"content":"<p>企服中心详情介绍一下<\/p><p><img src=\"http://park.hostop.net/upload/20180829/e67d6d8685520bb89f7c3dd2e61e030e.png\" alt=\"undefined\" style=\"width:50%\"><\/p>"}
         * tjIndex : [{"id":"16","title":"测试","logo":"","imgArr":[],"desc":"123456...","add_time":"6小时前"},{"id":"15","title":"园区招商分类资讯","logo":"","imgArr":[],"desc":"测试...","add_time":"8小时前"},{"id":"14","title":"测试跳转","logo":"","imgArr":[],"desc":"跳转...","add_time":"16小时前"},{"id":"5","title":"企业生活","logo":"","imgArr":[],"desc":"企业生活...","add_time":"19天前"},{"id":"3","title":"企业大健康","logo":"http://park.hostop.net/upload/20180903/a15e0bd070be4358eea00b2b634f5396.png","imgArr":["http://park.hostop.net/upload/20180903/a15e0bd070be4358eea00b2b634f5396.png"],"desc":"企业大健康...","add_time":"19天前"}]
         * background :
         */

        private InfoBean info;
        private String background;
        private List<UpTjArrBean> upTjArr;
        private List<BannerArrBean> bannerArr;
        private List<WordArrBean> wordArr;
        private List<CategoryBean> category;
        private List<TjIndexBean> tjIndex;

        public InfoBean getInfo() {
            return info;
        }

        public void setInfo(InfoBean info) {
            this.info = info;
        }

        public String getBackground() {
            return background;
        }

        public void setBackground(String background) {
            this.background = background;
        }

        public List<UpTjArrBean> getUpTjArr() {
            return upTjArr;
        }

        public void setUpTjArr(List<UpTjArrBean> upTjArr) {
            this.upTjArr = upTjArr;
        }

        public List<BannerArrBean> getBannerArr() {
            return bannerArr;
        }

        public void setBannerArr(List<BannerArrBean> bannerArr) {
            this.bannerArr = bannerArr;
        }

        public List<WordArrBean> getWordArr() {
            return wordArr;
        }

        public void setWordArr(List<WordArrBean> wordArr) {
            this.wordArr = wordArr;
        }

        public List<CategoryBean> getCategory() {
            return category;
        }

        public void setCategory(List<CategoryBean> category) {
            this.category = category;
        }

        public List<TjIndexBean> getTjIndex() {
            return tjIndex;
        }

        public void setTjIndex(List<TjIndexBean> tjIndex) {
            this.tjIndex = tjIndex;
        }

        public static class InfoBean {
            /**
             * content : <p>企服中心详情介绍一下</p><p><img src="http://park.hostop.net/upload/20180829/e67d6d8685520bb89f7c3dd2e61e030e.png" alt="undefined" style="width:50%"></p>
             */

            private String content;

            public String getContent() {
                return content;
            }

            public void setContent(String content) {
                this.content = content;
            }
        }

        public static class UpTjArrBean {
            /**
             * id : 8
             * title :
             * image : http://park.hostop.net/upload/20180906/1f5b2c9ae44040f2c49a083fd32a517f.jpg
             * add_time : 15天前
             * hrefType : 0
             * content :
             */

            private String id;
            private String title;
            private String image;
            private String add_time;
            private String hrefType;
            private String content;

            public String getId() {
                return id;
            }

            public void setId(String id) {
                this.id = id;
            }

            public String getTitle() {
                return title;
            }

            public void setTitle(String title) {
                this.title = title;
            }

            public String getImage() {
                return image;
            }

            public void setImage(String image) {
                this.image = image;
            }

            public String getAdd_time() {
                return add_time;
            }

            public void setAdd_time(String add_time) {
                this.add_time = add_time;
            }

            public String getHrefType() {
                return hrefType;
            }

            public void setHrefType(String hrefType) {
                this.hrefType = hrefType;
            }

            public String getContent() {
                return content;
            }

            public void setContent(String content) {
                this.content = content;
            }
        }

        public static class BannerArrBean {
            /**
             * id : 14
             * title :
             * image : http://park.hostop.net/upload/20180910/89126b24889519507d19f9adf5ad8ecb.jpg
             * add_time : 12天前
             * hrefType : 5
             * content :
             */

            private String id;
            private String title;
            private String image;
            private String add_time;
            private String hrefType;
            private String content;

            public String getId() {
                return id;
            }

            public void setId(String id) {
                this.id = id;
            }

            public String getTitle() {
                return title;
            }

            public void setTitle(String title) {
                this.title = title;
            }

            public String getImage() {
                return image;
            }

            public void setImage(String image) {
                this.image = image;
            }

            public String getAdd_time() {
                return add_time;
            }

            public void setAdd_time(String add_time) {
                this.add_time = add_time;
            }

            public String getHrefType() {
                return hrefType;
            }

            public void setHrefType(String hrefType) {
                this.hrefType = hrefType;
            }

            public String getContent() {
                return content;
            }

            public void setContent(String content) {
                this.content = content;
            }
        }

        public static class WordArrBean {
            /**
             * id : 5
             * title : 企业生活
             * logo :
             * imgArr : []
             * desc : 企业生活...
             * add_time : 19天前
             */

            private String id;
            private String title;
            private String logo;
            private String desc;
            private String add_time;
            private List<?> imgArr;

            public String getId() {
                return id;
            }

            public void setId(String id) {
                this.id = id;
            }

            public String getTitle() {
                return title;
            }

            public void setTitle(String title) {
                this.title = title;
            }

            public String getLogo() {
                return logo;
            }

            public void setLogo(String logo) {
                this.logo = logo;
            }

            public String getDesc() {
                return desc;
            }

            public void setDesc(String desc) {
                this.desc = desc;
            }

            public String getAdd_time() {
                return add_time;
            }

            public void setAdd_time(String add_time) {
                this.add_time = add_time;
            }

            public List<?> getImgArr() {
                return imgArr;
            }

            public void setImgArr(List<?> imgArr) {
                this.imgArr = imgArr;
            }
        }

        public static class CategoryBean {
            /**
             * id : 21
             * title : 企业咨询
             * logo : http://park.hostop.net/upload/20180910/caf8dfbef2d1b9446a0d421189f960d9.jpg
             * add_time : 11天前
             */

            private String id;
            private String title;
            private String logo;
            private String add_time;

            public String getId() {
                return id;
            }

            public void setId(String id) {
                this.id = id;
            }

            public String getTitle() {
                return title;
            }

            public void setTitle(String title) {
                this.title = title;
            }

            public String getLogo() {
                return logo;
            }

            public void setLogo(String logo) {
                this.logo = logo;
            }

            public String getAdd_time() {
                return add_time;
            }

            public void setAdd_time(String add_time) {
                this.add_time = add_time;
            }
        }

        public static class TjIndexBean implements MultiItemEntity {
            /**
             * id : 16
             * title : 测试
             * logo :
             * imgArr : []
             * desc : 123456...
             * add_time : 6小时前
             */

            private String id;
            private String title;
            private String logo;
            private String desc;
            private String add_time;
            private List<?> imgArr;








            public static final int LING = 0;
            public static final int ONE = 1;
            public static final int TWO = 2;
            public static final int THREE = 3;
            public static final int SEX = 6;
            public static final int NINE = 9;


            private int itemType;

            public void setItemType(int itemType) {
                this.itemType = itemType;
            }

            @Override
            public int getItemType() {
                return itemType;
            }







            public String getId() {
                return id;
            }

            public void setId(String id) {
                this.id = id;
            }

            public String getTitle() {
                return title;
            }

            public void setTitle(String title) {
                this.title = title;
            }

            public String getLogo() {
                return logo;
            }

            public void setLogo(String logo) {
                this.logo = logo;
            }

            public String getDesc() {
                return desc;
            }

            public void setDesc(String desc) {
                this.desc = desc;
            }

            public String getAdd_time() {
                return add_time;
            }

            public void setAdd_time(String add_time) {
                this.add_time = add_time;
            }

            public List<?> getImgArr() {
                return imgArr;
            }

            public void setImgArr(List<?> imgArr) {
                this.imgArr = imgArr;
            }
        }
    }

    public static class RequestBean {
        /**
         * parkId : 1
         * typeId : 2
         */

        private String parkId;
        private String typeId;

        public String getParkId() {
            return parkId;
        }

        public void setParkId(String parkId) {
            this.parkId = parkId;
        }

        public String getTypeId() {
            return typeId;
        }

        public void setTypeId(String typeId) {
            this.typeId = typeId;
        }
    }


//    /**
//     * code : 200
//     * msg :
//     * response :{"upTjArr":[{"id":"12","title":"","image":"http://park.hostop.net/upload/20180908/26c7e99ad5e7df6815badfb97057ab9c.jpg","add_time":"3天前","hrefType":"5","content":""},{"id":"4","title":"","image":"http://park.hostop.net/upload/20180906/3c20de4965161f35609ccdaf1fbee65f.jpg","add_time":"3天前","hrefType":"1","content":""},{"id":"3","title":"","image":"http://park.hostop.net/upload/20180906/3c20de4965161f35609ccdaf1fbee65f.jpg","add_time":"3天前","hrefType":"9","content":"<p><span style=\"text-align: justify;\">为加强抗菌药物管理，遏制细菌耐药，维护人民群众健康，促进经济社会协调发展，制定本行动计划。<span lang=\"EN-US\">&nbsp;<br><\/span><\/span><span style=\"text-align: justify;\">　　一、前言<\/span><span style=\"text-align: justify;\" lang=\"EN-US\">&nbsp;<\/span><span style=\"text-align: justify;\" lang=\"EN-US\"><br><\/span><span style=\"text-align: justify;\">　　我国是抗菌药物的生产和使用大国。抗菌药物广泛应用于医疗卫生、农业养殖领域，在治疗感染性疾病挽救患者生命、防治动物疫病提高养殖效益以及保障公共卫生安全中，发挥了重要作用。但是，由于新型抗菌药物研发能力不足、药店无处方销售抗菌药物、医疗和养殖领域不合理应用抗菌药物、制药企业废弃物排放不达标、群众合理用药意识不高等多种因素，细菌耐药问题日益突出。细菌耐药最终影响人类健康，但造成细菌耐药的因素及其后果却超越了卫生领域，给人类社会带来了生物安全威胁加大、环境污染加剧、经济发展制约等不利影响，迫切需要加强多部门多领域协同谋划、共同应对。<span lang=\"EN-US\">&nbsp;<br><\/span><\/span><span style=\"text-align: justify;\">　　二、工作目标<span lang=\"EN-US\">&nbsp;<\/span><\/span><span style=\"text-align: justify;\" lang=\"EN-US\"><br><\/span><span style=\"text-align: justify;\">　　从国家层面实施综合治理策略和措施，对抗菌药物的研发、生产、流通、应用、环境保护等各个环节加强监管，加强宣传教育和国际交流合作，应对细菌耐药带来的风险挑战。到<span lang=\"EN-US\">2020<\/span>年：<span lang=\"EN-US\">&nbsp;<br><\/span>　　（一）争取研发上市全新抗菌药物<span lang=\"EN-US\">1-2<\/span>个，新型诊断仪器设备和试剂<span lang=\"EN-US\">5-10<\/span>项。<span lang=\"EN-US\">&nbsp;<br><\/span>　　（二）零售药店凭处方销售抗菌药物的比例基本达到全覆盖。省（区、市）凭兽医处方销售抗菌药物的比例达到<span lang=\"EN-US\">50%<\/span>。<span lang=\"EN-US\">&nbsp;<br><\/span>　　（三）健全医疗机构、动物源抗菌药物应用和细菌耐药监测网络；建设细菌耐药参比实验室和菌种中心；建立医疗、养殖领域的抗菌药物应用和细菌耐药控制评价体系。<span lang=\"EN-US\">&nbsp;<br><\/span>　　（四）全国二级以上医院基本建立抗菌药物临床应用管理机制；医疗机构主要耐药菌增长率得到有效控制。<span lang=\"EN-US\">&nbsp;<\/span><\/span><\/p>"},{"id":"2","title":"","image":"http://park.hostop.net/upload/20180906/4d3a4db8e349a4ec7a096ade2d2aea81.jpg","add_time":"3天前","hrefType":"0","content":""}],"bannerArr":[{"id":"9","title":"","image":"http://park.hostop.net/upload/20180908/82e5322abc900ef875950bcc417d29fc.jpg","add_time":"2天前","hrefType":"0","content":""}],"wordArr":[{"id":"9","title":"","image":"http://park.hostop.net/upload/20180908/82e5322abc900ef875950bcc417d29fc.jpg","add_time":"2天前","hrefType":"0","content":""}],"category":[{"id":"9","title":"","image":"http://park.hostop.net/upload/20180908/82e5322abc900ef875950bcc417d29fc.jpg","add_time":"2天前","hrefType":"0","content":""}],"info":{"content":""},"tjIndex":[]}
//     * request : {"parkId":"1","typeId":"2"}
//     * other : []
//     */
//
//    private int code;
//    private String msg;
//    private ResponseBean response;
//    private RequestBean request;
//    private List<?> other;
//
//    public int getCode() {
//        return code;
//    }
//
//    public void setCode(int code) {
//        this.code = code;
//    }
//
//    public String getMsg() {
//        return msg;
//    }
//
//    public void setMsg(String msg) {
//        this.msg = msg;
//    }
//
//    public ResponseBean getResponse() {
//        return response;
//    }
//
//    public void setResponse(ResponseBean response) {
//        this.response = response;
//    }
//
//    public RequestBean getRequest() {
//        return request;
//    }
//
//    public void setRequest(RequestBean request) {
//        this.request = request;
//    }
//
//    public List<?> getOther() {
//        return other;
//    }
//
//    public void setOther(List<?> other) {
//        this.other = other;
//    }
//
//    public static class ResponseBean {
//        /**
//         * upTjArr : [{"id":"8","title":"","image":"http://park.hostop.net/upload/20180906/1f5b2c9ae44040f2c49a083fd32a517f.jpg","add_time":"12小时前","hrefType":"0","url":"?id=1"},{"id":"7","title":"","image":"http://park.hostop.net/upload/20180906/61892d4b8a1724c5480510ce9ddaa189.jpg","add_time":"12小时前","hrefType":"3","url":"?id=10"},{"id":"6","title":"","image":"http://park.hostop.net/upload/20180906/3c20de4965161f35609ccdaf1fbee65f.jpg","add_time":"12小时前","hrefType":"1","url":"?id=5"},{"id":"5","title":"","image":"http://park.hostop.net/upload/20180906/3c20de4965161f35609ccdaf1fbee65f.jpg","add_time":"12小时前","hrefType":"2","url":"?id=5"}]
//         * bannerArr : [{"id":"6","title":"","image":"http://park.hostop.net/upload/20180906/3c20de4965161f35609ccdaf1fbee65f.jpg","add_time":"12小时前","hrefType":"1","url":"?id=5"},{"id":"5","title":"","image":"http://park.hostop.net/upload/20180906/3c20de4965161f35609ccdaf1fbee65f.jpg","add_time":"12小时前","hrefType":"2","url":"?id=5"}]
//         * wordArr : [{"id":"9","title":"","image":"http://park.hostop.net/upload/20180908/82e5322abc900ef875950bcc417d29fc.jpg","add_time":"2天前","hrefType":"0","content":""}]
//         * category : [{"id":"18","title":"社会救助暂行办法","logo":"http://park.hostop.net/upload/20180910/235301500e050f394c70fb590abde155.jpg","add_time":"3分钟前"},{"id":"17","title":"测试","logo":"http://park.hostop.net/upload/20180910/6ece71a6d08b5c2a2422a4c89be3bfc9.jpg","add_time":"3小时前"},{"id":"16","title":"济南网点","logo":"http://park.hostop.net/upload/20180910/070291e48ee39343dea1f2809b0389c4.jpg","add_time":"3小时前"},{"id":"15","title":"河北网点","logo":"http://park.hostop.net","add_time":"4小时前"}]
//         * info : {"content":"<p>企服中心详情介绍一下<\/p><p><img src=\"http://park.hostop.net/upload/20180829/e67d6d8685520bb89f7c3dd2e61e030e.png\" alt=\"undefined\" style=\"width:50%\"><\/p>"}
////         * tjIndex : [{"id":"5","title":"企业生活","logo":"","imgArr":[]},{"id":"3","title":"企业大健康","logo":"http://park.hostop.net/upload/20180903/a15e0bd070be4358eea00b2b634f5396.png","imgArr":["http://park.hostop.net/upload/20180903/a15e0bd070be4358eea00b2b634f5396.png"]}]
//         *tjIndex : [{"id":"7","title":"123123123123","desc":"基本制造内的内容基本制造内的内容基本制造内的内容基本制造内的内容基本制造内的内容基本制造内的内容...","imgArr":["http://park.hostop.net/upload/20180816/8085d87ae42fa0ec15bc0a8f79ff4484.jpg"],"add_time":"8天前"},{"id":"6","title":"高级制造的文章","desc":"基本制造内的内容基本制造内的内容基本制造内的内容基本制造内的内容基本制造内的内容基本制造内的内容...","imgArr":["http://park.hostop.net/upload/20180816/8085d87ae42fa0ec15bc0a8f79ff4484.jpg","http://park.hostop.net/upload/20180816/c3e609af9e6c578b7d9f2a72d0a7f531.jpg","http://park.hostop.net/upload/20180816/8085d87ae42fa0ec15bc0a8f79ff4484.jpg"],"add_time":"22天前"},{"id":"5","title":"基本制造内的内容","desc":"基本制造内的内容基本制造内的内容基本制造内的内容基本制造内的内容基本制造内的内容基本制造内的内容...","imgArr":["http://park.hostop.net/upload/20180816/8085d87ae42fa0ec15bc0a8f79ff4484.jpg"],"add_time":"24天前"}]
//         */
//
//        private InfoBean info;
//        private List<UpTjArrBean> upTjArr;
//        private List<BannerArrBean> bannerArr;
//        private List<WordArrBean> wordArr;
//        private List<CategoryBean> category;
//        private List<TjIndexBean> tjIndex;
//
//        public InfoBean getInfo() {
//            return info;
//        }
//
//        public void setInfo(InfoBean info) {
//            this.info = info;
//        }
//
//        public List<UpTjArrBean> getUpTjArr() {
//            return upTjArr;
//        }
//
//        public void setUpTjArr(List<UpTjArrBean> upTjArr) {
//            this.upTjArr = upTjArr;
//        }
//
//        public List<BannerArrBean> getBannerArr() {
//            return bannerArr;
//        }
//
//        public void setBannerArr(List<BannerArrBean> bannerArr) {
//            this.bannerArr = bannerArr;
//        }
//
//        public List<WordArrBean> getWordArr() {
//            return wordArr;
//        }
//
//        public void setWordArr(List<WordArrBean> wordArr) {
//            this.wordArr = wordArr;
//        }
//
//        public List<CategoryBean> getCategory() {
//            return category;
//        }
//
//        public void setCategory(List<CategoryBean> category) {
//            this.category = category;
//        }
//
//        public List<TjIndexBean> getTjIndex() {
//            return tjIndex;
//        }
//
//        public void setTjIndex(List<TjIndexBean> tjIndex) {
//            this.tjIndex = tjIndex;
//        }
//
//        public static class InfoBean {
//            /**
//             * content : <p>企服中心详情介绍一下</p><p><img src="http://park.hostop.net/upload/20180829/e67d6d8685520bb89f7c3dd2e61e030e.png" alt="undefined" style="width:50%"></p>
//             */
//
//            private String content;
//
//            public String getContent() {
//                return content;
//            }
//
//            public void setContent(String content) {
//                this.content = content;
//            }
//        }
//
//        public static class UpTjArrBean {
//            /**
//             * id : 8
//             * title :
//             * image : http://park.hostop.net/upload/20180906/1f5b2c9ae44040f2c49a083fd32a517f.jpg
//             * add_time : 12小时前
//             * hrefType : 0
//             * url : ?id=1
//             * content :
//             */
//
//            private String id;
//            private String title;
//            private String image;
//            private String add_time;
//            private String hrefType;
//            private String url;
//            private String content;
//
//            public String getContent() {
//                return content;
//            }
//
//            public void setContent(String content) {
//                this.content = content;
//            }
//
//            public String getId() {
//                return id;
//            }
//
//            public void setId(String id) {
//                this.id = id;
//            }
//
//            public String getTitle() {
//                return title;
//            }
//
//            public void setTitle(String title) {
//                this.title = title;
//            }
//
//            public String getImage() {
//                return image;
//            }
//
//            public void setImage(String image) {
//                this.image = image;
//            }
//
//            public String getAdd_time() {
//                return add_time;
//            }
//
//            public void setAdd_time(String add_time) {
//                this.add_time = add_time;
//            }
//
//            public String getHrefType() {
//                return hrefType;
//            }
//
//            public void setHrefType(String hrefType) {
//                this.hrefType = hrefType;
//            }
//
//            public String getUrl() {
//                return url;
//            }
//
//            public void setUrl(String url) {
//                this.url = url;
//            }
//        }
//
//        public static class BannerArrBean {
//            /**
//             * id : 6
//             * title :
//             * image : http://park.hostop.net/upload/20180906/3c20de4965161f35609ccdaf1fbee65f.jpg
//             * add_time : 12小时前
//             * hrefType : 1
//             * url : ?id=5
//             */
//
//            private String id;
//            private String title;
//            private String image;
//            private String add_time;
//            private String hrefType;
//            private String url;
//
//            public String getId() {
//                return id;
//            }
//
//            public void setId(String id) {
//                this.id = id;
//            }
//
//            public String getTitle() {
//                return title;
//            }
//
//            public void setTitle(String title) {
//                this.title = title;
//            }
//
//            public String getImage() {
//                return image;
//            }
//
//            public void setImage(String image) {
//                this.image = image;
//            }
//
//            public String getAdd_time() {
//                return add_time;
//            }
//
//            public void setAdd_time(String add_time) {
//                this.add_time = add_time;
//            }
//
//            public String getHrefType() {
//                return hrefType;
//            }
//
//            public void setHrefType(String hrefType) {
//                this.hrefType = hrefType;
//            }
//
//            public String getUrl() {
//                return url;
//            }
//
//            public void setUrl(String url) {
//                this.url = url;
//            }
//        }
//
//        public static class WordArrBean {
//            /**
//             * id : 6
//             * title :
//             * image : http://park.hostop.net/upload/20180906/3c20de4965161f35609ccdaf1fbee65f.jpg
//             * add_time : 12小时前
//             * hrefType : 1
//             * url : ?id=5
//             * content :
//             */
//
//            private String id;
//            private String title;
//            private String image;
//            private String add_time;
//            private String hrefType;
//            private String url;
//            private String content;
//
//
//            public String getContent() {
//                return content;
//            }
//
//            public void setContent(String content) {
//                this.content = content;
//            }
//
//            public String getId() {
//                return id;
//            }
//
//            public void setId(String id) {
//                this.id = id;
//            }
//
//            public String getTitle() {
//                return title;
//            }
//
//            public void setTitle(String title) {
//                this.title = title;
//            }
//
//            public String getImage() {
//                return image;
//            }
//
//            public void setImage(String image) {
//                this.image = image;
//            }
//
//            public String getAdd_time() {
//                return add_time;
//            }
//
//            public void setAdd_time(String add_time) {
//                this.add_time = add_time;
//            }
//
//            public String getHrefType() {
//                return hrefType;
//            }
//
//            public void setHrefType(String hrefType) {
//                this.hrefType = hrefType;
//            }
//
//            public String getUrl() {
//                return url;
//            }
//
//            public void setUrl(String url) {
//                this.url = url;
//            }
//        }
//
//        public static class CategoryBean {
//            /**
//             * id : 6
//             * title :
//             * image : http://park.hostop.net/upload/20180906/3c20de4965161f35609ccdaf1fbee65f.jpg
//             * add_time : 12小时前
//             * hrefType : 1
//             * logo : http://park.hostop.net/upload/20180910/235301500e050f394c70fb590abde155.jpg
//             * url : ?id=5
//             */
//
//            private String id;
//            private String title;
//            private String image;
//            private String add_time;
//            private String hrefType;
//            private String url;
//            private String logo;
//
//            public String getLogo() {
//                return logo;
//            }
//
//            public void setLogo(String logo) {
//                this.logo = logo;
//            }
//
//            public String getId() {
//                return id;
//            }
//
//            public void setId(String id) {
//                this.id = id;
//            }
//
//            public String getTitle() {
//                return title;
//            }
//
//            public void setTitle(String title) {
//                this.title = title;
//            }
//
//            public String getImage() {
//                return image;
//            }
//
//            public void setImage(String image) {
//                this.image = image;
//            }
//
//            public String getAdd_time() {
//                return add_time;
//            }
//
//            public void setAdd_time(String add_time) {
//                this.add_time = add_time;
//            }
//
//            public String getHrefType() {
//                return hrefType;
//            }
//
//            public void setHrefType(String hrefType) {
//                this.hrefType = hrefType;
//            }
//
//            public String getUrl() {
//                return url;
//            }
//
//            public void setUrl(String url) {
//                this.url = url;
//            }
//        }
//
//        public static class TjIndexBean implements MultiItemEntity{
//            /**
//             * id : 5
//             * title : 企业生活
//             * logo :
//             * imgArr : []
//             */
//
//            private String id;
//            private String title;
//            private String logo;
//            private List<?> imgArr;
//
//            private String desc;
//            private String add_time;
//
//            public String getDesc() {
//                return desc;
//            }
//
//            public void setDesc(String desc) {
//                this.desc = desc;
//            }
//
//            public String getAdd_time() {
//                return add_time;
//            }
//
//            public void setAdd_time(String add_time) {
//                this.add_time = add_time;
//            }
//
//            public static final int LING = 0;
//            public static final int ONE = 1;
//            public static final int TWO = 2;
//            public static final int THREE = 3;
//            public static final int SEX = 6;
//            public static final int NINE = 9;
//
//
//            private int itemType;
//
//            public void setItemType(int itemType) {
//                this.itemType = itemType;
//            }
//
//            @Override
//            public int getItemType() {
//                return itemType;
//            }

//
//
//            public String getId() {
//                return id;
//            }
//
//            public void setId(String id) {
//                this.id = id;
//            }
//
//            public String getTitle() {
//                return title;
//            }
//
//            public void setTitle(String title) {
//                this.title = title;
//            }
//
//            public String getLogo() {
//                return logo;
//            }
//
//            public void setLogo(String logo) {
//                this.logo = logo;
//            }
//
//            public List<?> getImgArr() {
//                return imgArr;
//            }
//
//            public void setImgArr(List<?> imgArr) {
//                this.imgArr = imgArr;
//            }
//        }
//    }
//
//    public static class RequestBean {
//        /**
//         * parkId : 1
//         * typeId : 2
//         */
//
//        private String parkId;
//        private String typeId;
//
//        public String getParkId() {
//            return parkId;
//        }
//
//        public void setParkId(String parkId) {
//            this.parkId = parkId;
//        }
//
//        public String getTypeId() {
//            return typeId;
//        }
//
//        public void setTypeId(String typeId) {
//            this.typeId = typeId;
//        }
//    }
//
//
























}
