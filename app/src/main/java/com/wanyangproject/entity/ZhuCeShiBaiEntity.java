package com.wanyangproject.entity;

import java.util.List;

/**
 * Created by 甜甜 on 2018/9/29.
 */

public class ZhuCeShiBaiEntity {

    /**
     * code : 400
     * msg : 验证码填写错误
     * response : []
     * request : {"phone":"17600902222","password":"123456","repwd":"123456","code":"123456","authId":"217"}
     * other : []
     */

    private int code;
    private String msg;
    private RequestBean request;
    private List<?> response;
    private List<?> other;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public RequestBean getRequest() {
        return request;
    }

    public void setRequest(RequestBean request) {
        this.request = request;
    }

    public List<?> getResponse() {
        return response;
    }

    public void setResponse(List<?> response) {
        this.response = response;
    }

    public List<?> getOther() {
        return other;
    }

    public void setOther(List<?> other) {
        this.other = other;
    }

    public static class RequestBean {
        /**
         * phone : 17600902222
         * password : 123456
         * repwd : 123456
         * code : 123456
         * authId : 217
         */

        private String phone;
        private String password;
        private String repwd;
        private String code;
        private String authId;

        public String getPhone() {
            return phone;
        }

        public void setPhone(String phone) {
            this.phone = phone;
        }

        public String getPassword() {
            return password;
        }

        public void setPassword(String password) {
            this.password = password;
        }

        public String getRepwd() {
            return repwd;
        }

        public void setRepwd(String repwd) {
            this.repwd = repwd;
        }

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        public String getAuthId() {
            return authId;
        }

        public void setAuthId(String authId) {
            this.authId = authId;
        }
    }
}
