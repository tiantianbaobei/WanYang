package com.wanyangproject.entity;

import java.util.List;

/**
 * Created by 甜甜 on 2018/9/19.
 */

public class MenWeiShuiDianXinXiEntity {
    /**
     * code : 200
     * msg :
     * response : {"roomName":"C29#201","data":[{"waterUserId":"5070","waterUserName":"30幢170","balance":"0"}],"electricityUserId":"532","electricityUserName":"29幢108","dian":{"electricityUserId":"532","electricityUserName":"29幢108","balance":"0"}}
     * request : []
     * other : []
     */

    private int code;
    private String msg;
    private ResponseBean response;
    private List<?> request;
    private List<?> other;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public ResponseBean getResponse() {
        return response;
    }

    public void setResponse(ResponseBean response) {
        this.response = response;
    }

    public List<?> getRequest() {
        return request;
    }

    public void setRequest(List<?> request) {
        this.request = request;
    }

    public List<?> getOther() {
        return other;
    }

    public void setOther(List<?> other) {
        this.other = other;
    }

    public static class ResponseBean {
        /**
         * roomName : C29#201
         * data : [{"waterUserId":"5070","waterUserName":"30幢170","balance":"0"}]
         * electricityUserId : 532
         * electricityUserName : 29幢108
         * dian : {"electricityUserId":"532","electricityUserName":"29幢108","balance":"0"}
         */

        private String roomName;
        private String electricityUserId;
        private String electricityUserName;
        private DianBean dian;
        private List<DataBean> data;

        public String getRoomName() {
            return roomName;
        }

        public void setRoomName(String roomName) {
            this.roomName = roomName;
        }

        public String getElectricityUserId() {
            return electricityUserId;
        }

        public void setElectricityUserId(String electricityUserId) {
            this.electricityUserId = electricityUserId;
        }

        public String getElectricityUserName() {
            return electricityUserName;
        }

        public void setElectricityUserName(String electricityUserName) {
            this.electricityUserName = electricityUserName;
        }

        public DianBean getDian() {
            return dian;
        }

        public void setDian(DianBean dian) {
            this.dian = dian;
        }

        public List<DataBean> getData() {
            return data;
        }

        public void setData(List<DataBean> data) {
            this.data = data;
        }

        public static class DianBean {
            /**
             * electricityUserId : 532
             * electricityUserName : 29幢108
             * balance : 0
             */

            private String electricityUserId;
            private String electricityUserName;
            private String balance;

            public String getElectricityUserId() {
                return electricityUserId;
            }

            public void setElectricityUserId(String electricityUserId) {
                this.electricityUserId = electricityUserId;
            }

            public String getElectricityUserName() {
                return electricityUserName;
            }

            public void setElectricityUserName(String electricityUserName) {
                this.electricityUserName = electricityUserName;
            }

            public String getBalance() {
                return balance;
            }

            public void setBalance(String balance) {
                this.balance = balance;
            }
        }

        public static class DataBean {
            /**
             * waterUserId : 5070
             * waterUserName : 30幢170
             * balance : 0
             */

            private String waterUserId;
            private String waterUserName;
            private String balance;

            public String getWaterUserId() {
                return waterUserId;
            }

            public void setWaterUserId(String waterUserId) {
                this.waterUserId = waterUserId;
            }

            public String getWaterUserName() {
                return waterUserName;
            }

            public void setWaterUserName(String waterUserName) {
                this.waterUserName = waterUserName;
            }

            public String getBalance() {
                return balance;
            }

            public void setBalance(String balance) {
                this.balance = balance;
            }
        }
    }


//    /**
//     * code : 200
//     * msg :
//     * response : {"roomName":"F07#宿舍楼-1003","data":[{"waterUserId":"673","waterUserName":"F07地块10F-1003冷         ","balance":"-105"},{"waterUserId":"716","waterUserName":"F07地块10F-1003热         ","balance":"-60"}]}
//     * request : []
//     * other : []
//     */
//
//    private int code;
//    private String msg;
//    private ResponseBean response;
//    private List<?> request;
//    private List<?> other;
//
//    public int getCode() {
//        return code;
//    }
//
//    public void setCode(int code) {
//        this.code = code;
//    }
//
//    public String getMsg() {
//        return msg;
//    }
//
//    public void setMsg(String msg) {
//        this.msg = msg;
//    }
//
//    public ResponseBean getResponse() {
//        return response;
//    }
//
//    public void setResponse(ResponseBean response) {
//        this.response = response;
//    }
//
//    public List<?> getRequest() {
//        return request;
//    }
//
//    public void setRequest(List<?> request) {
//        this.request = request;
//    }
//
//    public List<?> getOther() {
//        return other;
//    }
//
//    public void setOther(List<?> other) {
//        this.other = other;
//    }
//
//    public static class ResponseBean {
//        /**
//         * roomName : F07#宿舍楼-1003
//         * data : [{"waterUserId":"673","waterUserName":"F07地块10F-1003冷         ","balance":"-105"},{"waterUserId":"716","waterUserName":"F07地块10F-1003热         ","balance":"-60"}]
//         */
//
//        private String roomName;
//        private List<DataBean> data;
//
//        public String getRoomName() {
//            return roomName;
//        }
//
//        public void setRoomName(String roomName) {
//            this.roomName = roomName;
//        }
//
//        public List<DataBean> getData() {
//            return data;
//        }
//
//        public void setData(List<DataBean> data) {
//            this.data = data;
//        }
//
//        public static class DataBean {
//            /**
//             * waterUserId : 673
//             * waterUserName : F07地块10F-1003冷
//             * balance : -105
//             */
//
//            private String waterUserId;
//            private String waterUserName;
//            private String balance;
//
//            public String getWaterUserId() {
//                return waterUserId;
//            }
//
//            public void setWaterUserId(String waterUserId) {
//                this.waterUserId = waterUserId;
//            }
//
//            public String getWaterUserName() {
//                return waterUserName;
//            }
//
//            public void setWaterUserName(String waterUserName) {
//                this.waterUserName = waterUserName;
//            }
//
//            public String getBalance() {
//                return balance;
//            }
//
//            public void setBalance(String balance) {
//                this.balance = balance;
//            }
//        }
//    }
}
