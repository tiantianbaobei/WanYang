package com.wanyangproject.entity;

import java.util.List;

/**
 * Created by 甜甜 on 2018/2/4.
 */

public class ShengShiQuEntity {

    /**
     * code : 200
     * explain : [{"ID":"7460","PARENT_ID":"7459","you_id":"110000","REGION_PARENT_ID":"1","REGION_NAME":"北京","REGION_TYPE":"1","ZIPCODE":null,"QUHAO":null,"Status":null},{"ID":"7481","PARENT_ID":"7459","you_id":"120000","REGION_PARENT_ID":"1","REGION_NAME":"天津","REGION_TYPE":"1","ZIPCODE":null,"QUHAO":null,"Status":null},{"ID":"7503","PARENT_ID":"7459","you_id":"130000","REGION_PARENT_ID":"1","REGION_NAME":"河北省","REGION_TYPE":"1","ZIPCODE":null,"QUHAO":null,"Status":null},{"ID":"7702","PARENT_ID":"7459","you_id":"140000","REGION_PARENT_ID":"1","REGION_NAME":"山西省","REGION_TYPE":"1","ZIPCODE":null,"QUHAO":null,"Status":null},{"ID":"7845","PARENT_ID":"7459","you_id":"150000","REGION_PARENT_ID":"1","REGION_NAME":"内蒙古自治区","REGION_TYPE":"1","ZIPCODE":null,"QUHAO":null,"Status":null},{"ID":"7971","PARENT_ID":"7459","you_id":"210000","REGION_PARENT_ID":"1","REGION_NAME":"辽宁省","REGION_TYPE":"1","ZIPCODE":null,"QUHAO":null,"Status":null},{"ID":"8106","PARENT_ID":"7459","you_id":"220000","REGION_PARENT_ID":"1","REGION_NAME":"吉林省","REGION_TYPE":"1","ZIPCODE":null,"QUHAO":null,"Status":null},{"ID":"8189","PARENT_ID":"7459","you_id":"230000","REGION_PARENT_ID":"1","REGION_NAME":"黑龙江省","REGION_TYPE":"1","ZIPCODE":null,"QUHAO":null,"Status":null},{"ID":"8348","PARENT_ID":"7459","you_id":"310000","REGION_PARENT_ID":"1","REGION_NAME":"上海","REGION_TYPE":"1","ZIPCODE":null,"QUHAO":null,"Status":null},{"ID":"8371","PARENT_ID":"7459","you_id":"320000","REGION_PARENT_ID":"1","REGION_NAME":"江苏省","REGION_TYPE":"1","ZIPCODE":null,"QUHAO":null,"Status":null},{"ID":"8510","PARENT_ID":"7459","you_id":"330000","REGION_PARENT_ID":"1","REGION_NAME":"浙江省","REGION_TYPE":"1","ZIPCODE":null,"QUHAO":null,"Status":null},{"ID":"8623","PARENT_ID":"7459","you_id":"340000","REGION_PARENT_ID":"1","REGION_NAME":"安徽省","REGION_TYPE":"1","ZIPCODE":null,"QUHAO":null,"Status":null},{"ID":"8765","PARENT_ID":"7459","you_id":"350000","REGION_PARENT_ID":"1","REGION_NAME":"福建省","REGION_TYPE":"1","ZIPCODE":null,"QUHAO":null,"Status":null},{"ID":"8869","PARENT_ID":"7459","you_id":"360000","REGION_PARENT_ID":"1","REGION_NAME":"江西省","REGION_TYPE":"1","ZIPCODE":null,"QUHAO":null,"Status":null},{"ID":"8995","PARENT_ID":"7459","you_id":"370000","REGION_PARENT_ID":"1","REGION_NAME":"山东省","REGION_TYPE":"1","ZIPCODE":null,"QUHAO":null,"Status":null},{"ID":"9175","PARENT_ID":"7459","you_id":"410000","REGION_PARENT_ID":"1","REGION_NAME":"河南省","REGION_TYPE":"1","ZIPCODE":null,"QUHAO":null,"Status":null},{"ID":"9370","PARENT_ID":"7459","you_id":"420000","REGION_PARENT_ID":"1","REGION_NAME":"湖北省","REGION_TYPE":"1","ZIPCODE":null,"QUHAO":null,"Status":null},{"ID":"9504","PARENT_ID":"7459","you_id":"430000","REGION_PARENT_ID":"1","REGION_NAME":"湖南省","REGION_TYPE":"1","ZIPCODE":null,"QUHAO":null,"Status":null},{"ID":"9655","PARENT_ID":"7459","you_id":"440000","REGION_PARENT_ID":"1","REGION_NAME":"广东省","REGION_TYPE":"1","ZIPCODE":null,"QUHAO":null,"Status":null},{"ID":"9822","PARENT_ID":"7459","you_id":"450000","REGION_PARENT_ID":"1","REGION_NAME":"广西壮族自治区","REGION_TYPE":"1","ZIPCODE":null,"QUHAO":null,"Status":null},{"ID":"9960","PARENT_ID":"7459","you_id":"460000","REGION_PARENT_ID":"1","REGION_NAME":"海南省","REGION_TYPE":"1","ZIPCODE":null,"QUHAO":null,"Status":null},{"ID":"9989","PARENT_ID":"7459","you_id":"500000","REGION_PARENT_ID":"1","REGION_NAME":"重庆","REGION_TYPE":"1","ZIPCODE":null,"QUHAO":null,"Status":null},{"ID":"10032","PARENT_ID":"7459","you_id":"510000","REGION_PARENT_ID":"1","REGION_NAME":"四川省","REGION_TYPE":"1","ZIPCODE":null,"QUHAO":null,"Status":null},{"ID":"10258","PARENT_ID":"7459","you_id":"520000","REGION_PARENT_ID":"1","REGION_NAME":"贵州省","REGION_TYPE":"1","ZIPCODE":null,"QUHAO":null,"Status":null},{"ID":"10366","PARENT_ID":"7459","you_id":"530000","REGION_PARENT_ID":"1","REGION_NAME":"云南省","REGION_TYPE":"1","ZIPCODE":null,"QUHAO":null,"Status":null},{"ID":"10528","PARENT_ID":"7459","you_id":"540000","REGION_PARENT_ID":"1","REGION_NAME":"西藏自治区","REGION_TYPE":"1","ZIPCODE":null,"QUHAO":null,"Status":null},{"ID":"10616","PARENT_ID":"7459","you_id":"610000","REGION_PARENT_ID":"1","REGION_NAME":"陕西省","REGION_TYPE":"1","ZIPCODE":null,"QUHAO":null,"Status":null},{"ID":"10744","PARENT_ID":"7459","you_id":"620000","REGION_PARENT_ID":"1","REGION_NAME":"甘肃省","REGION_TYPE":"1","ZIPCODE":null,"QUHAO":null,"Status":null},{"ID":"10858","PARENT_ID":"7459","you_id":"630000","REGION_PARENT_ID":"1","REGION_NAME":"青海省","REGION_TYPE":"1","ZIPCODE":null,"QUHAO":null,"Status":null},{"ID":"10918","PARENT_ID":"7459","you_id":"640000","REGION_PARENT_ID":"1","REGION_NAME":"宁夏回族自治区","REGION_TYPE":"1","ZIPCODE":null,"QUHAO":null,"Status":null},{"ID":"10951","PARENT_ID":"7459","you_id":"650000","REGION_PARENT_ID":"1","REGION_NAME":"新疆维吾尔自治区","REGION_TYPE":"1","ZIPCODE":null,"QUHAO":null,"Status":null},{"ID":"11080","PARENT_ID":"7459","you_id":"710000","REGION_PARENT_ID":"1","REGION_NAME":"台湾省","REGION_TYPE":"1","ZIPCODE":null,"QUHAO":null,"Status":null},{"ID":"11161","PARENT_ID":"7459","you_id":"810000","REGION_PARENT_ID":"1","REGION_NAME":"香港特别行政区","REGION_TYPE":"1","ZIPCODE":null,"QUHAO":null,"Status":null},{"ID":"11183","PARENT_ID":"7459","you_id":"820000","REGION_PARENT_ID":"1","REGION_NAME":"澳门特别行政区","REGION_TYPE":"1","ZIPCODE":null,"QUHAO":null,"Status":null}]
     */

    private String code;
    private List<ExplainBean> explain;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public List<ExplainBean> getExplain() {
        return explain;
    }

    public void setExplain(List<ExplainBean> explain) {
        this.explain = explain;
    }

    public static class ExplainBean {
        /**
         * ID : 7460
         * PARENT_ID : 7459
         * you_id : 110000
         * REGION_PARENT_ID : 1
         * REGION_NAME : 北京
         * REGION_TYPE : 1
         * ZIPCODE : null
         * QUHAO : null
         * Status : null
         */

        private String ID;
        private String PARENT_ID;
        private String you_id;
        private String REGION_PARENT_ID;
        private String REGION_NAME;
        private String REGION_TYPE;
        private Object ZIPCODE;
        private Object QUHAO;
        private Object Status;

        public String getID() {
            return ID;
        }

        public void setID(String ID) {
            this.ID = ID;
        }

        public String getPARENT_ID() {
            return PARENT_ID;
        }

        public void setPARENT_ID(String PARENT_ID) {
            this.PARENT_ID = PARENT_ID;
        }

        public String getYou_id() {
            return you_id;
        }

        public void setYou_id(String you_id) {
            this.you_id = you_id;
        }

        public String getREGION_PARENT_ID() {
            return REGION_PARENT_ID;
        }

        public void setREGION_PARENT_ID(String REGION_PARENT_ID) {
            this.REGION_PARENT_ID = REGION_PARENT_ID;
        }

        public String getREGION_NAME() {
            return REGION_NAME;
        }

        public void setREGION_NAME(String REGION_NAME) {
            this.REGION_NAME = REGION_NAME;
        }

        public String getREGION_TYPE() {
            return REGION_TYPE;
        }

        public void setREGION_TYPE(String REGION_TYPE) {
            this.REGION_TYPE = REGION_TYPE;
        }

        public Object getZIPCODE() {
            return ZIPCODE;
        }

        public void setZIPCODE(Object ZIPCODE) {
            this.ZIPCODE = ZIPCODE;
        }

        public Object getQUHAO() {
            return QUHAO;
        }

        public void setQUHAO(Object QUHAO) {
            this.QUHAO = QUHAO;
        }

        public Object getStatus() {
            return Status;
        }

        public void setStatus(Object Status) {
            this.Status = Status;
        }
    }
}
