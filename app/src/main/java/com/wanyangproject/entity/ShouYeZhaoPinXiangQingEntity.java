package com.wanyangproject.entity;

import java.util.List;

/**
 * Created by 甜甜 on 2018/8/22.
 */

public class ShouYeZhaoPinXiangQingEntity {
    /**
     * code : 200
     * msg :
     * response : {"data":{"id":"46","title":"测试921","money":"","yaoqiu":"测试要求","address":"测试地址","fuli":"测试福利","phone":"测试手机号","content":"123","seeNums":"5","add_time":"2天前","isLike":"0"},"enterInfo":{"info":{"enterId":"632","enterName":"浦江县张氏锁厂","parkName":"金华众创城","legalPerson":"张美娟","contact":"13868911275","logoUrl":"http://park.hostop.net/upload/static/logo.png","intro":"","lat":"","lon":"","desc":"","address":"","guimo":"","hangye":"","lng":""},"recruit":[]}}
     * request : {"id":"46"}
     * other : []
     */

    private int code;
    private String msg;
    private ResponseBean response;
    private RequestBean request;
    private List<?> other;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public ResponseBean getResponse() {
        return response;
    }

    public void setResponse(ResponseBean response) {
        this.response = response;
    }

    public RequestBean getRequest() {
        return request;
    }

    public void setRequest(RequestBean request) {
        this.request = request;
    }

    public List<?> getOther() {
        return other;
    }

    public void setOther(List<?> other) {
        this.other = other;
    }

    public static class ResponseBean {
        /**
         * data : {"id":"46","title":"测试921","money":"","yaoqiu":"测试要求","address":"测试地址","fuli":"测试福利","phone":"测试手机号","content":"123","seeNums":"5","add_time":"2天前","isLike":"0"}
         * enterInfo : {"info":{"enterId":"632","enterName":"浦江县张氏锁厂","parkName":"金华众创城","legalPerson":"张美娟","contact":"13868911275","logoUrl":"http://park.hostop.net/upload/static/logo.png","intro":"","lat":"","lon":"","desc":"","address":"","guimo":"","hangye":"","lng":""},"recruit":[]}
         */

        private DataBean data;
        private EnterInfoBean enterInfo;

        public DataBean getData() {
            return data;
        }

        public void setData(DataBean data) {
            this.data = data;
        }

        public EnterInfoBean getEnterInfo() {
            return enterInfo;
        }

        public void setEnterInfo(EnterInfoBean enterInfo) {
            this.enterInfo = enterInfo;
        }

        public static class DataBean {
            /**
             * id : 46
             * title : 测试921
             * money :
             * yaoqiu : 测试要求
             * address : 测试地址
             * fuli : 测试福利
             * phone : 测试手机号
             * content : 123
             * seeNums : 5
             * add_time : 2天前
             * isLike : 0
             */

            private String id;
            private String title;
            private String money;
            private String yaoqiu;
            private String address;
            private String fuli;
            private String phone;
            private String content;
            private String seeNums;
            private String add_time;
            private String isLike;

            public String getId() {
                return id;
            }

            public void setId(String id) {
                this.id = id;
            }

            public String getTitle() {
                return title;
            }

            public void setTitle(String title) {
                this.title = title;
            }

            public String getMoney() {
                return money;
            }

            public void setMoney(String money) {
                this.money = money;
            }

            public String getYaoqiu() {
                return yaoqiu;
            }

            public void setYaoqiu(String yaoqiu) {
                this.yaoqiu = yaoqiu;
            }

            public String getAddress() {
                return address;
            }

            public void setAddress(String address) {
                this.address = address;
            }

            public String getFuli() {
                return fuli;
            }

            public void setFuli(String fuli) {
                this.fuli = fuli;
            }

            public String getPhone() {
                return phone;
            }

            public void setPhone(String phone) {
                this.phone = phone;
            }

            public String getContent() {
                return content;
            }

            public void setContent(String content) {
                this.content = content;
            }

            public String getSeeNums() {
                return seeNums;
            }

            public void setSeeNums(String seeNums) {
                this.seeNums = seeNums;
            }

            public String getAdd_time() {
                return add_time;
            }

            public void setAdd_time(String add_time) {
                this.add_time = add_time;
            }

            public String getIsLike() {
                return isLike;
            }

            public void setIsLike(String isLike) {
                this.isLike = isLike;
            }
        }

        public static class EnterInfoBean {
            /**
             * info : {"enterId":"632","enterName":"浦江县张氏锁厂","parkName":"金华众创城","legalPerson":"张美娟","contact":"13868911275","logoUrl":"http://park.hostop.net/upload/static/logo.png","intro":"","lat":"","lon":"","desc":"","address":"","guimo":"","hangye":"","lng":""}
             * recruit : []
             */

            private InfoBean info;
            private List<?> recruit;

            public InfoBean getInfo() {
                return info;
            }

            public void setInfo(InfoBean info) {
                this.info = info;
            }

            public List<?> getRecruit() {
                return recruit;
            }

            public void setRecruit(List<?> recruit) {
                this.recruit = recruit;
            }

            public static class InfoBean {
                /**
                 * enterId : 632
                 * enterName : 浦江县张氏锁厂
                 * parkName : 金华众创城
                 * legalPerson : 张美娟
                 * contact : 13868911275
                 * logoUrl : http://park.hostop.net/upload/static/logo.png
                 * intro :
                 * lat :
                 * lon :
                 * desc :
                 * address :
                 * guimo :
                 * hangye :
                 * lng :
                 */

                private String enterId;
                private String enterName;
                private String parkName;
                private String legalPerson;
                private String contact;
                private String logoUrl;
                private String intro;
                private String lat;
                private String lon;
                private String desc;
                private String address;
                private String guimo;
                private String hangye;
                private String lng;

                public String getEnterId() {
                    return enterId;
                }

                public void setEnterId(String enterId) {
                    this.enterId = enterId;
                }

                public String getEnterName() {
                    return enterName;
                }

                public void setEnterName(String enterName) {
                    this.enterName = enterName;
                }

                public String getParkName() {
                    return parkName;
                }

                public void setParkName(String parkName) {
                    this.parkName = parkName;
                }

                public String getLegalPerson() {
                    return legalPerson;
                }

                public void setLegalPerson(String legalPerson) {
                    this.legalPerson = legalPerson;
                }

                public String getContact() {
                    return contact;
                }

                public void setContact(String contact) {
                    this.contact = contact;
                }

                public String getLogoUrl() {
                    return logoUrl;
                }

                public void setLogoUrl(String logoUrl) {
                    this.logoUrl = logoUrl;
                }

                public String getIntro() {
                    return intro;
                }

                public void setIntro(String intro) {
                    this.intro = intro;
                }

                public String getLat() {
                    return lat;
                }

                public void setLat(String lat) {
                    this.lat = lat;
                }

                public String getLon() {
                    return lon;
                }

                public void setLon(String lon) {
                    this.lon = lon;
                }

                public String getDesc() {
                    return desc;
                }

                public void setDesc(String desc) {
                    this.desc = desc;
                }

                public String getAddress() {
                    return address;
                }

                public void setAddress(String address) {
                    this.address = address;
                }

                public String getGuimo() {
                    return guimo;
                }

                public void setGuimo(String guimo) {
                    this.guimo = guimo;
                }

                public String getHangye() {
                    return hangye;
                }

                public void setHangye(String hangye) {
                    this.hangye = hangye;
                }

                public String getLng() {
                    return lng;
                }

                public void setLng(String lng) {
                    this.lng = lng;
                }
            }
        }
    }

    public static class RequestBean {
        /**
         * id : 46
         */

        private String id;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }
    }


//    /**
//     * code : 200
//     * msg :
//     * response : {"data":{"id":"23","title":"服务员","money":"薪资面议","yaoqiu":"大专以上、无不良嗜好","address":"济南市槐荫区匡山汽车大世界","fuli":"五险一金 周末双休","phone":"17615809705","content":"公司旗下的\u201c家居建材新零售互助平台\u201d，是重庆建材行业首个互联网+建材的电商平台。互联网+共享经济是平台的核心竞争力，凭借对传统建材和互联网的深刻理解，公司领先业界的产品理念和先进的互联网思维相结合。联合建材行业诸多一线建材品牌，强强联手，共享共赢。为建材行业提供更方便、快捷、安全、实用、多维的一站式服务体系。公司站在时代的前沿,坚持\u201c永不停步、与时俱进、不断创新\u201d的经营理念，从独享经济到共享经济，形成以互联网为基础的新零售生态圈，提升公司在新零售领域的竞争力，致力于成为新零售领军企业，并成为建材行业最具影响力和投资价值的创新型企业。","add_time":"17小时前"},"enterInfo":{"info":{"enterId":"44","enterName":"温州嘉创印务有限公司","parkName":"温州平阳众创城","legalPerson":"蒋义乐","contact":"13606774055","logoUrl":"http://park.hostop.net/upload/static/logo.png","intro":"","lat":"","lon":"","desc":"简介内容，当前未测试数据","address":"测试的位置信息","guimo":"30人","hangye":"娱乐休闲/餐饮/服务"},"recruit":[]}}
//     * request : {"id":"23"}
//     * other : []
//     */
//
//    private int code;
//    private String msg;
//    private ResponseBean response;
//    private RequestBean request;
//    private List<?> other;
//
//    public int getCode() {
//        return code;
//    }
//
//    public void setCode(int code) {
//        this.code = code;
//    }
//
//    public String getMsg() {
//        return msg;
//    }
//
//    public void setMsg(String msg) {
//        this.msg = msg;
//    }
//
//    public ResponseBean getResponse() {
//        return response;
//    }
//
//    public void setResponse(ResponseBean response) {
//        this.response = response;
//    }
//
//    public RequestBean getRequest() {
//        return request;
//    }
//
//    public void setRequest(RequestBean request) {
//        this.request = request;
//    }
//
//    public List<?> getOther() {
//        return other;
//    }
//
//    public void setOther(List<?> other) {
//        this.other = other;
//    }
//
//    public static class ResponseBean {
//        /**
//         * data : {"id":"23","title":"服务员","money":"薪资面议","yaoqiu":"大专以上、无不良嗜好","address":"济南市槐荫区匡山汽车大世界","fuli":"五险一金 周末双休","phone":"17615809705","content":"公司旗下的\u201c家居建材新零售互助平台\u201d，是重庆建材行业首个互联网+建材的电商平台。互联网+共享经济是平台的核心竞争力，凭借对传统建材和互联网的深刻理解，公司领先业界的产品理念和先进的互联网思维相结合。联合建材行业诸多一线建材品牌，强强联手，共享共赢。为建材行业提供更方便、快捷、安全、实用、多维的一站式服务体系。公司站在时代的前沿,坚持\u201c永不停步、与时俱进、不断创新\u201d的经营理念，从独享经济到共享经济，形成以互联网为基础的新零售生态圈，提升公司在新零售领域的竞争力，致力于成为新零售领军企业，并成为建材行业最具影响力和投资价值的创新型企业。","add_time":"17小时前"}
//         * enterInfo : {"info":{"enterId":"44","enterName":"温州嘉创印务有限公司","parkName":"温州平阳众创城","legalPerson":"蒋义乐","contact":"13606774055","logoUrl":"http://park.hostop.net/upload/static/logo.png","intro":"","lat":"","lon":"","desc":"简介内容，当前未测试数据","address":"测试的位置信息","guimo":"30人","hangye":"娱乐休闲/餐饮/服务"},"recruit":[]}
//         */
//
//        private DataBean data;
//        private EnterInfoBean enterInfo;
//
//        public DataBean getData() {
//            return data;
//        }
//
//        public void setData(DataBean data) {
//            this.data = data;
//        }
//
//        public EnterInfoBean getEnterInfo() {
//            return enterInfo;
//        }
//
//        public void setEnterInfo(EnterInfoBean enterInfo) {
//            this.enterInfo = enterInfo;
//        }
//
//        public static class DataBean {
//            /**
//             * id : 23
//             * title : 服务员
//             * money : 薪资面议
//             * yaoqiu : 大专以上、无不良嗜好
//             * address : 济南市槐荫区匡山汽车大世界
//             * fuli : 五险一金 周末双休
//             * phone : 17615809705
//             * content : 公司旗下的“家居建材新零售互助平台”，是重庆建材行业首个互联网+建材的电商平台。互联网+共享经济是平台的核心竞争力，凭借对传统建材和互联网的深刻理解，公司领先业界的产品理念和先进的互联网思维相结合。联合建材行业诸多一线建材品牌，强强联手，共享共赢。为建材行业提供更方便、快捷、安全、实用、多维的一站式服务体系。公司站在时代的前沿,坚持“永不停步、与时俱进、不断创新”的经营理念，从独享经济到共享经济，形成以互联网为基础的新零售生态圈，提升公司在新零售领域的竞争力，致力于成为新零售领军企业，并成为建材行业最具影响力和投资价值的创新型企业。
//             * add_time : 17小时前
//             */
//
//            private String id;
//            private String title;
//            private String money;
//            private String yaoqiu;
//            private String address;
//            private String fuli;
//            private String phone;
//            private String content;
//            private String add_time;
//
//            public String getId() {
//                return id;
//            }
//
//            public void setId(String id) {
//                this.id = id;
//            }
//
//            public String getTitle() {
//                return title;
//            }
//
//            public void setTitle(String title) {
//                this.title = title;
//            }
//
//            public String getMoney() {
//                return money;
//            }
//
//            public void setMoney(String money) {
//                this.money = money;
//            }
//
//            public String getYaoqiu() {
//                return yaoqiu;
//            }
//
//            public void setYaoqiu(String yaoqiu) {
//                this.yaoqiu = yaoqiu;
//            }
//
//            public String getAddress() {
//                return address;
//            }
//
//            public void setAddress(String address) {
//                this.address = address;
//            }
//
//            public String getFuli() {
//                return fuli;
//            }
//
//            public void setFuli(String fuli) {
//                this.fuli = fuli;
//            }
//
//            public String getPhone() {
//                return phone;
//            }
//
//            public void setPhone(String phone) {
//                this.phone = phone;
//            }
//
//            public String getContent() {
//                return content;
//            }
//
//            public void setContent(String content) {
//                this.content = content;
//            }
//
//            public String getAdd_time() {
//                return add_time;
//            }
//
//            public void setAdd_time(String add_time) {
//                this.add_time = add_time;
//            }
//        }
//
//        public static class EnterInfoBean {
//            /**
//             * info : {"enterId":"44","enterName":"温州嘉创印务有限公司","parkName":"温州平阳众创城","legalPerson":"蒋义乐","contact":"13606774055","logoUrl":"http://park.hostop.net/upload/static/logo.png","intro":"","lat":"","lon":"","desc":"简介内容，当前未测试数据","address":"测试的位置信息","guimo":"30人","hangye":"娱乐休闲/餐饮/服务"}
//             * recruit : []
//             */
//
//            private InfoBean info;
//            private List<?> recruit;
//
//            public InfoBean getInfo() {
//                return info;
//            }
//
//            public void setInfo(InfoBean info) {
//                this.info = info;
//            }
//
//            public List<?> getRecruit() {
//                return recruit;
//            }
//
//            public void setRecruit(List<?> recruit) {
//                this.recruit = recruit;
//            }
//
//            public static class InfoBean {
//                /**
//                 * enterId : 44
//                 * enterName : 温州嘉创印务有限公司
//                 * parkName : 温州平阳众创城
//                 * legalPerson : 蒋义乐
//                 * contact : 13606774055
//                 * logoUrl : http://park.hostop.net/upload/static/logo.png
//                 * intro :
//                 * lat :
//                 * lon :
//                 * desc : 简介内容，当前未测试数据
//                 * address : 测试的位置信息
//                 * guimo : 30人
//                 * hangye : 娱乐休闲/餐饮/服务
//                 */
//
//                private String enterId;
//                private String enterName;
//                private String parkName;
//                private String legalPerson;
//                private String contact;
//                private String logoUrl;
//                private String intro;
//                private String lat;
//                private String lon;
//                private String desc;
//                private String address;
//                private String guimo;
//                private String hangye;
//
//                public String getEnterId() {
//                    return enterId;
//                }
//
//                public void setEnterId(String enterId) {
//                    this.enterId = enterId;
//                }
//
//                public String getEnterName() {
//                    return enterName;
//                }
//
//                public void setEnterName(String enterName) {
//                    this.enterName = enterName;
//                }
//
//                public String getParkName() {
//                    return parkName;
//                }
//
//                public void setParkName(String parkName) {
//                    this.parkName = parkName;
//                }
//
//                public String getLegalPerson() {
//                    return legalPerson;
//                }
//
//                public void setLegalPerson(String legalPerson) {
//                    this.legalPerson = legalPerson;
//                }
//
//                public String getContact() {
//                    return contact;
//                }
//
//                public void setContact(String contact) {
//                    this.contact = contact;
//                }
//
//                public String getLogoUrl() {
//                    return logoUrl;
//                }
//
//                public void setLogoUrl(String logoUrl) {
//                    this.logoUrl = logoUrl;
//                }
//
//                public String getIntro() {
//                    return intro;
//                }
//
//                public void setIntro(String intro) {
//                    this.intro = intro;
//                }
//
//                public String getLat() {
//                    return lat;
//                }
//
//                public void setLat(String lat) {
//                    this.lat = lat;
//                }
//
//                public String getLon() {
//                    return lon;
//                }
//
//                public void setLon(String lon) {
//                    this.lon = lon;
//                }
//
//                public String getDesc() {
//                    return desc;
//                }
//
//                public void setDesc(String desc) {
//                    this.desc = desc;
//                }
//
//                public String getAddress() {
//                    return address;
//                }
//
//                public void setAddress(String address) {
//                    this.address = address;
//                }
//
//                public String getGuimo() {
//                    return guimo;
//                }
//
//                public void setGuimo(String guimo) {
//                    this.guimo = guimo;
//                }
//
//                public String getHangye() {
//                    return hangye;
//                }
//
//                public void setHangye(String hangye) {
//                    this.hangye = hangye;
//                }
//            }
//        }
//    }
//
//    public static class RequestBean {
//        /**
//         * id : 23
//         */
//
//        private String id;
//
//        public String getId() {
//            return id;
//        }
//
//        public void setId(String id) {
//            this.id = id;
//        }
//    }
}
