package com.wanyangproject.entity;

import java.util.List;

/**
 * Created by 甜甜 on 2018/9/15.
 */

public class FangKeSaoYiSaoEntity {


    /**
     * code : 200
     * msg :
     * response : {"id":"236","parkId":"1","phone":"15011718199","nums":"2","name":"jolly","daodaTime":"2018-09-15 15:36","chepai":"","add_time":"2018-09-15 15:36","add_ip":"124.133.211.40","special":"26ef38b2caa3af1ee379bd99ef064bcd6","photo":"upload/qrcode/2018-09-15/1e08440b8d6141e6914a837b6234d999b1.png","visit_time":"","visit_nums":"","visit_userid":"","type":"2","userid":"2","status":"1"}
     * request : {"parkId":"1","code":"26ef38b2caa3af1ee379bd99ef064bcd6","phone":"15275411070"}
     * other : []
     */

    private int code;
    private String msg;
    private ResponseBean response;
    private RequestBean request;
    private List<?> other;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public ResponseBean getResponse() {
        return response;
    }

    public void setResponse(ResponseBean response) {
        this.response = response;
    }

    public RequestBean getRequest() {
        return request;
    }

    public void setRequest(RequestBean request) {
        this.request = request;
    }

    public List<?> getOther() {
        return other;
    }

    public void setOther(List<?> other) {
        this.other = other;
    }

    public static class ResponseBean {
        /**
         * id : 236
         * parkId : 1
         * phone : 15011718199
         * nums : 2
         * name : jolly
         * daodaTime : 2018-09-15 15:36
         * chepai :
         * add_time : 2018-09-15 15:36
         * add_ip : 124.133.211.40
         * special : 26ef38b2caa3af1ee379bd99ef064bcd6
         * photo : upload/qrcode/2018-09-15/1e08440b8d6141e6914a837b6234d999b1.png
         * visit_time :
         * visit_nums :
         * visit_userid :
         * type : 2
         * userid : 2
         * status : 1
         */

        private String id;
        private String parkId;
        private String phone;
        private String nums;
        private String name;
        private String daodaTime;
        private String chepai;
        private String add_time;
        private String add_ip;
        private String special;
        private String photo;
        private String visit_time;
        private String visit_nums;
        private String visit_userid;
        private String type;
        private String userid;
        private String status;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getParkId() {
            return parkId;
        }

        public void setParkId(String parkId) {
            this.parkId = parkId;
        }

        public String getPhone() {
            return phone;
        }

        public void setPhone(String phone) {
            this.phone = phone;
        }

        public String getNums() {
            return nums;
        }

        public void setNums(String nums) {
            this.nums = nums;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getDaodaTime() {
            return daodaTime;
        }

        public void setDaodaTime(String daodaTime) {
            this.daodaTime = daodaTime;
        }

        public String getChepai() {
            return chepai;
        }

        public void setChepai(String chepai) {
            this.chepai = chepai;
        }

        public String getAdd_time() {
            return add_time;
        }

        public void setAdd_time(String add_time) {
            this.add_time = add_time;
        }

        public String getAdd_ip() {
            return add_ip;
        }

        public void setAdd_ip(String add_ip) {
            this.add_ip = add_ip;
        }

        public String getSpecial() {
            return special;
        }

        public void setSpecial(String special) {
            this.special = special;
        }

        public String getPhoto() {
            return photo;
        }

        public void setPhoto(String photo) {
            this.photo = photo;
        }

        public String getVisit_time() {
            return visit_time;
        }

        public void setVisit_time(String visit_time) {
            this.visit_time = visit_time;
        }

        public String getVisit_nums() {
            return visit_nums;
        }

        public void setVisit_nums(String visit_nums) {
            this.visit_nums = visit_nums;
        }

        public String getVisit_userid() {
            return visit_userid;
        }

        public void setVisit_userid(String visit_userid) {
            this.visit_userid = visit_userid;
        }

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public String getUserid() {
            return userid;
        }

        public void setUserid(String userid) {
            this.userid = userid;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }
    }

    public static class RequestBean {
        /**
         * parkId : 1
         * code : 26ef38b2caa3af1ee379bd99ef064bcd6
         * phone : 15275411070
         */

        private String parkId;
        private String code;
        private String phone;

        public String getParkId() {
            return parkId;
        }

        public void setParkId(String parkId) {
            this.parkId = parkId;
        }

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        public String getPhone() {
            return phone;
        }

        public void setPhone(String phone) {
            this.phone = phone;
        }
    }
}
