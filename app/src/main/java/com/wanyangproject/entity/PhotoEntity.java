package com.wanyangproject.entity;

import java.util.List;

/**
 * Created by 甜甜 on 2018/8/13.
 */

public class PhotoEntity {
    /**
     * code : 200
     * msg :
     * response : {"fileurl":"http://park.hostop.net/upload/20180915/65b56538c8b1cea29af25560e4ee8b0f.jpg"}
     * request : []
     * other : []
     */

    private int code;
    private String msg;
    private ResponseBean response;
    private List<?> request;
    private List<?> other;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public ResponseBean getResponse() {
        return response;
    }

    public void setResponse(ResponseBean response) {
        this.response = response;
    }

    public List<?> getRequest() {
        return request;
    }

    public void setRequest(List<?> request) {
        this.request = request;
    }

    public List<?> getOther() {
        return other;
    }

    public void setOther(List<?> other) {
        this.other = other;
    }

    public static class ResponseBean {
        /**
         * fileurl : http://park.hostop.net/upload/20180915/65b56538c8b1cea29af25560e4ee8b0f.jpg
         */

        private String fileurl;

        public String getFileurl() {
            return fileurl;
        }

        public void setFileurl(String fileurl) {
            this.fileurl = fileurl;
        }
    }


//
//    /**
//     * code : 200
//     * msg :
//     * response : {"fileurl":"http://park.hostop.net//upload/20180813/f2fbc9162ba7e567c987bfb0ccfbee45.jpg"}
//     * request : []
//     * other : []
//     */
//
//    private int code;
//    private String msg;
//    private ResponseBean response;
//    private List<?> request;
//    private List<?> other;
//    @SerializedName("request")
//    private List<?> requestX;
//    @SerializedName("other")
//    private List<?> otherX;
//
//    public int getCode() {
//        return code;
//    }
//
//    public void setCode(int code) {
//        this.code = code;
//    }
//
//    public String getMsg() {
//        return msg;
//    }
//
//    public void setMsg(String msg) {
//        this.msg = msg;
//    }
//
//    public ResponseBean getResponse() {
//        return response;
//    }
//
//    public void setResponse(ResponseBean response) {
//        this.response = response;
//    }
//
//    public List<?> getRequest() {
//        return request;
//    }
//
//    public void setRequest(List<?> request) {
//        this.request = request;
//    }
//
//    public List<?> getOther() {
//        return other;
//    }
//
//    public void setOther(List<?> other) {
//        this.other = other;
//    }
//
//    public List<?> getRequestX() {
//        return requestX;
//    }
//
//    public void setRequestX(List<?> requestX) {
//        this.requestX = requestX;
//    }
//
//    public List<?> getOtherX() {
//        return otherX;
//    }
//
//    public void setOtherX(List<?> otherX) {
//        this.otherX = otherX;
//    }
//
//    public static class ResponseBean {
//        /**
//         * fileurl : http://park.hostop.net//upload/20180813/f2fbc9162ba7e567c987bfb0ccfbee45.jpg
//         */
//
//        private String fileurl;
//
//        public String getFileurl() {
//            return fileurl;
//        }
//
//        public void setFileurl(String fileurl) {
//            this.fileurl = fileurl;
//        }
//    }
}
