package com.wanyangproject.entity;

import java.util.List;

/**
 * Created by 甜甜 on 2018/9/15.
 */

public class FangKeGuanLiShaChuShiBaiEntity {

    /**
     * code : 400
     * msg : 暂时不可以删除！
     * response : []
     * request : {"id":"233"}
     * other : []
     */

    private int code;
    private String msg;
    private RequestBean request;
    private List<?> response;
    private List<?> other;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public RequestBean getRequest() {
        return request;
    }

    public void setRequest(RequestBean request) {
        this.request = request;
    }

    public List<?> getResponse() {
        return response;
    }

    public void setResponse(List<?> response) {
        this.response = response;
    }

    public List<?> getOther() {
        return other;
    }

    public void setOther(List<?> other) {
        this.other = other;
    }

    public static class RequestBean {
        /**
         * id : 233
         */

        private String id;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }
    }
}
