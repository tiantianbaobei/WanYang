package com.wanyangproject.entity;

import java.util.List;

/**
 * Created by 甜甜 on 2018/8/27.
 */

public class XiTongXiaoXiWeiDuGeShuEntity {

    /**
     * code : 200
     * msg :
     * response : {"ge":"0"}
     * request : {"parkId":"1"}
     * other : []
     */

    private int code;
    private String msg;
    private ResponseBean response;
    private RequestBean request;
    private List<?> other;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public ResponseBean getResponse() {
        return response;
    }

    public void setResponse(ResponseBean response) {
        this.response = response;
    }

    public RequestBean getRequest() {
        return request;
    }

    public void setRequest(RequestBean request) {
        this.request = request;
    }

    public List<?> getOther() {
        return other;
    }

    public void setOther(List<?> other) {
        this.other = other;
    }

    public static class ResponseBean {
        /**
         * ge : 0
         */

        private String ge;

        public String getGe() {
            return ge;
        }

        public void setGe(String ge) {
            this.ge = ge;
        }
    }

    public static class RequestBean {
        /**
         * parkId : 1
         */

        private String parkId;

        public String getParkId() {
            return parkId;
        }

        public void setParkId(String parkId) {
            this.parkId = parkId;
        }
    }
}
