package com.wanyangproject.entity;

import java.util.List;

/**
 * Created by 甜甜 on 2018/8/26.
 */

public class FangKeDengJiXinXiEntity {

    /**
     * code : 200
     * msg :
     * response : {"content":"专业电路安装维修二、厨房卫生间下水管道马桶除臭\u2014专利下水管道防溢器\u2014\u2014彻底解决马桶管道溢水、下水管道返臭、地漏返臭等困扰卫生间的臭气异味困扰，还你一个清洁的健康的家庭。三、水管龙头管道维修：水管、水龙头、水管、水龙头、三角阀、花洒套培装、洗脸盆、淋浴房、卫浴镜、浴缸、五金挂件等安装维修。四、管道疏通加清洗：洗菜池、洗脸池下水管疏通、马桶、地漏疏通，疏通后较重要的是要管道清洗，保持干净联系我时，请说是在58同城看到的，谢谢！"}
     * request : []
     * other : []
     */

    private int code;
    private String msg;
    private ResponseBean response;
    private List<?> request;
    private List<?> other;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public ResponseBean getResponse() {
        return response;
    }

    public void setResponse(ResponseBean response) {
        this.response = response;
    }

    public List<?> getRequest() {
        return request;
    }

    public void setRequest(List<?> request) {
        this.request = request;
    }

    public List<?> getOther() {
        return other;
    }

    public void setOther(List<?> other) {
        this.other = other;
    }

    public static class ResponseBean {
        /**
         * content : 专业电路安装维修二、厨房卫生间下水管道马桶除臭—专利下水管道防溢器——彻底解决马桶管道溢水、下水管道返臭、地漏返臭等困扰卫生间的臭气异味困扰，还你一个清洁的健康的家庭。三、水管龙头管道维修：水管、水龙头、水管、水龙头、三角阀、花洒套培装、洗脸盆、淋浴房、卫浴镜、浴缸、五金挂件等安装维修。四、管道疏通加清洗：洗菜池、洗脸池下水管疏通、马桶、地漏疏通，疏通后较重要的是要管道清洗，保持干净联系我时，请说是在58同城看到的，谢谢！
         */

        private String content;

        public String getContent() {
            return content;
        }

        public void setContent(String content) {
            this.content = content;
        }
    }
}
