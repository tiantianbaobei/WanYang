package com.wanyangproject.entity;

import java.util.List;

/**
 * Created by 甜甜 on 2018/9/20.
 */

public class FaTieShiBaiEntity {

    /**
     * code : 400
     * msg : 您发表的帖子里面因为包含【传销】发布失败，请重新整体后再提交
     * response : []
     * request : {"parkId":"14","typeId":"13","content":"传销"}
     * other : []
     */

    private int code;
    private String msg;
    private RequestBean request;
    private List<?> response;
    private List<?> other;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public RequestBean getRequest() {
        return request;
    }

    public void setRequest(RequestBean request) {
        this.request = request;
    }

    public List<?> getResponse() {
        return response;
    }

    public void setResponse(List<?> response) {
        this.response = response;
    }

    public List<?> getOther() {
        return other;
    }

    public void setOther(List<?> other) {
        this.other = other;
    }

    public static class RequestBean {
        /**
         * parkId : 14
         * typeId : 13
         * content : 传销
         */

        private String parkId;
        private String typeId;
        private String content;

        public String getParkId() {
            return parkId;
        }

        public void setParkId(String parkId) {
            this.parkId = parkId;
        }

        public String getTypeId() {
            return typeId;
        }

        public void setTypeId(String typeId) {
            this.typeId = typeId;
        }

        public String getContent() {
            return content;
        }

        public void setContent(String content) {
            this.content = content;
        }
    }
}
