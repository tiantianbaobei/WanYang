package com.wanyangproject.entity;

import java.util.List;

/**
 * Created by 甜甜 on 2018/8/14.
 */

public class LunTanDianZanEntity {

    /**
     * code : 200
     * msg :
     * response : [{"id":"13","uinfo":{"userid":"2","avatar":"http://park.hostop.net/static/image/avatar.jpg","nickname":"未填写"}}]
     * request : {"forumId":"13"}
     * other : []
     */

    private int code;
    private String msg;
    private RequestBean request;
    private List<ResponseBean> response;
    private List<?> other;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public RequestBean getRequest() {
        return request;
    }

    public void setRequest(RequestBean request) {
        this.request = request;
    }

    public List<ResponseBean> getResponse() {
        return response;
    }

    public void setResponse(List<ResponseBean> response) {
        this.response = response;
    }

    public List<?> getOther() {
        return other;
    }

    public void setOther(List<?> other) {
        this.other = other;
    }

    public static class RequestBean {
        /**
         * forumId : 13
         */

        private String forumId;

        public String getForumId() {
            return forumId;
        }

        public void setForumId(String forumId) {
            this.forumId = forumId;
        }
    }

    public static class ResponseBean {
        /**
         * id : 13
         * uinfo : {"userid":"2","avatar":"http://park.hostop.net/static/image/avatar.jpg","nickname":"未填写"}
         */

        private String id;
        private UinfoBean uinfo;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public UinfoBean getUinfo() {
            return uinfo;
        }

        public void setUinfo(UinfoBean uinfo) {
            this.uinfo = uinfo;
        }

        public static class UinfoBean {
            /**
             * userid : 2
             * avatar : http://park.hostop.net/static/image/avatar.jpg
             * nickname : 未填写
             */

            private String userid;
            private String avatar;
            private String nickname;

            public String getUserid() {
                return userid;
            }

            public void setUserid(String userid) {
                this.userid = userid;
            }

            public String getAvatar() {
                return avatar;
            }

            public void setAvatar(String avatar) {
                this.avatar = avatar;
            }

            public String getNickname() {
                return nickname;
            }

            public void setNickname(String nickname) {
                this.nickname = nickname;
            }
        }
    }
}
