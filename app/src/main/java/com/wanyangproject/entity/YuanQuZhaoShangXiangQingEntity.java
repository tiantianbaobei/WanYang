package com.wanyangproject.entity;

import java.util.List;

/**
 * Created by 甜甜 on 2018/8/19.
 */

public class YuanQuZhaoShangXiangQingEntity {

    /**
     * code : 200
     * msg :
     * response : {"id":"22","title":"顶部推荐1","content":"顶部推荐1详情","imgArr":["http://park.hostop.net/upload/20180819/fd536d81467d35465845f86bb7550988.jpg","http://park.hostop.net/upload/20180819/c3bf07992f1024beae5ff82b6cd9d552.jpg","http://park.hostop.net/upload/20180819/798b92c1833b033fdcbb339c3976245c.jpg"],"add_time":"9小时前"}
     * request : {"id":"22"}
     * other : []
     */

    private int code;
    private String msg;
    private ResponseBean response;
    private RequestBean request;
    private List<?> other;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public ResponseBean getResponse() {
        return response;
    }

    public void setResponse(ResponseBean response) {
        this.response = response;
    }

    public RequestBean getRequest() {
        return request;
    }

    public void setRequest(RequestBean request) {
        this.request = request;
    }

    public List<?> getOther() {
        return other;
    }

    public void setOther(List<?> other) {
        this.other = other;
    }

    public static class ResponseBean {
        /**
         * id : 22
         * title : 顶部推荐1
         * content : 顶部推荐1详情
         * imgArr : ["http://park.hostop.net/upload/20180819/fd536d81467d35465845f86bb7550988.jpg","http://park.hostop.net/upload/20180819/c3bf07992f1024beae5ff82b6cd9d552.jpg","http://park.hostop.net/upload/20180819/798b92c1833b033fdcbb339c3976245c.jpg"]
         * add_time : 9小时前
         */

        private String id;
        private String title;
        private String content;
        private String add_time;
        private List<String> imgArr;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getContent() {
            return content;
        }

        public void setContent(String content) {
            this.content = content;
        }

        public String getAdd_time() {
            return add_time;
        }

        public void setAdd_time(String add_time) {
            this.add_time = add_time;
        }

        public List<String> getImgArr() {
            return imgArr;
        }

        public void setImgArr(List<String> imgArr) {
            this.imgArr = imgArr;
        }
    }

    public static class RequestBean {
        /**
         * id : 22
         */

        private String id;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }
    }
}
