package com.wanyangproject.entity;

import java.util.List;

/**
 * Created by 甜甜 on 2018/8/24.
 */

public class PingJiaEntity {

    /**
     * code : 200
     * msg :
     * response : {"mes":"评价成功！"}
     * request : {"parkId":"1","order_sn":"pk2sn20180818110322","Distribution":"3","service":"3","content":"测试"}
     * other : []
     */

    private int code;
    private String msg;
    private ResponseBean response;
    private RequestBean request;
    private List<?> other;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public ResponseBean getResponse() {
        return response;
    }

    public void setResponse(ResponseBean response) {
        this.response = response;
    }

    public RequestBean getRequest() {
        return request;
    }

    public void setRequest(RequestBean request) {
        this.request = request;
    }

    public List<?> getOther() {
        return other;
    }

    public void setOther(List<?> other) {
        this.other = other;
    }

    public static class ResponseBean {
        /**
         * mes : 评价成功！
         */

        private String mes;

        public String getMes() {
            return mes;
        }

        public void setMes(String mes) {
            this.mes = mes;
        }
    }

    public static class RequestBean {
        /**
         * parkId : 1
         * order_sn : pk2sn20180818110322
         * Distribution : 3
         * service : 3
         * content : 测试
         */

        private String parkId;
        private String order_sn;
        private String Distribution;
        private String service;
        private String content;

        public String getParkId() {
            return parkId;
        }

        public void setParkId(String parkId) {
            this.parkId = parkId;
        }

        public String getOrder_sn() {
            return order_sn;
        }

        public void setOrder_sn(String order_sn) {
            this.order_sn = order_sn;
        }

        public String getDistribution() {
            return Distribution;
        }

        public void setDistribution(String Distribution) {
            this.Distribution = Distribution;
        }

        public String getService() {
            return service;
        }

        public void setService(String service) {
            this.service = service;
        }

        public String getContent() {
            return content;
        }

        public void setContent(String content) {
            this.content = content;
        }
    }
}
