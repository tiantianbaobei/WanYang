package com.wanyangproject.entity;

import java.util.List;

/**
 * Created by 甜甜 on 2018/8/21.
 */

public class QiuZhiZhaoPinTitleEntity {

    /**
     * code : 200
     * msg :
     * response : [{"id":"0","title":"推荐"},{"id":"4","title":"客服"},{"id":"3","title":"销售"},{"id":"2","title":"餐饮"},{"id":"1","title":"服务"}]
     * request : []
     * other : []
     */

    private int code;
    private String msg;
    private List<ResponseBean> response;
    private List<?> request;
    private List<?> other;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public List<ResponseBean> getResponse() {
        return response;
    }

    public void setResponse(List<ResponseBean> response) {
        this.response = response;
    }

    public List<?> getRequest() {
        return request;
    }

    public void setRequest(List<?> request) {
        this.request = request;
    }

    public List<?> getOther() {
        return other;
    }

    public void setOther(List<?> other) {
        this.other = other;
    }

    public static class ResponseBean {
        /**
         * id : 0
         * title : 推荐
         */

        private String id;
        private String title;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }
    }
}
