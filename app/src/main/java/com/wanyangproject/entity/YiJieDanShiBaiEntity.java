package com.wanyangproject.entity;

import java.util.List;

/**
 * Created by 甜甜 on 2018/9/11.
 */

public class YiJieDanShiBaiEntity {

    /**
     * code : 400
     * msg : 暂无订单！
     * response : []
     * request : {"parkId":"1","phone":"17600904682"}
     * other : []
     */

    private int code;
    private String msg;
    private RequestBean request;
    private List<?> response;
    private List<?> other;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public RequestBean getRequest() {
        return request;
    }

    public void setRequest(RequestBean request) {
        this.request = request;
    }

    public List<?> getResponse() {
        return response;
    }

    public void setResponse(List<?> response) {
        this.response = response;
    }

    public List<?> getOther() {
        return other;
    }

    public void setOther(List<?> other) {
        this.other = other;
    }

    public static class RequestBean {
        /**
         * parkId : 1
         * phone : 17600904682
         */

        private String parkId;
        private String phone;

        public String getParkId() {
            return parkId;
        }

        public void setParkId(String parkId) {
            this.parkId = parkId;
        }

        public String getPhone() {
            return phone;
        }

        public void setPhone(String phone) {
            this.phone = phone;
        }
    }
}
