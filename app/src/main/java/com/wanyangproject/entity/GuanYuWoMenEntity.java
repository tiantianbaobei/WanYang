package com.wanyangproject.entity;

import java.util.List;

/**
 * Created by 甜甜 on 2018/9/19.
 */

public class GuanYuWoMenEntity {

    /**
     * code : 200
     * msg :
     * response : {"content":"<p><img src=\"http://park.hostop.net/upload/20180919/f8cd5bfb6ab6fd233f0e949469007266.png\" alt=\"undefined\" style=\"width:50%\"><br><\/p><p>园区为实现智能化资源整合，现已建立实施园区免费上网服务。400余无线WI-FI热点设备覆盖整个园区，每个终端用户可以享受4mb宽带畅享网络。无线WI-FI环境的建立不仅可以满足园区内企业及员工休闲使用，同时还可以方便外来客快速连接，并下载园区APP享受园区内所有服务。&nbsp;<\/p><p>地址：北京市海淀区中关村软件园&nbsp;<\/p><p>电话：010-237656<\/p><p>邮箱：wanyang@xxx.com&nbsp;<\/p>"}
     * request : []
     * other : []
     */

    private int code;
    private String msg;
    private ResponseBean response;
    private List<?> request;
    private List<?> other;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public ResponseBean getResponse() {
        return response;
    }

    public void setResponse(ResponseBean response) {
        this.response = response;
    }

    public List<?> getRequest() {
        return request;
    }

    public void setRequest(List<?> request) {
        this.request = request;
    }

    public List<?> getOther() {
        return other;
    }

    public void setOther(List<?> other) {
        this.other = other;
    }

    public static class ResponseBean {
        /**
         * content : <p><img src="http://park.hostop.net/upload/20180919/f8cd5bfb6ab6fd233f0e949469007266.png" alt="undefined" style="width:50%"><br></p><p>园区为实现智能化资源整合，现已建立实施园区免费上网服务。400余无线WI-FI热点设备覆盖整个园区，每个终端用户可以享受4mb宽带畅享网络。无线WI-FI环境的建立不仅可以满足园区内企业及员工休闲使用，同时还可以方便外来客快速连接，并下载园区APP享受园区内所有服务。&nbsp;</p><p>地址：北京市海淀区中关村软件园&nbsp;</p><p>电话：010-237656</p><p>邮箱：wanyang@xxx.com&nbsp;</p>
         */

        private String content;

        public String getContent() {
            return content;
        }

        public void setContent(String content) {
            this.content = content;
        }
    }
}
