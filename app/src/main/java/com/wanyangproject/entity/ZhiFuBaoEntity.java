package com.wanyangproject.entity;

import java.util.List;

/**
 * Created by 甜甜 on 2018/8/28.
 */

public class ZhiFuBaoEntity {

    /**
     * code : 200
     * msg :
     * response : {"mes":"alipay_sdk=alipay-sdk-php-20180705&amp;app_id=2018081761082719&amp;biz_content={"}
     * request : []
     * other : []
     */

    private int code;
    private String msg;
    private ResponseBean response;
    private List<?> request;
    private List<?> other;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public ResponseBean getResponse() {
        return response;
    }

    public void setResponse(ResponseBean response) {
        this.response = response;
    }

    public List<?> getRequest() {
        return request;
    }

    public void setRequest(List<?> request) {
        this.request = request;
    }

    public List<?> getOther() {
        return other;
    }

    public void setOther(List<?> other) {
        this.other = other;
    }

    public static class ResponseBean {
        /**
         * mes : alipay_sdk=alipay-sdk-php-20180705&amp;app_id=2018081761082719&amp;biz_content={
         */

        private String mes;

        public String getMes() {
            return mes;
        }

        public void setMes(String mes) {
            this.mes = mes;
        }
    }
}
