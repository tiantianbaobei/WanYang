package com.wanyangproject.entity;

import java.util.List;

/**
 * Created by 甜甜 on 2018/8/17.
 */

public class QiYeFuWuShenQingEntity {

    /**
     * code : 200
     * msg :
     * response : {"meage":"申请成功！"}
     * request : {"parkId":"1","Preferences_id":"1","money":"30","nember":"5","permanent":"2","use_time":"2018-09-02 - 2018-09-02","Remarks":"没","name":"哈哈","phone":"15988563555"}
     * other : []
     */

    private int code;
    private String msg;
    private ResponseBean response;
    private RequestBean request;
    private List<?> other;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public ResponseBean getResponse() {
        return response;
    }

    public void setResponse(ResponseBean response) {
        this.response = response;
    }

    public RequestBean getRequest() {
        return request;
    }

    public void setRequest(RequestBean request) {
        this.request = request;
    }

    public List<?> getOther() {
        return other;
    }

    public void setOther(List<?> other) {
        this.other = other;
    }

    public static class ResponseBean {
        /**
         * meage : 申请成功！
         */

        private String meage;

        public String getMeage() {
            return meage;
        }

        public void setMeage(String meage) {
            this.meage = meage;
        }
    }

    public static class RequestBean {
        /**
         * parkId : 1
         * Preferences_id : 1
         * money : 30
         * nember : 5
         * permanent : 2
         * use_time : 2018-09-02 - 2018-09-02
         * Remarks : 没
         * name : 哈哈
         * phone : 15988563555
         */

        private String parkId;
        private String Preferences_id;
        private String money;
        private String nember;
        private String permanent;
        private String use_time;
        private String Remarks;
        private String name;
        private String phone;

        public String getParkId() {
            return parkId;
        }

        public void setParkId(String parkId) {
            this.parkId = parkId;
        }

        public String getPreferences_id() {
            return Preferences_id;
        }

        public void setPreferences_id(String Preferences_id) {
            this.Preferences_id = Preferences_id;
        }

        public String getMoney() {
            return money;
        }

        public void setMoney(String money) {
            this.money = money;
        }

        public String getNember() {
            return nember;
        }

        public void setNember(String nember) {
            this.nember = nember;
        }

        public String getPermanent() {
            return permanent;
        }

        public void setPermanent(String permanent) {
            this.permanent = permanent;
        }

        public String getUse_time() {
            return use_time;
        }

        public void setUse_time(String use_time) {
            this.use_time = use_time;
        }

        public String getRemarks() {
            return Remarks;
        }

        public void setRemarks(String Remarks) {
            this.Remarks = Remarks;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getPhone() {
            return phone;
        }

        public void setPhone(String phone) {
            this.phone = phone;
        }
    }
}
