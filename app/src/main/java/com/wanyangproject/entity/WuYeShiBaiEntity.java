package com.wanyangproject.entity;

import java.util.List;

/**
 * Created by 甜甜 on 2018/9/21.
 */

public class WuYeShiBaiEntity {

    /**
     * code : 400
     * msg : 抱歉，目前只有企业用户可以申请！
     * response : []
     * request : {"image":"http://park.hostop.net/upload/20180921/418696637618bd697c138526f40c9d0d.jpg","typeId":"1","fuwuId":"166","content":"测试","name":"测试","phone":"15275411070","fuwuName":"营业执照变更"}
     * other : []
     */

    private int code;
    private String msg;
    private RequestBean request;
    private List<?> response;
    private List<?> other;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public RequestBean getRequest() {
        return request;
    }

    public void setRequest(RequestBean request) {
        this.request = request;
    }

    public List<?> getResponse() {
        return response;
    }

    public void setResponse(List<?> response) {
        this.response = response;
    }

    public List<?> getOther() {
        return other;
    }

    public void setOther(List<?> other) {
        this.other = other;
    }

    public static class RequestBean {
        /**
         * image : http://park.hostop.net/upload/20180921/418696637618bd697c138526f40c9d0d.jpg
         * typeId : 1
         * fuwuId : 166
         * content : 测试
         * name : 测试
         * phone : 15275411070
         * fuwuName : 营业执照变更
         */

        private String image;
        private String typeId;
        private String fuwuId;
        private String content;
        private String name;
        private String phone;
        private String fuwuName;

        public String getImage() {
            return image;
        }

        public void setImage(String image) {
            this.image = image;
        }

        public String getTypeId() {
            return typeId;
        }

        public void setTypeId(String typeId) {
            this.typeId = typeId;
        }

        public String getFuwuId() {
            return fuwuId;
        }

        public void setFuwuId(String fuwuId) {
            this.fuwuId = fuwuId;
        }

        public String getContent() {
            return content;
        }

        public void setContent(String content) {
            this.content = content;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getPhone() {
            return phone;
        }

        public void setPhone(String phone) {
            this.phone = phone;
        }

        public String getFuwuName() {
            return fuwuName;
        }

        public void setFuwuName(String fuwuName) {
            this.fuwuName = fuwuName;
        }
    }
}
