package com.wanyangproject.entity;

import java.util.List;

/**
 * Created by 甜甜 on 2018/9/5.
 */

public class XiuGaiEntity {

    /**
     * code : 200
     * msg :
     * response : []
     * request : {"avatar":"http://park.hostop.net/upload/20180905/aebd0e397fa1cee0f6391fa656eedb1b.jpg","nickname":"哈哈哈","sex":"1","birth":"1441382400000"}
     * other : []
     */

    private int code;
    private String msg;
    private RequestBean request;
    private List<?> response;
    private List<?> other;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public RequestBean getRequest() {
        return request;
    }

    public void setRequest(RequestBean request) {
        this.request = request;
    }

    public List<?> getResponse() {
        return response;
    }

    public void setResponse(List<?> response) {
        this.response = response;
    }

    public List<?> getOther() {
        return other;
    }

    public void setOther(List<?> other) {
        this.other = other;
    }

    public static class RequestBean {
        /**
         * avatar : http://park.hostop.net/upload/20180905/aebd0e397fa1cee0f6391fa656eedb1b.jpg
         * nickname : 哈哈哈
         * sex : 1
         * birth : 1441382400000
         */

        private String avatar;
        private String nickname;
        private String sex;
        private String birth;

        public String getAvatar() {
            return avatar;
        }

        public void setAvatar(String avatar) {
            this.avatar = avatar;
        }

        public String getNickname() {
            return nickname;
        }

        public void setNickname(String nickname) {
            this.nickname = nickname;
        }

        public String getSex() {
            return sex;
        }

        public void setSex(String sex) {
            this.sex = sex;
        }

        public String getBirth() {
            return birth;
        }

        public void setBirth(String birth) {
            this.birth = birth;
        }
    }
}
