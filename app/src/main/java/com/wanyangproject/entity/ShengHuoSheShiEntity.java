package com.wanyangproject.entity;

import java.util.List;

/**
 * Created by 甜甜 on 2018/9/2.
 */

public class ShengHuoSheShiEntity {

    /**
     * code : 200
     * msg :
     * response : {"content":"商家用户详情介绍一下<img src=\"http://park.hostop.net/upload/20180829/e67d6d8685520bb89f7c3dd2e61e030e.png\" alt=\"undefined\" style=\"width:50%\">"}
     * request : []
     * other : []
     */

    private int code;
    private String msg;
    private ResponseBean response;
    private List<?> request;
    private List<?> other;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public ResponseBean getResponse() {
        return response;
    }

    public void setResponse(ResponseBean response) {
        this.response = response;
    }

    public List<?> getRequest() {
        return request;
    }

    public void setRequest(List<?> request) {
        this.request = request;
    }

    public List<?> getOther() {
        return other;
    }

    public void setOther(List<?> other) {
        this.other = other;
    }

    public static class ResponseBean {
        /**
         * content : 商家用户详情介绍一下<img src="http://park.hostop.net/upload/20180829/e67d6d8685520bb89f7c3dd2e61e030e.png" alt="undefined" style="width:50%">
         */

        private String content;

        public String getContent() {
            return content;
        }

        public void setContent(String content) {
            this.content = content;
        }
    }
}
