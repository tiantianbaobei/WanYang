package com.wanyangproject.entity;

import java.util.List;

/**
 * Created by 甜甜 on 2018/9/6.
 */

public class QiYeChangFangGuanLiEntity {
    /**
     * code : 200
     * msg :
     * response : {"code":"200","message":"OK","data":[{"workshopId":"81","workshopName":"驰骋制罐    F07#7-101","parcelName":"F-07","parkName":"温州平阳众创城"},{"workshopId":"82","workshopName":"驰骋制罐      F07#7-201","parcelName":"F-07","parkName":"温州平阳众创城"},{"workshopId":"83","workshopName":"驰骋制罐    F07#7-301","parcelName":"F-07","parkName":"温州平阳众创城"}]}
     * request : {"parkId":"1"}
     * other : []
     */

    private int code;
    private String msg;
    private ResponseBean response;
    private RequestBean request;
    private List<?> other;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public ResponseBean getResponse() {
        return response;
    }

    public void setResponse(ResponseBean response) {
        this.response = response;
    }

    public RequestBean getRequest() {
        return request;
    }

    public void setRequest(RequestBean request) {
        this.request = request;
    }

    public List<?> getOther() {
        return other;
    }

    public void setOther(List<?> other) {
        this.other = other;
    }

    public static class ResponseBean {
        /**
         * code : 200
         * message : OK
         * data : [{"workshopId":"81","workshopName":"驰骋制罐    F07#7-101","parcelName":"F-07","parkName":"温州平阳众创城"},{"workshopId":"82","workshopName":"驰骋制罐      F07#7-201","parcelName":"F-07","parkName":"温州平阳众创城"},{"workshopId":"83","workshopName":"驰骋制罐    F07#7-301","parcelName":"F-07","parkName":"温州平阳众创城"}]
         */

        private String code;
        private String message;
        private List<DataBean> data;

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        public List<DataBean> getData() {
            return data;
        }

        public void setData(List<DataBean> data) {
            this.data = data;
        }

        public static class DataBean {
            /**
             * workshopId : 81
             * workshopName : 驰骋制罐    F07#7-101
             * parcelName : F-07
             * parkName : 温州平阳众创城
             */

            private String workshopId;
            private String workshopName;
            private String parcelName;
            private String parkName;

            public String getWorkshopId() {
                return workshopId;
            }

            public void setWorkshopId(String workshopId) {
                this.workshopId = workshopId;
            }

            public String getWorkshopName() {
                return workshopName;
            }

            public void setWorkshopName(String workshopName) {
                this.workshopName = workshopName;
            }

            public String getParcelName() {
                return parcelName;
            }

            public void setParcelName(String parcelName) {
                this.parcelName = parcelName;
            }

            public String getParkName() {
                return parkName;
            }

            public void setParkName(String parkName) {
                this.parkName = parkName;
            }
        }
    }

    public static class RequestBean {
        /**
         * parkId : 1
         */

        private String parkId;

        public String getParkId() {
            return parkId;
        }

        public void setParkId(String parkId) {
            this.parkId = parkId;
        }
    }


//
//    /**
//     * code : 200
//     * msg :
//     * response : {"code":"200","message":"OK","data":[{"workshopId":"93","workshopName":"瑞伟F09-1#17-201","parkName":"温州平阳众创城"}]}
//     * request : {"parkId":"1"}
//     * other : []
//     */
//
//    private int code;
//    private String msg;
//    private ResponseBean response;
//    private RequestBean request;
//    private List<?> other;
//
//    public int getCode() {
//        return code;
//    }
//
//    public void setCode(int code) {
//        this.code = code;
//    }
//
//    public String getMsg() {
//        return msg;
//    }
//
//    public void setMsg(String msg) {
//        this.msg = msg;
//    }
//
//    public ResponseBean getResponse() {
//        return response;
//    }
//
//    public void setResponse(ResponseBean response) {
//        this.response = response;
//    }
//
//    public RequestBean getRequest() {
//        return request;
//    }
//
//    public void setRequest(RequestBean request) {
//        this.request = request;
//    }
//
//    public List<?> getOther() {
//        return other;
//    }
//
//    public void setOther(List<?> other) {
//        this.other = other;
//    }
//
//    public static class ResponseBean {
//        /**
//         * code : 200
//         * message : OK
//         * data : [{"workshopId":"93","workshopName":"瑞伟F09-1#17-201","parkName":"温州平阳众创城"}]
//         */
//
//        private String code;
//        private String message;
//        private List<DataBean> data;
//
//        public String getCode() {
//            return code;
//        }
//
//        public void setCode(String code) {
//            this.code = code;
//        }
//
//        public String getMessage() {
//            return message;
//        }
//
//        public void setMessage(String message) {
//            this.message = message;
//        }
//
//        public List<DataBean> getData() {
//            return data;
//        }
//
//        public void setData(List<DataBean> data) {
//            this.data = data;
//        }
//
//        public static class DataBean {
//            /**
//             * workshopId : 93
//             * workshopName : 瑞伟F09-1#17-201
//             * parkName : 温州平阳众创城
//             */
//
//            private String workshopId;
//            private String workshopName;
//            private String parkName;
//
//            public String getWorkshopId() {
//                return workshopId;
//            }
//
//            public void setWorkshopId(String workshopId) {
//                this.workshopId = workshopId;
//            }
//
//            public String getWorkshopName() {
//                return workshopName;
//            }
//
//            public void setWorkshopName(String workshopName) {
//                this.workshopName = workshopName;
//            }
//
//            public String getParkName() {
//                return parkName;
//            }
//
//            public void setParkName(String parkName) {
//                this.parkName = parkName;
//            }
//        }
//    }
//
//    public static class RequestBean {
//        /**
//         * parkId : 1
//         */
//
//        private String parkId;
//
//        public String getParkId() {
//            return parkId;
//        }
//
//        public void setParkId(String parkId) {
//            this.parkId = parkId;
//        }
//    }
}
