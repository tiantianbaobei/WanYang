package com.wanyangproject.entity;

import java.util.List;

/**
 * Created by 甜甜 on 2018/9/5.
 */

public class DuanXinDengLuEntity {

    /**
     * code : 200
     * msg :
     * response : {"userid":"4","username":"15275411070","avatar":"http://park.hostop.net/static/image/logo.png","nickname":"未填写","realname":"","uuid":"472B9D11-2FEC-4CBD-8379-BE51E5C02AD3","parkId":"0","parkInfo":{"parkId":"1","parkName":"温州平阳众创城"},"status":"1","token":"29a387623d8d2b8796bd7a6f3ff4f8eb"}
     * request : {"phone":"15275411070","code":"522601","authId":"167"}
     * other : []
     */

    private int code;
    private String msg;
    private ResponseBean response;
    private RequestBean request;
    private List<?> other;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public ResponseBean getResponse() {
        return response;
    }

    public void setResponse(ResponseBean response) {
        this.response = response;
    }

    public RequestBean getRequest() {
        return request;
    }

    public void setRequest(RequestBean request) {
        this.request = request;
    }

    public List<?> getOther() {
        return other;
    }

    public void setOther(List<?> other) {
        this.other = other;
    }

    public static class ResponseBean {
        /**
         * userid : 4
         * username : 15275411070
         * avatar : http://park.hostop.net/static/image/logo.png
         * nickname : 未填写
         * realname :
         * uuid : 472B9D11-2FEC-4CBD-8379-BE51E5C02AD3
         * parkId : 0
         * parkInfo : {"parkId":"1","parkName":"温州平阳众创城"}
         * status : 1
         * token : 29a387623d8d2b8796bd7a6f3ff4f8eb
         */

        private String userid;
        private String username;
        private String avatar;
        private String nickname;
        private String realname;
        private String uuid;
        private String parkId;
        private ParkInfoBean parkInfo;
        private String status;
        private String token;

        public String getUserid() {
            return userid;
        }

        public void setUserid(String userid) {
            this.userid = userid;
        }

        public String getUsername() {
            return username;
        }

        public void setUsername(String username) {
            this.username = username;
        }

        public String getAvatar() {
            return avatar;
        }

        public void setAvatar(String avatar) {
            this.avatar = avatar;
        }

        public String getNickname() {
            return nickname;
        }

        public void setNickname(String nickname) {
            this.nickname = nickname;
        }

        public String getRealname() {
            return realname;
        }

        public void setRealname(String realname) {
            this.realname = realname;
        }

        public String getUuid() {
            return uuid;
        }

        public void setUuid(String uuid) {
            this.uuid = uuid;
        }

        public String getParkId() {
            return parkId;
        }

        public void setParkId(String parkId) {
            this.parkId = parkId;
        }

        public ParkInfoBean getParkInfo() {
            return parkInfo;
        }

        public void setParkInfo(ParkInfoBean parkInfo) {
            this.parkInfo = parkInfo;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getToken() {
            return token;
        }

        public void setToken(String token) {
            this.token = token;
        }

        public static class ParkInfoBean {
            /**
             * parkId : 1
             * parkName : 温州平阳众创城
             */

            private String parkId;
            private String parkName;

            public String getParkId() {
                return parkId;
            }

            public void setParkId(String parkId) {
                this.parkId = parkId;
            }

            public String getParkName() {
                return parkName;
            }

            public void setParkName(String parkName) {
                this.parkName = parkName;
            }
        }
    }

    public static class RequestBean {
        /**
         * phone : 15275411070
         * code : 522601
         * authId : 167
         */

        private String phone;
        private String code;
        private String authId;

        public String getPhone() {
            return phone;
        }

        public void setPhone(String phone) {
            this.phone = phone;
        }

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        public String getAuthId() {
            return authId;
        }

        public void setAuthId(String authId) {
            this.authId = authId;
        }
    }
}
