package com.wanyangproject.entity;

import java.util.List;

/**
 * Created by 甜甜 on 2018/8/14.
 */

public class ShangJiaHuoDongXiangQingEntity {

    /**
     * code : 200
     * msg :
     * response : {"ShopPreferences":{"id":"1","title":"商家优惠活动","introduction":"商家可在平台发布优惠活动","content":"45656456456456456424564354q3qweqweqw恶趣味去玩恶趣味群无去玩恶趣味额去玩 去 去玩去玩","type":"1","add_time":"2018-08-08 02:01:08","Recommend":"2","parkId":"1"}}
     * request : {"parkId":"1","Preferences_id":"1"}
     * other : []
     */

    private int code;
    private String msg;
    private ResponseBean response;
    private RequestBean request;
    private List<?> other;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public ResponseBean getResponse() {
        return response;
    }

    public void setResponse(ResponseBean response) {
        this.response = response;
    }

    public RequestBean getRequest() {
        return request;
    }

    public void setRequest(RequestBean request) {
        this.request = request;
    }

    public List<?> getOther() {
        return other;
    }

    public void setOther(List<?> other) {
        this.other = other;
    }

    public static class ResponseBean {
        /**
         * ShopPreferences : {"id":"1","title":"商家优惠活动","introduction":"商家可在平台发布优惠活动","content":"45656456456456456424564354q3qweqweqw恶趣味去玩恶趣味群无去玩恶趣味额去玩 去 去玩去玩","type":"1","add_time":"2018-08-08 02:01:08","Recommend":"2","parkId":"1"}
         */

        private ShopPreferencesBean ShopPreferences;

        public ShopPreferencesBean getShopPreferences() {
            return ShopPreferences;
        }

        public void setShopPreferences(ShopPreferencesBean ShopPreferences) {
            this.ShopPreferences = ShopPreferences;
        }

        public static class ShopPreferencesBean {
            /**
             * id : 1
             * title : 商家优惠活动
             * introduction : 商家可在平台发布优惠活动
             * content : 45656456456456456424564354q3qweqweqw恶趣味去玩恶趣味群无去玩恶趣味额去玩 去 去玩去玩
             * type : 1
             * add_time : 2018-08-08 02:01:08
             * Recommend : 2
             * parkId : 1
             */

            private String id;
            private String title;
            private String introduction;
            private String content;
            private String type;
            private String add_time;
            private String Recommend;
            private String parkId;

            public String getId() {
                return id;
            }

            public void setId(String id) {
                this.id = id;
            }

            public String getTitle() {
                return title;
            }

            public void setTitle(String title) {
                this.title = title;
            }

            public String getIntroduction() {
                return introduction;
            }

            public void setIntroduction(String introduction) {
                this.introduction = introduction;
            }

            public String getContent() {
                return content;
            }

            public void setContent(String content) {
                this.content = content;
            }

            public String getType() {
                return type;
            }

            public void setType(String type) {
                this.type = type;
            }

            public String getAdd_time() {
                return add_time;
            }

            public void setAdd_time(String add_time) {
                this.add_time = add_time;
            }

            public String getRecommend() {
                return Recommend;
            }

            public void setRecommend(String Recommend) {
                this.Recommend = Recommend;
            }

            public String getParkId() {
                return parkId;
            }

            public void setParkId(String parkId) {
                this.parkId = parkId;
            }
        }
    }

    public static class RequestBean {
        /**
         * parkId : 1
         * Preferences_id : 1
         */

        private String parkId;
        private String Preferences_id;

        public String getParkId() {
            return parkId;
        }

        public void setParkId(String parkId) {
            this.parkId = parkId;
        }

        public String getPreferences_id() {
            return Preferences_id;
        }

        public void setPreferences_id(String Preferences_id) {
            this.Preferences_id = Preferences_id;
        }
    }
}
