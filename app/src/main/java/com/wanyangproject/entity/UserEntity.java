package com.wanyangproject.entity;

import java.util.List;

/**
 * Created by 甜甜 on 2018/8/16.
 */

public class UserEntity {
    /**
     * code : 200
     * msg :
     * response : {"userid":"110","username":"15810646271","nickname":"wy184128","sex":"0","avatar":"http://park.hostop.net/static/image/logo.png","realname":"","idCard":"","age":"0","birth":""}
     * request : []
     * other : []
     */

    private int code;
    private String msg;
    private ResponseBean response;
    private List<?> request;
    private List<?> other;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public ResponseBean getResponse() {
        return response;
    }

    public void setResponse(ResponseBean response) {
        this.response = response;
    }

    public List<?> getRequest() {
        return request;
    }

    public void setRequest(List<?> request) {
        this.request = request;
    }

    public List<?> getOther() {
        return other;
    }

    public void setOther(List<?> other) {
        this.other = other;
    }

    public static class ResponseBean {
        /**
         * userid : 110
         * username : 15810646271
         * nickname : wy184128
         * sex : 0
         * avatar : http://park.hostop.net/static/image/logo.png
         * realname :
         * idCard :
         * age : 0
         * birth :
         */

        private String userid;
        private String username;
        private String nickname;
        private String sex;
        private String avatar;
        private String realname;
        private String idCard;
        private String age;
        private String birth;

        public String getUserid() {
            return userid;
        }

        public void setUserid(String userid) {
            this.userid = userid;
        }

        public String getUsername() {
            return username;
        }

        public void setUsername(String username) {
            this.username = username;
        }

        public String getNickname() {
            return nickname;
        }

        public void setNickname(String nickname) {
            this.nickname = nickname;
        }

        public String getSex() {
            return sex;
        }

        public void setSex(String sex) {
            this.sex = sex;
        }

        public String getAvatar() {
            return avatar;
        }

        public void setAvatar(String avatar) {
            this.avatar = avatar;
        }

        public String getRealname() {
            return realname;
        }

        public void setRealname(String realname) {
            this.realname = realname;
        }

        public String getIdCard() {
            return idCard;
        }

        public void setIdCard(String idCard) {
            this.idCard = idCard;
        }

        public String getAge() {
            return age;
        }

        public void setAge(String age) {
            this.age = age;
        }

        public String getBirth() {
            return birth;
        }

        public void setBirth(String birth) {
            this.birth = birth;
        }
    }


//    /**
//     * code : 200
//     * msg :
//     * response : {"userid":"93","username":"13233440220","nickname":"wy459807","sex":"0","avatar":"http://park.hostop.net/static/image/logo.png","age":"0","birth":"1540915200","realname":""}
//     * request : []
//     * other : []
//     */
//
//    private int code;
//    private String msg;
//    private ResponseBean response;
//    private List<?> request;
//    private List<?> other;
//
//    public int getCode() {
//        return code;
//    }
//
//    public void setCode(int code) {
//        this.code = code;
//    }
//
//    public String getMsg() {
//        return msg;
//    }
//
//    public void setMsg(String msg) {
//        this.msg = msg;
//    }
//
//    public ResponseBean getResponse() {
//        return response;
//    }
//
//    public void setResponse(ResponseBean response) {
//        this.response = response;
//    }
//
//    public List<?> getRequest() {
//        return request;
//    }
//
//    public void setRequest(List<?> request) {
//        this.request = request;
//    }
//
//    public List<?> getOther() {
//        return other;
//    }
//
//    public void setOther(List<?> other) {
//        this.other = other;
//    }
//
//    public static class ResponseBean {
//        /**
//         * userid : 93
//         * username : 13233440220
//         * nickname : wy459807
//         * sex : 0
//         * avatar : http://park.hostop.net/static/image/logo.png
//         * age : 0
//         * birth : 1540915200
//         * realname :
//         */
//
//        private String userid;
//        private String username;
//        private String nickname;
//        private String sex;
//        private String avatar;
//        private String age;
//        private String birth;
//        private String realname;
//
//        public String getUserid() {
//            return userid;
//        }
//
//        public void setUserid(String userid) {
//            this.userid = userid;
//        }
//
//        public String getUsername() {
//            return username;
//        }
//
//        public void setUsername(String username) {
//            this.username = username;
//        }
//
//        public String getNickname() {
//            return nickname;
//        }
//
//        public void setNickname(String nickname) {
//            this.nickname = nickname;
//        }
//
//        public String getSex() {
//            return sex;
//        }
//
//        public void setSex(String sex) {
//            this.sex = sex;
//        }
//
//        public String getAvatar() {
//            return avatar;
//        }
//
//        public void setAvatar(String avatar) {
//            this.avatar = avatar;
//        }
//
//        public String getAge() {
//            return age;
//        }
//
//        public void setAge(String age) {
//            this.age = age;
//        }
//
//        public String getBirth() {
//            return birth;
//        }
//
//        public void setBirth(String birth) {
//            this.birth = birth;
//        }
//
//        public String getRealname() {
//            return realname;
//        }
//
//        public void setRealname(String realname) {
//            this.realname = realname;
//        }
//    }































//    /**
//     * code : 200
//     * msg :
//     * response : {"userid":"13","username":"15810646251","nickname":"我的新名字","sex":"1","avatar":"http://park.hostop.net/upload/20180926/4e0a5783ae67f3c440c257d0fcd1be00.jpg","age":"21","birth":"875203200"}
//     * request : []
//     * other : []
//     */
//
//    private int code;
//    private String msg;
//    private ResponseBean response;
//    private List<?> request;
//    private List<?> other;
//
//    public int getCode() {
//        return code;
//    }
//
//    public void setCode(int code) {
//        this.code = code;
//    }
//
//    public String getMsg() {
//        return msg;
//    }
//
//    public void setMsg(String msg) {
//        this.msg = msg;
//    }
//
//    public ResponseBean getResponse() {
//        return response;
//    }
//
//    public void setResponse(ResponseBean response) {
//        this.response = response;
//    }
//
//    public List<?> getRequest() {
//        return request;
//    }
//
//    public void setRequest(List<?> request) {
//        this.request = request;
//    }
//
//    public List<?> getOther() {
//        return other;
//    }
//
//    public void setOther(List<?> other) {
//        this.other = other;
//    }
//
//    public static class ResponseBean {
//        /**
//         * userid : 13
//         * username : 15810646251
//         * nickname : 我的新名字
//         * sex : 1
//         * avatar : http://park.hostop.net/upload/20180926/4e0a5783ae67f3c440c257d0fcd1be00.jpg
//         * age : 21
//         * birth : 875203200
//         */
//
//        private String userid;
//        private String username;
//        private String nickname;
//        private String sex;
//        private String avatar;
//        private String age;
//        private String birth;
//
//        public String getUserid() {
//            return userid;
//        }
//
//        public void setUserid(String userid) {
//            this.userid = userid;
//        }
//
//        public String getUsername() {
//            return username;
//        }
//
//        public void setUsername(String username) {
//            this.username = username;
//        }
//
//        public String getNickname() {
//            return nickname;
//        }
//
//        public void setNickname(String nickname) {
//            this.nickname = nickname;
//        }
//
//        public String getSex() {
//            return sex;
//        }
//
//        public void setSex(String sex) {
//            this.sex = sex;
//        }
//
//        public String getAvatar() {
//            return avatar;
//        }
//
//        public void setAvatar(String avatar) {
//            this.avatar = avatar;
//        }
//
//        public String getAge() {
//            return age;
//        }
//
//        public void setAge(String age) {
//            this.age = age;
//        }
//
//        public String getBirth() {
//            return birth;
//        }
//
//        public void setBirth(String birth) {
//            this.birth = birth;
//        }
//    }
}
