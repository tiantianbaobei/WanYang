package com.wanyangproject.entity;

import java.util.List;

/**
 * Created by 甜甜 on 2018/8/27.
 */

public class ShouCangShangJiaXianShiEntity {

    /**
     * code : 200
     * msg :
     * response : [{"nickname":"商家","sales":"10","starting":"20","photo":"park.hostop.net//upload/20180823/bab664273f521cea285761a6e79569f6.png","cost":"5","mer_class":"1","id":"1"}]
     * request : {"parkId":"1","type":"1"}
     * other : []
     */

    private int code;
    private String msg;
    private RequestBean request;
    private List<ResponseBean> response;
    private List<?> other;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public RequestBean getRequest() {
        return request;
    }

    public void setRequest(RequestBean request) {
        this.request = request;
    }

    public List<ResponseBean> getResponse() {
        return response;
    }

    public void setResponse(List<ResponseBean> response) {
        this.response = response;
    }

    public List<?> getOther() {
        return other;
    }

    public void setOther(List<?> other) {
        this.other = other;
    }

    public static class RequestBean {
        /**
         * parkId : 1
         * type : 1
         */

        private String parkId;
        private String type;

        public String getParkId() {
            return parkId;
        }

        public void setParkId(String parkId) {
            this.parkId = parkId;
        }

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }
    }

    public static class ResponseBean {
        /**
         * nickname : 商家
         * sales : 10
         * starting : 20
         * photo : park.hostop.net//upload/20180823/bab664273f521cea285761a6e79569f6.png
         * cost : 5
         * mer_class : 1
         * id : 1
         */

        private String nickname;
        private String sales;
        private String starting;
        private String photo;
        private String cost;
        private String mer_class;
        private String id;

        public String getNickname() {
            return nickname;
        }

        public void setNickname(String nickname) {
            this.nickname = nickname;
        }

        public String getSales() {
            return sales;
        }

        public void setSales(String sales) {
            this.sales = sales;
        }

        public String getStarting() {
            return starting;
        }

        public void setStarting(String starting) {
            this.starting = starting;
        }

        public String getPhoto() {
            return photo;
        }

        public void setPhoto(String photo) {
            this.photo = photo;
        }

        public String getCost() {
            return cost;
        }

        public void setCost(String cost) {
            this.cost = cost;
        }

        public String getMer_class() {
            return mer_class;
        }

        public void setMer_class(String mer_class) {
            this.mer_class = mer_class;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }
    }
}
