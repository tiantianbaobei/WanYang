package com.wanyangproject.entity;

import java.util.List;

/**
 * Created by 甜甜 on 2018/8/25.
 */

public class FuWuShenQingEntity {


    /**
     * code : 200
     * msg :
     * response : {"id":"155","jiid":"545"}
     * request : {"typeId":"1","fuwuId":"195","content":"测试服务","name":"谢作华","phone":"17600902121","fuwuName":"测试服务"}
     * other : []
     */

    private int code;
    private String msg;
    private ResponseBean response;
    private RequestBean request;
    private List<?> other;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public ResponseBean getResponse() {
        return response;
    }

    public void setResponse(ResponseBean response) {
        this.response = response;
    }

    public RequestBean getRequest() {
        return request;
    }

    public void setRequest(RequestBean request) {
        this.request = request;
    }

    public List<?> getOther() {
        return other;
    }

    public void setOther(List<?> other) {
        this.other = other;
    }

    public static class ResponseBean {
        /**
         * id : 155
         * jiid : 545
         */

        private String id;
        private String jiid;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getJiid() {
            return jiid;
        }

        public void setJiid(String jiid) {
            this.jiid = jiid;
        }
    }

    public static class RequestBean {
        /**
         * typeId : 1
         * fuwuId : 195
         * content : 测试服务
         * name : 谢作华
         * phone : 17600902121
         * fuwuName : 测试服务
         */

        private String typeId;
        private String fuwuId;
        private String content;
        private String name;
        private String phone;
        private String fuwuName;

        public String getTypeId() {
            return typeId;
        }

        public void setTypeId(String typeId) {
            this.typeId = typeId;
        }

        public String getFuwuId() {
            return fuwuId;
        }

        public void setFuwuId(String fuwuId) {
            this.fuwuId = fuwuId;
        }

        public String getContent() {
            return content;
        }

        public void setContent(String content) {
            this.content = content;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getPhone() {
            return phone;
        }

        public void setPhone(String phone) {
            this.phone = phone;
        }

        public String getFuwuName() {
            return fuwuName;
        }

        public void setFuwuName(String fuwuName) {
            this.fuwuName = fuwuName;
        }
    }
}
