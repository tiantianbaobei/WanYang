package com.wanyangproject.entity;

import java.util.List;

/**
 * Created by 甜甜 on 2018/8/17.
 */

public class ShouHuoXinXiLieBiaoEntity {

    /**
     * code : 200
     * msg :
     * response : [{"id":"4","pkid":"1","user_id":"2","name":"测试","phone":"17600904682","region":"山东省济南市历下区","addres":"泉城路","add_time":"1534489783","type":"2"}]
     * request : {"parkId":"1"}
     * other : []
     */

    private int code;
    private String msg;
    private RequestBean request;
    private List<ResponseBean> response;
    private List<?> other;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public RequestBean getRequest() {
        return request;
    }

    public void setRequest(RequestBean request) {
        this.request = request;
    }

    public List<ResponseBean> getResponse() {
        return response;
    }

    public void setResponse(List<ResponseBean> response) {
        this.response = response;
    }

    public List<?> getOther() {
        return other;
    }

    public void setOther(List<?> other) {
        this.other = other;
    }

    public static class RequestBean {
        /**
         * parkId : 1
         */

        private String parkId;

        public String getParkId() {
            return parkId;
        }

        public void setParkId(String parkId) {
            this.parkId = parkId;
        }
    }

    public static class ResponseBean {
        /**
         * id : 4
         * pkid : 1
         * user_id : 2
         * name : 测试
         * phone : 17600904682
         * region : 山东省济南市历下区
         * addres : 泉城路
         * add_time : 1534489783
         * type : 2
         */

        private String id;
        private String pkid;
        private String user_id;
        private String name;
        private String phone;
        private String region;
        private String addres;
        private String add_time;
        private String type;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getPkid() {
            return pkid;
        }

        public void setPkid(String pkid) {
            this.pkid = pkid;
        }

        public String getUser_id() {
            return user_id;
        }

        public void setUser_id(String user_id) {
            this.user_id = user_id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getPhone() {
            return phone;
        }

        public void setPhone(String phone) {
            this.phone = phone;
        }

        public String getRegion() {
            return region;
        }

        public void setRegion(String region) {
            this.region = region;
        }

        public String getAddres() {
            return addres;
        }

        public void setAddres(String addres) {
            this.addres = addres;
        }

        public String getAdd_time() {
            return add_time;
        }

        public void setAdd_time(String add_time) {
            this.add_time = add_time;
        }

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }
    }
}
