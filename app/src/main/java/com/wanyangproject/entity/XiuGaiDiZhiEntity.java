package com.wanyangproject.entity;

import java.util.List;

/**
 * Created by 甜甜 on 2018/8/20.
 */

public class XiuGaiDiZhiEntity {

    /**
     * code : 200
     * msg :
     * response : {"mes":"修改成功！"}
     * request : {"name":"秀发i","phone":"15822075533","region":"北京省北京市朝阳区","addres":"哈哈","parkId":"1","type":"2","addres_id":"4"}
     * other : []
     */

    private int code;
    private String msg;
    private ResponseBean response;
    private RequestBean request;
    private List<?> other;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public ResponseBean getResponse() {
        return response;
    }

    public void setResponse(ResponseBean response) {
        this.response = response;
    }

    public RequestBean getRequest() {
        return request;
    }

    public void setRequest(RequestBean request) {
        this.request = request;
    }

    public List<?> getOther() {
        return other;
    }

    public void setOther(List<?> other) {
        this.other = other;
    }

    public static class ResponseBean {
        /**
         * mes : 修改成功！
         */

        private String mes;

        public String getMes() {
            return mes;
        }

        public void setMes(String mes) {
            this.mes = mes;
        }
    }

    public static class RequestBean {
        /**
         * name : 秀发i
         * phone : 15822075533
         * region : 北京省北京市朝阳区
         * addres : 哈哈
         * parkId : 1
         * type : 2
         * addres_id : 4
         */

        private String name;
        private String phone;
        private String region;
        private String addres;
        private String parkId;
        private String type;
        private String addres_id;

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getPhone() {
            return phone;
        }

        public void setPhone(String phone) {
            this.phone = phone;
        }

        public String getRegion() {
            return region;
        }

        public void setRegion(String region) {
            this.region = region;
        }

        public String getAddres() {
            return addres;
        }

        public void setAddres(String addres) {
            this.addres = addres;
        }

        public String getParkId() {
            return parkId;
        }

        public void setParkId(String parkId) {
            this.parkId = parkId;
        }

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public String getAddres_id() {
            return addres_id;
        }

        public void setAddres_id(String addres_id) {
            this.addres_id = addres_id;
        }
    }
}
