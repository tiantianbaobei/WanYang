package com.wanyangproject.entity;

import java.util.List;

/**
 * Created by 甜甜 on 2018/9/21.
 */

public class DengJiShiBaiEntity {

    /**
     * code : 400
     * msg : 抱歉，目前只有企业用户可以申请！
     * response : []
     * request : {"parkId":"1","name":"测试","phone":"15275411070","nums":"6","chepai":"","daodaoTime":"2018-09-21 17:39:00"}
     * other : []
     */

    private int code;
    private String msg;
    private RequestBean request;
    private List<?> response;
    private List<?> other;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public RequestBean getRequest() {
        return request;
    }

    public void setRequest(RequestBean request) {
        this.request = request;
    }

    public List<?> getResponse() {
        return response;
    }

    public void setResponse(List<?> response) {
        this.response = response;
    }

    public List<?> getOther() {
        return other;
    }

    public void setOther(List<?> other) {
        this.other = other;
    }

    public static class RequestBean {
        /**
         * parkId : 1
         * name : 测试
         * phone : 15275411070
         * nums : 6
         * chepai :
         * daodaoTime : 2018-09-21 17:39:00
         */

        private String parkId;
        private String name;
        private String phone;
        private String nums;
        private String chepai;
        private String daodaoTime;

        public String getParkId() {
            return parkId;
        }

        public void setParkId(String parkId) {
            this.parkId = parkId;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getPhone() {
            return phone;
        }

        public void setPhone(String phone) {
            this.phone = phone;
        }

        public String getNums() {
            return nums;
        }

        public void setNums(String nums) {
            this.nums = nums;
        }

        public String getChepai() {
            return chepai;
        }

        public void setChepai(String chepai) {
            this.chepai = chepai;
        }

        public String getDaodaoTime() {
            return daodaoTime;
        }

        public void setDaodaoTime(String daodaoTime) {
            this.daodaoTime = daodaoTime;
        }
    }
}
