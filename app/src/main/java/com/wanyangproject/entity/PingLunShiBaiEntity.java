package com.wanyangproject.entity;

import java.util.List;

/**
 * Created by 甜甜 on 2018/8/31.
 */

public class PingLunShiBaiEntity {

    /**
     * code : 400
     * msg : 您发表的评论里面包含提交失败，请重新整体后再提交
     * response : []
     * request : {"content":"哈哈","forumId":"35","upId":"0"}
     * other : []
     */

    private int code;
    private String msg;
    private RequestBean request;
    private List<?> response;
    private List<?> other;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public RequestBean getRequest() {
        return request;
    }

    public void setRequest(RequestBean request) {
        this.request = request;
    }

    public List<?> getResponse() {
        return response;
    }

    public void setResponse(List<?> response) {
        this.response = response;
    }

    public List<?> getOther() {
        return other;
    }

    public void setOther(List<?> other) {
        this.other = other;
    }

    public static class RequestBean {
        /**
         * content : 哈哈
         * forumId : 35
         * upId : 0
         */

        private String content;
        private String forumId;
        private String upId;

        public String getContent() {
            return content;
        }

        public void setContent(String content) {
            this.content = content;
        }

        public String getForumId() {
            return forumId;
        }

        public void setForumId(String forumId) {
            this.forumId = forumId;
        }

        public String getUpId() {
            return upId;
        }

        public void setUpId(String upId) {
            this.upId = upId;
        }
    }
}
