package com.wanyangproject.entity;

import java.util.List;

/**
 * Created by 甜甜 on 2018/10/9.
 */

public class UpDataEntity {

    /**
     * code : 200
     * msg :
     * response : {"src":"http://park.hostop.net/upload/20181009/9701e5015b2502eb48586190f7fa6ee6.jpg","v":"1"}
     * request : []
     * other : []
     */

    private int code;
    private String msg;
    private ResponseBean response;
    private List<?> request;
    private List<?> other;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public ResponseBean getResponse() {
        return response;
    }

    public void setResponse(ResponseBean response) {
        this.response = response;
    }

    public List<?> getRequest() {
        return request;
    }

    public void setRequest(List<?> request) {
        this.request = request;
    }

    public List<?> getOther() {
        return other;
    }

    public void setOther(List<?> other) {
        this.other = other;
    }

    public static class ResponseBean {
        /**
         * src : http://park.hostop.net/upload/20181009/9701e5015b2502eb48586190f7fa6ee6.jpg
         * v : 1.0
         */

        private String src;
        private String v;

        public String getSrc() {
            return src;
        }

        public void setSrc(String src) {
            this.src = src;
        }

        public String getV() {
            return v;
        }

        public void setV(String v) {
            this.v = v;
        }
    }
}
