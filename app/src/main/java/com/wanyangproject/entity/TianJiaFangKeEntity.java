package com.wanyangproject.entity;

import java.util.List;

/**
 * Created by 甜甜 on 2018/8/26.
 */

public class TianJiaFangKeEntity {
    /**
     * code : 200
     * msg :
     * response : {"photo":"upload/qrcode/2018-09-21/179e767f974c06d3fb23eba14c256a3951.png","logo":"static\\wanyang\\img\\download-logo.png"}
     * request : {"parkId":"1","name":"测试","phone":"17600904682","nums":"3","chepai":"","daodaoTime":"2018-09-21 13:27:26"}
     * other : []
     */

    private int code;
    private String msg;
    private ResponseBean response;
    private RequestBean request;
    private List<?> other;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public ResponseBean getResponse() {
        return response;
    }

    public void setResponse(ResponseBean response) {
        this.response = response;
    }

    public RequestBean getRequest() {
        return request;
    }

    public void setRequest(RequestBean request) {
        this.request = request;
    }

    public List<?> getOther() {
        return other;
    }

    public void setOther(List<?> other) {
        this.other = other;
    }

    public static class ResponseBean {
        /**
         * photo : upload/qrcode/2018-09-21/179e767f974c06d3fb23eba14c256a3951.png
         * logo : static\wanyang\img\download-logo.png
         */

        private String photo;
        private String logo;

        public String getPhoto() {
            return photo;
        }

        public void setPhoto(String photo) {
            this.photo = photo;
        }

        public String getLogo() {
            return logo;
        }

        public void setLogo(String logo) {
            this.logo = logo;
        }
    }

    public static class RequestBean {
        /**
         * parkId : 1
         * name : 测试
         * phone : 17600904682
         * nums : 3
         * chepai :
         * daodaoTime : 2018-09-21 13:27:26
         */

        private String parkId;
        private String name;
        private String phone;
        private String nums;
        private String chepai;
        private String daodaoTime;

        public String getParkId() {
            return parkId;
        }

        public void setParkId(String parkId) {
            this.parkId = parkId;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getPhone() {
            return phone;
        }

        public void setPhone(String phone) {
            this.phone = phone;
        }

        public String getNums() {
            return nums;
        }

        public void setNums(String nums) {
            this.nums = nums;
        }

        public String getChepai() {
            return chepai;
        }

        public void setChepai(String chepai) {
            this.chepai = chepai;
        }

        public String getDaodaoTime() {
            return daodaoTime;
        }

        public void setDaodaoTime(String daodaoTime) {
            this.daodaoTime = daodaoTime;
        }
    }


//    /**
//     * code : 200
//     * msg :
//     * response : {"photo":"upload/qrcode/2018-08-29/1d3fd43cf07969fb25b8d40b26f54aae21.png"}
//     * request : {"parkId":"1","name":"测试","phone":"12345678900","nums":"10","chepai":"","daodaoTime":"1535531820000"}
//     * other : []
//     */
//
//    private int code;
//    private String msg;
//    private ResponseBean response;
//    private RequestBean request;
//    private List<?> other;
//
//    public int getCode() {
//        return code;
//    }
//
//    public void setCode(int code) {
//        this.code = code;
//    }
//
//    public String getMsg() {
//        return msg;
//    }
//
//    public void setMsg(String msg) {
//        this.msg = msg;
//    }
//
//    public ResponseBean getResponse() {
//        return response;
//    }
//
//    public void setResponse(ResponseBean response) {
//        this.response = response;
//    }
//
//    public RequestBean getRequest() {
//        return request;
//    }
//
//    public void setRequest(RequestBean request) {
//        this.request = request;
//    }
//
//    public List<?> getOther() {
//        return other;
//    }
//
//    public void setOther(List<?> other) {
//        this.other = other;
//    }
//
//    public static class ResponseBean {
//        /**
//         * photo : upload/qrcode/2018-08-29/1d3fd43cf07969fb25b8d40b26f54aae21.png
//         */
//
//        private String photo;
//
//        public String getPhoto() {
//            return photo;
//        }
//
//        public void setPhoto(String photo) {
//            this.photo = photo;
//        }
//    }
//
//    public static class RequestBean {
//        /**
//         * parkId : 1
//         * name : 测试
//         * phone : 12345678900
//         * nums : 10
//         * chepai :
//         * daodaoTime : 1535531820000
//         */
//
//        private String parkId;
//        private String name;
//        private String phone;
//        private String nums;
//        private String chepai;
//        private String daodaoTime;
//
//        public String getParkId() {
//            return parkId;
//        }
//
//        public void setParkId(String parkId) {
//            this.parkId = parkId;
//        }
//
//        public String getName() {
//            return name;
//        }
//
//        public void setName(String name) {
//            this.name = name;
//        }
//
//        public String getPhone() {
//            return phone;
//        }
//
//        public void setPhone(String phone) {
//            this.phone = phone;
//        }
//
//        public String getNums() {
//            return nums;
//        }
//
//        public void setNums(String nums) {
//            this.nums = nums;
//        }
//
//        public String getChepai() {
//            return chepai;
//        }
//
//        public void setChepai(String chepai) {
//            this.chepai = chepai;
//        }
//
//        public String getDaodaoTime() {
//            return daodaoTime;
//        }
//
//        public void setDaodaoTime(String daodaoTime) {
//            this.daodaoTime = daodaoTime;
//        }
//    }













//
//    /**
//     * code : 200
//     * msg :
//     * response : []
//     * request : {"parkId":"1","name":"123","phone":"12345678","nums":"10","chepai":"546158","daodaoTime":"1535277840000"}
//     * other : []
//     */
//
//    private int code;
//    private String msg;
//    private RequestBean request;
//    private List<?> response;
//    private List<?> other;
//
//    public int getCode() {
//        return code;
//    }
//
//    public void setCode(int code) {
//        this.code = code;
//    }
//
//    public String getMsg() {
//        return msg;
//    }
//
//    public void setMsg(String msg) {
//        this.msg = msg;
//    }
//
//    public RequestBean getRequest() {
//        return request;
//    }
//
//    public void setRequest(RequestBean request) {
//        this.request = request;
//    }
//
//    public List<?> getResponse() {
//        return response;
//    }
//
//    public void setResponse(List<?> response) {
//        this.response = response;
//    }
//
//    public List<?> getOther() {
//        return other;
//    }
//
//    public void setOther(List<?> other) {
//        this.other = other;
//    }
//
//    public static class RequestBean {
//        /**
//         * parkId : 1
//         * name : 123
//         * phone : 12345678
//         * nums : 10
//         * chepai : 546158
//         * daodaoTime : 1535277840000
//         */
//
//        private String parkId;
//        private String name;
//        private String phone;
//        private String nums;
//        private String chepai;
//        private String daodaoTime;
//
//        public String getParkId() {
//            return parkId;
//        }
//
//        public void setParkId(String parkId) {
//            this.parkId = parkId;
//        }
//
//        public String getName() {
//            return name;
//        }
//
//        public void setName(String name) {
//            this.name = name;
//        }
//
//        public String getPhone() {
//            return phone;
//        }
//
//        public void setPhone(String phone) {
//            this.phone = phone;
//        }
//
//        public String getNums() {
//            return nums;
//        }
//
//        public void setNums(String nums) {
//            this.nums = nums;
//        }
//
//        public String getChepai() {
//            return chepai;
//        }
//
//        public void setChepai(String chepai) {
//            this.chepai = chepai;
//        }
//
//        public String getDaodaoTime() {
//            return daodaoTime;
//        }
//
//        public void setDaodaoTime(String daodaoTime) {
//            this.daodaoTime = daodaoTime;
//        }
//    }
}
