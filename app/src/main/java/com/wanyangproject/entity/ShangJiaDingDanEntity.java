package com.wanyangproject.entity;

import java.util.List;

/**
 * Created by 甜甜 on 2018/8/22.
 */

public class ShangJiaDingDanEntity {
    /**
     * code : 200
     * msg :
     * response : [{"id":"1016","order_sn":"pk3sn20180920150140","addres_add":"","type":"5","user_id":"3","addres_type":"2","pay_time":"1537426980","Reason":"","Remarks":"","Extraction":"09-20 16:00","queren_time":"","account":"","refund":"","yuji":"超时:25:59","order_goods":[{"goods_name":"测试","id":"30","master":"park.hostop.net//upload/20180911/21a8b535f68307c80150cf8ea7760175.jpg","pic":"0.01","volume":"0","fabulous":"0","stock":"100","market":"200","num":"1"}],"user_phone":"18364197153"},{"id":"1014","order_sn":"pk2sn20180920144159","addres_add":"山东省济南市历下区泉城路","type":"8","user_id":"2","addres_type":"1","pay_time":"1537425750","Reason":"","Remarks":"","Extraction":"","queren_time":"","account":"","refund":"","yuji":"超时:46:29","order_goods":[{"goods_name":"测试","id":"30","master":"park.hostop.net//upload/20180911/21a8b535f68307c80150cf8ea7760175.jpg","pic":"0.01","volume":"0","fabulous":"0","stock":"100","market":"200","num":"1"}],"user_phone":"17600904682"},{"id":"1006","order_sn":"pk2sn20180919175329","addres_add":"山东省济南市历下区泉城路","type":"8","user_id":"2","addres_type":"1","pay_time":"1537350835","Reason":"","Remarks":"","Extraction":"","queren_time":"","account":"","refund":"","yuji":"超时:35:04","order_goods":[{"goods_name":"测试","id":"30","master":"park.hostop.net//upload/20180911/21a8b535f68307c80150cf8ea7760175.jpg","pic":"0.01","volume":"0","fabulous":"0","stock":"100","market":"200","num":"1"}],"user_phone":"17600904682"},{"id":"849","order_sn":"pk2sn20180916212023","addres_add":"山东省济南市市中区123456","type":"12","user_id":"2","addres_type":"1","pay_time":"1537104077","Reason":"假冒品牌","Remarks":"","Extraction":"","queren_time":"","account":"17600904682","refund":"哈哈","yuji":"超时:07:42","order_goods":[{"goods_name":"测试","id":"30","master":"park.hostop.net//upload/20180911/21a8b535f68307c80150cf8ea7760175.jpg","pic":"0.01","volume":"0","fabulous":"0","stock":"100","market":"10","num":"1"}],"user_phone":"17600904682"},{"id":"843","order_sn":"pk2sn20180916205658","addres_add":"山东省济南市市中区123456","type":"8","user_id":"2","addres_type":"1","pay_time":"1537102631","Reason":"","Remarks":"","Extraction":"","queren_time":"","account":"","refund":"","yuji":"超时:31:48","order_goods":[{"goods_name":"测试","id":"30","master":"park.hostop.net//upload/20180911/21a8b535f68307c80150cf8ea7760175.jpg","pic":"0.01","volume":"0","fabulous":"0","stock":"100","market":"10","num":"1"}],"user_phone":"17600904682"},{"id":"842","order_sn":"pk2sn20180916205202","addres_add":"山东省济南市市中区123456","type":"8","user_id":"2","addres_type":"1","pay_time":"1537102334","Reason":"","Remarks":"","Extraction":"","queren_time":"","account":"","refund":"","yuji":"超时:36:45","order_goods":[{"goods_name":"测试","id":"30","master":"park.hostop.net//upload/20180911/21a8b535f68307c80150cf8ea7760175.jpg","pic":"0.01","volume":"0","fabulous":"0","stock":"100","market":"10","num":"1"}],"user_phone":"17600904682"},{"id":"841","order_sn":"pk2sn20180916205031","addres_add":"山东省济南市市中区123456","type":"8","user_id":"2","addres_type":"1","pay_time":"1537102250","Reason":"","Remarks":"","Extraction":"","queren_time":"","account":"","refund":"","yuji":"超时:38:09","order_goods":[{"goods_name":"测试","id":"30","master":"park.hostop.net//upload/20180911/21a8b535f68307c80150cf8ea7760175.jpg","pic":"0.01","volume":"0","fabulous":"0","stock":"100","market":"10","num":"1"}],"user_phone":"17600904682"},{"id":"839","order_sn":"pk2sn20180916204210","addres_add":"山东省济南市市中区123456","type":"8","user_id":"2","addres_type":"1","pay_time":"1537101743","Reason":"","Remarks":"","Extraction":"","queren_time":"1537411739","account":"","refund":"","yuji":"超时:46:36","order_goods":[{"goods_name":"测试","id":"30","master":"park.hostop.net//upload/20180911/21a8b535f68307c80150cf8ea7760175.jpg","pic":"0.01","volume":"0","fabulous":"0","stock":"100","market":"10","num":"1"}],"user_phone":"17600904682"},{"id":"838","order_sn":"pk2sn20180916204109","addres_add":"山东省济南市市中区123456","type":"8","user_id":"2","addres_type":"1","pay_time":"1537101680","Reason":"","Remarks":"","Extraction":"","queren_time":"","account":"","refund":"","yuji":"超时:47:39","order_goods":[{"goods_name":"测试","id":"30","master":"park.hostop.net//upload/20180911/21a8b535f68307c80150cf8ea7760175.jpg","pic":"0.01","volume":"0","fabulous":"0","stock":"100","market":"10","num":"1"}],"user_phone":"17600904682"},{"id":"834","order_sn":"pk2sn20180916203544","addres_add":"山东省济南市市中区123456","type":"8","user_id":"2","addres_type":"1","pay_time":"1537101357","Reason":"","Remarks":"","Extraction":"","queren_time":"","account":"","refund":"","yuji":"超时:53:02","order_goods":[{"goods_name":"测试","id":"30","master":"park.hostop.net//upload/20180911/21a8b535f68307c80150cf8ea7760175.jpg","pic":"0.01","volume":"0","fabulous":"0","stock":"100","market":"10","num":"1"}],"user_phone":"17600904682"}]
     * request : {"parkId":"1","phone":"17600904682"}
     * other : []
     */

    private int code;
    private String msg;
    private RequestBean request;
    private List<ResponseBean> response;
    private List<?> other;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public RequestBean getRequest() {
        return request;
    }

    public void setRequest(RequestBean request) {
        this.request = request;
    }

    public List<ResponseBean> getResponse() {
        return response;
    }

    public void setResponse(List<ResponseBean> response) {
        this.response = response;
    }

    public List<?> getOther() {
        return other;
    }

    public void setOther(List<?> other) {
        this.other = other;
    }

    public static class RequestBean {
        /**
         * parkId : 1
         * phone : 17600904682
         */

        private String parkId;
        private String phone;

        public String getParkId() {
            return parkId;
        }

        public void setParkId(String parkId) {
            this.parkId = parkId;
        }

        public String getPhone() {
            return phone;
        }

        public void setPhone(String phone) {
            this.phone = phone;
        }
    }

    public static class ResponseBean {
        /**
         * id : 1016
         * order_sn : pk3sn20180920150140
         * addres_add :
         * type : 5
         * user_id : 3
         * addres_type : 2
         * pay_time : 1537426980
         * Reason :
         * Remarks :
         * Extraction : 09-20 16:00
         * queren_time :
         * account :
         * refund :
         * yuji : 超时:25:59
         * order_goods : [{"goods_name":"测试","id":"30","master":"park.hostop.net//upload/20180911/21a8b535f68307c80150cf8ea7760175.jpg","pic":"0.01","volume":"0","fabulous":"0","stock":"100","market":"200","num":"1"}]
         * user_phone : 18364197153
         */

        private String id;
        private String order_sn;
        private String addres_add;
        private String type;
        private String user_id;
        private String addres_type;
        private String pay_time;
        private String Reason;
        private String Remarks;
        private String Extraction;
        private String queren_time;
        private String account;
        private String refund;
        private String yuji;
        private String user_phone;
        private List<OrderGoodsBean> order_goods;
        private String chaoshi;


        public String getChaoshi() {
            return chaoshi;
        }

        public void setChaoshi(String chaoshi) {
            this.chaoshi = chaoshi;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getOrder_sn() {
            return order_sn;
        }

        public void setOrder_sn(String order_sn) {
            this.order_sn = order_sn;
        }

        public String getAddres_add() {
            return addres_add;
        }

        public void setAddres_add(String addres_add) {
            this.addres_add = addres_add;
        }

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public String getUser_id() {
            return user_id;
        }

        public void setUser_id(String user_id) {
            this.user_id = user_id;
        }

        public String getAddres_type() {
            return addres_type;
        }

        public void setAddres_type(String addres_type) {
            this.addres_type = addres_type;
        }

        public String getPay_time() {
            return pay_time;
        }

        public void setPay_time(String pay_time) {
            this.pay_time = pay_time;
        }

        public String getReason() {
            return Reason;
        }

        public void setReason(String Reason) {
            this.Reason = Reason;
        }

        public String getRemarks() {
            return Remarks;
        }

        public void setRemarks(String Remarks) {
            this.Remarks = Remarks;
        }

        public String getExtraction() {
            return Extraction;
        }

        public void setExtraction(String Extraction) {
            this.Extraction = Extraction;
        }

        public String getQueren_time() {
            return queren_time;
        }

        public void setQueren_time(String queren_time) {
            this.queren_time = queren_time;
        }

        public String getAccount() {
            return account;
        }

        public void setAccount(String account) {
            this.account = account;
        }

        public String getRefund() {
            return refund;
        }

        public void setRefund(String refund) {
            this.refund = refund;
        }

        public String getYuji() {
            return yuji;
        }

        public void setYuji(String yuji) {
            this.yuji = yuji;
        }

        public String getUser_phone() {
            return user_phone;
        }

        public void setUser_phone(String user_phone) {
            this.user_phone = user_phone;
        }

        public List<OrderGoodsBean> getOrder_goods() {
            return order_goods;
        }

        public void setOrder_goods(List<OrderGoodsBean> order_goods) {
            this.order_goods = order_goods;
        }

        public static class OrderGoodsBean {
            /**
             * goods_name : 测试
             * id : 30
             * master : park.hostop.net//upload/20180911/21a8b535f68307c80150cf8ea7760175.jpg
             * pic : 0.01
             * volume : 0
             * fabulous : 0
             * stock : 100
             * market : 200
             * num : 1
             */

            private String goods_name;
            private String id;
            private String master;
            private String pic;
            private String volume;
            private String fabulous;
            private String stock;
            private String market;
            private String num;

            public String getGoods_name() {
                return goods_name;
            }

            public void setGoods_name(String goods_name) {
                this.goods_name = goods_name;
            }

            public String getId() {
                return id;
            }

            public void setId(String id) {
                this.id = id;
            }

            public String getMaster() {
                return master;
            }

            public void setMaster(String master) {
                this.master = master;
            }

            public String getPic() {
                return pic;
            }

            public void setPic(String pic) {
                this.pic = pic;
            }

            public String getVolume() {
                return volume;
            }

            public void setVolume(String volume) {
                this.volume = volume;
            }

            public String getFabulous() {
                return fabulous;
            }

            public void setFabulous(String fabulous) {
                this.fabulous = fabulous;
            }

            public String getStock() {
                return stock;
            }

            public void setStock(String stock) {
                this.stock = stock;
            }

            public String getMarket() {
                return market;
            }

            public void setMarket(String market) {
                this.market = market;
            }

            public String getNum() {
                return num;
            }

            public void setNum(String num) {
                this.num = num;
            }
        }
    }


//    /**
//     * code : 200
//     * msg :
//     * response : [{"id":"1006","order_sn":"pk2sn20180919175329","addres_add":"山东省济南市历下区泉城路","type":"8","user_id":"2","addres_type":"1","pay_time":"1537350835","Reason":"","Remarks":"","Extraction":"","queren_time":"","yuji":"超时:31:14","order_goods":[{"goods_name":"测试","id":"30","master":"park.hostop.net//upload/20180911/21a8b535f68307c80150cf8ea7760175.jpg","pic":"0.01","volume":"0","fabulous":"0","stock":"100","market":"200","num":"1"}],"user_phone":"17600904682"},{"id":"849","order_sn":"pk2sn20180916212023","addres_add":"山东省济南市市中区123456","type":"11","user_id":"2","addres_type":"1","pay_time":"1537104077","Reason":"假冒品牌","Remarks":"","Extraction":"","queren_time":"","yuji":"超时:03:52","order_goods":[{"goods_name":"测试","id":"30","master":"park.hostop.net//upload/20180911/21a8b535f68307c80150cf8ea7760175.jpg","pic":"0.01","volume":"0","fabulous":"0","stock":"100","market":"10","num":"1"}],"user_phone":"17600904682"},{"id":"843","order_sn":"pk2sn20180916205658","addres_add":"山东省济南市市中区123456","type":"7","user_id":"2","addres_type":"1","pay_time":"1537102631","Reason":"","Remarks":"","Extraction":"","queren_time":"","yuji":"超时:27:58","order_goods":[{"goods_name":"测试","id":"30","master":"park.hostop.net//upload/20180911/21a8b535f68307c80150cf8ea7760175.jpg","pic":"0.01","volume":"0","fabulous":"0","stock":"100","market":"10","num":"1"}],"user_phone":"17600904682"},{"id":"842","order_sn":"pk2sn20180916205202","addres_add":"山东省济南市市中区123456","type":"7","user_id":"2","addres_type":"1","pay_time":"1537102334","Reason":"","Remarks":"","Extraction":"","queren_time":"","yuji":"超时:32:55","order_goods":[{"goods_name":"测试","id":"30","master":"park.hostop.net//upload/20180911/21a8b535f68307c80150cf8ea7760175.jpg","pic":"0.01","volume":"0","fabulous":"0","stock":"100","market":"10","num":"1"}],"user_phone":"17600904682"},{"id":"841","order_sn":"pk2sn20180916205031","addres_add":"山东省济南市市中区123456","type":"7","user_id":"2","addres_type":"1","pay_time":"1537102250","Reason":"","Remarks":"","Extraction":"","queren_time":"","yuji":"超时:34:19","order_goods":[{"goods_name":"测试","id":"30","master":"park.hostop.net//upload/20180911/21a8b535f68307c80150cf8ea7760175.jpg","pic":"0.01","volume":"0","fabulous":"0","stock":"100","market":"10","num":"1"}],"user_phone":"17600904682"},{"id":"839","order_sn":"pk2sn20180916204210","addres_add":"山东省济南市市中区123456","type":"7","user_id":"2","addres_type":"1","pay_time":"1537101743","Reason":"","Remarks":"","Extraction":"","queren_time":"1537411739","yuji":"超时:42:46","order_goods":[{"goods_name":"测试","id":"30","master":"park.hostop.net//upload/20180911/21a8b535f68307c80150cf8ea7760175.jpg","pic":"0.01","volume":"0","fabulous":"0","stock":"100","market":"10","num":"1"}],"user_phone":"17600904682"},{"id":"838","order_sn":"pk2sn20180916204109","addres_add":"山东省济南市市中区123456","type":"5","user_id":"2","addres_type":"1","pay_time":"1537101680","Reason":"","Remarks":"","Extraction":"","queren_time":"","yuji":"超时:43:49","order_goods":[{"goods_name":"测试","id":"30","master":"park.hostop.net//upload/20180911/21a8b535f68307c80150cf8ea7760175.jpg","pic":"0.01","volume":"0","fabulous":"0","stock":"100","market":"10","num":"1"}],"user_phone":"17600904682"},{"id":"834","order_sn":"pk2sn20180916203544","addres_add":"山东省济南市市中区123456","type":"5","user_id":"2","addres_type":"1","pay_time":"1537101357","Reason":"","Remarks":"","Extraction":"","queren_time":"","yuji":"超时:49:12","order_goods":[{"goods_name":"测试","id":"30","master":"park.hostop.net//upload/20180911/21a8b535f68307c80150cf8ea7760175.jpg","pic":"0.01","volume":"0","fabulous":"0","stock":"100","market":"10","num":"1"}],"user_phone":"17600904682"},{"id":"832","order_sn":"pk2sn20180916202819","addres_add":"山东省济南市市中区123456","type":"8","user_id":"2","addres_type":"1","pay_time":"1537100915","Reason":"","Remarks":"","Extraction":"","queren_time":"","yuji":"超时:56:34","order_goods":[{"goods_name":"测试","id":"30","master":"park.hostop.net//upload/20180911/21a8b535f68307c80150cf8ea7760175.jpg","pic":"0.01","volume":"0","fabulous":"0","stock":"100","market":"10","num":"1"}],"user_phone":"17600904682"},{"id":"829","order_sn":"pk2sn20180916202137","addres_add":"山东省济南市市中区123456","type":"5","user_id":"2","addres_type":"1","pay_time":"1537100519","Reason":"","Remarks":"","Extraction":"","queren_time":"","yuji":"超时:03:10","order_goods":[{"goods_name":"测试","id":"30","master":"park.hostop.net//upload/20180911/21a8b535f68307c80150cf8ea7760175.jpg","pic":"0.01","volume":"0","fabulous":"0","stock":"100","market":"10","num":"1"}],"user_phone":"17600904682"}]
//     * request : {"parkId":"1","phone":"17600904682"}
//     * other : []
//     */
//
//    private int code;
//    private String msg;
//    private RequestBean request;
//    private List<ResponseBean> response;
//    private List<?> other;
//
//    public int getCode() {
//        return code;
//    }
//
//    public void setCode(int code) {
//        this.code = code;
//    }
//
//    public String getMsg() {
//        return msg;
//    }
//
//    public void setMsg(String msg) {
//        this.msg = msg;
//    }
//
//    public RequestBean getRequest() {
//        return request;
//    }
//
//    public void setRequest(RequestBean request) {
//        this.request = request;
//    }
//
//    public List<ResponseBean> getResponse() {
//        return response;
//    }
//
//    public void setResponse(List<ResponseBean> response) {
//        this.response = response;
//    }
//
//    public List<?> getOther() {
//        return other;
//    }
//
//    public void setOther(List<?> other) {
//        this.other = other;
//    }
//
//    public static class RequestBean {
//        /**
//         * parkId : 1
//         * phone : 17600904682
//         */
//
//        private String parkId;
//        private String phone;
//
//        public String getParkId() {
//            return parkId;
//        }
//
//        public void setParkId(String parkId) {
//            this.parkId = parkId;
//        }
//
//        public String getPhone() {
//            return phone;
//        }
//
//        public void setPhone(String phone) {
//            this.phone = phone;
//        }
//    }
//
//    public static class ResponseBean {
//        /**
//         * id : 1006
//         * order_sn : pk2sn20180919175329
//         * addres_add : 山东省济南市历下区泉城路
//         * type : 8
//         * user_id : 2
//         * addres_type : 1
//         * pay_time : 1537350835
//         * Reason :
//         * Remarks :
//         * Extraction :
//         * queren_time :
//         * yuji : 超时:31:14
//         * order_goods : [{"goods_name":"测试","id":"30","master":"park.hostop.net//upload/20180911/21a8b535f68307c80150cf8ea7760175.jpg","pic":"0.01","volume":"0","fabulous":"0","stock":"100","market":"200","num":"1"}]
//         * user_phone : 17600904682
//         */
//
//        private String id;
//        private String order_sn;
//        private String addres_add;
//        private String type;
//        private String user_id;
//        private String addres_type;
//        private String pay_time;
//        private String Reason;
//        private String Remarks;
//        private String Extraction;
//        private String queren_time;
//        private String yuji;
//        private String user_phone;
//        private List<OrderGoodsBean> order_goods;
//
//        public String getId() {
//            return id;
//        }
//
//        public void setId(String id) {
//            this.id = id;
//        }
//
//        public String getOrder_sn() {
//            return order_sn;
//        }
//
//        public void setOrder_sn(String order_sn) {
//            this.order_sn = order_sn;
//        }
//
//        public String getAddres_add() {
//            return addres_add;
//        }
//
//        public void setAddres_add(String addres_add) {
//            this.addres_add = addres_add;
//        }
//
//        public String getType() {
//            return type;
//        }
//
//        public void setType(String type) {
//            this.type = type;
//        }
//
//        public String getUser_id() {
//            return user_id;
//        }
//
//        public void setUser_id(String user_id) {
//            this.user_id = user_id;
//        }
//
//        public String getAddres_type() {
//            return addres_type;
//        }
//
//        public void setAddres_type(String addres_type) {
//            this.addres_type = addres_type;
//        }
//
//        public String getPay_time() {
//            return pay_time;
//        }
//
//        public void setPay_time(String pay_time) {
//            this.pay_time = pay_time;
//        }
//
//        public String getReason() {
//            return Reason;
//        }
//
//        public void setReason(String Reason) {
//            this.Reason = Reason;
//        }
//
//        public String getRemarks() {
//            return Remarks;
//        }
//
//        public void setRemarks(String Remarks) {
//            this.Remarks = Remarks;
//        }
//
//        public String getExtraction() {
//            return Extraction;
//        }
//
//        public void setExtraction(String Extraction) {
//            this.Extraction = Extraction;
//        }
//
//        public String getQueren_time() {
//            return queren_time;
//        }
//
//        public void setQueren_time(String queren_time) {
//            this.queren_time = queren_time;
//        }
//
//        public String getYuji() {
//            return yuji;
//        }
//
//        public void setYuji(String yuji) {
//            this.yuji = yuji;
//        }
//
//        public String getUser_phone() {
//            return user_phone;
//        }
//
//        public void setUser_phone(String user_phone) {
//            this.user_phone = user_phone;
//        }
//
//        public List<OrderGoodsBean> getOrder_goods() {
//            return order_goods;
//        }
//
//        public void setOrder_goods(List<OrderGoodsBean> order_goods) {
//            this.order_goods = order_goods;
//        }
//
//        public static class OrderGoodsBean {
//            /**
//             * goods_name : 测试
//             * id : 30
//             * master : park.hostop.net//upload/20180911/21a8b535f68307c80150cf8ea7760175.jpg
//             * pic : 0.01
//             * volume : 0
//             * fabulous : 0
//             * stock : 100
//             * market : 200
//             * num : 1
//             */
//
//            private String goods_name;
//            private String id;
//            private String master;
//            private String pic;
//            private String volume;
//            private String fabulous;
//            private String stock;
//            private String market;
//            private String num;
//
//            public String getGoods_name() {
//                return goods_name;
//            }
//
//            public void setGoods_name(String goods_name) {
//                this.goods_name = goods_name;
//            }
//
//            public String getId() {
//                return id;
//            }
//
//            public void setId(String id) {
//                this.id = id;
//            }
//
//            public String getMaster() {
//                return master;
//            }
//
//            public void setMaster(String master) {
//                this.master = master;
//            }
//
//            public String getPic() {
//                return pic;
//            }
//
//            public void setPic(String pic) {
//                this.pic = pic;
//            }
//
//            public String getVolume() {
//                return volume;
//            }
//
//            public void setVolume(String volume) {
//                this.volume = volume;
//            }
//
//            public String getFabulous() {
//                return fabulous;
//            }
//
//            public void setFabulous(String fabulous) {
//                this.fabulous = fabulous;
//            }
//
//            public String getStock() {
//                return stock;
//            }
//
//            public void setStock(String stock) {
//                this.stock = stock;
//            }
//
//            public String getMarket() {
//                return market;
//            }
//
//            public void setMarket(String market) {
//                this.market = market;
//            }
//
//            public String getNum() {
//                return num;
//            }
//
//            public void setNum(String num) {
//                this.num = num;
//            }
//        }
//    }

}
