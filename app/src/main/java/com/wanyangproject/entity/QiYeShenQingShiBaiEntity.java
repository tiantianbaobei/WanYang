package com.wanyangproject.entity;

import java.util.List;

/**
 * Created by 甜甜 on 2018/8/30.
 */

public class QiYeShenQingShiBaiEntity {

    /**
     * code : 400
     * msg : 您已申请过本优惠！
     * response : []
     * request : {"parkId":"1","Preferences_id":"2","money":"666","nember":"55","permanent":"1","Remarks":"123"}
     * other : []
     */

    private int code;
    private String msg;
    private RequestBean request;
    private List<?> response;
    private List<?> other;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public RequestBean getRequest() {
        return request;
    }

    public void setRequest(RequestBean request) {
        this.request = request;
    }

    public List<?> getResponse() {
        return response;
    }

    public void setResponse(List<?> response) {
        this.response = response;
    }

    public List<?> getOther() {
        return other;
    }

    public void setOther(List<?> other) {
        this.other = other;
    }

    public static class RequestBean {
        /**
         * parkId : 1
         * Preferences_id : 2
         * money : 666
         * nember : 55
         * permanent : 1
         * Remarks : 123
         */

        private String parkId;
        private String Preferences_id;
        private String money;
        private String nember;
        private String permanent;
        private String Remarks;

        public String getParkId() {
            return parkId;
        }

        public void setParkId(String parkId) {
            this.parkId = parkId;
        }

        public String getPreferences_id() {
            return Preferences_id;
        }

        public void setPreferences_id(String Preferences_id) {
            this.Preferences_id = Preferences_id;
        }

        public String getMoney() {
            return money;
        }

        public void setMoney(String money) {
            this.money = money;
        }

        public String getNember() {
            return nember;
        }

        public void setNember(String nember) {
            this.nember = nember;
        }

        public String getPermanent() {
            return permanent;
        }

        public void setPermanent(String permanent) {
            this.permanent = permanent;
        }

        public String getRemarks() {
            return Remarks;
        }

        public void setRemarks(String Remarks) {
            this.Remarks = Remarks;
        }
    }
}
