package com.wanyangproject.entity;

import java.util.List;

/**
 * Created by 甜甜 on 2018/9/2.
 */

public class PingLunChengGongEntity {
    /**
     * code : 200
     * msg : 发表成功
     * response : [{"id":"56","content":"1111123345667","add_time":"0秒前","upInfo":[],"uinfo":{"userid":"2","avatar":"http://park.hostop.net/static/image/logo.jpg","nickname":"未填写","username":"17600904682"},"canReply":"1"},{"id":"53","content":"1111111111111111111111","add_time":"57分钟前","upInfo":[],"uinfo":{"userid":"2","avatar":"http://park.hostop.net/static/image/logo.jpg","nickname":"未填写","username":"17600904682"},"canReply":"1"},{"id":"52","content":"00000000","add_time":"2小时前","upInfo":[],"uinfo":{"userid":"2","avatar":"http://park.hostop.net/static/image/logo.jpg","nickname":"未填写","username":"17600904682"},"canReply":"1"},{"id":"51","content":"123","add_time":"2小时前","upInfo":[],"uinfo":{"userid":"2","avatar":"http://park.hostop.net/static/image/logo.jpg","nickname":"未填写","username":"17600904682"},"canReply":"1"},{"id":"50","content":"哈哈哈","add_time":"2小时前","upInfo":[{"id":"51","content":"123","add_time":"2小时前","uinfo":{"userid":"2","avatar":"http://park.hostop.net/static/image/logo.jpg","nickname":"未填写","username":"17600904682"},"canReply":"0"}],"uinfo":{"userid":"2","avatar":"http://park.hostop.net/static/image/logo.jpg","nickname":"未填写","username":"17600904682"},"canReply":"1"},{"id":"44","content":"扣扣咯吐了0","add_time":"4小时前","upInfo":[{"id":"56","content":"1111123345667","add_time":"0秒前","uinfo":{"userid":"2","avatar":"http://park.hostop.net/static/image/logo.jpg","nickname":"未填写","username":"17600904682"},"canReply":"0"},{"id":"50","content":"哈哈哈","add_time":"2小时前","uinfo":{"userid":"2","avatar":"http://park.hostop.net/static/image/logo.jpg","nickname":"未填写","username":"17600904682"},"canReply":"0"}],"uinfo":{"userid":"2","avatar":"http://park.hostop.net/static/image/logo.jpg","nickname":"未填写","username":"17600904682"},"canReply":"1"},{"id":"43","content":"陪着我","add_time":"4小时前","upInfo":[{"id":"53","content":"1111111111111111111111","add_time":"57分钟前","uinfo":{"userid":"2","avatar":"http://park.hostop.net/static/image/logo.jpg","nickname":"未填写","username":"17600904682"},"canReply":"0"},{"id":"52","content":"00000000","add_time":"2小时前","uinfo":{"userid":"2","avatar":"http://park.hostop.net/static/image/logo.jpg","nickname":"未填写","username":"17600904682"},"canReply":"0"}],"uinfo":{"userid":"2","avatar":"http://park.hostop.net/static/image/logo.jpg","nickname":"未填写","username":"17600904682"},"canReply":"1"},{"id":"42","content":"好的","add_time":"4小时前","upInfo":[],"uinfo":{"userid":"8","avatar":"http://park.hostop.net/static/image/logo.jpg","nickname":"未填写","username":"13176688711"},"canReply":"1"}]
     * request : {"content":"1111123345667","forumId":"66","upId":"44"}
     * other : []
     */

    private int code;
    private String msg;
    private RequestBean request;
    private List<ResponseBean> response;
    private List<?> other;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public RequestBean getRequest() {
        return request;
    }

    public void setRequest(RequestBean request) {
        this.request = request;
    }

    public List<ResponseBean> getResponse() {
        return response;
    }

    public void setResponse(List<ResponseBean> response) {
        this.response = response;
    }

    public List<?> getOther() {
        return other;
    }

    public void setOther(List<?> other) {
        this.other = other;
    }

    public static class RequestBean {
        /**
         * content : 1111123345667
         * forumId : 66
         * upId : 44
         */

        private String content;
        private String forumId;
        private String upId;

        public String getContent() {
            return content;
        }

        public void setContent(String content) {
            this.content = content;
        }

        public String getForumId() {
            return forumId;
        }

        public void setForumId(String forumId) {
            this.forumId = forumId;
        }

        public String getUpId() {
            return upId;
        }

        public void setUpId(String upId) {
            this.upId = upId;
        }
    }

    public static class ResponseBean {
        /**
         * id : 56
         * content : 1111123345667
         * add_time : 0秒前
         * upInfo : []
         * uinfo : {"userid":"2","avatar":"http://park.hostop.net/static/image/logo.jpg","nickname":"未填写","username":"17600904682"}
         * canReply : 1
         */

        private String id;
        private String content;
        private String add_time;
        private UinfoBean uinfo;
        private String canReply;
        private List<?> upInfo;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getContent() {
            return content;
        }

        public void setContent(String content) {
            this.content = content;
        }

        public String getAdd_time() {
            return add_time;
        }

        public void setAdd_time(String add_time) {
            this.add_time = add_time;
        }

        public UinfoBean getUinfo() {
            return uinfo;
        }

        public void setUinfo(UinfoBean uinfo) {
            this.uinfo = uinfo;
        }

        public String getCanReply() {
            return canReply;
        }

        public void setCanReply(String canReply) {
            this.canReply = canReply;
        }

        public List<?> getUpInfo() {
            return upInfo;
        }

        public void setUpInfo(List<?> upInfo) {
            this.upInfo = upInfo;
        }

        public static class UinfoBean {
            /**
             * userid : 2
             * avatar : http://park.hostop.net/static/image/logo.jpg
             * nickname : 未填写
             * username : 17600904682
             */

            private String userid;
            private String avatar;
            private String nickname;
            private String username;

            public String getUserid() {
                return userid;
            }

            public void setUserid(String userid) {
                this.userid = userid;
            }

            public String getAvatar() {
                return avatar;
            }

            public void setAvatar(String avatar) {
                this.avatar = avatar;
            }

            public String getNickname() {
                return nickname;
            }

            public void setNickname(String nickname) {
                this.nickname = nickname;
            }

            public String getUsername() {
                return username;
            }

            public void setUsername(String username) {
                this.username = username;
            }
        }
    }


//    /**
//     * code : 200
//     * msg : 发表成功
//     * response : [{"id":"51","content":"123","add_time":"0秒前","upInfo":{"id":"50","content":"哈哈哈","add_time":"3分钟前","uinfo":{"userid":"2","avatar":"http://park.hostop.net/static/image/logo.jpg","nickname":"未填写","username":"17600904682"}},"status":"1","uinfo":{"userid":"2","avatar":"http://park.hostop.net/static/image/logo.jpg","nickname":"未填写","username":"17600904682"},"canReply":"0"},{"id":"50","content":"哈哈哈","add_time":"3分钟前","upInfo":{"id":"44","content":"扣扣咯吐了0","add_time":"3小时前","uinfo":{"userid":"2","avatar":"http://park.hostop.net/static/image/logo.jpg","nickname":"未填写","username":"17600904682"}},"status":"1","uinfo":{"userid":"2","avatar":"http://park.hostop.net/static/image/logo.jpg","nickname":"未填写","username":"17600904682"},"canReply":"0"},{"id":"44","content":"扣扣咯吐了0","add_time":"3小时前","upInfo":[],"status":"1","uinfo":{"userid":"2","avatar":"http://park.hostop.net/static/image/logo.jpg","nickname":"未填写","username":"17600904682"},"canReply":"1"},{"id":"43","content":"陪着我","add_time":"3小时前","upInfo":[],"status":"1","uinfo":{"userid":"2","avatar":"http://park.hostop.net/static/image/logo.jpg","nickname":"未填写","username":"17600904682"},"canReply":"1"},{"id":"42","content":"好的","add_time":"3小时前","upInfo":[],"status":"1","uinfo":{"userid":"8","avatar":"http://park.hostop.net/static/image/logo.jpg","nickname":"未填写","username":"13176688711"},"canReply":"1"}]
//     * request : {"content":"123","forumId":"66","upId":"50"}
//     * other : []
//     */
//
//    private int code;
//    private String msg;
//    private RequestBean request;
//    private List<ResponseBean> response;
//    private List<?> other;
//
//    public int getCode() {
//        return code;
//    }
//
//    public void setCode(int code) {
//        this.code = code;
//    }
//
//    public String getMsg() {
//        return msg;
//    }
//
//    public void setMsg(String msg) {
//        this.msg = msg;
//    }
//
//    public RequestBean getRequest() {
//        return request;
//    }
//
//    public void setRequest(RequestBean request) {
//        this.request = request;
//    }
//
//    public List<ResponseBean> getResponse() {
//        return response;
//    }
//
//    public void setResponse(List<ResponseBean> response) {
//        this.response = response;
//    }
//
//    public List<?> getOther() {
//        return other;
//    }
//
//    public void setOther(List<?> other) {
//        this.other = other;
//    }
//
//    public static class RequestBean {
//        /**
//         * content : 123
//         * forumId : 66
//         * upId : 50
//         */
//
//        private String content;
//        private String forumId;
//        private String upId;
//
//        public String getContent() {
//            return content;
//        }
//
//        public void setContent(String content) {
//            this.content = content;
//        }
//
//        public String getForumId() {
//            return forumId;
//        }
//
//        public void setForumId(String forumId) {
//            this.forumId = forumId;
//        }
//
//        public String getUpId() {
//            return upId;
//        }
//
//        public void setUpId(String upId) {
//            this.upId = upId;
//        }
//    }
//
//    public static class ResponseBean {
//        /**
//         * id : 51
//         * content : 123
//         * add_time : 0秒前
//         * upInfo : {"id":"50","content":"哈哈哈","add_time":"3分钟前","uinfo":{"userid":"2","avatar":"http://park.hostop.net/static/image/logo.jpg","nickname":"未填写","username":"17600904682"}}
//         * status : 1
//         * uinfo : {"userid":"2","avatar":"http://park.hostop.net/static/image/logo.jpg","nickname":"未填写","username":"17600904682"}
//         * canReply : 0
//         */
//
//        private String id;
//        private String content;
//        private String add_time;
//        private UpInfoBean upInfo;
//        private String status;
//        private UinfoBeanX uinfo;
//        private String canReply;
//
//        public String getId() {
//            return id;
//        }
//
//        public void setId(String id) {
//            this.id = id;
//        }
//
//        public String getContent() {
//            return content;
//        }
//
//        public void setContent(String content) {
//            this.content = content;
//        }
//
//        public String getAdd_time() {
//            return add_time;
//        }
//
//        public void setAdd_time(String add_time) {
//            this.add_time = add_time;
//        }
//
//        public UpInfoBean getUpInfo() {
//            return upInfo;
//        }
//
//        public void setUpInfo(UpInfoBean upInfo) {
//            this.upInfo = upInfo;
//        }
//
//        public String getStatus() {
//            return status;
//        }
//
//        public void setStatus(String status) {
//            this.status = status;
//        }
//
//        public UinfoBeanX getUinfo() {
//            return uinfo;
//        }
//
//        public void setUinfo(UinfoBeanX uinfo) {
//            this.uinfo = uinfo;
//        }
//
//        public String getCanReply() {
//            return canReply;
//        }
//
//        public void setCanReply(String canReply) {
//            this.canReply = canReply;
//        }
//
//        public static class UpInfoBean {
//            /**
//             * id : 50
//             * content : 哈哈哈
//             * add_time : 3分钟前
//             * uinfo : {"userid":"2","avatar":"http://park.hostop.net/static/image/logo.jpg","nickname":"未填写","username":"17600904682"}
//             */
//
//            private String id;
//            private String content;
//            private String add_time;
//            private UinfoBean uinfo;
//
//            public String getId() {
//                return id;
//            }
//
//            public void setId(String id) {
//                this.id = id;
//            }
//
//            public String getContent() {
//                return content;
//            }
//
//            public void setContent(String content) {
//                this.content = content;
//            }
//
//            public String getAdd_time() {
//                return add_time;
//            }
//
//            public void setAdd_time(String add_time) {
//                this.add_time = add_time;
//            }
//
//            public UinfoBean getUinfo() {
//                return uinfo;
//            }
//
//            public void setUinfo(UinfoBean uinfo) {
//                this.uinfo = uinfo;
//            }
//
//            public static class UinfoBean {
//                /**
//                 * userid : 2
//                 * avatar : http://park.hostop.net/static/image/logo.jpg
//                 * nickname : 未填写
//                 * username : 17600904682
//                 */
//
//                private String userid;
//                private String avatar;
//                private String nickname;
//                private String username;
//
//                public String getUserid() {
//                    return userid;
//                }
//
//                public void setUserid(String userid) {
//                    this.userid = userid;
//                }
//
//                public String getAvatar() {
//                    return avatar;
//                }
//
//                public void setAvatar(String avatar) {
//                    this.avatar = avatar;
//                }
//
//                public String getNickname() {
//                    return nickname;
//                }
//
//                public void setNickname(String nickname) {
//                    this.nickname = nickname;
//                }
//
//                public String getUsername() {
//                    return username;
//                }
//
//                public void setUsername(String username) {
//                    this.username = username;
//                }
//            }
//        }
//
//        public static class UinfoBeanX {
//            /**
//             * userid : 2
//             * avatar : http://park.hostop.net/static/image/logo.jpg
//             * nickname : 未填写
//             * username : 17600904682
//             */
//
//            private String userid;
//            private String avatar;
//            private String nickname;
//            private String username;
//
//            public String getUserid() {
//                return userid;
//            }
//
//            public void setUserid(String userid) {
//                this.userid = userid;
//            }
//
//            public String getAvatar() {
//                return avatar;
//            }
//
//            public void setAvatar(String avatar) {
//                this.avatar = avatar;
//            }
//
//            public String getNickname() {
//                return nickname;
//            }
//
//            public void setNickname(String nickname) {
//                this.nickname = nickname;
//            }
//
//            public String getUsername() {
//                return username;
//            }
//
//            public void setUsername(String username) {
//                this.username = username;
//            }
//        }
//    }
}
