package com.wanyangproject.entity;

import java.util.List;

/**
 * Created by 甜甜 on 2018/9/2.
 */

public class QiYeFuWuLiJiShenQingEntity {
    /**
     * code : 200
     * msg :
     * response : {"id":"317","jiid":"553"}
     * request : {"parkId":"1","Preferences_id":"166","typeId":"2","Remarks":"测试","name":"王振云","phone":"15810643251"}
     * other : []
     */

    private int code;
    private String msg;
    private ResponseBean response;
    private RequestBean request;
    private List<?> other;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public ResponseBean getResponse() {
        return response;
    }

    public void setResponse(ResponseBean response) {
        this.response = response;
    }

    public RequestBean getRequest() {
        return request;
    }

    public void setRequest(RequestBean request) {
        this.request = request;
    }

    public List<?> getOther() {
        return other;
    }

    public void setOther(List<?> other) {
        this.other = other;
    }

    public static class ResponseBean {
        /**
         * id : 317
         * jiid : 553
         */

        private String id;
        private String jiid;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getJiid() {
            return jiid;
        }

        public void setJiid(String jiid) {
            this.jiid = jiid;
        }
    }

    public static class RequestBean {
        /**
         * parkId : 1
         * Preferences_id : 166
         * typeId : 2
         * Remarks : 测试
         * name : 王振云
         * phone : 15810643251
         */

        private String parkId;
        private String Preferences_id;
        private String typeId;
        private String Remarks;
        private String name;
        private String phone;

        public String getParkId() {
            return parkId;
        }

        public void setParkId(String parkId) {
            this.parkId = parkId;
        }

        public String getPreferences_id() {
            return Preferences_id;
        }

        public void setPreferences_id(String Preferences_id) {
            this.Preferences_id = Preferences_id;
        }

        public String getTypeId() {
            return typeId;
        }

        public void setTypeId(String typeId) {
            this.typeId = typeId;
        }

        public String getRemarks() {
            return Remarks;
        }

        public void setRemarks(String Remarks) {
            this.Remarks = Remarks;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getPhone() {
            return phone;
        }

        public void setPhone(String phone) {
            this.phone = phone;
        }
    }


//    /**
//     * code : 200
//     * msg :
//     * response : {"id":"27"}
//     * request : {"parkId":"1","Preferences_id":"7","typeId":"2","Remarks":"没有啦","name":"天空","phone":"15756685365"}
//     * other : []
//     */
//
//    private int code;
//    private String msg;
//    private ResponseBean response;
//    private RequestBean request;
//    private List<?> other;
//
//    public int getCode() {
//        return code;
//    }
//
//    public void setCode(int code) {
//        this.code = code;
//    }
//
//    public String getMsg() {
//        return msg;
//    }
//
//    public void setMsg(String msg) {
//        this.msg = msg;
//    }
//
//    public ResponseBean getResponse() {
//        return response;
//    }
//
//    public void setResponse(ResponseBean response) {
//        this.response = response;
//    }
//
//    public RequestBean getRequest() {
//        return request;
//    }
//
//    public void setRequest(RequestBean request) {
//        this.request = request;
//    }
//
//    public List<?> getOther() {
//        return other;
//    }
//
//    public void setOther(List<?> other) {
//        this.other = other;
//    }
//
//    public static class ResponseBean {
//        /**
//         * id : 27
//         */
//
//        private String id;
//
//        public String getId() {
//            return id;
//        }
//
//        public void setId(String id) {
//            this.id = id;
//        }
//    }
//
//    public static class RequestBean {
//        /**
//         * parkId : 1
//         * Preferences_id : 7
//         * typeId : 2
//         * Remarks : 没有啦
//         * name : 天空
//         * phone : 15756685365
//         */
//
//        private String parkId;
//        private String Preferences_id;
//        private String typeId;
//        private String Remarks;
//        private String name;
//        private String phone;
//
//        public String getParkId() {
//            return parkId;
//        }
//
//        public void setParkId(String parkId) {
//            this.parkId = parkId;
//        }
//
//        public String getPreferences_id() {
//            return Preferences_id;
//        }
//
//        public void setPreferences_id(String Preferences_id) {
//            this.Preferences_id = Preferences_id;
//        }
//
//        public String getTypeId() {
//            return typeId;
//        }
//
//        public void setTypeId(String typeId) {
//            this.typeId = typeId;
//        }
//
//        public String getRemarks() {
//            return Remarks;
//        }
//
//        public void setRemarks(String Remarks) {
//            this.Remarks = Remarks;
//        }
//
//        public String getName() {
//            return name;
//        }
//
//        public void setName(String name) {
//            this.name = name;
//        }
//
//        public String getPhone() {
//            return phone;
//        }
//
//        public void setPhone(String phone) {
//            this.phone = phone;
//        }
//    }
}
