package com.wanyangproject.entity;

import java.util.List;

/**
 * Created by 甜甜 on 2018/8/10.
 */

public class TieZiXiangQingEntity {
    /**
     * code : 200
     * msg :
     * response : {"data":{"id":"66","content":"口渴","add_time":"18小时前","uinfo":{"userid":"4","avatar":"http://park.hostop.net/static/image/logo.jpg","nickname":"未填写","username":"15275411070"},"imgArr":["http://park.hostop.net/upload/20180901/bbd6fce79991700be50fb74fd273e284.png"],"reply":"10","like":"1","isLike":"1"},"replyArr":[{"id":"43","content":"陪着我","add_time":"4小时前","upInfo":[{"id":"58","content":"99999999999999","add_time":"0秒前","uinfo":{"userid":"2","avatar":"http://park.hostop.net/static/image/logo.jpg","nickname":"未填写","username":"17600904682"},"canReply":"0"},{"id":"57","content":"你在干嘛","add_time":"3分钟前","uinfo":{"userid":"2","avatar":"http://park.hostop.net/static/image/logo.jpg","nickname":"未填写","username":"17600904682"},"canReply":"0"},{"id":"53","content":"1111111111111111111111","add_time":"2小时前","uinfo":{"userid":"2","avatar":"http://park.hostop.net/static/image/logo.jpg","nickname":"未填写","username":"17600904682"},"canReply":"0"},{"id":"52","content":"00000000","add_time":"2小时前","uinfo":{"userid":"2","avatar":"http://park.hostop.net/static/image/logo.jpg","nickname":"未填写","username":"17600904682"},"canReply":"0"}],"uinfo":{"userid":"2","avatar":"http://park.hostop.net/static/image/logo.jpg","nickname":"未填写","username":"17600904682"},"canReply":"1"},{"id":"42","content":"好的","add_time":"4小时前","upInfo":[],"uinfo":{"userid":"8","avatar":"http://park.hostop.net/static/image/logo.jpg","nickname":"未填写","username":"13176688711"},"canReply":"1"}],"likeArr":[{"id":"147","uinfo":{"userid":"2","avatar":"http://park.hostop.net/static/image/logo.jpg","nickname":"未填写","username":"17600904682"}}]}
     * request : {"id":"66"}
     * other : []
     */

    private int code;
    private String msg;
    private ResponseBean response;
    private RequestBean request;
    private List<?> other;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public ResponseBean getResponse() {
        return response;
    }

    public void setResponse(ResponseBean response) {
        this.response = response;
    }

    public RequestBean getRequest() {
        return request;
    }

    public void setRequest(RequestBean request) {
        this.request = request;
    }

    public List<?> getOther() {
        return other;
    }

    public void setOther(List<?> other) {
        this.other = other;
    }

    public static class ResponseBean {
        /**
         * data : {"id":"66","content":"口渴","add_time":"18小时前","uinfo":{"userid":"4","avatar":"http://park.hostop.net/static/image/logo.jpg","nickname":"未填写","username":"15275411070"},"imgArr":["http://park.hostop.net/upload/20180901/bbd6fce79991700be50fb74fd273e284.png"],"reply":"10","like":"1","isLike":"1"}
         * replyArr : [{"id":"43","content":"陪着我","add_time":"4小时前","upInfo":[{"id":"58","content":"99999999999999","add_time":"0秒前","uinfo":{"userid":"2","avatar":"http://park.hostop.net/static/image/logo.jpg","nickname":"未填写","username":"17600904682"},"canReply":"0"},{"id":"57","content":"你在干嘛","add_time":"3分钟前","uinfo":{"userid":"2","avatar":"http://park.hostop.net/static/image/logo.jpg","nickname":"未填写","username":"17600904682"},"canReply":"0"},{"id":"53","content":"1111111111111111111111","add_time":"2小时前","uinfo":{"userid":"2","avatar":"http://park.hostop.net/static/image/logo.jpg","nickname":"未填写","username":"17600904682"},"canReply":"0"},{"id":"52","content":"00000000","add_time":"2小时前","uinfo":{"userid":"2","avatar":"http://park.hostop.net/static/image/logo.jpg","nickname":"未填写","username":"17600904682"},"canReply":"0"}],"uinfo":{"userid":"2","avatar":"http://park.hostop.net/static/image/logo.jpg","nickname":"未填写","username":"17600904682"},"canReply":"1"},{"id":"42","content":"好的","add_time":"4小时前","upInfo":[],"uinfo":{"userid":"8","avatar":"http://park.hostop.net/static/image/logo.jpg","nickname":"未填写","username":"13176688711"},"canReply":"1"}]
         * likeArr : [{"id":"147","uinfo":{"userid":"2","avatar":"http://park.hostop.net/static/image/logo.jpg","nickname":"未填写","username":"17600904682"}}]
         */

        private DataBean data;
        private List<ReplyArrBean> replyArr;
        private List<LikeArrBean> likeArr;

        public DataBean getData() {
            return data;
        }

        public void setData(DataBean data) {
            this.data = data;
        }

        public List<ReplyArrBean> getReplyArr() {
            return replyArr;
        }

        public void setReplyArr(List<ReplyArrBean> replyArr) {
            this.replyArr = replyArr;
        }

        public List<LikeArrBean> getLikeArr() {
            return likeArr;
        }

        public void setLikeArr(List<LikeArrBean> likeArr) {
            this.likeArr = likeArr;
        }

        public static class DataBean {
            /**
             * id : 66
             * content : 口渴
             * add_time : 18小时前
             * uinfo : {"userid":"4","avatar":"http://park.hostop.net/static/image/logo.jpg","nickname":"未填写","username":"15275411070"}
             * imgArr : ["http://park.hostop.net/upload/20180901/bbd6fce79991700be50fb74fd273e284.png"]
             * reply : 10
             * like : 1
             * isLike : 1
             * title
             */

            private String id;
            private String title;
            private String content;
            private String add_time;
            private UinfoBean uinfo;
            private String reply;
            private String like;
            private String isLike;
            private List<String> imgArr;


            public String getTitle() {
                return title;
            }

            public void setTitle(String title) {
                this.title = title;
            }

            public String getId() {
                return id;
            }

            public void setId(String id) {
                this.id = id;
            }

            public String getContent() {
                return content;
            }

            public void setContent(String content) {
                this.content = content;
            }

            public String getAdd_time() {
                return add_time;
            }

            public void setAdd_time(String add_time) {
                this.add_time = add_time;
            }

            public UinfoBean getUinfo() {
                return uinfo;
            }

            public void setUinfo(UinfoBean uinfo) {
                this.uinfo = uinfo;
            }

            public String getReply() {
                return reply;
            }

            public void setReply(String reply) {
                this.reply = reply;
            }

            public String getLike() {
                return like;
            }

            public void setLike(String like) {
                this.like = like;
            }

            public String getIsLike() {
                return isLike;
            }

            public void setIsLike(String isLike) {
                this.isLike = isLike;
            }

            public List<String> getImgArr() {
                return imgArr;
            }

            public void setImgArr(List<String> imgArr) {
                this.imgArr = imgArr;
            }

            public static class UinfoBean {
                /**
                 * userid : 4
                 * avatar : http://park.hostop.net/static/image/logo.jpg
                 * nickname : 未填写
                 * username : 15275411070
                 */

                private String userid;
                private String avatar;
                private String nickname;
                private String username;

                public String getUserid() {
                    return userid;
                }

                public void setUserid(String userid) {
                    this.userid = userid;
                }

                public String getAvatar() {
                    return avatar;
                }

                public void setAvatar(String avatar) {
                    this.avatar = avatar;
                }

                public String getNickname() {
                    return nickname;
                }

                public void setNickname(String nickname) {
                    this.nickname = nickname;
                }

                public String getUsername() {
                    return username;
                }

                public void setUsername(String username) {
                    this.username = username;
                }
            }
        }

        public static class ReplyArrBean {
            /**
             * id : 43
             * content : 陪着我
             * add_time : 4小时前
             * upInfo : [{"id":"58","content":"99999999999999","add_time":"0秒前","uinfo":{"userid":"2","avatar":"http://park.hostop.net/static/image/logo.jpg","nickname":"未填写","username":"17600904682"},"canReply":"0"},{"id":"57","content":"你在干嘛","add_time":"3分钟前","uinfo":{"userid":"2","avatar":"http://park.hostop.net/static/image/logo.jpg","nickname":"未填写","username":"17600904682"},"canReply":"0"},{"id":"53","content":"1111111111111111111111","add_time":"2小时前","uinfo":{"userid":"2","avatar":"http://park.hostop.net/static/image/logo.jpg","nickname":"未填写","username":"17600904682"},"canReply":"0"},{"id":"52","content":"00000000","add_time":"2小时前","uinfo":{"userid":"2","avatar":"http://park.hostop.net/static/image/logo.jpg","nickname":"未填写","username":"17600904682"},"canReply":"0"}]
             * uinfo : {"userid":"2","avatar":"http://park.hostop.net/static/image/logo.jpg","nickname":"未填写","username":"17600904682"}
             * canReply : 1
             */

            private String id;
            private String content;
            private String add_time;
            private UinfoBeanX uinfo;
            private String canReply;
            private List<UpInfoBean> upInfo;

            public String getId() {
                return id;
            }

            public void setId(String id) {
                this.id = id;
            }

            public String getContent() {
                return content;
            }

            public void setContent(String content) {
                this.content = content;
            }

            public String getAdd_time() {
                return add_time;
            }

            public void setAdd_time(String add_time) {
                this.add_time = add_time;
            }

            public UinfoBeanX getUinfo() {
                return uinfo;
            }

            public void setUinfo(UinfoBeanX uinfo) {
                this.uinfo = uinfo;
            }

            public String getCanReply() {
                return canReply;
            }

            public void setCanReply(String canReply) {
                this.canReply = canReply;
            }

            public List<UpInfoBean> getUpInfo() {
                return upInfo;
            }

            public void setUpInfo(List<UpInfoBean> upInfo) {
                this.upInfo = upInfo;
            }

            public static class UinfoBeanX {
                /**
                 * userid : 2
                 * avatar : http://park.hostop.net/static/image/logo.jpg
                 * nickname : 未填写
                 * username : 17600904682
                 */

                private String userid;
                private String avatar;
                private String nickname;
                private String username;

                public String getUserid() {
                    return userid;
                }

                public void setUserid(String userid) {
                    this.userid = userid;
                }

                public String getAvatar() {
                    return avatar;
                }

                public void setAvatar(String avatar) {
                    this.avatar = avatar;
                }

                public String getNickname() {
                    return nickname;
                }

                public void setNickname(String nickname) {
                    this.nickname = nickname;
                }

                public String getUsername() {
                    return username;
                }

                public void setUsername(String username) {
                    this.username = username;
                }
            }

            public static class UpInfoBean {
                /**
                 * id : 58
                 * content : 99999999999999
                 * add_time : 0秒前
                 * uinfo : {"userid":"2","avatar":"http://park.hostop.net/static/image/logo.jpg","nickname":"未填写","username":"17600904682"}
                 * canReply : 0
                 */

                private String id;
                private String content;
                private String add_time;
                private UinfoBeanXX uinfo;
                private String canReply;

                public String getId() {
                    return id;
                }

                public void setId(String id) {
                    this.id = id;
                }

                public String getContent() {
                    return content;
                }

                public void setContent(String content) {
                    this.content = content;
                }

                public String getAdd_time() {
                    return add_time;
                }

                public void setAdd_time(String add_time) {
                    this.add_time = add_time;
                }

                public UinfoBeanXX getUinfo() {
                    return uinfo;
                }

                public void setUinfo(UinfoBeanXX uinfo) {
                    this.uinfo = uinfo;
                }

                public String getCanReply() {
                    return canReply;
                }

                public void setCanReply(String canReply) {
                    this.canReply = canReply;
                }

                public static class UinfoBeanXX {
                    /**
                     * userid : 2
                     * avatar : http://park.hostop.net/static/image/logo.jpg
                     * nickname : 未填写
                     * username : 17600904682
                     */

                    private String userid;
                    private String avatar;
                    private String nickname;
                    private String username;

                    public String getUserid() {
                        return userid;
                    }

                    public void setUserid(String userid) {
                        this.userid = userid;
                    }

                    public String getAvatar() {
                        return avatar;
                    }

                    public void setAvatar(String avatar) {
                        this.avatar = avatar;
                    }

                    public String getNickname() {
                        return nickname;
                    }

                    public void setNickname(String nickname) {
                        this.nickname = nickname;
                    }

                    public String getUsername() {
                        return username;
                    }

                    public void setUsername(String username) {
                        this.username = username;
                    }
                }
            }
        }

        public static class LikeArrBean {
            /**
             * id : 147
             * uinfo : {"userid":"2","avatar":"http://park.hostop.net/static/image/logo.jpg","nickname":"未填写","username":"17600904682"}
             */

            private String id;
            private UinfoBeanXXX uinfo;

            public String getId() {
                return id;
            }

            public void setId(String id) {
                this.id = id;
            }

            public UinfoBeanXXX getUinfo() {
                return uinfo;
            }

            public void setUinfo(UinfoBeanXXX uinfo) {
                this.uinfo = uinfo;
            }

            public static class UinfoBeanXXX {
                /**
                 * userid : 2
                 * avatar : http://park.hostop.net/static/image/logo.jpg
                 * nickname : 未填写
                 * username : 17600904682
                 */

                private String userid;
                private String avatar;
                private String nickname;
                private String username;

                public String getUserid() {
                    return userid;
                }

                public void setUserid(String userid) {
                    this.userid = userid;
                }

                public String getAvatar() {
                    return avatar;
                }

                public void setAvatar(String avatar) {
                    this.avatar = avatar;
                }

                public String getNickname() {
                    return nickname;
                }

                public void setNickname(String nickname) {
                    this.nickname = nickname;
                }

                public String getUsername() {
                    return username;
                }

                public void setUsername(String username) {
                    this.username = username;
                }
            }
        }
    }

    public static class RequestBean {
        /**
         * id : 66
         */

        private String id;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }
    }


//
//    /**
//     * code : 200
//     * msg :
//     * response : {"data":{"id":"1","content":"园区樱花开了。樱花花色幽香艳丽，为早春重要的观花树种，盛开时节花繁艳丽，满树烂漫，如云似霞，极为壮观。","add_time":"4天前","uinfo":{"userid":"1","avatar":"http://park.hostop.net/static/image/avatar.jpg","nickname":"未填写"},"imgArr":["http://park.hostop.net//upload/20180807/756809b8966c1c1073ef5d387a7826c4.jpg","http://park.hostop.net//upload/20180807/756809b8966c1c1073ef5d387a7826c4.jpg","http://park.hostop.net//upload/20180807/756809b8966c1c1073ef5d387a7826c4.jpg",""],"reply":"0","like":"0","isLike":"1"},"replyArr":[{"id":"2","content":"这是回复的第一条","add_time":"3天前","upInfo":{"id":"1","content":"很棒啊","add_time":"4天前","uinfo":{"userid":"1","avatar":"http://park.hostop.net/static/image/avatar.jpg","nickname":"未填写"}},"uinfo":{"userid":"1","avatar":"http://park.hostop.net/static/image/avatar.jpg","nickname":"未填写"},"canReply":"0"},{"id":"1","content":"很棒啊","add_time":"4天前","upInfo":[],"uinfo":{"userid":"1","avatar":"http://park.hostop.net/static/image/avatar.jpg","nickname":"未填写"},"canReply":"1"}],"likeArr":[{"id":"2","uinfo":{"userid":"2","avatar":"http://park.hostop.net/static/image/avatar.jpg","nickname":"未填写"}}]}
//     * request : {"id":"1"}
//     * other : []
//     */
//
//    private int code;
//    private String msg;
//    private ResponseBean response;
//    private RequestBean request;
//    private List<?> other;
//
//    public int getCode() {
//        return code;
//    }
//
//    public void setCode(int code) {
//        this.code = code;
//    }
//
//    public String getMsg() {
//        return msg;
//    }
//
//    public void setMsg(String msg) {
//        this.msg = msg;
//    }
//
//    public ResponseBean getResponse() {
//        return response;
//    }
//
//    public void setResponse(ResponseBean response) {
//        this.response = response;
//    }
//
//    public RequestBean getRequest() {
//        return request;
//    }
//
//    public void setRequest(RequestBean request) {
//        this.request = request;
//    }
//
//    public List<?> getOther() {
//        return other;
//    }
//
//    public void setOther(List<?> other) {
//        this.other = other;
//    }
//
//    public static class ResponseBean {
//        /**
//         * data : {"id":"1","content":"园区樱花开了。樱花花色幽香艳丽，为早春重要的观花树种，盛开时节花繁艳丽，满树烂漫，如云似霞，极为壮观。","add_time":"4天前","uinfo":{"userid":"1","avatar":"http://park.hostop.net/static/image/avatar.jpg","nickname":"未填写"},"imgArr":["http://park.hostop.net//upload/20180807/756809b8966c1c1073ef5d387a7826c4.jpg","http://park.hostop.net//upload/20180807/756809b8966c1c1073ef5d387a7826c4.jpg","http://park.hostop.net//upload/20180807/756809b8966c1c1073ef5d387a7826c4.jpg",""],"reply":"0","like":"0","isLike":"1"}
//         * replyArr : [{"id":"2","content":"这是回复的第一条","add_time":"3天前","upInfo":{"id":"1","content":"很棒啊","add_time":"4天前","uinfo":{"userid":"1","avatar":"http://park.hostop.net/static/image/avatar.jpg","nickname":"未填写"}},"uinfo":{"userid":"1","avatar":"http://park.hostop.net/static/image/avatar.jpg","nickname":"未填写"},"canReply":"0"},{"id":"1","content":"很棒啊","add_time":"4天前","upInfo":[],"uinfo":{"userid":"1","avatar":"http://park.hostop.net/static/image/avatar.jpg","nickname":"未填写"},"canReply":"1"}]
//         * likeArr : [{"id":"2","uinfo":{"userid":"2","avatar":"http://park.hostop.net/static/image/avatar.jpg","nickname":"未填写"}}]
//         */
//
//        private DataBean data;
//        private List<ReplyArrBean> replyArr;
//        private List<LikeArrBean> likeArr;
//
//        public DataBean getData() {
//            return data;
//        }
//
//        public void setData(DataBean data) {
//            this.data = data;
//        }
//
//        public List<ReplyArrBean> getReplyArr() {
//            return replyArr;
//        }
//
//        public void setReplyArr(List<ReplyArrBean> replyArr) {
//            this.replyArr = replyArr;
//        }
//
//        public List<LikeArrBean> getLikeArr() {
//            return likeArr;
//        }
//
//        public void setLikeArr(List<LikeArrBean> likeArr) {
//            this.likeArr = likeArr;
//        }
//
//        public static class DataBean {
//            /**
//             * id : 1
//             * content : 园区樱花开了。樱花花色幽香艳丽，为早春重要的观花树种，盛开时节花繁艳丽，满树烂漫，如云似霞，极为壮观。
//             * add_time : 4天前
//             * uinfo : {"userid":"1","avatar":"http://park.hostop.net/static/image/avatar.jpg","nickname":"未填写"}
//             * imgArr : ["http://park.hostop.net//upload/20180807/756809b8966c1c1073ef5d387a7826c4.jpg","http://park.hostop.net//upload/20180807/756809b8966c1c1073ef5d387a7826c4.jpg","http://park.hostop.net//upload/20180807/756809b8966c1c1073ef5d387a7826c4.jpg",""]
//             * reply : 0
//             * like : 0
//             * isLike : 1
//             */
//
//            private String id;
//            private String content;
//            private String add_time;
//            private UinfoBean uinfo;
//            private String reply;
//            private String like;
//            private String isLike;
//            private List<String> imgArr;
//
//            public String getId() {
//                return id;
//            }
//
//            public void setId(String id) {
//                this.id = id;
//            }
//
//            public String getContent() {
//                return content;
//            }
//
//            public void setContent(String content) {
//                this.content = content;
//            }
//
//            public String getAdd_time() {
//                return add_time;
//            }
//
//            public void setAdd_time(String add_time) {
//                this.add_time = add_time;
//            }
//
//            public UinfoBean getUinfo() {
//                return uinfo;
//            }
//
//            public void setUinfo(UinfoBean uinfo) {
//                this.uinfo = uinfo;
//            }
//
//            public String getReply() {
//                return reply;
//            }
//
//            public void setReply(String reply) {
//                this.reply = reply;
//            }
//
//            public String getLike() {
//                return like;
//            }
//
//            public void setLike(String like) {
//                this.like = like;
//            }
//
//            public String getIsLike() {
//                return isLike;
//            }
//
//            public void setIsLike(String isLike) {
//                this.isLike = isLike;
//            }
//
//            public List<String> getImgArr() {
//                return imgArr;
//            }
//
//            public void setImgArr(List<String> imgArr) {
//                this.imgArr = imgArr;
//            }
//
//            public static class UinfoBean {
//                /**
//                 * userid : 1
//                 * avatar : http://park.hostop.net/static/image/avatar.jpg
//                 * nickname : 未填写
//                 */
//
//                private String userid;
//                private String avatar;
//                private String nickname;
//
//                public String getUserid() {
//                    return userid;
//                }
//
//                public void setUserid(String userid) {
//                    this.userid = userid;
//                }
//
//                public String getAvatar() {
//                    return avatar;
//                }
//
//                public void setAvatar(String avatar) {
//                    this.avatar = avatar;
//                }
//
//                public String getNickname() {
//                    return nickname;
//                }
//
//                public void setNickname(String nickname) {
//                    this.nickname = nickname;
//                }
//            }
//        }
//
//        public static class ReplyArrBean {
//            /**
//             * id : 2
//             * content : 这是回复的第一条
//             * add_time : 3天前
//             * upInfo : [{"id":"1","content":"很棒啊","add_time":"4天前","uinfo":{"userid":"1","avatar":"http://park.hostop.net/static/image/avatar.jpg","nickname":"未填写"}}]
//             * uinfo : {"userid":"1","avatar":"http://park.hostop.net/static/image/avatar.jpg","nickname":"未填写"}
//             * canReply : 0
//             */
//
//            private String id;
//            private String content;
//            private String add_time;
////            private List<UpInfoBean> upInfo;
//            private UinfoBeanXX uinfo;
//            private String canReply;
//
//            public String getId() {
//                return id;
//            }
//
//            public void setId(String id) {
//                this.id = id;
//            }
//
//            public String getContent() {
//                return content;
//            }
//
//            public void setContent(String content) {
//                this.content = content;
//            }
//
//            public String getAdd_time() {
//                return add_time;
//            }
//
//            public void setAdd_time(String add_time) {
//                this.add_time = add_time;
//            }
//
////            public List<UpInfoBean> getUpInfo() {
////                return upInfo;
////            }
////
////            public void setUpInfo(List<UpInfoBean> upInfo) {
////                this.upInfo = upInfo;
////            }
//
//            public UinfoBeanXX getUinfo() {
//                return uinfo;
//            }
//
//            public void setUinfo(UinfoBeanXX uinfo) {
//                this.uinfo = uinfo;
//            }
//
//            public String getCanReply() {
//                return canReply;
//            }
//
//            public void setCanReply(String canReply) {
//                this.canReply = canReply;
//            }
//
//            public static class UpInfoBean {
//                /**
//                 * id : 1
//                 * content : 很棒啊
//                 * add_time : 4天前
//                 * uinfo : {"userid":"1","avatar":"http://park.hostop.net/static/image/avatar.jpg","nickname":"未填写"}
//                 */
//
//                private String id;
//                private String content;
//                private String add_time;
//                private UinfoBeanX uinfo;
//
//                public String getId() {
//                    return id;
//                }
//
//                public void setId(String id) {
//                    this.id = id;
//                }
//
//                public String getContent() {
//                    return content;
//                }
//
//                public void setContent(String content) {
//                    this.content = content;
//                }
//
//                public String getAdd_time() {
//                    return add_time;
//                }
//
//                public void setAdd_time(String add_time) {
//                    this.add_time = add_time;
//                }
//
//                public UinfoBeanX getUinfo() {
//                    return uinfo;
//                }
//
//                public void setUinfo(UinfoBeanX uinfo) {
//                    this.uinfo = uinfo;
//                }
//
//                public static class UinfoBeanX {
//                    /**
//                     * userid : 1
//                     * avatar : http://park.hostop.net/static/image/avatar.jpg
//                     * nickname : 未填写
//                     */
//
//                    private String userid;
//                    private String avatar;
//                    private String nickname;
//
//                    public String getUserid() {
//                        return userid;
//                    }
//
//                    public void setUserid(String userid) {
//                        this.userid = userid;
//                    }
//
//                    public String getAvatar() {
//                        return avatar;
//                    }
//
//                    public void setAvatar(String avatar) {
//                        this.avatar = avatar;
//                    }
//
//                    public String getNickname() {
//                        return nickname;
//                    }
//
//                    public void setNickname(String nickname) {
//                        this.nickname = nickname;
//                    }
//                }
//            }
//
//            public static class UinfoBeanXX {
//                /**
//                 * userid : 1
//                 * avatar : http://park.hostop.net/static/image/avatar.jpg
//                 * nickname : 未填写
//                 */
//
//                private String userid;
//                private String avatar;
//                private String nickname;
//
//                public String getUserid() {
//                    return userid;
//                }
//
//                public void setUserid(String userid) {
//                    this.userid = userid;
//                }
//
//                public String getAvatar() {
//                    return avatar;
//                }
//
//                public void setAvatar(String avatar) {
//                    this.avatar = avatar;
//                }
//
//                public String getNickname() {
//                    return nickname;
//                }
//
//                public void setNickname(String nickname) {
//                    this.nickname = nickname;
//                }
//            }
//        }
//
//        public static class LikeArrBean {
//            /**
//             * id : 2
//             * uinfo : {"userid":"2","avatar":"http://park.hostop.net/static/image/avatar.jpg","nickname":"未填写"}
//             */
//
//            private String id;
//            private UinfoBeanXXX uinfo;
//
//            public String getId() {
//                return id;
//            }
//
//            public void setId(String id) {
//                this.id = id;
//            }
//
//            public UinfoBeanXXX getUinfo() {
//                return uinfo;
//            }
//
//            public void setUinfo(UinfoBeanXXX uinfo) {
//                this.uinfo = uinfo;
//            }
//
//            public static class UinfoBeanXXX {
//                /**
//                 * userid : 2
//                 * avatar : http://park.hostop.net/static/image/avatar.jpg
//                 * nickname : 未填写
//                 */
//
//                private String userid;
//                private String avatar;
//                private String nickname;
//
//                public String getUserid() {
//                    return userid;
//                }
//
//                public void setUserid(String userid) {
//                    this.userid = userid;
//                }
//
//                public String getAvatar() {
//                    return avatar;
//                }
//
//                public void setAvatar(String avatar) {
//                    this.avatar = avatar;
//                }
//
//                public String getNickname() {
//                    return nickname;
//                }
//
//                public void setNickname(String nickname) {
//                    this.nickname = nickname;
//                }
//            }
//        }
//    }
//
//    public static class RequestBean {
//        /**
//         * id : 1
//         */
//
//        private String id;
//
//        public String getId() {
//            return id;
//        }
//
//        public void setId(String id) {
//            this.id = id;
//        }
//    }
}
