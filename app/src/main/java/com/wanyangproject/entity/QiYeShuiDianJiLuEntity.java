package com.wanyangproject.entity;

import java.util.List;

/**
 * Created by 甜甜 on 2018/9/6.
 */

public class QiYeShuiDianJiLuEntity {
    /**
     * code : 200
     * msg :
     * response : {"code":"200","message":"OK","data":{"total":"251","pageSize":"20","list":[{"month":"2018-02","cost":"0"},{"month":"2018-03","cost":"-44"},{"month":"2018-04","cost":"0"},{"month":"2018-05","cost":"0"},{"month":"2018-06","cost":"0"},{"month":"2018-07","cost":"20"},{"month":"2018-08","cost":"40"},{"month":"2018-09","cost":"15"},{"month":"2018-10","cost":"15"},{"month":"2018-11","cost":"15"}]}}
     * request : {"type":"1","parkId":"1","electricityUserId":"175","page":"0"}
     * other : []
     */

    private int code;
    private String msg;
    private ResponseBean response;
    private RequestBean request;
    private List<?> other;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public ResponseBean getResponse() {
        return response;
    }

    public void setResponse(ResponseBean response) {
        this.response = response;
    }

    public RequestBean getRequest() {
        return request;
    }

    public void setRequest(RequestBean request) {
        this.request = request;
    }

    public List<?> getOther() {
        return other;
    }

    public void setOther(List<?> other) {
        this.other = other;
    }

    public static class ResponseBean {
        /**
         * code : 200
         * message : OK
         * data : {"total":"251","pageSize":"20","list":[{"month":"2018-02","cost":"0"},{"month":"2018-03","cost":"-44"},{"month":"2018-04","cost":"0"},{"month":"2018-05","cost":"0"},{"month":"2018-06","cost":"0"},{"month":"2018-07","cost":"20"},{"month":"2018-08","cost":"40"},{"month":"2018-09","cost":"15"},{"month":"2018-10","cost":"15"},{"month":"2018-11","cost":"15"}]}
         */

        private String code;
        private String message;
        private DataBean data;

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        public DataBean getData() {
            return data;
        }

        public void setData(DataBean data) {
            this.data = data;
        }

        public static class DataBean {
            /**
             * total : 251
             * pageSize : 20
             * list : [{"month":"2018-02","cost":"0"},{"month":"2018-03","cost":"-44"},{"month":"2018-04","cost":"0"},{"month":"2018-05","cost":"0"},{"month":"2018-06","cost":"0"},{"month":"2018-07","cost":"20"},{"month":"2018-08","cost":"40"},{"month":"2018-09","cost":"15"},{"month":"2018-10","cost":"15"},{"month":"2018-11","cost":"15"}]
             */

            private String total;
            private String pageSize;
            private List<ListBean> list;

            public String getTotal() {
                return total;
            }

            public void setTotal(String total) {
                this.total = total;
            }

            public String getPageSize() {
                return pageSize;
            }

            public void setPageSize(String pageSize) {
                this.pageSize = pageSize;
            }

            public List<ListBean> getList() {
                return list;
            }

            public void setList(List<ListBean> list) {
                this.list = list;
            }

            public static class ListBean {
                /**
                 * month : 2018-02
                 * cost : 0
                 *
                 */

                private String month;
                private String cost;
                private String name;


                public String getName() {
                    return name;
                }

                public void setName(String name) {
                    this.name = name;
                }

                public String getMonth() {
                    return month;
                }

                public void setMonth(String month) {
                    this.month = month;
                }

                public String getCost() {
                    return cost;
                }

                public void setCost(String cost) {
                    this.cost = cost;
                }
            }
        }
    }

    public static class RequestBean {
        /**
         * type : 1
         * parkId : 1
         * electricityUserId : 175
         * page : 0
         */

        private String type;
        private String parkId;
        private String electricityUserId;
        private String page;

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public String getParkId() {
            return parkId;
        }

        public void setParkId(String parkId) {
            this.parkId = parkId;
        }

        public String getElectricityUserId() {
            return electricityUserId;
        }

        public void setElectricityUserId(String electricityUserId) {
            this.electricityUserId = electricityUserId;
        }

        public String getPage() {
            return page;
        }

        public void setPage(String page) {
            this.page = page;
        }
    }


//    /**
//     * code : 200
//     * msg :
//     * response : {"code":"200","message":"OK","data":{"total":"234","pageSize":"20","list":[{"month":"2018-02","cost":"0"},{"month":"2018-03","cost":"21"},{"month":"2018-04","cost":"0"},{"month":"2018-05","cost":"10"},{"month":"2018-06","cost":"10"},{"month":"2018-07","cost":"10"},{"month":"2018-08","cost":"10"},{"month":"2018-09","cost":"10"},{"month":"2018-10","cost":"5"},{"month":"2018-11","cost":"5"}]}}
//     * request : {"typeId":"1","parkId":"1","page":"0"}
//     * other : []
//     */
//
//    private int code;
//    private String msg;
//    private ResponseBean response;
//    private RequestBean request;
//    private List<?> other;
//
//    public int getCode() {
//        return code;
//    }
//
//    public void setCode(int code) {
//        this.code = code;
//    }
//
//    public String getMsg() {
//        return msg;
//    }
//
//    public void setMsg(String msg) {
//        this.msg = msg;
//    }
//
//    public ResponseBean getResponse() {
//        return response;
//    }
//
//    public void setResponse(ResponseBean response) {
//        this.response = response;
//    }
//
//    public RequestBean getRequest() {
//        return request;
//    }
//
//    public void setRequest(RequestBean request) {
//        this.request = request;
//    }
//
//    public List<?> getOther() {
//        return other;
//    }
//
//    public void setOther(List<?> other) {
//        this.other = other;
//    }
//
//    public static class ResponseBean {
//        /**
//         * code : 200
//         * message : OK
//         * data : {"total":"234","pageSize":"20","list":[{"month":"2018-02","cost":"0"},{"month":"2018-03","cost":"21"},{"month":"2018-04","cost":"0"},{"month":"2018-05","cost":"10"},{"month":"2018-06","cost":"10"},{"month":"2018-07","cost":"10"},{"month":"2018-08","cost":"10"},{"month":"2018-09","cost":"10"},{"month":"2018-10","cost":"5"},{"month":"2018-11","cost":"5"}]}
//         */
//
//        private String code;
//        private String message;
//        private DataBean data;
//
//        public String getCode() {
//            return code;
//        }
//
//        public void setCode(String code) {
//            this.code = code;
//        }
//
//        public String getMessage() {
//            return message;
//        }
//
//        public void setMessage(String message) {
//            this.message = message;
//        }
//
//        public DataBean getData() {
//            return data;
//        }
//
//        public void setData(DataBean data) {
//            this.data = data;
//        }
//
//        public static class DataBean {
//            /**
//             * total : 234
//             * pageSize : 20
//             * list : [{"month":"2018-02","cost":"0"},{"month":"2018-03","cost":"21"},{"month":"2018-04","cost":"0"},{"month":"2018-05","cost":"10"},{"month":"2018-06","cost":"10"},{"month":"2018-07","cost":"10"},{"month":"2018-08","cost":"10"},{"month":"2018-09","cost":"10"},{"month":"2018-10","cost":"5"},{"month":"2018-11","cost":"5"}]
//             */
//
//            private String total;
//            private String pageSize;
//            private List<ListBean> list;
//
//            public String getTotal() {
//                return total;
//            }
//
//            public void setTotal(String total) {
//                this.total = total;
//            }
//
//            public String getPageSize() {
//                return pageSize;
//            }
//
//            public void setPageSize(String pageSize) {
//                this.pageSize = pageSize;
//            }
//
//            public List<ListBean> getList() {
//                return list;
//            }
//
//            public void setList(List<ListBean> list) {
//                this.list = list;
//            }
//
//            public static class ListBean {
//                /**
//                 * month : 2018-02
//                 * cost : 0
//                 */
//
//                private String month;
//                private String cost;
//
//                public String getMonth() {
//                    return month;
//                }
//
//                public void setMonth(String month) {
//                    this.month = month;
//                }
//
//                public String getCost() {
//                    return cost;
//                }
//
//                public void setCost(String cost) {
//                    this.cost = cost;
//                }
//            }
//        }
//    }
//
//    public static class RequestBean {
//        /**
//         * typeId : 1
//         * parkId : 1
//         * page : 0
//         */
//
//        private String typeId;
//        private String parkId;
//        private String page;
//
//        public String getTypeId() {
//            return typeId;
//        }
//
//        public void setTypeId(String typeId) {
//            this.typeId = typeId;
//        }
//
//        public String getParkId() {
//            return parkId;
//        }
//
//        public void setParkId(String parkId) {
//            this.parkId = parkId;
//        }
//
//        public String getPage() {
//            return page;
//        }
//
//        public void setPage(String page) {
//            this.page = page;
//        }
//    }




















//
//    /**
//     * code : 200
//     * msg :
//     * response : [{"month":"09月","cost":"113"},{"month":"8月","cost":"341"},{"month":"7月","cost":"392"},{"month":"6月","cost":"347"}]
//     * request : {"parkId":"1,14,"}
//     * other : []
//     */
//
//    private int code;
//    private String msg;
//    private RequestBean request;
//    private List<ResponseBean> response;
//    private List<?> other;
//
//    public int getCode() {
//        return code;
//    }
//
//    public void setCode(int code) {
//        this.code = code;
//    }
//
//    public String getMsg() {
//        return msg;
//    }
//
//    public void setMsg(String msg) {
//        this.msg = msg;
//    }
//
//    public RequestBean getRequest() {
//        return request;
//    }
//
//    public void setRequest(RequestBean request) {
//        this.request = request;
//    }
//
//    public List<ResponseBean> getResponse() {
//        return response;
//    }
//
//    public void setResponse(List<ResponseBean> response) {
//        this.response = response;
//    }
//
//    public List<?> getOther() {
//        return other;
//    }
//
//    public void setOther(List<?> other) {
//        this.other = other;
//    }
//
//    public static class RequestBean {
//        /**
//         * parkId : 1,14,
//         */
//
//        private String parkId;
//
//        public String getParkId() {
//            return parkId;
//        }
//
//        public void setParkId(String parkId) {
//            this.parkId = parkId;
//        }
//    }
//
//    public static class ResponseBean {
//        /**
//         * month : 09月
//         * cost : 113
//         */
//
//        private String month;
//        private String cost;
//
//        public String getMonth() {
//            return month;
//        }
//
//        public void setMonth(String month) {
//            this.month = month;
//        }
//
//        public String getCost() {
//            return cost;
//        }
//
//        public void setCost(String cost) {
//            this.cost = cost;
//        }
//    }
}
