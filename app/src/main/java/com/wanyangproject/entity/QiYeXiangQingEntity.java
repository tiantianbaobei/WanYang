package com.wanyangproject.entity;

import java.util.List;

/**
 * Created by 甜甜 on 2018/8/15.
 */

public class QiYeXiangQingEntity {
    /**
     * code : 200
     * msg :
     * response : {"info":{"enterId":"45","enterName":"平阳马太贵金属有限公司","parkName":"温州平阳众创城","legalPerson":"徐贤春","contact":"13868380818","logoUrl":"http://park.hostop.net/upload/static/logo.png","intro":"","lat":"","lon":"","desc":"","address":"","guimo":"","hangye":"","lng":""},"recruit":[{"id":"2","title":"大堂经理","money":"薪资面议","desc":"点餐、打扫卫生、传菜、接待客人等"},{"id":"1","title":"服务员","money":"薪资面议","desc":"点餐、打扫卫生、传菜、接待客人等"}]}
     * request : {"parkId":"1","enterId":"45"}
     * other : []
     */

    private int code;
    private String msg;
    private ResponseBean response;
    private RequestBean request;
    private List<?> other;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public ResponseBean getResponse() {
        return response;
    }

    public void setResponse(ResponseBean response) {
        this.response = response;
    }

    public RequestBean getRequest() {
        return request;
    }

    public void setRequest(RequestBean request) {
        this.request = request;
    }

    public List<?> getOther() {
        return other;
    }

    public void setOther(List<?> other) {
        this.other = other;
    }

    public static class ResponseBean {
        /**
         * info : {"enterId":"45","enterName":"平阳马太贵金属有限公司","parkName":"温州平阳众创城","legalPerson":"徐贤春","contact":"13868380818","logoUrl":"http://park.hostop.net/upload/static/logo.png","intro":"","lat":"","lon":"","desc":"","address":"","guimo":"","hangye":"","lng":""}
         * recruit : [{"id":"2","title":"大堂经理","money":"薪资面议","desc":"点餐、打扫卫生、传菜、接待客人等"},{"id":"1","title":"服务员","money":"薪资面议","desc":"点餐、打扫卫生、传菜、接待客人等"}]
         */

        private InfoBean info;
        private List<RecruitBean> recruit;

        public InfoBean getInfo() {
            return info;
        }

        public void setInfo(InfoBean info) {
            this.info = info;
        }

        public List<RecruitBean> getRecruit() {
            return recruit;
        }

        public void setRecruit(List<RecruitBean> recruit) {
            this.recruit = recruit;
        }

        public static class InfoBean {
            /**
             * enterId : 45
             * enterName : 平阳马太贵金属有限公司
             * parkName : 温州平阳众创城
             * legalPerson : 徐贤春
             * contact : 13868380818
             * logoUrl : http://park.hostop.net/upload/static/logo.png
             * intro :
             * lat :
             * lon :
             * desc :
             * address :
             * guimo :
             * hangye :
             * lng :
             */

            private String enterId;
            private String enterName;
            private String parkName;
            private String legalPerson;
            private String contact;
            private String logoUrl;
            private String intro;
            private String lat;
            private String lon;
            private String desc;
            private String address;
            private String guimo;
            private String hangye;
            private String lng;

            public String getEnterId() {
                return enterId;
            }

            public void setEnterId(String enterId) {
                this.enterId = enterId;
            }

            public String getEnterName() {
                return enterName;
            }

            public void setEnterName(String enterName) {
                this.enterName = enterName;
            }

            public String getParkName() {
                return parkName;
            }

            public void setParkName(String parkName) {
                this.parkName = parkName;
            }

            public String getLegalPerson() {
                return legalPerson;
            }

            public void setLegalPerson(String legalPerson) {
                this.legalPerson = legalPerson;
            }

            public String getContact() {
                return contact;
            }

            public void setContact(String contact) {
                this.contact = contact;
            }

            public String getLogoUrl() {
                return logoUrl;
            }

            public void setLogoUrl(String logoUrl) {
                this.logoUrl = logoUrl;
            }

            public String getIntro() {
                return intro;
            }

            public void setIntro(String intro) {
                this.intro = intro;
            }

            public String getLat() {
                return lat;
            }

            public void setLat(String lat) {
                this.lat = lat;
            }

            public String getLon() {
                return lon;
            }

            public void setLon(String lon) {
                this.lon = lon;
            }

            public String getDesc() {
                return desc;
            }

            public void setDesc(String desc) {
                this.desc = desc;
            }

            public String getAddress() {
                return address;
            }

            public void setAddress(String address) {
                this.address = address;
            }

            public String getGuimo() {
                return guimo;
            }

            public void setGuimo(String guimo) {
                this.guimo = guimo;
            }

            public String getHangye() {
                return hangye;
            }

            public void setHangye(String hangye) {
                this.hangye = hangye;
            }

            public String getLng() {
                return lng;
            }

            public void setLng(String lng) {
                this.lng = lng;
            }
        }

        public static class RecruitBean {
            /**
             * id : 2
             * title : 大堂经理
             * money : 薪资面议
             * desc : 点餐、打扫卫生、传菜、接待客人等
             */

            private String id;
            private String title;
            private String money;
            private String desc;

            public String getId() {
                return id;
            }

            public void setId(String id) {
                this.id = id;
            }

            public String getTitle() {
                return title;
            }

            public void setTitle(String title) {
                this.title = title;
            }

            public String getMoney() {
                return money;
            }

            public void setMoney(String money) {
                this.money = money;
            }

            public String getDesc() {
                return desc;
            }

            public void setDesc(String desc) {
                this.desc = desc;
            }
        }
    }

    public static class RequestBean {
        /**
         * parkId : 1
         * enterId : 45
         */

        private String parkId;
        private String enterId;

        public String getParkId() {
            return parkId;
        }

        public void setParkId(String parkId) {
            this.parkId = parkId;
        }

        public String getEnterId() {
            return enterId;
        }

        public void setEnterId(String enterId) {
            this.enterId = enterId;
        }
    }


//    /**
//     * code : 200
//     * msg :
//     * response : {"info":{"enterId":"45","enterName":"平阳马太贵金属有限公司","parkName":"温州平阳众创城","legalPerson":"徐贤春","contact":"13868380818","logoUrl":"http://park.hostop.net/upload/static/logo.png","intro":"","lat":"","lon":"","desc":"简介内容，当前未测试数据","address":"测试的位置信息"},"recruit":[{"id":"2","title":"大堂经理","money":"薪资面议","desc":"点餐、打扫卫生、传菜、接待客人等"},{"id":"1","title":"服务员","money":"薪资面议","desc":"点餐、打扫卫生、传菜、接待客人等"}]}
//     * request : {"parkId":"1","enterId":"45"}
//     * other : []
//     */
//
//    private int code;
//    private String msg;
//    private ResponseBean response;
//    private RequestBean request;
//    private List<?> other;
//
//    public int getCode() {
//        return code;
//    }
//
//    public void setCode(int code) {
//        this.code = code;
//    }
//
//    public String getMsg() {
//        return msg;
//    }
//
//    public void setMsg(String msg) {
//        this.msg = msg;
//    }
//
//    public ResponseBean getResponse() {
//        return response;
//    }
//
//    public void setResponse(ResponseBean response) {
//        this.response = response;
//    }
//
//    public RequestBean getRequest() {
//        return request;
//    }
//
//    public void setRequest(RequestBean request) {
//        this.request = request;
//    }
//
//    public List<?> getOther() {
//        return other;
//    }
//
//    public void setOther(List<?> other) {
//        this.other = other;
//    }
//
//    public static class ResponseBean {
//        /**
//         * info : {"enterId":"45","enterName":"平阳马太贵金属有限公司","parkName":"温州平阳众创城","legalPerson":"徐贤春","contact":"13868380818","logoUrl":"http://park.hostop.net/upload/static/logo.png","intro":"","lat":"","lon":"","desc":"简介内容，当前未测试数据","address":"测试的位置信息"}
//         * recruit : [{"id":"2","title":"大堂经理","money":"薪资面议","desc":"点餐、打扫卫生、传菜、接待客人等"},{"id":"1","title":"服务员","money":"薪资面议","desc":"点餐、打扫卫生、传菜、接待客人等"}]
//         */
//
//        private InfoBean info;
//        private List<RecruitBean> recruit;
//
//        public InfoBean getInfo() {
//            return info;
//        }
//
//        public void setInfo(InfoBean info) {
//            this.info = info;
//        }
//
//        public List<RecruitBean> getRecruit() {
//            return recruit;
//        }
//
//        public void setRecruit(List<RecruitBean> recruit) {
//            this.recruit = recruit;
//        }
//
//        public static class InfoBean {
//            /**
//             * enterId : 45
//             * enterName : 平阳马太贵金属有限公司
//             * parkName : 温州平阳众创城
//             * legalPerson : 徐贤春
//             * contact : 13868380818
//             * logoUrl : http://park.hostop.net/upload/static/logo.png
//             * intro :
//             * lat :
//             * lon :
//             * desc : 简介内容，当前未测试数据
//             * address : 测试的位置信息
//             */
//
//            private String enterId;
//            private String enterName;
//            private String parkName;
//            private String legalPerson;
//            private String contact;
//            private String logoUrl;
//            private String intro;
//            private String lat;
//            private String lon;
//            private String desc;
//            private String address;
//            private String lng;
//
//
//            public String getLng() {
//                return lng;
//            }
//
//            public void setLng(String lng) {
//                this.lng = lng;
//            }
//
//            public String getEnterId() {
//                return enterId;
//            }
//
//            public void setEnterId(String enterId) {
//                this.enterId = enterId;
//            }
//
//            public String getEnterName() {
//                return enterName;
//            }
//
//            public void setEnterName(String enterName) {
//                this.enterName = enterName;
//            }
//
//            public String getParkName() {
//                return parkName;
//            }
//
//            public void setParkName(String parkName) {
//                this.parkName = parkName;
//            }
//
//            public String getLegalPerson() {
//                return legalPerson;
//            }
//
//            public void setLegalPerson(String legalPerson) {
//                this.legalPerson = legalPerson;
//            }
//
//            public String getContact() {
//                return contact;
//            }
//
//            public void setContact(String contact) {
//                this.contact = contact;
//            }
//
//            public String getLogoUrl() {
//                return logoUrl;
//            }
//
//            public void setLogoUrl(String logoUrl) {
//                this.logoUrl = logoUrl;
//            }
//
//            public String getIntro() {
//                return intro;
//            }
//
//            public void setIntro(String intro) {
//                this.intro = intro;
//            }
//
//            public String getLat() {
//                return lat;
//            }
//
//            public void setLat(String lat) {
//                this.lat = lat;
//            }
//
//            public String getLon() {
//                return lon;
//            }
//
//            public void setLon(String lon) {
//                this.lon = lon;
//            }
//
//            public String getDesc() {
//                return desc;
//            }
//
//            public void setDesc(String desc) {
//                this.desc = desc;
//            }
//
//            public String getAddress() {
//                return address;
//            }
//
//            public void setAddress(String address) {
//                this.address = address;
//            }
//        }
//
//        public static class RecruitBean {
//            /**
//             * id : 2
//             * title : 大堂经理
//             * money : 薪资面议
//             * desc : 点餐、打扫卫生、传菜、接待客人等
//             */
//
//            private String id;
//            private String title;
//            private String money;
//            private String desc;
//
//            public String getId() {
//                return id;
//            }
//
//            public void setId(String id) {
//                this.id = id;
//            }
//
//            public String getTitle() {
//                return title;
//            }
//
//            public void setTitle(String title) {
//                this.title = title;
//            }
//
//            public String getMoney() {
//                return money;
//            }
//
//            public void setMoney(String money) {
//                this.money = money;
//            }
//
//            public String getDesc() {
//                return desc;
//            }
//
//            public void setDesc(String desc) {
//                this.desc = desc;
//            }
//        }
//    }
//
//    public static class RequestBean {
//        /**
//         * parkId : 1
//         * enterId : 45
//         */
//
//        private String parkId;
//        private String enterId;
//
//        public String getParkId() {
//            return parkId;
//        }
//
//        public void setParkId(String parkId) {
//            this.parkId = parkId;
//        }
//
//        public String getEnterId() {
//            return enterId;
//        }
//
//        public void setEnterId(String enterId) {
//            this.enterId = enterId;
//        }
//    }
}
