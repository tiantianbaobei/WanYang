package com.wanyangproject.entity;

import java.util.List;

/**
 * Created by 甜甜 on 2018/11/2.
 */

public class BianJiFangKeEntity {

    /**
     * code : 200
     * msg :
     * response : {"photo":"upload/qrcode/2018-11-02/13a7ca6db9df2e54fe38a3356bd0448811.png","logo":"static\\wanyang\\img\\download-logo.png"}
     * request : {"parkId":"1","name":"测试名","phone":"13336343701","nums":"6","chepai":"鲁S00000","daodaoTime":"2018-11-02 11:20","id":"348"}
     * other : []
     */

    private int code;
    private String msg;
    private ResponseBean response;
    private RequestBean request;
    private List<?> other;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public ResponseBean getResponse() {
        return response;
    }

    public void setResponse(ResponseBean response) {
        this.response = response;
    }

    public RequestBean getRequest() {
        return request;
    }

    public void setRequest(RequestBean request) {
        this.request = request;
    }

    public List<?> getOther() {
        return other;
    }

    public void setOther(List<?> other) {
        this.other = other;
    }

    public static class ResponseBean {
        /**
         * photo : upload/qrcode/2018-11-02/13a7ca6db9df2e54fe38a3356bd0448811.png
         * logo : static\wanyang\img\download-logo.png
         */

        private String photo;
        private String logo;

        public String getPhoto() {
            return photo;
        }

        public void setPhoto(String photo) {
            this.photo = photo;
        }

        public String getLogo() {
            return logo;
        }

        public void setLogo(String logo) {
            this.logo = logo;
        }
    }

    public static class RequestBean {
        /**
         * parkId : 1
         * name : 测试名
         * phone : 13336343701
         * nums : 6
         * chepai : 鲁S00000
         * daodaoTime : 2018-11-02 11:20
         * id : 348
         */

        private String parkId;
        private String name;
        private String phone;
        private String nums;
        private String chepai;
        private String daodaoTime;
        private String id;

        public String getParkId() {
            return parkId;
        }

        public void setParkId(String parkId) {
            this.parkId = parkId;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getPhone() {
            return phone;
        }

        public void setPhone(String phone) {
            this.phone = phone;
        }

        public String getNums() {
            return nums;
        }

        public void setNums(String nums) {
            this.nums = nums;
        }

        public String getChepai() {
            return chepai;
        }

        public void setChepai(String chepai) {
            this.chepai = chepai;
        }

        public String getDaodaoTime() {
            return daodaoTime;
        }

        public void setDaodaoTime(String daodaoTime) {
            this.daodaoTime = daodaoTime;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }
    }
}
