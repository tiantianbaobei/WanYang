package com.wanyangproject.entity;

import java.util.List;

/**
 * Created by 甜甜 on 2018/8/21.
 */

public class ShangJiaShangPinGuanLiEntity {
    /**
     * code : 200
     * msg :
     * response : {"goods":[{"name":"水果","id":"21","goods":[{"goods_name":"火龙果","type":"1","id":"14","master":"park.hostop.net//upload/20180820/77c2ca30c7965e4ada32dd6aa48c3878.jpg","pic":"59","volume":"0","fabulous":"0","stock":"12","market":"69"},{"goods_name":"测试","type":"1","id":"4","master":"park.hostop.net//upload/20180804/2da84761de43d358bfd2f89627fdaf42.png","pic":"0.01","volume":"0","fabulous":"0","stock":"1","market":"21"}]},{"name":"商品","id":"14","goods":[]},{"name":"商品","id":"15","goods":[]},{"name":"商品","id":"16","goods":[]},{"name":"商品","id":"17","goods":[]},{"name":"商品","id":"18","goods":[]},{"name":"商品","id":"19","goods":[]},{"name":"商品","id":"20","goods":[]},{"name":"商品","id":"13","goods":[]},{"name":"商品","id":"12","goods":[]},{"name":"商品","id":"11","goods":[]},{"name":"商品","id":"10","goods":[]},{"name":"商品","id":"9","goods":[]},{"name":"商品","id":"8","goods":[]},{"name":"商品","id":"7","goods":[]},{"name":"商品","id":"6","goods":[]},{"name":"43223","id":"3","goods":[]},{"name":"11","id":"2","goods":[{"goods_name":"测试3423","type":"1","id":"5","master":"park.hostop.net//upload/20180820/9ae4ff65f4004050c7a0c4ce7ca05073.jpg","pic":"13","volume":"0","fabulous":"0","stock":"1","market":"100"}]}]}
     * request : {"parkId":"1","phone":"15822075533"}
     * other : []
     */

    private int code;
    private String msg;
    private ResponseBean response;
    private RequestBean request;
    private List<?> other;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public ResponseBean getResponse() {
        return response;
    }

    public void setResponse(ResponseBean response) {
        this.response = response;
    }

    public RequestBean getRequest() {
        return request;
    }

    public void setRequest(RequestBean request) {
        this.request = request;
    }

    public List<?> getOther() {
        return other;
    }

    public void setOther(List<?> other) {
        this.other = other;
    }

    public static class ResponseBean {
        private List<GoodsBeanX> goods;

        public List<GoodsBeanX> getGoods() {
            return goods;
        }

        public void setGoods(List<GoodsBeanX> goods) {
            this.goods = goods;
        }

        public static class GoodsBeanX {
            /**
             * name : 水果
             * id : 21
             * goods : [{"goods_name":"火龙果","type":"1","id":"14","master":"park.hostop.net//upload/20180820/77c2ca30c7965e4ada32dd6aa48c3878.jpg","pic":"59","volume":"0","fabulous":"0","stock":"12","market":"69"},{"goods_name":"测试","type":"1","id":"4","master":"park.hostop.net//upload/20180804/2da84761de43d358bfd2f89627fdaf42.png","pic":"0.01","volume":"0","fabulous":"0","stock":"1","market":"21"}]
             */

            private String name;
            private String id;
            private List<GoodsBean> goods;



            private boolean ok;
//
            public boolean isOk() {
                return ok;
            }

            public void setOk(boolean ok) {
                this.ok = ok;
            }

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }

            public String getId() {
                return id;
            }

            public void setId(String id) {
                this.id = id;
            }

            public List<GoodsBean> getGoods() {
                return goods;
            }

            public void setGoods(List<GoodsBean> goods) {
                this.goods = goods;
            }

            public static class GoodsBean {
                /**
                 * goods_name : 火龙果
                 * type : 1
                 * id : 14
                 * master : park.hostop.net//upload/20180820/77c2ca30c7965e4ada32dd6aa48c3878.jpg
                 * pic : 59
                 * volume : 0
                 * fabulous : 0
                 * stock : 12
                 * market : 69
                 */

                private String goods_name;
                private String type;
                private String id;
                private String master;
                private String pic;
                private String volume;
                private String fabulous;
                private String stock;
                private String market;

                public String getGoods_name() {
                    return goods_name;
                }

                public void setGoods_name(String goods_name) {
                    this.goods_name = goods_name;
                }

                public String getType() {
                    return type;
                }

                public void setType(String type) {
                    this.type = type;
                }

                public String getId() {
                    return id;
                }

                public void setId(String id) {
                    this.id = id;
                }

                public String getMaster() {
                    return master;
                }

                public void setMaster(String master) {
                    this.master = master;
                }

                public String getPic() {
                    return pic;
                }

                public void setPic(String pic) {
                    this.pic = pic;
                }

                public String getVolume() {
                    return volume;
                }

                public void setVolume(String volume) {
                    this.volume = volume;
                }

                public String getFabulous() {
                    return fabulous;
                }

                public void setFabulous(String fabulous) {
                    this.fabulous = fabulous;
                }

                public String getStock() {
                    return stock;
                }

                public void setStock(String stock) {
                    this.stock = stock;
                }

                public String getMarket() {
                    return market;
                }

                public void setMarket(String market) {
                    this.market = market;
                }
            }
        }
    }

    public static class RequestBean {
        /**
         * parkId : 1
         * phone : 15822075533
         */

        private String parkId;
        private String phone;

        public String getParkId() {
            return parkId;
        }

        public void setParkId(String parkId) {
            this.parkId = parkId;
        }

        public String getPhone() {
            return phone;
        }

        public void setPhone(String phone) {
            this.phone = phone;
        }
    }


//    /**
//     * code : 200
//     * msg :
//     * response : {"goods":[{"name":"水果","id":"21","goods":[{"goods_name":"火龙果","type":"1","id":"14","master":"park.hostop.net//upload/20180820/77c2ca30c7965e4ada32dd6aa48c3878.jpg","pic":"59","volume":"0","fabulous":"0"},{"goods_name":"测试","type":"2","id":"4","master":"park.hostop.net//upload/20180804/2da84761de43d358bfd2f89627fdaf42.png","pic":"0.01","volume":"0","fabulous":"0"}]},{"name":"商品","id":"14","goods":[]},{"name":"商品","id":"15","goods":[]},{"name":"商品","id":"16","goods":[]},{"name":"商品","id":"17","goods":[]},{"name":"商品","id":"18","goods":[]},{"name":"商品","id":"19","goods":[]},{"name":"商品","id":"20","goods":[]},{"name":"商品","id":"13","goods":[]},{"name":"商品","id":"12","goods":[]},{"name":"商品","id":"11","goods":[]},{"name":"商品","id":"10","goods":[]},{"name":"商品","id":"9","goods":[]},{"name":"商品","id":"8","goods":[]},{"name":"商品","id":"7","goods":[]},{"name":"商品","id":"6","goods":[]},{"name":"43223","id":"3","goods":[]},{"name":"11","id":"2","goods":[{"goods_name":"测试3423","type":"1","id":"5","master":"park.hostop.net//upload/20180820/9ae4ff65f4004050c7a0c4ce7ca05073.jpg","pic":"13","volume":"0","fabulous":"0"}]}]}
//     * request : {"parkId":"1","phone":"15822075533"}
//     * other : []
//     */
//
//    private int code;
//    private String msg;
//    private ResponseBean response;
//    private RequestBean request;
//    private List<?> other;
//
//    public int getCode() {
//        return code;
//    }
//
//    public void setCode(int code) {
//        this.code = code;
//    }
//
//    public String getMsg() {
//        return msg;
//    }
//
//    public void setMsg(String msg) {
//        this.msg = msg;
//    }
//
//    public ResponseBean getResponse() {
//        return response;
//    }
//
//    public void setResponse(ResponseBean response) {
//        this.response = response;
//    }
//
//    public RequestBean getRequest() {
//        return request;
//    }
//
//    public void setRequest(RequestBean request) {
//        this.request = request;
//    }
//
//    public List<?> getOther() {
//        return other;
//    }
//
//    public void setOther(List<?> other) {
//        this.other = other;
//    }
//
//    public static class ResponseBean {
//        private List<GoodsBeanX> goods;
//
//        public List<GoodsBeanX> getGoods() {
//            return goods;
//        }
//
//        public void setGoods(List<GoodsBeanX> goods) {
//            this.goods = goods;
//        }
//
//        public static class GoodsBeanX {
//            /**
//             * name : 水果
//             * id : 21
//             * goods : [{"goods_name":"火龙果","type":"1","id":"14","master":"park.hostop.net//upload/20180820/77c2ca30c7965e4ada32dd6aa48c3878.jpg","pic":"59","volume":"0","fabulous":"0"},{"goods_name":"测试","type":"2","id":"4","master":"park.hostop.net//upload/20180804/2da84761de43d358bfd2f89627fdaf42.png","pic":"0.01","volume":"0","fabulous":"0"}]
//             */
//
//
//            private boolean ok;
//
//            public boolean isOk() {
//                return ok;
//            }
//
//            public void setOk(boolean ok) {
//                this.ok = ok;
//            }
//
//
//            private String name;
//            private String id;
//            private List<GoodsBean> goods;
//
//            public String getName() {
//                return name;
//            }
//
//            public void setName(String name) {
//                this.name = name;
//            }
//
//            public String getId() {
//                return id;
//            }
//
//            public void setId(String id) {
//                this.id = id;
//            }
//
//            public List<GoodsBean> getGoods() {
//                return goods;
//            }
//
//            public void setGoods(List<GoodsBean> goods) {
//                this.goods = goods;
//            }
//
//            public static class GoodsBean {
//                /**
//                 * goods_name : 火龙果
//                 * type : 1
//                 * id : 14
//                 * master : park.hostop.net//upload/20180820/77c2ca30c7965e4ada32dd6aa48c3878.jpg
//                 * pic : 59
//                 * volume : 0
//                 * fabulous : 0
//                 */
//
//                private String goods_name;
//                private String type;
//                private String id;
//                private String master;
//                private String pic;
//                private String volume;
//                private String fabulous;
//
//                public String getGoods_name() {
//                    return goods_name;
//                }
//
//                public void setGoods_name(String goods_name) {
//                    this.goods_name = goods_name;
//                }
//
//                public String getType() {
//                    return type;
//                }
//
//                public void setType(String type) {
//                    this.type = type;
//                }
//
//                public String getId() {
//                    return id;
//                }
//
//                public void setId(String id) {
//                    this.id = id;
//                }
//
//                public String getMaster() {
//                    return master;
//                }
//
//                public void setMaster(String master) {
//                    this.master = master;
//                }
//
//                public String getPic() {
//                    return pic;
//                }
//
//                public void setPic(String pic) {
//                    this.pic = pic;
//                }
//
//                public String getVolume() {
//                    return volume;
//                }
//
//                public void setVolume(String volume) {
//                    this.volume = volume;
//                }
//
//                public String getFabulous() {
//                    return fabulous;
//                }
//
//                public void setFabulous(String fabulous) {
//                    this.fabulous = fabulous;
//                }
//            }
//        }
//    }
//
//    public static class RequestBean {
//        /**
//         * parkId : 1
//         * phone : 15822075533
//         */
//
//        private String parkId;
//        private String phone;
//
//        public String getParkId() {
//            return parkId;
//        }
//
//        public void setParkId(String parkId) {
//            this.parkId = parkId;
//        }
//
//        public String getPhone() {
//            return phone;
//        }
//
//        public void setPhone(String phone) {
//            this.phone = phone;
//        }
//    }


}
