package com.wanyangproject.entity;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by 甜甜 on 2018/8/24.
 */

public class QiYeFuWuXiangQingEntity {

    /**
     * code : 200
     * msg :
     * response : {"id":"7","title":"企业工商年报","introduction":"企业工商年报怎么报具体步骤","content":"<p><span style=\"text-align: justify;\">&nbsp; &nbsp; &nbsp; &nbsp;自2014年10月1日起，每年1月1日开始办理上一年度《营业执照》网上年度报告（以下简称网上年报，网上年报可理解为手续简便的营业执照年检）。为避免临近6月30日截止期限网上年报系统用户激增，而可能出现的网络拥堵或被踢出系统的现象，请提前尽快办理。<\/span><\/p><p style=\"text-align: justify;\">&nbsp; &nbsp; &nbsp; 根据个人操作截图，以河南省为例说明。<\/p><p style=\"text-align: justify;\"><img src=\"http://park.hostop.net/upload/20180908/9ee8049d6bffc368562fd29b24be7dfb.jpg\" alt=\"undefined\" style=\"width:50%\"><br><\/p><p style=\"text-align: justify;\">&nbsp; &nbsp; &nbsp;未按规定办理网上年告的，将会被企业信用信息系统列入经营异常名录（俗称黑名单），可能给公司、企业及经营者在多个领域带来不必要的麻烦。&nbsp;&nbsp;&nbsp; &nbsp; 依据2014年10月1日起实施的《企业信息公示暂行条例》和《个体工商户年度报告暂行办法》等法规、规章的规定，2013年底前注册的，应当办理2013和2014年度的网上年报；2014年底前注册的，应办理2014年度的网上年报；以此类推。另外，企业重要信息发生变化的，应在20日内上网公示。&nbsp; &nbsp; 实在不会办理的，可以携带下列材料到辖区工商所学习：&nbsp;&nbsp;&nbsp;&nbsp;1、《营业执照》副本；2、所有的许可证副本；3、法定代表人或负责人身份证；4、手机现场接收短信，办理过网上年报的或注册过联络员的带注册登记的手机；5、公司、企业等法人单位另需资产负债表、损益表和公司章程。<\/p><p style=\"text-align: justify;\"><\/p><p style=\"text-align: justify;\">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;<\/p>","type":"1","add_time":"","Recommend":"1","parkId":"1","class":"2","wyservicePoolId":"168","fujian":""}
     * request : {"parkId":"1","id":"7","typeId":"2"}
     * other : []
     */

    private int code;
    private String msg;
    private ResponseBean response;
    private RequestBean request;
    private List<?> other;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public ResponseBean getResponse() {
        return response;
    }

    public void setResponse(ResponseBean response) {
        this.response = response;
    }

    public RequestBean getRequest() {
        return request;
    }

    public void setRequest(RequestBean request) {
        this.request = request;
    }

    public List<?> getOther() {
        return other;
    }

    public void setOther(List<?> other) {
        this.other = other;
    }

    public static class ResponseBean {
        /**
         * id : 7
         * title : 企业工商年报
         * introduction : 企业工商年报怎么报具体步骤
         * content : <p><span style="text-align: justify;">&nbsp; &nbsp; &nbsp; &nbsp;自2014年10月1日起，每年1月1日开始办理上一年度《营业执照》网上年度报告（以下简称网上年报，网上年报可理解为手续简便的营业执照年检）。为避免临近6月30日截止期限网上年报系统用户激增，而可能出现的网络拥堵或被踢出系统的现象，请提前尽快办理。</span></p><p style="text-align: justify;">&nbsp; &nbsp; &nbsp; 根据个人操作截图，以河南省为例说明。</p><p style="text-align: justify;"><img src="http://park.hostop.net/upload/20180908/9ee8049d6bffc368562fd29b24be7dfb.jpg" alt="undefined" style="width:50%"><br></p><p style="text-align: justify;">&nbsp; &nbsp; &nbsp;未按规定办理网上年告的，将会被企业信用信息系统列入经营异常名录（俗称黑名单），可能给公司、企业及经营者在多个领域带来不必要的麻烦。&nbsp;&nbsp;&nbsp; &nbsp; 依据2014年10月1日起实施的《企业信息公示暂行条例》和《个体工商户年度报告暂行办法》等法规、规章的规定，2013年底前注册的，应当办理2013和2014年度的网上年报；2014年底前注册的，应办理2014年度的网上年报；以此类推。另外，企业重要信息发生变化的，应在20日内上网公示。&nbsp; &nbsp; 实在不会办理的，可以携带下列材料到辖区工商所学习：&nbsp;&nbsp;&nbsp;&nbsp;1、《营业执照》副本；2、所有的许可证副本；3、法定代表人或负责人身份证；4、手机现场接收短信，办理过网上年报的或注册过联络员的带注册登记的手机；5、公司、企业等法人单位另需资产负债表、损益表和公司章程。</p><p style="text-align: justify;"></p><p style="text-align: justify;">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;</p>
         * type : 1
         * add_time :
         * Recommend : 1
         * parkId : 1
         * class : 2
         * wyservicePoolId : 168
         * fujian :
         * desc：
         * fujian_name:
         */

        private String id;
        private String title;
        private String introduction;
        private String content;
        private String type;
        private String add_time;
        private String Recommend;
        private String parkId;
        @SerializedName("class")
        private String classX;
        private String wyservicePoolId;
        private String fujian;
        private String desc;
        private String fujian_name;


        public String getFujian_name() {
            return fujian_name;
        }

        public void setFujian_name(String fujian_name) {
            this.fujian_name = fujian_name;
        }

        public String getDesc() {
            return desc;
        }

        public void setDesc(String desc) {
            this.desc = desc;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getIntroduction() {
            return introduction;
        }

        public void setIntroduction(String introduction) {
            this.introduction = introduction;
        }

        public String getContent() {
            return content;
        }

        public void setContent(String content) {
            this.content = content;
        }

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public String getAdd_time() {
            return add_time;
        }

        public void setAdd_time(String add_time) {
            this.add_time = add_time;
        }

        public String getRecommend() {
            return Recommend;
        }

        public void setRecommend(String Recommend) {
            this.Recommend = Recommend;
        }

        public String getParkId() {
            return parkId;
        }

        public void setParkId(String parkId) {
            this.parkId = parkId;
        }

        public String getClassX() {
            return classX;
        }

        public void setClassX(String classX) {
            this.classX = classX;
        }

        public String getWyservicePoolId() {
            return wyservicePoolId;
        }

        public void setWyservicePoolId(String wyservicePoolId) {
            this.wyservicePoolId = wyservicePoolId;
        }

        public String getFujian() {
            return fujian;
        }

        public void setFujian(String fujian) {
            this.fujian = fujian;
        }
    }

    public static class RequestBean {
        /**
         * parkId : 1
         * id : 7
         * typeId : 2
         */

        private String parkId;
        private String id;
        private String typeId;

        public String getParkId() {
            return parkId;
        }

        public void setParkId(String parkId) {
            this.parkId = parkId;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getTypeId() {
            return typeId;
        }

        public void setTypeId(String typeId) {
            this.typeId = typeId;
        }
    }
}
