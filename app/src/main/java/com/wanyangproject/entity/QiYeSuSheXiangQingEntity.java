package com.wanyangproject.entity;

import java.util.List;

/**
 * Created by 甜甜 on 2018/9/6.
 */

public class QiYeSuSheXiangQingEntity {
    /**
     * code : 200
     * msg :
     * response : {"code":"200","message":"OK","data":{"dormitoryId":"254","dormitoryName":"1007寝室","parkName":"温州平阳众创城","parcelName":"F-07","buildingName":"F07#宿舍楼","roomName":"F07#宿舍楼-1007","enterName":"温州驰骋制罐有限公司"},"shui":{"code":"200","message":"OK","data":[{"waterUserId":"677","waterUserName":"F07地块10F-1007冷         ","yu":"-15"},{"waterUserId":"720","waterUserName":"F07地块10F-1007热         ","yu":"-20"}]},"dian":{"code":"40013","message":"接口查出结果为空"}}
     * request : {"parkId":"1","dormitoryId":"254"}
     * other : []
     */

    private int code;
    private String msg;
    private ResponseBean response;
    private RequestBean request;
    private List<?> other;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public ResponseBean getResponse() {
        return response;
    }

    public void setResponse(ResponseBean response) {
        this.response = response;
    }

    public RequestBean getRequest() {
        return request;
    }

    public void setRequest(RequestBean request) {
        this.request = request;
    }

    public List<?> getOther() {
        return other;
    }

    public void setOther(List<?> other) {
        this.other = other;
    }

    public static class ResponseBean {
        /**
         * code : 200
         * message : OK
         * data : {"dormitoryId":"254","dormitoryName":"1007寝室","parkName":"温州平阳众创城","parcelName":"F-07","buildingName":"F07#宿舍楼","roomName":"F07#宿舍楼-1007","enterName":"温州驰骋制罐有限公司"}
         * shui : {"code":"200","message":"OK","data":[{"waterUserId":"677","waterUserName":"F07地块10F-1007冷         ","yu":"-15"},{"waterUserId":"720","waterUserName":"F07地块10F-1007热         ","yu":"-20"}]}
         * dian : {"code":"40013","message":"接口查出结果为空"}
         */

        private String code;
        private String message;
        private DataBean data;
        private ShuiBean shui;
        private DianBean dian;

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        public DataBean getData() {
            return data;
        }

        public void setData(DataBean data) {
            this.data = data;
        }

        public ShuiBean getShui() {
            return shui;
        }

        public void setShui(ShuiBean shui) {
            this.shui = shui;
        }

        public DianBean getDian() {
            return dian;
        }

        public void setDian(DianBean dian) {
            this.dian = dian;
        }

        public static class DataBean {
            /**
             * dormitoryId : 254
             * dormitoryName : 1007寝室
             * parkName : 温州平阳众创城
             * parcelName : F-07
             * buildingName : F07#宿舍楼
             * roomName : F07#宿舍楼-1007
             * enterName : 温州驰骋制罐有限公司
             */

            private String dormitoryId;
            private String dormitoryName;
            private String parkName;
            private String parcelName;
            private String buildingName;
            private String roomName;
            private String enterName;

            public String getDormitoryId() {
                return dormitoryId;
            }

            public void setDormitoryId(String dormitoryId) {
                this.dormitoryId = dormitoryId;
            }

            public String getDormitoryName() {
                return dormitoryName;
            }

            public void setDormitoryName(String dormitoryName) {
                this.dormitoryName = dormitoryName;
            }

            public String getParkName() {
                return parkName;
            }

            public void setParkName(String parkName) {
                this.parkName = parkName;
            }

            public String getParcelName() {
                return parcelName;
            }

            public void setParcelName(String parcelName) {
                this.parcelName = parcelName;
            }

            public String getBuildingName() {
                return buildingName;
            }

            public void setBuildingName(String buildingName) {
                this.buildingName = buildingName;
            }

            public String getRoomName() {
                return roomName;
            }

            public void setRoomName(String roomName) {
                this.roomName = roomName;
            }

            public String getEnterName() {
                return enterName;
            }

            public void setEnterName(String enterName) {
                this.enterName = enterName;
            }
        }

        public static class ShuiBean {
            /**
             * code : 200
             * message : OK
             * data : [{"waterUserId":"677","waterUserName":"F07地块10F-1007冷         ","yu":"-15"},{"waterUserId":"720","waterUserName":"F07地块10F-1007热         ","yu":"-20"}]
             */

            private String code;
            private String message;
            private List<DataBeanX> data;

            public String getCode() {
                return code;
            }

            public void setCode(String code) {
                this.code = code;
            }

            public String getMessage() {
                return message;
            }

            public void setMessage(String message) {
                this.message = message;
            }

            public List<DataBeanX> getData() {
                return data;
            }

            public void setData(List<DataBeanX> data) {
                this.data = data;
            }

            public static class DataBeanX {
                /**
                 * waterUserId : 677
                 * waterUserName : F07地块10F-1007冷
                 * yu : -15
                 */

                private String waterUserId;
                private String waterUserName;
                private String yu;

                public String getWaterUserId() {
                    return waterUserId;
                }

                public void setWaterUserId(String waterUserId) {
                    this.waterUserId = waterUserId;
                }

                public String getWaterUserName() {
                    return waterUserName;
                }

                public void setWaterUserName(String waterUserName) {
                    this.waterUserName = waterUserName;
                }

                public String getYu() {
                    return yu;
                }

                public void setYu(String yu) {
                    this.yu = yu;
                }
            }
        }

        public static class DianBean {
            /**
             * code : 40013
             * message : 接口查出结果为空
             */

            private String code;
            private String message;

            public String getCode() {
                return code;
            }

            public void setCode(String code) {
                this.code = code;
            }

            public String getMessage() {
                return message;
            }

            public void setMessage(String message) {
                this.message = message;
            }
        }
    }

    public static class RequestBean {
        /**
         * parkId : 1
         * dormitoryId : 254
         */

        private String parkId;
        private String dormitoryId;

        public String getParkId() {
            return parkId;
        }

        public void setParkId(String parkId) {
            this.parkId = parkId;
        }

        public String getDormitoryId() {
            return dormitoryId;
        }

        public void setDormitoryId(String dormitoryId) {
            this.dormitoryId = dormitoryId;
        }
    }


//
//    /**
//     * code : 200
//     * msg :
//     * response : {"code":"200","message":"OK","data":{"dormitoryId":"1","dormitoryName":"301寝室","parkName":"温州平阳众创城","parcelName":"F-07","buildingName":"F07#宿舍楼","roomName":"F07#宿舍楼-301","enterName":"众创城园区运营子公司"}}
//     * request : {"parkId":"1","dormitoryId":"1"}
//     * other : []
//     */
//
//    private int code;
//    private String msg;
//    private ResponseBean response;
//    private RequestBean request;
//    private List<?> other;
//
//    public int getCode() {
//        return code;
//    }
//
//    public void setCode(int code) {
//        this.code = code;
//    }
//
//    public String getMsg() {
//        return msg;
//    }
//
//    public void setMsg(String msg) {
//        this.msg = msg;
//    }
//
//    public ResponseBean getResponse() {
//        return response;
//    }
//
//    public void setResponse(ResponseBean response) {
//        this.response = response;
//    }
//
//    public RequestBean getRequest() {
//        return request;
//    }
//
//    public void setRequest(RequestBean request) {
//        this.request = request;
//    }
//
//    public List<?> getOther() {
//        return other;
//    }
//
//    public void setOther(List<?> other) {
//        this.other = other;
//    }
//
//    public static class ResponseBean {
//        /**
//         * code : 200
//         * message : OK
//         * data : {"dormitoryId":"1","dormitoryName":"301寝室","parkName":"温州平阳众创城","parcelName":"F-07","buildingName":"F07#宿舍楼","roomName":"F07#宿舍楼-301","enterName":"众创城园区运营子公司"}
//         */
//
//        private String code;
//        private String message;
//        private DataBean data;
//
//        public String getCode() {
//            return code;
//        }
//
//        public void setCode(String code) {
//            this.code = code;
//        }
//
//        public String getMessage() {
//            return message;
//        }
//
//        public void setMessage(String message) {
//            this.message = message;
//        }
//
//        public DataBean getData() {
//            return data;
//        }
//
//        public void setData(DataBean data) {
//            this.data = data;
//        }
//
//        public static class DataBean {
//            /**
//             * dormitoryId : 1
//             * dormitoryName : 301寝室
//             * parkName : 温州平阳众创城
//             * parcelName : F-07
//             * buildingName : F07#宿舍楼
//             * roomName : F07#宿舍楼-301
//             * enterName : 众创城园区运营子公司
//             */
//
//            private String dormitoryId;
//            private String dormitoryName;
//            private String parkName;
//            private String parcelName;
//            private String buildingName;
//            private String roomName;
//            private String enterName;
//
//            public String getDormitoryId() {
//                return dormitoryId;
//            }
//
//            public void setDormitoryId(String dormitoryId) {
//                this.dormitoryId = dormitoryId;
//            }
//
//            public String getDormitoryName() {
//                return dormitoryName;
//            }
//
//            public void setDormitoryName(String dormitoryName) {
//                this.dormitoryName = dormitoryName;
//            }
//
//            public String getParkName() {
//                return parkName;
//            }
//
//            public void setParkName(String parkName) {
//                this.parkName = parkName;
//            }
//
//            public String getParcelName() {
//                return parcelName;
//            }
//
//            public void setParcelName(String parcelName) {
//                this.parcelName = parcelName;
//            }
//
//            public String getBuildingName() {
//                return buildingName;
//            }
//
//            public void setBuildingName(String buildingName) {
//                this.buildingName = buildingName;
//            }
//
//            public String getRoomName() {
//                return roomName;
//            }
//
//            public void setRoomName(String roomName) {
//                this.roomName = roomName;
//            }
//
//            public String getEnterName() {
//                return enterName;
//            }
//
//            public void setEnterName(String enterName) {
//                this.enterName = enterName;
//            }
//        }
//    }
//
//    public static class RequestBean {
//        /**
//         * parkId : 1
//         * dormitoryId : 1
//         */
//
//        private String parkId;
//        private String dormitoryId;
//
//        public String getParkId() {
//            return parkId;
//        }
//
//        public void setParkId(String parkId) {
//            this.parkId = parkId;
//        }
//
//        public String getDormitoryId() {
//            return dormitoryId;
//        }
//
//        public void setDormitoryId(String dormitoryId) {
//            this.dormitoryId = dormitoryId;
//        }
//    }
}
