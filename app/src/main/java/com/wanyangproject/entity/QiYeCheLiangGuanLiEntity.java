package com.wanyangproject.entity;

import java.util.List;

/**
 * Created by 甜甜 on 2018/9/5.
 */

public class QiYeCheLiangGuanLiEntity {

    /**
     * code : 200
     * msg :
     * response : [{"vehicleId":"13","plateNumber":"浙CY9019","personName":"吕荣彬","phone":"13606778817"},{"vehicleId":"244","plateNumber":"浙C10Z05","personName":"吕荣彬","phone":"13606778817"},{"vehicleId":"258","plateNumber":"浙CL8V87","personName":"吕荣彬","phone":"13606778817"},{"vehicleId":"346","plateNumber":"浙CB2X13","personName":"吕荣彬","phone":"13606778817"},{"vehicleId":"366","plateNumber":"浙CS8V73","personName":"吕荣彬","phone":"13606778817"}]
     * request : []
     * other : []
     */

    private int code;
    private String msg;
    private List<ResponseBean> response;
    private List<?> request;
    private List<?> other;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public List<ResponseBean> getResponse() {
        return response;
    }

    public void setResponse(List<ResponseBean> response) {
        this.response = response;
    }

    public List<?> getRequest() {
        return request;
    }

    public void setRequest(List<?> request) {
        this.request = request;
    }

    public List<?> getOther() {
        return other;
    }

    public void setOther(List<?> other) {
        this.other = other;
    }

    public static class ResponseBean {
        /**
         * vehicleId : 13
         * plateNumber : 浙CY9019
         * personName : 吕荣彬
         * phone : 13606778817
         */

        private String vehicleId;
        private String plateNumber;
        private String personName;
        private String phone;

        public String getVehicleId() {
            return vehicleId;
        }

        public void setVehicleId(String vehicleId) {
            this.vehicleId = vehicleId;
        }

        public String getPlateNumber() {
            return plateNumber;
        }

        public void setPlateNumber(String plateNumber) {
            this.plateNumber = plateNumber;
        }

        public String getPersonName() {
            return personName;
        }

        public void setPersonName(String personName) {
            this.personName = personName;
        }

        public String getPhone() {
            return phone;
        }

        public void setPhone(String phone) {
            this.phone = phone;
        }
    }
}
