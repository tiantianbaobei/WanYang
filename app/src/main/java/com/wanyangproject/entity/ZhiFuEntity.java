package com.wanyangproject.entity;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by 甜甜 on 2018/8/15.
 */

public class ZhiFuEntity {
    /**
     * code : 200
     * msg :
     * response : {"prepayid":"wx17174548633306bb6922d10b1676346366","appid":"wx363ae6178db5e958","partnerid":"1511703941","package":"Sign=WXPay","noncestr":"iaBjpvMHTGrewy73db6z2mfE1nPq4t8Y","timestamp":"1534499148","sign":"1C869656A927D7ADC49876DEB6BC8DC9"}
     * request : {"order_sn":"pk2sn20180817174532","money":"0.01"}
     * other : []
     */

    private int code;
    private String msg;
    private ResponseBean response;
    private RequestBean request;
    private List<?> other;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public ResponseBean getResponse() {
        return response;
    }

    public void setResponse(ResponseBean response) {
        this.response = response;
    }

    public RequestBean getRequest() {
        return request;
    }

    public void setRequest(RequestBean request) {
        this.request = request;
    }

    public List<?> getOther() {
        return other;
    }

    public void setOther(List<?> other) {
        this.other = other;
    }

    public static class ResponseBean {
        /**
         * prepayid : wx17174548633306bb6922d10b1676346366
         * appid : wx363ae6178db5e958
         * partnerid : 1511703941
         * package : Sign=WXPay
         * noncestr : iaBjpvMHTGrewy73db6z2mfE1nPq4t8Y
         * timestamp : 1534499148
         * sign : 1C869656A927D7ADC49876DEB6BC8DC9
         */

        private String prepayid;
        private String appid;
        private String partnerid;
        @SerializedName("package")
        private String packageX;
        private String noncestr;
        private String timestamp;
        private String sign;

        public String getPrepayid() {
            return prepayid;
        }

        public void setPrepayid(String prepayid) {
            this.prepayid = prepayid;
        }

        public String getAppid() {
            return appid;
        }

        public void setAppid(String appid) {
            this.appid = appid;
        }

        public String getPartnerid() {
            return partnerid;
        }

        public void setPartnerid(String partnerid) {
            this.partnerid = partnerid;
        }

        public String getPackageX() {
            return packageX;
        }

        public void setPackageX(String packageX) {
            this.packageX = packageX;
        }

        public String getNoncestr() {
            return noncestr;
        }

        public void setNoncestr(String noncestr) {
            this.noncestr = noncestr;
        }

        public String getTimestamp() {
            return timestamp;
        }

        public void setTimestamp(String timestamp) {
            this.timestamp = timestamp;
        }

        public String getSign() {
            return sign;
        }

        public void setSign(String sign) {
            this.sign = sign;
        }
    }

    public static class RequestBean {
        /**
         * order_sn : pk2sn20180817174532
         * money : 0.01
         */

        private String order_sn;
        private String money;

        public String getOrder_sn() {
            return order_sn;
        }

        public void setOrder_sn(String order_sn) {
            this.order_sn = order_sn;
        }

        public String getMoney() {
            return money;
        }

        public void setMoney(String money) {
            this.money = money;
        }
    }


//    /**
//     * code : 200
//     * msg :
//     * response : {"prepayid":"wx15162228963516a46102b7493103473440","appid":"wx363ae6178db5e958","partnerid":"1511703941","package":"Sign=WXPay","noncestr":"F3jB2XG8yQWlHeSJRzKxYUsEC7ifNc4p","timestamp":"1534321349","sign":"8988192004F0AEB63C76435CB038F51C"}
//     * request : []
//     * other : []
//     */
//
//    private int code;
//    private String msg;
//    private ResponseBean response;
//    private List<?> request;
//    private List<?> other;
//
//    public int getCode() {
//        return code;
//    }
//
//    public void setCode(int code) {
//        this.code = code;
//    }
//
//    public String getMsg() {
//        return msg;
//    }
//
//    public void setMsg(String msg) {
//        this.msg = msg;
//    }
//
//    public ResponseBean getResponse() {
//        return response;
//    }
//
//    public void setResponse(ResponseBean response) {
//        this.response = response;
//    }
//
//    public List<?> getRequest() {
//        return request;
//    }
//
//    public void setRequest(List<?> request) {
//        this.request = request;
//    }
//
//    public List<?> getOther() {
//        return other;
//    }
//
//    public void setOther(List<?> other) {
//        this.other = other;
//    }
//
//    public static class ResponseBean {
//        /**
//         * prepayid : wx15162228963516a46102b7493103473440
//         * appid : wx363ae6178db5e958
//         * partnerid : 1511703941
//         * package : Sign=WXPay
//         * noncestr : F3jB2XG8yQWlHeSJRzKxYUsEC7ifNc4p
//         * timestamp : 1534321349
//         * sign : 8988192004F0AEB63C76435CB038F51C
//         */
//
//        private String prepayid;
//        private String appid;
//        private String partnerid;
//        @SerializedName("package")
//        private String packageX;
//        private String noncestr;
//        private String timestamp;
//        private String sign;
//
//        public String getPrepayid() {
//            return prepayid;
//        }
//
//        public void setPrepayid(String prepayid) {
//            this.prepayid = prepayid;
//        }
//
//        public String getAppid() {
//            return appid;
//        }
//
//        public void setAppid(String appid) {
//            this.appid = appid;
//        }
//
//        public String getPartnerid() {
//            return partnerid;
//        }
//
//        public void setPartnerid(String partnerid) {
//            this.partnerid = partnerid;
//        }
//
//        public String getPackageX() {
//            return packageX;
//        }
//
//        public void setPackageX(String packageX) {
//            this.packageX = packageX;
//        }
//
//        public String getNoncestr() {
//            return noncestr;
//        }
//
//        public void setNoncestr(String noncestr) {
//            this.noncestr = noncestr;
//        }
//
//        public String getTimestamp() {
//            return timestamp;
//        }
//
//        public void setTimestamp(String timestamp) {
//            this.timestamp = timestamp;
//        }
//
//        public String getSign() {
//            return sign;
//        }
//
//        public void setSign(String sign) {
//            this.sign = sign;
//        }
//    }
}
