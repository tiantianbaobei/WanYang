package com.wanyangproject.entity;

import java.util.List;

/**
 * Created by 甜甜 on 2018/8/30.
 */

public class ShangPinXiangQingEntity {
    /**
     * code : 200
     * msg :
     * response : {"goods":{"discount":"","goods_name":"奶茶","id":"96","master":["http://park.hostop.net/upload/20181128/300a719a1be0ae1c2a7adcfbe2445619.jpg","http://park.hostop.net/upload/20181128/aacc702cf64a902cbbeda1b88af2c0e5.jpg","http://park.hostop.net/upload/20181128/b68265ca3853a02b627463f7b97f2d41.jpg"],"master_2":"http://park.hostop.net/upload/20181128/aacc702cf64a902cbbeda1b88af2c0e5.jpg","master_3":"http://park.hostop.net/upload/20181128/b68265ca3853a02b627463f7b97f2d41.jpg","master_4":"","master_5":"","pic":"0.01","volume":"0","fabulous":"0","market":"0.01","content":""}}
     * request : {"parkId":"1","goodsid":"96"}
     * other : []
     */

    private int code;
    private String msg;
    private ResponseBean response;
    private RequestBean request;
    private List<?> other;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public ResponseBean getResponse() {
        return response;
    }

    public void setResponse(ResponseBean response) {
        this.response = response;
    }

    public RequestBean getRequest() {
        return request;
    }

    public void setRequest(RequestBean request) {
        this.request = request;
    }

    public List<?> getOther() {
        return other;
    }

    public void setOther(List<?> other) {
        this.other = other;
    }

    public static class ResponseBean {
        /**
         * goods : {"discount":"","goods_name":"奶茶","id":"96","master":["http://park.hostop.net/upload/20181128/300a719a1be0ae1c2a7adcfbe2445619.jpg","http://park.hostop.net/upload/20181128/aacc702cf64a902cbbeda1b88af2c0e5.jpg","http://park.hostop.net/upload/20181128/b68265ca3853a02b627463f7b97f2d41.jpg"],"master_2":"http://park.hostop.net/upload/20181128/aacc702cf64a902cbbeda1b88af2c0e5.jpg","master_3":"http://park.hostop.net/upload/20181128/b68265ca3853a02b627463f7b97f2d41.jpg","master_4":"","master_5":"","pic":"0.01","volume":"0","fabulous":"0","market":"0.01","content":""}
         */

        private GoodsBean goods;

        public GoodsBean getGoods() {
            return goods;
        }

        public void setGoods(GoodsBean goods) {
            this.goods = goods;
        }

        public static class GoodsBean {
            /**
             * discount :
             * goods_name : 奶茶
             * id : 96
             * master : ["http://park.hostop.net/upload/20181128/300a719a1be0ae1c2a7adcfbe2445619.jpg","http://park.hostop.net/upload/20181128/aacc702cf64a902cbbeda1b88af2c0e5.jpg","http://park.hostop.net/upload/20181128/b68265ca3853a02b627463f7b97f2d41.jpg"]
             * master_2 : http://park.hostop.net/upload/20181128/aacc702cf64a902cbbeda1b88af2c0e5.jpg
             * master_3 : http://park.hostop.net/upload/20181128/b68265ca3853a02b627463f7b97f2d41.jpg
             * master_4 :
             * master_5 :
             * pic : 0.01
             * volume : 0
             * fabulous : 0
             * market : 0.01
             * content :
             */

            private String discount;
            private String goods_name;
            private String id;
            private String master_2;
            private String master_3;
            private String master_4;
            private String master_5;
            private String pic;
            private String volume;
            private String fabulous;
            private String market;
            private String content;
            private List<String> master;

            public String getDiscount() {
                return discount;
            }

            public void setDiscount(String discount) {
                this.discount = discount;
            }

            public String getGoods_name() {
                return goods_name;
            }

            public void setGoods_name(String goods_name) {
                this.goods_name = goods_name;
            }

            public String getId() {
                return id;
            }

            public void setId(String id) {
                this.id = id;
            }

            public String getMaster_2() {
                return master_2;
            }

            public void setMaster_2(String master_2) {
                this.master_2 = master_2;
            }

            public String getMaster_3() {
                return master_3;
            }

            public void setMaster_3(String master_3) {
                this.master_3 = master_3;
            }

            public String getMaster_4() {
                return master_4;
            }

            public void setMaster_4(String master_4) {
                this.master_4 = master_4;
            }

            public String getMaster_5() {
                return master_5;
            }

            public void setMaster_5(String master_5) {
                this.master_5 = master_5;
            }

            public String getPic() {
                return pic;
            }

            public void setPic(String pic) {
                this.pic = pic;
            }

            public String getVolume() {
                return volume;
            }

            public void setVolume(String volume) {
                this.volume = volume;
            }

            public String getFabulous() {
                return fabulous;
            }

            public void setFabulous(String fabulous) {
                this.fabulous = fabulous;
            }

            public String getMarket() {
                return market;
            }

            public void setMarket(String market) {
                this.market = market;
            }

            public String getContent() {
                return content;
            }

            public void setContent(String content) {
                this.content = content;
            }

            public List<String> getMaster() {
                return master;
            }

            public void setMaster(List<String> master) {
                this.master = master;
            }
        }
    }

    public static class RequestBean {
        /**
         * parkId : 1
         * goodsid : 96
         */

        private String parkId;
        private String goodsid;

        public String getParkId() {
            return parkId;
        }

        public void setParkId(String parkId) {
            this.parkId = parkId;
        }

        public String getGoodsid() {
            return goodsid;
        }

        public void setGoodsid(String goodsid) {
            this.goodsid = goodsid;
        }
    }


//    /**
//     * code : 200
//     * msg :
//     * response : {"goods":{"discount":"","goods_name":"测试","id":"30","master":"park.hostop.net//upload/20180911/21a8b535f68307c80150cf8ea7760175.jpg","pic":"0.01","volume":"5","fabulous":"0","market":"200","content":""}}
//     * request : {"parkId":"1","goodsid":"30"}
//     * other : []
//     */
//
//    private int code;
//    private String msg;
//    private ResponseBean response;
//    private RequestBean request;
//    private List<?> other;
//
//    public int getCode() {
//        return code;
//    }
//
//    public void setCode(int code) {
//        this.code = code;
//    }
//
//    public String getMsg() {
//        return msg;
//    }
//
//    public void setMsg(String msg) {
//        this.msg = msg;
//    }
//
//    public ResponseBean getResponse() {
//        return response;
//    }
//
//    public void setResponse(ResponseBean response) {
//        this.response = response;
//    }
//
//    public RequestBean getRequest() {
//        return request;
//    }
//
//    public void setRequest(RequestBean request) {
//        this.request = request;
//    }
//
//    public List<?> getOther() {
//        return other;
//    }
//
//    public void setOther(List<?> other) {
//        this.other = other;
//    }
//
//    public static class ResponseBean {
//        /**
//         * goods : {"discount":"","goods_name":"测试","id":"30","master":"park.hostop.net//upload/20180911/21a8b535f68307c80150cf8ea7760175.jpg","pic":"0.01","volume":"5","fabulous":"0","market":"200","content":""}
//         */
//
//        private GoodsBean goods;
//
//        public GoodsBean getGoods() {
//            return goods;
//        }
//
//        public void setGoods(GoodsBean goods) {
//            this.goods = goods;
//        }
//
//        public static class GoodsBean {
//            /**
//             * discount :
//             * goods_name : 测试
//             * id : 30
//             * master : park.hostop.net//upload/20180911/21a8b535f68307c80150cf8ea7760175.jpg
//             * pic : 0.01
//             * volume : 5
//             * fabulous : 0
//             * market : 200
//             * content :
//             */
//
//            private String discount;
//            private String goods_name;
//            private String id;
//            private String master;
//            private String pic;
//            private String volume;
//            private String fabulous;
//            private String market;
//            private String content;
//
//            public String getDiscount() {
//                return discount;
//            }
//
//            public void setDiscount(String discount) {
//                this.discount = discount;
//            }
//
//            public String getGoods_name() {
//                return goods_name;
//            }
//
//            public void setGoods_name(String goods_name) {
//                this.goods_name = goods_name;
//            }
//
//            public String getId() {
//                return id;
//            }
//
//            public void setId(String id) {
//                this.id = id;
//            }
//
//            public String getMaster() {
//                return master;
//            }
//
//            public void setMaster(String master) {
//                this.master = master;
//            }
//
//            public String getPic() {
//                return pic;
//            }
//
//            public void setPic(String pic) {
//                this.pic = pic;
//            }
//
//            public String getVolume() {
//                return volume;
//            }
//
//            public void setVolume(String volume) {
//                this.volume = volume;
//            }
//
//            public String getFabulous() {
//                return fabulous;
//            }
//
//            public void setFabulous(String fabulous) {
//                this.fabulous = fabulous;
//            }
//
//            public String getMarket() {
//                return market;
//            }
//
//            public void setMarket(String market) {
//                this.market = market;
//            }
//
//            public String getContent() {
//                return content;
//            }
//
//            public void setContent(String content) {
//                this.content = content;
//            }
//        }
//    }
//
//    public static class RequestBean {
//        /**
//         * parkId : 1
//         * goodsid : 30
//         */
//
//        private String parkId;
//        private String goodsid;
//
//        public String getParkId() {
//            return parkId;
//        }
//
//        public void setParkId(String parkId) {
//            this.parkId = parkId;
//        }
//
//        public String getGoodsid() {
//            return goodsid;
//        }
//
//        public void setGoodsid(String goodsid) {
//            this.goodsid = goodsid;
//        }
//    }




















//
//    /**
//     * code : 200
//     * msg :
//     * response : {"goods":{"goods_name":"测试","id":"30","master":"park.hostop.net//upload/20180911/21a8b535f68307c80150cf8ea7760175.jpg","pic":"0.01","volume":"0","fabulous":"0","market":"10","content":""}}
//     * request : {"parkId":"1","goodsid":"30"}
//     * other : []
//     */
//
//    private int code;
//    private String msg;
//    private ResponseBean response;
//    private RequestBean request;
//    private List<?> other;
//
//    public int getCode() {
//        return code;
//    }
//
//    public void setCode(int code) {
//        this.code = code;
//    }
//
//    public String getMsg() {
//        return msg;
//    }
//
//    public void setMsg(String msg) {
//        this.msg = msg;
//    }
//
//    public ResponseBean getResponse() {
//        return response;
//    }
//
//    public void setResponse(ResponseBean response) {
//        this.response = response;
//    }
//
//    public RequestBean getRequest() {
//        return request;
//    }
//
//    public void setRequest(RequestBean request) {
//        this.request = request;
//    }
//
//    public List<?> getOther() {
//        return other;
//    }
//
//    public void setOther(List<?> other) {
//        this.other = other;
//    }
//
//    public static class ResponseBean {
//        /**
//         * goods : {"goods_name":"测试","id":"30","master":"park.hostop.net//upload/20180911/21a8b535f68307c80150cf8ea7760175.jpg","pic":"0.01","volume":"0","fabulous":"0","market":"10","content":""}
//         */
//
//        private GoodsBean goods;
//
//        public GoodsBean getGoods() {
//            return goods;
//        }
//
//        public void setGoods(GoodsBean goods) {
//            this.goods = goods;
//        }
//
//        public static class GoodsBean {
//            /**
//             * goods_name : 测试
//             * id : 30
//             * master : park.hostop.net//upload/20180911/21a8b535f68307c80150cf8ea7760175.jpg
//             * pic : 0.01
//             * volume : 0
//             * fabulous : 0
//             * market : 10
//             * content :
//             */
//
//            private String goods_name;
//            private String id;
//            private String master;
//            private String pic;
//            private String volume;
//            private String fabulous;
//            private String market;
//            private String content;
//
//            public String getGoods_name() {
//                return goods_name;
//            }
//
//            public void setGoods_name(String goods_name) {
//                this.goods_name = goods_name;
//            }
//
//            public String getId() {
//                return id;
//            }
//
//            public void setId(String id) {
//                this.id = id;
//            }
//
//            public String getMaster() {
//                return master;
//            }
//
//            public void setMaster(String master) {
//                this.master = master;
//            }
//
//            public String getPic() {
//                return pic;
//            }
//
//            public void setPic(String pic) {
//                this.pic = pic;
//            }
//
//            public String getVolume() {
//                return volume;
//            }
//
//            public void setVolume(String volume) {
//                this.volume = volume;
//            }
//
//            public String getFabulous() {
//                return fabulous;
//            }
//
//            public void setFabulous(String fabulous) {
//                this.fabulous = fabulous;
//            }
//
//            public String getMarket() {
//                return market;
//            }
//
//            public void setMarket(String market) {
//                this.market = market;
//            }
//
//            public String getContent() {
//                return content;
//            }
//
//            public void setContent(String content) {
//                this.content = content;
//            }
//        }
//    }
//
//    public static class RequestBean {
//        /**
//         * parkId : 1
//         * goodsid : 30
//         */
//
//        private String parkId;
//        private String goodsid;
//
//        public String getParkId() {
//            return parkId;
//        }
//
//        public void setParkId(String parkId) {
//            this.parkId = parkId;
//        }
//
//        public String getGoodsid() {
//            return goodsid;
//        }
//
//        public void setGoodsid(String goodsid) {
//            this.goodsid = goodsid;
//        }
//    }
}
