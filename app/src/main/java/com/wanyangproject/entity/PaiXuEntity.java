package com.wanyangproject.entity;

import java.util.List;

/**
 * Created by 甜甜 on 2018/8/8.
 */

public class PaiXuEntity {
    /**
     * code : 200
     * msg :
     * response : [{"nickname":"统一银座","sales":"19","starting":"0","business":"1","photo":"park.hostop.net//upload/20180825/7d102fa5387124d0a053a7be5af34741.jpg","cost":"0","id":"4"},{"nickname":"商家","sales":"6","starting":"0","business":"1","photo":"park.hostop.net//upload/20180823/bab664273f521cea285761a6e79569f6.png","cost":"0","id":"1"},{"nickname":"测试名称","sales":"0","starting":"0","business":"2","photo":"park.hostop.net//static/image/shang.png","cost":"0","id":"23"},{"nickname":"一分钟","sales":"0","starting":"0","business":"2","photo":"park.hostop.net//static/image/shang.png","cost":"0","id":"25"}]
     * request : {"parkId":"1","classId":"1","sales":"1"}
     * other : []
     */

    private int code;
    private String msg;
    private RequestBean request;
    private List<ResponseBean> response;
    private List<?> other;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public RequestBean getRequest() {
        return request;
    }

    public void setRequest(RequestBean request) {
        this.request = request;
    }

    public List<ResponseBean> getResponse() {
        return response;
    }

    public void setResponse(List<ResponseBean> response) {
        this.response = response;
    }

    public List<?> getOther() {
        return other;
    }

    public void setOther(List<?> other) {
        this.other = other;
    }

    public static class RequestBean {
        /**
         * parkId : 1
         * classId : 1
         * sales : 1
         */

        private String parkId;
        private String classId;
        private String sales;

        public String getParkId() {
            return parkId;
        }

        public void setParkId(String parkId) {
            this.parkId = parkId;
        }

        public String getClassId() {
            return classId;
        }

        public void setClassId(String classId) {
            this.classId = classId;
        }

        public String getSales() {
            return sales;
        }

        public void setSales(String sales) {
            this.sales = sales;
        }
    }

    public static class ResponseBean {
        /**
         * nickname : 统一银座
         * sales : 19
         * starting : 0
         * business : 1
         * photo : park.hostop.net//upload/20180825/7d102fa5387124d0a053a7be5af34741.jpg
         * cost : 0
         * id : 4
         */

        private String nickname;
        private String sales;
        private String starting;
        private String business;
        private String photo;
        private String cost;
        private String id;

        public String getNickname() {
            return nickname;
        }

        public void setNickname(String nickname) {
            this.nickname = nickname;
        }

        public String getSales() {
            return sales;
        }

        public void setSales(String sales) {
            this.sales = sales;
        }

        public String getStarting() {
            return starting;
        }

        public void setStarting(String starting) {
            this.starting = starting;
        }

        public String getBusiness() {
            return business;
        }

        public void setBusiness(String business) {
            this.business = business;
        }

        public String getPhoto() {
            return photo;
        }

        public void setPhoto(String photo) {
            this.photo = photo;
        }

        public String getCost() {
            return cost;
        }

        public void setCost(String cost) {
            this.cost = cost;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }
    }


//
//    /**
//     * code : 200
//     * msg :
//     * response : [{"nickname":"统一银座","sales":"20","starting":"20","photo":"park.hostop.net/static/image/0.jpg","cost":"10","id":"2"}]
//     * request : {"parkId":"1","classId":"2"}
//     * other : []
//     */
//
//    private int code;
//    private String msg;
//    private RequestBean request;
//    private List<ResponseBean> response;
//    private List<?> other;
//
//    public int getCode() {
//        return code;
//    }
//
//    public void setCode(int code) {
//        this.code = code;
//    }
//
//    public String getMsg() {
//        return msg;
//    }
//
//    public void setMsg(String msg) {
//        this.msg = msg;
//    }
//
//    public RequestBean getRequest() {
//        return request;
//    }
//
//    public void setRequest(RequestBean request) {
//        this.request = request;
//    }
//
//    public List<ResponseBean> getResponse() {
//        return response;
//    }
//
//    public void setResponse(List<ResponseBean> response) {
//        this.response = response;
//    }
//
//    public List<?> getOther() {
//        return other;
//    }
//
//    public void setOther(List<?> other) {
//        this.other = other;
//    }
//
//    public static class RequestBean {
//        /**
//         * parkId : 1
//         * classId : 2
//         */
//
//        private String parkId;
//        private String classId;
//
//        public String getParkId() {
//            return parkId;
//        }
//
//        public void setParkId(String parkId) {
//            this.parkId = parkId;
//        }
//
//        public String getClassId() {
//            return classId;
//        }
//
//        public void setClassId(String classId) {
//            this.classId = classId;
//        }
//    }
//
//    public static class ResponseBean {
//        /**
//         * nickname : 统一银座
//         * sales : 20
//         * starting : 20
//         * photo : park.hostop.net/static/image/0.jpg
//         * cost : 10
//         * id : 2
//         */
//
//        private String nickname;
//        private String sales;
//        private String starting;
//        private String photo;
//        private String cost;
//        private String id;
//
//        public String getNickname() {
//            return nickname;
//        }
//
//        public void setNickname(String nickname) {
//            this.nickname = nickname;
//        }
//
//        public String getSales() {
//            return sales;
//        }
//
//        public void setSales(String sales) {
//            this.sales = sales;
//        }
//
//        public String getStarting() {
//            return starting;
//        }
//
//        public void setStarting(String starting) {
//            this.starting = starting;
//        }
//
//        public String getPhoto() {
//            return photo;
//        }
//
//        public void setPhoto(String photo) {
//            this.photo = photo;
//        }
//
//        public String getCost() {
//            return cost;
//        }
//
//        public void setCost(String cost) {
//            this.cost = cost;
//        }
//
//        public String getId() {
//            return id;
//        }
//
//        public void setId(String id) {
//            this.id = id;
//        }
//    }
}
