package com.wanyangproject.entity;

import java.util.List;

/**
 * Created by 甜甜 on 2018/8/6.
 */

public class RegisterEntity {

    /**
     * code : 200
     * msg :
     * response : {"userid":"2","username":"17600904682","avatar":"/static/images/toux.png","nickname":"未填写","realname":"","uuid":"0","parkId":"0","parkInfo":{"parkId":"1","parkName":"温州平阳众创城"},"token":"b49cd84de79b665fbb630190cc054044"}
     * request : {"phone":"17600904682","password":"123456789","repwd":"123456789","code":"792055","authId":"31"}
     * other : []
     */

    private int code;
    private String msg;
    private ResponseBean response;
    private RequestBean request;
    private List<?> other;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public ResponseBean getResponse() {
        return response;
    }

    public void setResponse(ResponseBean response) {
        this.response = response;
    }

    public RequestBean getRequest() {
        return request;
    }

    public void setRequest(RequestBean request) {
        this.request = request;
    }

    public List<?> getOther() {
        return other;
    }

    public void setOther(List<?> other) {
        this.other = other;
    }

    public static class ResponseBean {
        /**
         * userid : 2
         * username : 17600904682
         * avatar : /static/images/toux.png
         * nickname : 未填写
         * realname :
         * uuid : 0
         * parkId : 0
         * parkInfo : {"parkId":"1","parkName":"温州平阳众创城"}
         * token : b49cd84de79b665fbb630190cc054044
         */

        private String userid;
        private String username;
        private String avatar;
        private String nickname;
        private String realname;
        private String uuid;
        private String parkId;
        private ParkInfoBean parkInfo;
        private String token;

        public String getUserid() {
            return userid;
        }

        public void setUserid(String userid) {
            this.userid = userid;
        }

        public String getUsername() {
            return username;
        }

        public void setUsername(String username) {
            this.username = username;
        }

        public String getAvatar() {
            return avatar;
        }

        public void setAvatar(String avatar) {
            this.avatar = avatar;
        }

        public String getNickname() {
            return nickname;
        }

        public void setNickname(String nickname) {
            this.nickname = nickname;
        }

        public String getRealname() {
            return realname;
        }

        public void setRealname(String realname) {
            this.realname = realname;
        }

        public String getUuid() {
            return uuid;
        }

        public void setUuid(String uuid) {
            this.uuid = uuid;
        }

        public String getParkId() {
            return parkId;
        }

        public void setParkId(String parkId) {
            this.parkId = parkId;
        }

        public ParkInfoBean getParkInfo() {
            return parkInfo;
        }

        public void setParkInfo(ParkInfoBean parkInfo) {
            this.parkInfo = parkInfo;
        }

        public String getToken() {
            return token;
        }

        public void setToken(String token) {
            this.token = token;
        }

        public static class ParkInfoBean {
            /**
             * parkId : 1
             * parkName : 温州平阳众创城
             */

            private String parkId;
            private String parkName;

            public String getParkId() {
                return parkId;
            }

            public void setParkId(String parkId) {
                this.parkId = parkId;
            }

            public String getParkName() {
                return parkName;
            }

            public void setParkName(String parkName) {
                this.parkName = parkName;
            }
        }
    }

    public static class RequestBean {
        /**
         * phone : 17600904682
         * password : 123456789
         * repwd : 123456789
         * code : 792055
         * authId : 31
         */

        private String phone;
        private String password;
        private String repwd;
        private String code;
        private String authId;

        public String getPhone() {
            return phone;
        }

        public void setPhone(String phone) {
            this.phone = phone;
        }

        public String getPassword() {
            return password;
        }

        public void setPassword(String password) {
            this.password = password;
        }

        public String getRepwd() {
            return repwd;
        }

        public void setRepwd(String repwd) {
            this.repwd = repwd;
        }

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        public String getAuthId() {
            return authId;
        }

        public void setAuthId(String authId) {
            this.authId = authId;
        }
    }
}
