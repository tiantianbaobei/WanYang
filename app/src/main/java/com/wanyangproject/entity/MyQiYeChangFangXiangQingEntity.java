package com.wanyangproject.entity;

import java.util.List;

/**
 * Created by 甜甜 on 2018/9/6.
 */

public class MyQiYeChangFangXiangQingEntity {
    /**
     * code : 200
     * msg :
     * response : {"code":"200","message":"OK","data":{"workshopId":"82","workshopName":"驰骋制罐      F07#7-201","parkName":"温州平阳众创城","parcelName":"F-07","buildingName":"F07#7","roomName":"F07#7-201","enterName":"温州驰骋制罐有限公司"},"shui":{"code":"200","message":"OK","data":[{"waterUserId":"40","waterUserName":"F07D-7201"}]},"dian":{"code":"40013","message":"接口查出结果为空"}}
     * request : {"parkId":"1","workshopId":"82"}
     * other : []
     */

    private int code;
    private String msg;
    private ResponseBean response;
    private RequestBean request;
    private List<?> other;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public ResponseBean getResponse() {
        return response;
    }

    public void setResponse(ResponseBean response) {
        this.response = response;
    }

    public RequestBean getRequest() {
        return request;
    }

    public void setRequest(RequestBean request) {
        this.request = request;
    }

    public List<?> getOther() {
        return other;
    }

    public void setOther(List<?> other) {
        this.other = other;
    }

    public static class ResponseBean {
        /**
         * code : 200
         * message : OK
         * data : {"workshopId":"82","workshopName":"驰骋制罐      F07#7-201","parkName":"温州平阳众创城","parcelName":"F-07","buildingName":"F07#7","roomName":"F07#7-201","enterName":"温州驰骋制罐有限公司"}
         * shui : {"code":"200","message":"OK","data":[{"waterUserId":"40","waterUserName":"F07D-7201"}]}
         * dian : {"code":"40013","message":"接口查出结果为空"}
         */

        private String code;
        private String message;
        private DataBean data;
        private ShuiBean shui;
        private DianBean dian;

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        public DataBean getData() {
            return data;
        }

        public void setData(DataBean data) {
            this.data = data;
        }

        public ShuiBean getShui() {
            return shui;
        }

        public void setShui(ShuiBean shui) {
            this.shui = shui;
        }

        public DianBean getDian() {
            return dian;
        }

        public void setDian(DianBean dian) {
            this.dian = dian;
        }

        public static class DataBean {
            /**
             * workshopId : 82
             * workshopName : 驰骋制罐      F07#7-201
             * parkName : 温州平阳众创城
             * parcelName : F-07
             * buildingName : F07#7
             * roomName : F07#7-201
             * enterName : 温州驰骋制罐有限公司
             */

            private String workshopId;
            private String workshopName;
            private String parkName;
            private String parcelName;
            private String buildingName;
            private String roomName;
            private String enterName;

            public String getWorkshopId() {
                return workshopId;
            }

            public void setWorkshopId(String workshopId) {
                this.workshopId = workshopId;
            }

            public String getWorkshopName() {
                return workshopName;
            }

            public void setWorkshopName(String workshopName) {
                this.workshopName = workshopName;
            }

            public String getParkName() {
                return parkName;
            }

            public void setParkName(String parkName) {
                this.parkName = parkName;
            }

            public String getParcelName() {
                return parcelName;
            }

            public void setParcelName(String parcelName) {
                this.parcelName = parcelName;
            }

            public String getBuildingName() {
                return buildingName;
            }

            public void setBuildingName(String buildingName) {
                this.buildingName = buildingName;
            }

            public String getRoomName() {
                return roomName;
            }

            public void setRoomName(String roomName) {
                this.roomName = roomName;
            }

            public String getEnterName() {
                return enterName;
            }

            public void setEnterName(String enterName) {
                this.enterName = enterName;
            }
        }

        public static class ShuiBean {
            /**
             * code : 200
             * message : OK
             * data : [{"waterUserId":"40","waterUserName":"F07D-7201"}]
             */

            private String code;
            private String message;
            private List<DataBeanX> data;

            public String getCode() {
                return code;
            }

            public void setCode(String code) {
                this.code = code;
            }

            public String getMessage() {
                return message;
            }

            public void setMessage(String message) {
                this.message = message;
            }

            public List<DataBeanX> getData() {
                return data;
            }

            public void setData(List<DataBeanX> data) {
                this.data = data;
            }

            public static class DataBeanX {
                /**
                 * waterUserId : 40
                 * waterUserName : F07D-7201
                 *
                 */

                private String waterUserId;
                private String waterUserName;
                private String yu;


                public String getYu() {
                    return yu;
                }

                public void setYu(String yu) {
                    this.yu = yu;
                }

                public String getWaterUserId() {
                    return waterUserId;
                }

                public void setWaterUserId(String waterUserId) {
                    this.waterUserId = waterUserId;
                }

                public String getWaterUserName() {
                    return waterUserName;
                }

                public void setWaterUserName(String waterUserName) {
                    this.waterUserName = waterUserName;
                }
            }
        }

        public static class DianBean {
            /**
             * code : 40013
             * message : 接口查出结果为空
             */

            private String code;
            private String message;

            public String getCode() {
                return code;
            }

            public void setCode(String code) {
                this.code = code;
            }

            public String getMessage() {
                return message;
            }

            public void setMessage(String message) {
                this.message = message;
            }
        }
    }

    public static class RequestBean {
        /**
         * parkId : 1
         * workshopId : 82
         */

        private String parkId;
        private String workshopId;

        public String getParkId() {
            return parkId;
        }

        public void setParkId(String parkId) {
            this.parkId = parkId;
        }

        public String getWorkshopId() {
            return workshopId;
        }

        public void setWorkshopId(String workshopId) {
            this.workshopId = workshopId;
        }
    }


//    /**
//     * code : 200
//     * msg :
//     * response : {"code":"200","message":"OK","data":{"workshopId":"82","workshopName":"驰骋制罐      F07#7-201","parkName":"温州平阳众创城","parcelName":"F-07","buildingName":"F07#7","roomName":"F07#7-201","enterName":"温州驰骋制罐有限公司"},"shui":{"code":"200","message":"OK","data":[{"waterUserId":"40","waterUserName":"F07D-7201"}]},"dian":{"code":"40013","message":"接口查出结果为空"}}
//     * request : {"parkId":"1","workshopId":"82"}
//     * other : []
//     */
//
//    private int code;
//    private String msg;
//    private ResponseBean response;
//    private RequestBean request;
//    private List<?> other;
//
//    public int getCode() {
//        return code;
//    }
//
//    public void setCode(int code) {
//        this.code = code;
//    }
//
//    public String getMsg() {
//        return msg;
//    }
//
//    public void setMsg(String msg) {
//        this.msg = msg;
//    }
//
//    public ResponseBean getResponse() {
//        return response;
//    }
//
//    public void setResponse(ResponseBean response) {
//        this.response = response;
//    }
//
//    public RequestBean getRequest() {
//        return request;
//    }
//
//    public void setRequest(RequestBean request) {
//        this.request = request;
//    }
//
//    public List<?> getOther() {
//        return other;
//    }
//
//    public void setOther(List<?> other) {
//        this.other = other;
//    }
//
//    public static class ResponseBean {
//        /**
//         * code : 200
//         * message : OK
//         * data : {"workshopId":"82","workshopName":"驰骋制罐      F07#7-201","parkName":"温州平阳众创城","parcelName":"F-07","buildingName":"F07#7","roomName":"F07#7-201","enterName":"温州驰骋制罐有限公司"}
//         * shui : {"code":"200","message":"OK","data":[{"waterUserId":"40","waterUserName":"F07D-7201"}]}
//         * dian : {"code":"40013","message":"接口查出结果为空"}
//         */
//
//        private String code;
//        private String message;
//        private DataBean data;
//        private ShuiBean shui;
//        private DianBean dian;
//
//        public String getCode() {
//            return code;
//        }
//
//        public void setCode(String code) {
//            this.code = code;
//        }
//
//        public String getMessage() {
//            return message;
//        }
//
//        public void setMessage(String message) {
//            this.message = message;
//        }
//
//        public DataBean getData() {
//            return data;
//        }
//
//        public void setData(DataBean data) {
//            this.data = data;
//        }
//
//        public ShuiBean getShui() {
//            return shui;
//        }
//
//        public void setShui(ShuiBean shui) {
//            this.shui = shui;
//        }
//
//        public DianBean getDian() {
//            return dian;
//        }
//
//        public void setDian(DianBean dian) {
//            this.dian = dian;
//        }
//
//        public static class DataBean {
//            /**
//             * workshopId : 82
//             * workshopName : 驰骋制罐      F07#7-201
//             * parkName : 温州平阳众创城
//             * parcelName : F-07
//             * buildingName : F07#7
//             * roomName : F07#7-201
//             * enterName : 温州驰骋制罐有限公司
//             */
//
//            private String workshopId;
//            private String workshopName;
//            private String parkName;
//            private String parcelName;
//            private String buildingName;
//            private String roomName;
//            private String enterName;
//
//            public String getWorkshopId() {
//                return workshopId;
//            }
//
//            public void setWorkshopId(String workshopId) {
//                this.workshopId = workshopId;
//            }
//
//            public String getWorkshopName() {
//                return workshopName;
//            }
//
//            public void setWorkshopName(String workshopName) {
//                this.workshopName = workshopName;
//            }
//
//            public String getParkName() {
//                return parkName;
//            }
//
//            public void setParkName(String parkName) {
//                this.parkName = parkName;
//            }
//
//            public String getParcelName() {
//                return parcelName;
//            }
//
//            public void setParcelName(String parcelName) {
//                this.parcelName = parcelName;
//            }
//
//            public String getBuildingName() {
//                return buildingName;
//            }
//
//            public void setBuildingName(String buildingName) {
//                this.buildingName = buildingName;
//            }
//
//            public String getRoomName() {
//                return roomName;
//            }
//
//            public void setRoomName(String roomName) {
//                this.roomName = roomName;
//            }
//
//            public String getEnterName() {
//                return enterName;
//            }
//
//            public void setEnterName(String enterName) {
//                this.enterName = enterName;
//            }
//        }
//
//        public static class ShuiBean {
//            /**
//             * code : 200
//             * message : OK
//             * data : [{"waterUserId":"40","waterUserName":"F07D-7201"}]
//             */
//
//            private String code;
//            private String message;
//            private List<DataBeanX> data;
//
//            public String getCode() {
//                return code;
//            }
//
//            public void setCode(String code) {
//                this.code = code;
//            }
//
//            public String getMessage() {
//                return message;
//            }
//
//            public void setMessage(String message) {
//                this.message = message;
//            }
//
//            public List<DataBeanX> getData() {
//                return data;
//            }
//
//            public void setData(List<DataBeanX> data) {
//                this.data = data;
//            }
//
//            public static class DataBeanX {
//                /**
//                 * waterUserId : 40
//                 * waterUserName : F07D-7201
//                 */
//
//                private String waterUserId;
//                private String waterUserName;
//
//                public String getWaterUserId() {
//                    return waterUserId;
//                }
//
//                public void setWaterUserId(String waterUserId) {
//                    this.waterUserId = waterUserId;
//                }
//
//                public String getWaterUserName() {
//                    return waterUserName;
//                }
//
//                public void setWaterUserName(String waterUserName) {
//                    this.waterUserName = waterUserName;
//                }
//            }
//        }
//
//        public static class DianBean {
//            /**
//             * code : 40013
//             * message : 接口查出结果为空
//             */
//
//            private String code;
//            private String message;
//
//            public String getCode() {
//                return code;
//            }
//
//            public void setCode(String code) {
//                this.code = code;
//            }
//
//            public String getMessage() {
//                return message;
//            }
//
//            public void setMessage(String message) {
//                this.message = message;
//            }
//        }
//    }
//
//    public static class RequestBean {
//        /**
//         * parkId : 1
//         * workshopId : 82
//         */
//
//        private String parkId;
//        private String workshopId;
//
//        public String getParkId() {
//            return parkId;
//        }
//
//        public void setParkId(String parkId) {
//            this.parkId = parkId;
//        }
//
//        public String getWorkshopId() {
//            return workshopId;
//        }
//
//        public void setWorkshopId(String workshopId) {
//            this.workshopId = workshopId;
//        }
//    }















//    /**
//     * code : 200
//     * msg :
//     * response : {"code":"200","message":"OK","data":{"workshopId":"29","workshopName":"优创机械      F07#2-102","parkName":"温州平阳众创城","parcelName":"F-07","buildingName":"F07#2","roomName":"F07#2-102","enterName":"温州优创机械有限公司"}}
//     * request : {"parkId":"1","workshopId":"29"}
//     * other : []
//     */
//
//    private int code;
//    private String msg;
//    private ResponseBean response;
//    private RequestBean request;
//    private List<?> other;
//
//    public int getCode() {
//        return code;
//    }
//
//    public void setCode(int code) {
//        this.code = code;
//    }
//
//    public String getMsg() {
//        return msg;
//    }
//
//    public void setMsg(String msg) {
//        this.msg = msg;
//    }
//
//    public ResponseBean getResponse() {
//        return response;
//    }
//
//    public void setResponse(ResponseBean response) {
//        this.response = response;
//    }
//
//    public RequestBean getRequest() {
//        return request;
//    }
//
//    public void setRequest(RequestBean request) {
//        this.request = request;
//    }
//
//    public List<?> getOther() {
//        return other;
//    }
//
//    public void setOther(List<?> other) {
//        this.other = other;
//    }
//
//    public static class ResponseBean {
//        /**
//         * code : 200
//         * message : OK
//         * data : {"workshopId":"29","workshopName":"优创机械      F07#2-102","parkName":"温州平阳众创城","parcelName":"F-07","buildingName":"F07#2","roomName":"F07#2-102","enterName":"温州优创机械有限公司"}
//         */
//
//        private String code;
//        private String message;
//        private DataBean data;
//
//        public String getCode() {
//            return code;
//        }
//
//        public void setCode(String code) {
//            this.code = code;
//        }
//
//        public String getMessage() {
//            return message;
//        }
//
//        public void setMessage(String message) {
//            this.message = message;
//        }
//
//        public DataBean getData() {
//            return data;
//        }
//
//        public void setData(DataBean data) {
//            this.data = data;
//        }
//
//        public static class DataBean {
//            /**
//             * workshopId : 29
//             * workshopName : 优创机械      F07#2-102
//             * parkName : 温州平阳众创城
//             * parcelName : F-07
//             * buildingName : F07#2
//             * roomName : F07#2-102
//             * enterName : 温州优创机械有限公司
//             */
//
//            private String workshopId;
//            private String workshopName;
//            private String parkName;
//            private String parcelName;
//            private String buildingName;
//            private String roomName;
//            private String enterName;
//
//            public String getWorkshopId() {
//                return workshopId;
//            }
//
//            public void setWorkshopId(String workshopId) {
//                this.workshopId = workshopId;
//            }
//
//            public String getWorkshopName() {
//                return workshopName;
//            }
//
//            public void setWorkshopName(String workshopName) {
//                this.workshopName = workshopName;
//            }
//
//            public String getParkName() {
//                return parkName;
//            }
//
//            public void setParkName(String parkName) {
//                this.parkName = parkName;
//            }
//
//            public String getParcelName() {
//                return parcelName;
//            }
//
//            public void setParcelName(String parcelName) {
//                this.parcelName = parcelName;
//            }
//
//            public String getBuildingName() {
//                return buildingName;
//            }
//
//            public void setBuildingName(String buildingName) {
//                this.buildingName = buildingName;
//            }
//
//            public String getRoomName() {
//                return roomName;
//            }
//
//            public void setRoomName(String roomName) {
//                this.roomName = roomName;
//            }
//
//            public String getEnterName() {
//                return enterName;
//            }
//
//            public void setEnterName(String enterName) {
//                this.enterName = enterName;
//            }
//        }
//    }
//
//    public static class RequestBean {
//        /**
//         * parkId : 1
//         * workshopId : 29
//         */
//
//        private String parkId;
//        private String workshopId;
//
//        public String getParkId() {
//            return parkId;
//        }
//
//        public void setParkId(String parkId) {
//            this.parkId = parkId;
//        }
//
//        public String getWorkshopId() {
//            return workshopId;
//        }
//
//        public void setWorkshopId(String workshopId) {
//            this.workshopId = workshopId;
//        }
//    }
}
