package com.wanyangproject.entity;

import java.util.List;

/**
 * Created by 甜甜 on 2018/8/24.
 */

public class ShangJiaXianShiJineEntity {

    /**
     * code : 200
     * msg :
     * response : {"money":"0","Capital":[{"id":"8","user_id":"1","time":"2018-08-23 18:08:20","money":"0.01","title":"用户确认收货","type":"3","pk_id":"1"},{"id":"7","user_id":"1","time":"2018-08-23 18:06:32","money":"0.01","title":"用户确认收货","type":"3","pk_id":"1"},{"id":"6","user_id":"1","time":"2018-08-23 18:06:25","money":"0.01","title":"用户确认收货","type":"3","pk_id":"1"}]}
     * request : {"atool_timestamp":"1535036313","parkId":"1"}
     * other : []
     */

    private int code;
    private String msg;
    private ResponseBean response;
    private RequestBean request;
    private List<?> other;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public ResponseBean getResponse() {
        return response;
    }

    public void setResponse(ResponseBean response) {
        this.response = response;
    }

    public RequestBean getRequest() {
        return request;
    }

    public void setRequest(RequestBean request) {
        this.request = request;
    }

    public List<?> getOther() {
        return other;
    }

    public void setOther(List<?> other) {
        this.other = other;
    }

    public static class ResponseBean {
        /**
         * money : 0
         * Capital : [{"id":"8","user_id":"1","time":"2018-08-23 18:08:20","money":"0.01","title":"用户确认收货","type":"3","pk_id":"1"},{"id":"7","user_id":"1","time":"2018-08-23 18:06:32","money":"0.01","title":"用户确认收货","type":"3","pk_id":"1"},{"id":"6","user_id":"1","time":"2018-08-23 18:06:25","money":"0.01","title":"用户确认收货","type":"3","pk_id":"1"}]
         */

        private String money;
        private List<CapitalBean> Capital;

        public String getMoney() {
            return money;
        }

        public void setMoney(String money) {
            this.money = money;
        }

        public List<CapitalBean> getCapital() {
            return Capital;
        }

        public void setCapital(List<CapitalBean> Capital) {
            this.Capital = Capital;
        }

        public static class CapitalBean {
            /**
             * id : 8
             * user_id : 1
             * time : 2018-08-23 18:08:20
             * money : 0.01
             * title : 用户确认收货
             * type : 3
             * pk_id : 1
             */

            private String id;
            private String user_id;
            private String time;
            private String money;
            private String title;
            private String type;
            private String pk_id;

            public String getId() {
                return id;
            }

            public void setId(String id) {
                this.id = id;
            }

            public String getUser_id() {
                return user_id;
            }

            public void setUser_id(String user_id) {
                this.user_id = user_id;
            }

            public String getTime() {
                return time;
            }

            public void setTime(String time) {
                this.time = time;
            }

            public String getMoney() {
                return money;
            }

            public void setMoney(String money) {
                this.money = money;
            }

            public String getTitle() {
                return title;
            }

            public void setTitle(String title) {
                this.title = title;
            }

            public String getType() {
                return type;
            }

            public void setType(String type) {
                this.type = type;
            }

            public String getPk_id() {
                return pk_id;
            }

            public void setPk_id(String pk_id) {
                this.pk_id = pk_id;
            }
        }
    }

    public static class RequestBean {
        /**
         * atool_timestamp : 1535036313
         * parkId : 1
         */

        private String atool_timestamp;
        private String parkId;

        public String getAtool_timestamp() {
            return atool_timestamp;
        }

        public void setAtool_timestamp(String atool_timestamp) {
            this.atool_timestamp = atool_timestamp;
        }

        public String getParkId() {
            return parkId;
        }

        public void setParkId(String parkId) {
            this.parkId = parkId;
        }
    }
}
