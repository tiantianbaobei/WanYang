package com.wanyangproject.entity;

/**
 * Created by 甜甜 on 2018/8/27.
 */

public class WeinXinTokenEntity {

    /**
     * access_token : 13_Py6xautvY24RdAS2LGOxluFSozeXBgHQz4k86lKncRYd1Aobu2mMIaehXcFohw4SlTaggn5p90qc9INZ0W8Au-z2jnMjZGWa1CWLlTZn0O0
     * expires_in : 7200
     * refresh_token : 13_5yYFHbf3f-FsrP3jWljqvL1CDnxdkdqi2TXwVqXVPV2wDbShY3z5k3XymppvJTnV_x_cM5MA1DfR3YfGboAKN6nj_Ts8FsNY5TrlsvmFP08
     * openid : of9-E0g7hKgksAz85N4A_XB3q2z8
     * scope : snsapi_userinfo
     * unionid : oY_xv1jvyQ9P7QGBd-qtjUZlwm9E
     */

    private String access_token;
    private int expires_in;
    private String refresh_token;
    private String openid;
    private String scope;
    private String unionid;

    public String getAccess_token() {
        return access_token;
    }

    public void setAccess_token(String access_token) {
        this.access_token = access_token;
    }

    public int getExpires_in() {
        return expires_in;
    }

    public void setExpires_in(int expires_in) {
        this.expires_in = expires_in;
    }

    public String getRefresh_token() {
        return refresh_token;
    }

    public void setRefresh_token(String refresh_token) {
        this.refresh_token = refresh_token;
    }

    public String getOpenid() {
        return openid;
    }

    public void setOpenid(String openid) {
        this.openid = openid;
    }

    public String getScope() {
        return scope;
    }

    public void setScope(String scope) {
        this.scope = scope;
    }

    public String getUnionid() {
        return unionid;
    }

    public void setUnionid(String unionid) {
        this.unionid = unionid;
    }
}
