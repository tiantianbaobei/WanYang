package com.wanyangproject.entity;

import java.util.List;

/**
 * Created by 甜甜 on 2018/8/16.
 */

public class YuanQuShouCeEntity {

    /**
     * code : 200
     * msg :
     * response : {"list":[{"id":"3","title":"生活指南","add_time":"1532701658"},{"id":"2","title":"入园须知","add_time":"1532701640"}],"bqArr":[{"id":"1","title":"员工","canClick":"1","isChecked":"1"},{"id":"2","title":"企业","canClick":"1","isChecked":"0"},{"id":"3","title":"商家","canClick":"1","isChecked":"0"},{"id":"4","title":"游客","canClick":"1","isChecked":"0"}]}
     * request : {"parkId":"1","bqId":"1"}
     * other : []
     */

    private int code;
    private String msg;
    private ResponseBean response;
    private RequestBean request;
    private List<?> other;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public ResponseBean getResponse() {
        return response;
    }

    public void setResponse(ResponseBean response) {
        this.response = response;
    }

    public RequestBean getRequest() {
        return request;
    }

    public void setRequest(RequestBean request) {
        this.request = request;
    }

    public List<?> getOther() {
        return other;
    }

    public void setOther(List<?> other) {
        this.other = other;
    }

    public static class ResponseBean {
        private List<ListBean> list;
        private List<BqArrBean> bqArr;

        public List<ListBean> getList() {
            return list;
        }

        public void setList(List<ListBean> list) {
            this.list = list;
        }

        public List<BqArrBean> getBqArr() {
            return bqArr;
        }

        public void setBqArr(List<BqArrBean> bqArr) {
            this.bqArr = bqArr;
        }

        public static class ListBean {
            /**
             * id : 3
             * title : 生活指南
             * add_time : 1532701658
             */

            private String id;
            private String title;
            private String add_time;

            public String getId() {
                return id;
            }

            public void setId(String id) {
                this.id = id;
            }

            public String getTitle() {
                return title;
            }

            public void setTitle(String title) {
                this.title = title;
            }

            public String getAdd_time() {
                return add_time;
            }

            public void setAdd_time(String add_time) {
                this.add_time = add_time;
            }
        }

        public static class BqArrBean {
            /**
             * id : 1
             * title : 员工
             * canClick : 1
             * isChecked : 1
             */

            private String id;
            private String title;
            private String canClick;
            private String isChecked;

            public String getId() {
                return id;
            }

            public void setId(String id) {
                this.id = id;
            }

            public String getTitle() {
                return title;
            }

            public void setTitle(String title) {
                this.title = title;
            }

            public String getCanClick() {
                return canClick;
            }

            public void setCanClick(String canClick) {
                this.canClick = canClick;
            }

            public String getIsChecked() {
                return isChecked;
            }

            public void setIsChecked(String isChecked) {
                this.isChecked = isChecked;
            }
        }
    }

    public static class RequestBean {
        /**
         * parkId : 1
         * bqId : 1
         */

        private String parkId;
        private String bqId;

        public String getParkId() {
            return parkId;
        }

        public void setParkId(String parkId) {
            this.parkId = parkId;
        }

        public String getBqId() {
            return bqId;
        }

        public void setBqId(String bqId) {
            this.bqId = bqId;
        }
    }
}
