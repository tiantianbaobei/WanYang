package com.wanyangproject.entity;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by 甜甜 on 2018/9/28.
 */

public class ShangJiaRuZhuXiuGaiEntity {

    /**
     * code : 200
     * msg :
     * response : {"id":"2","user_id":"2","pk_id":"1","name":"店铺i名称","addres":"店铺地址","Applicant_name":"申请人姓名","Applicant_phone":"申请人手机号","class":"2","introduce":"店铺详情","legal_name":"法人姓名","number_Positive":"/upload/20180825/7d102fa5387124d0a053a7be5af34741.jpg","number_side":"/upload/20180825/7d102fa5387124d0a053a7be5af34741.jpg","license":"/upload/20180825/7d102fa5387124d0a053a7be5af34741.jpg","add_time":"1535268164","type":"3","operation_time":"1537881138","Refusal":"546456"}
     * request : {"parkId":"1","id":"2"}
     * other : []
     */

    private int code;
    private String msg;
    private ResponseBean response;
    private RequestBean request;
    private List<?> other;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public ResponseBean getResponse() {
        return response;
    }

    public void setResponse(ResponseBean response) {
        this.response = response;
    }

    public RequestBean getRequest() {
        return request;
    }

    public void setRequest(RequestBean request) {
        this.request = request;
    }

    public List<?> getOther() {
        return other;
    }

    public void setOther(List<?> other) {
        this.other = other;
    }

    public static class ResponseBean {
        /**
         * id : 2
         * user_id : 2
         * pk_id : 1
         * name : 店铺i名称
         * addres : 店铺地址
         * Applicant_name : 申请人姓名
         * Applicant_phone : 申请人手机号
         * class : 2
         * introduce : 店铺详情
         * legal_name : 法人姓名
         * number_Positive : /upload/20180825/7d102fa5387124d0a053a7be5af34741.jpg
         * number_side : /upload/20180825/7d102fa5387124d0a053a7be5af34741.jpg
         * license : /upload/20180825/7d102fa5387124d0a053a7be5af34741.jpg
         * add_time : 1535268164
         * type : 3
         * operation_time : 1537881138
         * Refusal : 546456
         */

        private String id;
        private String user_id;
        private String pk_id;
        private String name;
        private String addres;
        private String Applicant_name;
        private String Applicant_phone;
        @SerializedName("class")
        private String classX;
        private String introduce;
        private String legal_name;
        private String number_Positive;
        private String number_side;
        private String license;
        private String add_time;
        private String type;
        private String operation_time;
        private String Refusal;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getUser_id() {
            return user_id;
        }

        public void setUser_id(String user_id) {
            this.user_id = user_id;
        }

        public String getPk_id() {
            return pk_id;
        }

        public void setPk_id(String pk_id) {
            this.pk_id = pk_id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getAddres() {
            return addres;
        }

        public void setAddres(String addres) {
            this.addres = addres;
        }

        public String getApplicant_name() {
            return Applicant_name;
        }

        public void setApplicant_name(String Applicant_name) {
            this.Applicant_name = Applicant_name;
        }

        public String getApplicant_phone() {
            return Applicant_phone;
        }

        public void setApplicant_phone(String Applicant_phone) {
            this.Applicant_phone = Applicant_phone;
        }

        public String getClassX() {
            return classX;
        }

        public void setClassX(String classX) {
            this.classX = classX;
        }

        public String getIntroduce() {
            return introduce;
        }

        public void setIntroduce(String introduce) {
            this.introduce = introduce;
        }

        public String getLegal_name() {
            return legal_name;
        }

        public void setLegal_name(String legal_name) {
            this.legal_name = legal_name;
        }

        public String getNumber_Positive() {
            return number_Positive;
        }

        public void setNumber_Positive(String number_Positive) {
            this.number_Positive = number_Positive;
        }

        public String getNumber_side() {
            return number_side;
        }

        public void setNumber_side(String number_side) {
            this.number_side = number_side;
        }

        public String getLicense() {
            return license;
        }

        public void setLicense(String license) {
            this.license = license;
        }

        public String getAdd_time() {
            return add_time;
        }

        public void setAdd_time(String add_time) {
            this.add_time = add_time;
        }

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public String getOperation_time() {
            return operation_time;
        }

        public void setOperation_time(String operation_time) {
            this.operation_time = operation_time;
        }

        public String getRefusal() {
            return Refusal;
        }

        public void setRefusal(String Refusal) {
            this.Refusal = Refusal;
        }
    }

    public static class RequestBean {
        /**
         * parkId : 1
         * id : 2
         */

        private String parkId;
        private String id;

        public String getParkId() {
            return parkId;
        }

        public void setParkId(String parkId) {
            this.parkId = parkId;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }
    }
}
