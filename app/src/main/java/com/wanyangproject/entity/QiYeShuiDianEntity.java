package com.wanyangproject.entity;

import java.util.List;

/**
 * Created by 甜甜 on 2018/11/22.
 */

public class QiYeShuiDianEntity {

    /**
     * code : 200
     * msg :
     * response : [{"month":"2018-02","cost":"0"},{"month":"2018-03","cost":"662"},{"month":"2018-04","cost":"45"},{"month":"2018-05","cost":"0"},{"month":"2018-06","cost":"0"},{"month":"2018-07","cost":"2780"},{"month":"2018-08","cost":"845"},{"month":"2018-09","cost":"935"},{"month":"2018-10","cost":"895"},{"month":"2018-11","cost":"595"}]
     * request : {"type":"1","parkId":"1"}
     * other : []
     */

    private int code;
    private String msg;
    private RequestBean request;
    private List<ResponseBean> response;
    private List<?> other;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public RequestBean getRequest() {
        return request;
    }

    public void setRequest(RequestBean request) {
        this.request = request;
    }

    public List<ResponseBean> getResponse() {
        return response;
    }

    public void setResponse(List<ResponseBean> response) {
        this.response = response;
    }

    public List<?> getOther() {
        return other;
    }

    public void setOther(List<?> other) {
        this.other = other;
    }

    public static class RequestBean {
        /**
         * type : 1
         * parkId : 1
         */

        private String type;
        private String parkId;

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public String getParkId() {
            return parkId;
        }

        public void setParkId(String parkId) {
            this.parkId = parkId;
        }
    }

    public static class ResponseBean {
        /**
         * month : 2018-02
         * cost : 0
         */

        private String month;
        private String cost;

        public String getMonth() {
            return month;
        }

        public void setMonth(String month) {
            this.month = month;
        }

        public String getCost() {
            return cost;
        }

        public void setCost(String cost) {
            this.cost = cost;
        }
    }
}
