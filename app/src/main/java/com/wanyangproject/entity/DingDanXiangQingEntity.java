package com.wanyangproject.entity;

import java.util.List;

/**
 * Created by 甜甜 on 2018/8/24.
 */

public class DingDanXiangQingEntity {

    /**
     * code : 200
     * msg :
     * response : {"order_sn":"pk2sn20180914153239","money":"13.0","type":"1","addres_name":"","addres_phone":"","addres_add":"","addres_type":"0","shop_id":"1","add_time":"2018-09-14 15:32:39","fahuo_time":"1970-01-01 08:00:00","shouhuo_time":"1970-01-01 08:00:00","pay_time":"1970-01-01 08:00:00","shop":{"cost":"0","nickname":"商家","photo":"park.hostop.net//upload/20180823/bab664273f521cea285761a6e79569f6.png"},"goods":[{"num":"1","pic":"13","goods_name":"测试3423","master":"park.hostop.net//upload/20180820/9ae4ff65f4004050c7a0c4ce7ca05073.jpg","id":"5"}]}
     * request : {"parkId":"1","order_sn":"pk2sn20180914153239"}
     * other : []
     */

    private int code;
    private String msg;
    private ResponseBean response;
    private RequestBean request;
    private List<?> other;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public ResponseBean getResponse() {
        return response;
    }

    public void setResponse(ResponseBean response) {
        this.response = response;
    }

    public RequestBean getRequest() {
        return request;
    }

    public void setRequest(RequestBean request) {
        this.request = request;
    }

    public List<?> getOther() {
        return other;
    }

    public void setOther(List<?> other) {
        this.other = other;
    }

    public static class ResponseBean {
        /**
         * order_sn : pk2sn20180914153239
         * money : 13.0
         * type : 1
         * addres_name :
         * addres_phone :
         * addres_add :
         * addres_type : 0
         * shop_id : 1
         * add_time : 2018-09-14 15:32:39
         * fahuo_time : 1970-01-01 08:00:00
         * shouhuo_time : 1970-01-01 08:00:00
         * pay_time : 1970-01-01 08:00:00
         * shop : {"cost":"0","nickname":"商家","photo":"park.hostop.net//upload/20180823/bab664273f521cea285761a6e79569f6.png"}
         * goods : [{"num":"1","pic":"13","goods_name":"测试3423","master":"park.hostop.net//upload/20180820/9ae4ff65f4004050c7a0c4ce7ca05073.jpg","id":"5"}]
         */

        private String order_sn;
        private String money;
        private String type;
        private String addres_name;
        private String addres_phone;
        private String addres_add;
        private String addres_type;
        private String shop_id;
        private String add_time;
        private String fahuo_time;
        private String shouhuo_time;
        private String pay_time;
        private ShopBean shop;
        private String addr_type;
        private List<GoodsBean> goods;
        private String Remarks;
        private String Reason_time;


        public String getReason_time() {
            return Reason_time;
        }

        public void setReason_time(String reason_time) {
            Reason_time = reason_time;
        }

        public String getRemarks() {
            return Remarks;
        }

        public void setRemarks(String remarks) {
            Remarks = remarks;
        }

        public String getAddr_type() {
            return addr_type;
        }

        public void setAddr_type(String addr_type) {
            this.addr_type = addr_type;
        }

        public String getOrder_sn() {
            return order_sn;
        }

        public void setOrder_sn(String order_sn) {
            this.order_sn = order_sn;
        }

        public String getMoney() {
            return money;
        }

        public void setMoney(String money) {
            this.money = money;
        }

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public String getAddres_name() {
            return addres_name;
        }

        public void setAddres_name(String addres_name) {
            this.addres_name = addres_name;
        }

        public String getAddres_phone() {
            return addres_phone;
        }

        public void setAddres_phone(String addres_phone) {
            this.addres_phone = addres_phone;
        }

        public String getAddres_add() {
            return addres_add;
        }

        public void setAddres_add(String addres_add) {
            this.addres_add = addres_add;
        }

        public String getAddres_type() {
            return addres_type;
        }

        public void setAddres_type(String addres_type) {
            this.addres_type = addres_type;
        }

        public String getShop_id() {
            return shop_id;
        }

        public void setShop_id(String shop_id) {
            this.shop_id = shop_id;
        }

        public String getAdd_time() {
            return add_time;
        }

        public void setAdd_time(String add_time) {
            this.add_time = add_time;
        }

        public String getFahuo_time() {
            return fahuo_time;
        }

        public void setFahuo_time(String fahuo_time) {
            this.fahuo_time = fahuo_time;
        }

        public String getShouhuo_time() {
            return shouhuo_time;
        }

        public void setShouhuo_time(String shouhuo_time) {
            this.shouhuo_time = shouhuo_time;
        }

        public String getPay_time() {
            return pay_time;
        }

        public void setPay_time(String pay_time) {
            this.pay_time = pay_time;
        }

        public ShopBean getShop() {
            return shop;
        }

        public void setShop(ShopBean shop) {
            this.shop = shop;
        }

        public List<GoodsBean> getGoods() {
            return goods;
        }

        public void setGoods(List<GoodsBean> goods) {
            this.goods = goods;
        }

        public static class ShopBean {
            /**
             * cost : 0
             * nickname : 商家
             * photo : park.hostop.net//upload/20180823/bab664273f521cea285761a6e79569f6.png
             */

            private String cost;
            private String nickname;
            private String photo;

            public String getCost() {
                return cost;
            }

            public void setCost(String cost) {
                this.cost = cost;
            }

            public String getNickname() {
                return nickname;
            }

            public void setNickname(String nickname) {
                this.nickname = nickname;
            }

            public String getPhoto() {
                return photo;
            }

            public void setPhoto(String photo) {
                this.photo = photo;
            }
        }

        public static class GoodsBean {
            /**
             * num : 1
             * pic : 13
             * goods_name : 测试3423
             * master : park.hostop.net//upload/20180820/9ae4ff65f4004050c7a0c4ce7ca05073.jpg
             * id : 5
             */

            private String num;
            private String pic;
            private String goods_name;
            private String master;
            private String id;

            public String getNum() {
                return num;
            }

            public void setNum(String num) {
                this.num = num;
            }

            public String getPic() {
                return pic;
            }

            public void setPic(String pic) {
                this.pic = pic;
            }

            public String getGoods_name() {
                return goods_name;
            }

            public void setGoods_name(String goods_name) {
                this.goods_name = goods_name;
            }

            public String getMaster() {
                return master;
            }

            public void setMaster(String master) {
                this.master = master;
            }

            public String getId() {
                return id;
            }

            public void setId(String id) {
                this.id = id;
            }
        }
    }

    public static class RequestBean {
        /**
         * parkId : 1
         * order_sn : pk2sn20180914153239
         */

        private String parkId;
        private String order_sn;

        public String getParkId() {
            return parkId;
        }

        public void setParkId(String parkId) {
            this.parkId = parkId;
        }

        public String getOrder_sn() {
            return order_sn;
        }

        public void setOrder_sn(String order_sn) {
            this.order_sn = order_sn;
        }
    }
}
