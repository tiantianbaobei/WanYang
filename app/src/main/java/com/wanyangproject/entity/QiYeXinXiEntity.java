package com.wanyangproject.entity;

import java.util.List;

/**
 * Created by 甜甜 on 2018/10/13.
 */

public class QiYeXinXiEntity {

    /**
     * code : 200
     * msg :
     * response : {"info":{"enterId":"","enterName":"","parkName":"","legalPerson":"","contact":"","logoUrl":"http://park.hostop.net/static/image/wanyang.png","intro":"","lat":"","lon":"","desc":"","address":"","guimo":"","hangye":"","lng":""},"recruit":[]}
     * request : {"parkId":"1"}
     * other : []
     */

    private int code;
    private String msg;
    private ResponseBean response;
    private RequestBean request;
    private List<?> other;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public ResponseBean getResponse() {
        return response;
    }

    public void setResponse(ResponseBean response) {
        this.response = response;
    }

    public RequestBean getRequest() {
        return request;
    }

    public void setRequest(RequestBean request) {
        this.request = request;
    }

    public List<?> getOther() {
        return other;
    }

    public void setOther(List<?> other) {
        this.other = other;
    }

    public static class ResponseBean {
        /**
         * info : {"enterId":"","enterName":"","parkName":"","legalPerson":"","contact":"","logoUrl":"http://park.hostop.net/static/image/wanyang.png","intro":"","lat":"","lon":"","desc":"","address":"","guimo":"","hangye":"","lng":""}
         * recruit : []
         */

        private InfoBean info;
        private List<?> recruit;

        public InfoBean getInfo() {
            return info;
        }

        public void setInfo(InfoBean info) {
            this.info = info;
        }

        public List<?> getRecruit() {
            return recruit;
        }

        public void setRecruit(List<?> recruit) {
            this.recruit = recruit;
        }

        public static class InfoBean {
            /**
             * enterId :
             * enterName :
             * parkName :
             * legalPerson :
             * contact :
             * logoUrl : http://park.hostop.net/static/image/wanyang.png
             * intro :
             * lat :
             * lon :
             * desc :
             * address :
             * guimo :
             * hangye :
             * lng :
             */

            private String enterId;
            private String enterName;
            private String parkName;
            private String legalPerson;
            private String contact;
            private String logoUrl;
            private String intro;
            private String lat;
            private String lon;
            private String desc;
            private String address;
            private String guimo;
            private String hangye;
            private String lng;

            public String getEnterId() {
                return enterId;
            }

            public void setEnterId(String enterId) {
                this.enterId = enterId;
            }

            public String getEnterName() {
                return enterName;
            }

            public void setEnterName(String enterName) {
                this.enterName = enterName;
            }

            public String getParkName() {
                return parkName;
            }

            public void setParkName(String parkName) {
                this.parkName = parkName;
            }

            public String getLegalPerson() {
                return legalPerson;
            }

            public void setLegalPerson(String legalPerson) {
                this.legalPerson = legalPerson;
            }

            public String getContact() {
                return contact;
            }

            public void setContact(String contact) {
                this.contact = contact;
            }

            public String getLogoUrl() {
                return logoUrl;
            }

            public void setLogoUrl(String logoUrl) {
                this.logoUrl = logoUrl;
            }

            public String getIntro() {
                return intro;
            }

            public void setIntro(String intro) {
                this.intro = intro;
            }

            public String getLat() {
                return lat;
            }

            public void setLat(String lat) {
                this.lat = lat;
            }

            public String getLon() {
                return lon;
            }

            public void setLon(String lon) {
                this.lon = lon;
            }

            public String getDesc() {
                return desc;
            }

            public void setDesc(String desc) {
                this.desc = desc;
            }

            public String getAddress() {
                return address;
            }

            public void setAddress(String address) {
                this.address = address;
            }

            public String getGuimo() {
                return guimo;
            }

            public void setGuimo(String guimo) {
                this.guimo = guimo;
            }

            public String getHangye() {
                return hangye;
            }

            public void setHangye(String hangye) {
                this.hangye = hangye;
            }

            public String getLng() {
                return lng;
            }

            public void setLng(String lng) {
                this.lng = lng;
            }
        }
    }

    public static class RequestBean {
        /**
         * parkId : 1
         */

        private String parkId;

        public String getParkId() {
            return parkId;
        }

        public void setParkId(String parkId) {
            this.parkId = parkId;
        }
    }
}
