package com.wanyangproject.entity;

import java.util.List;

/**
 * Created by 甜甜 on 2018/8/29.
 */

public class FuWuJiLuShenQingXiangQingEntity {


    /**
     * code : 200
     * msg :
     * response : {"id":"51","name":"18301437927","user_id":"2","pk_id":"1","add_time":"2018-09-13 10:33","type":"1","service":"7","Remarks":"测试","Refusal":"333","operation_time":"","photo":"","buchong_time":"","entype":"1","username":"17600904682","nickname":"未填写","servicename":"企业工商年报","content":"企业工商年报怎么报具体步骤","buc":[{"id":"9","entype":"1","Remarks":"我是183","guan_id":"51","Refusal":"333","photo":"","time":""}]}
     * request : {"id":"51","entype":"1"}
     * other : []
     */

    private int code;
    private String msg;
    private ResponseBean response;
    private RequestBean request;
    private List<?> other;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public ResponseBean getResponse() {
        return response;
    }

    public void setResponse(ResponseBean response) {
        this.response = response;
    }

    public RequestBean getRequest() {
        return request;
    }

    public void setRequest(RequestBean request) {
        this.request = request;
    }

    public List<?> getOther() {
        return other;
    }

    public void setOther(List<?> other) {
        this.other = other;
    }

    public static class ResponseBean {
        /**
         * id : 51
         * name : 18301437927
         * user_id : 2
         * pk_id : 1
         * add_time : 2018-09-13 10:33
         * type : 1
         * service : 7
         * Remarks : 测试
         * Refusal : 333
         * operation_time :
         * photo :
         * buchong_time :
         * entype : 1
         * username : 17600904682
         * nickname : 未填写
         * servicename : 企业工商年报
         * content : 企业工商年报怎么报具体步骤
         * applyPhone
         * buc : [{"id":"9","entype":"1","Remarks":"我是183","guan_id":"51","Refusal":"333","photo":"","time":""}]
         */

        private String id;
        private String name;
        private String user_id;
        private String pk_id;
        private String add_time;
        private String type;
        private String service;
        private String Remarks;
        private String Refusal;
        private String operation_time;
        private String photo;
        private String buchong_time;
        private String entype;
        private String username;
        private String nickname;
        private String servicename;
        private String content;
        private String sqname;
        private List<BucBean> buc;
        private String phone;
        private String applyPhone;


        public String getApplyPhone() {
            return applyPhone;
        }

        public void setApplyPhone(String applyPhone) {
            this.applyPhone = applyPhone;
        }

        public String getPhone() {
            return phone;
        }

        public void setPhone(String phone) {
            this.phone = phone;
        }

        public String getSqname() {
            return sqname;
        }

        public void setSqname(String sqname) {
            this.sqname = sqname;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getUser_id() {
            return user_id;
        }

        public void setUser_id(String user_id) {
            this.user_id = user_id;
        }

        public String getPk_id() {
            return pk_id;
        }

        public void setPk_id(String pk_id) {
            this.pk_id = pk_id;
        }

        public String getAdd_time() {
            return add_time;
        }

        public void setAdd_time(String add_time) {
            this.add_time = add_time;
        }

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public String getService() {
            return service;
        }

        public void setService(String service) {
            this.service = service;
        }

        public String getRemarks() {
            return Remarks;
        }

        public void setRemarks(String Remarks) {
            this.Remarks = Remarks;
        }

        public String getRefusal() {
            return Refusal;
        }

        public void setRefusal(String Refusal) {
            this.Refusal = Refusal;
        }

        public String getOperation_time() {
            return operation_time;
        }

        public void setOperation_time(String operation_time) {
            this.operation_time = operation_time;
        }

        public String getPhoto() {
            return photo;
        }

        public void setPhoto(String photo) {
            this.photo = photo;
        }

        public String getBuchong_time() {
            return buchong_time;
        }

        public void setBuchong_time(String buchong_time) {
            this.buchong_time = buchong_time;
        }

        public String getEntype() {
            return entype;
        }

        public void setEntype(String entype) {
            this.entype = entype;
        }

        public String getUsername() {
            return username;
        }

        public void setUsername(String username) {
            this.username = username;
        }

        public String getNickname() {
            return nickname;
        }

        public void setNickname(String nickname) {
            this.nickname = nickname;
        }

        public String getServicename() {
            return servicename;
        }

        public void setServicename(String servicename) {
            this.servicename = servicename;
        }

        public String getContent() {
            return content;
        }

        public void setContent(String content) {
            this.content = content;
        }

        public List<BucBean> getBuc() {
            return buc;
        }

        public void setBuc(List<BucBean> buc) {
            this.buc = buc;
        }

        public static class BucBean {
            /**
             * id : 9
             * entype : 1
             * Remarks : 我是183
             * guan_id : 51
             * Refusal : 333
             * photo :
             * time :
             */

            private String id;
            private String entype;
            private String Remarks;
            private String guan_id;
            private String Refusal;
            private String photo;
            private String time;

            public String getId() {
                return id;
            }

            public void setId(String id) {
                this.id = id;
            }

            public String getEntype() {
                return entype;
            }

            public void setEntype(String entype) {
                this.entype = entype;
            }

            public String getRemarks() {
                return Remarks;
            }

            public void setRemarks(String Remarks) {
                this.Remarks = Remarks;
            }

            public String getGuan_id() {
                return guan_id;
            }

            public void setGuan_id(String guan_id) {
                this.guan_id = guan_id;
            }

            public String getRefusal() {
                return Refusal;
            }

            public void setRefusal(String Refusal) {
                this.Refusal = Refusal;
            }

            public String getPhoto() {
                return photo;
            }

            public void setPhoto(String photo) {
                this.photo = photo;
            }

            public String getTime() {
                return time;
            }

            public void setTime(String time) {
                this.time = time;
            }
        }
    }

    public static class RequestBean {
        /**
         * id : 51
         * entype : 1
         */

        private String id;
        private String entype;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getEntype() {
            return entype;
        }

        public void setEntype(String entype) {
            this.entype = entype;
        }
    }
}
