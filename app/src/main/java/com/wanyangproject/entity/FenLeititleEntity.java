package com.wanyangproject.entity;

import java.util.List;

/**
 * Created by 甜甜 on 2018/9/6.
 */

public class FenLeititleEntity {
    /**
     * code : 200
     * msg :
     * response : {"info":{"id":"23","title":"招商咨询","content":"园区商铺招商对象为国内品牌生产厂家、国内有较强的经营实力，较大经营规模的商业机构、有丰富营销经验的区域级品牌代理商、有一定实力的新品牌厂家及个人。统一招租，商家自主经营，以低投入高回报的投资保障模式，拓展市场空间，做财富第一手主人。","address":"滨海新区海泽路与阳屿路交叉口","phone":"0577-8039666","background":"http://park.hostop.net/static/image/background.png","photo":"http://park.hostop.net/upload/20180927/476b8027b6031cb0d78e8ba594d8e03a.jpg","add_time":"2018-09-12 11:29:14","longitude":"120.68742","latitude":"27.668614"},"list":[{"id":"18","title":"小微园大智慧 全省围观温州小微园建设\u201c四大法宝\u201d","logo":"","imgArr":[],"desc":"8月29日，全省小微企业园建设提升暨\u201c低散乱\u201d整治推进大会在温州举行，深入推进小微企业园建设提升和\u201c低小散\u201d整治工作，推...","add_time":"2018-09-22 14:09"},{"id":"17","title":"智能峰会正式开启，各界嘉宾云集","logo":"","imgArr":[],"desc":"据了解，本次智博会总共签约项目34个，总投资额约261亿元人民币，项目主要涉及智能制造、工业互联网、机器人、集成电路、智...","add_time":"2018-09-22 13:09"},{"id":"15","title":"全球智能经济峰会暨第八届智博会在宁波开幕 慈溪万洋众创城智慧园区百亿项目签约落地","logo":"","imgArr":[],"desc":"9月7日全球智能经济峰会暨第八届智博会开幕。从全球智能经济峰会暨第八届智博会上获悉，本次智博会总共签约项目34个，总投资...","add_time":"2018-09-21 08:43"}]}
     * request : {"typeId":"23"}
     * other : []
     */

    private int code;
    private String msg;
    private ResponseBean response;
    private RequestBean request;
    private List<?> other;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public ResponseBean getResponse() {
        return response;
    }

    public void setResponse(ResponseBean response) {
        this.response = response;
    }

    public RequestBean getRequest() {
        return request;
    }

    public void setRequest(RequestBean request) {
        this.request = request;
    }

    public List<?> getOther() {
        return other;
    }

    public void setOther(List<?> other) {
        this.other = other;
    }

    public static class ResponseBean {
        /**
         * info : {"id":"23","title":"招商咨询","content":"园区商铺招商对象为国内品牌生产厂家、国内有较强的经营实力，较大经营规模的商业机构、有丰富营销经验的区域级品牌代理商、有一定实力的新品牌厂家及个人。统一招租，商家自主经营，以低投入高回报的投资保障模式，拓展市场空间，做财富第一手主人。","address":"滨海新区海泽路与阳屿路交叉口","phone":"0577-8039666","background":"http://park.hostop.net/static/image/background.png","photo":"http://park.hostop.net/upload/20180927/476b8027b6031cb0d78e8ba594d8e03a.jpg","add_time":"2018-09-12 11:29:14","longitude":"120.68742","latitude":"27.668614"}
         * list : [{"id":"18","title":"小微园大智慧 全省围观温州小微园建设\u201c四大法宝\u201d","logo":"","imgArr":[],"desc":"8月29日，全省小微企业园建设提升暨\u201c低散乱\u201d整治推进大会在温州举行，深入推进小微企业园建设提升和\u201c低小散\u201d整治工作，推...","add_time":"2018-09-22 14:09"},{"id":"17","title":"智能峰会正式开启，各界嘉宾云集","logo":"","imgArr":[],"desc":"据了解，本次智博会总共签约项目34个，总投资额约261亿元人民币，项目主要涉及智能制造、工业互联网、机器人、集成电路、智...","add_time":"2018-09-22 13:09"},{"id":"15","title":"全球智能经济峰会暨第八届智博会在宁波开幕 慈溪万洋众创城智慧园区百亿项目签约落地","logo":"","imgArr":[],"desc":"9月7日全球智能经济峰会暨第八届智博会开幕。从全球智能经济峰会暨第八届智博会上获悉，本次智博会总共签约项目34个，总投资...","add_time":"2018-09-21 08:43"}]
         */

        private InfoBean info;
        private List<ListBean> list;

        public InfoBean getInfo() {
            return info;
        }

        public void setInfo(InfoBean info) {
            this.info = info;
        }

        public List<ListBean> getList() {
            return list;
        }

        public void setList(List<ListBean> list) {
            this.list = list;
        }

        public static class InfoBean {
            /**
             * id : 23
             * title : 招商咨询
             * content : 园区商铺招商对象为国内品牌生产厂家、国内有较强的经营实力，较大经营规模的商业机构、有丰富营销经验的区域级品牌代理商、有一定实力的新品牌厂家及个人。统一招租，商家自主经营，以低投入高回报的投资保障模式，拓展市场空间，做财富第一手主人。
             * address : 滨海新区海泽路与阳屿路交叉口
             * phone : 0577-8039666
             * background : http://park.hostop.net/static/image/background.png
             * photo : http://park.hostop.net/upload/20180927/476b8027b6031cb0d78e8ba594d8e03a.jpg
             * add_time : 2018-09-12 11:29:14
             * longitude : 120.68742
             * latitude : 27.668614
             */

            private String id;
            private String title;
            private String content;
            private String address;
            private String phone;
            private String background;
            private String photo;
            private String add_time;
            private String longitude;
            private String latitude;

            public String getId() {
                return id;
            }

            public void setId(String id) {
                this.id = id;
            }

            public String getTitle() {
                return title;
            }

            public void setTitle(String title) {
                this.title = title;
            }

            public String getContent() {
                return content;
            }

            public void setContent(String content) {
                this.content = content;
            }

            public String getAddress() {
                return address;
            }

            public void setAddress(String address) {
                this.address = address;
            }

            public String getPhone() {
                return phone;
            }

            public void setPhone(String phone) {
                this.phone = phone;
            }

            public String getBackground() {
                return background;
            }

            public void setBackground(String background) {
                this.background = background;
            }

            public String getPhoto() {
                return photo;
            }

            public void setPhoto(String photo) {
                this.photo = photo;
            }

            public String getAdd_time() {
                return add_time;
            }

            public void setAdd_time(String add_time) {
                this.add_time = add_time;
            }

            public String getLongitude() {
                return longitude;
            }

            public void setLongitude(String longitude) {
                this.longitude = longitude;
            }

            public String getLatitude() {
                return latitude;
            }

            public void setLatitude(String latitude) {
                this.latitude = latitude;
            }
        }

        public static class ListBean {
            /**
             * id : 18
             * title : 小微园大智慧 全省围观温州小微园建设“四大法宝”
             * logo :
             * imgArr : []
             * desc : 8月29日，全省小微企业园建设提升暨“低散乱”整治推进大会在温州举行，深入推进小微企业园建设提升和“低小散”整治工作，推...
             * add_time : 2018-09-22 14:09
             */

            private String id;
            private String title;
            private String logo;
            private String desc;
            private String add_time;
            private List<?> imgArr;

            public String getId() {
                return id;
            }

            public void setId(String id) {
                this.id = id;
            }

            public String getTitle() {
                return title;
            }

            public void setTitle(String title) {
                this.title = title;
            }

            public String getLogo() {
                return logo;
            }

            public void setLogo(String logo) {
                this.logo = logo;
            }

            public String getDesc() {
                return desc;
            }

            public void setDesc(String desc) {
                this.desc = desc;
            }

            public String getAdd_time() {
                return add_time;
            }

            public void setAdd_time(String add_time) {
                this.add_time = add_time;
            }

            public List<?> getImgArr() {
                return imgArr;
            }

            public void setImgArr(List<?> imgArr) {
                this.imgArr = imgArr;
            }
        }
    }

    public static class RequestBean {
        /**
         * typeId : 23
         */

        private String typeId;

        public String getTypeId() {
            return typeId;
        }

        public void setTypeId(String typeId) {
            this.typeId = typeId;
        }
    }


//    /**
//     * code : 200
//     * msg :
//     * response : {"info":{"id":"3","title":"招商最新资讯","content":"56151651651651","address":"","phone":"13176688711","background":"http://park.hostop.net/upload/20180902/90526b548849435cb3213dab45f1ca2d.jpg","add_time":"15天前"},"list":[{"id":"10","title":"啥地方","logo":"http://park.hostop.net/upload/20180910/c6c365b55cbc867932efeb4ac6d25ffc.jpg","imgArr":["http://park.hostop.net/upload/20180910/c6c365b55cbc867932efeb4ac6d25ffc.jpg"],"desc":"详情介绍爱仕达囧妃何赛飞啊发世界爱上...","add_time":"7天前"},{"id":"8","title":"biaotibiaoti","logo":"http://park.hostop.net/upload/20180905/df3a0b76eda3204aba048dd1295b5413.jpg","imgArr":["http://park.hostop.net/upload/20180905/df3a0b76eda3204aba048dd1295b5413.jpg"],"desc":"wanyang=园区生活...","add_time":"12天前"}]}
//     * request : {"typeId":"3"}
//     * other : []
//     */
//
//    private int code;
//    private String msg;
//    private ResponseBean response;
//    private RequestBean request;
//    private List<?> other;
//
//    public int getCode() {
//        return code;
//    }
//
//    public void setCode(int code) {
//        this.code = code;
//    }
//
//    public String getMsg() {
//        return msg;
//    }
//
//    public void setMsg(String msg) {
//        this.msg = msg;
//    }
//
//    public ResponseBean getResponse() {
//        return response;
//    }
//
//    public void setResponse(ResponseBean response) {
//        this.response = response;
//    }
//
//    public RequestBean getRequest() {
//        return request;
//    }
//
//    public void setRequest(RequestBean request) {
//        this.request = request;
//    }
//
//    public List<?> getOther() {
//        return other;
//    }
//
//    public void setOther(List<?> other) {
//        this.other = other;
//    }
//
//    public static class ResponseBean {
//        /**
//         * info : {"id":"3","title":"招商最新资讯","content":"56151651651651","address":"","phone":"13176688711","background":"http://park.hostop.net/upload/20180902/90526b548849435cb3213dab45f1ca2d.jpg","add_time":"15天前"}
//         * list : [{"id":"10","title":"啥地方","logo":"http://park.hostop.net/upload/20180910/c6c365b55cbc867932efeb4ac6d25ffc.jpg","imgArr":["http://park.hostop.net/upload/20180910/c6c365b55cbc867932efeb4ac6d25ffc.jpg"],"desc":"详情介绍爱仕达囧妃何赛飞啊发世界爱上...","add_time":"7天前"},{"id":"8","title":"biaotibiaoti","logo":"http://park.hostop.net/upload/20180905/df3a0b76eda3204aba048dd1295b5413.jpg","imgArr":["http://park.hostop.net/upload/20180905/df3a0b76eda3204aba048dd1295b5413.jpg"],"desc":"wanyang=园区生活...","add_time":"12天前"}]
//         */
//
//        private InfoBean info;
//        private List<ListBean> list;
//
//        public InfoBean getInfo() {
//            return info;
//        }
//
//        public void setInfo(InfoBean info) {
//            this.info = info;
//        }
//
//        public List<ListBean> getList() {
//            return list;
//        }
//
//        public void setList(List<ListBean> list) {
//            this.list = list;
//        }
//
//        public static class InfoBean {
//            /**
//             * id : 3
//             * title : 招商最新资讯
//             * content : 56151651651651
//             * address :
//             * phone : 13176688711
//             * background : http://park.hostop.net/upload/20180902/90526b548849435cb3213dab45f1ca2d.jpg
//             * add_time : 15天前
//             */
//
//            private String id;
//            private String title;
//            private String content;
//            private String address;
//            private String phone;
//            private String background;
//            private String add_time;
//            private String longitude;
//            private String latitude;
//
//
//            public String getLongitude() {
//                return longitude;
//            }
//
//            public void setLongitude(String longitude) {
//                this.longitude = longitude;
//            }
//
//            public String getLatitude() {
//                return latitude;
//            }
//
//            public void setLatitude(String latitude) {
//                this.latitude = latitude;
//            }
//
//            public String getId() {
//                return id;
//            }
//
//            public void setId(String id) {
//                this.id = id;
//            }
//
//            public String getTitle() {
//                return title;
//            }
//
//            public void setTitle(String title) {
//                this.title = title;
//            }
//
//            public String getContent() {
//                return content;
//            }
//
//            public void setContent(String content) {
//                this.content = content;
//            }
//
//            public String getAddress() {
//                return address;
//            }
//
//            public void setAddress(String address) {
//                this.address = address;
//            }
//
//            public String getPhone() {
//                return phone;
//            }
//
//            public void setPhone(String phone) {
//                this.phone = phone;
//            }
//
//            public String getBackground() {
//                return background;
//            }
//
//            public void setBackground(String background) {
//                this.background = background;
//            }
//
//            public String getAdd_time() {
//                return add_time;
//            }
//
//            public void setAdd_time(String add_time) {
//                this.add_time = add_time;
//            }
//        }
//
//        public static class ListBean {
//            /**
//             * id : 10
//             * title : 啥地方
//             * logo : http://park.hostop.net/upload/20180910/c6c365b55cbc867932efeb4ac6d25ffc.jpg
//             * imgArr : ["http://park.hostop.net/upload/20180910/c6c365b55cbc867932efeb4ac6d25ffc.jpg"]
//             * desc : 详情介绍爱仕达囧妃何赛飞啊发世界爱上...
//             * add_time : 7天前
//             */
//
//            private String id;
//            private String title;
//            private String logo;
//            private String desc;
//            private String add_time;
//            private List<String> imgArr;
//
//            public String getId() {
//                return id;
//            }
//
//            public void setId(String id) {
//                this.id = id;
//            }
//
//            public String getTitle() {
//                return title;
//            }
//
//            public void setTitle(String title) {
//                this.title = title;
//            }
//
//            public String getLogo() {
//                return logo;
//            }
//
//            public void setLogo(String logo) {
//                this.logo = logo;
//            }
//
//            public String getDesc() {
//                return desc;
//            }
//
//            public void setDesc(String desc) {
//                this.desc = desc;
//            }
//
//            public String getAdd_time() {
//                return add_time;
//            }
//
//            public void setAdd_time(String add_time) {
//                this.add_time = add_time;
//            }
//
//            public List<String> getImgArr() {
//                return imgArr;
//            }
//
//            public void setImgArr(List<String> imgArr) {
//                this.imgArr = imgArr;
//            }
//        }
//    }
//
//    public static class RequestBean {
//        /**
//         * typeId : 3
//         */
//
//        private String typeId;
//
//        public String getTypeId() {
//            return typeId;
//        }
//
//        public void setTypeId(String typeId) {
//            this.typeId = typeId;
//        }
//    }



















//
//    /**
//     * code : 200
//     * msg :
//     * response : {"info":{"id":"3","title":"招商最新资讯","content":"56151651651651","address":"","phone":"13176688711","add_time":"8天前"},"list":[{"id":"10","title":"啥地方","logo":"http://park.hostop.net/upload/20180910/c6c365b55cbc867932efeb4ac6d25ffc.jpg","imgArr":["http://park.hostop.net/upload/20180910/c6c365b55cbc867932efeb4ac6d25ffc.jpg"],"desc":"详情介绍爱仕达囧妃何赛飞啊发世界爱上...","add_time":"4小时前"},{"id":"8","title":"biaotibiaoti","logo":"http://park.hostop.net/upload/20180905/df3a0b76eda3204aba048dd1295b5413.jpg","imgArr":["http://park.hostop.net/upload/20180905/df3a0b76eda3204aba048dd1295b5413.jpg"],"desc":"wanyang=园区生活...","add_time":"5天前"},{"id":"1","title":"P2P--医疗法规的解决办法","logo":"http://park.hostop.net/upload/20180902/43ee99efff59bac59c3ea00fb1d2494d.jpg","imgArr":["http://park.hostop.net/upload/20180902/43ee99efff59bac59c3ea00fb1d2494d.jpg"],"desc":"为加强抗菌药物管理，遏制细菌耐药，维护人民群众健康，促进经济社会协调发展，制定本行动计划。&nbsp;　　一、前言&nb...","add_time":"8天前"}]}
//     * request : {"typeId":"3"}
//     * other : []
//     */
//
//    private int code;
//    private String msg;
//    private ResponseBean response;
//    private RequestBean request;
//    private List<?> other;
//
//    public int getCode() {
//        return code;
//    }
//
//    public void setCode(int code) {
//        this.code = code;
//    }
//
//    public String getMsg() {
//        return msg;
//    }
//
//    public void setMsg(String msg) {
//        this.msg = msg;
//    }
//
//    public ResponseBean getResponse() {
//        return response;
//    }
//
//    public void setResponse(ResponseBean response) {
//        this.response = response;
//    }
//
//    public RequestBean getRequest() {
//        return request;
//    }
//
//    public void setRequest(RequestBean request) {
//        this.request = request;
//    }
//
//    public List<?> getOther() {
//        return other;
//    }
//
//    public void setOther(List<?> other) {
//        this.other = other;
//    }
//
//    public static class ResponseBean {
//        /**
//         * info : {"id":"3","title":"招商最新资讯","content":"56151651651651","address":"","phone":"13176688711","add_time":"8天前"}
//         * list : [{"id":"10","title":"啥地方","logo":"http://park.hostop.net/upload/20180910/c6c365b55cbc867932efeb4ac6d25ffc.jpg","imgArr":["http://park.hostop.net/upload/20180910/c6c365b55cbc867932efeb4ac6d25ffc.jpg"],"desc":"详情介绍爱仕达囧妃何赛飞啊发世界爱上...","add_time":"4小时前"},{"id":"8","title":"biaotibiaoti","logo":"http://park.hostop.net/upload/20180905/df3a0b76eda3204aba048dd1295b5413.jpg","imgArr":["http://park.hostop.net/upload/20180905/df3a0b76eda3204aba048dd1295b5413.jpg"],"desc":"wanyang=园区生活...","add_time":"5天前"},{"id":"1","title":"P2P--医疗法规的解决办法","logo":"http://park.hostop.net/upload/20180902/43ee99efff59bac59c3ea00fb1d2494d.jpg","imgArr":["http://park.hostop.net/upload/20180902/43ee99efff59bac59c3ea00fb1d2494d.jpg"],"desc":"为加强抗菌药物管理，遏制细菌耐药，维护人民群众健康，促进经济社会协调发展，制定本行动计划。&nbsp;　　一、前言&nb...","add_time":"8天前"}]
//         */
//
//        private InfoBean info;
//        private List<ListBean> list;
//
//        public InfoBean getInfo() {
//            return info;
//        }
//
//        public void setInfo(InfoBean info) {
//            this.info = info;
//        }
//
//        public List<ListBean> getList() {
//            return list;
//        }
//
//        public void setList(List<ListBean> list) {
//            this.list = list;
//        }
//
//        public static class InfoBean {
//            /**
//             * id : 3
//             * title : 招商最新资讯
//             * content : 56151651651651
//             * address :
//             * phone : 13176688711
//             * add_time : 8天前
//             */
//
//            private String id;
//            private String title;
//            private String content;
//            private String address;
//            private String phone;
//            private String add_time;
//
//            public String getId() {
//                return id;
//            }
//
//            public void setId(String id) {
//                this.id = id;
//            }
//
//            public String getTitle() {
//                return title;
//            }
//
//            public void setTitle(String title) {
//                this.title = title;
//            }
//
//            public String getContent() {
//                return content;
//            }
//
//            public void setContent(String content) {
//                this.content = content;
//            }
//
//            public String getAddress() {
//                return address;
//            }
//
//            public void setAddress(String address) {
//                this.address = address;
//            }
//
//            public String getPhone() {
//                return phone;
//            }
//
//            public void setPhone(String phone) {
//                this.phone = phone;
//            }
//
//            public String getAdd_time() {
//                return add_time;
//            }
//
//            public void setAdd_time(String add_time) {
//                this.add_time = add_time;
//            }
//        }
//
//        public static class ListBean {
//            /**
//             * id : 10
//             * title : 啥地方
//             * logo : http://park.hostop.net/upload/20180910/c6c365b55cbc867932efeb4ac6d25ffc.jpg
//             * imgArr : ["http://park.hostop.net/upload/20180910/c6c365b55cbc867932efeb4ac6d25ffc.jpg"]
//             * desc : 详情介绍爱仕达囧妃何赛飞啊发世界爱上...
//             * add_time : 4小时前
//             */
//
//            private String id;
//            private String title;
//            private String logo;
//            private String desc;
//            private String add_time;
//            private List<String> imgArr;
//
//            public String getId() {
//                return id;
//            }
//
//            public void setId(String id) {
//                this.id = id;
//            }
//
//            public String getTitle() {
//                return title;
//            }
//
//            public void setTitle(String title) {
//                this.title = title;
//            }
//
//            public String getLogo() {
//                return logo;
//            }
//
//            public void setLogo(String logo) {
//                this.logo = logo;
//            }
//
//            public String getDesc() {
//                return desc;
//            }
//
//            public void setDesc(String desc) {
//                this.desc = desc;
//            }
//
//            public String getAdd_time() {
//                return add_time;
//            }
//
//            public void setAdd_time(String add_time) {
//                this.add_time = add_time;
//            }
//
//            public List<String> getImgArr() {
//                return imgArr;
//            }
//
//            public void setImgArr(List<String> imgArr) {
//                this.imgArr = imgArr;
//            }
//        }
//    }
//
//    public static class RequestBean {
//        /**
//         * typeId : 3
//         */
//
//        private String typeId;
//
//        public String getTypeId() {
//            return typeId;
//        }
//
//        public void setTypeId(String typeId) {
//            this.typeId = typeId;
//        }
//    }
}
