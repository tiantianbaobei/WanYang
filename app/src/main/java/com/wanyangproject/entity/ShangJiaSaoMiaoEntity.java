package com.wanyangproject.entity;

import java.util.List;

/**
 * Created by 甜甜 on 2018/8/23.
 */

public class ShangJiaSaoMiaoEntity {
    /**
     * code : 200
     * msg :
     * response : {"id":"359","shop_id":"4","user_id":"","use_time":"2018-09-02 - 2018-10-20","coupons_id":"38","money":"69","photo":"upload/qrcode/2018-09-15/1d949777b8c25208acfc5d1b2c9b8c6000.png","type":"1","special":"10d4236bfc0d6e1b4ce16906d879a6b73","pk_id":"1","permanent":"2","users_time":"1536981366","add_time":"1536972947","name":"测试测试"}
     * request : {"parkId":"1","code":"10d4236bfc0d6e1b4ce16906d879a6b73","phone":"17600904682"}
     * other : []
     */

    private int code;
    private String msg;
    private ResponseBean response;
    private RequestBean request;
    private List<?> other;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public ResponseBean getResponse() {
        return response;
    }

    public void setResponse(ResponseBean response) {
        this.response = response;
    }

    public RequestBean getRequest() {
        return request;
    }

    public void setRequest(RequestBean request) {
        this.request = request;
    }

    public List<?> getOther() {
        return other;
    }

    public void setOther(List<?> other) {
        this.other = other;
    }

    public static class ResponseBean {
        /**
         * id : 359
         * shop_id : 4
         * user_id :
         * use_time : 2018-09-02 - 2018-10-20
         * coupons_id : 38
         * money : 69
         * photo : upload/qrcode/2018-09-15/1d949777b8c25208acfc5d1b2c9b8c6000.png
         * type : 1
         * special : 10d4236bfc0d6e1b4ce16906d879a6b73
         * pk_id : 1
         * permanent : 2
         * users_time : 1536981366
         * add_time : 1536972947
         * name : 测试测试
         */

        private String id;
        private String shop_id;
        private String user_id;
        private String use_time;
        private String coupons_id;
        private String money;
        private String photo;
        private String type;
        private String special;
        private String pk_id;
        private String permanent;
        private String users_time;
        private String add_time;
        private String name;





        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getShop_id() {
            return shop_id;
        }

        public void setShop_id(String shop_id) {
            this.shop_id = shop_id;
        }

        public String getUser_id() {
            return user_id;
        }

        public void setUser_id(String user_id) {
            this.user_id = user_id;
        }

        public String getUse_time() {
            return use_time;
        }

        public void setUse_time(String use_time) {
            this.use_time = use_time;
        }

        public String getCoupons_id() {
            return coupons_id;
        }

        public void setCoupons_id(String coupons_id) {
            this.coupons_id = coupons_id;
        }

        public String getMoney() {
            return money;
        }

        public void setMoney(String money) {
            this.money = money;
        }

        public String getPhoto() {
            return photo;
        }

        public void setPhoto(String photo) {
            this.photo = photo;
        }

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public String getSpecial() {
            return special;
        }

        public void setSpecial(String special) {
            this.special = special;
        }

        public String getPk_id() {
            return pk_id;
        }

        public void setPk_id(String pk_id) {
            this.pk_id = pk_id;
        }

        public String getPermanent() {
            return permanent;
        }

        public void setPermanent(String permanent) {
            this.permanent = permanent;
        }

        public String getUsers_time() {
            return users_time;
        }

        public void setUsers_time(String users_time) {
            this.users_time = users_time;
        }

        public String getAdd_time() {
            return add_time;
        }

        public void setAdd_time(String add_time) {
            this.add_time = add_time;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }
    }

    public static class RequestBean {
        /**
         * parkId : 1
         * code : 10d4236bfc0d6e1b4ce16906d879a6b73
         * phone : 17600904682
         */

        private String parkId;
        private String code;
        private String phone;

        public String getParkId() {
            return parkId;
        }

        public void setParkId(String parkId) {
            this.parkId = parkId;
        }

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        public String getPhone() {
            return phone;
        }

        public void setPhone(String phone) {
            this.phone = phone;
        }
    }





//    /**
//     * code : 400
//     * msg : 这不是您商家的二维码！
//     * response : []
//     * request : {"parkId":"1","code":"123456","phone":"15822075533"}
//     * other : []
//     */
//
//    private int code;
//    private String msg;
//    private RequestBean request;
//    private List<?> response;
//    private List<?> other;
//
//    public int getCode() {
//        return code;
//    }
//
//    public void setCode(int code) {
//        this.code = code;
//    }
//
//    public String getMsg() {
//        return msg;
//    }
//
//    public void setMsg(String msg) {
//        this.msg = msg;
//    }
//
//    public RequestBean getRequest() {
//        return request;
//    }
//
//    public void setRequest(RequestBean request) {
//        this.request = request;
//    }
//
//    public List<?> getResponse() {
//        return response;
//    }
//
//    public void setResponse(List<?> response) {
//        this.response = response;
//    }
//
//    public List<?> getOther() {
//        return other;
//    }
//
//    public void setOther(List<?> other) {
//        this.other = other;
//    }
//
//    public static class RequestBean {
//        /**
//         * parkId : 1
//         * code : 123456
//         * phone : 15822075533
//         */
//
//        private String parkId;
//        private String code;
//        private String phone;
//
//        public String getParkId() {
//            return parkId;
//        }
//
//        public void setParkId(String parkId) {
//            this.parkId = parkId;
//        }
//
//        public String getCode() {
//            return code;
//        }
//
//        public void setCode(String code) {
//            this.code = code;
//        }
//
//        public String getPhone() {
//            return phone;
//        }
//
//        public void setPhone(String phone) {
//            this.phone = phone;
//        }
//    }
}
