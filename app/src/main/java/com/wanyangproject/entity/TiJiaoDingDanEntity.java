package com.wanyangproject.entity;

import java.util.List;

/**
 * Created by 甜甜 on 2018/8/8.
 */

public class TiJiaoDingDanEntity {

    /**
     * code : 200
     * msg :
     * response : {"goods":[{"num":"3","id":"4","goods_name":"测试","pic":"1","master":"park.hostop.net//upload/20180804/2da84761de43d358bfd2f89627fdaf42.png"},{"num":"2","id":"2","goods_name":"商品2","pic":"10","master":"park.hostop.net//upload/20180804/963d0fc2a816b184851bb41116478e85.png"}],"shop":{"id":"1","nickname":"彪1","photo":"park.hostop.net/static/image/0.jpg"}}
     * request : {"parkId":"1","shopId":"1","order_sn":"pk2sn20180807150818"}
     * other : []
     */

    private int code;
    private String msg;
    private ResponseBean response;
    private RequestBean request;
    private List<?> other;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public ResponseBean getResponse() {
        return response;
    }

    public void setResponse(ResponseBean response) {
        this.response = response;
    }

    public RequestBean getRequest() {
        return request;
    }

    public void setRequest(RequestBean request) {
        this.request = request;
    }

    public List<?> getOther() {
        return other;
    }

    public void setOther(List<?> other) {
        this.other = other;
    }

    public static class ResponseBean {
        /**
         * goods : [{"num":"3","id":"4","goods_name":"测试","pic":"1","master":"park.hostop.net//upload/20180804/2da84761de43d358bfd2f89627fdaf42.png"},{"num":"2","id":"2","goods_name":"商品2","pic":"10","master":"park.hostop.net//upload/20180804/963d0fc2a816b184851bb41116478e85.png"}]
         * shop : {"id":"1","nickname":"彪1","photo":"park.hostop.net/static/image/0.jpg"}
         */

        private ShopBean shop;
        private List<GoodsBean> goods;

        public ShopBean getShop() {
            return shop;
        }

        public void setShop(ShopBean shop) {
            this.shop = shop;
        }

        public List<GoodsBean> getGoods() {
            return goods;
        }

        public void setGoods(List<GoodsBean> goods) {
            this.goods = goods;
        }

        public static class ShopBean {
            /**
             * id : 1
             * nickname : 彪1
             * photo : park.hostop.net/static/image/0.jpg
             */

            private String id;
            private String nickname;
            private String photo;

            public String getId() {
                return id;
            }

            public void setId(String id) {
                this.id = id;
            }

            public String getNickname() {
                return nickname;
            }

            public void setNickname(String nickname) {
                this.nickname = nickname;
            }

            public String getPhoto() {
                return photo;
            }

            public void setPhoto(String photo) {
                this.photo = photo;
            }
        }

        public static class GoodsBean {
            /**
             * num : 3
             * id : 4
             * goods_name : 测试
             * pic : 1
             * master : park.hostop.net//upload/20180804/2da84761de43d358bfd2f89627fdaf42.png
             */

            private String num;
            private String id;
            private String goods_name;
            private String pic;
            private String master;

            public String getNum() {
                return num;
            }

            public void setNum(String num) {
                this.num = num;
            }

            public String getId() {
                return id;
            }

            public void setId(String id) {
                this.id = id;
            }

            public String getGoods_name() {
                return goods_name;
            }

            public void setGoods_name(String goods_name) {
                this.goods_name = goods_name;
            }

            public String getPic() {
                return pic;
            }

            public void setPic(String pic) {
                this.pic = pic;
            }

            public String getMaster() {
                return master;
            }

            public void setMaster(String master) {
                this.master = master;
            }
        }
    }

    public static class RequestBean {
        /**
         * parkId : 1
         * shopId : 1
         * order_sn : pk2sn20180807150818
         */

        private String parkId;
        private String shopId;
        private String order_sn;

        public String getParkId() {
            return parkId;
        }

        public void setParkId(String parkId) {
            this.parkId = parkId;
        }

        public String getShopId() {
            return shopId;
        }

        public void setShopId(String shopId) {
            this.shopId = shopId;
        }

        public String getOrder_sn() {
            return order_sn;
        }

        public void setOrder_sn(String order_sn) {
            this.order_sn = order_sn;
        }
    }
}
