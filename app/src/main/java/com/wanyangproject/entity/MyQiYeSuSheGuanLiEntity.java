package com.wanyangproject.entity;

import java.util.List;

/**
 * Created by 甜甜 on 2018/9/6.
 */

public class MyQiYeSuSheGuanLiEntity {


    /**
     * code : 200
     * msg :
     * response : {"code":"200","message":"OK","data":[{"dormitoryId":"1","dormitoryName":"301寝室","parcelName":"F-07","parkName":"温州平阳众创城","dikuai":"F07"},{"dormitoryId":"2","dormitoryName":"302寝室","parcelName":"F-07","parkName":"温州平阳众创城","dikuai":"F07"},{"dormitoryId":"3","dormitoryName":"303寝室","parcelName":"F-07","parkName":"温州平阳众创城","dikuai":"F07"},{"dormitoryId":"4","dormitoryName":"304寝室","parcelName":"F-07","parkName":"温州平阳众创城","dikuai":"F07"},{"dormitoryId":"5","dormitoryName":"305寝室","parcelName":"F-07","parkName":"温州平阳众创城","dikuai":"F07"},{"dormitoryId":"6","dormitoryName":"306寝室","parcelName":"F-07","parkName":"温州平阳众创城","dikuai":"F07"},{"dormitoryId":"248","dormitoryName":"1001寝室","parcelName":"F-07","parkName":"温州平阳众创城","dikuai":"F07"},{"dormitoryId":"249","dormitoryName":"1002寝室","parcelName":"F-07","parkName":"温州平阳众创城","dikuai":"F07"},{"dormitoryId":"250","dormitoryName":"1003寝室","parcelName":"F-07","parkName":"温州平阳众创城","dikuai":"F07"},{"dormitoryId":"251","dormitoryName":"1004寝室","parcelName":"F-07","parkName":"温州平阳众创城","dikuai":"F07"},{"dormitoryId":"252","dormitoryName":"1005寝室","parcelName":"F-07","parkName":"温州平阳众创城","dikuai":"F07"},{"dormitoryId":"253","dormitoryName":"1006寝室","parcelName":"F-07","parkName":"温州平阳众创城","dikuai":"F07"},{"dormitoryId":"289","dormitoryName":"946寝室","parcelName":"F-07","parkName":"温州平阳众创城","dikuai":"F07"}]}
     * request : {"parkId":"1","dormitoryName":""}
     * other : []
     */

    private int code;
    private String msg;
    private ResponseBean response;
    private RequestBean request;
    private List<?> other;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public ResponseBean getResponse() {
        return response;
    }

    public void setResponse(ResponseBean response) {
        this.response = response;
    }

    public RequestBean getRequest() {
        return request;
    }

    public void setRequest(RequestBean request) {
        this.request = request;
    }

    public List<?> getOther() {
        return other;
    }

    public void setOther(List<?> other) {
        this.other = other;
    }

    public static class ResponseBean {
        /**
         * code : 200
         * message : OK
         * data : [{"dormitoryId":"1","dormitoryName":"301寝室","parcelName":"F-07","parkName":"温州平阳众创城","dikuai":"F07"},{"dormitoryId":"2","dormitoryName":"302寝室","parcelName":"F-07","parkName":"温州平阳众创城","dikuai":"F07"},{"dormitoryId":"3","dormitoryName":"303寝室","parcelName":"F-07","parkName":"温州平阳众创城","dikuai":"F07"},{"dormitoryId":"4","dormitoryName":"304寝室","parcelName":"F-07","parkName":"温州平阳众创城","dikuai":"F07"},{"dormitoryId":"5","dormitoryName":"305寝室","parcelName":"F-07","parkName":"温州平阳众创城","dikuai":"F07"},{"dormitoryId":"6","dormitoryName":"306寝室","parcelName":"F-07","parkName":"温州平阳众创城","dikuai":"F07"},{"dormitoryId":"248","dormitoryName":"1001寝室","parcelName":"F-07","parkName":"温州平阳众创城","dikuai":"F07"},{"dormitoryId":"249","dormitoryName":"1002寝室","parcelName":"F-07","parkName":"温州平阳众创城","dikuai":"F07"},{"dormitoryId":"250","dormitoryName":"1003寝室","parcelName":"F-07","parkName":"温州平阳众创城","dikuai":"F07"},{"dormitoryId":"251","dormitoryName":"1004寝室","parcelName":"F-07","parkName":"温州平阳众创城","dikuai":"F07"},{"dormitoryId":"252","dormitoryName":"1005寝室","parcelName":"F-07","parkName":"温州平阳众创城","dikuai":"F07"},{"dormitoryId":"253","dormitoryName":"1006寝室","parcelName":"F-07","parkName":"温州平阳众创城","dikuai":"F07"},{"dormitoryId":"289","dormitoryName":"946寝室","parcelName":"F-07","parkName":"温州平阳众创城","dikuai":"F07"}]
         */

        private String code;
        private String message;
        private List<DataBean> data;

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        public List<DataBean> getData() {
            return data;
        }

        public void setData(List<DataBean> data) {
            this.data = data;
        }

        public static class DataBean {
            /**
             * dormitoryId : 1
             * dormitoryName : 301寝室
             * parcelName : F-07
             * parkName : 温州平阳众创城
             * dikuai : F07
             */

            private String dormitoryId;
            private String dormitoryName;
            private String parcelName;
            private String parkName;
            private String dikuai;

            public String getDormitoryId() {
                return dormitoryId;
            }

            public void setDormitoryId(String dormitoryId) {
                this.dormitoryId = dormitoryId;
            }

            public String getDormitoryName() {
                return dormitoryName;
            }

            public void setDormitoryName(String dormitoryName) {
                this.dormitoryName = dormitoryName;
            }

            public String getParcelName() {
                return parcelName;
            }

            public void setParcelName(String parcelName) {
                this.parcelName = parcelName;
            }

            public String getParkName() {
                return parkName;
            }

            public void setParkName(String parkName) {
                this.parkName = parkName;
            }

            public String getDikuai() {
                return dikuai;
            }

            public void setDikuai(String dikuai) {
                this.dikuai = dikuai;
            }
        }
    }

    public static class RequestBean {
        /**
         * parkId : 1
         * dormitoryName :
         */

        private String parkId;
        private String dormitoryName;

        public String getParkId() {
            return parkId;
        }

        public void setParkId(String parkId) {
            this.parkId = parkId;
        }

        public String getDormitoryName() {
            return dormitoryName;
        }

        public void setDormitoryName(String dormitoryName) {
            this.dormitoryName = dormitoryName;
        }
    }
}
