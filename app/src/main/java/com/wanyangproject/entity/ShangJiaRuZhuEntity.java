package com.wanyangproject.entity;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by 甜甜 on 2018/8/26.
 */

public class ShangJiaRuZhuEntity {

    /**
     * code : 400
     * msg : 您已申请过！
     * response : []
     * request : {"parkId":"1","Applicant_name":"换个卡","Applicant_phone":"17600904682","name":"号不经常","class":"超市","addres":"胡库","introduce":"不路过","legal_name":"费用","number_Positive":"http://park.hostop.net/upload/20180826/5d57409bb9605ba8bf2c180bb65d4d85.jpg","number_side":"http://park.hostop.net/upload/20180826/be30c4ad7b6d85a65fb23832f897d868.jpg","license":"http://park.hostop.net/upload/20180826/43d838b6e0f5bb23b7295fa82922d692.jpg","authId":"54","code":"771378"}
     * other : []
     */

    private int code;
    private String msg;
    private RequestBean request;
    private List<?> response;
    private List<?> other;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public RequestBean getRequest() {
        return request;
    }

    public void setRequest(RequestBean request) {
        this.request = request;
    }

    public List<?> getResponse() {
        return response;
    }

    public void setResponse(List<?> response) {
        this.response = response;
    }

    public List<?> getOther() {
        return other;
    }

    public void setOther(List<?> other) {
        this.other = other;
    }

    public static class RequestBean {
        /**
         * parkId : 1
         * Applicant_name : 换个卡
         * Applicant_phone : 17600904682
         * name : 号不经常
         * class : 超市
         * addres : 胡库
         * introduce : 不路过
         * legal_name : 费用
         * number_Positive : http://park.hostop.net/upload/20180826/5d57409bb9605ba8bf2c180bb65d4d85.jpg
         * number_side : http://park.hostop.net/upload/20180826/be30c4ad7b6d85a65fb23832f897d868.jpg
         * license : http://park.hostop.net/upload/20180826/43d838b6e0f5bb23b7295fa82922d692.jpg
         * authId : 54
         * code : 771378
         */

        private String parkId;
        private String Applicant_name;
        private String Applicant_phone;
        private String name;
        @SerializedName("class")
        private String classX;
        private String addres;
        private String introduce;
        private String legal_name;
        private String number_Positive;
        private String number_side;
        private String license;
        private String authId;
        private String code;

        public String getParkId() {
            return parkId;
        }

        public void setParkId(String parkId) {
            this.parkId = parkId;
        }

        public String getApplicant_name() {
            return Applicant_name;
        }

        public void setApplicant_name(String Applicant_name) {
            this.Applicant_name = Applicant_name;
        }

        public String getApplicant_phone() {
            return Applicant_phone;
        }

        public void setApplicant_phone(String Applicant_phone) {
            this.Applicant_phone = Applicant_phone;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getClassX() {
            return classX;
        }

        public void setClassX(String classX) {
            this.classX = classX;
        }

        public String getAddres() {
            return addres;
        }

        public void setAddres(String addres) {
            this.addres = addres;
        }

        public String getIntroduce() {
            return introduce;
        }

        public void setIntroduce(String introduce) {
            this.introduce = introduce;
        }

        public String getLegal_name() {
            return legal_name;
        }

        public void setLegal_name(String legal_name) {
            this.legal_name = legal_name;
        }

        public String getNumber_Positive() {
            return number_Positive;
        }

        public void setNumber_Positive(String number_Positive) {
            this.number_Positive = number_Positive;
        }

        public String getNumber_side() {
            return number_side;
        }

        public void setNumber_side(String number_side) {
            this.number_side = number_side;
        }

        public String getLicense() {
            return license;
        }

        public void setLicense(String license) {
            this.license = license;
        }

        public String getAuthId() {
            return authId;
        }

        public void setAuthId(String authId) {
            this.authId = authId;
        }

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }
    }
}
