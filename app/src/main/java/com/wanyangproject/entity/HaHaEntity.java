package com.wanyangproject.entity;

import java.util.List;

/**
 * Created by 甜甜 on 2018/9/10.
 */

public class HaHaEntity {

    /**
     * code : 200
     * msg :
     * response : {"upTjArr":[{"id":"11","title":"","image":"http://park.hostop.net/upload/20180906/1f5b2c9ae44040f2c49a083fd32a517f.jpg","add_time":"4天前","hrefType":"0","content":""},{"id":"10","title":"","image":"http://park.hostop.net/upload/20180906/1f5b2c9ae44040f2c49a083fd32a517f.jpg","add_time":"4天前","hrefType":"6","content":""},{"id":"9","title":"","image":"http://park.hostop.net/upload/20180906/1f5b2c9ae44040f2c49a083fd32a517f.jpg","add_time":"4天前","hrefType":"0","content":""},{"id":"1","title":"","image":"http://park.hostop.net/upload/20180908/766c7d1ee6700e30614a32c4296c5cbb.jpg","add_time":"4天前","hrefType":"0","content":""}],"bannerArr":[{"id":"15","title":"","image":"http://park.hostop.net/upload/20180910/647754cf1a336cc1aacdef142f0d852a.jpg","add_time":"34分钟前","hrefType":"9","content":""},{"id":"12","title":"","image":"http://park.hostop.net/upload/20180910/b8e0185c9eae88a100e728c249f1410b.jpg","add_time":"3小时前","hrefType":"0","content":""},{"id":"11","title":"","image":"http://park.hostop.net/upload/20180910/99fb1e69020d08ce27355864885df48e.jpg","add_time":"3小时前","hrefType":"0","content":""}],"wordArr":[{"id":"10","title":"啥地方","logo":"http://park.hostop.net/upload/20180910/c6c365b55cbc867932efeb4ac6d25ffc.jpg","imgArr":["http://park.hostop.net/upload/20180910/c6c365b55cbc867932efeb4ac6d25ffc.jpg"],"desc":"详情介绍爱仕达囧妃何赛飞啊发世界爱上...","add_time":"4小时前"},{"id":"8","title":"biaotibiaoti","logo":"http://park.hostop.net/upload/20180905/df3a0b76eda3204aba048dd1295b5413.jpg","imgArr":["http://park.hostop.net/upload/20180905/df3a0b76eda3204aba048dd1295b5413.jpg"],"desc":"wanyang=园区生活...","add_time":"5天前"},{"id":"1","title":"P2P--医疗法规的解决办法","logo":"http://park.hostop.net/upload/20180902/43ee99efff59bac59c3ea00fb1d2494d.jpg","imgArr":["http://park.hostop.net/upload/20180902/43ee99efff59bac59c3ea00fb1d2494d.jpg"],"desc":"为加强抗菌药物管理，遏制细菌耐药，维护人民群众健康，促进经济社会协调发展，制定本行动计划。&nbsp;　　一、前言&nb...","add_time":"8天前"}],"category":[{"id":"18","title":"社会救助暂行办法","logo":"http://park.hostop.net/upload/20180910/235301500e050f394c70fb590abde155.jpg","add_time":"3分钟前"},{"id":"17","title":"测试","logo":"http://park.hostop.net/upload/20180910/6ece71a6d08b5c2a2422a4c89be3bfc9.jpg","add_time":"3小时前"},{"id":"16","title":"济南网点","logo":"http://park.hostop.net/upload/20180910/070291e48ee39343dea1f2809b0389c4.jpg","add_time":"3小时前"},{"id":"15","title":"河北网点","logo":"http://park.hostop.net","add_time":"4小时前"}],"info":{"content":""},"tjIndex":[]}
     * request : {"parkId":"1","typeId":"3"}
     * other : []
     */

    private int code;
    private String msg;
    private ResponseBean response;
    private RequestBean request;
    private List<?> other;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public ResponseBean getResponse() {
        return response;
    }

    public void setResponse(ResponseBean response) {
        this.response = response;
    }

    public RequestBean getRequest() {
        return request;
    }

    public void setRequest(RequestBean request) {
        this.request = request;
    }

    public List<?> getOther() {
        return other;
    }

    public void setOther(List<?> other) {
        this.other = other;
    }

    public static class ResponseBean {
        /**
         * upTjArr : [{"id":"11","title":"","image":"http://park.hostop.net/upload/20180906/1f5b2c9ae44040f2c49a083fd32a517f.jpg","add_time":"4天前","hrefType":"0","content":""},{"id":"10","title":"","image":"http://park.hostop.net/upload/20180906/1f5b2c9ae44040f2c49a083fd32a517f.jpg","add_time":"4天前","hrefType":"6","content":""},{"id":"9","title":"","image":"http://park.hostop.net/upload/20180906/1f5b2c9ae44040f2c49a083fd32a517f.jpg","add_time":"4天前","hrefType":"0","content":""},{"id":"1","title":"","image":"http://park.hostop.net/upload/20180908/766c7d1ee6700e30614a32c4296c5cbb.jpg","add_time":"4天前","hrefType":"0","content":""}]
         * bannerArr : [{"id":"15","title":"","image":"http://park.hostop.net/upload/20180910/647754cf1a336cc1aacdef142f0d852a.jpg","add_time":"34分钟前","hrefType":"9","content":""},{"id":"12","title":"","image":"http://park.hostop.net/upload/20180910/b8e0185c9eae88a100e728c249f1410b.jpg","add_time":"3小时前","hrefType":"0","content":""},{"id":"11","title":"","image":"http://park.hostop.net/upload/20180910/99fb1e69020d08ce27355864885df48e.jpg","add_time":"3小时前","hrefType":"0","content":""}]
         * wordArr : [{"id":"10","title":"啥地方","logo":"http://park.hostop.net/upload/20180910/c6c365b55cbc867932efeb4ac6d25ffc.jpg","imgArr":["http://park.hostop.net/upload/20180910/c6c365b55cbc867932efeb4ac6d25ffc.jpg"],"desc":"详情介绍爱仕达囧妃何赛飞啊发世界爱上...","add_time":"4小时前"},{"id":"8","title":"biaotibiaoti","logo":"http://park.hostop.net/upload/20180905/df3a0b76eda3204aba048dd1295b5413.jpg","imgArr":["http://park.hostop.net/upload/20180905/df3a0b76eda3204aba048dd1295b5413.jpg"],"desc":"wanyang=园区生活...","add_time":"5天前"},{"id":"1","title":"P2P--医疗法规的解决办法","logo":"http://park.hostop.net/upload/20180902/43ee99efff59bac59c3ea00fb1d2494d.jpg","imgArr":["http://park.hostop.net/upload/20180902/43ee99efff59bac59c3ea00fb1d2494d.jpg"],"desc":"为加强抗菌药物管理，遏制细菌耐药，维护人民群众健康，促进经济社会协调发展，制定本行动计划。&nbsp;　　一、前言&nb...","add_time":"8天前"}]
         * category : [{"id":"18","title":"社会救助暂行办法","logo":"http://park.hostop.net/upload/20180910/235301500e050f394c70fb590abde155.jpg","add_time":"3分钟前"},{"id":"17","title":"测试","logo":"http://park.hostop.net/upload/20180910/6ece71a6d08b5c2a2422a4c89be3bfc9.jpg","add_time":"3小时前"},{"id":"16","title":"济南网点","logo":"http://park.hostop.net/upload/20180910/070291e48ee39343dea1f2809b0389c4.jpg","add_time":"3小时前"},{"id":"15","title":"河北网点","logo":"http://park.hostop.net","add_time":"4小时前"}]
         * info : {"content":""}
         * tjIndex : []
         */

        private InfoBean info;
        private List<UpTjArrBean> upTjArr;
        private List<BannerArrBean> bannerArr;
        private List<WordArrBean> wordArr;
        private List<CategoryBean> category;
        private List<?> tjIndex;

        public InfoBean getInfo() {
            return info;
        }

        public void setInfo(InfoBean info) {
            this.info = info;
        }

        public List<UpTjArrBean> getUpTjArr() {
            return upTjArr;
        }

        public void setUpTjArr(List<UpTjArrBean> upTjArr) {
            this.upTjArr = upTjArr;
        }

        public List<BannerArrBean> getBannerArr() {
            return bannerArr;
        }

        public void setBannerArr(List<BannerArrBean> bannerArr) {
            this.bannerArr = bannerArr;
        }

        public List<WordArrBean> getWordArr() {
            return wordArr;
        }

        public void setWordArr(List<WordArrBean> wordArr) {
            this.wordArr = wordArr;
        }

        public List<CategoryBean> getCategory() {
            return category;
        }

        public void setCategory(List<CategoryBean> category) {
            this.category = category;
        }

        public List<?> getTjIndex() {
            return tjIndex;
        }

        public void setTjIndex(List<?> tjIndex) {
            this.tjIndex = tjIndex;
        }

        public static class InfoBean {
            /**
             * content :
             */

            private String content;

            public String getContent() {
                return content;
            }

            public void setContent(String content) {
                this.content = content;
            }
        }

        public static class UpTjArrBean {
            /**
             * id : 11
             * title :
             * image : http://park.hostop.net/upload/20180906/1f5b2c9ae44040f2c49a083fd32a517f.jpg
             * add_time : 4天前
             * hrefType : 0
             * content :
             */

            private String id;
            private String title;
            private String image;
            private String add_time;
            private String hrefType;
            private String content;

            public String getId() {
                return id;
            }

            public void setId(String id) {
                this.id = id;
            }

            public String getTitle() {
                return title;
            }

            public void setTitle(String title) {
                this.title = title;
            }

            public String getImage() {
                return image;
            }

            public void setImage(String image) {
                this.image = image;
            }

            public String getAdd_time() {
                return add_time;
            }

            public void setAdd_time(String add_time) {
                this.add_time = add_time;
            }

            public String getHrefType() {
                return hrefType;
            }

            public void setHrefType(String hrefType) {
                this.hrefType = hrefType;
            }

            public String getContent() {
                return content;
            }

            public void setContent(String content) {
                this.content = content;
            }
        }

        public static class BannerArrBean {
            /**
             * id : 15
             * title :
             * image : http://park.hostop.net/upload/20180910/647754cf1a336cc1aacdef142f0d852a.jpg
             * add_time : 34分钟前
             * hrefType : 9
             * content :
             */

            private String id;
            private String title;
            private String image;
            private String add_time;
            private String hrefType;
            private String content;

            public String getId() {
                return id;
            }

            public void setId(String id) {
                this.id = id;
            }

            public String getTitle() {
                return title;
            }

            public void setTitle(String title) {
                this.title = title;
            }

            public String getImage() {
                return image;
            }

            public void setImage(String image) {
                this.image = image;
            }

            public String getAdd_time() {
                return add_time;
            }

            public void setAdd_time(String add_time) {
                this.add_time = add_time;
            }

            public String getHrefType() {
                return hrefType;
            }

            public void setHrefType(String hrefType) {
                this.hrefType = hrefType;
            }

            public String getContent() {
                return content;
            }

            public void setContent(String content) {
                this.content = content;
            }
        }

        public static class WordArrBean {
            /**
             * id : 10
             * title : 啥地方
             * logo : http://park.hostop.net/upload/20180910/c6c365b55cbc867932efeb4ac6d25ffc.jpg
             * imgArr : ["http://park.hostop.net/upload/20180910/c6c365b55cbc867932efeb4ac6d25ffc.jpg"]
             * desc : 详情介绍爱仕达囧妃何赛飞啊发世界爱上...
             * add_time : 4小时前
             */

            private String id;
            private String title;
            private String logo;
            private String desc;
            private String add_time;
            private List<String> imgArr;

            public String getId() {
                return id;
            }

            public void setId(String id) {
                this.id = id;
            }

            public String getTitle() {
                return title;
            }

            public void setTitle(String title) {
                this.title = title;
            }

            public String getLogo() {
                return logo;
            }

            public void setLogo(String logo) {
                this.logo = logo;
            }

            public String getDesc() {
                return desc;
            }

            public void setDesc(String desc) {
                this.desc = desc;
            }

            public String getAdd_time() {
                return add_time;
            }

            public void setAdd_time(String add_time) {
                this.add_time = add_time;
            }

            public List<String> getImgArr() {
                return imgArr;
            }

            public void setImgArr(List<String> imgArr) {
                this.imgArr = imgArr;
            }
        }

        public static class CategoryBean {
            /**
             * id : 18
             * title : 社会救助暂行办法
             * logo : http://park.hostop.net/upload/20180910/235301500e050f394c70fb590abde155.jpg
             * add_time : 3分钟前
             */

            private String id;
            private String title;
            private String logo;
            private String add_time;

            public String getId() {
                return id;
            }

            public void setId(String id) {
                this.id = id;
            }

            public String getTitle() {
                return title;
            }

            public void setTitle(String title) {
                this.title = title;
            }

            public String getLogo() {
                return logo;
            }

            public void setLogo(String logo) {
                this.logo = logo;
            }

            public String getAdd_time() {
                return add_time;
            }

            public void setAdd_time(String add_time) {
                this.add_time = add_time;
            }
        }
    }

    public static class RequestBean {
        /**
         * parkId : 1
         * typeId : 3
         */

        private String parkId;
        private String typeId;

        public String getParkId() {
            return parkId;
        }

        public void setParkId(String parkId) {
            this.parkId = parkId;
        }

        public String getTypeId() {
            return typeId;
        }

        public void setTypeId(String typeId) {
            this.typeId = typeId;
        }
    }
}
