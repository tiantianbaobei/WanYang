package com.wanyangproject.entity;

import java.util.List;

/**
 * Created by 甜甜 on 2018/8/6.
 */

public class LoginEntity {


    /**
     * code : 200
     * msg :
     * response : {"userid":"2","username":"17600904682","realname":"","avatar":"http://park.hostop.net/upload/20180915/6755fdb2ff9a3f6cdb253300aecc5f69.jpg","nickname":"呵呵呵","uuid":"8AE5A3FB-9B39-4E56-85F1-A5117A2DF693","parkId":"1","parkInfo":{"parkId":"1","parkName":"温州平阳众创城"},"status":"1","typeId":"0","birth":"2147483647","typeId2":"3","token":"83fe557752783abc976e70e7fa1486e2","toBind":"0"}
     * request : {"phone":"17600904682","password":"123456"}
     * other : []
     */

    private int code;
    private String msg;
    private ResponseBean response;
    private RequestBean request;
    private List<?> other;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public ResponseBean getResponse() {
        return response;
    }

    public void setResponse(ResponseBean response) {
        this.response = response;
    }

    public RequestBean getRequest() {
        return request;
    }

    public void setRequest(RequestBean request) {
        this.request = request;
    }

    public List<?> getOther() {
        return other;
    }

    public void setOther(List<?> other) {
        this.other = other;
    }

    public static class ResponseBean {
        /**
         * userid : 2
         * username : 17600904682
         * realname :
         * avatar : http://park.hostop.net/upload/20180915/6755fdb2ff9a3f6cdb253300aecc5f69.jpg
         * nickname : 呵呵呵
         * uuid : 8AE5A3FB-9B39-4E56-85F1-A5117A2DF693
         * parkId : 1
         * parkInfo : {"parkId":"1","parkName":"温州平阳众创城"}
         * status : 1
         * typeId : 0
         * birth : 2147483647
         * typeId2 : 3
         * token : 83fe557752783abc976e70e7fa1486e2
         * toBind : 0
         */

        private String userid;
        private String username;
        private String realname;
        private String avatar;
        private String nickname;
        private String uuid;
        private String parkId;
        private ParkInfoBean parkInfo;
        private String status;
        private String typeId;
        private String birth;
        private String typeId2;
        private String token;
        private String toBind;

        public String getUserid() {
            return userid;
        }

        public void setUserid(String userid) {
            this.userid = userid;
        }

        public String getUsername() {
            return username;
        }

        public void setUsername(String username) {
            this.username = username;
        }

        public String getRealname() {
            return realname;
        }

        public void setRealname(String realname) {
            this.realname = realname;
        }

        public String getAvatar() {
            return avatar;
        }

        public void setAvatar(String avatar) {
            this.avatar = avatar;
        }

        public String getNickname() {
            return nickname;
        }

        public void setNickname(String nickname) {
            this.nickname = nickname;
        }

        public String getUuid() {
            return uuid;
        }

        public void setUuid(String uuid) {
            this.uuid = uuid;
        }

        public String getParkId() {
            return parkId;
        }

        public void setParkId(String parkId) {
            this.parkId = parkId;
        }

        public ParkInfoBean getParkInfo() {
            return parkInfo;
        }

        public void setParkInfo(ParkInfoBean parkInfo) {
            this.parkInfo = parkInfo;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getTypeId() {
            return typeId;
        }

        public void setTypeId(String typeId) {
            this.typeId = typeId;
        }

        public String getBirth() {
            return birth;
        }

        public void setBirth(String birth) {
            this.birth = birth;
        }

        public String getTypeId2() {
            return typeId2;
        }

        public void setTypeId2(String typeId2) {
            this.typeId2 = typeId2;
        }

        public String getToken() {
            return token;
        }

        public void setToken(String token) {
            this.token = token;
        }

        public String getToBind() {
            return toBind;
        }

        public void setToBind(String toBind) {
            this.toBind = toBind;
        }

        public static class ParkInfoBean {
            /**
             * parkId : 1
             * parkName : 温州平阳众创城
             */

            private String parkId;
            private String parkName;

            public String getParkId() {
                return parkId;
            }

            public void setParkId(String parkId) {
                this.parkId = parkId;
            }

            public String getParkName() {
                return parkName;
            }

            public void setParkName(String parkName) {
                this.parkName = parkName;
            }
        }
    }

    public static class RequestBean {
        /**
         * phone : 17600904682
         * password : 123456
         */

        private String phone;
        private String password;

        public String getPhone() {
            return phone;
        }

        public void setPhone(String phone) {
            this.phone = phone;
        }

        public String getPassword() {
            return password;
        }

        public void setPassword(String password) {
            this.password = password;
        }
    }
}
