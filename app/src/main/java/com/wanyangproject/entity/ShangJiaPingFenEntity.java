package com.wanyangproject.entity;

import java.util.List;

/**
 * Created by 甜甜 on 2018/8/25.
 */

public class ShangJiaPingFenEntity {
    /**
     * code : 200
     * msg :
     * response : {"list":[{"id":"19","Distribution":"3","service":"3","type":"2","goods_id":"30","user_id":"2","shop_id":"4","content":"的","order_sn":"pk2sn20180913114358","add_time":"2018-09-13 15:57:24","nickname":"呵呵呵","avatar":"http://park.hostop.net/upload/20180915/6755fdb2ff9a3f6cdb253300aecc5f69.jpg"},{"id":"18","Distribution":"3","service":"3","type":"2","goods_id":"30","user_id":"2","shop_id":"4","content":"吧","order_sn":"pk2sn20180913142646","add_time":"2018-09-13 15:57:17","nickname":"呵呵呵","avatar":"http://park.hostop.net/upload/20180915/6755fdb2ff9a3f6cdb253300aecc5f69.jpg"},{"id":"13","Distribution":"3","service":"3","type":"2","goods_id":"30","user_id":"2","shop_id":"4","content":"测试","order_sn":"pk2sn20180913095119","add_time":"2018-09-13 09:52:42","nickname":"呵呵呵","avatar":"http://park.hostop.net/upload/20180915/6755fdb2ff9a3f6cdb253300aecc5f69.jpg"},{"id":"12","Distribution":"5","service":"5","type":"2","goods_id":"","user_id":"4","shop_id":"4","content":"啊额咯咯KKK","order_sn":"pk4sn20180912185736","add_time":"2018-09-12 19:00:50","nickname":"未填写","avatar":"http://park.hostop.net/upload/20180915/e1018ded93926ef13ff0e3c44c247e0f.png"},{"id":"11","Distribution":"5","service":"5","type":"2","goods_id":"","user_id":"4","shop_id":"4","content":"knot","order_sn":"pk4sn20180912142129","add_time":"2018-09-12 17:16:40","nickname":"未填写","avatar":"http://park.hostop.net/upload/20180915/e1018ded93926ef13ff0e3c44c247e0f.png"},{"id":"10","Distribution":"5","service":"5","type":"2","goods_id":"","user_id":"4","shop_id":"4","content":"jolly","order_sn":"pk4sn20180912142549","add_time":"2018-09-12 17:12:41","nickname":"未填写","avatar":"http://park.hostop.net/upload/20180915/e1018ded93926ef13ff0e3c44c247e0f.png"}],"shu":"6","ping":"4.0 "}
     * request : {"parkId":"1","phone":"17600904682"}
     * other : []
     */

    private int code;
    private String msg;
    private ResponseBean response;
    private RequestBean request;
    private List<?> other;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public ResponseBean getResponse() {
        return response;
    }

    public void setResponse(ResponseBean response) {
        this.response = response;
    }

    public RequestBean getRequest() {
        return request;
    }

    public void setRequest(RequestBean request) {
        this.request = request;
    }

    public List<?> getOther() {
        return other;
    }

    public void setOther(List<?> other) {
        this.other = other;
    }

    public static class ResponseBean {
        /**
         * list : [{"id":"19","Distribution":"3","service":"3","type":"2","goods_id":"30","user_id":"2","shop_id":"4","content":"的","order_sn":"pk2sn20180913114358","add_time":"2018-09-13 15:57:24","nickname":"呵呵呵","avatar":"http://park.hostop.net/upload/20180915/6755fdb2ff9a3f6cdb253300aecc5f69.jpg"},{"id":"18","Distribution":"3","service":"3","type":"2","goods_id":"30","user_id":"2","shop_id":"4","content":"吧","order_sn":"pk2sn20180913142646","add_time":"2018-09-13 15:57:17","nickname":"呵呵呵","avatar":"http://park.hostop.net/upload/20180915/6755fdb2ff9a3f6cdb253300aecc5f69.jpg"},{"id":"13","Distribution":"3","service":"3","type":"2","goods_id":"30","user_id":"2","shop_id":"4","content":"测试","order_sn":"pk2sn20180913095119","add_time":"2018-09-13 09:52:42","nickname":"呵呵呵","avatar":"http://park.hostop.net/upload/20180915/6755fdb2ff9a3f6cdb253300aecc5f69.jpg"},{"id":"12","Distribution":"5","service":"5","type":"2","goods_id":"","user_id":"4","shop_id":"4","content":"啊额咯咯KKK","order_sn":"pk4sn20180912185736","add_time":"2018-09-12 19:00:50","nickname":"未填写","avatar":"http://park.hostop.net/upload/20180915/e1018ded93926ef13ff0e3c44c247e0f.png"},{"id":"11","Distribution":"5","service":"5","type":"2","goods_id":"","user_id":"4","shop_id":"4","content":"knot","order_sn":"pk4sn20180912142129","add_time":"2018-09-12 17:16:40","nickname":"未填写","avatar":"http://park.hostop.net/upload/20180915/e1018ded93926ef13ff0e3c44c247e0f.png"},{"id":"10","Distribution":"5","service":"5","type":"2","goods_id":"","user_id":"4","shop_id":"4","content":"jolly","order_sn":"pk4sn20180912142549","add_time":"2018-09-12 17:12:41","nickname":"未填写","avatar":"http://park.hostop.net/upload/20180915/e1018ded93926ef13ff0e3c44c247e0f.png"}]
         * shu : 6
         * ping : 4.0
         */

        private String shu;
        private String ping;
        private List<ListBean> list;

        public String getShu() {
            return shu;
        }

        public void setShu(String shu) {
            this.shu = shu;
        }

        public String getPing() {
            return ping;
        }

        public void setPing(String ping) {
            this.ping = ping;
        }

        public List<ListBean> getList() {
            return list;
        }

        public void setList(List<ListBean> list) {
            this.list = list;
        }

        public static class ListBean {
            /**
             * id : 19
             * Distribution : 3
             * service : 3
             * type : 2
             * goods_id : 30
             * user_id : 2
             * shop_id : 4
             * content : 的
             * order_sn : pk2sn20180913114358
             * add_time : 2018-09-13 15:57:24
             * nickname : 呵呵呵
             * avatar : http://park.hostop.net/upload/20180915/6755fdb2ff9a3f6cdb253300aecc5f69.jpg
             */

            private String id;
            private String Distribution;
            private String service;
            private String type;
            private String goods_id;
            private String user_id;
            private String shop_id;
            private String content;
            private String order_sn;
            private String add_time;
            private String nickname;
            private String avatar;

            public String getId() {
                return id;
            }

            public void setId(String id) {
                this.id = id;
            }

            public String getDistribution() {
                return Distribution;
            }

            public void setDistribution(String Distribution) {
                this.Distribution = Distribution;
            }

            public String getService() {
                return service;
            }

            public void setService(String service) {
                this.service = service;
            }

            public String getType() {
                return type;
            }

            public void setType(String type) {
                this.type = type;
            }

            public String getGoods_id() {
                return goods_id;
            }

            public void setGoods_id(String goods_id) {
                this.goods_id = goods_id;
            }

            public String getUser_id() {
                return user_id;
            }

            public void setUser_id(String user_id) {
                this.user_id = user_id;
            }

            public String getShop_id() {
                return shop_id;
            }

            public void setShop_id(String shop_id) {
                this.shop_id = shop_id;
            }

            public String getContent() {
                return content;
            }

            public void setContent(String content) {
                this.content = content;
            }

            public String getOrder_sn() {
                return order_sn;
            }

            public void setOrder_sn(String order_sn) {
                this.order_sn = order_sn;
            }

            public String getAdd_time() {
                return add_time;
            }

            public void setAdd_time(String add_time) {
                this.add_time = add_time;
            }

            public String getNickname() {
                return nickname;
            }

            public void setNickname(String nickname) {
                this.nickname = nickname;
            }

            public String getAvatar() {
                return avatar;
            }

            public void setAvatar(String avatar) {
                this.avatar = avatar;
            }
        }
    }

    public static class RequestBean {
        /**
         * parkId : 1
         * phone : 17600904682
         */

        private String parkId;
        private String phone;

        public String getParkId() {
            return parkId;
        }

        public void setParkId(String parkId) {
            this.parkId = parkId;
        }

        public String getPhone() {
            return phone;
        }

        public void setPhone(String phone) {
            this.phone = phone;
        }
    }


//    private String id;
//    private String Distribution;  //配送星级
//    private String service; //商家服务星级
//    private String type; //	配送星级
//    private String goods_id;
//    private String user_id;
//    private String shop_id;
//    private String content; //	评价内容
//    private String order_sn;
//    private String add_time; // 评价时间
//    private String shu;//评价数量
//    private String ping;//商家评分
//
//
//    public String getId() {
//        return id;
//    }
//
//    public void setId(String id) {
//        this.id = id;
//    }
//
//    public String getDistribution() {
//        return Distribution;
//    }
//
//    public void setDistribution(String distribution) {
//        Distribution = distribution;
//    }
//
//    public String getService() {
//        return service;
//    }
//
//    public void setService(String service) {
//        this.service = service;
//    }
//
//    public String getType() {
//        return type;
//    }
//
//    public void setType(String type) {
//        this.type = type;
//    }
//
//    public String getGoods_id() {
//        return goods_id;
//    }
//
//    public void setGoods_id(String goods_id) {
//        this.goods_id = goods_id;
//    }
//
//    public String getUser_id() {
//        return user_id;
//    }
//
//    public void setUser_id(String user_id) {
//        this.user_id = user_id;
//    }
//
//    public String getShop_id() {
//        return shop_id;
//    }
//
//    public void setShop_id(String shop_id) {
//        this.shop_id = shop_id;
//    }
//
//    public String getContent() {
//        return content;
//    }
//
//    public void setContent(String content) {
//        this.content = content;
//    }
//
//    public String getOrder_sn() {
//        return order_sn;
//    }
//
//    public void setOrder_sn(String order_sn) {
//        this.order_sn = order_sn;
//    }
//
//    public String getAdd_time() {
//        return add_time;
//    }
//
//    public void setAdd_time(String add_time) {
//        this.add_time = add_time;
//    }
//
//    public String getShu() {
//        return shu;
//    }
//
//    public void setShu(String shu) {
//        this.shu = shu;
//    }
//
//    public String getPing() {
//        return ping;
//    }
//
//    public void setPing(String ping) {
//        this.ping = ping;
//    }



}
