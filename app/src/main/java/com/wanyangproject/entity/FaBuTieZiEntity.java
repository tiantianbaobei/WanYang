package com.wanyangproject.entity;

import java.util.List;

/**
 * Created by 甜甜 on 2018/9/4.
 */

public class FaBuTieZiEntity {
    /**
     * code : 200
     * msg :
     * response : {"list":[{"id":"252","title":"测试","content":"本来","add_time":"2018-12-12 11:04","uinfo":{"userid":"119","avatar":"http://park.vyzcc.com/static/image/logo.png","nickname":"wy908820","username":"17600902121"},"imgArr":[""],"reply":"0","like":"0","isLike":"0","status":"1","setTop":"0","isPc":"2"}],"nextPage":"1"}
     * request : []
     * other : []
     */

    private int code;
    private String msg;
    private ResponseBean response;
    private List<?> request;
    private List<?> other;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public ResponseBean getResponse() {
        return response;
    }

    public void setResponse(ResponseBean response) {
        this.response = response;
    }

    public List<?> getRequest() {
        return request;
    }

    public void setRequest(List<?> request) {
        this.request = request;
    }

    public List<?> getOther() {
        return other;
    }

    public void setOther(List<?> other) {
        this.other = other;
    }

    public static class ResponseBean {
        /**
         * list : [{"id":"252","title":"测试","content":"本来","add_time":"2018-12-12 11:04","uinfo":{"userid":"119","avatar":"http://park.vyzcc.com/static/image/logo.png","nickname":"wy908820","username":"17600902121"},"imgArr":[""],"reply":"0","like":"0","isLike":"0","status":"1","setTop":"0","isPc":"2"}]
         * nextPage : 1
         */

        private String nextPage;
        private List<ListBean> list;

        public String getNextPage() {
            return nextPage;
        }

        public void setNextPage(String nextPage) {
            this.nextPage = nextPage;
        }

        public List<ListBean> getList() {
            return list;
        }

        public void setList(List<ListBean> list) {
            this.list = list;
        }

        public static class ListBean {
            /**
             * id : 252
             * title : 测试
             * content : 本来
             * add_time : 2018-12-12 11:04
             * uinfo : {"userid":"119","avatar":"http://park.vyzcc.com/static/image/logo.png","nickname":"wy908820","username":"17600902121"}
             * imgArr : [""]
             * reply : 0
             * like : 0
             * isLike : 0
             * status : 1
             * setTop : 0
             * isPc : 2
             */

            private String id;
            private String title;
            private String content;
            private String add_time;
            private UinfoBean uinfo;
            private String reply;
            private String like;
            private String isLike;
            private String status;
            private String setTop;
            private String isPc;
            private List<String> imgArr;

            public String getId() {
                return id;
            }

            public void setId(String id) {
                this.id = id;
            }

            public String getTitle() {
                return title;
            }

            public void setTitle(String title) {
                this.title = title;
            }

            public String getContent() {
                return content;
            }

            public void setContent(String content) {
                this.content = content;
            }

            public String getAdd_time() {
                return add_time;
            }

            public void setAdd_time(String add_time) {
                this.add_time = add_time;
            }

            public UinfoBean getUinfo() {
                return uinfo;
            }

            public void setUinfo(UinfoBean uinfo) {
                this.uinfo = uinfo;
            }

            public String getReply() {
                return reply;
            }

            public void setReply(String reply) {
                this.reply = reply;
            }

            public String getLike() {
                return like;
            }

            public void setLike(String like) {
                this.like = like;
            }

            public String getIsLike() {
                return isLike;
            }

            public void setIsLike(String isLike) {
                this.isLike = isLike;
            }

            public String getStatus() {
                return status;
            }

            public void setStatus(String status) {
                this.status = status;
            }

            public String getSetTop() {
                return setTop;
            }

            public void setSetTop(String setTop) {
                this.setTop = setTop;
            }

            public String getIsPc() {
                return isPc;
            }

            public void setIsPc(String isPc) {
                this.isPc = isPc;
            }

            public List<String> getImgArr() {
                return imgArr;
            }

            public void setImgArr(List<String> imgArr) {
                this.imgArr = imgArr;
            }

            public static class UinfoBean {
                /**
                 * userid : 119
                 * avatar : http://park.vyzcc.com/static/image/logo.png
                 * nickname : wy908820
                 * username : 17600902121
                 */

                private String userid;
                private String avatar;
                private String nickname;
                private String username;

                public String getUserid() {
                    return userid;
                }

                public void setUserid(String userid) {
                    this.userid = userid;
                }

                public String getAvatar() {
                    return avatar;
                }

                public void setAvatar(String avatar) {
                    this.avatar = avatar;
                }

                public String getNickname() {
                    return nickname;
                }

                public void setNickname(String nickname) {
                    this.nickname = nickname;
                }

                public String getUsername() {
                    return username;
                }

                public void setUsername(String username) {
                    this.username = username;
                }
            }
        }
    }


//    /**
//     * code : 200
//     * msg :
//     * response : {"list":[{"id":"189","title":"测试","content":"哈哈","add_time":"2018-11-05 09:41","uinfo":{"userid":"93","avatar":"http://park.hostop.net/static/image/logo.png","nickname":"wy459807","username":"13233440220"},"imgArr":["http://park.hostop.net/upload/20181105/102a3ca600d492c428044c48fece6075.png"],"reply":"0","like":"0","isLike":"0","status":"0","setTop":"0"},{"id":"188","title":"标题","content":"内容","add_time":"2018-11-05 09:39","uinfo":{"userid":"93","avatar":"http://park.hostop.net/static/image/logo.png","nickname":"wy459807","username":"13233440220"},"imgArr":[""],"reply":"0","like":"0","isLike":"0","status":"1","setTop":"0"},{"id":"186","title":"","content":"啊啊啊","add_time":"2018-11-01 14:23","uinfo":{"userid":"93","avatar":"http://park.hostop.net/static/image/logo.png","nickname":"wy459807","username":"13233440220"},"imgArr":["http://park.hostop.net/upload/20181101/cde5fe8868875c964b6ec1ad5eb1c9bf.png"],"reply":"0","like":"0","isLike":"0","status":"0","setTop":"0"}],"nextPage":"1"}
//     * request : []
//     * other : []
//     */
//
//    private int code;
//    private String msg;
//    private ResponseBean response;
//    private List<?> request;
//    private List<?> other;
//
//    public int getCode() {
//        return code;
//    }
//
//    public void setCode(int code) {
//        this.code = code;
//    }
//
//    public String getMsg() {
//        return msg;
//    }
//
//    public void setMsg(String msg) {
//        this.msg = msg;
//    }
//
//    public ResponseBean getResponse() {
//        return response;
//    }
//
//    public void setResponse(ResponseBean response) {
//        this.response = response;
//    }
//
//    public List<?> getRequest() {
//        return request;
//    }
//
//    public void setRequest(List<?> request) {
//        this.request = request;
//    }
//
//    public List<?> getOther() {
//        return other;
//    }
//
//    public void setOther(List<?> other) {
//        this.other = other;
//    }
//
//    public static class ResponseBean {
//        /**
//         * list : [{"id":"189","title":"测试","content":"哈哈","add_time":"2018-11-05 09:41","uinfo":{"userid":"93","avatar":"http://park.hostop.net/static/image/logo.png","nickname":"wy459807","username":"13233440220"},"imgArr":["http://park.hostop.net/upload/20181105/102a3ca600d492c428044c48fece6075.png"],"reply":"0","like":"0","isLike":"0","status":"0","setTop":"0"},{"id":"188","title":"标题","content":"内容","add_time":"2018-11-05 09:39","uinfo":{"userid":"93","avatar":"http://park.hostop.net/static/image/logo.png","nickname":"wy459807","username":"13233440220"},"imgArr":[""],"reply":"0","like":"0","isLike":"0","status":"1","setTop":"0"},{"id":"186","title":"","content":"啊啊啊","add_time":"2018-11-01 14:23","uinfo":{"userid":"93","avatar":"http://park.hostop.net/static/image/logo.png","nickname":"wy459807","username":"13233440220"},"imgArr":["http://park.hostop.net/upload/20181101/cde5fe8868875c964b6ec1ad5eb1c9bf.png"],"reply":"0","like":"0","isLike":"0","status":"0","setTop":"0"}]
//         * nextPage : 1
//         */
//
//        private String nextPage;
//        private List<ListBean> list;
//
//        public String getNextPage() {
//            return nextPage;
//        }
//
//        public void setNextPage(String nextPage) {
//            this.nextPage = nextPage;
//        }
//
//        public List<ListBean> getList() {
//            return list;
//        }
//
//        public void setList(List<ListBean> list) {
//            this.list = list;
//        }
//
//        public static class ListBean {
//            /**
//             * id : 189
//             * title : 测试
//             * content : 哈哈
//             * add_time : 2018-11-05 09:41
//             * uinfo : {"userid":"93","avatar":"http://park.hostop.net/static/image/logo.png","nickname":"wy459807","username":"13233440220"}
//             * imgArr : ["http://park.hostop.net/upload/20181105/102a3ca600d492c428044c48fece6075.png"]
//             * reply : 0
//             * like : 0
//             * isLike : 0
//             * status : 0
//             * setTop : 0
//             */
//
//            private String id;
//            private String title;
//            private String content;
//            private String add_time;
//            private UinfoBean uinfo;
//            private String reply;
//            private String like;
//            private String isLike;
//            private String status;
//            private String setTop;
//            private List<String> imgArr;
//
//            public String getId() {
//                return id;
//            }
//
//            public void setId(String id) {
//                this.id = id;
//            }
//
//            public String getTitle() {
//                return title;
//            }
//
//            public void setTitle(String title) {
//                this.title = title;
//            }
//
//            public String getContent() {
//                return content;
//            }
//
//            public void setContent(String content) {
//                this.content = content;
//            }
//
//            public String getAdd_time() {
//                return add_time;
//            }
//
//            public void setAdd_time(String add_time) {
//                this.add_time = add_time;
//            }
//
//            public UinfoBean getUinfo() {
//                return uinfo;
//            }
//
//            public void setUinfo(UinfoBean uinfo) {
//                this.uinfo = uinfo;
//            }
//
//            public String getReply() {
//                return reply;
//            }
//
//            public void setReply(String reply) {
//                this.reply = reply;
//            }
//
//            public String getLike() {
//                return like;
//            }
//
//            public void setLike(String like) {
//                this.like = like;
//            }
//
//            public String getIsLike() {
//                return isLike;
//            }
//
//            public void setIsLike(String isLike) {
//                this.isLike = isLike;
//            }
//
//            public String getStatus() {
//                return status;
//            }
//
//            public void setStatus(String status) {
//                this.status = status;
//            }
//
//            public String getSetTop() {
//                return setTop;
//            }
//
//            public void setSetTop(String setTop) {
//                this.setTop = setTop;
//            }
//
//            public List<String> getImgArr() {
//                return imgArr;
//            }
//
//            public void setImgArr(List<String> imgArr) {
//                this.imgArr = imgArr;
//            }
//
//            public static class UinfoBean {
//                /**
//                 * userid : 93
//                 * avatar : http://park.hostop.net/static/image/logo.png
//                 * nickname : wy459807
//                 * username : 13233440220
//                 */
//
//                private String userid;
//                private String avatar;
//                private String nickname;
//                private String username;
//
//                public String getUserid() {
//                    return userid;
//                }
//
//                public void setUserid(String userid) {
//                    this.userid = userid;
//                }
//
//                public String getAvatar() {
//                    return avatar;
//                }
//
//                public void setAvatar(String avatar) {
//                    this.avatar = avatar;
//                }
//
//                public String getNickname() {
//                    return nickname;
//                }
//
//                public void setNickname(String nickname) {
//                    this.nickname = nickname;
//                }
//
//                public String getUsername() {
//                    return username;
//                }
//
//                public void setUsername(String username) {
//                    this.username = username;
//                }
//            }
//        }
//    }
















//
//    /**
//     * code : 200
//     * msg :
//     * response : {"list":[{"id":"73","title":"测试","add_time":"3小时前","uinfo":{"userid":"3","avatar":"http://park.hostop.net/static/image/logo.png","nickname":"未填写","username":"18364197153"},"imgArr":["http://park.hostop.net/upload/20180904/4fc27573674657324aafd2f318fde60b.jpg"],"reply":"0","like":"0","isLike":"0","status":"1"}],"nextPage":"1"}
//     * request : []
//     * other : []
//     */
//
//    private int code;
//    private String msg;
//    private ResponseBean response;
//    private List<?> request;
//    private List<?> other;
//
//    public int getCode() {
//        return code;
//    }
//
//    public void setCode(int code) {
//        this.code = code;
//    }
//
//    public String getMsg() {
//        return msg;
//    }
//
//    public void setMsg(String msg) {
//        this.msg = msg;
//    }
//
//    public ResponseBean getResponse() {
//        return response;
//    }
//
//    public void setResponse(ResponseBean response) {
//        this.response = response;
//    }
//
//    public List<?> getRequest() {
//        return request;
//    }
//
//    public void setRequest(List<?> request) {
//        this.request = request;
//    }
//
//    public List<?> getOther() {
//        return other;
//    }
//
//    public void setOther(List<?> other) {
//        this.other = other;
//    }
//
//    public static class ResponseBean {
//        /**
//         * list : [{"id":"73","title":"测试","add_time":"3小时前","uinfo":{"userid":"3","avatar":"http://park.hostop.net/static/image/logo.png","nickname":"未填写","username":"18364197153"},"imgArr":["http://park.hostop.net/upload/20180904/4fc27573674657324aafd2f318fde60b.jpg"],"reply":"0","like":"0","isLike":"0","status":"1"}]
//         * nextPage : 1
//         */
//
//        private String nextPage;
//        private List<ListBean> list;
//
//        public String getNextPage() {
//            return nextPage;
//        }
//
//        public void setNextPage(String nextPage) {
//            this.nextPage = nextPage;
//        }
//
//        public List<ListBean> getList() {
//            return list;
//        }
//
//        public void setList(List<ListBean> list) {
//            this.list = list;
//        }
//
//        public static class ListBean {
//            /**
//             * id : 73
//             * title : 测试
//             * add_time : 3小时前
//             * uinfo : {"userid":"3","avatar":"http://park.hostop.net/static/image/logo.png","nickname":"未填写","username":"18364197153"}
//             * imgArr : ["http://park.hostop.net/upload/20180904/4fc27573674657324aafd2f318fde60b.jpg"]
//             * reply : 0
//             * like : 0
//             * isLike : 0
//             * status : 1
//             */
//
//            private String id;
//            private String title;
//            private String add_time;
//            private UinfoBean uinfo;
//            private String reply;
//            private String like;
//            private String isLike;
//            private String status;
//            private List<String> imgArr;
//
//            public String getId() {
//                return id;
//            }
//
//            public void setId(String id) {
//                this.id = id;
//            }
//
//            public String getTitle() {
//                return title;
//            }
//
//            public void setTitle(String title) {
//                this.title = title;
//            }
//
//            public String getAdd_time() {
//                return add_time;
//            }
//
//            public void setAdd_time(String add_time) {
//                this.add_time = add_time;
//            }
//
//            public UinfoBean getUinfo() {
//                return uinfo;
//            }
//
//            public void setUinfo(UinfoBean uinfo) {
//                this.uinfo = uinfo;
//            }
//
//            public String getReply() {
//                return reply;
//            }
//
//            public void setReply(String reply) {
//                this.reply = reply;
//            }
//
//            public String getLike() {
//                return like;
//            }
//
//            public void setLike(String like) {
//                this.like = like;
//            }
//
//            public String getIsLike() {
//                return isLike;
//            }
//
//            public void setIsLike(String isLike) {
//                this.isLike = isLike;
//            }
//
//            public String getStatus() {
//                return status;
//            }
//
//            public void setStatus(String status) {
//                this.status = status;
//            }
//
//            public List<String> getImgArr() {
//                return imgArr;
//            }
//
//            public void setImgArr(List<String> imgArr) {
//                this.imgArr = imgArr;
//            }
//
//            public static class UinfoBean {
//                /**
//                 * userid : 3
//                 * avatar : http://park.hostop.net/static/image/logo.png
//                 * nickname : 未填写
//                 * username : 18364197153
//                 */
//
//                private String userid;
//                private String avatar;
//                private String nickname;
//                private String username;
//
//                public String getUserid() {
//                    return userid;
//                }
//
//                public void setUserid(String userid) {
//                    this.userid = userid;
//                }
//
//                public String getAvatar() {
//                    return avatar;
//                }
//
//                public void setAvatar(String avatar) {
//                    this.avatar = avatar;
//                }
//
//                public String getNickname() {
//                    return nickname;
//                }
//
//                public void setNickname(String nickname) {
//                    this.nickname = nickname;
//                }
//
//                public String getUsername() {
//                    return username;
//                }
//
//                public void setUsername(String username) {
//                    this.username = username;
//                }
//            }
//        }
//    }
}
