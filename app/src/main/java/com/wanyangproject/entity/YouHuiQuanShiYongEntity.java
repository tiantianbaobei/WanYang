package com.wanyangproject.entity;

import java.util.List;

/**
 * Created by 甜甜 on 2018/9/15.
 */

public class YouHuiQuanShiYongEntity {

    /**
     * code : 200
     * msg :
     * response : {"mes":"成功","type":"1"}
     * request : {"phone":"17600904682","parkId":"1","code":"10d4236bfc0d6e1b4ce16906d879a6b73"}
     * other : []
     */

    private int code;
    private String msg;
    private ResponseBean response;
    private RequestBean request;
    private List<?> other;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public ResponseBean getResponse() {
        return response;
    }

    public void setResponse(ResponseBean response) {
        this.response = response;
    }

    public RequestBean getRequest() {
        return request;
    }

    public void setRequest(RequestBean request) {
        this.request = request;
    }

    public List<?> getOther() {
        return other;
    }

    public void setOther(List<?> other) {
        this.other = other;
    }

    public static class ResponseBean {
        /**
         * mes : 成功
         * type : 1
         */

        private String mes;
        private String type;

        public String getMes() {
            return mes;
        }

        public void setMes(String mes) {
            this.mes = mes;
        }

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }
    }

    public static class RequestBean {
        /**
         * phone : 17600904682
         * parkId : 1
         * code : 10d4236bfc0d6e1b4ce16906d879a6b73
         */

        private String phone;
        private String parkId;
        private String code;

        public String getPhone() {
            return phone;
        }

        public void setPhone(String phone) {
            this.phone = phone;
        }

        public String getParkId() {
            return parkId;
        }

        public void setParkId(String parkId) {
            this.parkId = parkId;
        }

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }
    }
}
