package com.wanyangproject.entity;

import java.util.ArrayList;

/**
 * Created by 甜甜 on 2018/8/7.
 */

public class NumberEntity {
    public String id;
    public String number;
    public String pic;
    public String name;


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public static ArrayList<NumberEntity> lists = new ArrayList<>();
    private static String qisongjia;
    private static String peisongfei;


    public static String getQisongjia() {
        return qisongjia;
    }

    public static void setQisongjia(String qisongjia) {
        NumberEntity.qisongjia = qisongjia;
    }

    public static String getPeisongfei() {
        return peisongfei;
    }

    public static void setPeisongfei(String peisongfei) {
        NumberEntity.peisongfei = peisongfei;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getPic() {
        return pic;
    }

    public void setPic(String pic) {
        this.pic = pic;
    }
}
