package com.wanyangproject.entity;

import java.util.List;

/**
 * Created by 甜甜 on 2018/8/21.
 */

public class ShangJiaShangPinZhuangTaiEntity {

    /**
     * code : 200
     * msg :
     * response : {"mes":"操作成功！"}
     * request : {"parkId":"1","phone":"17600904682","goods_id":"14","type":"2"}
     * other : []
     */

    private int code;
    private String msg;
    private ResponseBean response;
    private RequestBean request;
    private List<?> other;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public ResponseBean getResponse() {
        return response;
    }

    public void setResponse(ResponseBean response) {
        this.response = response;
    }

    public RequestBean getRequest() {
        return request;
    }

    public void setRequest(RequestBean request) {
        this.request = request;
    }

    public List<?> getOther() {
        return other;
    }

    public void setOther(List<?> other) {
        this.other = other;
    }

    public static class ResponseBean {
        /**
         * mes : 操作成功！
         */

        private String mes;

        public String getMes() {
            return mes;
        }

        public void setMes(String mes) {
            this.mes = mes;
        }
    }

    public static class RequestBean {
        /**
         * parkId : 1
         * phone : 17600904682
         * goods_id : 14
         * type : 2
         */

        private String parkId;
        private String phone;
        private String goods_id;
        private String type;

        public String getParkId() {
            return parkId;
        }

        public void setParkId(String parkId) {
            this.parkId = parkId;
        }

        public String getPhone() {
            return phone;
        }

        public void setPhone(String phone) {
            this.phone = phone;
        }

        public String getGoods_id() {
            return goods_id;
        }

        public void setGoods_id(String goods_id) {
            this.goods_id = goods_id;
        }

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }
    }
}
