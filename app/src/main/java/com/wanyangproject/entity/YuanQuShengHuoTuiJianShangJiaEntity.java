package com.wanyangproject.entity;

import java.util.List;

/**
 * Created by 甜甜 on 2018/8/6.
 */

public class YuanQuShengHuoTuiJianShangJiaEntity {
    /**
     * code : 200
     * msg :
     * response : {"shop":[{"nickname":"永丰超市","sales":"0","business":"2","starting":"0","photo":"park.hostop.net//static/image/shang.png","cost":"0","mer_class":"商超便利","id":"29"},{"nickname":"好吃","sales":"1","business":"1","starting":"1","photo":"park.hostop.net//static/image/shang.png","cost":"0","mer_class":"美食","id":"17"},{"nickname":"太一三和","sales":"14","business":"1","starting":"0.02","photo":"park.hostop.net//upload/20180916/eeade6ef29e79588472dc144c7f7211b.png","cost":"0.02","mer_class":"","id":"10"},{"nickname":"201809012030","sales":"207","business":"1","starting":"0.01","photo":"park.hostop.net//upload/20180921/b86116e208b14014779c83494d9b9211.jpg","cost":"0","mer_class":"商超便利","id":"8"},{"nickname":"商家","sales":"6","business":"1","starting":"0","photo":"park.hostop.net//upload/20180823/bab664273f521cea285761a6e79569f6.png","cost":"0","mer_class":"外卖","id":"1"}],"MerchantClass":[{"name":"外卖","photo":"park.hostop.net/upload/20181022/032243b0c57aa1f4d8e1d6c05899198b.png","id":"1"},{"name":"购物","photo":"park.hostop.net/upload/20181022/4f1ba7f0071615bccd4c8da961abdf08.png","id":"8"},{"name":"美容美发","photo":"park.hostop.net/upload/20181022/4b99d1ed4b0415e35b716423733918b8.png","id":"7"},{"name":"休闲娱乐","photo":"park.hostop.net/upload/20181022/9ec49854958c5990ade2e0a883fde3f0.png","id":"6"},{"name":"商超便利","photo":"park.hostop.net/upload/20181022/c12bb49fa3c03567822dba9d8cff5f30.png","id":"2"},{"name":"医药健康","photo":"park.hostop.net/upload/20181022/0a9d19df454df6aa192efeb825c32fd2.png","id":"4"},{"name":"美食","photo":"park.hostop.net/upload/20181022/19b37fd3398d5e2f16b0909e49f8b152.png","id":"5"}],"ShopPreferences":[{"title":"优惠政策来啦,园区生活福利强势来袭","introduction":"商家活动","id":"1"}]}
     * request : {"parkId":"1"}
     * other : []
     */

    private int code;
    private String msg;
    private ResponseBean response;
    private RequestBean request;
    private List<?> other;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public ResponseBean getResponse() {
        return response;
    }

    public void setResponse(ResponseBean response) {
        this.response = response;
    }

    public RequestBean getRequest() {
        return request;
    }

    public void setRequest(RequestBean request) {
        this.request = request;
    }

    public List<?> getOther() {
        return other;
    }

    public void setOther(List<?> other) {
        this.other = other;
    }

    public static class ResponseBean {
        private List<ShopBean> shop;
        private List<MerchantClassBean> MerchantClass;
        private List<ShopPreferencesBean> ShopPreferences;

        public List<ShopBean> getShop() {
            return shop;
        }

        public void setShop(List<ShopBean> shop) {
            this.shop = shop;
        }

        public List<MerchantClassBean> getMerchantClass() {
            return MerchantClass;
        }

        public void setMerchantClass(List<MerchantClassBean> MerchantClass) {
            this.MerchantClass = MerchantClass;
        }

        public List<ShopPreferencesBean> getShopPreferences() {
            return ShopPreferences;
        }

        public void setShopPreferences(List<ShopPreferencesBean> ShopPreferences) {
            this.ShopPreferences = ShopPreferences;
        }

        public static class ShopBean {
            /**
             * nickname : 永丰超市
             * sales : 0
             * business : 2
             * starting : 0
             * photo : park.hostop.net//static/image/shang.png
             * cost : 0
             * mer_class : 商超便利
             * id : 29
             */

            private String nickname;
            private String sales;
            private String business;
            private String starting;
            private String photo;
            private String cost;
            private String mer_class;
            private String id;

            public String getNickname() {
                return nickname;
            }

            public void setNickname(String nickname) {
                this.nickname = nickname;
            }

            public String getSales() {
                return sales;
            }

            public void setSales(String sales) {
                this.sales = sales;
            }

            public String getBusiness() {
                return business;
            }

            public void setBusiness(String business) {
                this.business = business;
            }

            public String getStarting() {
                return starting;
            }

            public void setStarting(String starting) {
                this.starting = starting;
            }

            public String getPhoto() {
                return photo;
            }

            public void setPhoto(String photo) {
                this.photo = photo;
            }

            public String getCost() {
                return cost;
            }

            public void setCost(String cost) {
                this.cost = cost;
            }

            public String getMer_class() {
                return mer_class;
            }

            public void setMer_class(String mer_class) {
                this.mer_class = mer_class;
            }

            public String getId() {
                return id;
            }

            public void setId(String id) {
                this.id = id;
            }
        }

        public static class MerchantClassBean {
            /**
             * name : 外卖
             * photo : park.hostop.net/upload/20181022/032243b0c57aa1f4d8e1d6c05899198b.png
             * id : 1
             */

            private String name;
            private String photo;
            private String id;

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }

            public String getPhoto() {
                return photo;
            }

            public void setPhoto(String photo) {
                this.photo = photo;
            }

            public String getId() {
                return id;
            }

            public void setId(String id) {
                this.id = id;
            }
        }

        public static class ShopPreferencesBean {
            /**
             * title : 优惠政策来啦,园区生活福利强势来袭
             * introduction : 商家活动
             * id : 1
             */

            private String title;
            private String introduction;
            private String id;

            public String getTitle() {
                return title;
            }

            public void setTitle(String title) {
                this.title = title;
            }

            public String getIntroduction() {
                return introduction;
            }

            public void setIntroduction(String introduction) {
                this.introduction = introduction;
            }

            public String getId() {
                return id;
            }

            public void setId(String id) {
                this.id = id;
            }
        }
    }

    public static class RequestBean {
        /**
         * parkId : 1
         */

        private String parkId;

        public String getParkId() {
            return parkId;
        }

        public void setParkId(String parkId) {
            this.parkId = parkId;
        }
    }


//    /**
//     * code : 200
//     * msg :
//     * response : {"shop":[{"nickname":"统一银座","sales":"20","starting":"20","photo":"park.hostop.net/static/image/0.jpg","cost":"10","mer_class":"外卖","id":"2"},{"nickname":"彪1","sales":"10","starting":"20","photo":"park.hostop.net/static/image/0.jpg","cost":"10","mer_class":"外卖","id":"1"}],"MerchantClass":[{"name":"超市","photo":"park.hostop.net/static/image/0.jpg","id":"2"},{"name":"外卖","photo":"park.hostop.net/static/image/0.jpg","id":"1"}],"ShopPreferences":[{"title":"商家优惠活动","introduction":"商家可在平台发布优惠活动","id":"1"}]}
//     * request : {"parkId":"1"}
//     * other : []
//     */
//
//    private int code;
//    private String msg;
//    private ResponseBean response;
//    private RequestBean request;
//    private List<?> other;
//
//    public int getCode() {
//        return code;
//    }
//
//    public void setCode(int code) {
//        this.code = code;
//    }
//
//    public String getMsg() {
//        return msg;
//    }
//
//    public void setMsg(String msg) {
//        this.msg = msg;
//    }
//
//    public ResponseBean getResponse() {
//        return response;
//    }
//
//    public void setResponse(ResponseBean response) {
//        this.response = response;
//    }
//
//    public RequestBean getRequest() {
//        return request;
//    }
//
//    public void setRequest(RequestBean request) {
//        this.request = request;
//    }
//
//    public List<?> getOther() {
//        return other;
//    }
//
//    public void setOther(List<?> other) {
//        this.other = other;
//    }
//
//    public static class ResponseBean {
//        private List<ShopBean> shop;
//        private List<MerchantClassBean> MerchantClass;
//        private List<ShopPreferencesBean> ShopPreferences;
//
//        public List<ShopBean> getShop() {
//            return shop;
//        }
//
//        public void setShop(List<ShopBean> shop) {
//            this.shop = shop;
//        }
//
//        public List<MerchantClassBean> getMerchantClass() {
//            return MerchantClass;
//        }
//
//        public void setMerchantClass(List<MerchantClassBean> MerchantClass) {
//            this.MerchantClass = MerchantClass;
//        }
//
//        public List<ShopPreferencesBean> getShopPreferences() {
//            return ShopPreferences;
//        }
//
//        public void setShopPreferences(List<ShopPreferencesBean> ShopPreferences) {
//            this.ShopPreferences = ShopPreferences;
//        }
//
//        public static class ShopBean {
//            /**
//             * nickname : 统一银座
//             * sales : 20
//             * starting : 20
//             * photo : park.hostop.net/static/image/0.jpg
//             * cost : 10
//             * mer_class : 外卖
//             * id : 2
//             */
//
//            private String nickname;
//            private String sales;
//            private String starting;
//            private String photo;
//            private String cost;
//            private String mer_class;
//            private String id;
//
//            public String getNickname() {
//                return nickname;
//            }
//
//            public void setNickname(String nickname) {
//                this.nickname = nickname;
//            }
//
//            public String getSales() {
//                return sales;
//            }
//
//            public void setSales(String sales) {
//                this.sales = sales;
//            }
//
//            public String getStarting() {
//                return starting;
//            }
//
//            public void setStarting(String starting) {
//                this.starting = starting;
//            }
//
//            public String getPhoto() {
//                return photo;
//            }
//
//            public void setPhoto(String photo) {
//                this.photo = photo;
//            }
//
//            public String getCost() {
//                return cost;
//            }
//
//            public void setCost(String cost) {
//                this.cost = cost;
//            }
//
//            public String getMer_class() {
//                return mer_class;
//            }
//
//            public void setMer_class(String mer_class) {
//                this.mer_class = mer_class;
//            }
//
//            public String getId() {
//                return id;
//            }
//
//            public void setId(String id) {
//                this.id = id;
//            }
//        }
//
//        public static class MerchantClassBean {
//            /**
//             * name : 超市
//             * photo : park.hostop.net/static/image/0.jpg
//             * id : 2
//             */
//
//            private String name;
//            private String photo;
//            private String id;
//
//            public String getName() {
//                return name;
//            }
//
//            public void setName(String name) {
//                this.name = name;
//            }
//
//            public String getPhoto() {
//                return photo;
//            }
//
//            public void setPhoto(String photo) {
//                this.photo = photo;
//            }
//
//            public String getId() {
//                return id;
//            }
//
//            public void setId(String id) {
//                this.id = id;
//            }
//        }
//
//        public static class ShopPreferencesBean {
//            /**
//             * title : 商家优惠活动
//             * introduction : 商家可在平台发布优惠活动
//             * id : 1
//             */
//
//            private String title;
//            private String introduction;
//            private String id;
//
//            public String getTitle() {
//                return title;
//            }
//
//            public void setTitle(String title) {
//                this.title = title;
//            }
//
//            public String getIntroduction() {
//                return introduction;
//            }
//
//            public void setIntroduction(String introduction) {
//                this.introduction = introduction;
//            }
//
//            public String getId() {
//                return id;
//            }
//
//            public void setId(String id) {
//                this.id = id;
//            }
//        }
//    }
//
//    public static class RequestBean {
//        /**
//         * parkId : 1
//         */
//
//        private String parkId;
//
//        public String getParkId() {
//            return parkId;
//        }
//
//        public void setParkId(String parkId) {
//            this.parkId = parkId;
//        }
//    }

































//    /**
//     * code : 200
//     * msg :
//     * response : {"shop":[{"nickname":"统一银座","sales":"20","starting":"20","photo":"park.hostop.net/static/image/0.jpg","cost":"10","mer_class":"超市",""id":"2},{"nickname":"彪1","sales":"10","starting":"20","photo":"park.hostop.net/static/image/0.jpg","cost":"10","mer_class":"外卖","id":"1"}],"MerchantClass":[{"name":"超市","photo":"park.hostop.net/static/image/0.jpg","id": "2"},{"name":"外卖","photo":"park.hostop.net/static/image/0.jpg","id": "1"}]}
//     * request : {"parkId":"1"}
//     * other : []
//     */
//
//    private int code;
//    private String msg;
//    private ResponseBean response;
//    private RequestBean request;
//    private List<?> other;
//
//    public int getCode() {
//        return code;
//    }
//
//    public void setCode(int code) {
//        this.code = code;
//    }
//
//    public String getMsg() {
//        return msg;
//    }
//
//    public void setMsg(String msg) {
//        this.msg = msg;
//    }
//
//    public ResponseBean getResponse() {
//        return response;
//    }
//
//    public void setResponse(ResponseBean response) {
//        this.response = response;
//    }
//
//    public RequestBean getRequest() {
//        return request;
//    }
//
//    public void setRequest(RequestBean request) {
//        this.request = request;
//    }
//
//    public List<?> getOther() {
//        return other;
//    }
//
//    public void setOther(List<?> other) {
//        this.other = other;
//    }
//
//    public static class ResponseBean {
//        private List<ShopBean> shop;
//        private List<MerchantClassBean> MerchantClass;
//
//        public List<ShopBean> getShop() {
//            return shop;
//        }
//
//        public void setShop(List<ShopBean> shop) {
//            this.shop = shop;
//        }
//
//        public List<MerchantClassBean> getMerchantClass() {
//            return MerchantClass;
//        }
//
//        public void setMerchantClass(List<MerchantClassBean> MerchantClass) {
//            this.MerchantClass = MerchantClass;
//        }
//
//        public static class ShopBean {
//            /**
//             * nickname : 统一银座
//             * sales : 20
//             * starting : 20
//             * photo : park.hostop.net/static/image/0.jpg
//             * cost : 10
//             * mer_class : 超市
//             * id : 2
//             */
//
//            private String nickname;
//            private String sales;
//            private String starting;
//            private String photo;
//            private String cost;
//            private String mer_class;
//            private String id;
//
//            public String getNickname() {
//                return nickname;
//            }
//
//            public void setNickname(String nickname) {
//                this.nickname = nickname;
//            }
//
//            public String getSales() {
//                return sales;
//            }
//
//            public void setSales(String sales) {
//                this.sales = sales;
//            }
//
//            public String getStarting() {
//                return starting;
//            }
//
//            public void setStarting(String starting) {
//                this.starting = starting;
//            }
//
//            public String getPhoto() {
//                return photo;
//            }
//
//            public void setPhoto(String photo) {
//                this.photo = photo;
//            }
//
//            public String getCost() {
//                return cost;
//            }
//
//            public void setCost(String cost) {
//                this.cost = cost;
//            }
//
//            public String getMer_class() {
//                return mer_class;
//            }
//
//            public void setMer_class(String mer_class) {
//                this.mer_class = mer_class;
//            }
//
//            public String getId() {
//                return id;
//            }
//
//            public void setId(String id) {
//                this.id = id;
//            }
//        }
//
//        public static class MerchantClassBean {
//            /**
//             * name : 超市
//             * photo : park.hostop.net/static/image/0.jpg
//             * id : 2
//             */
//
//            private String name;
//            private String photo;
//            private String id;
//
//            public String getId() {
//                return id;
//            }
//
//            public void setId(String id) {
//                this.id = id;
//            }
//
//            public String getName() {
//                return name;
//            }
//
//            public void setName(String name) {
//                this.name = name;
//            }
//
//            public String getPhoto() {
//                return photo;
//            }
//
//            public void setPhoto(String photo) {
//                this.photo = photo;
//            }
//        }
//    }
//
//    public static class RequestBean {
//        /**
//         * parkId : 1
//         */
//
//        private String parkId;
//
//        public String getParkId() {
//            return parkId;
//        }
//
//        public void setParkId(String parkId) {
//            this.parkId = parkId;
//        }
//    }
}
