package com.wanyangproject.entity;

import java.util.List;

/**
 * Created by 甜甜 on 2018/8/27.
 */

public class YuanQuShouCeXiangQingEntity {

    /**
     * code : 200
     * msg :
     * response : {"id":"3","title":"生活指南","content":"万洋集团自身定位中小微企业的服务商和众创平台的运营商，按照\u2018产业集聚、产城融合、资源共享、产融互动\u2019模式，专门打造承接企业转移的平台，解决中小微企业在发展过程中生产场地受限的瓶颈。项目结合温州当地产业基础，招商引进机械、汽摩配、时尚产业全产业链配套服务企业，打造产业集群，降低企业运营成本；智慧园区管理系统，实行智能刷卡门禁系统。科学利用大数据平台，为企业提供更人性化的\u2018保姆式服务\u2019，园区内配置了统一管理的员工集体宿舍、中央食堂和社区生活服务设施，让产业工人在厂门口就可享受完善的生活配套服务，解决企业招工难、留工难的问题。","add_time":"2018-07-27 22:27:38"}
     * request : {"id":"3"}
     * other : []
     */

    private int code;
    private String msg;
    private ResponseBean response;
    private RequestBean request;
    private List<?> other;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public ResponseBean getResponse() {
        return response;
    }

    public void setResponse(ResponseBean response) {
        this.response = response;
    }

    public RequestBean getRequest() {
        return request;
    }

    public void setRequest(RequestBean request) {
        this.request = request;
    }

    public List<?> getOther() {
        return other;
    }

    public void setOther(List<?> other) {
        this.other = other;
    }

    public static class ResponseBean {
        /**
         * id : 3
         * title : 生活指南
         * content : 万洋集团自身定位中小微企业的服务商和众创平台的运营商，按照‘产业集聚、产城融合、资源共享、产融互动’模式，专门打造承接企业转移的平台，解决中小微企业在发展过程中生产场地受限的瓶颈。项目结合温州当地产业基础，招商引进机械、汽摩配、时尚产业全产业链配套服务企业，打造产业集群，降低企业运营成本；智慧园区管理系统，实行智能刷卡门禁系统。科学利用大数据平台，为企业提供更人性化的‘保姆式服务’，园区内配置了统一管理的员工集体宿舍、中央食堂和社区生活服务设施，让产业工人在厂门口就可享受完善的生活配套服务，解决企业招工难、留工难的问题。
         * add_time : 2018-07-27 22:27:38
         */

        private String id;
        private String title;
        private String content;
        private String add_time;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getContent() {
            return content;
        }

        public void setContent(String content) {
            this.content = content;
        }

        public String getAdd_time() {
            return add_time;
        }

        public void setAdd_time(String add_time) {
            this.add_time = add_time;
        }
    }

    public static class RequestBean {
        /**
         * id : 3
         */

        private String id;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }
    }
}
