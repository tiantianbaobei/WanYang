package com.wanyangproject.entity;

import java.util.List;

/**
 * Created by 甜甜 on 2018/8/21.
 */

public class ShangJiaShangJiaGuanLiEntity {
    /**
     * code : 200
     * msg :
     * response : {"shop":{"id":"4","nickname":"统一银座","address":"地址3","baidu_address":"地址3","star_time":"07:30","end_time":"20:30","distribution":"07:30-20:30","phone":"15275412207","cost":"0","starting":"0","content":"<p>U7b80U4ecb<\/p>","sales":"26","photo":"park.hostop.net//upload/20180825/7d102fa5387124d0a053a7be5af34741.jpg","business":"1","longitude":"36.45515","latitude":"36.65","collection":"-1","service":"2","lat":"","lng":""}}
     * request : {"parkId":"1","phone":"17600904682"}
     * other : []
     */

    private int code;
    private String msg;
    private ResponseBean response;
    private RequestBean request;
    private List<?> other;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public ResponseBean getResponse() {
        return response;
    }

    public void setResponse(ResponseBean response) {
        this.response = response;
    }

    public RequestBean getRequest() {
        return request;
    }

    public void setRequest(RequestBean request) {
        this.request = request;
    }

    public List<?> getOther() {
        return other;
    }

    public void setOther(List<?> other) {
        this.other = other;
    }

    public static class ResponseBean {
        /**
         * shop : {"id":"4","nickname":"统一银座","address":"地址3","baidu_address":"地址3","star_time":"07:30","end_time":"20:30","distribution":"07:30-20:30","phone":"15275412207","cost":"0","starting":"0","content":"<p>U7b80U4ecb<\/p>","sales":"26","photo":"park.hostop.net//upload/20180825/7d102fa5387124d0a053a7be5af34741.jpg","business":"1","longitude":"36.45515","latitude":"36.65","collection":"-1","service":"2","lat":"","lng":""}
         */

        private ShopBean shop;

        public ShopBean getShop() {
            return shop;
        }

        public void setShop(ShopBean shop) {
            this.shop = shop;
        }

        public static class ShopBean {
            /**
             * id : 4
             * nickname : 统一银座
             * address : 地址3
             * baidu_address : 地址3
             * star_time : 07:30
             * end_time : 20:30
             * distribution : 07:30-20:30
             * phone : 15275412207
             * cost : 0
             * starting : 0
             * content : <p>U7b80U4ecb</p>
             * sales : 26
             * photo : park.hostop.net//upload/20180825/7d102fa5387124d0a053a7be5af34741.jpg
             * business : 1
             * longitude : 36.45515
             * latitude : 36.65
             * collection : -1
             * service : 2
             * lat :
             * lng :
             */

            private String id;
            private String nickname;
            private String address;
            private String baidu_address;
            private String star_time;
            private String end_time;
            private String distribution;
            private String phone;
            private String cost;
            private String starting;
            private String content;
            private String sales;
            private String photo;
            private String business;
            private String longitude;
            private String latitude;
            private String collection;
            private String service;
            private String lat;
            private String lng;

            public String getId() {
                return id;
            }

            public void setId(String id) {
                this.id = id;
            }

            public String getNickname() {
                return nickname;
            }

            public void setNickname(String nickname) {
                this.nickname = nickname;
            }

            public String getAddress() {
                return address;
            }

            public void setAddress(String address) {
                this.address = address;
            }

            public String getBaidu_address() {
                return baidu_address;
            }

            public void setBaidu_address(String baidu_address) {
                this.baidu_address = baidu_address;
            }

            public String getStar_time() {
                return star_time;
            }

            public void setStar_time(String star_time) {
                this.star_time = star_time;
            }

            public String getEnd_time() {
                return end_time;
            }

            public void setEnd_time(String end_time) {
                this.end_time = end_time;
            }

            public String getDistribution() {
                return distribution;
            }

            public void setDistribution(String distribution) {
                this.distribution = distribution;
            }

            public String getPhone() {
                return phone;
            }

            public void setPhone(String phone) {
                this.phone = phone;
            }

            public String getCost() {
                return cost;
            }

            public void setCost(String cost) {
                this.cost = cost;
            }

            public String getStarting() {
                return starting;
            }

            public void setStarting(String starting) {
                this.starting = starting;
            }

            public String getContent() {
                return content;
            }

            public void setContent(String content) {
                this.content = content;
            }

            public String getSales() {
                return sales;
            }

            public void setSales(String sales) {
                this.sales = sales;
            }

            public String getPhoto() {
                return photo;
            }

            public void setPhoto(String photo) {
                this.photo = photo;
            }

            public String getBusiness() {
                return business;
            }

            public void setBusiness(String business) {
                this.business = business;
            }

            public String getLongitude() {
                return longitude;
            }

            public void setLongitude(String longitude) {
                this.longitude = longitude;
            }

            public String getLatitude() {
                return latitude;
            }

            public void setLatitude(String latitude) {
                this.latitude = latitude;
            }

            public String getCollection() {
                return collection;
            }

            public void setCollection(String collection) {
                this.collection = collection;
            }

            public String getService() {
                return service;
            }

            public void setService(String service) {
                this.service = service;
            }

            public String getLat() {
                return lat;
            }

            public void setLat(String lat) {
                this.lat = lat;
            }

            public String getLng() {
                return lng;
            }

            public void setLng(String lng) {
                this.lng = lng;
            }
        }
    }

    public static class RequestBean {
        /**
         * parkId : 1
         * phone : 17600904682
         */

        private String parkId;
        private String phone;

        public String getParkId() {
            return parkId;
        }

        public void setParkId(String parkId) {
            this.parkId = parkId;
        }

        public String getPhone() {
            return phone;
        }

        public void setPhone(String phone) {
            this.phone = phone;
        }
    }


//    /**
//     * code : 200
//     * msg :
//     * response : {"shop":{"id":"4","nickname":"统一银座","address":"地址3","baidu_address":"地址3","star_time":"07:30","end_time":"20:30","distribution":"07:30-20:30","phone":"15275412207","cost":"0","starting":"0","content":"<p>U7b80U4ecb<\/p>","sales":"24","photo":"park.hostop.net//upload/20180825/7d102fa5387124d0a053a7be5af34741.jpg","business":"1","longitude":"36.45515","latitude":"36.65","collection":"-1","service":"3"}}
//     * request : {"parkId":"1","phone":"17600904682"}
//     * other : []
//     */
//
//    private int code;
//    private String msg;
//    private ResponseBean response;
//    private RequestBean request;
//    private List<?> other;
//
//    public int getCode() {
//        return code;
//    }
//
//    public void setCode(int code) {
//        this.code = code;
//    }
//
//    public String getMsg() {
//        return msg;
//    }
//
//    public void setMsg(String msg) {
//        this.msg = msg;
//    }
//
//    public ResponseBean getResponse() {
//        return response;
//    }
//
//    public void setResponse(ResponseBean response) {
//        this.response = response;
//    }
//
//    public RequestBean getRequest() {
//        return request;
//    }
//
//    public void setRequest(RequestBean request) {
//        this.request = request;
//    }
//
//    public List<?> getOther() {
//        return other;
//    }
//
//    public void setOther(List<?> other) {
//        this.other = other;
//    }
//
//    public static class ResponseBean {
//        /**
//         * shop : {"id":"4","nickname":"统一银座","address":"地址3","baidu_address":"地址3","star_time":"07:30","end_time":"20:30","distribution":"07:30-20:30","phone":"15275412207","cost":"0","starting":"0","content":"<p>U7b80U4ecb<\/p>","sales":"24","photo":"park.hostop.net//upload/20180825/7d102fa5387124d0a053a7be5af34741.jpg","business":"1","longitude":"36.45515","latitude":"36.65","collection":"-1","service":"3"}
//         */
//
//        private ShopBean shop;
//
//        public ShopBean getShop() {
//            return shop;
//        }
//
//        public void setShop(ShopBean shop) {
//            this.shop = shop;
//        }
//
//        public static class ShopBean {
//            /**
//             * id : 4
//             * nickname : 统一银座
//             * address : 地址3
//             * baidu_address : 地址3
//             * star_time : 07:30
//             * end_time : 20:30
//             * distribution : 07:30-20:30
//             * phone : 15275412207
//             * cost : 0
//             * starting : 0
//             * content : <p>U7b80U4ecb</p>
//             * sales : 24
//             * photo : park.hostop.net//upload/20180825/7d102fa5387124d0a053a7be5af34741.jpg
//             * business : 1
//             * longitude : 36.45515
//             * latitude : 36.65
//             * collection : -1
//             * service : 3
//             */
//
//            private String id;
//            private String nickname;
//            private String address;
//            private String baidu_address;
//            private String star_time;
//            private String end_time;
//            private String distribution;
//            private String phone;
//            private String cost;
//            private String starting;
//            private String content;
//            private String sales;
//            private String photo;
//            private String business;
//            private String longitude;
//            private String latitude;
//            private String collection;
//            private String service;
//
//            public String getId() {
//                return id;
//            }
//
//            public void setId(String id) {
//                this.id = id;
//            }
//
//            public String getNickname() {
//                return nickname;
//            }
//
//            public void setNickname(String nickname) {
//                this.nickname = nickname;
//            }
//
//            public String getAddress() {
//                return address;
//            }
//
//            public void setAddress(String address) {
//                this.address = address;
//            }
//
//            public String getBaidu_address() {
//                return baidu_address;
//            }
//
//            public void setBaidu_address(String baidu_address) {
//                this.baidu_address = baidu_address;
//            }
//
//            public String getStar_time() {
//                return star_time;
//            }
//
//            public void setStar_time(String star_time) {
//                this.star_time = star_time;
//            }
//
//            public String getEnd_time() {
//                return end_time;
//            }
//
//            public void setEnd_time(String end_time) {
//                this.end_time = end_time;
//            }
//
//            public String getDistribution() {
//                return distribution;
//            }
//
//            public void setDistribution(String distribution) {
//                this.distribution = distribution;
//            }
//
//            public String getPhone() {
//                return phone;
//            }
//
//            public void setPhone(String phone) {
//                this.phone = phone;
//            }
//
//            public String getCost() {
//                return cost;
//            }
//
//            public void setCost(String cost) {
//                this.cost = cost;
//            }
//
//            public String getStarting() {
//                return starting;
//            }
//
//            public void setStarting(String starting) {
//                this.starting = starting;
//            }
//
//            public String getContent() {
//                return content;
//            }
//
//            public void setContent(String content) {
//                this.content = content;
//            }
//
//            public String getSales() {
//                return sales;
//            }
//
//            public void setSales(String sales) {
//                this.sales = sales;
//            }
//
//            public String getPhoto() {
//                return photo;
//            }
//
//            public void setPhoto(String photo) {
//                this.photo = photo;
//            }
//
//            public String getBusiness() {
//                return business;
//            }
//
//            public void setBusiness(String business) {
//                this.business = business;
//            }
//
//            public String getLongitude() {
//                return longitude;
//            }
//
//            public void setLongitude(String longitude) {
//                this.longitude = longitude;
//            }
//
//            public String getLatitude() {
//                return latitude;
//            }
//
//            public void setLatitude(String latitude) {
//                this.latitude = latitude;
//            }
//
//            public String getCollection() {
//                return collection;
//            }
//
//            public void setCollection(String collection) {
//                this.collection = collection;
//            }
//
//            public String getService() {
//                return service;
//            }
//
//            public void setService(String service) {
//                this.service = service;
//            }
//        }
//    }
//
//    public static class RequestBean {
//        /**
//         * parkId : 1
//         * phone : 17600904682
//         */
//
//        private String parkId;
//        private String phone;
//
//        public String getParkId() {
//            return parkId;
//        }
//
//        public void setParkId(String parkId) {
//            this.parkId = parkId;
//        }
//
//        public String getPhone() {
//            return phone;
//        }
//
//        public void setPhone(String phone) {
//            this.phone = phone;
//        }
//    }































//    /**
//     * code : 200
//     * msg :
//     * response : {"shop":{"id":"4","nickname":"统一银座","address":"地址3","baidu_address":"地址3","star_time":"17:30","end_time":"18:28","distribution":"17:30-18:28","phone":"15275412207","cost":"0","starting":"0","content":"<p>U7b80U4ecb<\/p>","sales":"24","photo":"park.hostop.net//upload/20180825/7d102fa5387124d0a053a7be5af34741.jpg","business":"1","longitude":"117","latitude":"36.65","collection":"-1","service":"3"}}
//     * request : {"parkId":"1","phone":"17600904682"}
//     * other : []
//     */
//
//    private int code;
//    private String msg;
//    private ResponseBean response;
//    private RequestBean request;
//    private List<?> other;
//
//    public int getCode() {
//        return code;
//    }
//
//    public void setCode(int code) {
//        this.code = code;
//    }
//
//    public String getMsg() {
//        return msg;
//    }
//
//    public void setMsg(String msg) {
//        this.msg = msg;
//    }
//
//    public ResponseBean getResponse() {
//        return response;
//    }
//
//    public void setResponse(ResponseBean response) {
//        this.response = response;
//    }
//
//    public RequestBean getRequest() {
//        return request;
//    }
//
//    public void setRequest(RequestBean request) {
//        this.request = request;
//    }
//
//    public List<?> getOther() {
//        return other;
//    }
//
//    public void setOther(List<?> other) {
//        this.other = other;
//    }
//
//    public static class ResponseBean {
//        /**
//         * shop : {"id":"4","nickname":"统一银座","address":"地址3","baidu_address":"地址3","star_time":"17:30","end_time":"18:28","distribution":"17:30-18:28","phone":"15275412207","cost":"0","starting":"0","content":"<p>U7b80U4ecb<\/p>","sales":"24","photo":"park.hostop.net//upload/20180825/7d102fa5387124d0a053a7be5af34741.jpg","business":"1","longitude":"117","latitude":"36.65","collection":"-1","service":"3"}
//         */
//
//        private ShopBean shop;
//
//        public ShopBean getShop() {
//            return shop;
//        }
//
//        public void setShop(ShopBean shop) {
//            this.shop = shop;
//        }
//
//        public static class ShopBean {
//            /**
//             * id : 4
//             * nickname : 统一银座
//             * address : 地址3
//             * baidu_address : 地址3
//             * star_time : 17:30
//             * end_time : 18:28
//             * distribution : 17:30-18:28
//             * phone : 15275412207
//             * cost : 0
//             * starting : 0
//             * content : <p>U7b80U4ecb</p>
//             * sales : 24
//             * photo : park.hostop.net//upload/20180825/7d102fa5387124d0a053a7be5af34741.jpg
//             * business : 1
//             * longitude : 117
//             * latitude : 36.65
//             * collection : -1
//             * service : 3
//             */
//
//            private String id;
//            private String nickname;
//            private String address;
//            private String baidu_address;
//            private String star_time;
//            private String end_time;
//            private String distribution;
//            private String phone;
//            private String cost;
//            private String starting;
//            private String content;
//            private String sales;
//            private String photo;
//            private String business;
//            private String longitude;
//            private String latitude;
//            private String collection;
//            private String service;
//
//            public String getId() {
//                return id;
//            }
//
//            public void setId(String id) {
//                this.id = id;
//            }
//
//            public String getNickname() {
//                return nickname;
//            }
//
//            public void setNickname(String nickname) {
//                this.nickname = nickname;
//            }
//
//            public String getAddress() {
//                return address;
//            }
//
//            public void setAddress(String address) {
//                this.address = address;
//            }
//
//            public String getBaidu_address() {
//                return baidu_address;
//            }
//
//            public void setBaidu_address(String baidu_address) {
//                this.baidu_address = baidu_address;
//            }
//
//            public String getStar_time() {
//                return star_time;
//            }
//
//            public void setStar_time(String star_time) {
//                this.star_time = star_time;
//            }
//
//            public String getEnd_time() {
//                return end_time;
//            }
//
//            public void setEnd_time(String end_time) {
//                this.end_time = end_time;
//            }
//
//            public String getDistribution() {
//                return distribution;
//            }
//
//            public void setDistribution(String distribution) {
//                this.distribution = distribution;
//            }
//
//            public String getPhone() {
//                return phone;
//            }
//
//            public void setPhone(String phone) {
//                this.phone = phone;
//            }
//
//            public String getCost() {
//                return cost;
//            }
//
//            public void setCost(String cost) {
//                this.cost = cost;
//            }
//
//            public String getStarting() {
//                return starting;
//            }
//
//            public void setStarting(String starting) {
//                this.starting = starting;
//            }
//
//            public String getContent() {
//                return content;
//            }
//
//            public void setContent(String content) {
//                this.content = content;
//            }
//
//            public String getSales() {
//                return sales;
//            }
//
//            public void setSales(String sales) {
//                this.sales = sales;
//            }
//
//            public String getPhoto() {
//                return photo;
//            }
//
//            public void setPhoto(String photo) {
//                this.photo = photo;
//            }
//
//            public String getBusiness() {
//                return business;
//            }
//
//            public void setBusiness(String business) {
//                this.business = business;
//            }
//
//            public String getLongitude() {
//                return longitude;
//            }
//
//            public void setLongitude(String longitude) {
//                this.longitude = longitude;
//            }
//
//            public String getLatitude() {
//                return latitude;
//            }
//
//            public void setLatitude(String latitude) {
//                this.latitude = latitude;
//            }
//
//            public String getCollection() {
//                return collection;
//            }
//
//            public void setCollection(String collection) {
//                this.collection = collection;
//            }
//
//            public String getService() {
//                return service;
//            }
//
//            public void setService(String service) {
//                this.service = service;
//            }
//        }
//    }
//
//    public static class RequestBean {
//        /**
//         * parkId : 1
//         * phone : 17600904682
//         */
//
//        private String parkId;
//        private String phone;
//
//        public String getParkId() {
//            return parkId;
//        }
//
//        public void setParkId(String parkId) {
//            this.parkId = parkId;
//        }
//
//        public String getPhone() {
//            return phone;
//        }
//
//        public void setPhone(String phone) {
//            this.phone = phone;
//        }
//    }























//
//    /**
//     * code : 200
//     * msg :
//     * response : {"shop":{"id":"4","nickname":"店铺i名1","address":"店铺地址","baidu_address":"哈哈地址","star_time":"08:00","end_time":"20:00","distribution":"08:00-20:00","phone":"15896325665","cost":"","starting":"0.01","content":"测试","sales":"","photo":"park.hostop.net//upload/20180912/d8a4ca042bac56210a2969ded59fab7b.jpg","business":"2","longitude":"36.45515","latitude":"","collection":"1"}}
//     * request : {"parkId":"1","phone":"17600904682"}
//     * other : []
//     */
//
//    private int code;
//    private String msg;
//    private ResponseBean response;
//    private RequestBean request;
//    private List<?> other;
//
//    public int getCode() {
//        return code;
//    }
//
//    public void setCode(int code) {
//        this.code = code;
//    }
//
//    public String getMsg() {
//        return msg;
//    }
//
//    public void setMsg(String msg) {
//        this.msg = msg;
//    }
//
//    public ResponseBean getResponse() {
//        return response;
//    }
//
//    public void setResponse(ResponseBean response) {
//        this.response = response;
//    }
//
//    public RequestBean getRequest() {
//        return request;
//    }
//
//    public void setRequest(RequestBean request) {
//        this.request = request;
//    }
//
//    public List<?> getOther() {
//        return other;
//    }
//
//    public void setOther(List<?> other) {
//        this.other = other;
//    }
//
//    public static class ResponseBean {
//        /**
//         * shop : {"id":"4","nickname":"店铺i名1","address":"店铺地址","baidu_address":"哈哈地址","star_time":"08:00","end_time":"20:00","distribution":"08:00-20:00","phone":"15896325665","cost":"","starting":"0.01","content":"测试","sales":"","photo":"park.hostop.net//upload/20180912/d8a4ca042bac56210a2969ded59fab7b.jpg","business":"2","longitude":"36.45515","latitude":"","collection":"1"}
//         */
//
//        private ShopBean shop;
//
//        public ShopBean getShop() {
//            return shop;
//        }
//
//        public void setShop(ShopBean shop) {
//            this.shop = shop;
//        }
//
//        public static class ShopBean {
//            /**
//             * id : 4
//             * nickname : 店铺i名1
//             * address : 店铺地址
//             * baidu_address : 哈哈地址
//             * star_time : 08:00
//             * end_time : 20:00
//             * distribution : 08:00-20:00
//             * phone : 15896325665
//             * cost :
//             * starting : 0.01
//             * content : 测试
//             * sales :
//             * photo : park.hostop.net//upload/20180912/d8a4ca042bac56210a2969ded59fab7b.jpg
//             * business : 2
//             * longitude : 36.45515
//             * latitude :
//             * collection : 1
//             */
//
//            private String id;
//            private String nickname;
//            private String address;
//            private String baidu_address;
//            private String star_time;
//            private String end_time;
//            private String distribution;
//            private String phone;
//            private String cost;
//            private String starting;
//            private String content;
//            private String sales;
//            private String photo;
//            private String business;
//            private String longitude;
//            private String latitude;
//            private String collection;
//
//            public String getId() {
//                return id;
//            }
//
//            public void setId(String id) {
//                this.id = id;
//            }
//
//            public String getNickname() {
//                return nickname;
//            }
//
//            public void setNickname(String nickname) {
//                this.nickname = nickname;
//            }
//
//            public String getAddress() {
//                return address;
//            }
//
//            public void setAddress(String address) {
//                this.address = address;
//            }
//
//            public String getBaidu_address() {
//                return baidu_address;
//            }
//
//            public void setBaidu_address(String baidu_address) {
//                this.baidu_address = baidu_address;
//            }
//
//            public String getStar_time() {
//                return star_time;
//            }
//
//            public void setStar_time(String star_time) {
//                this.star_time = star_time;
//            }
//
//            public String getEnd_time() {
//                return end_time;
//            }
//
//            public void setEnd_time(String end_time) {
//                this.end_time = end_time;
//            }
//
//            public String getDistribution() {
//                return distribution;
//            }
//
//            public void setDistribution(String distribution) {
//                this.distribution = distribution;
//            }
//
//            public String getPhone() {
//                return phone;
//            }
//
//            public void setPhone(String phone) {
//                this.phone = phone;
//            }
//
//            public String getCost() {
//                return cost;
//            }
//
//            public void setCost(String cost) {
//                this.cost = cost;
//            }
//
//            public String getStarting() {
//                return starting;
//            }
//
//            public void setStarting(String starting) {
//                this.starting = starting;
//            }
//
//            public String getContent() {
//                return content;
//            }
//
//            public void setContent(String content) {
//                this.content = content;
//            }
//
//            public String getSales() {
//                return sales;
//            }
//
//            public void setSales(String sales) {
//                this.sales = sales;
//            }
//
//            public String getPhoto() {
//                return photo;
//            }
//
//            public void setPhoto(String photo) {
//                this.photo = photo;
//            }
//
//            public String getBusiness() {
//                return business;
//            }
//
//            public void setBusiness(String business) {
//                this.business = business;
//            }
//
//            public String getLongitude() {
//                return longitude;
//            }
//
//            public void setLongitude(String longitude) {
//                this.longitude = longitude;
//            }
//
//            public String getLatitude() {
//                return latitude;
//            }
//
//            public void setLatitude(String latitude) {
//                this.latitude = latitude;
//            }
//
//            public String getCollection() {
//                return collection;
//            }
//
//            public void setCollection(String collection) {
//                this.collection = collection;
//            }
//        }
//    }
//
//    public static class RequestBean {
//        /**
//         * parkId : 1
//         * phone : 17600904682
//         */
//
//        private String parkId;
//        private String phone;
//
//        public String getParkId() {
//            return parkId;
//        }
//
//        public void setParkId(String parkId) {
//            this.parkId = parkId;
//        }
//
//        public String getPhone() {
//            return phone;
//        }
//
//        public void setPhone(String phone) {
//            this.phone = phone;
//        }
//    }
}
