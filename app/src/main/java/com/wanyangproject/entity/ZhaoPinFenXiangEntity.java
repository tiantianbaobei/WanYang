package com.wanyangproject.entity;

import java.util.List;

/**
 * Created by 甜甜 on 2018/9/4.
 */

public class ZhaoPinFenXiangEntity {

    /**
     * code : 200
     * msg :
     * response : {"url":"http://park.hostop.net/index/index/sharejob/id/23.html","image":"http://park.hostop.net/static/image/logo.png","title":"【招聘】服务员"}
     * request : {"id":"23"}
     * other : []
     */

    private int code;
    private String msg;
    private ResponseBean response;
    private RequestBean request;
    private List<?> other;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public ResponseBean getResponse() {
        return response;
    }

    public void setResponse(ResponseBean response) {
        this.response = response;
    }

    public RequestBean getRequest() {
        return request;
    }

    public void setRequest(RequestBean request) {
        this.request = request;
    }

    public List<?> getOther() {
        return other;
    }

    public void setOther(List<?> other) {
        this.other = other;
    }

    public static class ResponseBean {
        /**
         * url : http://park.hostop.net/index/index/sharejob/id/23.html
         * image : http://park.hostop.net/static/image/logo.png
         * title : 【招聘】服务员
         */

        private String url;
        private String image;
        private String title;

        public String getUrl() {
            return url;
        }

        public void setUrl(String url) {
            this.url = url;
        }

        public String getImage() {
            return image;
        }

        public void setImage(String image) {
            this.image = image;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }
    }

    public static class RequestBean {
        /**
         * id : 23
         */

        private String id;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }
    }
}
