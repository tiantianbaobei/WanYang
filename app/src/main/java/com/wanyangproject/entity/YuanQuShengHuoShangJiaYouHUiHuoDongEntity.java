package com.wanyangproject.entity;

import java.util.List;

/**
 * Created by 甜甜 on 2018/10/11.
 */

public class YuanQuShengHuoShangJiaYouHUiHuoDongEntity {

    /**
     * code : 200
     * msg :
     * response : [{"id":"37","content":"园区商家可享有的专属福利","title":"优惠政策来啦"}]
     * request : []
     * other : []
     */

    private int code;
    private String msg;
    private List<ResponseBean> response;
    private List<?> request;
    private List<?> other;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public List<ResponseBean> getResponse() {
        return response;
    }

    public void setResponse(List<ResponseBean> response) {
        this.response = response;
    }

    public List<?> getRequest() {
        return request;
    }

    public void setRequest(List<?> request) {
        this.request = request;
    }

    public List<?> getOther() {
        return other;
    }

    public void setOther(List<?> other) {
        this.other = other;
    }

    public static class ResponseBean {
        /**
         * id : 37
         * content : 园区商家可享有的专属福利
         * title : 优惠政策来啦
         */

        private String id;
        private String content;
        private String title;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getContent() {
            return content;
        }

        public void setContent(String content) {
            this.content = content;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }
    }
}
