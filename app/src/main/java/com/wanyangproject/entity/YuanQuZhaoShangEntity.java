package com.wanyangproject.entity;

import java.util.List;

/**
 * Created by 甜甜 on 2018/8/19.
 */

public class YuanQuZhaoShangEntity {
    /**
     * code : 200
     * msg :
     * response : {"upTjArr":[{"id":"11","title":"政法新规","image":"http://park.hostop.net/upload/20180910/376f9622b48b03193133a9257a7df4c7.jpg","add_time":"15天前","hrefType":"4","content":""},{"id":"10","title":"","image":"http://park.hostop.net/upload/20180910/a5a6a7568848cceab1772290f8c4c95e.jpg","add_time":"15天前","hrefType":"2","content":""},{"id":"9","title":"","image":"http://park.hostop.net/upload/20180906/1f5b2c9ae44040f2c49a083fd32a517f.jpg","add_time":"15天前","hrefType":"0","content":""},{"id":"1","title":"","image":"http://park.hostop.net/upload/20180908/766c7d1ee6700e30614a32c4296c5cbb.jpg","add_time":"15天前","hrefType":"0","content":""}],"bannerArr":[{"id":"15","title":"","image":"http://park.hostop.net/upload/20180910/647754cf1a336cc1aacdef142f0d852a.jpg","add_time":"12天前","hrefType":"9","content":""},{"id":"12","title":"","image":"http://park.hostop.net/upload/20180910/b8e0185c9eae88a100e728c249f1410b.jpg","add_time":"12天前","hrefType":"4","content":""}],"wordArr":[{"id":"14","title":"测试跳转","logo":"","imgArr":[],"desc":"跳转...","add_time":"16小时前"}],"category":[{"id":"27","title":"","logo":"http://park.hostop.net","add_time":"3天前"},{"id":"19","title":"政府非招标采购管理","logo":"http://park.hostop.net/upload/20180910/7885af9447757dd1a740e1024aa6f300.jpg","add_time":"12天前"},{"id":"18","title":"社会救助暂行办法","logo":"http://park.hostop.net/upload/20180910/235301500e050f394c70fb590abde155.jpg","add_time":"12天前"},{"id":"16","title":"济南网点","logo":"http://park.hostop.net/upload/20180910/070291e48ee39343dea1f2809b0389c4.jpg","add_time":"12天前"}],"info":{"content":""},"tjIndex":[],"background":"http://park.hostop.net/upload/20180912/3050ab85e0ee90f57fc44f655599e0b4.jpg"}
     * request : {"parkId":"1","typeId":"3"}
     * other : []
     */

    private int code;
    private String msg;
    private ResponseBean response;
    private RequestBean request;
    private List<?> other;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public ResponseBean getResponse() {
        return response;
    }

    public void setResponse(ResponseBean response) {
        this.response = response;
    }

    public RequestBean getRequest() {
        return request;
    }

    public void setRequest(RequestBean request) {
        this.request = request;
    }

    public List<?> getOther() {
        return other;
    }

    public void setOther(List<?> other) {
        this.other = other;
    }

    public static class ResponseBean {
        /**
         * upTjArr : [{"id":"11","title":"政法新规","image":"http://park.hostop.net/upload/20180910/376f9622b48b03193133a9257a7df4c7.jpg","add_time":"15天前","hrefType":"4","content":""},{"id":"10","title":"","image":"http://park.hostop.net/upload/20180910/a5a6a7568848cceab1772290f8c4c95e.jpg","add_time":"15天前","hrefType":"2","content":""},{"id":"9","title":"","image":"http://park.hostop.net/upload/20180906/1f5b2c9ae44040f2c49a083fd32a517f.jpg","add_time":"15天前","hrefType":"0","content":""},{"id":"1","title":"","image":"http://park.hostop.net/upload/20180908/766c7d1ee6700e30614a32c4296c5cbb.jpg","add_time":"15天前","hrefType":"0","content":""}]
         * bannerArr : [{"id":"15","title":"","image":"http://park.hostop.net/upload/20180910/647754cf1a336cc1aacdef142f0d852a.jpg","add_time":"12天前","hrefType":"9","content":""},{"id":"12","title":"","image":"http://park.hostop.net/upload/20180910/b8e0185c9eae88a100e728c249f1410b.jpg","add_time":"12天前","hrefType":"4","content":""}]
         * wordArr : [{"id":"14","title":"测试跳转","logo":"","imgArr":[],"desc":"跳转...","add_time":"16小时前"}]
         * category : [{"id":"27","title":"","logo":"http://park.hostop.net","add_time":"3天前"},{"id":"19","title":"政府非招标采购管理","logo":"http://park.hostop.net/upload/20180910/7885af9447757dd1a740e1024aa6f300.jpg","add_time":"12天前"},{"id":"18","title":"社会救助暂行办法","logo":"http://park.hostop.net/upload/20180910/235301500e050f394c70fb590abde155.jpg","add_time":"12天前"},{"id":"16","title":"济南网点","logo":"http://park.hostop.net/upload/20180910/070291e48ee39343dea1f2809b0389c4.jpg","add_time":"12天前"}]
         * info : {"content":""}
         * tjIndex : []
         * background : http://park.hostop.net/upload/20180912/3050ab85e0ee90f57fc44f655599e0b4.jpg
         */

        private InfoBean info;
        private String background;
        private List<UpTjArrBean> upTjArr;
        private List<BannerArrBean> bannerArr;
        private List<WordArrBean> wordArr;
        private List<CategoryBean> category;
        private List<?> tjIndex;

        public InfoBean getInfo() {
            return info;
        }

        public void setInfo(InfoBean info) {
            this.info = info;
        }

        public String getBackground() {
            return background;
        }

        public void setBackground(String background) {
            this.background = background;
        }

        public List<UpTjArrBean> getUpTjArr() {
            return upTjArr;
        }

        public void setUpTjArr(List<UpTjArrBean> upTjArr) {
            this.upTjArr = upTjArr;
        }

        public List<BannerArrBean> getBannerArr() {
            return bannerArr;
        }

        public void setBannerArr(List<BannerArrBean> bannerArr) {
            this.bannerArr = bannerArr;
        }

        public List<WordArrBean> getWordArr() {
            return wordArr;
        }

        public void setWordArr(List<WordArrBean> wordArr) {
            this.wordArr = wordArr;
        }

        public List<CategoryBean> getCategory() {
            return category;
        }

        public void setCategory(List<CategoryBean> category) {
            this.category = category;
        }

        public List<?> getTjIndex() {
            return tjIndex;
        }

        public void setTjIndex(List<?> tjIndex) {
            this.tjIndex = tjIndex;
        }

        public static class InfoBean {
            /**
             * content :
             */

            private String content;

            public String getContent() {
                return content;
            }

            public void setContent(String content) {
                this.content = content;
            }
        }

        public static class UpTjArrBean {
            /**
             * id : 11
             * title : 政法新规
             * image : http://park.hostop.net/upload/20180910/376f9622b48b03193133a9257a7df4c7.jpg
             * add_time : 15天前
             * hrefType : 4
             * content :
             */

            private String id;
            private String title;
            private String image;
            private String add_time;
            private String hrefType;
            private String content;

            public String getId() {
                return id;
            }

            public void setId(String id) {
                this.id = id;
            }

            public String getTitle() {
                return title;
            }

            public void setTitle(String title) {
                this.title = title;
            }

            public String getImage() {
                return image;
            }

            public void setImage(String image) {
                this.image = image;
            }

            public String getAdd_time() {
                return add_time;
            }

            public void setAdd_time(String add_time) {
                this.add_time = add_time;
            }

            public String getHrefType() {
                return hrefType;
            }

            public void setHrefType(String hrefType) {
                this.hrefType = hrefType;
            }

            public String getContent() {
                return content;
            }

            public void setContent(String content) {
                this.content = content;
            }
        }

        public static class BannerArrBean {
            /**
             * id : 15
             * title :
             * image : http://park.hostop.net/upload/20180910/647754cf1a336cc1aacdef142f0d852a.jpg
             * add_time : 12天前
             * hrefType : 9
             * content :
             */

            private String id;
            private String title;
            private String image;
            private String add_time;
            private String hrefType;
            private String content;

            public String getId() {
                return id;
            }

            public void setId(String id) {
                this.id = id;
            }

            public String getTitle() {
                return title;
            }

            public void setTitle(String title) {
                this.title = title;
            }

            public String getImage() {
                return image;
            }

            public void setImage(String image) {
                this.image = image;
            }

            public String getAdd_time() {
                return add_time;
            }

            public void setAdd_time(String add_time) {
                this.add_time = add_time;
            }

            public String getHrefType() {
                return hrefType;
            }

            public void setHrefType(String hrefType) {
                this.hrefType = hrefType;
            }

            public String getContent() {
                return content;
            }

            public void setContent(String content) {
                this.content = content;
            }
        }

        public static class WordArrBean {
            /**
             * id : 14
             * title : 测试跳转
             * logo :
             * imgArr : []
             * desc : 跳转...
             * add_time : 16小时前
             */

            private String id;
            private String title;
            private String logo;
            private String desc;
            private String add_time;
            private List<?> imgArr;

            public String getId() {
                return id;
            }

            public void setId(String id) {
                this.id = id;
            }

            public String getTitle() {
                return title;
            }

            public void setTitle(String title) {
                this.title = title;
            }

            public String getLogo() {
                return logo;
            }

            public void setLogo(String logo) {
                this.logo = logo;
            }

            public String getDesc() {
                return desc;
            }

            public void setDesc(String desc) {
                this.desc = desc;
            }

            public String getAdd_time() {
                return add_time;
            }

            public void setAdd_time(String add_time) {
                this.add_time = add_time;
            }

            public List<?> getImgArr() {
                return imgArr;
            }

            public void setImgArr(List<?> imgArr) {
                this.imgArr = imgArr;
            }
        }

        public static class CategoryBean {
            /**
             * id : 27
             * title :
             * logo : http://park.hostop.net
             * add_time : 3天前
             */

            private String id;
            private String title;
            private String logo;
            private String add_time;

            public String getId() {
                return id;
            }

            public void setId(String id) {
                this.id = id;
            }

            public String getTitle() {
                return title;
            }

            public void setTitle(String title) {
                this.title = title;
            }

            public String getLogo() {
                return logo;
            }

            public void setLogo(String logo) {
                this.logo = logo;
            }

            public String getAdd_time() {
                return add_time;
            }

            public void setAdd_time(String add_time) {
                this.add_time = add_time;
            }
        }
    }

    public static class RequestBean {
        /**
         * parkId : 1
         * typeId : 3
         */

        private String parkId;
        private String typeId;

        public String getParkId() {
            return parkId;
        }

        public void setParkId(String parkId) {
            this.parkId = parkId;
        }

        public String getTypeId() {
            return typeId;
        }

        public void setTypeId(String typeId) {
            this.typeId = typeId;
        }
    }


//    /**
//     * code : 200
//     * msg :
//     * response : {"upTjArr":[{"id":"12","title":"园区新闻","image":"http://park.hostop.net/upload/20180916/a0c21d980d73a762952f54a2da814f31.jpg","add_time":"15天前","hrefType":"9","content":"<p><span style=\"text-align: justify;\">8月10日，湖北省襄阳市考察团在湖北省委常委、襄阳市市委书记李乐成带领下考察了万洋集团，考察交流小微企业园建设运营情况。万洋集团总裁吴建明接待了考察团。<\/span><\/p><p><span style=\"text-align: justify;\"><img src=\"http://park.hostop.net/upload/20180914/647b5d5d293e01bb9e80571427e7ddcb.jpg\" alt=\"undefined\"><br><\/span><\/p><p style=\"text-align: justify;\"><span>考察团在万洋集团总部召开座谈会，吴建明向李乐成一行详细介绍了万洋集团的情况以及万洋众创城建设运营模式。<\/span><\/p><p style=\"text-align: justify;\"><span>万洋集团按照\u201c产业集聚、产城融合、资源共享、产融互动\u201d的模式，已在长三角、珠三角及国内其他区域重点城市开发了17个制造业集聚平台，面积超2500万平方米<\/span><\/p><p style=\"text-align: justify;\"><span><img src=\"http://park.hostop.net/upload/20180914/575b0734f46f287565655a356a42bf93.jpg\" alt=\"undefined\"><br><\/span><\/p><p style=\"text-align: justify;\"><span>针对企业升级转型资金压力大的问题，万洋集团创造性地提出了\u201c产融互动\u201d的运营模式，搭建了中小微企业与银行金融机构实现合作的平台，引入厂房按揭模式，降低企业资金压力，集聚批量客户，享受集团信用等级，提供全周期全供应量金融服务，有效解决企业\u201c融资难、融资贵\u201d问题，倍受各方的欢迎和赞誉。另外，厂房实现按需定制、产权分割，园区实现智能化管理，形成企业生产大数据和金融大数据，彻底解决银行对中小微企业放款的顾虑，可以放心为企业提供相对应的金融配套方案。<\/span><\/p><p><span style=\"text-align: justify;\"><br><\/span><\/p><p><span style=\"text-align: justify;\"><br><\/span><\/p>"},{"id":"4","title":"论坛社区","image":"http://park.hostop.net/upload/20180912/523d545aab340708624ff4d57f1c678c.jpg","add_time":"15天前","hrefType":"6","content":""},{"id":"3","title":"园区资讯","image":"http://park.hostop.net/upload/20180906/3c20de4965161f35609ccdaf1fbee65f.jpg","add_time":"15天前","hrefType":"9","content":"<p><span style=\"text-align: justify;\">为加强抗菌药物管理，遏制细菌耐药，维护人民群众健康，促进经济社会协调发展，制定本行动计划。<span lang=\"EN-US\">&nbsp;<br><\/span><\/span><span style=\"text-align: justify;\">　　一、前言<\/span><span style=\"text-align: justify;\" lang=\"EN-US\">&nbsp;<\/span><span style=\"text-align: justify;\" lang=\"EN-US\"><br><\/span><span style=\"text-align: justify;\">　　我国是抗菌药物的生产和使用大国。抗菌药物广泛应用于医疗卫生、农业养殖领域，在治疗感染性疾病挽救患者生命、防治动物疫病提高养殖效益以及保障公共卫生安全中，发挥了重要作用。但是，由于新型抗菌药物研发能力不足、药店无处方销售抗菌药物、医疗和养殖领域不合理应用抗菌药物、制药企业废弃物排放不达标、群众合理用药意识不高等多种因素，细菌耐药问题日益突出。细菌耐药最终影响人类健康，但造成细菌耐药的因素及其后果却超越了卫生领域，给人类社会带来了生物安全威胁加大、环境污染加剧、经济发展制约等不利影响，迫切需要加强多部门多领域协同谋划、共同应对。<span lang=\"EN-US\">&nbsp;<br><\/span><\/span><span style=\"text-align: justify;\">　　二、工作目标<span lang=\"EN-US\">&nbsp;<\/span><\/span><span style=\"text-align: justify;\" lang=\"EN-US\"><br><\/span><span style=\"text-align: justify;\">　　从国家层面实施综合治理策略和措施，对抗菌药物的研发、生产、流通、应用、环境保护等各个环节加强监管，加强宣传教育和国际交流合作，应对细菌耐药带来的风险挑战。到<span lang=\"EN-US\">2020<\/span>年：<span lang=\"EN-US\">&nbsp;<br><\/span>　　（一）争取研发上市全新抗菌药物<span lang=\"EN-US\">1-2<\/span>个，新型诊断仪器设备和试剂<span lang=\"EN-US\">5-10<\/span>项。<span lang=\"EN-US\">&nbsp;<br><\/span>　　（二）零售药店凭处方销售抗菌药物的比例基本达到全覆盖。省（区、市）凭兽医处方销售抗菌药物的比例达到<span lang=\"EN-US\">50%<\/span>。<span lang=\"EN-US\">&nbsp;<br><\/span>　　（三）健全医疗机构、动物源抗菌药物应用和细菌耐药监测网络；建设细菌耐药参比实验室和菌种中心；建立医疗、养殖领域的抗菌药物应用和细菌耐药控制评价体系。<span lang=\"EN-US\">&nbsp;<br><\/span>　　（四）全国二级以上医院基本建立抗菌药物临床应用管理机制；医疗机构主要耐药菌增长率得到有效控制。<span lang=\"EN-US\">&nbsp;<\/span><\/span><\/p>"},{"id":"2","title":"动态信息","image":"http://park.hostop.net/upload/20180906/4d3a4db8e349a4ec7a096ade2d2aea81.jpg","add_time":"15天前","hrefType":"0","content":""}],"bannerArr":[{"id":"16","title":"","image":"http://park.hostop.net/upload/20180915/b5ebe97b7d97d8d7cd0148ebefc564f9.png","add_time":"12天前","hrefType":"11","content":""},{"id":"13","title":"","image":"http://park.hostop.net/upload/20180915/df0cb8b2ab84af7ef8919700377ea3c0.jpg","add_time":"12天前","hrefType":"1","content":""},{"id":"1","title":"","image":"http://park.hostop.net/upload/20180906/3c20de4965161f35609ccdaf1fbee65f.jpg","add_time":"15天前","hrefType":"7","content":""}],"wordArr":[{"id":"16","title":"测试","logo":"","imgArr":[],"desc":"123456...","add_time":"5小时前"},{"id":"15","title":"园区招商分类资讯","logo":"","imgArr":[],"desc":"测试...","add_time":"7小时前"}],"category":[{"id":"23","title":"分类四","logo":"http://park.hostop.net/upload/20180914/abd4f332dbe6976038efa199f7ea3eba.png","add_time":"10天前"},{"id":"22","title":"分类三","logo":"http://park.hostop.net/upload/20180914/d3b25586913575f015857fd52ed061b3.jpg","add_time":"10天前"},{"id":"20","title":"企业招商","logo":"http://park.hostop.net/upload/20180910/4af0bc448911755bf02a98159c2ee762.jpg","add_time":"11天前"},{"id":"3","title":"招商最新资讯","logo":"http://park.hostop.net/upload/20180902/b466c66a0cd9ac086f77046a2da800c9.jpg","add_time":"19天前"}],"info":{"content":""},"tjIndex":[],"background":"http://park.hostop.net/upload/20180915/7749a7fcfadf7fe37b3a4d2f430a5498.png"}
//     * request : {"parkId":"1","typeId":"1"}
//     * other : []
//     */
//
//    private int code;
//    private String msg;
//    private ResponseBean response;
//    private RequestBean request;
//    private List<?> other;
//
//    public int getCode() {
//        return code;
//    }
//
//    public void setCode(int code) {
//        this.code = code;
//    }
//
//    public String getMsg() {
//        return msg;
//    }
//
//    public void setMsg(String msg) {
//        this.msg = msg;
//    }
//
//    public ResponseBean getResponse() {
//        return response;
//    }
//
//    public void setResponse(ResponseBean response) {
//        this.response = response;
//    }
//
//    public RequestBean getRequest() {
//        return request;
//    }
//
//    public void setRequest(RequestBean request) {
//        this.request = request;
//    }
//
//    public List<?> getOther() {
//        return other;
//    }
//
//    public void setOther(List<?> other) {
//        this.other = other;
//    }
//
//    public static class ResponseBean {
//        /**
//         * upTjArr : [{"id":"12","title":"园区新闻","image":"http://park.hostop.net/upload/20180916/a0c21d980d73a762952f54a2da814f31.jpg","add_time":"15天前","hrefType":"9","content":"<p><span style=\"text-align: justify;\">8月10日，湖北省襄阳市考察团在湖北省委常委、襄阳市市委书记李乐成带领下考察了万洋集团，考察交流小微企业园建设运营情况。万洋集团总裁吴建明接待了考察团。<\/span><\/p><p><span style=\"text-align: justify;\"><img src=\"http://park.hostop.net/upload/20180914/647b5d5d293e01bb9e80571427e7ddcb.jpg\" alt=\"undefined\"><br><\/span><\/p><p style=\"text-align: justify;\"><span>考察团在万洋集团总部召开座谈会，吴建明向李乐成一行详细介绍了万洋集团的情况以及万洋众创城建设运营模式。<\/span><\/p><p style=\"text-align: justify;\"><span>万洋集团按照\u201c产业集聚、产城融合、资源共享、产融互动\u201d的模式，已在长三角、珠三角及国内其他区域重点城市开发了17个制造业集聚平台，面积超2500万平方米<\/span><\/p><p style=\"text-align: justify;\"><span><img src=\"http://park.hostop.net/upload/20180914/575b0734f46f287565655a356a42bf93.jpg\" alt=\"undefined\"><br><\/span><\/p><p style=\"text-align: justify;\"><span>针对企业升级转型资金压力大的问题，万洋集团创造性地提出了\u201c产融互动\u201d的运营模式，搭建了中小微企业与银行金融机构实现合作的平台，引入厂房按揭模式，降低企业资金压力，集聚批量客户，享受集团信用等级，提供全周期全供应量金融服务，有效解决企业\u201c融资难、融资贵\u201d问题，倍受各方的欢迎和赞誉。另外，厂房实现按需定制、产权分割，园区实现智能化管理，形成企业生产大数据和金融大数据，彻底解决银行对中小微企业放款的顾虑，可以放心为企业提供相对应的金融配套方案。<\/span><\/p><p><span style=\"text-align: justify;\"><br><\/span><\/p><p><span style=\"text-align: justify;\"><br><\/span><\/p>"},{"id":"4","title":"论坛社区","image":"http://park.hostop.net/upload/20180912/523d545aab340708624ff4d57f1c678c.jpg","add_time":"15天前","hrefType":"6","content":""},{"id":"3","title":"园区资讯","image":"http://park.hostop.net/upload/20180906/3c20de4965161f35609ccdaf1fbee65f.jpg","add_time":"15天前","hrefType":"9","content":"<p><span style=\"text-align: justify;\">为加强抗菌药物管理，遏制细菌耐药，维护人民群众健康，促进经济社会协调发展，制定本行动计划。<span lang=\"EN-US\">&nbsp;<br><\/span><\/span><span style=\"text-align: justify;\">　　一、前言<\/span><span style=\"text-align: justify;\" lang=\"EN-US\">&nbsp;<\/span><span style=\"text-align: justify;\" lang=\"EN-US\"><br><\/span><span style=\"text-align: justify;\">　　我国是抗菌药物的生产和使用大国。抗菌药物广泛应用于医疗卫生、农业养殖领域，在治疗感染性疾病挽救患者生命、防治动物疫病提高养殖效益以及保障公共卫生安全中，发挥了重要作用。但是，由于新型抗菌药物研发能力不足、药店无处方销售抗菌药物、医疗和养殖领域不合理应用抗菌药物、制药企业废弃物排放不达标、群众合理用药意识不高等多种因素，细菌耐药问题日益突出。细菌耐药最终影响人类健康，但造成细菌耐药的因素及其后果却超越了卫生领域，给人类社会带来了生物安全威胁加大、环境污染加剧、经济发展制约等不利影响，迫切需要加强多部门多领域协同谋划、共同应对。<span lang=\"EN-US\">&nbsp;<br><\/span><\/span><span style=\"text-align: justify;\">　　二、工作目标<span lang=\"EN-US\">&nbsp;<\/span><\/span><span style=\"text-align: justify;\" lang=\"EN-US\"><br><\/span><span style=\"text-align: justify;\">　　从国家层面实施综合治理策略和措施，对抗菌药物的研发、生产、流通、应用、环境保护等各个环节加强监管，加强宣传教育和国际交流合作，应对细菌耐药带来的风险挑战。到<span lang=\"EN-US\">2020<\/span>年：<span lang=\"EN-US\">&nbsp;<br><\/span>　　（一）争取研发上市全新抗菌药物<span lang=\"EN-US\">1-2<\/span>个，新型诊断仪器设备和试剂<span lang=\"EN-US\">5-10<\/span>项。<span lang=\"EN-US\">&nbsp;<br><\/span>　　（二）零售药店凭处方销售抗菌药物的比例基本达到全覆盖。省（区、市）凭兽医处方销售抗菌药物的比例达到<span lang=\"EN-US\">50%<\/span>。<span lang=\"EN-US\">&nbsp;<br><\/span>　　（三）健全医疗机构、动物源抗菌药物应用和细菌耐药监测网络；建设细菌耐药参比实验室和菌种中心；建立医疗、养殖领域的抗菌药物应用和细菌耐药控制评价体系。<span lang=\"EN-US\">&nbsp;<br><\/span>　　（四）全国二级以上医院基本建立抗菌药物临床应用管理机制；医疗机构主要耐药菌增长率得到有效控制。<span lang=\"EN-US\">&nbsp;<\/span><\/span><\/p>"},{"id":"2","title":"动态信息","image":"http://park.hostop.net/upload/20180906/4d3a4db8e349a4ec7a096ade2d2aea81.jpg","add_time":"15天前","hrefType":"0","content":""}]
//         * bannerArr : [{"id":"16","title":"","image":"http://park.hostop.net/upload/20180915/b5ebe97b7d97d8d7cd0148ebefc564f9.png","add_time":"12天前","hrefType":"11","content":""},{"id":"13","title":"","image":"http://park.hostop.net/upload/20180915/df0cb8b2ab84af7ef8919700377ea3c0.jpg","add_time":"12天前","hrefType":"1","content":""},{"id":"1","title":"","image":"http://park.hostop.net/upload/20180906/3c20de4965161f35609ccdaf1fbee65f.jpg","add_time":"15天前","hrefType":"7","content":""}]
//         * wordArr : [{"id":"16","title":"测试","logo":"","imgArr":[],"desc":"123456...","add_time":"5小时前"},{"id":"15","title":"园区招商分类资讯","logo":"","imgArr":[],"desc":"测试...","add_time":"7小时前"}]
//         * category : [{"id":"23","title":"分类四","logo":"http://park.hostop.net/upload/20180914/abd4f332dbe6976038efa199f7ea3eba.png","add_time":"10天前"},{"id":"22","title":"分类三","logo":"http://park.hostop.net/upload/20180914/d3b25586913575f015857fd52ed061b3.jpg","add_time":"10天前"},{"id":"20","title":"企业招商","logo":"http://park.hostop.net/upload/20180910/4af0bc448911755bf02a98159c2ee762.jpg","add_time":"11天前"},{"id":"3","title":"招商最新资讯","logo":"http://park.hostop.net/upload/20180902/b466c66a0cd9ac086f77046a2da800c9.jpg","add_time":"19天前"}]
//         * info : {"content":""}
//         * tjIndex : []
//         * background : http://park.hostop.net/upload/20180915/7749a7fcfadf7fe37b3a4d2f430a5498.png
//         */
//
//        private InfoBean info;
//        private String background;
//        private List<UpTjArrBean> upTjArr;
//        private List<BannerArrBean> bannerArr;
//        private List<WordArrBean> wordArr;
//        private List<CategoryBean> category;
//        private List<?> tjIndex;
//
//        public InfoBean getInfo() {
//            return info;
//        }
//
//        public void setInfo(InfoBean info) {
//            this.info = info;
//        }
//
//        public String getBackground() {
//            return background;
//        }
//
//        public void setBackground(String background) {
//            this.background = background;
//        }
//
//        public List<UpTjArrBean> getUpTjArr() {
//            return upTjArr;
//        }
//
//        public void setUpTjArr(List<UpTjArrBean> upTjArr) {
//            this.upTjArr = upTjArr;
//        }
//
//        public List<BannerArrBean> getBannerArr() {
//            return bannerArr;
//        }
//
//        public void setBannerArr(List<BannerArrBean> bannerArr) {
//            this.bannerArr = bannerArr;
//        }
//
//        public List<WordArrBean> getWordArr() {
//            return wordArr;
//        }
//
//        public void setWordArr(List<WordArrBean> wordArr) {
//            this.wordArr = wordArr;
//        }
//
//        public List<CategoryBean> getCategory() {
//            return category;
//        }
//
//        public void setCategory(List<CategoryBean> category) {
//            this.category = category;
//        }
//
//        public List<?> getTjIndex() {
//            return tjIndex;
//        }
//
//        public void setTjIndex(List<?> tjIndex) {
//            this.tjIndex = tjIndex;
//        }
//
//        public static class InfoBean {
//            /**
//             * content :
//             */
//
//            private String content;
//
//            public String getContent() {
//                return content;
//            }
//
//            public void setContent(String content) {
//                this.content = content;
//            }
//        }
//
//        public static class UpTjArrBean {
//            /**
//             * id : 12
//             * title : 园区新闻
//             * image : http://park.hostop.net/upload/20180916/a0c21d980d73a762952f54a2da814f31.jpg
//             * add_time : 15天前
//             * hrefType : 9
//             * content : <p><span style="text-align: justify;">8月10日，湖北省襄阳市考察团在湖北省委常委、襄阳市市委书记李乐成带领下考察了万洋集团，考察交流小微企业园建设运营情况。万洋集团总裁吴建明接待了考察团。</span></p><p><span style="text-align: justify;"><img src="http://park.hostop.net/upload/20180914/647b5d5d293e01bb9e80571427e7ddcb.jpg" alt="undefined"><br></span></p><p style="text-align: justify;"><span>考察团在万洋集团总部召开座谈会，吴建明向李乐成一行详细介绍了万洋集团的情况以及万洋众创城建设运营模式。</span></p><p style="text-align: justify;"><span>万洋集团按照“产业集聚、产城融合、资源共享、产融互动”的模式，已在长三角、珠三角及国内其他区域重点城市开发了17个制造业集聚平台，面积超2500万平方米</span></p><p style="text-align: justify;"><span><img src="http://park.hostop.net/upload/20180914/575b0734f46f287565655a356a42bf93.jpg" alt="undefined"><br></span></p><p style="text-align: justify;"><span>针对企业升级转型资金压力大的问题，万洋集团创造性地提出了“产融互动”的运营模式，搭建了中小微企业与银行金融机构实现合作的平台，引入厂房按揭模式，降低企业资金压力，集聚批量客户，享受集团信用等级，提供全周期全供应量金融服务，有效解决企业“融资难、融资贵”问题，倍受各方的欢迎和赞誉。另外，厂房实现按需定制、产权分割，园区实现智能化管理，形成企业生产大数据和金融大数据，彻底解决银行对中小微企业放款的顾虑，可以放心为企业提供相对应的金融配套方案。</span></p><p><span style="text-align: justify;"><br></span></p><p><span style="text-align: justify;"><br></span></p>
//             */
//
//            private String id;
//            private String title;
//            private String image;
//            private String add_time;
//            private String hrefType;
//            private String content;
//
//            public String getId() {
//                return id;
//            }
//
//            public void setId(String id) {
//                this.id = id;
//            }
//
//            public String getTitle() {
//                return title;
//            }
//
//            public void setTitle(String title) {
//                this.title = title;
//            }
//
//            public String getImage() {
//                return image;
//            }
//
//            public void setImage(String image) {
//                this.image = image;
//            }
//
//            public String getAdd_time() {
//                return add_time;
//            }
//
//            public void setAdd_time(String add_time) {
//                this.add_time = add_time;
//            }
//
//            public String getHrefType() {
//                return hrefType;
//            }
//
//            public void setHrefType(String hrefType) {
//                this.hrefType = hrefType;
//            }
//
//            public String getContent() {
//                return content;
//            }
//
//            public void setContent(String content) {
//                this.content = content;
//            }
//        }
//
//        public static class BannerArrBean {
//            /**
//             * id : 16
//             * title :
//             * image : http://park.hostop.net/upload/20180915/b5ebe97b7d97d8d7cd0148ebefc564f9.png
//             * add_time : 12天前
//             * hrefType : 11
//             * content :
//             */
//
//            private String id;
//            private String title;
//            private String image;
//            private String add_time;
//            private String hrefType;
//            private String content;
//
//            public String getId() {
//                return id;
//            }
//
//            public void setId(String id) {
//                this.id = id;
//            }
//
//            public String getTitle() {
//                return title;
//            }
//
//            public void setTitle(String title) {
//                this.title = title;
//            }
//
//            public String getImage() {
//                return image;
//            }
//
//            public void setImage(String image) {
//                this.image = image;
//            }
//
//            public String getAdd_time() {
//                return add_time;
//            }
//
//            public void setAdd_time(String add_time) {
//                this.add_time = add_time;
//            }
//
//            public String getHrefType() {
//                return hrefType;
//            }
//
//            public void setHrefType(String hrefType) {
//                this.hrefType = hrefType;
//            }
//
//            public String getContent() {
//                return content;
//            }
//
//            public void setContent(String content) {
//                this.content = content;
//            }
//        }
//
//        public static class WordArrBean {
//            /**
//             * id : 16
//             * title : 测试
//             * logo :
//             * imgArr : []
//             * desc : 123456...
//             * add_time : 5小时前
//             */
//
//            private String id;
//            private String title;
//            private String logo;
//            private String desc;
//            private String add_time;
//            private List<?> imgArr;
//
//            public String getId() {
//                return id;
//            }
//
//            public void setId(String id) {
//                this.id = id;
//            }
//
//            public String getTitle() {
//                return title;
//            }
//
//            public void setTitle(String title) {
//                this.title = title;
//            }
//
//            public String getLogo() {
//                return logo;
//            }
//
//            public void setLogo(String logo) {
//                this.logo = logo;
//            }
//
//            public String getDesc() {
//                return desc;
//            }
//
//            public void setDesc(String desc) {
//                this.desc = desc;
//            }
//
//            public String getAdd_time() {
//                return add_time;
//            }
//
//            public void setAdd_time(String add_time) {
//                this.add_time = add_time;
//            }
//
//            public List<?> getImgArr() {
//                return imgArr;
//            }
//
//            public void setImgArr(List<?> imgArr) {
//                this.imgArr = imgArr;
//            }
//        }
//
//        public static class CategoryBean {
//            /**
//             * id : 23
//             * title : 分类四
//             * logo : http://park.hostop.net/upload/20180914/abd4f332dbe6976038efa199f7ea3eba.png
//             * add_time : 10天前
//             */
//
//            private String id;
//            private String title;
//            private String logo;
//            private String add_time;
//
//            public String getId() {
//                return id;
//            }
//
//            public void setId(String id) {
//                this.id = id;
//            }
//
//            public String getTitle() {
//                return title;
//            }
//
//            public void setTitle(String title) {
//                this.title = title;
//            }
//
//            public String getLogo() {
//                return logo;
//            }
//
//            public void setLogo(String logo) {
//                this.logo = logo;
//            }
//
//            public String getAdd_time() {
//                return add_time;
//            }
//
//            public void setAdd_time(String add_time) {
//                this.add_time = add_time;
//            }
//        }
//    }
//
//    public static class RequestBean {
//        /**
//         * parkId : 1
//         * typeId : 1
//         */
//
//        private String parkId;
//        private String typeId;
//
//        public String getParkId() {
//            return parkId;
//        }
//
//        public void setParkId(String parkId) {
//            this.parkId = parkId;
//        }
//
//        public String getTypeId() {
//            return typeId;
//        }
//
//        public void setTypeId(String typeId) {
//            this.typeId = typeId;
//        }
//    }
//








































//    /**
//     * code : 200
//     * msg :
//     * response : {"upTjArr":[{"id":"12","title":"","image":"http://park.hostop.net/upload/20180908/26c7e99ad5e7df6815badfb97057ab9c.jpg","add_time":"3天前","hrefType":"5","content":""},{"id":"4","title":"","image":"http://park.hostop.net/upload/20180906/3c20de4965161f35609ccdaf1fbee65f.jpg","add_time":"3天前","hrefType":"1","content":""},{"id":"3","title":"","image":"http://park.hostop.net/upload/20180906/3c20de4965161f35609ccdaf1fbee65f.jpg","add_time":"3天前","hrefType":"9","content":"<p><span style=\"text-align: justify;\">为加强抗菌药物管理，遏制细菌耐药，维护人民群众健康，促进经济社会协调发展，制定本行动计划。<span lang=\"EN-US\">&nbsp;<br><\/span><\/span><span style=\"text-align: justify;\">　　一、前言<\/span><span style=\"text-align: justify;\" lang=\"EN-US\">&nbsp;<\/span><span style=\"text-align: justify;\" lang=\"EN-US\"><br><\/span><span style=\"text-align: justify;\">　　我国是抗菌药物的生产和使用大国。抗菌药物广泛应用于医疗卫生、农业养殖领域，在治疗感染性疾病挽救患者生命、防治动物疫病提高养殖效益以及保障公共卫生安全中，发挥了重要作用。但是，由于新型抗菌药物研发能力不足、药店无处方销售抗菌药物、医疗和养殖领域不合理应用抗菌药物、制药企业废弃物排放不达标、群众合理用药意识不高等多种因素，细菌耐药问题日益突出。细菌耐药最终影响人类健康，但造成细菌耐药的因素及其后果却超越了卫生领域，给人类社会带来了生物安全威胁加大、环境污染加剧、经济发展制约等不利影响，迫切需要加强多部门多领域协同谋划、共同应对。<span lang=\"EN-US\">&nbsp;<br><\/span><\/span><span style=\"text-align: justify;\">　　二、工作目标<span lang=\"EN-US\">&nbsp;<\/span><\/span><span style=\"text-align: justify;\" lang=\"EN-US\"><br><\/span><span style=\"text-align: justify;\">　　从国家层面实施综合治理策略和措施，对抗菌药物的研发、生产、流通、应用、环境保护等各个环节加强监管，加强宣传教育和国际交流合作，应对细菌耐药带来的风险挑战。到<span lang=\"EN-US\">2020<\/span>年：<span lang=\"EN-US\">&nbsp;<br><\/span>　　（一）争取研发上市全新抗菌药物<span lang=\"EN-US\">1-2<\/span>个，新型诊断仪器设备和试剂<span lang=\"EN-US\">5-10<\/span>项。<span lang=\"EN-US\">&nbsp;<br><\/span>　　（二）零售药店凭处方销售抗菌药物的比例基本达到全覆盖。省（区、市）凭兽医处方销售抗菌药物的比例达到<span lang=\"EN-US\">50%<\/span>。<span lang=\"EN-US\">&nbsp;<br><\/span>　　（三）健全医疗机构、动物源抗菌药物应用和细菌耐药监测网络；建设细菌耐药参比实验室和菌种中心；建立医疗、养殖领域的抗菌药物应用和细菌耐药控制评价体系。<span lang=\"EN-US\">&nbsp;<br><\/span>　　（四）全国二级以上医院基本建立抗菌药物临床应用管理机制；医疗机构主要耐药菌增长率得到有效控制。<span lang=\"EN-US\">&nbsp;<\/span><\/span><\/p>"},{"id":"2","title":"","image":"http://park.hostop.net/upload/20180906/4d3a4db8e349a4ec7a096ade2d2aea81.jpg","add_time":"3天前","hrefType":"0","content":""}],"bannerArr":[{"id":"9","title":"","image":"http://park.hostop.net/upload/20180908/82e5322abc900ef875950bcc417d29fc.jpg","add_time":"2天前","hrefType":"0","content":""}],"wordArr":[{"id":"9","title":"","image":"http://park.hostop.net/upload/20180908/82e5322abc900ef875950bcc417d29fc.jpg","add_time":"2天前","hrefType":"0","content":""}],"category":[{"id":"9","title":"","image":"http://park.hostop.net/upload/20180908/82e5322abc900ef875950bcc417d29fc.jpg","add_time":"2天前","hrefType":"0","content":""}],"info":{"content":""},"tjIndex":[]}
//     * request : {"parkId":"1","typeId":"1"}
//     * other : []
//     */
//
//    private int code;
//    private String msg;
//    private ResponseBean response;
//    private RequestBean request;
//    private List<?> other;
//    private String background;
//
//
//    public String getBackground() {
//        return background;
//    }
//
//    public void setBackground(String background) {
//        this.background = background;
//    }
//
//    public int getCode() {
//        return code;
//    }
//
//    public void setCode(int code) {
//        this.code = code;
//    }
//
//    public String getMsg() {
//        return msg;
//    }
//
//    public void setMsg(String msg) {
//        this.msg = msg;
//    }
//
//    public ResponseBean getResponse() {
//        return response;
//    }
//
//    public void setResponse(ResponseBean response) {
//        this.response = response;
//    }
//
//    public RequestBean getRequest() {
//        return request;
//    }
//
//    public void setRequest(RequestBean request) {
//        this.request = request;
//    }
//
//    public List<?> getOther() {
//        return other;
//    }
//
//    public void setOther(List<?> other) {
//        this.other = other;
//    }
//
//    public static class ResponseBean {
//        /**
//         * upTjArr : [{"id":"12","title":"","image":"http://park.hostop.net/upload/20180908/26c7e99ad5e7df6815badfb97057ab9c.jpg","add_time":"3天前","hrefType":"5","content":""},{"id":"4","title":"","image":"http://park.hostop.net/upload/20180906/3c20de4965161f35609ccdaf1fbee65f.jpg","add_time":"3天前","hrefType":"1","content":""},{"id":"3","title":"","image":"http://park.hostop.net/upload/20180906/3c20de4965161f35609ccdaf1fbee65f.jpg","add_time":"3天前","hrefType":"9","content":"<p><span style=\"text-align: justify;\">为加强抗菌药物管理，遏制细菌耐药，维护人民群众健康，促进经济社会协调发展，制定本行动计划。<span lang=\"EN-US\">&nbsp;<br><\/span><\/span><span style=\"text-align: justify;\">　　一、前言<\/span><span style=\"text-align: justify;\" lang=\"EN-US\">&nbsp;<\/span><span style=\"text-align: justify;\" lang=\"EN-US\"><br><\/span><span style=\"text-align: justify;\">　　我国是抗菌药物的生产和使用大国。抗菌药物广泛应用于医疗卫生、农业养殖领域，在治疗感染性疾病挽救患者生命、防治动物疫病提高养殖效益以及保障公共卫生安全中，发挥了重要作用。但是，由于新型抗菌药物研发能力不足、药店无处方销售抗菌药物、医疗和养殖领域不合理应用抗菌药物、制药企业废弃物排放不达标、群众合理用药意识不高等多种因素，细菌耐药问题日益突出。细菌耐药最终影响人类健康，但造成细菌耐药的因素及其后果却超越了卫生领域，给人类社会带来了生物安全威胁加大、环境污染加剧、经济发展制约等不利影响，迫切需要加强多部门多领域协同谋划、共同应对。<span lang=\"EN-US\">&nbsp;<br><\/span><\/span><span style=\"text-align: justify;\">　　二、工作目标<span lang=\"EN-US\">&nbsp;<\/span><\/span><span style=\"text-align: justify;\" lang=\"EN-US\"><br><\/span><span style=\"text-align: justify;\">　　从国家层面实施综合治理策略和措施，对抗菌药物的研发、生产、流通、应用、环境保护等各个环节加强监管，加强宣传教育和国际交流合作，应对细菌耐药带来的风险挑战。到<span lang=\"EN-US\">2020<\/span>年：<span lang=\"EN-US\">&nbsp;<br><\/span>　　（一）争取研发上市全新抗菌药物<span lang=\"EN-US\">1-2<\/span>个，新型诊断仪器设备和试剂<span lang=\"EN-US\">5-10<\/span>项。<span lang=\"EN-US\">&nbsp;<br><\/span>　　（二）零售药店凭处方销售抗菌药物的比例基本达到全覆盖。省（区、市）凭兽医处方销售抗菌药物的比例达到<span lang=\"EN-US\">50%<\/span>。<span lang=\"EN-US\">&nbsp;<br><\/span>　　（三）健全医疗机构、动物源抗菌药物应用和细菌耐药监测网络；建设细菌耐药参比实验室和菌种中心；建立医疗、养殖领域的抗菌药物应用和细菌耐药控制评价体系。<span lang=\"EN-US\">&nbsp;<br><\/span>　　（四）全国二级以上医院基本建立抗菌药物临床应用管理机制；医疗机构主要耐药菌增长率得到有效控制。<span lang=\"EN-US\">&nbsp;<\/span><\/span><\/p>"},{"id":"2","title":"","image":"http://park.hostop.net/upload/20180906/4d3a4db8e349a4ec7a096ade2d2aea81.jpg","add_time":"3天前","hrefType":"0","content":""}]
//         * bannerArr : [{"id":"9","title":"","image":"http://park.hostop.net/upload/20180908/82e5322abc900ef875950bcc417d29fc.jpg","add_time":"2天前","hrefType":"0","content":""}]
//         * wordArr : [{"id":"9","title":"","image":"http://park.hostop.net/upload/20180908/82e5322abc900ef875950bcc417d29fc.jpg","add_time":"2天前","hrefType":"0","content":""}]
//         * category : [{"id":"9","title":"","image":"http://park.hostop.net/upload/20180908/82e5322abc900ef875950bcc417d29fc.jpg","add_time":"2天前","hrefType":"0","content":""}]
//         * info : {"content":""}
//         * tjIndex : []
//         */
//
//        private InfoBean info;
//        private List<UpTjArrBean> upTjArr;
//        private List<BannerArrBean> bannerArr;
//        private List<WordArrBean> wordArr;
//        private List<CategoryBean> category;
//        private List<?> tjIndex;
//
//        public InfoBean getInfo() {
//            return info;
//        }
//
//        public void setInfo(InfoBean info) {
//            this.info = info;
//        }
//
//        public List<UpTjArrBean> getUpTjArr() {
//            return upTjArr;
//        }
//
//        public void setUpTjArr(List<UpTjArrBean> upTjArr) {
//            this.upTjArr = upTjArr;
//        }
//
//        public List<BannerArrBean> getBannerArr() {
//            return bannerArr;
//        }
//
//        public void setBannerArr(List<BannerArrBean> bannerArr) {
//            this.bannerArr = bannerArr;
//        }
//
//        public List<WordArrBean> getWordArr() {
//            return wordArr;
//        }
//
//        public void setWordArr(List<WordArrBean> wordArr) {
//            this.wordArr = wordArr;
//        }
//
//        public List<CategoryBean> getCategory() {
//            return category;
//        }
//
//        public void setCategory(List<CategoryBean> category) {
//            this.category = category;
//        }
//
//        public List<?> getTjIndex() {
//            return tjIndex;
//        }
//
//        public void setTjIndex(List<?> tjIndex) {
//            this.tjIndex = tjIndex;
//        }
//
//        public static class InfoBean {
//            /**
//             * content :
//             */
//
//            private String content;
//
//            public String getContent() {
//                return content;
//            }
//
//            public void setContent(String content) {
//                this.content = content;
//            }
//        }
//
//        public static class UpTjArrBean {
//            /**
//             * id : 12
//             * title :
//             * image : http://park.hostop.net/upload/20180908/26c7e99ad5e7df6815badfb97057ab9c.jpg
//             * add_time : 3天前
//             * hrefType : 5
//             * content :
//             */
//
//            private String id;
//            private String title;
//            private String image;
//            private String add_time;
//            private String hrefType;
//            private String content;
//
//            public String getId() {
//                return id;
//            }
//
//            public void setId(String id) {
//                this.id = id;
//            }
//
//            public String getTitle() {
//                return title;
//            }
//
//            public void setTitle(String title) {
//                this.title = title;
//            }
//
//            public String getImage() {
//                return image;
//            }
//
//            public void setImage(String image) {
//                this.image = image;
//            }
//
//            public String getAdd_time() {
//                return add_time;
//            }
//
//            public void setAdd_time(String add_time) {
//                this.add_time = add_time;
//            }
//
//            public String getHrefType() {
//                return hrefType;
//            }
//
//            public void setHrefType(String hrefType) {
//                this.hrefType = hrefType;
//            }
//
//            public String getContent() {
//                return content;
//            }
//
//            public void setContent(String content) {
//                this.content = content;
//            }
//        }
//
//        public static class BannerArrBean {
//            /**
//             * id : 9
//             * title :
//             * image : http://park.hostop.net/upload/20180908/82e5322abc900ef875950bcc417d29fc.jpg
//             * add_time : 2天前
//             * hrefType : 0
//             * content :
//             */
//
//            private String id;
//            private String title;
//            private String image;
//            private String add_time;
//            private String hrefType;
//            private String content;
//
//            public String getId() {
//                return id;
//            }
//
//            public void setId(String id) {
//                this.id = id;
//            }
//
//            public String getTitle() {
//                return title;
//            }
//
//            public void setTitle(String title) {
//                this.title = title;
//            }
//
//            public String getImage() {
//                return image;
//            }
//
//            public void setImage(String image) {
//                this.image = image;
//            }
//
//            public String getAdd_time() {
//                return add_time;
//            }
//
//            public void setAdd_time(String add_time) {
//                this.add_time = add_time;
//            }
//
//            public String getHrefType() {
//                return hrefType;
//            }
//
//            public void setHrefType(String hrefType) {
//                this.hrefType = hrefType;
//            }
//
//            public String getContent() {
//                return content;
//            }
//
//            public void setContent(String content) {
//                this.content = content;
//            }
//        }
//
//        public static class WordArrBean {
//            /**
//             * id : 9
//             * title :
//             * image : http://park.hostop.net/upload/20180908/82e5322abc900ef875950bcc417d29fc.jpg
//             * add_time : 2天前
//             * hrefType : 0
//             * content :
//             */
//
//            private String id;
//            private String title;
//            private String image;
//            private String add_time;
//            private String hrefType;
//            private String content;
//
//            public String getId() {
//                return id;
//            }
//
//            public void setId(String id) {
//                this.id = id;
//            }
//
//            public String getTitle() {
//                return title;
//            }
//
//            public void setTitle(String title) {
//                this.title = title;
//            }
//
//            public String getImage() {
//                return image;
//            }
//
//            public void setImage(String image) {
//                this.image = image;
//            }
//
//            public String getAdd_time() {
//                return add_time;
//            }
//
//            public void setAdd_time(String add_time) {
//                this.add_time = add_time;
//            }
//
//            public String getHrefType() {
//                return hrefType;
//            }
//
//            public void setHrefType(String hrefType) {
//                this.hrefType = hrefType;
//            }
//
//            public String getContent() {
//                return content;
//            }
//
//            public void setContent(String content) {
//                this.content = content;
//            }
//        }
//
//        public static class CategoryBean {
//            /**
//             * id : 9
//             * title :
//             * image : http://park.hostop.net/upload/20180908/82e5322abc900ef875950bcc417d29fc.jpg
//             * add_time : 2天前
//             * hrefType : 0
//             * content :
//             * logo : http://park.hostop.net/upload/20180910/235301500e050f394c70fb590abde155.jpg
//             */
//
//            private String id;
//            private String title;
//            private String image;
//            private String add_time;
//            private String hrefType;
//            private String content;
//            private String logo;
//
//
//            public String getLogo() {
//                return logo;
//            }
//
//            public void setLogo(String logo) {
//                this.logo = logo;
//            }
//
//            public String getId() {
//                return id;
//            }
//
//            public void setId(String id) {
//                this.id = id;
//            }
//
//            public String getTitle() {
//                return title;
//            }
//
//            public void setTitle(String title) {
//                this.title = title;
//            }
//
//            public String getImage() {
//                return image;
//            }
//
//            public void setImage(String image) {
//                this.image = image;
//            }
//
//            public String getAdd_time() {
//                return add_time;
//            }
//
//            public void setAdd_time(String add_time) {
//                this.add_time = add_time;
//            }
//
//            public String getHrefType() {
//                return hrefType;
//            }
//
//            public void setHrefType(String hrefType) {
//                this.hrefType = hrefType;
//            }
//
//            public String getContent() {
//                return content;
//            }
//
//            public void setContent(String content) {
//                this.content = content;
//            }
//        }
//    }
//
//    public static class RequestBean {
//        /**
//         * parkId : 1
//         * typeId : 1
//         */
//
//        private String parkId;
//        private String typeId;
//
//        public String getParkId() {
//            return parkId;
//        }
//
//        public void setParkId(String parkId) {
//            this.parkId = parkId;
//        }
//
//        public String getTypeId() {
//            return typeId;
//        }
//
//        public void setTypeId(String typeId) {
//            this.typeId = typeId;
//        }
//    }



































//    /**
//     * code : 200
//     * msg :
//     * response : {"upTjArr":[{"id":"22","title":"顶部推荐1","logo":"/upload/20180819/fd536d81467d35465845f86bb7550988.jpg","imgArr":["http://park.hostop.net/upload/20180819/fd536d81467d35465845f86bb7550988.jpg","http://park.hostop.net/upload/20180819/c3bf07992f1024beae5ff82b6cd9d552.jpg","http://park.hostop.net/upload/20180819/798b92c1833b033fdcbb339c3976245c.jpg"]},{"id":"21","title":"顶部推荐2","logo":"/upload/20180817/2bd62b02d7589a9e0f2e28f7ee410e3b.jpg","imgArr":["http://park.hostop.net/upload/20180817/2bd62b02d7589a9e0f2e28f7ee410e3b.jpg"]},{"id":"20","title":"顶部推荐3","logo":"/upload/20180816/3d794b7e80f775c5071c7fdac02af6df.jpg","imgArr":["http://park.hostop.net/upload/20180816/3d794b7e80f775c5071c7fdac02af6df.jpg"]},{"id":"18","title":"大力开展招商引资等工作","logo":"/upload/20180816/a4b30ad79d411538c6f962b996b736cc.jpg","imgArr":["http://park.hostop.net/upload/20180816/a4b30ad79d411538c6f962b996b736cc.jpg","http://park.hostop.net/upload/20180817/2ef3314c040131593d958cbdc3a343a9.jpg","http://park.hostop.net/upload/20180817/0e2ab7267902cf12c18bdc5868b4ecc1.jpg"]}],"bannerArr":[{"id":"21","title":"顶部推荐2","logo":"/upload/20180817/2bd62b02d7589a9e0f2e28f7ee410e3b.jpg","imgArr":["http://park.hostop.net/upload/20180817/2bd62b02d7589a9e0f2e28f7ee410e3b.jpg"]},{"id":"18","title":"大力开展招商引资等工作","logo":"/upload/20180816/a4b30ad79d411538c6f962b996b736cc.jpg","imgArr":["http://park.hostop.net/upload/20180816/a4b30ad79d411538c6f962b996b736cc.jpg","http://park.hostop.net/upload/20180817/2ef3314c040131593d958cbdc3a343a9.jpg","http://park.hostop.net/upload/20180817/0e2ab7267902cf12c18bdc5868b4ecc1.jpg"]}],"wordArr":[{"id":"21","title":"顶部推荐2","logo":"/upload/20180817/2bd62b02d7589a9e0f2e28f7ee410e3b.jpg","imgArr":["http://park.hostop.net/upload/20180817/2bd62b02d7589a9e0f2e28f7ee410e3b.jpg"]},{"id":"18","title":"大力开展招商引资等工作","logo":"/upload/20180816/a4b30ad79d411538c6f962b996b736cc.jpg","imgArr":["http://park.hostop.net/upload/20180816/a4b30ad79d411538c6f962b996b736cc.jpg","http://park.hostop.net/upload/20180817/2ef3314c040131593d958cbdc3a343a9.jpg","http://park.hostop.net/upload/20180817/0e2ab7267902cf12c18bdc5868b4ecc1.jpg"]}],"category":[{"id":"21","title":"顶部推荐2","logo":"/upload/20180817/2bd62b02d7589a9e0f2e28f7ee410e3b.jpg","imgArr":["http://park.hostop.net/upload/20180817/2bd62b02d7589a9e0f2e28f7ee410e3b.jpg"]},{"id":"18","title":"大力开展招商引资等工作","logo":"/upload/20180816/a4b30ad79d411538c6f962b996b736cc.jpg","imgArr":["http://park.hostop.net/upload/20180816/a4b30ad79d411538c6f962b996b736cc.jpg","http://park.hostop.net/upload/20180817/2ef3314c040131593d958cbdc3a343a9.jpg","http://park.hostop.net/upload/20180817/0e2ab7267902cf12c18bdc5868b4ecc1.jpg"]}],"info":{"content":""},"tjIndex":[{"id":"7","title":"123123123123","desc":"基本制造内的内容基本制造内的内容基本制造内的内容基本制造内的内容基本制造内的内容基本制造内的内容...","imgArr":["http://park.hostop.net/upload/20180816/8085d87ae42fa0ec15bc0a8f79ff4484.jpg"],"add_time":"8天前"},{"id":"6","title":"高级制造的文章","desc":"基本制造内的内容基本制造内的内容基本制造内的内容基本制造内的内容基本制造内的内容基本制造内的内容...","imgArr":["http://park.hostop.net/upload/20180816/8085d87ae42fa0ec15bc0a8f79ff4484.jpg","http://park.hostop.net/upload/20180816/c3e609af9e6c578b7d9f2a72d0a7f531.jpg","http://park.hostop.net/upload/20180816/8085d87ae42fa0ec15bc0a8f79ff4484.jpg"],"add_time":"22天前"},{"id":"5","title":"基本制造内的内容","desc":"基本制造内的内容基本制造内的内容基本制造内的内容基本制造内的内容基本制造内的内容基本制造内的内容...","imgArr":["http://park.hostop.net/upload/20180816/8085d87ae42fa0ec15bc0a8f79ff4484.jpg"],"add_time":"24天前"}]}
//     * request : {"parkId":"1","typeId":"1"}
//     * other : []
//     */
//
//    private int code;
//    private String msg;
//    private ResponseBean response;
//    private RequestBean request;
//    private List<?> other;
//
//    public int getCode() {
//        return code;
//    }
//
//    public void setCode(int code) {
//        this.code = code;
//    }
//
//    public String getMsg() {
//        return msg;
//    }
//
//    public void setMsg(String msg) {
//        this.msg = msg;
//    }
//
//    public ResponseBean getResponse() {
//        return response;
//    }
//
//    public void setResponse(ResponseBean response) {
//        this.response = response;
//    }
//
//    public RequestBean getRequest() {
//        return request;
//    }
//
//    public void setRequest(RequestBean request) {
//        this.request = request;
//    }
//
//    public List<?> getOther() {
//        return other;
//    }
//
//    public void setOther(List<?> other) {
//        this.other = other;
//    }
//
//    public static class ResponseBean  {
//        /**
//         * upTjArr : [{"id":"22","title":"顶部推荐1","logo":"/upload/20180819/fd536d81467d35465845f86bb7550988.jpg","imgArr":["http://park.hostop.net/upload/20180819/fd536d81467d35465845f86bb7550988.jpg","http://park.hostop.net/upload/20180819/c3bf07992f1024beae5ff82b6cd9d552.jpg","http://park.hostop.net/upload/20180819/798b92c1833b033fdcbb339c3976245c.jpg"]},{"id":"21","title":"顶部推荐2","logo":"/upload/20180817/2bd62b02d7589a9e0f2e28f7ee410e3b.jpg","imgArr":["http://park.hostop.net/upload/20180817/2bd62b02d7589a9e0f2e28f7ee410e3b.jpg"]},{"id":"20","title":"顶部推荐3","logo":"/upload/20180816/3d794b7e80f775c5071c7fdac02af6df.jpg","imgArr":["http://park.hostop.net/upload/20180816/3d794b7e80f775c5071c7fdac02af6df.jpg"]},{"id":"18","title":"大力开展招商引资等工作","logo":"/upload/20180816/a4b30ad79d411538c6f962b996b736cc.jpg","imgArr":["http://park.hostop.net/upload/20180816/a4b30ad79d411538c6f962b996b736cc.jpg","http://park.hostop.net/upload/20180817/2ef3314c040131593d958cbdc3a343a9.jpg","http://park.hostop.net/upload/20180817/0e2ab7267902cf12c18bdc5868b4ecc1.jpg"]}]
//         * bannerArr : [{"id":"21","title":"顶部推荐2","logo":"/upload/20180817/2bd62b02d7589a9e0f2e28f7ee410e3b.jpg","imgArr":["http://park.hostop.net/upload/20180817/2bd62b02d7589a9e0f2e28f7ee410e3b.jpg"]},{"id":"18","title":"大力开展招商引资等工作","logo":"/upload/20180816/a4b30ad79d411538c6f962b996b736cc.jpg","imgArr":["http://park.hostop.net/upload/20180816/a4b30ad79d411538c6f962b996b736cc.jpg","http://park.hostop.net/upload/20180817/2ef3314c040131593d958cbdc3a343a9.jpg","http://park.hostop.net/upload/20180817/0e2ab7267902cf12c18bdc5868b4ecc1.jpg"]}]
//         * wordArr : [{"id":"21","title":"顶部推荐2","logo":"/upload/20180817/2bd62b02d7589a9e0f2e28f7ee410e3b.jpg","imgArr":["http://park.hostop.net/upload/20180817/2bd62b02d7589a9e0f2e28f7ee410e3b.jpg"]},{"id":"18","title":"大力开展招商引资等工作","logo":"/upload/20180816/a4b30ad79d411538c6f962b996b736cc.jpg","imgArr":["http://park.hostop.net/upload/20180816/a4b30ad79d411538c6f962b996b736cc.jpg","http://park.hostop.net/upload/20180817/2ef3314c040131593d958cbdc3a343a9.jpg","http://park.hostop.net/upload/20180817/0e2ab7267902cf12c18bdc5868b4ecc1.jpg"]}]
//         * category : [{"id":"21","title":"顶部推荐2","logo":"/upload/20180817/2bd62b02d7589a9e0f2e28f7ee410e3b.jpg","imgArr":["http://park.hostop.net/upload/20180817/2bd62b02d7589a9e0f2e28f7ee410e3b.jpg"]},{"id":"18","title":"大力开展招商引资等工作","logo":"/upload/20180816/a4b30ad79d411538c6f962b996b736cc.jpg","imgArr":["http://park.hostop.net/upload/20180816/a4b30ad79d411538c6f962b996b736cc.jpg","http://park.hostop.net/upload/20180817/2ef3314c040131593d958cbdc3a343a9.jpg","http://park.hostop.net/upload/20180817/0e2ab7267902cf12c18bdc5868b4ecc1.jpg"]}]
//         * info : {"content":""}
//         * tjIndex : [{"id":"7","title":"123123123123","desc":"基本制造内的内容基本制造内的内容基本制造内的内容基本制造内的内容基本制造内的内容基本制造内的内容...","imgArr":["http://park.hostop.net/upload/20180816/8085d87ae42fa0ec15bc0a8f79ff4484.jpg"],"add_time":"8天前"},{"id":"6","title":"高级制造的文章","desc":"基本制造内的内容基本制造内的内容基本制造内的内容基本制造内的内容基本制造内的内容基本制造内的内容...","imgArr":["http://park.hostop.net/upload/20180816/8085d87ae42fa0ec15bc0a8f79ff4484.jpg","http://park.hostop.net/upload/20180816/c3e609af9e6c578b7d9f2a72d0a7f531.jpg","http://park.hostop.net/upload/20180816/8085d87ae42fa0ec15bc0a8f79ff4484.jpg"],"add_time":"22天前"},{"id":"5","title":"基本制造内的内容","desc":"基本制造内的内容基本制造内的内容基本制造内的内容基本制造内的内容基本制造内的内容基本制造内的内容...","imgArr":["http://park.hostop.net/upload/20180816/8085d87ae42fa0ec15bc0a8f79ff4484.jpg"],"add_time":"24天前"}]
//         */
//
//        private InfoBean info;
//        private List<UpTjArrBean> upTjArr;
//        private List<BannerArrBean> bannerArr;
//        private List<WordArrBean> wordArr;
//        private List<CategoryBean> category;
//        private List<TjIndexBean> tjIndex;
//
//
//
//
//
//        public InfoBean getInfo() {
//            return info;
//        }
//
//        public void setInfo(InfoBean info) {
//            this.info = info;
//        }
//
//        public List<UpTjArrBean> getUpTjArr() {
//            return upTjArr;
//        }
//
//        public void setUpTjArr(List<UpTjArrBean> upTjArr) {
//            this.upTjArr = upTjArr;
//        }
//
//        public List<BannerArrBean> getBannerArr() {
//            return bannerArr;
//        }
//
//        public void setBannerArr(List<BannerArrBean> bannerArr) {
//            this.bannerArr = bannerArr;
//        }
//
//        public List<WordArrBean> getWordArr() {
//            return wordArr;
//        }
//
//        public void setWordArr(List<WordArrBean> wordArr) {
//            this.wordArr = wordArr;
//        }
//
//        public List<CategoryBean> getCategory() {
//            return category;
//        }
//
//        public void setCategory(List<CategoryBean> category) {
//            this.category = category;
//        }
//
//
//        public void setTjIndex(List<TjIndexBean> tjIndex) {
//            this.tjIndex = tjIndex;
//        }
//
//
//
//        public static class InfoBean {
//            /**
//             * content :
//             */
//
//            private String content;
//
//            public String getContent() {
//                return content;
//            }
//
//            public void setContent(String content) {
//                this.content = content;
//            }
//        }
//
//        public static class UpTjArrBean {
//            /**
//             * id : 22
//             * title : 顶部推荐1
//             * logo : /upload/20180819/fd536d81467d35465845f86bb7550988.jpg
//             * imgArr : ["http://park.hostop.net/upload/20180819/fd536d81467d35465845f86bb7550988.jpg","http://park.hostop.net/upload/20180819/c3bf07992f1024beae5ff82b6cd9d552.jpg","http://park.hostop.net/upload/20180819/798b92c1833b033fdcbb339c3976245c.jpg"]
//             */
//
//            private String id;
//            private String title;
//            private String logo;
//            private List<String> imgArr;
//
//            public String getId() {
//                return id;
//            }
//
//            public void setId(String id) {
//                this.id = id;
//            }
//
//            public String getTitle() {
//                return title;
//            }
//
//            public void setTitle(String title) {
//                this.title = title;
//            }
//
//            public String getLogo() {
//                return logo;
//            }
//
//            public void setLogo(String logo) {
//                this.logo = logo;
//            }
//
//            public List<String> getImgArr() {
//                return imgArr;
//            }
//
//            public void setImgArr(List<String> imgArr) {
//                this.imgArr = imgArr;
//            }
//        }
//
//        public static class BannerArrBean {
//            /**
//             * id : 21
//             * title : 顶部推荐2
//             * logo : /upload/20180817/2bd62b02d7589a9e0f2e28f7ee410e3b.jpg
//             * imgArr : ["http://park.hostop.net/upload/20180817/2bd62b02d7589a9e0f2e28f7ee410e3b.jpg"]
//             */
//
//            private String id;
//            private String title;
//            private String logo;
//            private List<String> imgArr;
//
//            public String getId() {
//                return id;
//            }
//
//            public void setId(String id) {
//                this.id = id;
//            }
//
//            public String getTitle() {
//                return title;
//            }
//
//            public void setTitle(String title) {
//                this.title = title;
//            }
//
//            public String getLogo() {
//                return logo;
//            }
//
//            public void setLogo(String logo) {
//                this.logo = logo;
//            }
//
//            public List<String> getImgArr() {
//                return imgArr;
//            }
//
//            public void setImgArr(List<String> imgArr) {
//                this.imgArr = imgArr;
//            }
//        }
//
//        public static class WordArrBean {
//            /**
//             * id : 21
//             * title : 顶部推荐2
//             * logo : /upload/20180817/2bd62b02d7589a9e0f2e28f7ee410e3b.jpg
//             * imgArr : ["http://park.hostop.net/upload/20180817/2bd62b02d7589a9e0f2e28f7ee410e3b.jpg"]
//             */
//
//            private String id;
//            private String title;
//            private String logo;
//            private List<String> imgArr;
//
//            public String getId() {
//                return id;
//            }
//
//            public void setId(String id) {
//                this.id = id;
//            }
//
//            public String getTitle() {
//                return title;
//            }
//
//            public void setTitle(String title) {
//                this.title = title;
//            }
//
//            public String getLogo() {
//                return logo;
//            }
//
//            public void setLogo(String logo) {
//                this.logo = logo;
//            }
//
//            public List<String> getImgArr() {
//                return imgArr;
//            }
//
//            public void setImgArr(List<String> imgArr) {
//                this.imgArr = imgArr;
//            }
//        }
//
//        public static class CategoryBean {
//            /**
//             * id : 21
//             * title : 顶部推荐2
//             * logo : /upload/20180817/2bd62b02d7589a9e0f2e28f7ee410e3b.jpg
//             * imgArr : ["http://park.hostop.net/upload/20180817/2bd62b02d7589a9e0f2e28f7ee410e3b.jpg"]
//             */
//
//            private String id;
//            private String title;
//            private String logo;
//            private List<String> imgArr;
//
//            public String getId() {
//                return id;
//            }
//
//            public void setId(String id) {
//                this.id = id;
//            }
//
//            public String getTitle() {
//                return title;
//            }
//
//            public void setTitle(String title) {
//                this.title = title;
//            }
//
//            public String getLogo() {
//                return logo;
//            }
//
//            public void setLogo(String logo) {
//                this.logo = logo;
//            }
//
//            public List<String> getImgArr() {
//                return imgArr;
//            }
//
//            public void setImgArr(List<String> imgArr) {
//                this.imgArr = imgArr;
//            }
//        }
//
//        public static class TjIndexBean{
//            /**
//             * id : 7
//             * title : 123123123123
//             * desc : 基本制造内的内容基本制造内的内容基本制造内的内容基本制造内的内容基本制造内的内容基本制造内的内容...
//             * imgArr : ["http://park.hostop.net/upload/20180816/8085d87ae42fa0ec15bc0a8f79ff4484.jpg"]
//             * add_time : 8天前
//             */
//
//            private String id;
//            private String title;
//            private String desc;
//            private String add_time;
//            private List<String> imgArr;
//
//
//
//
//            public String getId() {
//                return id;
//            }
//
//            public void setId(String id) {
//                this.id = id;
//            }
//
//            public String getTitle() {
//                return title;
//            }
//
//            public void setTitle(String title) {
//                this.title = title;
//            }
//
//            public String getDesc() {
//                return desc;
//            }
//
//            public void setDesc(String desc) {
//                this.desc = desc;
//            }
//
//            public String getAdd_time() {
//                return add_time;
//            }
//
//            public void setAdd_time(String add_time) {
//                this.add_time = add_time;
//            }
//
//            public List<String> getImgArr() {
//                return imgArr;
//            }
//
//            public void setImgArr(List<String> imgArr) {
//                this.imgArr = imgArr;
//            }
//        }
//    }
//
//    public static class RequestBean {
//        /**
//         * parkId : 1
//         * typeId : 1
//         */
//
//        private String parkId;
//        private String typeId;
//
//        public String getParkId() {
//            return parkId;
//        }
//
//        public void setParkId(String parkId) {
//            this.parkId = parkId;
//        }
//
//        public String getTypeId() {
//            return typeId;
//        }
//
//        public void setTypeId(String typeId) {
//            this.typeId = typeId;
//        }
//    }




































//
//    /**
//     * code : 200
//     * msg :
//     * response : {"upTjArr":[{"id":"22","title":"顶部推荐1","logo":"/upload/20180819/fd536d81467d35465845f86bb7550988.jpg","imgArr":["http://park.hostop.net/upload/20180819/fd536d81467d35465845f86bb7550988.jpg","http://park.hostop.net/upload/20180819/c3bf07992f1024beae5ff82b6cd9d552.jpg","http://park.hostop.net/upload/20180819/798b92c1833b033fdcbb339c3976245c.jpg"]},{"id":"21","title":"顶部推荐2","logo":"/upload/20180817/2bd62b02d7589a9e0f2e28f7ee410e3b.jpg","imgArr":["http://park.hostop.net/upload/20180817/2bd62b02d7589a9e0f2e28f7ee410e3b.jpg"]},{"id":"20","title":"顶部推荐3","logo":"/upload/20180816/3d794b7e80f775c5071c7fdac02af6df.jpg","imgArr":["http://park.hostop.net/upload/20180816/3d794b7e80f775c5071c7fdac02af6df.jpg"]},{"id":"18","title":"大力开展招商引资等工作","logo":"/upload/20180816/a4b30ad79d411538c6f962b996b736cc.jpg","imgArr":["http://park.hostop.net/upload/20180816/a4b30ad79d411538c6f962b996b736cc.jpg","http://park.hostop.net/upload/20180817/2ef3314c040131593d958cbdc3a343a9.jpg","http://park.hostop.net/upload/20180817/0e2ab7267902cf12c18bdc5868b4ecc1.jpg"]}],"bannerArr":[{"id":"21","title":"顶部推荐2","logo":"/upload/20180817/2bd62b02d7589a9e0f2e28f7ee410e3b.jpg","imgArr":["http://park.hostop.net/upload/20180816/a4b30ad79d411538c6f962b996b736cc.jpg","http://park.hostop.net/upload/20180817/2ef3314c040131593d958cbdc3a343a9.jpg","http://park.hostop.net/upload/20180817/0e2ab7267902cf12c18bdc5868b4ecc1.jpg"]},{"id":"18","title":"大力开展招商引资等工作","logo":"/upload/20180816/a4b30ad79d411538c6f962b996b736cc.jpg","imgArr":["http://park.hostop.net/upload/20180816/a4b30ad79d411538c6f962b996b736cc.jpg","http://park.hostop.net/upload/20180817/2ef3314c040131593d958cbdc3a343a9.jpg","http://park.hostop.net/upload/20180817/0e2ab7267902cf12c18bdc5868b4ecc1.jpg"]}],"wordArr":[{"id":"21","title":"顶部推荐2","logo":"/upload/20180817/2bd62b02d7589a9e0f2e28f7ee410e3b.jpg","imgArr":["http://park.hostop.net/upload/20180817/2bd62b02d7589a9e0f2e28f7ee410e3b.jpg"]},{"id":"18","title":"大力开展招商引资等工作","logo":"/upload/20180816/a4b30ad79d411538c6f962b996b736cc.jpg","imgArr":["http://park.hostop.net/upload/20180816/a4b30ad79d411538c6f962b996b736cc.jpg","http://park.hostop.net/upload/20180817/2ef3314c040131593d958cbdc3a343a9.jpg","http://park.hostop.net/upload/20180817/0e2ab7267902cf12c18bdc5868b4ecc1.jpg"]}],"category":[{"id":"21","title":"顶部推荐2","logo":"/upload/20180817/2bd62b02d7589a9e0f2e28f7ee410e3b.jpg","imgArr":["http://park.hostop.net/upload/20180817/2bd62b02d7589a9e0f2e28f7ee410e3b.jpg"]},{"id":"18","title":"大力开展招商引资等工作","logo":"/upload/20180816/a4b30ad79d411538c6f962b996b736cc.jpg","imgArr":["http://park.hostop.net/upload/20180816/a4b30ad79d411538c6f962b996b736cc.jpg","http://park.hostop.net/upload/20180817/2ef3314c040131593d958cbdc3a343a9.jpg","http://park.hostop.net/upload/20180817/0e2ab7267902cf12c18bdc5868b4ecc1.jpg"]}],"info":{"content":""},"tjIndex":[]}
//     * request : {"parkId":"1","typeId":"1"}
//     * other : []
//     */
//
//    private int code;
//    private String msg;
//    private ResponseBean response;
//    private RequestBean request;
//    private List<?> other;
//
//    public int getCode() {
//        return code;
//    }
//
//    public void setCode(int code) {
//        this.code = code;
//    }
//
//    public String getMsg() {
//        return msg;
//    }
//
//    public void setMsg(String msg) {
//        this.msg = msg;
//    }
//
//    public ResponseBean getResponse() {
//        return response;
//    }
//
//    public void setResponse(ResponseBean response) {
//        this.response = response;
//    }
//
//    public RequestBean getRequest() {
//        return request;
//    }
//
//    public void setRequest(RequestBean request) {
//        this.request = request;
//    }
//
//    public List<?> getOther() {
//        return other;
//    }
//
//    public void setOther(List<?> other) {
//        this.other = other;
//    }
//
//    public static class ResponseBean {
//        /**
//         * upTjArr : [{"id":"22","title":"顶部推荐1","logo":"/upload/20180819/fd536d81467d35465845f86bb7550988.jpg","imgArr":["http://park.hostop.net/upload/20180819/fd536d81467d35465845f86bb7550988.jpg","http://park.hostop.net/upload/20180819/c3bf07992f1024beae5ff82b6cd9d552.jpg","http://park.hostop.net/upload/20180819/798b92c1833b033fdcbb339c3976245c.jpg"]},{"id":"21","title":"顶部推荐2","logo":"/upload/20180817/2bd62b02d7589a9e0f2e28f7ee410e3b.jpg","imgArr":["http://park.hostop.net/upload/20180817/2bd62b02d7589a9e0f2e28f7ee410e3b.jpg"]},{"id":"20","title":"顶部推荐3","logo":"/upload/20180816/3d794b7e80f775c5071c7fdac02af6df.jpg","imgArr":["http://park.hostop.net/upload/20180816/3d794b7e80f775c5071c7fdac02af6df.jpg"]},{"id":"18","title":"大力开展招商引资等工作","logo":"/upload/20180816/a4b30ad79d411538c6f962b996b736cc.jpg","imgArr":["http://park.hostop.net/upload/20180816/a4b30ad79d411538c6f962b996b736cc.jpg","http://park.hostop.net/upload/20180817/2ef3314c040131593d958cbdc3a343a9.jpg","http://park.hostop.net/upload/20180817/0e2ab7267902cf12c18bdc5868b4ecc1.jpg"]}]
//         * bannerArr : [{"id":"21","title":"顶部推荐2","logo":"/upload/20180817/2bd62b02d7589a9e0f2e28f7ee410e3b.jpg","imgArr":["http://park.hostop.net/upload/20180817/2bd62b02d7589a9e0f2e28f7ee410e3b.jpg"]},{"id":"18","title":"大力开展招商引资等工作","logo":"/upload/20180816/a4b30ad79d411538c6f962b996b736cc.jpg","imgArr":["http://park.hostop.net/upload/20180816/a4b30ad79d411538c6f962b996b736cc.jpg","http://park.hostop.net/upload/20180817/2ef3314c040131593d958cbdc3a343a9.jpg","http://park.hostop.net/upload/20180817/0e2ab7267902cf12c18bdc5868b4ecc1.jpg"]}]
//         * wordArr : [{"id":"21","title":"顶部推荐2","logo":"/upload/20180817/2bd62b02d7589a9e0f2e28f7ee410e3b.jpg","imgArr":["http://park.hostop.net/upload/20180817/2bd62b02d7589a9e0f2e28f7ee410e3b.jpg"]},{"id":"18","title":"大力开展招商引资等工作","logo":"/upload/20180816/a4b30ad79d411538c6f962b996b736cc.jpg","imgArr":["http://park.hostop.net/upload/20180816/a4b30ad79d411538c6f962b996b736cc.jpg","http://park.hostop.net/upload/20180817/2ef3314c040131593d958cbdc3a343a9.jpg","http://park.hostop.net/upload/20180817/0e2ab7267902cf12c18bdc5868b4ecc1.jpg"]}]
//         * category : [{"id":"21","title":"顶部推荐2","logo":"/upload/20180817/2bd62b02d7589a9e0f2e28f7ee410e3b.jpg","imgArr":["http://park.hostop.net/upload/20180817/2bd62b02d7589a9e0f2e28f7ee410e3b.jpg"]},{"id":"18","title":"大力开展招商引资等工作","logo":"/upload/20180816/a4b30ad79d411538c6f962b996b736cc.jpg","imgArr":["http://park.hostop.net/upload/20180816/a4b30ad79d411538c6f962b996b736cc.jpg","http://park.hostop.net/upload/20180817/2ef3314c040131593d958cbdc3a343a9.jpg","http://park.hostop.net/upload/20180817/0e2ab7267902cf12c18bdc5868b4ecc1.jpg"]}]
//         * info : {"content":""}
//         * tjIndex : []
//         */
//
//        private InfoBean info;
//        private List<UpTjArrBean> upTjArr;
//        private List<BannerArrBean> bannerArr;
//        private List<WordArrBean> wordArr;
//        private List<CategoryBean> category;
//        private List<?> tjIndex;
//
//        public InfoBean getInfo() {
//            return info;
//        }
//
//        public void setInfo(InfoBean info) {
//            this.info = info;
//        }
//
//        public List<UpTjArrBean> getUpTjArr() {
//            return upTjArr;
//        }
//
//        public void setUpTjArr(List<UpTjArrBean> upTjArr) {
//            this.upTjArr = upTjArr;
//        }
//
//        public List<BannerArrBean> getBannerArr() {
//            return bannerArr;
//        }
//
//        public void setBannerArr(List<BannerArrBean> bannerArr) {
//            this.bannerArr = bannerArr;
//        }
//
//        public List<WordArrBean> getWordArr() {
//            return wordArr;
//        }
//
//        public void setWordArr(List<WordArrBean> wordArr) {
//            this.wordArr = wordArr;
//        }
//
//        public List<CategoryBean> getCategory() {
//            return category;
//        }
//
//        public void setCategory(List<CategoryBean> category) {
//            this.category = category;
//        }
//
//        public List<?> getTjIndex() {
//            return tjIndex;
//        }
//
//        public void setTjIndex(List<?> tjIndex) {
//            this.tjIndex = tjIndex;
//        }
//
//        public static class InfoBean {
//            /**
//             * content :
//             */
//
//            private String content;
//
//            public String getContent() {
//                return content;
//            }
//
//            public void setContent(String content) {
//                this.content = content;
//            }
//        }
//
//        public static class UpTjArrBean {
//            /**
//             * id : 22
//             * title : 顶部推荐1
//             * logo : /upload/20180819/fd536d81467d35465845f86bb7550988.jpg
//             * imgArr : ["http://park.hostop.net/upload/20180819/fd536d81467d35465845f86bb7550988.jpg","http://park.hostop.net/upload/20180819/c3bf07992f1024beae5ff82b6cd9d552.jpg","http://park.hostop.net/upload/20180819/798b92c1833b033fdcbb339c3976245c.jpg"]
//             */
//
//            private String id;
//            private String title;
//            private String logo;
//            private List<String> imgArr;
//
//            public String getId() {
//                return id;
//            }
//
//            public void setId(String id) {
//                this.id = id;
//            }
//
//            public String getTitle() {
//                return title;
//            }
//
//            public void setTitle(String title) {
//                this.title = title;
//            }
//
//            public String getLogo() {
//                return logo;
//            }
//
//            public void setLogo(String logo) {
//                this.logo = logo;
//            }
//
//            public List<String> getImgArr() {
//                return imgArr;
//            }
//
//            public void setImgArr(List<String> imgArr) {
//                this.imgArr = imgArr;
//            }
//        }
//
//        public static class BannerArrBean {
//            /**
//             * id : 21
//             * title : 顶部推荐2
//             * logo : /upload/20180817/2bd62b02d7589a9e0f2e28f7ee410e3b.jpg
//             * imgArr : ["http://park.hostop.net/upload/20180817/2bd62b02d7589a9e0f2e28f7ee410e3b.jpg"]
//             */
//
//            private String id;
//            private String title;
//            private String logo;
//            private List<String> imgArr;
//
//            public String getId() {
//                return id;
//            }
//
//            public void setId(String id) {
//                this.id = id;
//            }
//
//            public String getTitle() {
//                return title;
//            }
//
//            public void setTitle(String title) {
//                this.title = title;
//            }
//
//            public String getLogo() {
//                return logo;
//            }
//
//            public void setLogo(String logo) {
//                this.logo = logo;
//            }
//
//            public List<String> getImgArr() {
//                return imgArr;
//            }
//
//            public void setImgArr(List<String> imgArr) {
//                this.imgArr = imgArr;
//            }
//        }
//
//        public static class WordArrBean {
//            /**
//             * id : 21
//             * title : 顶部推荐2
//             * logo : /upload/20180817/2bd62b02d7589a9e0f2e28f7ee410e3b.jpg
//             * imgArr : ["http://park.hostop.net/upload/20180817/2bd62b02d7589a9e0f2e28f7ee410e3b.jpg"]
//             */
//
//            private String id;
//            private String title;
//            private String logo;
//            private List<String> imgArr;
//
//            public String getId() {
//                return id;
//            }
//
//            public void setId(String id) {
//                this.id = id;
//            }
//
//            public String getTitle() {
//                return title;
//            }
//
//            public void setTitle(String title) {
//                this.title = title;
//            }
//
//            public String getLogo() {
//                return logo;
//            }
//
//            public void setLogo(String logo) {
//                this.logo = logo;
//            }
//
//            public List<String> getImgArr() {
//                return imgArr;
//            }
//
//            public void setImgArr(List<String> imgArr) {
//                this.imgArr = imgArr;
//            }
//        }
//
//        public static class CategoryBean {
//            /**
//             * id : 21
//             * title : 顶部推荐2
//             * logo : /upload/20180817/2bd62b02d7589a9e0f2e28f7ee410e3b.jpg
//             * imgArr : ["http://park.hostop.net/upload/20180817/2bd62b02d7589a9e0f2e28f7ee410e3b.jpg"]
//             */
//
//            private String id;
//            private String title;
//            private String logo;
//            private List<String> imgArr;
//
//            public String getId() {
//                return id;
//            }
//
//            public void setId(String id) {
//                this.id = id;
//            }
//
//            public String getTitle() {
//                return title;
//            }
//
//            public void setTitle(String title) {
//                this.title = title;
//            }
//
//            public String getLogo() {
//                return logo;
//            }
//
//            public void setLogo(String logo) {
//                this.logo = logo;
//            }
//
//            public List<String> getImgArr() {
//                return imgArr;
//            }
//
//            public void setImgArr(List<String> imgArr) {
//                this.imgArr = imgArr;
//            }
//        }
//    }
//
//    public static class RequestBean {
//        /**
//         * parkId : 1
//         * typeId : 1
//         */
//
//        private String parkId;
//        private String typeId;
//
//        public String getParkId() {
//            return parkId;
//        }
//
//        public void setParkId(String parkId) {
//            this.parkId = parkId;
//        }
//
//        public String getTypeId() {
//            return typeId;
//        }
//
//        public void setTypeId(String typeId) {
//            this.typeId = typeId;
//        }
//    }
}
