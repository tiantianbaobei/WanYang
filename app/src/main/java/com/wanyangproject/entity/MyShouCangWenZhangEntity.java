package com.wanyangproject.entity;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by 甜甜 on 2018/9/10.
 */

public class MyShouCangWenZhangEntity {
    /**
     * code : 200
     * msg :
     * response : [{"id":"214","title":"biaotibiaoti","add_time":"1537152712","typeId":"4","0":"8","1":"","2":"","desc":"wanyang=园区生活..."},{"id":"210","title":"铺开小微园 凝聚大产业","add_time":"1537072469","typeId":"3","0":"18","1":"","2":"","desc":"正在紧张施工中的工地，会是一幅怎样的景象？走在平阳滨海新区万洋小微科技创业园的到访者，短短几分钟内就能直接发现或间接感受..."},{"id":"206","title":"视频 | 万洋众创城联接华为云","add_time":"1537067993","typeId":"3","0":"17","1":"","2":"","desc":"&nbsp;9月6日下午，2017华为全联接大会峰会在上海卓美亚酒店大观舞台举行。在峰会上万洋众创城总经理吴建明分享了其..."}]
     * request : []
     * other : []
     */

    private int code;
    private String msg;
    private List<ResponseBean> response;
    private List<?> request;
    private List<?> other;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public List<ResponseBean> getResponse() {
        return response;
    }

    public void setResponse(List<ResponseBean> response) {
        this.response = response;
    }

    public List<?> getRequest() {
        return request;
    }

    public void setRequest(List<?> request) {
        this.request = request;
    }

    public List<?> getOther() {
        return other;
    }

    public void setOther(List<?> other) {
        this.other = other;
    }

    public static class ResponseBean {
        /**
         * id : 214
         * title : biaotibiaoti
         * add_time : 1537152712
         * typeId : 4
         * 0 : 8
         * 1 :
         * 2 :
         * desc : wanyang=园区生活...
         */

        private String id;
        private String title;
        private String add_time;
        private String typeId;
        @SerializedName("0")
        private String _$0;
        @SerializedName("1")
        private String _$1;
        @SerializedName("2")
        private String _$2;
        private String desc;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getAdd_time() {
            return add_time;
        }

        public void setAdd_time(String add_time) {
            this.add_time = add_time;
        }

        public String getTypeId() {
            return typeId;
        }

        public void setTypeId(String typeId) {
            this.typeId = typeId;
        }

        public String get_$0() {
            return _$0;
        }

        public void set_$0(String _$0) {
            this._$0 = _$0;
        }

        public String get_$1() {
            return _$1;
        }

        public void set_$1(String _$1) {
            this._$1 = _$1;
        }

        public String get_$2() {
            return _$2;
        }

        public void set_$2(String _$2) {
            this._$2 = _$2;
        }

        public String getDesc() {
            return desc;
        }

        public void setDesc(String desc) {
            this.desc = desc;
        }
    }


//    /**
//     * code : 200
//     * msg :
//     * response : [{"id":"205","title":"视频 | 万洋众创城联接华为云","add_time":"1537067658","typeId":"3","0":"17","1":"","2":"","desc":"&nbsp;9月6日下午，2017华为全联接大会峰会在上海卓美亚酒店大观舞台举行。在峰会上万洋众创城总经理吴建明分享了其..."}]
//     * request : []
//     * other : []
//     */
//
//    private int code;
//    private String msg;
//    private List<ResponseBean> response;
//    private List<?> request;
//    private List<?> other;
//
//    public int getCode() {
//        return code;
//    }
//
//    public void setCode(int code) {
//        this.code = code;
//    }
//
//    public String getMsg() {
//        return msg;
//    }
//
//    public void setMsg(String msg) {
//        this.msg = msg;
//    }
//
//    public List<ResponseBean> getResponse() {
//        return response;
//    }
//
//    public void setResponse(List<ResponseBean> response) {
//        this.response = response;
//    }
//
//    public List<?> getRequest() {
//        return request;
//    }
//
//    public void setRequest(List<?> request) {
//        this.request = request;
//    }
//
//    public List<?> getOther() {
//        return other;
//    }
//
//    public void setOther(List<?> other) {
//        this.other = other;
//    }
//
//    public static class ResponseBean {
//        /**
//         * id : 205
//         * title : 视频 | 万洋众创城联接华为云
//         * add_time : 1537067658
//         * typeId : 3
//         * 0 : 17
//         * 1 :
//         * 2 :
//         * desc : &nbsp;9月6日下午，2017华为全联接大会峰会在上海卓美亚酒店大观舞台举行。在峰会上万洋众创城总经理吴建明分享了其...
//         */
//
//        private String id;
//        private String title;
//        private String add_time;
//        private String typeId;
//        @SerializedName("0")
//        private String _$0;
//        @SerializedName("1")
//        private String _$1;
//        @SerializedName("2")
//        private String _$2;
//        private String desc;
//
//        public String getId() {
//            return id;
//        }
//
//        public void setId(String id) {
//            this.id = id;
//        }
//
//        public String getTitle() {
//            return title;
//        }
//
//        public void setTitle(String title) {
//            this.title = title;
//        }
//
//        public String getAdd_time() {
//            return add_time;
//        }
//
//        public void setAdd_time(String add_time) {
//            this.add_time = add_time;
//        }
//
//        public String getTypeId() {
//            return typeId;
//        }
//
//        public void setTypeId(String typeId) {
//            this.typeId = typeId;
//        }
//
//        public String get_$0() {
//            return _$0;
//        }
//
//        public void set_$0(String _$0) {
//            this._$0 = _$0;
//        }
//
//        public String get_$1() {
//            return _$1;
//        }
//
//        public void set_$1(String _$1) {
//            this._$1 = _$1;
//        }
//
//        public String get_$2() {
//            return _$2;
//        }
//
//        public void set_$2(String _$2) {
//            this._$2 = _$2;
//        }
//
//        public String getDesc() {
//            return desc;
//        }
//
//        public void setDesc(String desc) {
//            this.desc = desc;
//        }
//    }
}
