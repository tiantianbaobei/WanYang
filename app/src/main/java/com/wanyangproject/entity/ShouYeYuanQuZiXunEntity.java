package com.wanyangproject.entity;

import java.util.List;

/**
 * Created by 甜甜 on 2018/8/17.
 */

public class ShouYeYuanQuZiXunEntity {

    /**
     * code : 200
     * msg :
     * response : [{"id":"2","title":"在党的旗帜下奋进强军新时代","desc":"新华社北京８月１６日电题：在党的旗帜下奋进强军新时代\u2014\u2014以习近平同志为核心的党中央领导和推进人民军队党的建设述评　　新华...","logo":"http://park.hostop.net/upload/20180816/47c1d7310b9bff1194b85a614b58cc7d.jpg","add_time":"11小时前"},{"id":"1","title":"最新 今天晚上出现50年难遇流星雨","desc":"万洋集团自身定位中小微企业的服务商和众创平台的运营商，实行智能刷卡门禁系统。科学利用大数据平台，为企业提供更人性化的\u2018保...","logo":"http://park.hostop.net/upload/20180816/8085d87ae42fa0ec15bc0a8f79ff4484.jpg","add_time":"11小时前"}]
     * request : {"parkId":"1"}
     * other : []
     */

    private int code;
    private String msg;
    private RequestBean request;
    private List<ResponseBean> response;
    private List<?> other;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public RequestBean getRequest() {
        return request;
    }

    public void setRequest(RequestBean request) {
        this.request = request;
    }

    public List<ResponseBean> getResponse() {
        return response;
    }

    public void setResponse(List<ResponseBean> response) {
        this.response = response;
    }

    public List<?> getOther() {
        return other;
    }

    public void setOther(List<?> other) {
        this.other = other;
    }

    public static class RequestBean {
        /**
         * parkId : 1
         */

        private String parkId;

        public String getParkId() {
            return parkId;
        }

        public void setParkId(String parkId) {
            this.parkId = parkId;
        }
    }

    public static class ResponseBean {
        /**
         * id : 2
         * title : 在党的旗帜下奋进强军新时代
         * desc : 新华社北京８月１６日电题：在党的旗帜下奋进强军新时代——以习近平同志为核心的党中央领导和推进人民军队党的建设述评　　新华...
         * logo : http://park.hostop.net/upload/20180816/47c1d7310b9bff1194b85a614b58cc7d.jpg
         * add_time : 11小时前
         */

        private String id;
        private String title;
        private String desc;
        private String logo;
        private String add_time;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getDesc() {
            return desc;
        }

        public void setDesc(String desc) {
            this.desc = desc;
        }

        public String getLogo() {
            return logo;
        }

        public void setLogo(String logo) {
            this.logo = logo;
        }

        public String getAdd_time() {
            return add_time;
        }

        public void setAdd_time(String add_time) {
            this.add_time = add_time;
        }
    }
}
