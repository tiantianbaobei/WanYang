package com.wanyangproject.entity;

import java.util.List;

/**
 * Created by 甜甜 on 2018/8/17.
 */

public class ChanYeJinRongXiangQingEntity {
    /**
     * code : 200
     * msg :
     * response : {"id":"2","title":"网商银行属于正规银行吗？会不会有风险","content":"<p style=\"text-align: justify;\">00年前出现的金融<a id=\"neilianxitong\" target=\"_blank\" href=\"http://baike.wdzj.com/doc-view-3102.html\">传销<\/a>类<span class=\"highlight_yuqing\">骗局<\/span>\u2014\u2014<span class=\"highlight_yuqing\">庞氏<span class=\"highlight_yuqing\">骗局<\/span><\/span>，如今是穿了新衣漂洋过海来骗你，教你三招识别<span class=\"highlight_yuqing\">骗局<\/span>不上当。9张图350个名单，让你看看如今\u201c百花争艳\u201d的金融\u201c盛世\u201d！<\/p><p><span style=\"text-align: justify;\">1警方整理的违法机构名单<\/span><\/p><p style=\"text-align: justify;\">目前市面上各类资金盘传销及网络传销泛滥成灾，据悉，该类传销组织会在短期内<span class=\"highlight_yuqing\">圈钱<\/span>关网<span class=\"highlight_yuqing\"><a id=\"neilianxitong\" target=\"_blank\" href=\"http://baike.wdzj.com/doc-view-1341.html\">跑路<\/a><\/span>，需要大家提高警惕。现在小编收集了一份义乌市公安局官方认证平台：义乌经侦<span class=\"highlight_yuqing\">预警<\/span>平台，提供的一份350多家<span class=\"highlight_yuqing\">涉嫌<\/span>违法资金盘的机构，希望大家一起来看看以下名单都有哪些机构上榜，同时也请大家对此名单广而告之，帮助身边亲友远离<span class=\"highlight_yuqing\">骗局<\/span>，守住血汗钱！<br><\/p><p><span style=\"text-align: justify;\">2<\/span><span class=\"highlight_yuqing\" style=\"text-align: justify;\">骗局<\/span><span style=\"text-align: justify;\">大致分为这几类<\/span><\/p><p style=\"text-align: justify;\">俗话说有钱人死于信<a id=\"neilianxitong\" target=\"_blank\" href=\"http://baike.wdzj.com/doc-view-4320.html\">托<\/a>，中产死于P2P，屌丝死于<a id=\"neilianxitong\" target=\"_blank\" href=\"http://www.wdzj.com/jhzt/180223/cg_140/\">炒股<\/a>，在这个繁华的世界，总有一款<span class=\"highlight_yuqing\">骗局<\/span>适合你。<br><\/p><p style=\"text-align: justify;\"><span class=\"highlight_yuqing\">骗局<\/span>：<span class=\"highlight_yuqing\">庞氏<span class=\"highlight_yuqing\">骗局<\/span><\/span><\/p><p style=\"text-align: justify;\"><span class=\"highlight_yuqing\">庞氏<span class=\"highlight_yuqing\">骗局<\/span><\/span>是一种最常见的<a id=\"neilianxitong\" target=\"_blank\" href=\"http://www.wdzj.com/\">投资<\/a><span class=\"highlight_yuqing\">诈骗<\/span>，是金字塔<span class=\"highlight_yuqing\">骗局<\/span>的变体，很多非法的传销集团就是用这一招聚敛钱财的。这种骗术是一个名叫查尔斯·庞奇的<a id=\"neilianxitong\" target=\"_blank\" href=\"http://baike.wdzj.com/doc-view-2247.html\">投机<\/a>商人\u201c发明\u201d的。<span class=\"highlight_yuqing\">庞氏<span class=\"highlight_yuqing\">骗局<\/span><\/span>在中国又称\u201c拆东墙补西墙\u201d、\u201c空手套白狼\u201d。简言之，就是利用新<a id=\"neilianxitong\" target=\"_blank\" href=\"http://www.wdzj.com/juhe/170519/tzrdq_168/\">投资人的钱<\/a>来向老<a id=\"neilianxitong\" target=\"_blank\" href=\"http://www.wdzj.com/juhe/170517/tzz_168/\">投资者<\/a>支付利息和短期回报，以制造赚钱的假象，进而<span class=\"highlight_yuqing\">骗取<\/span>更多的钱。查尔斯·庞奇1920年开始从事投资<span class=\"highlight_yuqing\">欺诈<\/span>，大约4万人被卷入<span class=\"highlight_yuqing\">骗局<\/span>，<span class=\"highlight_yuqing\">被骗<\/span>金额达<a id=\"neilianxitong\" target=\"_blank\" href=\"http://www.wdzj.com/juhe/171124/1500wmy_12/\">1500万美元<\/a>，相当于今天的1.<a id=\"neilianxitong\" target=\"_blank\" href=\"http://www.wdzj.com/juhe/171124/5ymy_12/\">5亿美元<\/a>。庞奇最后锒铛入狱。<\/p>","imgArr":["http://park.hostop.net/upload/20180901/afdfa3e27b60a49f64d2f316353e5e38.jpg","http://park.hostop.net/upload/20180901/b90fc628e0f1d27e74a149b0cc61c854.jpg","http://park.hostop.net/upload/20180901/3dd26c6d5a425f806f87b1223d5d70f9.jpg"],"add_time":"20天前"}
     * request : {"id":"2","nTypeId":"2"}
     * other : []
     */

    private int code;
    private String msg;
    private ResponseBean response;
    private RequestBean request;
    private List<?> other;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public ResponseBean getResponse() {
        return response;
    }

    public void setResponse(ResponseBean response) {
        this.response = response;
    }

    public RequestBean getRequest() {
        return request;
    }

    public void setRequest(RequestBean request) {
        this.request = request;
    }

    public List<?> getOther() {
        return other;
    }

    public void setOther(List<?> other) {
        this.other = other;
    }

    public static class ResponseBean {
        /**
         * id : 2
         * title : 网商银行属于正规银行吗？会不会有风险
         * content : <p style="text-align: justify;">00年前出现的金融<a id="neilianxitong" target="_blank" href="http://baike.wdzj.com/doc-view-3102.html">传销</a>类<span class="highlight_yuqing">骗局</span>——<span class="highlight_yuqing">庞氏<span class="highlight_yuqing">骗局</span></span>，如今是穿了新衣漂洋过海来骗你，教你三招识别<span class="highlight_yuqing">骗局</span>不上当。9张图350个名单，让你看看如今“百花争艳”的金融“盛世”！</p><p><span style="text-align: justify;">1警方整理的违法机构名单</span></p><p style="text-align: justify;">目前市面上各类资金盘传销及网络传销泛滥成灾，据悉，该类传销组织会在短期内<span class="highlight_yuqing">圈钱</span>关网<span class="highlight_yuqing"><a id="neilianxitong" target="_blank" href="http://baike.wdzj.com/doc-view-1341.html">跑路</a></span>，需要大家提高警惕。现在小编收集了一份义乌市公安局官方认证平台：义乌经侦<span class="highlight_yuqing">预警</span>平台，提供的一份350多家<span class="highlight_yuqing">涉嫌</span>违法资金盘的机构，希望大家一起来看看以下名单都有哪些机构上榜，同时也请大家对此名单广而告之，帮助身边亲友远离<span class="highlight_yuqing">骗局</span>，守住血汗钱！<br></p><p><span style="text-align: justify;">2</span><span class="highlight_yuqing" style="text-align: justify;">骗局</span><span style="text-align: justify;">大致分为这几类</span></p><p style="text-align: justify;">俗话说有钱人死于信<a id="neilianxitong" target="_blank" href="http://baike.wdzj.com/doc-view-4320.html">托</a>，中产死于P2P，屌丝死于<a id="neilianxitong" target="_blank" href="http://www.wdzj.com/jhzt/180223/cg_140/">炒股</a>，在这个繁华的世界，总有一款<span class="highlight_yuqing">骗局</span>适合你。<br></p><p style="text-align: justify;"><span class="highlight_yuqing">骗局</span>：<span class="highlight_yuqing">庞氏<span class="highlight_yuqing">骗局</span></span></p><p style="text-align: justify;"><span class="highlight_yuqing">庞氏<span class="highlight_yuqing">骗局</span></span>是一种最常见的<a id="neilianxitong" target="_blank" href="http://www.wdzj.com/">投资</a><span class="highlight_yuqing">诈骗</span>，是金字塔<span class="highlight_yuqing">骗局</span>的变体，很多非法的传销集团就是用这一招聚敛钱财的。这种骗术是一个名叫查尔斯·庞奇的<a id="neilianxitong" target="_blank" href="http://baike.wdzj.com/doc-view-2247.html">投机</a>商人“发明”的。<span class="highlight_yuqing">庞氏<span class="highlight_yuqing">骗局</span></span>在中国又称“拆东墙补西墙”、“空手套白狼”。简言之，就是利用新<a id="neilianxitong" target="_blank" href="http://www.wdzj.com/juhe/170519/tzrdq_168/">投资人的钱</a>来向老<a id="neilianxitong" target="_blank" href="http://www.wdzj.com/juhe/170517/tzz_168/">投资者</a>支付利息和短期回报，以制造赚钱的假象，进而<span class="highlight_yuqing">骗取</span>更多的钱。查尔斯·庞奇1920年开始从事投资<span class="highlight_yuqing">欺诈</span>，大约4万人被卷入<span class="highlight_yuqing">骗局</span>，<span class="highlight_yuqing">被骗</span>金额达<a id="neilianxitong" target="_blank" href="http://www.wdzj.com/juhe/171124/1500wmy_12/">1500万美元</a>，相当于今天的1.<a id="neilianxitong" target="_blank" href="http://www.wdzj.com/juhe/171124/5ymy_12/">5亿美元</a>。庞奇最后锒铛入狱。</p>
         * imgArr : ["http://park.hostop.net/upload/20180901/afdfa3e27b60a49f64d2f316353e5e38.jpg","http://park.hostop.net/upload/20180901/b90fc628e0f1d27e74a149b0cc61c854.jpg","http://park.hostop.net/upload/20180901/3dd26c6d5a425f806f87b1223d5d70f9.jpg"]
         * add_time : 20天前
         */

        private String id;
        private String title;
        private String content;
        private String add_time;
        private List<String> imgArr;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getContent() {
            return content;
        }

        public void setContent(String content) {
            this.content = content;
        }

        public String getAdd_time() {
            return add_time;
        }

        public void setAdd_time(String add_time) {
            this.add_time = add_time;
        }

        public List<String> getImgArr() {
            return imgArr;
        }

        public void setImgArr(List<String> imgArr) {
            this.imgArr = imgArr;
        }
    }

    public static class RequestBean {
        /**
         * id : 2
         * nTypeId : 2
         */

        private String id;
        private String nTypeId;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getNTypeId() {
            return nTypeId;
        }

        public void setNTypeId(String nTypeId) {
            this.nTypeId = nTypeId;
        }
    }


//    /**
//     * code : 200
//     * msg :
//     * response : {"id":"","title":"","content":"","imgArr":"http://park.hostop.net","add_time":"1970-01-01 08:00:00"}
//     * request : {"id":"22","nTypeId":"1"}
//     * other : []
//     */
//
//    private int code;
//    private String msg;
//    private ResponseBean response;
//    private RequestBean request;
//    private List<?> other;
//
//    public int getCode() {
//        return code;
//    }
//
//    public void setCode(int code) {
//        this.code = code;
//    }
//
//    public String getMsg() {
//        return msg;
//    }
//
//    public void setMsg(String msg) {
//        this.msg = msg;
//    }
//
//    public ResponseBean getResponse() {
//        return response;
//    }
//
//    public void setResponse(ResponseBean response) {
//        this.response = response;
//    }
//
//    public RequestBean getRequest() {
//        return request;
//    }
//
//    public void setRequest(RequestBean request) {
//        this.request = request;
//    }
//
//    public List<?> getOther() {
//        return other;
//    }
//
//    public void setOther(List<?> other) {
//        this.other = other;
//    }
//
//    public static class ResponseBean {
//        /**
//         * id :
//         * title :
//         * content :
//         * imgArr : http://park.hostop.net
//         * add_time : 1970-01-01 08:00:00
//         */
//
//        private String id;
//        private String title;
//        private String content;
//        private String imgArr;
//        private String add_time;
//
//        public String getId() {
//            return id;
//        }
//
//        public void setId(String id) {
//            this.id = id;
//        }
//
//        public String getTitle() {
//            return title;
//        }
//
//        public void setTitle(String title) {
//            this.title = title;
//        }
//
//        public String getContent() {
//            return content;
//        }
//
//        public void setContent(String content) {
//            this.content = content;
//        }
//
//        public String getImgArr() {
//            return imgArr;
//        }
//
//        public void setImgArr(String imgArr) {
//            this.imgArr = imgArr;
//        }
//
//        public String getAdd_time() {
//            return add_time;
//        }
//
//        public void setAdd_time(String add_time) {
//            this.add_time = add_time;
//        }
//    }
//
//    public static class RequestBean {
//        /**
//         * id : 22
//         * nTypeId : 1
//         */
//
//        private String id;
//        private String nTypeId;
//
//        public String getId() {
//            return id;
//        }
//
//        public void setId(String id) {
//            this.id = id;
//        }
//
//        public String getNTypeId() {
//            return nTypeId;
//        }
//
//        public void setNTypeId(String nTypeId) {
//            this.nTypeId = nTypeId;
//        }
//    }



















//    /**
//     * code : 200
//     * msg :
//     * response : {"id":"7","title":"123123123123","content":"基本制造内的内容基本制造内的内容基本制造内的内容基本制造内的内容基本制造内的内容基本制造内的内容","imgArr":["http://park.hostop.net/upload/20180816/8085d87ae42fa0ec15bc0a8f79ff4484.jpg","http://park.hostop.net/upload/20180816/c3e609af9e6c578b7d9f2a72d0a7f531.jpg"],"add_time":"16小时前"}
//     * request : {"id":"7","nTypeId":"2"}
//     * other : []
//     */
//
//    private int code;
//    private String msg;
//    private ResponseBean response;
//    private RequestBean request;
//    private List<?> other;
//
//    public int getCode() {
//        return code;
//    }
//
//    public void setCode(int code) {
//        this.code = code;
//    }
//
//    public String getMsg() {
//        return msg;
//    }
//
//    public void setMsg(String msg) {
//        this.msg = msg;
//    }
//
//    public ResponseBean getResponse() {
//        return response;
//    }
//
//    public void setResponse(ResponseBean response) {
//        this.response = response;
//    }
//
//    public RequestBean getRequest() {
//        return request;
//    }
//
//    public void setRequest(RequestBean request) {
//        this.request = request;
//    }
//
//    public List<?> getOther() {
//        return other;
//    }
//
//    public void setOther(List<?> other) {
//        this.other = other;
//    }
//
//    public static class ResponseBean {
//        /**
//         * id : 7
//         * title : 123123123123
//         * content : 基本制造内的内容基本制造内的内容基本制造内的内容基本制造内的内容基本制造内的内容基本制造内的内容
//         * imgArr : ["http://park.hostop.net/upload/20180816/8085d87ae42fa0ec15bc0a8f79ff4484.jpg","http://park.hostop.net/upload/20180816/c3e609af9e6c578b7d9f2a72d0a7f531.jpg"]
//         * add_time : 16小时前
//         */
//
//        private String id;
//        private String title;
//        private String content;
//        private String add_time;
//        private List<String> imgArr;
//
//        public String getId() {
//            return id;
//        }
//
//        public void setId(String id) {
//            this.id = id;
//        }
//
//        public String getTitle() {
//            return title;
//        }
//
//        public void setTitle(String title) {
//            this.title = title;
//        }
//
//        public String getContent() {
//            return content;
//        }
//
//        public void setContent(String content) {
//            this.content = content;
//        }
//
//        public String getAdd_time() {
//            return add_time;
//        }
//
//        public void setAdd_time(String add_time) {
//            this.add_time = add_time;
//        }
//
//        public List<String> getImgArr() {
//            return imgArr;
//        }
//
//        public void setImgArr(List<String> imgArr) {
//            this.imgArr = imgArr;
//        }
//    }
//
//    public static class RequestBean {
//        /**
//         * id : 7
//         * nTypeId : 2
//         */
//
//        private String id;
//        private String nTypeId;
//
//        public String getId() {
//            return id;
//        }
//
//        public void setId(String id) {
//            this.id = id;
//        }
//
//        public String getNTypeId() {
//            return nTypeId;
//        }
//
//        public void setNTypeId(String nTypeId) {
//            this.nTypeId = nTypeId;
//        }
//    }
}
