package com.wanyangproject.entity;

import java.util.List;

/**
 * Created by 甜甜 on 2018/9/17.
 */

public class ShuiFeiJiLuEntity {
    /**
     * code : 200
     * msg :
     * response : {"code":"200","message":"OK","data":{"total":"2","pageSize":"20","list":[{"parkPersonId":"817","amount":"300","time":"2018-08-02 11:02:17","chargeOrderId":"3862","chargeType":"0"},{"parkPersonId":"817","amount":"300","time":"2018-08-02 10:58:10","chargeOrderId":"3860","chargeType":"0"}]}}
     * request : {"parkId":"1","page":"0","type":"3"}
     * other : []
     */

    private int code;
    private String msg;
    private ResponseBean response;
    private RequestBean request;
    private List<?> other;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public ResponseBean getResponse() {
        return response;
    }

    public void setResponse(ResponseBean response) {
        this.response = response;
    }

    public RequestBean getRequest() {
        return request;
    }

    public void setRequest(RequestBean request) {
        this.request = request;
    }

    public List<?> getOther() {
        return other;
    }

    public void setOther(List<?> other) {
        this.other = other;
    }

    public static class ResponseBean {
        /**
         * code : 200
         * message : OK
         * data : {"total":"2","pageSize":"20","list":[{"parkPersonId":"817","amount":"300","time":"2018-08-02 11:02:17","chargeOrderId":"3862","chargeType":"0"},{"parkPersonId":"817","amount":"300","time":"2018-08-02 10:58:10","chargeOrderId":"3860","chargeType":"0"}]}
         */

        private String code;
        private String message;
        private DataBean data;

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        public DataBean getData() {
            return data;
        }

        public void setData(DataBean data) {
            this.data = data;
        }

        public static class DataBean {
            /**
             * total : 2
             * pageSize : 20
             * list : [{"parkPersonId":"817","amount":"300","time":"2018-08-02 11:02:17","chargeOrderId":"3862","chargeType":"0"},{"parkPersonId":"817","amount":"300","time":"2018-08-02 10:58:10","chargeOrderId":"3860","chargeType":"0"}]
             */

            private String total;
            private String pageSize;
            private List<ListBean> list;

            public String getTotal() {
                return total;
            }

            public void setTotal(String total) {
                this.total = total;
            }

            public String getPageSize() {
                return pageSize;
            }

            public void setPageSize(String pageSize) {
                this.pageSize = pageSize;
            }

            public List<ListBean> getList() {
                return list;
            }

            public void setList(List<ListBean> list) {
                this.list = list;
            }

            public static class ListBean {
                /**
                 * parkPersonId : 817
                 * amount : 300
                 * time : 2018-08-02 11:02:17
                 * chargeOrderId : 3862
                 * chargeType : 0
                 */

                private String parkPersonId;
                private String amount;
                private String time;
                private String chargeOrderId;
                private String chargeType;

                public String getParkPersonId() {
                    return parkPersonId;
                }

                public void setParkPersonId(String parkPersonId) {
                    this.parkPersonId = parkPersonId;
                }

                public String getAmount() {
                    return amount;
                }

                public void setAmount(String amount) {
                    this.amount = amount;
                }

                public String getTime() {
                    return time;
                }

                public void setTime(String time) {
                    this.time = time;
                }

                public String getChargeOrderId() {
                    return chargeOrderId;
                }

                public void setChargeOrderId(String chargeOrderId) {
                    this.chargeOrderId = chargeOrderId;
                }

                public String getChargeType() {
                    return chargeType;
                }

                public void setChargeType(String chargeType) {
                    this.chargeType = chargeType;
                }
            }
        }
    }

    public static class RequestBean {
        /**
         * parkId : 1
         * page : 0
         * type : 3
         */

        private String parkId;
        private String page;
        private String type;

        public String getParkId() {
            return parkId;
        }

        public void setParkId(String parkId) {
            this.parkId = parkId;
        }

        public String getPage() {
            return page;
        }

        public void setPage(String page) {
            this.page = page;
        }

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }
    }


//    /**
//     * code : 200
//     * msg :
//     * response : {"code":"200","message":"OK","data":{"total":"3","pageSize":"20","list":[{"waterUserId":"43","amount":"1","time":"2018-03-09 14:49:45","chargeOrderId":"119"},{"waterUserId":"43","amount":"485","time":"2018-03-30 15:26:10","chargeOrderId":"1136"},{"waterUserId":"43","amount":"200","time":"2018-05-14 09:44:22","chargeOrderId":"2010"}]}}
//     * request : {"parkId":"1","page":"0","type":"1"}
//     * other : []
//     */
//
//    private int code;
//    private String msg;
//    private ResponseBean response;
//    private RequestBean request;
//    private List<?> other;
//
//    public int getCode() {
//        return code;
//    }
//
//    public void setCode(int code) {
//        this.code = code;
//    }
//
//    public String getMsg() {
//        return msg;
//    }
//
//    public void setMsg(String msg) {
//        this.msg = msg;
//    }
//
//    public ResponseBean getResponse() {
//        return response;
//    }
//
//    public void setResponse(ResponseBean response) {
//        this.response = response;
//    }
//
//    public RequestBean getRequest() {
//        return request;
//    }
//
//    public void setRequest(RequestBean request) {
//        this.request = request;
//    }
//
//    public List<?> getOther() {
//        return other;
//    }
//
//    public void setOther(List<?> other) {
//        this.other = other;
//    }
//
//    public static class ResponseBean {
//        /**
//         * code : 200
//         * message : OK
//         * data : {"total":"3","pageSize":"20","list":[{"waterUserId":"43","amount":"1","time":"2018-03-09 14:49:45","chargeOrderId":"119"},{"waterUserId":"43","amount":"485","time":"2018-03-30 15:26:10","chargeOrderId":"1136"},{"waterUserId":"43","amount":"200","time":"2018-05-14 09:44:22","chargeOrderId":"2010"}]}
//         */
//
//        private String code;
//        private String message;
//        private DataBean data;
//
//        public String getCode() {
//            return code;
//        }
//
//        public void setCode(String code) {
//            this.code = code;
//        }
//
//        public String getMessage() {
//            return message;
//        }
//
//        public void setMessage(String message) {
//            this.message = message;
//        }
//
//        public DataBean getData() {
//            return data;
//        }
//
//        public void setData(DataBean data) {
//            this.data = data;
//        }
//
//        public static class DataBean {
//            /**
//             * total : 3
//             * pageSize : 20
//             * list : [{"waterUserId":"43","amount":"1","time":"2018-03-09 14:49:45","chargeOrderId":"119"},{"waterUserId":"43","amount":"485","time":"2018-03-30 15:26:10","chargeOrderId":"1136"},{"waterUserId":"43","amount":"200","time":"2018-05-14 09:44:22","chargeOrderId":"2010"}]
//             */
//
//            private String total;
//            private String pageSize;
//            private List<ListBean> list;
//
//            public String getTotal() {
//                return total;
//            }
//
//            public void setTotal(String total) {
//                this.total = total;
//            }
//
//            public String getPageSize() {
//                return pageSize;
//            }
//
//            public void setPageSize(String pageSize) {
//                this.pageSize = pageSize;
//            }
//
//            public List<ListBean> getList() {
//                return list;
//            }
//
//            public void setList(List<ListBean> list) {
//                this.list = list;
//            }
//
//            public static class ListBean {
//                /**
//                 * waterUserId : 43
//                 * amount : 1
//                 * time : 2018-03-09 14:49:45
//                 * chargeOrderId : 119
//                 */
//
//                private String waterUserId;
//                private String amount;
//                private String time;
//                private String chargeOrderId;
//
//                public String getWaterUserId() {
//                    return waterUserId;
//                }
//
//                public void setWaterUserId(String waterUserId) {
//                    this.waterUserId = waterUserId;
//                }
//
//                public String getAmount() {
//                    return amount;
//                }
//
//                public void setAmount(String amount) {
//                    this.amount = amount;
//                }
//
//                public String getTime() {
//                    return time;
//                }
//
//                public void setTime(String time) {
//                    this.time = time;
//                }
//
//                public String getChargeOrderId() {
//                    return chargeOrderId;
//                }
//
//                public void setChargeOrderId(String chargeOrderId) {
//                    this.chargeOrderId = chargeOrderId;
//                }
//            }
//        }
//    }
//
//    public static class RequestBean {
//        /**
//         * parkId : 1
//         * page : 0
//         * type : 1
//         */
//
//        private String parkId;
//        private String page;
//        private String type;
//
//        public String getParkId() {
//            return parkId;
//        }
//
//        public void setParkId(String parkId) {
//            this.parkId = parkId;
//        }
//
//        public String getPage() {
//            return page;
//        }
//
//        public void setPage(String page) {
//            this.page = page;
//        }
//
//        public String getType() {
//            return type;
//        }
//
//        public void setType(String type) {
//            this.type = type;
//        }
//    }
}
