package com.wanyangproject.entity;

import java.util.List;

/**
 * Created by 甜甜 on 2018/9/4.
 */

public class Weixin400Entity {

    /**
     * code : 400
     * msg : 需要先绑定手机号
     * response : {"bindPhone":"1"}
     * request : {"wxkey":"of9-E0g7hKgksAz85N4A_XB3q2z8","uuid":""}
     * other : []
     */

    private int code;
    private String msg;
    private ResponseBean response;
    private RequestBean request;
    private List<?> other;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public ResponseBean getResponse() {
        return response;
    }

    public void setResponse(ResponseBean response) {
        this.response = response;
    }

    public RequestBean getRequest() {
        return request;
    }

    public void setRequest(RequestBean request) {
        this.request = request;
    }

    public List<?> getOther() {
        return other;
    }

    public void setOther(List<?> other) {
        this.other = other;
    }

    public static class ResponseBean {
        /**
         * bindPhone : 1
         */

        private String bindPhone;

        public String getBindPhone() {
            return bindPhone;
        }

        public void setBindPhone(String bindPhone) {
            this.bindPhone = bindPhone;
        }
    }

    public static class RequestBean {
        /**
         * wxkey : of9-E0g7hKgksAz85N4A_XB3q2z8
         * uuid :
         */

        private String wxkey;
        private String uuid;

        public String getWxkey() {
            return wxkey;
        }

        public void setWxkey(String wxkey) {
            this.wxkey = wxkey;
        }

        public String getUuid() {
            return uuid;
        }

        public void setUuid(String uuid) {
            this.uuid = uuid;
        }
    }
}
