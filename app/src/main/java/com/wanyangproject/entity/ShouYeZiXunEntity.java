package com.wanyangproject.entity;

import java.util.List;

/**
 * Created by 甜甜 on 2018/8/28.
 */

public class ShouYeZiXunEntity {

    /**
     * code : 200
     * msg :
     * response : {"id":"2","title":"在党的旗帜下奋进强军新时代","desc":"1","add_time":"4天前","content":"新华社北京８月１６日电题：在党的旗帜下奋进强军新时代\u2014\u2014以习近平同志为核心的党中央领导和推进人民军队党的建设述评　　新华社记者李宣良、梅世雄、梅常伟　解放军报记者欧灿、费士廷、尹航　　旗帜引领方向，思想照亮航程。　　每当历史关口、紧要关头，党的领袖、军队统帅都高瞻远瞩、运筹帷幄、把航掌舵，为我军党的建设指引前进方向。　　２０１３年初春，习主席在全国两会期间鲜明提出\u201c建设一支听党指挥、能打胜仗、作风优良的人民军队\u201d的强军目标，指明了我军党的建设根本使命。　　２０１４年金秋，习主席亲自决策、亲自领导召开古田全军政治工作会议，在关键时刻扶危定倾，开启思想建党、政治建军新征程。　　２０１７年深秋，习主席在党的十九大报告中把\u201c坚持党对人民军队的绝对领导\u201d上升为新时代坚持和发展中国特色社会主义的基本方略。这次大会把习近平新时代中国特色社会主义思想和习近平强军思想鲜明写入党章，为新时代我军党的建设提供了根本遵循\u2026\u2026　　万山磅礴看主峰，风正帆悬起新航。党的十八大以来，以习近平同志为核心的党中央把我军党的建设纳入全面从严治党战略布局统筹推进，领导我军党的建设在直面问题中前进、在勇于变革中加强、在继承创新中发展，取得历史性成就\u2014\u2014　　\u201c四个意识\u201d、\u201c四个自信\u201d、\u201c三个维护\u201d根植军心，全军在党的旗帜下空前团结凝聚；突出军委主席负责制在党领导军队制度体系中的统领地位，党对军队绝对领导全面加强；归正工作重心，推动战斗力标准立起来落下去，各级党委领导备战打仗能力有新跃升；强力正风肃纪反腐，全面彻底肃清郭伯雄、徐才厚流毒影响，回归初心、回归传统、回归本真。"}
     * request : {"id":"2"}
     * other : []
     */

    private int code;
    private String msg;
    private ResponseBean response;
    private RequestBean request;
    private List<?> other;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public ResponseBean getResponse() {
        return response;
    }

    public void setResponse(ResponseBean response) {
        this.response = response;
    }

    public RequestBean getRequest() {
        return request;
    }

    public void setRequest(RequestBean request) {
        this.request = request;
    }

    public List<?> getOther() {
        return other;
    }

    public void setOther(List<?> other) {
        this.other = other;
    }

    public static class ResponseBean {
        /**
         * id : 2
         * title : 在党的旗帜下奋进强军新时代
         * desc : 1
         * add_time : 4天前
         * content : 新华社北京８月１６日电题：在党的旗帜下奋进强军新时代——以习近平同志为核心的党中央领导和推进人民军队党的建设述评　　新华社记者李宣良、梅世雄、梅常伟　解放军报记者欧灿、费士廷、尹航　　旗帜引领方向，思想照亮航程。　　每当历史关口、紧要关头，党的领袖、军队统帅都高瞻远瞩、运筹帷幄、把航掌舵，为我军党的建设指引前进方向。　　２０１３年初春，习主席在全国两会期间鲜明提出“建设一支听党指挥、能打胜仗、作风优良的人民军队”的强军目标，指明了我军党的建设根本使命。　　２０１４年金秋，习主席亲自决策、亲自领导召开古田全军政治工作会议，在关键时刻扶危定倾，开启思想建党、政治建军新征程。　　２０１７年深秋，习主席在党的十九大报告中把“坚持党对人民军队的绝对领导”上升为新时代坚持和发展中国特色社会主义的基本方略。这次大会把习近平新时代中国特色社会主义思想和习近平强军思想鲜明写入党章，为新时代我军党的建设提供了根本遵循……　　万山磅礴看主峰，风正帆悬起新航。党的十八大以来，以习近平同志为核心的党中央把我军党的建设纳入全面从严治党战略布局统筹推进，领导我军党的建设在直面问题中前进、在勇于变革中加强、在继承创新中发展，取得历史性成就——　　“四个意识”、“四个自信”、“三个维护”根植军心，全军在党的旗帜下空前团结凝聚；突出军委主席负责制在党领导军队制度体系中的统领地位，党对军队绝对领导全面加强；归正工作重心，推动战斗力标准立起来落下去，各级党委领导备战打仗能力有新跃升；强力正风肃纪反腐，全面彻底肃清郭伯雄、徐才厚流毒影响，回归初心、回归传统、回归本真。
         */

        private String id;
        private String title;
        private String desc;
        private String add_time;
        private String content;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getDesc() {
            return desc;
        }

        public void setDesc(String desc) {
            this.desc = desc;
        }

        public String getAdd_time() {
            return add_time;
        }

        public void setAdd_time(String add_time) {
            this.add_time = add_time;
        }

        public String getContent() {
            return content;
        }

        public void setContent(String content) {
            this.content = content;
        }
    }

    public static class RequestBean {
        /**
         * id : 2
         */

        private String id;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }
    }
}
