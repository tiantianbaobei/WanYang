package com.wanyangproject.entity;

import java.util.List;

/**
 * Created by 甜甜 on 2018/9/30.
 */

public class ZhiNengZhiZaoXiangQingEntity {

    /**
     * code : 200
     * msg :
     * response : {"id":"3","title":"浙江省委书记车俊率全省各级领导，现场考察平阳万洋众创城","content":"<p><img src=\"http://park.hostop.net/upload/20180913/bd8650bd064558d05221ab080303c49d.jpg\" alt=\"undefined\"><\/p><p style=\"text-align: justify;\"><strong><span>8月30日，浙江省小微企业园建设提升暨\u201c低散乱\u201d整治推进大会在温州举行，浙江省委书记车俊及各市、县（区）主要领导出席大会，并分批莅临平阳万洋众创城考察指导。<\/span><\/strong><\/p><section data-role=\"paragraph\" class=\"\" style=\"text-align: justify;\"><section data-role=\"outer\" label=\"Powered by 135editor.com\"><section class=\"\" data-tools=\"135编辑器\" data-id=\"92736\"><section data-width=\"100%\"><section data-width=\"100%\"><section class=\"\"><p>小微企业园是以解决小企业\u201c低散乱\u201d实际问题为导向设计的集成化抓手，也是解决小微企业发展空间、保护产业链和现实生产力最直接、最紧迫、最有效的重大举措。<\/p><p>我省从小微企业园建设入手，通过高质量小微企业的集聚发展，形成规模化产业集群，从而破解小微企业\u201c低散乱\u201d的痼疾。<\/p><p><strong><span>省委书记车俊指出：没有小微企业的高质量发展，就没有浙江经济的高质量发展。<\/span><\/strong><\/p><p><strong><span>车俊书记同时还提及了万洋众创城：万洋众创城占地866亩，是我省目前已建规模最大、配套功能最完善的小微园项目。<\/span><\/strong><\/p><p><strong><span><img src=\"http://park.hostop.net/upload/20180913/85b460708b975b2ac21143baff57ad7a.jpg\" alt=\"undefined\"><br><\/span><\/strong><\/p><p><span><b><span><span>万洋众创城创新模式受广泛关注<\/span><\/span><span>&nbsp;<\/span><\/b><\/span><\/p><p><span>平阳万洋众创城位于平阳滨海新区，项目用地866.9亩，建设规模约<span>150<\/span>万平方米，总投资<span>35<\/span>亿元。该项目不仅是温州市目前用地规模和开工建筑面积最大的小微园项目<\/span><\/p><p><span>，也是全省目前已建单体面积最大、配套功能最完善的小微园项目。<\/span><\/p><p><span><img src=\"http://park.hostop.net/upload/20180913/ca08f4a7a0a3984f121a22ddd82c5d3c.jpg\" alt=\"undefined\"><br><\/span><\/p><p><span>8月30日上午，大会参会领导分两批考察参观了平阳万洋众创城，平阳县委书记董智武分别向参会领导介绍了平阳县小微园建设情况和平阳万洋众创城的建设运营模式，特别万洋众创城的创新模式受到了领导们的广泛关注<\/span><\/p><p><span><img src=\"http://park.hostop.net/upload/20180913/1083f4c00d8e2acc60268ffb4b2eb452.jpg\" alt=\"undefined\"><br><\/span><\/p><section><span><span class=\"\" data-brushtype=\"text\" style=\"text-align: center;\"><span class=\"\" data-brushtype=\"text\">开发模式<\/span><\/span><span class=\"\" data-brushtype=\"text\" style=\"text-align: center;\"><span>按产业思维打造的制造业集聚平台<\/span><\/span><\/span><\/section><section class=\"\" data-style=\"text-indent: 2em;;\"><p>平阳万洋众创城项目由万洋集团开发建设，是省内首个按产业思维打造的制造业集聚平台。万洋集团在多年建设运营小微园的实践基础上，总结出\u201c产业集聚，产城融合、资源共享、产融互动\u201d的模式，有效整合各方资源，解决治理低散乱和企业转型升级过程中的诸多问题。<\/p><p><img src=\"http://park.hostop.net/upload/20180913/5932aba95cfc8e5a1a453d89b42357d6.jpg\" alt=\"undefined\" style=\"width:50%\"><br><\/p><\/section><\/section><\/section><\/section><\/section><\/section><\/section>","imgArr":[],"add_time":"17天前","isshoucang":"1"}
     * request : {"id":"3","nTypeId":"1"}
     * other : []
     */

    private int code;
    private String msg;
    private ResponseBean response;
    private RequestBean request;
    private List<?> other;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public ResponseBean getResponse() {
        return response;
    }

    public void setResponse(ResponseBean response) {
        this.response = response;
    }

    public RequestBean getRequest() {
        return request;
    }

    public void setRequest(RequestBean request) {
        this.request = request;
    }

    public List<?> getOther() {
        return other;
    }

    public void setOther(List<?> other) {
        this.other = other;
    }

    public static class ResponseBean {
        /**
         * id : 3
         * title : 浙江省委书记车俊率全省各级领导，现场考察平阳万洋众创城
         * content : <p><img src="http://park.hostop.net/upload/20180913/bd8650bd064558d05221ab080303c49d.jpg" alt="undefined"></p><p style="text-align: justify;"><strong><span>8月30日，浙江省小微企业园建设提升暨“低散乱”整治推进大会在温州举行，浙江省委书记车俊及各市、县（区）主要领导出席大会，并分批莅临平阳万洋众创城考察指导。</span></strong></p><section data-role="paragraph" class="" style="text-align: justify;"><section data-role="outer" label="Powered by 135editor.com"><section class="" data-tools="135编辑器" data-id="92736"><section data-width="100%"><section data-width="100%"><section class=""><p>小微企业园是以解决小企业“低散乱”实际问题为导向设计的集成化抓手，也是解决小微企业发展空间、保护产业链和现实生产力最直接、最紧迫、最有效的重大举措。</p><p>我省从小微企业园建设入手，通过高质量小微企业的集聚发展，形成规模化产业集群，从而破解小微企业“低散乱”的痼疾。</p><p><strong><span>省委书记车俊指出：没有小微企业的高质量发展，就没有浙江经济的高质量发展。</span></strong></p><p><strong><span>车俊书记同时还提及了万洋众创城：万洋众创城占地866亩，是我省目前已建规模最大、配套功能最完善的小微园项目。</span></strong></p><p><strong><span><img src="http://park.hostop.net/upload/20180913/85b460708b975b2ac21143baff57ad7a.jpg" alt="undefined"><br></span></strong></p><p><span><b><span><span>万洋众创城创新模式受广泛关注</span></span><span>&nbsp;</span></b></span></p><p><span>平阳万洋众创城位于平阳滨海新区，项目用地866.9亩，建设规模约<span>150</span>万平方米，总投资<span>35</span>亿元。该项目不仅是温州市目前用地规模和开工建筑面积最大的小微园项目</span></p><p><span>，也是全省目前已建单体面积最大、配套功能最完善的小微园项目。</span></p><p><span><img src="http://park.hostop.net/upload/20180913/ca08f4a7a0a3984f121a22ddd82c5d3c.jpg" alt="undefined"><br></span></p><p><span>8月30日上午，大会参会领导分两批考察参观了平阳万洋众创城，平阳县委书记董智武分别向参会领导介绍了平阳县小微园建设情况和平阳万洋众创城的建设运营模式，特别万洋众创城的创新模式受到了领导们的广泛关注</span></p><p><span><img src="http://park.hostop.net/upload/20180913/1083f4c00d8e2acc60268ffb4b2eb452.jpg" alt="undefined"><br></span></p><section><span><span class="" data-brushtype="text" style="text-align: center;"><span class="" data-brushtype="text">开发模式</span></span><span class="" data-brushtype="text" style="text-align: center;"><span>按产业思维打造的制造业集聚平台</span></span></span></section><section class="" data-style="text-indent: 2em;;"><p>平阳万洋众创城项目由万洋集团开发建设，是省内首个按产业思维打造的制造业集聚平台。万洋集团在多年建设运营小微园的实践基础上，总结出“产业集聚，产城融合、资源共享、产融互动”的模式，有效整合各方资源，解决治理低散乱和企业转型升级过程中的诸多问题。</p><p><img src="http://park.hostop.net/upload/20180913/5932aba95cfc8e5a1a453d89b42357d6.jpg" alt="undefined" style="width:50%"><br></p></section></section></section></section></section></section></section>
         * imgArr : []
         * add_time : 17天前
         * isshoucang : 1
         */

        private String id;
        private String title;
        private String content;
        private String add_time;
        private String isshoucang;
        private List<?> imgArr;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getContent() {
            return content;
        }

        public void setContent(String content) {
            this.content = content;
        }

        public String getAdd_time() {
            return add_time;
        }

        public void setAdd_time(String add_time) {
            this.add_time = add_time;
        }

        public String getIsshoucang() {
            return isshoucang;
        }

        public void setIsshoucang(String isshoucang) {
            this.isshoucang = isshoucang;
        }

        public List<?> getImgArr() {
            return imgArr;
        }

        public void setImgArr(List<?> imgArr) {
            this.imgArr = imgArr;
        }
    }

    public static class RequestBean {
        /**
         * id : 3
         * nTypeId : 1
         */

        private String id;
        private String nTypeId;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getNTypeId() {
            return nTypeId;
        }

        public void setNTypeId(String nTypeId) {
            this.nTypeId = nTypeId;
        }
    }
}
