package com.wanyangproject.entity;

import java.util.List;

/**
 * Created by 甜甜 on 2018/8/28.
 */

public class FuWuJiLuJinXingEntity {
    /**
     * code : 200
     * msg :
     * response : [{"id":"542","servicename":"营业执照变更","add_time":"2018-11-29 11:55","entype":"1","state":"0"},{"id":"543","servicename":"保洁绿化服务","add_time":"2018-11-29 11:55","entype":"3","state":"0"},{"id":"541","servicename":"秩序部服务","add_time":"2018-11-29 11:45","entype":"3","state":"0"},{"id":"540","servicename":"工程服务","add_time":"2018-11-29 11:40","entype":"3","state":"0"},{"id":"539","servicename":"营业执照变更","add_time":"2018-11-29 11:36","entype":"1","state":"0"},{"id":"530","servicename":"营业执照变更","add_time":"2018-11-29 09:45","entype":"1","state":"0"},{"id":"529","servicename":"保洁绿化服务","add_time":"2018-11-29 09:44","entype":"3","state":"0"},{"id":"526","servicename":"企业工商年报","add_time":"2018-11-29 09:07","entype":"1","state":"0"},{"id":"525","servicename":"营业执照变更","add_time":"2018-11-29 09:01","entype":"1","state":"0"},{"id":"502","servicename":"保洁绿化服务","add_time":"2018-11-28 15:51","entype":"3","state":"0"},{"id":"495","servicename":"工程服务","add_time":"2018-11-27 17:56","entype":"3","state":"1"},{"id":"489","servicename":"工程服务","add_time":"2018-11-26 16:59","entype":"3","state":"0"},{"id":"485","servicename":"宿舍空调报修","add_time":"2018-11-26 11:07","entype":"3","state":"0"},{"id":"484","servicename":"宿舍空调报修","add_time":"2018-11-26 11:06","entype":"3","state":"0"},{"id":"483","servicename":"工程服务","add_time":"2018-11-25 13:12","entype":"3","state":"1"},{"id":"476","servicename":"变压器申报报装","add_time":"2018-11-23 14:14","entype":"1","state":"0"},{"id":"473","servicename":"变压器申报报装","add_time":"2018-11-23 13:58","entype":"1","state":"0"},{"id":"467","servicename":"变压器申报报装","add_time":"2018-11-23 13:43","entype":"1","state":"0"},{"id":"462","servicename":"变压器申报报装","add_time":"2018-11-23 12:48","entype":"1","state":"0"},{"id":"453","servicename":"变压器申报报装","add_time":"2018-11-22 14:21","entype":"1","state":"1"},{"id":"451","servicename":"变压器申报报装","add_time":"2018-11-22 14:17","entype":"1","state":"1"}]
     * request : {"typeId":"0","pk_id":"1","quan":"0"}
     * other : []
     */

    private int code;
    private String msg;
    private RequestBean request;
    private List<ResponseBean> response;
    private List<?> other;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public RequestBean getRequest() {
        return request;
    }

    public void setRequest(RequestBean request) {
        this.request = request;
    }

    public List<ResponseBean> getResponse() {
        return response;
    }

    public void setResponse(List<ResponseBean> response) {
        this.response = response;
    }

    public List<?> getOther() {
        return other;
    }

    public void setOther(List<?> other) {
        this.other = other;
    }

    public static class RequestBean {
        /**
         * typeId : 0
         * pk_id : 1
         * quan : 0
         */

        private String typeId;
        private String pk_id;
        private String quan;

        public String getTypeId() {
            return typeId;
        }

        public void setTypeId(String typeId) {
            this.typeId = typeId;
        }

        public String getPk_id() {
            return pk_id;
        }

        public void setPk_id(String pk_id) {
            this.pk_id = pk_id;
        }

        public String getQuan() {
            return quan;
        }

        public void setQuan(String quan) {
            this.quan = quan;
        }
    }

    public static class ResponseBean {
        /**
         * id : 542
         * servicename : 营业执照变更
         * add_time : 2018-11-29 11:55
         * entype : 1
         * state : 0
         */

        private String id;
        private String servicename;
        private String add_time;
        private String entype;
        private String state;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getServicename() {
            return servicename;
        }

        public void setServicename(String servicename) {
            this.servicename = servicename;
        }

        public String getAdd_time() {
            return add_time;
        }

        public void setAdd_time(String add_time) {
            this.add_time = add_time;
        }

        public String getEntype() {
            return entype;
        }

        public void setEntype(String entype) {
            this.entype = entype;
        }

        public String getState() {
            return state;
        }

        public void setState(String state) {
            this.state = state;
        }
    }


//
//    /**
//     * code : 200
//     * msg :
//     * response : [{"id":"34","name":"","user_id":"2","pk_id":"1","add_time":"2018-09-09 16:20","type":"1","service":"7","Remarks":"","Refusal":"","operation_time":"","servicename":"企业工商年报","entype":"1"},{"id":"30","name":"","user_id":"2","pk_id":"1","add_time":"2018-09-02 20:27","type":"1","service":"7","Remarks":"","Refusal":"","operation_time":"","servicename":"企业工商年报","entype":"1"},{"id":"28","name":"15789665464","user_id":"2","pk_id":"1","add_time":"2018-09-02 20:01","type":"1","service":"7","Remarks":"备注","Refusal":"","operation_time":"","servicename":"企业工商年报","entype":"1"},{"id":"26","name":"","user_id":"2","pk_id":"1","add_time":"2018-09-02 19:46","type":"1","service":"12","Remarks":"备注","Refusal":"","operation_time":"","servicename":"无偿使用证明","entype":"1"},{"id":"25","name":"","user_id":"2","pk_id":"1","add_time":"2018-09-02 19:43","type":"1","service":"12","Remarks":"备注","Refusal":"","operation_time":"","servicename":"无偿使用证明","entype":"1"},{"id":"24","name":"","user_id":"2","pk_id":"1","add_time":"2018-09-02 19:43","type":"1","service":"12","Remarks":"备注","Refusal":"","operation_time":"","servicename":"无偿使用证明","entype":"1"},{"id":"23","name":"","user_id":"2","pk_id":"1","add_time":"2018-09-02 19:43","type":"1","service":"12","Remarks":"备注","Refusal":"","operation_time":"","servicename":"无偿使用证明","entype":"1"},{"id":"22","name":"","user_id":"2","pk_id":"1","add_time":"2018-09-02 19:41","type":"1","service":"12","Remarks":"备注","Refusal":"","operation_time":"","servicename":"无偿使用证明","entype":"1"},{"id":"21","name":"","user_id":"2","pk_id":"1","add_time":"2018-09-02 19:38","type":"1","service":"12","Remarks":"备注","Refusal":"","operation_time":"","servicename":"无偿使用证明","entype":"1"},{"id":"19","name":"","user_id":"2","pk_id":"1","add_time":"2018-09-02 19:36","type":"1","service":"12","Remarks":"备注","Refusal":"","operation_time":"","servicename":"无偿使用证明","entype":"1"},{"id":"18","name":"","user_id":"2","pk_id":"1","add_time":"2018-09-02 19:35","type":"1","service":"12","Remarks":"备注","Refusal":"","operation_time":"","servicename":"无偿使用证明","entype":"1"}]
//     * request : {"typeId":"0"}
//     * other : []
//     */
//
//    private int code;
//    private String msg;
//    private RequestBean request;
//    private List<ResponseBean> response;
//    private List<?> other;
//
//    public int getCode() {
//        return code;
//    }
//
//    public void setCode(int code) {
//        this.code = code;
//    }
//
//    public String getMsg() {
//        return msg;
//    }
//
//    public void setMsg(String msg) {
//        this.msg = msg;
//    }
//
//    public RequestBean getRequest() {
//        return request;
//    }
//
//    public void setRequest(RequestBean request) {
//        this.request = request;
//    }
//
//    public List<ResponseBean> getResponse() {
//        return response;
//    }
//
//    public void setResponse(List<ResponseBean> response) {
//        this.response = response;
//    }
//
//    public List<?> getOther() {
//        return other;
//    }
//
//    public void setOther(List<?> other) {
//        this.other = other;
//    }
//
//    public static class RequestBean {
//        /**
//         * typeId : 0
//         */
//
//        private String typeId;
//
//        public String getTypeId() {
//            return typeId;
//        }
//
//        public void setTypeId(String typeId) {
//            this.typeId = typeId;
//        }
//    }
//
//    public static class ResponseBean {
//        /**
//         * id : 34
//         * name :
//         * user_id : 2
//         * pk_id : 1
//         * add_time : 2018-09-09 16:20
//         * type : 1
//         * service : 7
//         * Remarks :
//         * Refusal :
//         * operation_time :
//         * servicename : 企业工商年报
//         * entype : 1
//         */
//
//        private String id;
//        private String name;
//        private String user_id;
//        private String pk_id;
//        private String add_time;
//        private String type;
//        private String service;
//        private String Remarks;
//        private String Refusal;
//        private String operation_time;
//        private String servicename;
//        private String entype;
//
//        public String getId() {
//            return id;
//        }
//
//        public void setId(String id) {
//            this.id = id;
//        }
//
//        public String getName() {
//            return name;
//        }
//
//        public void setName(String name) {
//            this.name = name;
//        }
//
//        public String getUser_id() {
//            return user_id;
//        }
//
//        public void setUser_id(String user_id) {
//            this.user_id = user_id;
//        }
//
//        public String getPk_id() {
//            return pk_id;
//        }
//
//        public void setPk_id(String pk_id) {
//            this.pk_id = pk_id;
//        }
//
//        public String getAdd_time() {
//            return add_time;
//        }
//
//        public void setAdd_time(String add_time) {
//            this.add_time = add_time;
//        }
//
//        public String getType() {
//            return type;
//        }
//
//        public void setType(String type) {
//            this.type = type;
//        }
//
//        public String getService() {
//            return service;
//        }
//
//        public void setService(String service) {
//            this.service = service;
//        }
//
//        public String getRemarks() {
//            return Remarks;
//        }
//
//        public void setRemarks(String Remarks) {
//            this.Remarks = Remarks;
//        }
//
//        public String getRefusal() {
//            return Refusal;
//        }
//
//        public void setRefusal(String Refusal) {
//            this.Refusal = Refusal;
//        }
//
//        public String getOperation_time() {
//            return operation_time;
//        }
//
//        public void setOperation_time(String operation_time) {
//            this.operation_time = operation_time;
//        }
//
//        public String getServicename() {
//            return servicename;
//        }
//
//        public void setServicename(String servicename) {
//            this.servicename = servicename;
//        }
//
//        public String getEntype() {
//            return entype;
//        }
//
//        public void setEntype(String entype) {
//            this.entype = entype;
//        }
//    }
}
