package com.wanyangproject.entity;

import java.util.List;

/**
 * Created by 甜甜 on 2018/9/7.
 */

public class QiYeZhaoPinGuanLiEntity {


    /**
     * code : 200
     * msg :
     * response : [{"id":"92","title":"钳工","money":"","yaoqiu":"高中","address":"滨海新区海泽路与阳屿路交叉口","enterName":"温州驰骋制罐有限公司"}]
     * request : {"parkId":"1"}
     * other : []
     */

    private int code;
    private String msg;
    private RequestBean request;
    private List<ResponseBean> response;
    private List<?> other;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public RequestBean getRequest() {
        return request;
    }

    public void setRequest(RequestBean request) {
        this.request = request;
    }

    public List<ResponseBean> getResponse() {
        return response;
    }

    public void setResponse(List<ResponseBean> response) {
        this.response = response;
    }

    public List<?> getOther() {
        return other;
    }

    public void setOther(List<?> other) {
        this.other = other;
    }

    public static class RequestBean {
        /**
         * parkId : 1
         */

        private String parkId;

        public String getParkId() {
            return parkId;
        }

        public void setParkId(String parkId) {
            this.parkId = parkId;
        }
    }

    public static class ResponseBean {
        /**
         * id : 92
         * title : 钳工
         * money :
         * yaoqiu : 高中
         * address : 滨海新区海泽路与阳屿路交叉口
         * enterName : 温州驰骋制罐有限公司
         */

        private String id;
        private String title;
        private String money;
        private String yaoqiu;
        private String address;
        private String enterName;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getMoney() {
            return money;
        }

        public void setMoney(String money) {
            this.money = money;
        }

        public String getYaoqiu() {
            return yaoqiu;
        }

        public void setYaoqiu(String yaoqiu) {
            this.yaoqiu = yaoqiu;
        }

        public String getAddress() {
            return address;
        }

        public void setAddress(String address) {
            this.address = address;
        }

        public String getEnterName() {
            return enterName;
        }

        public void setEnterName(String enterName) {
            this.enterName = enterName;
        }
    }
}
