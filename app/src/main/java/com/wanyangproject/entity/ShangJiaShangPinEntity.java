package com.wanyangproject.entity;

import java.util.List;

/**
 * Created by 甜甜 on 2018/8/7.
 */

public class ShangJiaShangPinEntity {



    /**
     * code : 200
     * msg :
     * response : {"shop":{"shop":{"id":"1","nickname":"彪1","address":"地址","baidu_address":"商家地图","star_time":"02:02:02","end_time":"02:02:02","distribution":"01:01:00 - 05:00:00","phone":"12321","cost":"10","starting":"20","content":"<p>1231<\/p>","sales":"10","photo":"park.hostop.net/static/image/0.jpg"}},"goods":[{"name":"商品","id":"21","goods":[{"goods_name":"测试","id":"4","master":"park.hostop.net//upload/20180804/2da84761de43d358bfd2f89627fdaf42.png","pic":"1","volume":"0","fabulous":"0"},{"goods_name":"商品2","id":"2","master":"park.hostop.net//upload/20180804/963d0fc2a816b184851bb41116478e85.png","pic":"10","volume":"0","fabulous":"0"}]},{"name":"商品","id":"14","goods":[]},{"name":"商品","id":"15","goods":[]},{"name":"商品","id":"16","goods":[]},{"name":"商品","id":"17","goods":[]},{"name":"商品","id":"18","goods":[]},{"name":"商品","id":"19","goods":[]},{"name":"商品","id":"20","goods":[]},{"name":"商品","id":"13","goods":[]},{"name":"商品","id":"12","goods":[]},{"name":"商品","id":"11","goods":[]},{"name":"商品","id":"10","goods":[]},{"name":"商品","id":"9","goods":[]},{"name":"商品","id":"8","goods":[]},{"name":"商品","id":"7","goods":[]},{"name":"商品","id":"6","goods":[]},{"name":"43223","id":"3","goods":[]},{"name":"11","id":"2","goods":[{"goods_name":"测试3423","id":"5","master":"park.hostop.net//upload/20180804/44e7b28fe70e5c75225ca10eb756d3d1.png","pic":"13","volume":"0","fabulous":"0"}]}]}
     * request : {"parkId":"1","shopId":"1"}
     * other : []
     */



    private int code;
    private String msg;
    private ResponseBean response;
    private RequestBean request;
    private List<?> other;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public ResponseBean getResponse() {
        return response;
    }

    public void setResponse(ResponseBean response) {
        this.response = response;
    }

    public RequestBean getRequest() {
        return request;
    }

    public void setRequest(RequestBean request) {
        this.request = request;
    }

    public List<?> getOther() {
        return other;
    }

    public void setOther(List<?> other) {
        this.other = other;
    }

    public static class ResponseBean {
        /**
         * shop : {"shop":{"id":"1","nickname":"彪1","address":"地址","baidu_address":"商家地图","star_time":"02:02:02","end_time":"02:02:02","distribution":"01:01:00 - 05:00:00","phone":"12321","cost":"10","starting":"20","content":"<p>1231<\/p>","sales":"10","photo":"park.hostop.net/static/image/0.jpg"}}
         * goods : [{"name":"商品","id":"21","goods":[{"goods_name":"测试","id":"4","master":"park.hostop.net//upload/20180804/2da84761de43d358bfd2f89627fdaf42.png","pic":"1","volume":"0","fabulous":"0"},{"goods_name":"商品2","id":"2","master":"park.hostop.net//upload/20180804/963d0fc2a816b184851bb41116478e85.png","pic":"10","volume":"0","fabulous":"0"}]},{"name":"商品","id":"14","goods":[]},{"name":"商品","id":"15","goods":[]},{"name":"商品","id":"16","goods":[]},{"name":"商品","id":"17","goods":[]},{"name":"商品","id":"18","goods":[]},{"name":"商品","id":"19","goods":[]},{"name":"商品","id":"20","goods":[]},{"name":"商品","id":"13","goods":[]},{"name":"商品","id":"12","goods":[]},{"name":"商品","id":"11","goods":[]},{"name":"商品","id":"10","goods":[]},{"name":"商品","id":"9","goods":[]},{"name":"商品","id":"8","goods":[]},{"name":"商品","id":"7","goods":[]},{"name":"商品","id":"6","goods":[]},{"name":"43223","id":"3","goods":[]},{"name":"11","id":"2","goods":[{"goods_name":"测试3423","id":"5","master":"park.hostop.net//upload/20180804/44e7b28fe70e5c75225ca10eb756d3d1.png","pic":"13","volume":"0","fabulous":"0"}]}]
         */

        private ShopBeanX shop;
        private List<GoodsBeanX> goods;

        public ShopBeanX getShop() {
            return shop;
        }

        public void setShop(ShopBeanX shop) {
            this.shop = shop;
        }

        public List<GoodsBeanX> getGoods() {
            return goods;
        }

        public void setGoods(List<GoodsBeanX> goods) {
            this.goods = goods;
        }

        public static class ShopBeanX {
            /**
             * shop : {"id":"1","shophoto":"",nickname":"彪1","address":"地址","baidu_address":"商家地图","star_time":"02:02:02","end_time":"02:02:02","distribution":"01:01:00 - 05:00:00","phone":"12321","cost":"10","starting":"20","content":"<p>1231<\/p>","sales":"10","photo":"park.hostop.net/static/image/0.jpg"}
             */

            private ShopBean shop;

            public ShopBean getShop() {
                return shop;
            }

            public void setShop(ShopBean shop) {
                this.shop = shop;
            }

            public static class ShopBean {
                /**
                 * id : 1
                 * nickname : 彪1
                 * address : 地址
                 * baidu_address : 商家地图
                 * star_time : 02:02:02
                 * end_time : 02:02:02
                 * distribution : 01:01:00 - 05:00:00
                 * phone : 12321
                 * cost : 10
                 * starting : 20
                 * content : <p>1231</p>
                 * sales : 10
                 * photo : park.hostop.net/static/image/0.jpg
                 * shophoto :park.hostop.net\/\/upload\/20180918\/cdb80c3eb7a4bb53d131585daa8b00e5.jpg
                 */

                private String id;
                private String nickname;
                private String address;
                private String baidu_address;
                private String star_time;
                private String end_time;
                private String distribution;
                private String phone;
                private String cost;
                private String starting;
                private String content;
                private String sales;
                private String photo;
                private String shophoto;
                private String collection;
                private String lat;
                private String lng;


                public String getLat() {
                    return lat;
                }

                public void setLat(String lat) {
                    this.lat = lat;
                }

                public String getLng() {
                    return lng;
                }

                public void setLng(String lng) {
                    this.lng = lng;
                }

                public String getCollection() {
                    return collection;
                }

                public void setCollection(String collection) {
                    this.collection = collection;
                }

                public String getShophoto() {
                    return shophoto;
                }

                public void setShophoto(String shophoto) {
                    this.shophoto = shophoto;
                }

                public String getId() {
                    return id;
                }

                public void setId(String id) {
                    this.id = id;
                }

                public String getNickname() {
                    return nickname;
                }

                public void setNickname(String nickname) {
                    this.nickname = nickname;
                }

                public String getAddress() {
                    return address;
                }

                public void setAddress(String address) {
                    this.address = address;
                }

                public String getBaidu_address() {
                    return baidu_address;
                }

                public void setBaidu_address(String baidu_address) {
                    this.baidu_address = baidu_address;
                }

                public String getStar_time() {
                    return star_time;
                }

                public void setStar_time(String star_time) {
                    this.star_time = star_time;
                }

                public String getEnd_time() {
                    return end_time;
                }

                public void setEnd_time(String end_time) {
                    this.end_time = end_time;
                }

                public String getDistribution() {
                    return distribution;
                }

                public void setDistribution(String distribution) {
                    this.distribution = distribution;
                }

                public String getPhone() {
                    return phone;
                }

                public void setPhone(String phone) {
                    this.phone = phone;
                }

                public String getCost() {
                    return cost;
                }

                public void setCost(String cost) {
                    this.cost = cost;
                }

                public String getStarting() {
                    return starting;
                }

                public void setStarting(String starting) {
                    this.starting = starting;
                }

                public String getContent() {
                    return content;
                }

                public void setContent(String content) {
                    this.content = content;
                }

                public String getSales() {
                    return sales;
                }

                public void setSales(String sales) {
                    this.sales = sales;
                }

                public String getPhoto() {
                    return photo;
                }

                public void setPhoto(String photo) {
                    this.photo = photo;
                }
            }
        }

        public static class GoodsBeanX {
            /**
             * name : 商品
             * id : 21
             * goods : [{"goods_name":"测试","id":"4","master":"park.hostop.net//upload/20180804/2da84761de43d358bfd2f89627fdaf42.png","pic":"1","volume":"0","fabulous":"0"},{"goods_name":"商品2","id":"2","master":"park.hostop.net//upload/20180804/963d0fc2a816b184851bb41116478e85.png","pic":"10","volume":"0","fabulous":"0","stock": "1","market": "100"}]
             */

            private String name;
            private String id;
            private List<GoodsBean> goods;

            @Override
            public String toString() {
                return "GoodsBeanX{" +
                        "goods=" + goods +
                        '}';
            }

            private boolean ok;

            public boolean isOk() {
                return ok;
            }

            public void setOk(boolean ok) {
                this.ok = ok;
            }





            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }

            public String getId() {
                return id;
            }

            public void setId(String id) {
                this.id = id;
            }

            public List<GoodsBean> getGoods() {
                return goods;
            }

            public void setGoods(List<GoodsBean> goods) {
                this.goods = goods;
            }

            public static class GoodsBean {
                /**
                 * goods_name : 测试
                 * id : 4
                 * master : park.hostop.net//upload/20180804/2da84761de43d358bfd2f89627fdaf42.png
                 * pic : 1
                 * volume : 0
                 * fabulous : 0
                 * stock: 1,
                 * market": 100
                 */

                private String goods_name;
                private String id;
                private String master;
                private String pic;
                private String volume;
                private String fabulous;
                private String stock;
                private String market;

                public String getStock() {
                    return stock;
                }

                public void setStock(String stock) {
                    this.stock = stock;
                }

                public String getMarket() {
                    return market;
                }

                public void setMarket(String market) {
                    this.market = market;
                }

                public String getGoods_name() {
                    return goods_name;
                }

                public void setGoods_name(String goods_name) {
                    this.goods_name = goods_name;
                }

                public String getId() {
                    return id;
                }

                public void setId(String id) {
                    this.id = id;
                }

                public String getMaster() {
                    return master;
                }

                public void setMaster(String master) {
                    this.master = master;
                }

                public String getPic() {
                    return pic;
                }

                public void setPic(String pic) {
                    this.pic = pic;
                }

                public String getVolume() {
                    return volume;
                }

                public void setVolume(String volume) {
                    this.volume = volume;
                }

                public String getFabulous() {
                    return fabulous;
                }

                public void setFabulous(String fabulous) {
                    this.fabulous = fabulous;
                }
            }
        }
    }

    public static class RequestBean {
        /**
         * parkId : 1
         * shopId : 1
         */

        private String parkId;
        private String shopId;

        public String getParkId() {
            return parkId;
        }

        public void setParkId(String parkId) {
            this.parkId = parkId;
        }

        public String getShopId() {
            return shopId;
        }

        public void setShopId(String shopId) {
            this.shopId = shopId;
        }
    }
}
