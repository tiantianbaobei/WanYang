package com.wanyangproject.entity;

import java.util.List;

/**
 * Created by 甜甜 on 2018/8/17.
 */

public class ShouHuoDiZhiSheZhiMoRenEntity {

    /**
     * code : 200
     * msg :
     * response : {"meg":"设置成功！"}
     * request : {"parkId":"1","addressid":"4"}
     * other : []
     */

    private int code;
    private String msg;
    private ResponseBean response;
    private RequestBean request;
    private List<?> other;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public ResponseBean getResponse() {
        return response;
    }

    public void setResponse(ResponseBean response) {
        this.response = response;
    }

    public RequestBean getRequest() {
        return request;
    }

    public void setRequest(RequestBean request) {
        this.request = request;
    }

    public List<?> getOther() {
        return other;
    }

    public void setOther(List<?> other) {
        this.other = other;
    }

    public static class ResponseBean {
        /**
         * meg : 设置成功！
         */

        private String meg;

        public String getMeg() {
            return meg;
        }

        public void setMeg(String meg) {
            this.meg = meg;
        }
    }

    public static class RequestBean {
        /**
         * parkId : 1
         * addressid : 4
         */

        private String parkId;
        private String addressid;

        public String getParkId() {
            return parkId;
        }

        public void setParkId(String parkId) {
            this.parkId = parkId;
        }

        public String getAddressid() {
            return addressid;
        }

        public void setAddressid(String addressid) {
            this.addressid = addressid;
        }
    }
}
