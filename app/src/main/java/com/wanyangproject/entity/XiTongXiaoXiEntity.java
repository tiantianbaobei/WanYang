package com.wanyangproject.entity;

import java.util.List;

/**
 * Created by 甜甜 on 2018/8/27.
 */

public class XiTongXiaoXiEntity {


    /**
     * code : 200
     * msg :
     * response : [{"id":"18","title":"访客提醒","pk_id":"1","user_id":"2","contes":"请您到达园区时出示二维码。","type":"6","add_time":"2018-08-59 08:37:59","look":"2","Visitor":"2","photo":"upload/qrcode/2018-08-30/1081fa33f993aa125d2e5121d5ccaf7761.png"},{"id":"17","title":"访客提醒","pk_id":"1","user_id":"2","contes":"请您到达园区时出示二维码。","type":"6","add_time":"2018-08-39 08:33:39","look":"2","Visitor":"2","photo":"upload/qrcode/2018-08-30/11f18dc32294a2eee1a41206f40eb715c1.png"},{"id":"16","title":"访客提醒","pk_id":"1","user_id":"2","contes":"请您到达园区时出示二维码。","type":"6","add_time":"2018-08-33 11:52:33","look":"2","Visitor":"2","photo":"upload/qrcode/2018-08-29/1079b2ed8cc8b01acfb0037854368737d1.png"},{"id":"15","title":"访客提醒","pk_id":"1","user_id":"2","contes":"请您到达园区时出示二维码。","type":"6","add_time":"2018-08-15 11:49:15","look":"2","Visitor":"2","photo":"upload/qrcode/2018-08-29/10a486e1c1da10450c2f9877f5362c29a1.png"},{"id":"14","title":"访客提醒","pk_id":"1","user_id":"2","contes":"请您到达园区时出示二维码。","type":"6","add_time":"2018-08-16 11:40:16","look":"2","Visitor":"2","photo":"upload/qrcode/2018-08-29/1d84e89f721d77587f23e2187b5ba5e601.png"},{"id":"13","title":"访客提醒","pk_id":"1","user_id":"2","contes":"请您到达园区时出示二维码。","type":"6","add_time":"2018-08-15 11:38:15","look":"2","Visitor":"2","photo":"upload/qrcode/2018-08-29/115b8709415357932eee5523115ec28a31.png"},{"id":"12","title":"访客提醒","pk_id":"1","user_id":"2","contes":"请您到达园区时出示二维码。","type":"6","add_time":"2018-08-35 11:31:35","look":"2","Visitor":"2","photo":"upload/qrcode/2018-08-29/19ad789edc8c65ec3268420896832ca0e1.png"},{"id":"11","title":"访客提醒","pk_id":"1","user_id":"2","contes":"请您到达园区时出示二维码。","type":"6","add_time":"2018-08-36 11:25:36","look":"2","Visitor":"2","photo":"upload/qrcode/2018-08-29/1f4e4104ef1d56943e470ff044974e7771.png"},{"id":"10","title":"访客提醒","pk_id":"1","user_id":"2","contes":"请您到达园区时出示二维码。","type":"6","add_time":"2018-08-49 11:16:49","look":"2","Visitor":"2","photo":"upload/qrcode/2018-08-29/13d316691d91efa8d22bc603311602f9f1.png"},{"id":"8","title":"访客提醒","pk_id":"1","user_id":"2","contes":"请您到达园区时出示二维码。","type":"6","add_time":"2018-08-45 11:08:45","look":"2","Visitor":"2","photo":"upload/qrcode/2018-08-29/18dc801b7abd7fc6f75153d40a33946af1.png"}]
     * request : {"parkId":"1","page":"1"}
     * other : []
     */

    private int code;
    private String msg;
    private RequestBean request;
    private List<ResponseBean> response;
    private List<?> other;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public RequestBean getRequest() {
        return request;
    }

    public void setRequest(RequestBean request) {
        this.request = request;
    }

    public List<ResponseBean> getResponse() {
        return response;
    }

    public void setResponse(List<ResponseBean> response) {
        this.response = response;
    }

    public List<?> getOther() {
        return other;
    }

    public void setOther(List<?> other) {
        this.other = other;
    }

    public static class RequestBean {
        /**
         * parkId : 1
         * page : 1
         */

        private String parkId;
        private String page;

        public String getParkId() {
            return parkId;
        }

        public void setParkId(String parkId) {
            this.parkId = parkId;
        }

        public String getPage() {
            return page;
        }

        public void setPage(String page) {
            this.page = page;
        }
    }

    public static class ResponseBean {
        /**
         * id : 18
         * title : 访客提醒
         * pk_id : 1
         * user_id : 2
         * contes : 请您到达园区时出示二维码。
         * type : 6
         * add_time : 2018-08-59 08:37:59
         * look : 2
         * Visitor : 2
         * photo : upload/qrcode/2018-08-30/1081fa33f993aa125d2e5121d5ccaf7761.png
         */

        private String id;
        private String title;
        private String pk_id;
        private String user_id;
        private String contes;
        private String type;
        private String add_time;
        private String look;
        private String Visitor;
        private String photo;
        private boolean zhuangtai = false;
        private boolean zhuangtai1 = false;


        public boolean isZhuangtai() {
            return zhuangtai;
        }

        public void setZhuangtai(boolean zhuangtai) {
            this.zhuangtai = zhuangtai;
        }

        public boolean isZhuangtai1() {
            return zhuangtai1;
        }

        public void setZhuangtai1(boolean zhuangtai1) {
            this.zhuangtai1 = zhuangtai1;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getPk_id() {
            return pk_id;
        }

        public void setPk_id(String pk_id) {
            this.pk_id = pk_id;
        }

        public String getUser_id() {
            return user_id;
        }

        public void setUser_id(String user_id) {
            this.user_id = user_id;
        }

        public String getContes() {
            return contes;
        }

        public void setContes(String contes) {
            this.contes = contes;
        }

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public String getAdd_time() {
            return add_time;
        }

        public void setAdd_time(String add_time) {
            this.add_time = add_time;
        }

        public String getLook() {
            return look;
        }

        public void setLook(String look) {
            this.look = look;
        }

        public String getVisitor() {
            return Visitor;
        }

        public void setVisitor(String Visitor) {
            this.Visitor = Visitor;
        }

        public String getPhoto() {
            return photo;
        }

        public void setPhoto(String photo) {
            this.photo = photo;
        }
    }
}
