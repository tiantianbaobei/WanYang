package com.wanyangproject.entity;

import java.util.List;

/**
 * Created by 甜甜 on 2018/9/10.
 */

public class MyLunTanPingLunEntity {
    /**
     * code : 200
     * msg :
     * response : [{"id":"250","title":"地铁、公交、摩托、步行\u2026\u2026上千万北京青年的通勤之路","content":"\r\n\t地铁、公交、摩托、步行，还有打车和共享单车，多种...","add_time":"1544583459","uinfo":{"userid":"135","avatar":"http://park.vyzcc.com/static/image/logo.png","nickname":"wy523796","username":"18410710711"},"imgArr":["http://park.vyzcc.com/upload/20181211/987ba9c6d263c3100926096956eaf349.jpg"],"reply":"1","like":"2","isLike":"0","status":"1","setTop":"0","isPc":"1","replyContent":"123","replyId":"318","replystatusName":"审核通过"}]
     * request : []
     * other : []
     */

    private int code;
    private String msg;
    private List<ResponseBean> response;
    private List<?> request;
    private List<?> other;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public List<ResponseBean> getResponse() {
        return response;
    }

    public void setResponse(List<ResponseBean> response) {
        this.response = response;
    }

    public List<?> getRequest() {
        return request;
    }

    public void setRequest(List<?> request) {
        this.request = request;
    }

    public List<?> getOther() {
        return other;
    }

    public void setOther(List<?> other) {
        this.other = other;
    }

    public static class ResponseBean {
        /**
         * id : 250
         * title : 地铁、公交、摩托、步行……上千万北京青年的通勤之路
         * content :
         地铁、公交、摩托、步行，还有打车和共享单车，多种...
         * add_time : 1544583459
         * uinfo : {"userid":"135","avatar":"http://park.vyzcc.com/static/image/logo.png","nickname":"wy523796","username":"18410710711"}
         * imgArr : ["http://park.vyzcc.com/upload/20181211/987ba9c6d263c3100926096956eaf349.jpg"]
         * reply : 1
         * like : 2
         * isLike : 0
         * status : 1
         * setTop : 0
         * isPc : 1
         * replyContent : 123
         * replyId : 318
         * replystatusName : 审核通过
         */

        private String id;
        private String title;
        private String content;
        private String add_time;
        private UinfoBean uinfo;
        private String reply;
        private String like;
        private String isLike;
        private String status;
        private String setTop;
        private String isPc;
        private String replyContent;
        private String replyId;
        private String replystatusName;
        private List<String> imgArr;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getContent() {
            return content;
        }

        public void setContent(String content) {
            this.content = content;
        }

        public String getAdd_time() {
            return add_time;
        }

        public void setAdd_time(String add_time) {
            this.add_time = add_time;
        }

        public UinfoBean getUinfo() {
            return uinfo;
        }

        public void setUinfo(UinfoBean uinfo) {
            this.uinfo = uinfo;
        }

        public String getReply() {
            return reply;
        }

        public void setReply(String reply) {
            this.reply = reply;
        }

        public String getLike() {
            return like;
        }

        public void setLike(String like) {
            this.like = like;
        }

        public String getIsLike() {
            return isLike;
        }

        public void setIsLike(String isLike) {
            this.isLike = isLike;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getSetTop() {
            return setTop;
        }

        public void setSetTop(String setTop) {
            this.setTop = setTop;
        }

        public String getIsPc() {
            return isPc;
        }

        public void setIsPc(String isPc) {
            this.isPc = isPc;
        }

        public String getReplyContent() {
            return replyContent;
        }

        public void setReplyContent(String replyContent) {
            this.replyContent = replyContent;
        }

        public String getReplyId() {
            return replyId;
        }

        public void setReplyId(String replyId) {
            this.replyId = replyId;
        }

        public String getReplystatusName() {
            return replystatusName;
        }

        public void setReplystatusName(String replystatusName) {
            this.replystatusName = replystatusName;
        }

        public List<String> getImgArr() {
            return imgArr;
        }

        public void setImgArr(List<String> imgArr) {
            this.imgArr = imgArr;
        }

        public static class UinfoBean {
            /**
             * userid : 135
             * avatar : http://park.vyzcc.com/static/image/logo.png
             * nickname : wy523796
             * username : 18410710711
             */

            private String userid;
            private String avatar;
            private String nickname;
            private String username;

            public String getUserid() {
                return userid;
            }

            public void setUserid(String userid) {
                this.userid = userid;
            }

            public String getAvatar() {
                return avatar;
            }

            public void setAvatar(String avatar) {
                this.avatar = avatar;
            }

            public String getNickname() {
                return nickname;
            }

            public void setNickname(String nickname) {
                this.nickname = nickname;
            }

            public String getUsername() {
                return username;
            }

            public void setUsername(String username) {
                this.username = username;
            }
        }
    }


//    /**
//     * code : 200
//     * msg :
//     * response : [{"id":"115","title":"大家都知道，奈特十字韧带断裂已...","add_time":"1537002618","uinfo":{"userid":"6","avatar":"http://park.hostop.net/upload/20180918/ab04c4709880c285df0a11759b50d72f.png","nickname":"迷你屋","username":"18301437927"},"imgArr":[""],"reply":"4","like":"3","isLike":"0","status":"1","setTop":"0","replyContent":"2014到2017年奈特的高阶都是联盟倒数级别，加上近一年多没打球，就算恢复的还可以也没啥用","replyId":"166","replystatusName":"已发布"},{"id":"115","title":"大家都知道，奈特十字韧带断裂已...","add_time":"1537002606","uinfo":{"userid":"6","avatar":"http://park.hostop.net/upload/20180918/ab04c4709880c285df0a11759b50d72f.png","nickname":"迷你屋","username":"18301437927"},"imgArr":[""],"reply":"4","like":"3","isLike":"0","status":"1","setTop":"0","replyContent":"2014到2017年奈特的高阶都是联盟倒数级别，加上近一年多没打球，就算恢复的还可以也没啥用","replyId":"165","replystatusName":"已发布"},{"id":"115","title":"大家都知道，奈特十字韧带断裂已...","add_time":"1537002485","uinfo":{"userid":"6","avatar":"http://park.hostop.net/upload/20180918/ab04c4709880c285df0a11759b50d72f.png","nickname":"迷你屋","username":"18301437927"},"imgArr":[""],"reply":"4","like":"3","isLike":"0","status":"1","setTop":"0","replyContent":"在哪里","replyId":"164","replystatusName":"已发布"}]
//     * request : []
//     * other : []
//     */
//
//    private int code;
//    private String msg;
//    private List<ResponseBean> response;
//    private List<?> request;
//    private List<?> other;
//
//    public int getCode() {
//        return code;
//    }
//
//    public void setCode(int code) {
//        this.code = code;
//    }
//
//    public String getMsg() {
//        return msg;
//    }
//
//    public void setMsg(String msg) {
//        this.msg = msg;
//    }
//
//    public List<ResponseBean> getResponse() {
//        return response;
//    }
//
//    public void setResponse(List<ResponseBean> response) {
//        this.response = response;
//    }
//
//    public List<?> getRequest() {
//        return request;
//    }
//
//    public void setRequest(List<?> request) {
//        this.request = request;
//    }
//
//    public List<?> getOther() {
//        return other;
//    }
//
//    public void setOther(List<?> other) {
//        this.other = other;
//    }
//
//    public static class ResponseBean {
//        /**
//         * id : 115
//         * title : 大家都知道，奈特十字韧带断裂已...
//         * add_time : 1537002618
//         * uinfo : {"userid":"6","avatar":"http://park.hostop.net/upload/20180918/ab04c4709880c285df0a11759b50d72f.png","nickname":"迷你屋","username":"18301437927"}
//         * imgArr : [""]
//         * reply : 4
//         * like : 3
//         * isLike : 0
//         * status : 1
//         * setTop : 0
//         * replyContent : 2014到2017年奈特的高阶都是联盟倒数级别，加上近一年多没打球，就算恢复的还可以也没啥用
//         * replyId : 166
//         * replystatusName : 已发布
//         */
//
//        private String id;
//        private String title;
//        private String add_time;
//        private UinfoBean uinfo;
//        private String reply;
//        private String like;
//        private String isLike;
//        private String status;
//        private String setTop;
//        private String replyContent;
//        private String replyId;
//        private String replystatusName;
//        private List<String> imgArr;
//
//        public String getId() {
//            return id;
//        }
//
//        public void setId(String id) {
//            this.id = id;
//        }
//
//        public String getTitle() {
//            return title;
//        }
//
//        public void setTitle(String title) {
//            this.title = title;
//        }
//
//        public String getAdd_time() {
//            return add_time;
//        }
//
//        public void setAdd_time(String add_time) {
//            this.add_time = add_time;
//        }
//
//        public UinfoBean getUinfo() {
//            return uinfo;
//        }
//
//        public void setUinfo(UinfoBean uinfo) {
//            this.uinfo = uinfo;
//        }
//
//        public String getReply() {
//            return reply;
//        }
//
//        public void setReply(String reply) {
//            this.reply = reply;
//        }
//
//        public String getLike() {
//            return like;
//        }
//
//        public void setLike(String like) {
//            this.like = like;
//        }
//
//        public String getIsLike() {
//            return isLike;
//        }
//
//        public void setIsLike(String isLike) {
//            this.isLike = isLike;
//        }
//
//        public String getStatus() {
//            return status;
//        }
//
//        public void setStatus(String status) {
//            this.status = status;
//        }
//
//        public String getSetTop() {
//            return setTop;
//        }
//
//        public void setSetTop(String setTop) {
//            this.setTop = setTop;
//        }
//
//        public String getReplyContent() {
//            return replyContent;
//        }
//
//        public void setReplyContent(String replyContent) {
//            this.replyContent = replyContent;
//        }
//
//        public String getReplyId() {
//            return replyId;
//        }
//
//        public void setReplyId(String replyId) {
//            this.replyId = replyId;
//        }
//
//        public String getReplystatusName() {
//            return replystatusName;
//        }
//
//        public void setReplystatusName(String replystatusName) {
//            this.replystatusName = replystatusName;
//        }
//
//        public List<String> getImgArr() {
//            return imgArr;
//        }
//
//        public void setImgArr(List<String> imgArr) {
//            this.imgArr = imgArr;
//        }
//
//        public static class UinfoBean {
//            /**
//             * userid : 6
//             * avatar : http://park.hostop.net/upload/20180918/ab04c4709880c285df0a11759b50d72f.png
//             * nickname : 迷你屋
//             * username : 18301437927
//             */
//
//            private String userid;
//            private String avatar;
//            private String nickname;
//            private String username;
//
//            public String getUserid() {
//                return userid;
//            }
//
//            public void setUserid(String userid) {
//                this.userid = userid;
//            }
//
//            public String getAvatar() {
//                return avatar;
//            }
//
//            public void setAvatar(String avatar) {
//                this.avatar = avatar;
//            }
//
//            public String getNickname() {
//                return nickname;
//            }
//
//            public void setNickname(String nickname) {
//                this.nickname = nickname;
//            }
//
//            public String getUsername() {
//                return username;
//            }
//
//            public void setUsername(String username) {
//                this.username = username;
//            }
//        }
//    }






















//    /**
//     * code : 200
//     * msg :
//     * response : [{"id":"25","title":"从前有座灵剑山孔酷我all，空间...","add_time":"1537262426","uinfo":{"userid":"2","avatar":"http://park.hostop.net/upload/20180915/6755fdb2ff9a3f6cdb253300aecc5f69.jpg","nickname":"呵呵呵","username":"17600904682"},"imgArr":["http://park.hostop.net/upload/20180828/5d2f58bcea15b58052b20c7b1ffb34dc.jpg","http://park.hostop.net/upload/20180828/bb91f70215d68008327d41f98635dcde.jpg","http://park.hostop.net/upload/20180828/258924a445e8088b9b26fe7cf05f4bfe.jpg"],"reply":"4","like":"1","isLike":"0","status":"1","setTop":"0","replyContent":"测试917","replyId":"178","replystatusName":"已发布"}]
//     * request : []
//     * other : []
//     */
//
//    private int code;
//    private String msg;
//    private List<ResponseBean> response;
//    private List<?> request;
//    private List<?> other;
//
//    public int getCode() {
//        return code;
//    }
//
//    public void setCode(int code) {
//        this.code = code;
//    }
//
//    public String getMsg() {
//        return msg;
//    }
//
//    public void setMsg(String msg) {
//        this.msg = msg;
//    }
//
//    public List<ResponseBean> getResponse() {
//        return response;
//    }
//
//    public void setResponse(List<ResponseBean> response) {
//        this.response = response;
//    }
//
//    public List<?> getRequest() {
//        return request;
//    }
//
//    public void setRequest(List<?> request) {
//        this.request = request;
//    }
//
//    public List<?> getOther() {
//        return other;
//    }
//
//    public void setOther(List<?> other) {
//        this.other = other;
//    }
//
//    public static class ResponseBean {
//        /**
//         * id : 25
//         * title : 从前有座灵剑山孔酷我all，空间...
//         * add_time : 1537262426
//         * uinfo : {"userid":"2","avatar":"http://park.hostop.net/upload/20180915/6755fdb2ff9a3f6cdb253300aecc5f69.jpg","nickname":"呵呵呵","username":"17600904682"}
//         * imgArr : ["http://park.hostop.net/upload/20180828/5d2f58bcea15b58052b20c7b1ffb34dc.jpg","http://park.hostop.net/upload/20180828/bb91f70215d68008327d41f98635dcde.jpg","http://park.hostop.net/upload/20180828/258924a445e8088b9b26fe7cf05f4bfe.jpg"]
//         * reply : 4
//         * like : 1
//         * isLike : 0
//         * status : 1
//         * setTop : 0
//         * replyContent : 测试917
//         * replyId : 178
//         * replystatusName : 已发布
//         */
//
//        private String id;
//        private String title;
//        private String add_time;
//        private UinfoBean uinfo;
//        private String reply;
//        private String like;
//        private String isLike;
//        private String status;
//        private String setTop;
//        private String replyContent;
//        private String replyId;
//        private String replystatusName;
//        private List<String> imgArr;
//
//        public String getId() {
//            return id;
//        }
//
//        public void setId(String id) {
//            this.id = id;
//        }
//
//        public String getTitle() {
//            return title;
//        }
//
//        public void setTitle(String title) {
//            this.title = title;
//        }
//
//        public String getAdd_time() {
//            return add_time;
//        }
//
//        public void setAdd_time(String add_time) {
//            this.add_time = add_time;
//        }
//
//        public UinfoBean getUinfo() {
//            return uinfo;
//        }
//
//        public void setUinfo(UinfoBean uinfo) {
//            this.uinfo = uinfo;
//        }
//
//        public String getReply() {
//            return reply;
//        }
//
//        public void setReply(String reply) {
//            this.reply = reply;
//        }
//
//        public String getLike() {
//            return like;
//        }
//
//        public void setLike(String like) {
//            this.like = like;
//        }
//
//        public String getIsLike() {
//            return isLike;
//        }
//
//        public void setIsLike(String isLike) {
//            this.isLike = isLike;
//        }
//
//        public String getStatus() {
//            return status;
//        }
//
//        public void setStatus(String status) {
//            this.status = status;
//        }
//
//        public String getSetTop() {
//            return setTop;
//        }
//
//        public void setSetTop(String setTop) {
//            this.setTop = setTop;
//        }
//
//        public String getReplyContent() {
//            return replyContent;
//        }
//
//        public void setReplyContent(String replyContent) {
//            this.replyContent = replyContent;
//        }
//
//        public String getReplyId() {
//            return replyId;
//        }
//
//        public void setReplyId(String replyId) {
//            this.replyId = replyId;
//        }
//
//        public String getReplystatusName() {
//            return replystatusName;
//        }
//
//        public void setReplystatusName(String replystatusName) {
//            this.replystatusName = replystatusName;
//        }
//
//        public List<String> getImgArr() {
//            return imgArr;
//        }
//
//        public void setImgArr(List<String> imgArr) {
//            this.imgArr = imgArr;
//        }
//
//        public static class UinfoBean {
//            /**
//             * userid : 2
//             * avatar : http://park.hostop.net/upload/20180915/6755fdb2ff9a3f6cdb253300aecc5f69.jpg
//             * nickname : 呵呵呵
//             * username : 17600904682
//             */
//
//            private String userid;
//            private String avatar;
//            private String nickname;
//            private String username;
//
//            public String getUserid() {
//                return userid;
//            }
//
//            public void setUserid(String userid) {
//                this.userid = userid;
//            }
//
//            public String getAvatar() {
//                return avatar;
//            }
//
//            public void setAvatar(String avatar) {
//                this.avatar = avatar;
//            }
//
//            public String getNickname() {
//                return nickname;
//            }
//
//            public void setNickname(String nickname) {
//                this.nickname = nickname;
//            }
//
//            public String getUsername() {
//                return username;
//            }
//
//            public void setUsername(String username) {
//                this.username = username;
//            }
//        }
//    }





















//
//    /**
//     * code : 200
//     * msg :
//     * response : [{"id":"74","title":"测试","add_time":"6天前","uinfo":{"userid":"11","avatar":"http://park.hostop.net/static/image/logo.png","nickname":"未填写","username":"18857711932"},"imgArr":["http://park.hostop.net/upload/20180905/2195e7e37d56455b78dc65d461ca69de.png"],"reply":"4","like":"1","isLike":"0","status":"1","replyContent":"可口可乐了","replyId":"95"},{"id":"74","title":"测试","add_time":"6天前","uinfo":{"userid":"11","avatar":"http://park.hostop.net/static/image/logo.png","nickname":"未填写","username":"18857711932"},"imgArr":["http://park.hostop.net/upload/20180905/2195e7e37d56455b78dc65d461ca69de.png"],"reply":"4","like":"1","isLike":"0","status":"1","replyContent":"哈哈哈","replyId":"94"}]
//     * request : []
//     * other : []
//     */
//
//    private int code;
//    private String msg;
//    private List<ResponseBean> response;
//    private List<?> request;
//    private List<?> other;
//
//    public int getCode() {
//        return code;
//    }
//
//    public void setCode(int code) {
//        this.code = code;
//    }
//
//    public String getMsg() {
//        return msg;
//    }
//
//    public void setMsg(String msg) {
//        this.msg = msg;
//    }
//
//    public List<ResponseBean> getResponse() {
//        return response;
//    }
//
//    public void setResponse(List<ResponseBean> response) {
//        this.response = response;
//    }
//
//    public List<?> getRequest() {
//        return request;
//    }
//
//    public void setRequest(List<?> request) {
//        this.request = request;
//    }
//
//    public List<?> getOther() {
//        return other;
//    }
//
//    public void setOther(List<?> other) {
//        this.other = other;
//    }
//
//    public static class ResponseBean {
//        /**
//         * id : 74
//         * title : 测试
//         * add_time : 6天前
//         * uinfo : {"userid":"11","avatar":"http://park.hostop.net/static/image/logo.png","nickname":"未填写","username":"18857711932"}
//         * imgArr : ["http://park.hostop.net/upload/20180905/2195e7e37d56455b78dc65d461ca69de.png"]
//         * reply : 4
//         * like : 1
//         * isLike : 0
//         * status : 1
//         * replyContent : 可口可乐了
//         * replyId : 95
//         */
//
//        private String id;
//        private String title;
//        private String add_time;
//        private UinfoBean uinfo;
//        private String reply;
//        private String like;
//        private String isLike;
//        private String status;
//        private String replyContent;
//        private String replyId;
//        private List<String> imgArr;
//
//        public String getId() {
//            return id;
//        }
//
//        public void setId(String id) {
//            this.id = id;
//        }
//
//        public String getTitle() {
//            return title;
//        }
//
//        public void setTitle(String title) {
//            this.title = title;
//        }
//
//        public String getAdd_time() {
//            return add_time;
//        }
//
//        public void setAdd_time(String add_time) {
//            this.add_time = add_time;
//        }
//
//        public UinfoBean getUinfo() {
//            return uinfo;
//        }
//
//        public void setUinfo(UinfoBean uinfo) {
//            this.uinfo = uinfo;
//        }
//
//        public String getReply() {
//            return reply;
//        }
//
//        public void setReply(String reply) {
//            this.reply = reply;
//        }
//
//        public String getLike() {
//            return like;
//        }
//
//        public void setLike(String like) {
//            this.like = like;
//        }
//
//        public String getIsLike() {
//            return isLike;
//        }
//
//        public void setIsLike(String isLike) {
//            this.isLike = isLike;
//        }
//
//        public String getStatus() {
//            return status;
//        }
//
//        public void setStatus(String status) {
//            this.status = status;
//        }
//
//        public String getReplyContent() {
//            return replyContent;
//        }
//
//        public void setReplyContent(String replyContent) {
//            this.replyContent = replyContent;
//        }
//
//        public String getReplyId() {
//            return replyId;
//        }
//
//        public void setReplyId(String replyId) {
//            this.replyId = replyId;
//        }
//
//        public List<String> getImgArr() {
//            return imgArr;
//        }
//
//        public void setImgArr(List<String> imgArr) {
//            this.imgArr = imgArr;
//        }
//
//        public static class UinfoBean {
//            /**
//             * userid : 11
//             * avatar : http://park.hostop.net/static/image/logo.png
//             * nickname : 未填写
//             * username : 18857711932
//             */
//
//            private String userid;
//            private String avatar;
//            private String nickname;
//            private String username;
//
//            public String getUserid() {
//                return userid;
//            }
//
//            public void setUserid(String userid) {
//                this.userid = userid;
//            }
//
//            public String getAvatar() {
//                return avatar;
//            }
//
//            public void setAvatar(String avatar) {
//                this.avatar = avatar;
//            }
//
//            public String getNickname() {
//                return nickname;
//            }
//
//            public void setNickname(String nickname) {
//                this.nickname = nickname;
//            }
//
//            public String getUsername() {
//                return username;
//            }
//
//            public void setUsername(String username) {
//                this.username = username;
//            }
//        }
//    }
}
