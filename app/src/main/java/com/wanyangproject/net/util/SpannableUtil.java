package com.wanyangproject.net.util;

import android.graphics.Color;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.widget.TextView;

public class SpannableUtil {

    public static void colorSpan(TextView textView, String text){
        SpannableString spanString = new SpannableString(text);
        ForegroundColorSpan span = new ForegroundColorSpan(Color.parseColor("#2577FE"));
        spanString.setSpan(span, 0, 4, Spannable.SPAN_INCLUSIVE_INCLUSIVE);
        textView.append(spanString);
    }
}
