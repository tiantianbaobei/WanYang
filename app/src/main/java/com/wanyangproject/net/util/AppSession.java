package com.wanyangproject.net.util;

import android.content.Context;

public class AppSession {

    private Context context;
    private static AppSession mInstance;
    private ACache mCache;

    private AppSession(Context context) {
        this.context = context;
        mCache = ACache.get(this.context);
    }

    public static AppSession getInstance(Context context) {
        if (mInstance == null) {
            synchronized (AppSession.class) {
                if (mInstance == null) {
                    mInstance = new AppSession(context.getApplicationContext());
                }
            }
        }
        return mInstance;
    }

//    public String getId() {
//        return mCache.getAsString(Constants.SUPPLIER_ID);
//    }
}





























