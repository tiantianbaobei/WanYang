package com.wanyangproject.net.util;

import android.app.Activity;
import android.content.Context;

import com.wanyangproject.myfragment.CollectFragment;
import com.wanyangproject.myfragment.DianFragment;
import com.wanyangproject.myfragment.ForumCommentFragment;
import com.wanyangproject.myfragment.ForumIssueFragment;
import com.wanyangproject.myfragment.ForumLikeFragment;
import com.wanyangproject.myfragment.OrderAllFragment;
import com.wanyangproject.myfragment.OrderAppraistFragment;
import com.wanyangproject.myfragment.OrderFinishFragment;
import com.wanyangproject.myfragment.OrderPayFragment;
import com.wanyangproject.myfragment.OrderReceiveFragment;
import com.wanyangproject.myfragment.ShuiFragment;
import com.wanyangproject.ui.CompanyJoinActivity;
import com.wanyangproject.ui.FangKeActivity;
import com.wanyangproject.widget.recyclerview.IRecyclerSwipe;


import java.net.ConnectException;
import java.net.SocketTimeoutException;

import rx.Subscriber;

public abstract class ProgressSubscriber<T> extends Subscriber<T> implements ProgressCancel {

    private Context context;
    private ProgressDialog handler;
    private IRecyclerSwipe iRecyclerSwipe;

    public ProgressSubscriber(Context context, OrderPayFragment orderFinishFragment) {
//        this(context, null);
        this.context = context;
    }

    public ProgressSubscriber(Context context, DianFragment iRecyclerSwipe) {
        this.context = context;
        this.iRecyclerSwipe = iRecyclerSwipe;
        handler = new ProgressDialog(context, this, true);
    }

    public ProgressSubscriber(Context mActivity, Context shuiFragment) {
    }

    private void showProgressDialog() {
        if (handler != null) {
            handler.obtainMessage(ProgressDialog.SHOW_PROGRESS_DIALOG).sendToTarget();
        }
    }

    private void dismissProgressDialog() {
        if (handler != null) {
            handler.obtainMessage(ProgressDialog.DISMISS_PROGRESS_DIALOG).sendToTarget();
            handler = null;
        }
    }

    @Override
    public void onStart() {
        showProgressDialog();
        super.onStart();
    }

    @Override
    public void onCompleted() {
        if (iRecyclerSwipe != null) {
            iRecyclerSwipe.hideSwipeLoading();
        }
        dismissProgressDialog();
    }

    @Override
    public void onError(Throwable e) {
        if (iRecyclerSwipe != null) {
            iRecyclerSwipe.hideSwipeLoading();
//            iRecyclerSwipe.showItemFail(iRecyclerSwipe.getName() + " 列表数据获取失败！");
        }
        if (e instanceof SocketTimeoutException) {
            ToastUtil.show(context, "网络中断，请检查您的网络状态！");
        } else if (e instanceof ConnectException) {
            ToastUtil.show(context, "网络中断，请检查您的网络状态！");
        } else {
            ToastUtil.show(context, "error:" + e.getMessage());
        }
        dismissProgressDialog();
    }

    @Override
    public void onNext(T t) {
        next(t);
        if (iRecyclerSwipe != null) {
            iRecyclerSwipe.hideSwipeLoading();
        }
    }

    @Override
    public void onCancel() {
        if (!this.isUnsubscribed()) {
            this.unsubscribe();
        }
    }

    public abstract void next(T t);
}
