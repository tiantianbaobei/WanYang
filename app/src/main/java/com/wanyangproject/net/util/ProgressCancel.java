package com.wanyangproject.net.util;

public interface ProgressCancel {
    void onCancel();
}
