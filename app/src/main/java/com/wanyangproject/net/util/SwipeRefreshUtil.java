package com.wanyangproject.net.util;

import android.support.v4.widget.SwipeRefreshLayout;

import com.wanyangproject.R;


public class SwipeRefreshUtil {

    public static void setColors(SwipeRefreshLayout swipeRefresh) {
        swipeRefresh.setColorSchemeResources(
                R.color.refresh_blue_light,
                R.color.refresh_red_light,
                R.color.refresh_yellow_light,
                R.color.refresh_green_light
        );
    }
}
