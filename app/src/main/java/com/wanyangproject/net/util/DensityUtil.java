package com.wanyangproject.net.util;

import android.content.Context;

public class DensityUtil {

    /**
     * 把dp单位的值换算成像素单位的值
     */
    public static int dp2px(Context context, int dp) {
        float density = context.getResources().getDisplayMetrics().density;
        return (int) (dp * density + 0.5f);
    }

    /**
     * 把像素单位的值换算成dp单位的值
     */
    public static int px2dp(Context context, int px) {
        float density = context.getResources().getDisplayMetrics().density;
        return (int) (px / density + 0.5f);
    }
}
