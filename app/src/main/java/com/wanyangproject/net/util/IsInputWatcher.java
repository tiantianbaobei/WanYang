package com.wanyangproject.net.util;

import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;

public abstract class IsInputWatcher implements TextWatcher {

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        if (TextUtils.isEmpty(s)) {
            noInput();
        } else {
            inputting();
        }
    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {

    }

    @Override
    public void afterTextChanged(Editable s) {
        if (TextUtils.isEmpty(s)) {
            noInput();
        } else {
            inputting();
        }
    }

    public abstract void noInput();
    public abstract void inputting();
}
