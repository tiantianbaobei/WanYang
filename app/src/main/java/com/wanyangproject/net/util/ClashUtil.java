package com.wanyangproject.net.util;

import android.support.v4.widget.SwipeRefreshLayout;
import android.view.ViewTreeObserver;
import android.widget.ScrollView;

import com.wanyangproject.widget.scrollview.StickyNestedScrollView;


public class ClashUtil {

    public static void solveSwipeAndScroll(final SwipeRefreshLayout swipeRefresh, final ScrollView scrollView) {
        if (scrollView != null) {
            scrollView.getViewTreeObserver().addOnScrollChangedListener(new ViewTreeObserver.OnScrollChangedListener() {
                @Override
                public void onScrollChanged() {
                    if (swipeRefresh != null) {
                        swipeRefresh.setEnabled(scrollView.getScrollY() == 0);
                    }
                }
            });
        }
    }

    public static void solveSwipeAndScroll(final SwipeRefreshLayout swipeRefresh, final StickyNestedScrollView scrollView) {
        if (scrollView != null) {
            scrollView.getViewTreeObserver().addOnScrollChangedListener(new ViewTreeObserver.OnScrollChangedListener() {
                @Override
                public void onScrollChanged() {
                    if (swipeRefresh != null) {
                        swipeRefresh.setEnabled(scrollView.getScrollY() == 0);
                    }
                }
            });
        }
    }
}
