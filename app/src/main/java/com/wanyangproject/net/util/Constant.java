package com.wanyangproject.net.util;

import android.os.Environment;

public interface Constant {

    String ROLE_INDEX = "role_index";
//    游客
    int ROLE_TOURIST = 0;
//    企业
    int ROLE_COMPANY = 1;
//    员工
    int ROLE_EMPLOY = 2;
//    门卫
    int ROLE_DOOR = 3;
//    商家
    int BTN_SHANGJIA = 4;
//    物业
    int BTN_WUYE = 5;

    String ORDER_INDEX = "order_index";
//    全部
    int ORDER_ALL = 0;
//    待支付
    int ORDER_PAY = 1;
//    待收货
    int ORDER_RECEIVE = 2;
//    待评价
    int ORDER_APPRAISE = 3;
//    已完成
    int ORDER_FINISH = 4;

    String FORUM_INDEX = "forum_index";
    int FORUM_ISSUE = 0;
    int FORUM_COMMENT = 1;
    int FORUM_LIKE = 2;

    String BASE_PATH = Environment.getExternalStorageDirectory().getPath() + "/com.wanyangproject/download/";

    String ENCLOSURE_URL = "enclosure_url"; // 附件地址
    String ENCLOSURE_PATH = "enclosure_path"; // 附件本地文件路径

    String ACTION_DOWNLOAD = "com.wanyangproject.action.DOWNLOAD"; // 服务下载事件
    String ACTION_DOWNLOAD_INIT = "com.wanyangproject.action.ACTION_DOWNLOAD_INIT"; // 广播接收初始化下载
    String ACTION_DOWNLOAD_LOADING = "com.wanyangproject.action.ACTION_DOWNLOAD_LOADING"; // 广播接收下载进度
    String ACTION_DOWNLOAD_END = "com.wanyangproject.action.ACTION_DOWNLOAD_END"; // 广播接收下载结果
    String EXTRA_SIZE = "com.wanyangproject.extra.EXTRA_SIZE"; // 服务下载进度
}
