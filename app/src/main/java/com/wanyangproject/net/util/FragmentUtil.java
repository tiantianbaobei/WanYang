package com.wanyangproject.net.util;

import android.support.v4.app.Fragment;

import com.wanyangproject.myfragment.CollectFragment;
import com.wanyangproject.myfragment.DianFragment;
import com.wanyangproject.myfragment.ForumCommentFragment;
import com.wanyangproject.myfragment.ForumIssueFragment;
import com.wanyangproject.myfragment.ForumLikeFragment;
import com.wanyangproject.myfragment.MineFragment;
import com.wanyangproject.myfragment.OrderAllFragment;
import com.wanyangproject.myfragment.OrderAppraistFragment;
import com.wanyangproject.myfragment.OrderFinishFragment;
import com.wanyangproject.myfragment.OrderPayFragment;
import com.wanyangproject.myfragment.OrderReceiveFragment;
import com.wanyangproject.myfragment.ShuiFragment;

import java.util.ArrayList;
import java.util.List;

public class FragmentUtil {

    public static List<Fragment> getMainFragment() {
        List<Fragment> fragmentList = new ArrayList<>();
        fragmentList.add(new Fragment());
        fragmentList.add(new Fragment());
        fragmentList.add(new Fragment());
        fragmentList.add(new MineFragment());
        return fragmentList;
    }

    public static List<Fragment> getOrderFragment() {
        List<Fragment> fragmentList = new ArrayList<>();
        fragmentList.add(OrderAllFragment.newInstance());
        fragmentList.add(OrderPayFragment.newInstance());
        fragmentList.add(OrderReceiveFragment.newInstance());
        fragmentList.add(OrderAppraistFragment.newInstance());
        fragmentList.add(OrderFinishFragment.newInstance());
        return fragmentList;
    }

    public static List<Fragment> getCollectFragment() {
        List<Fragment> fragmentList = new ArrayList<>();
        fragmentList.add(CollectFragment.newInstance());
        fragmentList.add(CollectFragment.newInstance());
        fragmentList.add(CollectFragment.newInstance());
        fragmentList.add(CollectFragment.newInstance());
        return fragmentList;
    }

    public static List<Fragment> getForumFragment() {
        List<Fragment> fragmentList = new ArrayList<>();
        fragmentList.add(ForumIssueFragment.newInstance());
        fragmentList.add(ForumCommentFragment.newInstance());
        fragmentList.add(ForumLikeFragment.newInstance());
        return fragmentList;
    }

    public static List<Fragment> getShuidianFragment() {
        List<Fragment> fragmentList = new ArrayList<>();
        fragmentList.add(ShuiFragment.newInstance());
        fragmentList.add(DianFragment.newInstance());
        return fragmentList;
    }
}