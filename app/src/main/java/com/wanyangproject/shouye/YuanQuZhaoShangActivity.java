package com.wanyangproject.shouye;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.ViewFlipper;

import com.bumptech.glide.Glide;
import com.google.gson.Gson;
import com.wanyangproject.R;
import com.wanyangproject.activity.HomeActivity;
import com.wanyangproject.activity.WenZhangXiangQingActivity;
import com.wanyangproject.activity.YuanQuZhaoShangFenLeiActivity;
import com.wanyangproject.activity.YuanQuZhaoShangXiangQingActivity;
import com.wanyangproject.activity.YuanQuZhaoShangYiJianFanKuiActivity;
import com.wanyangproject.adapter.YuanQuZhaoShangAdapter;
import com.wanyangproject.adapter.YuanQuZhaoShangFuWuFenLeiAdapter;
import com.wanyangproject.entity.ShouYeBannerEntity;
import com.wanyangproject.entity.YuanQuZhaoShangEntity;
import com.wanyangproject.fuwuactivity.FangKeDengJiActivity;
import com.wanyangproject.fuwuactivity.YuanQuTongZhiActivity;
import com.wanyangproject.my.MyBangDingActivity;
import com.wanyangproject.my.MyShouCangActivity;
import com.wanyangproject.my.QuanBuActivity;
import com.wanyangproject.my.RuZhuShenQingActivity;
import com.wanyangproject.my.SettingActivity;
import com.wanyangproject.utils.ContractUtils;
import com.wanyangproject.utils.FuWuTiaoZhuFragment;
import com.wanyangproject.utils.FuWuTiaoZhuan;
import com.wanyangproject.utils.LunTanTiaoZhuan;
import com.wanyangproject.utils.TiaoZhuanFragment;
import com.youth.banner.Banner;
import com.youth.banner.BannerConfig;
import com.youth.banner.listener.OnBannerListener;
import com.youth.banner.loader.ImageLoader;
import com.zhy.http.okhttp.OkHttpUtils;
import com.zhy.http.okhttp.callback.StringCallback;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.Call;

public class YuanQuZhaoShangActivity extends AppCompatActivity implements ViewPager.OnPageChangeListener {


    @BindView(R.id.image_back)
    ImageView imageBack;
    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    //    @BindView(R.id.viewPager)
//    ViewPager viewPager;
    @BindView(R.id.recyclerView_fuwufenlei)
    RecyclerView recyclerViewFuwufenlei;
    @BindView(R.id.banner)
    Banner banner;
    @BindView(R.id.tv_fankui)
    TextView tvFankui;
    @BindView(R.id.relative_fankui)
    RelativeLayout relativeFankui;
    @BindView(R.id.image)
    ImageView image;
    private YuanQuZhaoShangAdapter yuanQuZhaoShangAdapter;
    private List<View> list = new ArrayList<>();
    private Boolean isOk = true;
    private YuanQuZhaoShangFuWuFenLeiAdapter yuanQuZhaoShangFuWuFenLeiAdapter;
    private YuanQuZhaoShangEntity yuanQuZhaoShangEntity;
    @BindView(R.id.viewflipper)
    ViewFlipper viewflipper;
    private TextView tv_scroll_one;
    private ShouYeBannerEntity shouYeBannerEntity;
    private TiaoZhuanFragment tiaoZhuanFragment;
    private FuWuTiaoZhuFragment fuWuTiaoZhuFragment;
//    private LunTanTiaoZhuan lunTanTiaoZhuan;
//    private FuWuTiaoZhuan fuWuTiaoZhuan;


//    private Handler handler = new Handler() {
//        @Override
//        public void handleMessage(Message msg) {
//            super.handleMessage(msg);
//            switch (msg.what) {
//                case 1:
//                    viewPager.setCurrentItem(msg.arg1);
//                    break;
//                case 2:
//                    viewPager.setCurrentItem(msg.arg1);
//                    break;
//            }
//        }
//    };


    public TiaoZhuanFragment getTiaoZhuanFragment() {
        return tiaoZhuanFragment;
    }

    public FuWuTiaoZhuFragment getFuWuTiaoZhuFragment() {
        return fuWuTiaoZhuFragment;
    }

    public void setFuWuTiaoZhuFragment(FuWuTiaoZhuFragment fuWuTiaoZhuFragment) {
        this.fuWuTiaoZhuFragment = fuWuTiaoZhuFragment;
    }

    public void setTiaoZhuanFragment(TiaoZhuanFragment tiaoZhuanFragment) {
        this.tiaoZhuanFragment = tiaoZhuanFragment;
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_yuan_qu_zhao_shang);
        ButterKnife.bind(this);
        if (Build.VERSION.SDK_INT >= 21) {
            View dview = getWindow().getDecorView();
            dview.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN | View.SYSTEM_UI_FLAG_LAYOUT_STABLE);
            getWindow().setStatusBarColor(Color.TRANSPARENT);
        }


//        viewPager = (ViewPager) findViewById(R.id.viewPager);
//        View viewOne = LayoutInflater.from(YuanQuZhaoShangActivity.this).inflate(R.layout.item_one, null);
//        View viewTwo = LayoutInflater.from(YuanQuZhaoShangActivity.this).inflate(R.layout.item_two, null);
//        View viewThree = LayoutInflater.from(YuanQuZhaoShangActivity.this).inflate(R.layout.item_three, null);
//        list.add(viewOne);
//        list.add(viewTwo);
//        list.add(viewThree);
//        MyAdapter adapter = new MyAdapter(list);
//        viewPager.setAdapter(adapter);
//        initThread();
//        viewPager.addOnPageChangeListener(this);

//        园区招商的网络请求
        initQuanQuZhaoShangHttp();


//        园区招商banner网络请求
        initBannerHttp();


    }






    //    园区招商banner网络请求
    private void initBannerHttp() {
        final ProgressDialog progressDialog = new ProgressDialog(YuanQuZhaoShangActivity.this);
        progressDialog.setTitle("提示");
        progressDialog.setMessage("请等待...");
        progressDialog.setCancelable(false);
        progressDialog.show();
        OkHttpUtils.post()
                .url(ContractUtils.LOGIN_URL + "news/getBanner")
                .addHeader("token", ContractUtils.getTOKEN(YuanQuZhaoShangActivity.this))
                .addParams("parkId", ContractUtils.getParkId(YuanQuZhaoShangActivity.this))
                .addParams("typeId", "1")
                .build()
                .execute(new StringCallback() {
                    @Override
                    public void onError(Call call, Exception e, int id) {
                        progressDialog.dismiss();
                    }

                    @Override
                    public void onResponse(String response, int id) {
                        System.out.println(response + "         园区招商banner网络请求");
                        ContractUtils.Code500(YuanQuZhaoShangActivity.this, response);
                        progressDialog.dismiss();
                        if (response.indexOf("200") != -1) {
                            Gson gson = new Gson();
                            shouYeBannerEntity = gson.fromJson(response, ShouYeBannerEntity.class);

                            //                        banner轮播
                            ArrayList<String> list = new ArrayList<String>();
                            for (int i = 0; i < shouYeBannerEntity.getResponse().size(); i++) {
//                                YuanQuZhaoShangEntity.ResponseBean.BannerArrBean bannerArrBean = yuanQuZhaoShangEntity.getResponse().getBannerArr().get(i);
                                String image = shouYeBannerEntity.getResponse().get(i).getImage();
                                list.add(image);
//                            list.add(ContractUtils.YUANQUZHAOSHANG_PHOTO_URL + bannerArrBean.getLogo());
                            }
                            banner.setBannerStyle(BannerConfig.CIRCLE_INDICATOR);
                            banner.setImageLoader(new MyImageLoader());
                            banner.setImages(list);
                            banner.setDelayTime(2000);
                            banner.setOnBannerListener(new OnBannerListener() {
                                @Override
                                public void OnBannerClick(int position) {
//                                    Intent intent = new Intent(YuanQuZhaoShangActivity.this, YuanQuZhaoShangXiangQingActivity.class);
//                                    intent.putExtra("id", yuanQuZhaoShangEntity.getResponse().getBannerArr().get(position).getId());
//                                    System.out.println(yuanQuZhaoShangEntity.getResponse().getBannerArr().get(position).getId() + "     点击banner详情的传送的id");
//                                    startActivity(intent);
                                    if (shouYeBannerEntity.getResponse().get(position).getHrefType().equals("0")) {
                                        Intent intenttongzhi = new Intent(YuanQuZhaoShangActivity.this, YuanQuTongZhiActivity.class);
                                        intenttongzhi.putExtra("zhi","zhi");
                                        startActivity(intenttongzhi);

//                                        Intent intent1 = new Intent(YuanQuZhaoShangActivity.this, ChanYeJinRongActivity.class);
//                                        startActivity(intent1);
                                    } else if (shouYeBannerEntity.getResponse().get(position).getHrefType().equals("1")) {
                                        Intent intent1 = new Intent(YuanQuZhaoShangActivity.this, YuanQuShouCeActivity.class);
                                        startActivity(intent1);
                                    } else if (shouYeBannerEntity.getResponse().get(position).getHrefType().equals("2")) {
                                        Intent intent1 = new Intent(YuanQuZhaoShangActivity.this, YuanQuQiYeActivity.class);
                                        startActivity(intent1);
                                    } else if (shouYeBannerEntity.getResponse().get(position).getHrefType().equals("3")) {
                                        Intent intent1 = new Intent(YuanQuZhaoShangActivity.this, ZhengCeFaGuiActivity.class);
                                        startActivity(intent1);
                                    } else if (shouYeBannerEntity.getResponse().get(position).getHrefType().equals("4")) {
                                        Intent intent1 = new Intent(YuanQuZhaoShangActivity.this, ZhiNengZhiZaoActivity.class);
                                        startActivity(intent1);
                                    } else if (shouYeBannerEntity.getResponse().get(position).getHrefType().equals("5")) {
                                        Intent intent1 = new Intent(YuanQuZhaoShangActivity.this, YuanQuZhaoShangActivity.class);
                                        startActivity(intent1);
                                    } else if (shouYeBannerEntity.getResponse().get(position).getHrefType().equals("6")) {
                                        Intent intent2 = new Intent(YuanQuZhaoShangActivity.this, HomeActivity.class);
                                        intent2.putExtra("luntan","luntan");
                                        startActivity(intent2);
                                    } else if (shouYeBannerEntity.getResponse().get(position).getHrefType().equals("7")) {
                                        Intent intent2 = new Intent(YuanQuZhaoShangActivity.this, HomeActivity.class);
                                        intent2.putExtra("fuwu","fuwu");
                                        startActivity(intent2);
                                    } else if (shouYeBannerEntity.getResponse().get(position).getHrefType().equals("8")) {
                                        Intent intent = new Intent(YuanQuZhaoShangActivity.this, HomeActivity.class);
                                        startActivity(intent);
                                    } else if (yuanQuZhaoShangEntity.getResponse().getUpTjArr().get(position).getHrefType().equals("9")) {
                                        if (yuanQuZhaoShangEntity.getResponse().getUpTjArr().get(position).getContent() != null) {
                                            Intent intent = new Intent(YuanQuZhaoShangActivity.this, WenZhangXiangQingActivity.class);
                                            intent.putExtra("content", yuanQuZhaoShangEntity.getResponse().getUpTjArr().get(position).getContent());
                                            startActivity(intent);
                                        }
                                    } else if (shouYeBannerEntity.getResponse().get(position).getHrefType().equals("10")) {
                                        if (shouYeBannerEntity.getResponse().get(position).getContent() != null) {
                                            Intent intent = new Intent(YuanQuZhaoShangActivity.this, YuanQuShengHuoActivity.class);
                                            startActivity(intent);
                                        }
                                    } else if (shouYeBannerEntity.getResponse().get(position).getHrefType().equals("11")) {
                                        if (shouYeBannerEntity.getResponse().get(position).getContent() != null) {
                                            return;
                                        }
                                    } else if (shouYeBannerEntity.getResponse().get(position).getHrefType().equals("12")) {
                                        if (shouYeBannerEntity.getResponse().get(position).getContent() != null) {
                                            Intent intent = new Intent(YuanQuZhaoShangActivity.this, SettingActivity.class);
                                            startActivity(intent);
                                        }
                                    } else if (shouYeBannerEntity.getResponse().get(position).getHrefType().equals("13")) {
                                        if (shouYeBannerEntity.getResponse().get(position).getContent() != null) {
                                            Intent intent = new Intent(YuanQuZhaoShangActivity.this, QiuZhiZhaoPinActivity.class);
                                            startActivity(intent);
                                        }
                                    } else if (shouYeBannerEntity.getResponse().get(position).getHrefType().equals("14")) {
                                        if (shouYeBannerEntity.getResponse().get(position).getContent() != null) {
                                            Intent intent = new Intent(YuanQuZhaoShangActivity.this, ChanYeJinRongActivity.class);
                                            startActivity(intent);
                                        }
                                    } else if (shouYeBannerEntity.getResponse().get(position).getHrefType().equals("15")) {
                                        if (shouYeBannerEntity.getResponse().get(position).getContent() != null) {
                                            Intent intent = new Intent(YuanQuZhaoShangActivity.this, YuanQuTongZhiActivity.class);
                                            startActivity(intent);
                                        }
                                    } else if (shouYeBannerEntity.getResponse().get(position).getHrefType().equals("16")) {
                                        if (shouYeBannerEntity.getResponse().get(position).getContent() != null) {
                                            Intent intent = new Intent(YuanQuZhaoShangActivity.this, FangKeDengJiActivity.class);
                                            startActivity(intent);
                                        }
                                    } else if (shouYeBannerEntity.getResponse().get(position).getHrefType().equals("17")) {
                                        if (shouYeBannerEntity.getResponse().get(position).getContent() != null) {
                                            Intent intent = new Intent(YuanQuZhaoShangActivity.this, QuanBuActivity.class);
                                            intent.putExtra("daizhifu","0");
                                            startActivity(intent);
                                        }
                                    } else if (shouYeBannerEntity.getResponse().get(position).getHrefType().equals("18")) {
                                        if (shouYeBannerEntity.getResponse().get(position).getContent() != null) {
                                            Intent intent = new Intent(YuanQuZhaoShangActivity.this, MyShouCangActivity.class);
                                            startActivity(intent);
                                        }
                                    } else if (shouYeBannerEntity.getResponse().get(position).getHrefType().equals("19")) {
                                        if (shouYeBannerEntity.getResponse().get(position).getContent() != null) {
                                            Intent intent = new Intent(YuanQuZhaoShangActivity.this, ShouHuoXinXiActivity.class);
                                            startActivity(intent);
                                        }
                                    } else if (shouYeBannerEntity.getResponse().get(position).getHrefType().equals("20")) {
                                        if (shouYeBannerEntity.getResponse().get(position).getContent() != null) {
                                            Intent intent = new Intent(YuanQuZhaoShangActivity.this, RuZhuShenQingActivity.class);
                                            startActivity(intent);
                                        }
                                    } else if (shouYeBannerEntity.getResponse().get(position).getHrefType().equals("21")) {
                                        if (shouYeBannerEntity.getResponse().get(position).getContent() != null) {
                                            Intent intent = new Intent(YuanQuZhaoShangActivity.this, MyBangDingActivity.class);
                                            startActivity(intent);
                                        }
                                    }
                                }
                            });
                            banner.start();
                        }
                    }
                });
    }

    //    园区招商的网络请求
    private void initQuanQuZhaoShangHttp() {
        final ProgressDialog progressDialog = new ProgressDialog(YuanQuZhaoShangActivity.this);
        progressDialog.setTitle("提示");
        progressDialog.setMessage("请等待...");
        progressDialog.setCancelable(false);
        progressDialog.show();
        OkHttpUtils.post()
                .url(ContractUtils.LOGIN_URL + "news/getCategory")
                .addHeader("token", ContractUtils.getTOKEN(YuanQuZhaoShangActivity.this))
                .addParams("parkId", ContractUtils.getParkId(YuanQuZhaoShangActivity.this))
                .addParams("typeId", "1")//1:园区招商，2：企服中心，3:政策法规
                .build()
                .execute(new StringCallback() {
                    @Override
                    public void onError(Call call, Exception e, int id) {
                        progressDialog.dismiss();
                    }

                    @Override
                    public void onResponse(String response, int id) {
                        ContractUtils.Code500(YuanQuZhaoShangActivity.this, response);
                        System.out.println(response + "     园区招商的网络请求");
                        progressDialog.dismiss();
                        if (response.indexOf("200") != -1) {
                            Gson gson = new Gson();
                            yuanQuZhaoShangEntity = gson.fromJson(response, YuanQuZhaoShangEntity.class);


                            if(yuanQuZhaoShangEntity.getResponse().getBackground() == null){

                            }else{
                                Glide.with(YuanQuZhaoShangActivity.this).load(yuanQuZhaoShangEntity.getResponse().getBackground()).into(image);
                            }


                            yuanQuZhaoShangAdapter = new YuanQuZhaoShangAdapter(YuanQuZhaoShangActivity.this, yuanQuZhaoShangEntity.getResponse().getUpTjArr());
                            LinearLayoutManager manager = new LinearLayoutManager(YuanQuZhaoShangActivity.this);
                            //设置为横向滑动
                            manager.setOrientation(LinearLayoutManager.HORIZONTAL);
                            recyclerView.setLayoutManager(manager);
                            recyclerView.setAdapter(yuanQuZhaoShangAdapter);

                            yuanQuZhaoShangAdapter.setXingQingClick(new YuanQuZhaoShangAdapter.XingQingClick() {
                                @Override
                                public void xiangqingClick(int position) {
//                                Intent intent = new Intent(YuanQuZhaoShangActivity.this, YuanQuZhaoShangXiangQingActivity.class);
//                                intent.putExtra("id", yuanQuZhaoShangEntity.getResponse().getUpTjArr().get(position).getId());
//                                System.out.println(yuanQuZhaoShangEntity.getResponse().getUpTjArr().get(position).getId() + "       点击上面滑动的传送的文章的id");
//                                startActivity(intent);







                                    if (yuanQuZhaoShangEntity.getResponse().getUpTjArr().get(position).getHrefType().equals("0")) {
                                        Intent intenttongzhi = new Intent(YuanQuZhaoShangActivity.this, YuanQuTongZhiActivity.class);
                                        intenttongzhi.putExtra("zhi","zhi");
                                        startActivity(intenttongzhi);

//                                        Intent intent1 = new Intent(YuanQuZhaoShangActivity.this, ChanYeJinRongActivity.class);
//                                        startActivity(intent1);
                                    } else if (yuanQuZhaoShangEntity.getResponse().getUpTjArr().get(position).getHrefType().equals("1")) {
                                        Intent intent1 = new Intent(YuanQuZhaoShangActivity.this, YuanQuShouCeActivity.class);
                                        startActivity(intent1);
                                    } else if (yuanQuZhaoShangEntity.getResponse().getUpTjArr().get(position).getHrefType().equals("2")) {
                                        Intent intent1 = new Intent(YuanQuZhaoShangActivity.this, YuanQuQiYeActivity.class);
                                        startActivity(intent1);
                                    } else if (yuanQuZhaoShangEntity.getResponse().getUpTjArr().get(position).getHrefType().equals("3")) {
                                        Intent intent1 = new Intent(YuanQuZhaoShangActivity.this, ZhengCeFaGuiActivity.class);
                                        startActivity(intent1);
                                    } else if (yuanQuZhaoShangEntity.getResponse().getUpTjArr().get(position).getHrefType().equals("4")) {
                                        Intent intent1 = new Intent(YuanQuZhaoShangActivity.this, ZhiNengZhiZaoActivity.class);
                                        startActivity(intent1);
                                    } else if (yuanQuZhaoShangEntity.getResponse().getUpTjArr().get(position).getHrefType().equals("5")) {
                                        Intent intent1 = new Intent(YuanQuZhaoShangActivity.this, YuanQuZhaoShangActivity.class);
                                        startActivity(intent1);
                                    } else if (yuanQuZhaoShangEntity.getResponse().getUpTjArr().get(position).getHrefType().equals("6")) {
                                        Intent intent2 = new Intent(YuanQuZhaoShangActivity.this, HomeActivity.class);
                                        intent2.putExtra("luntan","luntan");
                                        startActivity(intent2);
                                    } else if (yuanQuZhaoShangEntity.getResponse().getUpTjArr().get(position).getHrefType().equals("7")) {
                                        Intent intent2 = new Intent(YuanQuZhaoShangActivity.this, HomeActivity.class);
                                        intent2.putExtra("fuwu","fuwu");
                                        startActivity(intent2);
                                    } else if (yuanQuZhaoShangEntity.getResponse().getUpTjArr().get(position).getHrefType().equals("8")) {
                                        Intent intent = new Intent(YuanQuZhaoShangActivity.this, HomeActivity.class);
                                        startActivity(intent);
                                    } else if (yuanQuZhaoShangEntity.getResponse().getUpTjArr().get(position).getHrefType().equals("9")) {
                                        if (yuanQuZhaoShangEntity.getResponse().getUpTjArr().get(position).getContent() != null) {
                                            Intent intent = new Intent(YuanQuZhaoShangActivity.this, WenZhangXiangQingActivity.class);
                                            intent.putExtra("content", yuanQuZhaoShangEntity.getResponse().getUpTjArr().get(position).getContent());
                                            startActivity(intent);
                                        }
                                    } else if (yuanQuZhaoShangEntity.getResponse().getUpTjArr().get(position).getHrefType().equals("10")) {
                                        if (yuanQuZhaoShangEntity.getResponse().getUpTjArr().get(position).getContent() != null) {
                                            Intent intent = new Intent(YuanQuZhaoShangActivity.this, YuanQuShengHuoActivity.class);
                                            startActivity(intent);
                                        }
                                    } else if (yuanQuZhaoShangEntity.getResponse().getUpTjArr().get(position).getHrefType().equals("11")) {
                                        if (yuanQuZhaoShangEntity.getResponse().getUpTjArr().get(position).getContent() != null) {
                                            return;
                                        }
                                    } else if (yuanQuZhaoShangEntity.getResponse().getUpTjArr().get(position).getHrefType().equals("12")) {
                                        if (yuanQuZhaoShangEntity.getResponse().getUpTjArr().get(position).getContent() != null) {
                                            Intent intent = new Intent(YuanQuZhaoShangActivity.this, SettingActivity.class);
                                            startActivity(intent);
                                        }
                                    } else if (yuanQuZhaoShangEntity.getResponse().getUpTjArr().get(position).getHrefType().equals("13")) {
                                        if (yuanQuZhaoShangEntity.getResponse().getUpTjArr().get(position).getContent() != null) {
                                            Intent intent = new Intent(YuanQuZhaoShangActivity.this, QiuZhiZhaoPinActivity.class);
                                            startActivity(intent);
                                        }
                                    } else if (yuanQuZhaoShangEntity.getResponse().getUpTjArr().get(position).getHrefType().equals("14")) {
                                        if (yuanQuZhaoShangEntity.getResponse().getUpTjArr().get(position).getContent() != null) {
                                            Intent intent = new Intent(YuanQuZhaoShangActivity.this, ChanYeJinRongActivity.class);
                                            startActivity(intent);
                                        }
                                    } else if (yuanQuZhaoShangEntity.getResponse().getUpTjArr().get(position).getHrefType().equals("15")) {
                                        if (yuanQuZhaoShangEntity.getResponse().getUpTjArr().get(position).getContent() != null) {
                                            Intent intent = new Intent(YuanQuZhaoShangActivity.this, YuanQuTongZhiActivity.class);
                                            startActivity(intent);
                                        }
                                    } else if (yuanQuZhaoShangEntity.getResponse().getUpTjArr().get(position).getHrefType().equals("16")) {
                                        if (yuanQuZhaoShangEntity.getResponse().getUpTjArr().get(position).getContent() != null) {
                                            Intent intent = new Intent(YuanQuZhaoShangActivity.this, FangKeDengJiActivity.class);
                                            startActivity(intent);
                                        }
                                    } else if (yuanQuZhaoShangEntity.getResponse().getUpTjArr().get(position).getHrefType().equals("17")) {
                                        if (yuanQuZhaoShangEntity.getResponse().getUpTjArr().get(position).getContent() != null) {
                                            Intent intent = new Intent(YuanQuZhaoShangActivity.this, QuanBuActivity.class);
                                            intent.putExtra("daizhifu","0");
                                            startActivity(intent);
                                        }
                                    } else if (yuanQuZhaoShangEntity.getResponse().getUpTjArr().get(position).getHrefType().equals("18")) {
                                        if (yuanQuZhaoShangEntity.getResponse().getUpTjArr().get(position).getContent() != null) {
                                            Intent intent = new Intent(YuanQuZhaoShangActivity.this, MyShouCangActivity.class);
                                            startActivity(intent);
                                        }
                                    } else if (yuanQuZhaoShangEntity.getResponse().getUpTjArr().get(position).getHrefType().equals("19")) {
                                        if (yuanQuZhaoShangEntity.getResponse().getUpTjArr().get(position).getContent() != null) {
                                            Intent intent = new Intent(YuanQuZhaoShangActivity.this, ShouHuoXinXiActivity.class);
                                            startActivity(intent);
                                        }
                                    } else if (yuanQuZhaoShangEntity.getResponse().getUpTjArr().get(position).getHrefType().equals("20")) {
                                        if (yuanQuZhaoShangEntity.getResponse().getUpTjArr().get(position).getContent() != null) {
                                            Intent intent = new Intent(YuanQuZhaoShangActivity.this, RuZhuShenQingActivity.class);
                                            startActivity(intent);
                                        }
                                    } else if (yuanQuZhaoShangEntity.getResponse().getUpTjArr().get(position).getHrefType().equals("21")) {
                                        if (yuanQuZhaoShangEntity.getResponse().getUpTjArr().get(position).getContent() != null) {
                                            Intent intent = new Intent(YuanQuZhaoShangActivity.this, MyBangDingActivity.class);
                                            startActivity(intent);
                                        }
                                    }



















//                                    if (yuanQuZhaoShangEntity.getResponse().getUpTjArr().get(position).getHrefType().equals("0")) {
//                                        Intent intent1 = new Intent(YuanQuZhaoShangActivity.this, ChanYeJinRongActivity.class);
//                                        startActivity(intent1);
//                                    } else if (yuanQuZhaoShangEntity.getResponse().getUpTjArr().get(position).getHrefType().equals("1")) {
//                                        Intent intent1 = new Intent(YuanQuZhaoShangActivity.this, YuanQuShouCeActivity.class);
//                                        startActivity(intent1);
//                                    } else if (yuanQuZhaoShangEntity.getResponse().getUpTjArr().get(position).getHrefType().equals("2")) {
//                                        Intent intent1 = new Intent(YuanQuZhaoShangActivity.this, YuanQuQiYeActivity.class);
//                                        startActivity(intent1);
//                                    } else if (yuanQuZhaoShangEntity.getResponse().getUpTjArr().get(position).getHrefType().equals("3")) {
//                                        Intent intent1 = new Intent(YuanQuZhaoShangActivity.this, ZhengCeFaGuiActivity.class);
//                                        startActivity(intent1);
//                                    } else if (yuanQuZhaoShangEntity.getResponse().getUpTjArr().get(position).getHrefType().equals("4")) {
//                                        Intent intent1 = new Intent(YuanQuZhaoShangActivity.this, ZhiNengZhiZaoActivity.class);
//                                        startActivity(intent1);
//                                    } else if (yuanQuZhaoShangEntity.getResponse().getUpTjArr().get(position).getHrefType().equals("5")) {
//                                        Intent intent1 = new Intent(YuanQuZhaoShangActivity.this, YuanQuZhaoShangActivity.class);
//                                        startActivity(intent1);
//                                    } else if (yuanQuZhaoShangEntity.getResponse().getUpTjArr().get(position).getHrefType().equals("6")) {
//                                        if (tiaoZhuanFragment != null) {
//                                            tiaoZhuanFragment.onClick();
//                                        }
//                                    } else if (yuanQuZhaoShangEntity.getResponse().getUpTjArr().get(position).getHrefType().equals("7")) {
//                                        if (fuWuTiaoZhuFragment != null) {
//                                            fuWuTiaoZhuFragment.onClick1();
//                                        }
//                                    } else if (yuanQuZhaoShangEntity.getResponse().getUpTjArr().get(position).getHrefType().equals("8")) {
//                                        Intent intent = new Intent(YuanQuZhaoShangActivity.this, HomeActivity.class);
//                                        startActivity(intent);
//                                    } else if (yuanQuZhaoShangEntity.getResponse().getUpTjArr().get(position).getHrefType().equals("9")) {
//                                        if (yuanQuZhaoShangEntity.getResponse().getUpTjArr().get(position).getContent() != null) {
//                                            Intent intent = new Intent(YuanQuZhaoShangActivity.this, WenZhangXiangQingActivity.class);
//                                            intent.putExtra("content", yuanQuZhaoShangEntity.getResponse().getUpTjArr().get(position).getContent());
//                                            startActivity(intent);
//                                        }
//                                    }
                                }
                            });


//                        文字广告位
                            for (int i = 0; i < yuanQuZhaoShangEntity.getResponse().getWordArr().size(); i++) {
                                View view = View.inflate(YuanQuZhaoShangActivity.this, R.layout.scroll_tv, null);
                                tv_scroll_one = (TextView) view.findViewById(R.id.tv_scroll_one);
                                tv_scroll_one.setText(yuanQuZhaoShangEntity.getResponse().getWordArr().get(i).getTitle());
                                viewflipper.addView(view);
                            }
                            viewflipper.startFlipping();
                            viewflipper.setInAnimation(AnimationUtils.loadAnimation(YuanQuZhaoShangActivity.this, R.anim.push_down_in));
                            viewflipper.setOutAnimation(AnimationUtils.loadAnimation(YuanQuZhaoShangActivity.this, R.anim.push_down_out));

                            viewflipper.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View arg0) {
                                    // TODO Auto-generated method stub
//                                        startActivity(new Intent(MainActivity.this, Art.class));
//                                Intent intent = new Intent(getContext(), QiYeXiangQingActivity.class);
////                                    应该传企业的id   tiantian
//                                System.out.println(viewflipper.getDisplayedChild()+"              zzzzzzzzzzzzzzzzz");
//                                intent.putExtra("id",shouYeTuiJianQiYeEntity.getResponse().get(viewflipper.getDisplayedChild()).getEnterId());
//                                startActivity(intent);


                                    Intent intent = new Intent(YuanQuZhaoShangActivity.this, YuanQuZhaoShangXiangQingActivity.class);
                                    intent.putExtra("id", yuanQuZhaoShangEntity.getResponse().getWordArr().get(viewflipper.getDisplayedChild()).getId());
                                    intent.putExtra("typeId","6");///0 园区手册   1 智能制造   2 产业金融   3 园区资讯   4 政策法规    5企服中心    6 园区招商
                                    System.out.println(yuanQuZhaoShangEntity.getResponse().getWordArr().get(viewflipper.getDisplayedChild()).getId() + "     点击banner详情的传送的id");
                                    startActivity(intent);
//


//                                if (yuanQuZhaoShangEntity.getResponse().getWordArr().get(viewflipper.getDisplayedChild()).getHrefType() != null) {
//                                    if (yuanQuZhaoShangEntity.getResponse().getWordArr().get(viewflipper.getDisplayedChild()).getHrefType().equals("0")) {
//                                        Intent intent1 = new Intent(YuanQuZhaoShangActivity.this, ChanYeJinRongActivity.class);
//                                        startActivity(intent1);
//                                    } else if (yuanQuZhaoShangEntity.getResponse().getWordArr().get(viewflipper.getDisplayedChild()).getHrefType().equals("1")) {
//                                        Intent intent1 = new Intent(YuanQuZhaoShangActivity.this, YuanQuShouCeActivity.class);
//                                        startActivity(intent1);
//                                    } else if (yuanQuZhaoShangEntity.getResponse().getWordArr().get(viewflipper.getDisplayedChild()).getHrefType().equals("2")) {
//                                        Intent intent1 = new Intent(YuanQuZhaoShangActivity.this, YuanQuQiYeActivity.class);
//                                        startActivity(intent1);
//                                    } else if (yuanQuZhaoShangEntity.getResponse().getWordArr().get(viewflipper.getDisplayedChild()).getHrefType().equals("3")) {
//                                        Intent intent1 = new Intent(YuanQuZhaoShangActivity.this, ZhengCeFaGuiActivity.class);
//                                        startActivity(intent1);
//                                    } else if (yuanQuZhaoShangEntity.getResponse().getWordArr().get(viewflipper.getDisplayedChild()).getHrefType().equals("4")) {
//                                        Intent intent1 = new Intent(YuanQuZhaoShangActivity.this, ZhiNengZhiZaoActivity.class);
//                                        startActivity(intent1);
//                                    } else if (yuanQuZhaoShangEntity.getResponse().getWordArr().get(viewflipper.getDisplayedChild()).getHrefType().equals("5")) {
//                                        Intent intent1 = new Intent(YuanQuZhaoShangActivity.this, YuanQuZhaoShangActivity.class);
//                                        startActivity(intent1);
//                                    } else if (yuanQuZhaoShangEntity.getResponse().getWordArr().get(viewflipper.getDisplayedChild()).getHrefType().equals("6")) {
//                                        if (tiaoZhuanFragment != null) {
//                                            tiaoZhuanFragment.onClick();
//                                        }
//                                    } else if (yuanQuZhaoShangEntity.getResponse().getWordArr().get(viewflipper.getDisplayedChild()).getHrefType().equals("7")) {
//                                        if (fuWuTiaoZhuFragment != null) {
//                                            fuWuTiaoZhuFragment.onClick1();
//                                        }
//                                    } else if (yuanQuZhaoShangEntity.getResponse().getWordArr().get(viewflipper.getDisplayedChild()).getHrefType().equals("8")) {
//                                        Intent intent = new Intent(YuanQuZhaoShangActivity.this, HomeActivity.class);
//                                        startActivity(intent);
//                                    } else if (yuanQuZhaoShangEntity.getResponse().getWordArr().get(viewflipper.getDisplayedChild()).getHrefType().equals("9")) {
//                                        if (yuanQuZhaoShangEntity.getResponse().getWordArr().get(viewflipper.getDisplayedChild()).getContent() != null) {
//                                            Intent intent = new Intent(YuanQuZhaoShangActivity.this, WenZhangXiangQingActivity.class);
//                                            intent.putExtra("content", yuanQuZhaoShangEntity.getResponse().getWordArr().get(viewflipper.getDisplayedChild()).getContent());
//                                            startActivity(intent);
//                                        }
//                                    }
//                                }
                                }
                            });


//                        viewflipper.setOnClickListener(new View.OnClickListener() {
//                            @Override
//                            public void onClick(View view) {
//                                Intent intent = new Intent(YuanQuZhaoShangActivity.this, YuanQuZhaoShangGunDongWenZiXiangQingActivity.class);
//                                intent.putExtra("wenzhangid",yuanQuZhaoShangEntity.getResponse().getWordArr().get())
//                                startActivity(intent);
//                            }
//                        });

////                        banner轮播
//                        ArrayList<String> list = new ArrayList<String>();
//                        for (int i = 0; i < yuanQuZhaoShangEntity.getResponse().getBannerArr().size(); i++) {
//                            YuanQuZhaoShangEntity.ResponseBean.BannerArrBean bannerArrBean = yuanQuZhaoShangEntity.getResponse().getBannerArr().get(i);
//                            list.add( bannerArrBean.getImage());
////                            list.add(ContractUtils.YUANQUZHAOSHANG_PHOTO_URL + bannerArrBean.getLogo());
//                        }
//                        banner.setBannerStyle(BannerConfig.CIRCLE_INDICATOR);
//                        banner.setImageLoader(new MyImageLoader());
//                        banner.setImages(list);
//                        banner.setDelayTime(2000);
//                        banner.setOnBannerListener(new OnBannerListener() {
//                            @Override
//                            public void OnBannerClick(int position) {
//                                Intent intent = new Intent(YuanQuZhaoShangActivity.this, YuanQuZhaoShangXiangQingActivity.class);
//                                intent.putExtra("id", yuanQuZhaoShangEntity.getResponse().getBannerArr().get(position).getId());
//                                System.out.println(yuanQuZhaoShangEntity.getResponse().getBannerArr().get(position).getId() + "     点击banner详情的传送的id");
//                                startActivity(intent);
//                            }
//                        });
//                        banner.start();


                            //      园区招商服务分类
                            yuanQuZhaoShangFuWuFenLeiAdapter = new YuanQuZhaoShangFuWuFenLeiAdapter(YuanQuZhaoShangActivity.this, yuanQuZhaoShangEntity.getResponse().getCategory());
                            LinearLayoutManager manager1 = new LinearLayoutManager(YuanQuZhaoShangActivity.this);
                            recyclerViewFuwufenlei.setLayoutManager(manager1);
                            recyclerViewFuwufenlei.setAdapter(yuanQuZhaoShangFuWuFenLeiAdapter);

                            yuanQuZhaoShangFuWuFenLeiAdapter.setFenLeiXiangQingClick(new YuanQuZhaoShangFuWuFenLeiAdapter.FenLeiXiangQingClick() {
                                @Override
                                public void fenleixiangqingClick(String id) {
                                    Intent intent = new Intent(YuanQuZhaoShangActivity.this, YuanQuZhaoShangFenLeiActivity.class);
                                    intent.putExtra("id", id);
                                    intent.putExtra("typeid", "1");
                                    intent.putExtra("zixuntypeid","6");//0 园区手册   1 智能制造   2 产业金融   3 园区资讯   4 政策法规    5企服中心    6 园区招商
                                    System.out.println(id + "     园区招商服务分类传送的id");
                                    startActivity(intent);
                                }
                            });
                        }

                    }
                });
    }


    public class MyImageLoader extends ImageLoader {
        @Override
        public void displayImage(Context context, Object path, ImageView imageView) {
            Glide.with(context).load(path).into(imageView);
        }
    }


    @OnClick({R.id.image_back, R.id.recyclerView, R.id.tv_fankui, R.id.relative_fankui})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.image_back:
                finish();
                break;
            case R.id.recyclerView:
                break;
            case R.id.tv_fankui:
                Intent intent = new Intent(YuanQuZhaoShangActivity.this, YuanQuZhaoShangYiJianFanKuiActivity.class);
                startActivity(intent);
                break;
            case R.id.relative_fankui:
                Intent intent1 = new Intent(YuanQuZhaoShangActivity.this, YuanQuZhaoShangYiJianFanKuiActivity.class);
                startActivity(intent1);
                break;
        }
    }


//    private void initThread() {
//        new Thread() {
//            @Override
//            public void run() {
//                super.run();
//                try {
//                    while (isOk) {
//                        Thread.sleep(3000);
//                        Message message = Message.obtain();
//                        int currentItem = viewPager.getCurrentItem();
//                        if (currentItem == 2) {
//                            currentItem = 0;
//                            message.what = 1;
//                            message.arg1 = currentItem;
//                            handler.sendMessage(message);
//                        } else {
//                            message.what = 2;
//                            message.arg1 = currentItem + 1;
//                            handler.sendMessage(message);
//                        }
//                    }
//                } catch (InterruptedException e) {
//                    e.printStackTrace();
//                }
//            }
//        }.start();
//    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {

    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }
}
