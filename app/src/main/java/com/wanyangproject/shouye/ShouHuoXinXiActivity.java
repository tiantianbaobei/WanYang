package com.wanyangproject.shouye;

import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.view.Display;
import android.view.Gravity;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.wanyangproject.R;
import com.wanyangproject.adapter.ShouHuoXinXiAdapter;
import com.wanyangproject.entity.ShanChuEntity;
import com.wanyangproject.entity.ShouHuoDiZhiSheZhiMoRenEntity;
import com.wanyangproject.entity.ShouHuoXinXiLieBiaoEntity;
import com.wanyangproject.utils.ContractUtils;
import com.wanyangproject.utils.SharepreferencesDiZhi;
import com.zhy.http.okhttp.OkHttpUtils;
import com.zhy.http.okhttp.callback.StringCallback;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.Call;

//收货信息
public class ShouHuoXinXiActivity extends AppCompatActivity {
    @BindView(R.id.image_back)
    ImageView imageBack;
    @BindView(R.id.image_add)
    ImageView imageAdd;
    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;

//    @BindView(R.id.image_back)
//    ImageView imageBack;
//    @BindView(R.id.image_add)
//    ImageView imageAdd;
//    @BindView(R.id.image_false)
//    ImageView imageFalse;
//    @BindView(R.id.image_bianji)
//    ImageView imageBianji;
//    @BindView(R.id.image_delete)
//    ImageView imageDelete;
//    @BindView(R.id.image_false1)
//    ImageView imageFalse1;
//    @BindView(R.id.image_bianji1)
//    ImageView imageBianji1;
//    @BindView(R.id.image_delete1)
//    ImageView imageDelete1;
//    @BindView(R.id.relative)
//    RelativeLayout relative;
//    @BindView(R.id.relative1)
//    RelativeLayout relative1;
    private ShouHuoXinXiAdapter shouHuoXinXiAdapter;
    private ShouHuoXinXiLieBiaoEntity shouHuoXinXiLieBiaoEntity;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shou_huo_xin_xi);
        ButterKnife.bind(this);
        initView();
        if (Build.VERSION.SDK_INT >= 21) {
            View dview = getWindow().getDecorView();
            dview.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN | View.SYSTEM_UI_FLAG_LAYOUT_STABLE);
            getWindow().setStatusBarColor(Color.TRANSPARENT);
        }


//        收货列表的网络请求
        initShouHuoLieBiaoHttp();

    }


    @Override
    protected void onResume() {
        super.onResume();
        //        收货列表的网络请求
        initShouHuoLieBiaoHttp();
    }

    //    收货列表的网络请求
    private void initShouHuoLieBiaoHttp() {
        OkHttpUtils.post()
                .url(ContractUtils.LOGIN_URL+"Life/reslist")
                .addHeader("token",ContractUtils.getTOKEN(ShouHuoXinXiActivity.this))
                .addParams("parkId",ContractUtils.getParkId(ShouHuoXinXiActivity.this))
                .build()
                .execute(new StringCallback() {
                    @Override
                    public void onError(Call call, Exception e, int id) {
                        System.out.println(e+"     收货信息列表eeeee");
                    }

                    @Override
                    public void onResponse(final String response, int id) {
                        ContractUtils.Code500(ShouHuoXinXiActivity.this,response);
                        System.out.println(response+"      收货信息列表的网络请求");

                        if(response.indexOf("200") != -1){
                            Gson gson = new Gson();
                            shouHuoXinXiLieBiaoEntity = gson.fromJson(response, ShouHuoXinXiLieBiaoEntity.class);
                            shouHuoXinXiAdapter = new ShouHuoXinXiAdapter(ShouHuoXinXiActivity.this,shouHuoXinXiLieBiaoEntity.getResponse());
                            LinearLayoutManager manager = new LinearLayoutManager(ShouHuoXinXiActivity.this);
                            recyclerView.setLayoutManager(manager);
                            recyclerView.setAdapter(shouHuoXinXiAdapter);

//                            编辑
                            shouHuoXinXiAdapter.setBianJiClick(new ShouHuoXinXiAdapter.BianJiClick() {
                                @Override
                                public void bianjiClick(int position) {
                                    Intent intent = new Intent(ShouHuoXinXiActivity.this,TianJiaShouHuoXinXiActivity.class);
                                    intent.putExtra("name",shouHuoXinXiLieBiaoEntity.getResponse().get(position).getName());
                                    intent.putExtra("phone",shouHuoXinXiLieBiaoEntity.getResponse().get(position).getPhone());
                                    intent.putExtra("xuanzedizhi",shouHuoXinXiLieBiaoEntity.getResponse().get(position).getRegion());
                                    intent.putExtra("xiangxidizhi",shouHuoXinXiLieBiaoEntity.getResponse().get(position).getAddres());
                                    intent.putExtra("morendizhi",shouHuoXinXiLieBiaoEntity.getResponse().get(position).getType());
                                    intent.putExtra("id",shouHuoXinXiLieBiaoEntity.getResponse().get(position).getId());
                                    System.out.println(shouHuoXinXiLieBiaoEntity.getResponse().get(position).getAddres()+"     点击编辑传送的编辑信息");
                                    startActivity(intent);
                                }
                            });

                            shouHuoXinXiAdapter.setMoRenClick(new ShouHuoXinXiAdapter.MoRenClick() {
                                @Override
                                public void morenClick(int position,String id) {
                                    SharedPreferences sharedPreferences = getSharedPreferences("dz", Context.MODE_APPEND);
                                    String string = sharedPreferences.getString("dizhi", "dizhi");

                                    if(string.equals(id)){
                                        SharepreferencesDiZhi.saveDiZhi(ShouHuoXinXiActivity.this,"1111");
                                    }else {
                                        SharepreferencesDiZhi.saveDiZhi(ShouHuoXinXiActivity.this,id);
                                    }

//                                    收货地址设置为默认的网络请求
                                    initShouHuoDiZhiMoRenHttp(position,id);
                                }
                            });


//                            点击行布局想上个界面传收货信息
//                            zanshi
                            shouHuoXinXiAdapter.setDianJiHangBuJuClick(new ShouHuoXinXiAdapter.DianJiHangBuJuClick() {
                                @Override
                                public void dianjihangbujuClick(int position, String phone, String name, String address,String addressid,String sheng) {
                                    Intent intent=new Intent();
                                    intent.putExtra("phone",phone);
                                    intent.putExtra("name",name);
                                    intent.putExtra("address",address);
                                    intent.putExtra("addressid",addressid);
                                    intent.putExtra("sheng",sheng);
                                    intent.setAction("shouhuoxinxi");
                                    System.out.println(addressid+"         idididid");
                                    sendBroadcast(intent);
                                    finish();
                                }
                            });
//                            shouHuoXinXiAdapter.setDianJiHangBuJuClick(new ShouHuoXinXiAdapter.DianJiHangBuJuClick() {
//                                @Override
//                                public void dianjihangbujuClick(int position) {
//
//                                }
//                            });


//                            删除
                            shouHuoXinXiAdapter.setShanChuClick(new ShouHuoXinXiAdapter.ShanChuClick() {
                                @Override
                                public void shanchuclick(final int position) {
                                    final AlertDialog dialog = new AlertDialog.Builder(ShouHuoXinXiActivity.this).create();
                                    dialog.show();

                                    dialog.getWindow().setContentView(R.layout.delete_alertdialog);
                                    dialog.getWindow().setBackgroundDrawableResource(R.drawable.shape_my_youke);
//                                    dialog.getWindow().setGravity(Gravity.TOP);
                                    dialog.setCancelable(false);
                                    WindowManager windowManager=getWindowManager();
                                    Display defaultDisplay = windowManager.getDefaultDisplay();
                                    WindowManager.LayoutParams attributes = dialog.getWindow().getAttributes();
                                    attributes.width= (int) (defaultDisplay.getWidth()*0.8);
                                    dialog.getWindow().setAttributes(attributes);
                                    dialog.getWindow().findViewById(R.id.tv_quxiao).setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View view) {
                                            dialog.dismiss();
                                        }
                                    });
                                    dialog.getWindow().findViewById(R.id.tv_queding).setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View view) {
                                            System.out.println(position+"    删除的id111");
                                            System.out.println(shouHuoXinXiLieBiaoEntity.getResponse().get(position).getId()+"    删除收货地址的id222");
//                                            删除收货地址的网络请求
                                            initDeleteHttp(position);
                                            dialog.dismiss();
                                        }
                                    });
                                }
                            });
                        }else if(response.indexOf("400") != -1){
                            if(shouHuoXinXiLieBiaoEntity != null){
                                shouHuoXinXiLieBiaoEntity.getResponse().clear();
                                shouHuoXinXiAdapter.notifyDataSetChanged();
                            }
                        }
                    }
                });
    }



//    收货地址设置为默认的网络请求
    private void initShouHuoDiZhiMoRenHttp(int position ,String id) {
        OkHttpUtils.post()
                .url(ContractUtils.LOGIN_URL+"Life/ressmoren")
                .addHeader("token",ContractUtils.getTOKEN(ShouHuoXinXiActivity.this))
                .addParams("parkId",ContractUtils.getParkId(ShouHuoXinXiActivity.this))
                .addParams("addressid",id)
                .build()
                .execute(new StringCallback() {
                    @Override
                    public void onError(Call call, Exception e, int id) {

                    }

                    @Override
                    public void onResponse(String response, int id) {
                        ContractUtils.Code500(ShouHuoXinXiActivity.this,response);
                        System.out.println(response+"    收货地址设置为默认地址的网络请求");
                        Gson gson = new Gson();
                        ShouHuoDiZhiSheZhiMoRenEntity shouHuoDiZhiSheZhiMoRenEntity = gson.fromJson(response, ShouHuoDiZhiSheZhiMoRenEntity.class);
                        if(response.indexOf("200") != -1){


                            //        收货列表的网络请求
                            initShouHuoLieBiaoHttp();
                            Toast.makeText(ShouHuoXinXiActivity.this, shouHuoDiZhiSheZhiMoRenEntity.getResponse().getMeg(), Toast.LENGTH_SHORT).show();
                        }else{
                            Toast.makeText(ShouHuoXinXiActivity.this, shouHuoDiZhiSheZhiMoRenEntity.getResponse().getMeg(), Toast.LENGTH_SHORT).show();
                        }
                    }
                });
    }



//    删除收货地址的网络请求
    private void initDeleteHttp(int position) {
        OkHttpUtils.post()
                .url(ContractUtils.LOGIN_URL+"Life/resdel")
                .addHeader("token",ContractUtils.getTOKEN(ShouHuoXinXiActivity.this))
                .addParams("parkId",ContractUtils.getParkId(ShouHuoXinXiActivity.this))
                .addParams("addressid",shouHuoXinXiLieBiaoEntity.getResponse().get(position).getId())
                .build()
                .execute(new StringCallback() {
                    @Override
                    public void onError(Call call, Exception e, int id) {

                    }

                    @Override
                    public void onResponse(String response, int id) {
                        ContractUtils.Code500(ShouHuoXinXiActivity.this,response);
                        System.out.println(response+"       删除收货地址的网络请求");
                        Gson gson = new Gson();
                        ShanChuEntity shanChuEntity = gson.fromJson(response, ShanChuEntity.class);
                        if(response.indexOf("200") != -1){
                            Toast.makeText(ShouHuoXinXiActivity.this, shanChuEntity.getResponse().getMeg(), Toast.LENGTH_SHORT).show();
                            //        收货列表的网络请求
                            initShouHuoLieBiaoHttp();
                        }else{
                            Toast.makeText(ShouHuoXinXiActivity.this, shanChuEntity.getResponse().getMeg(), Toast.LENGTH_SHORT).show();
                        }
                    }
                });
    }


    private void initView() {
//        shouHuoXinXiAdapter = new ShouHuoXinXiAdapter(ShouHuoXinXiActivity.this);
//        LinearLayoutManager manager = new LinearLayoutManager(this);
//        recyclerView.setLayoutManager(manager);
//        recyclerView.setAdapter(shouHuoXinXiAdapter);
//
//
//        shouHuoXinXiAdapter.setShanChuClick(new ShouHuoXinXiAdapter.ShanChuClick() {
//            @Override
//            public void shanchuclick(int position) {
//                final AlertDialog dialog = new AlertDialog.Builder(ShouHuoXinXiActivity.this).create();
//                dialog.show();
//                dialog.getWindow().setContentView(R.layout.delete_alertdialog);
//                dialog.setCancelable(false);
//                dialog.getWindow().findViewById(R.id.tv_quxiao).setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View view) {
//                        dialog.dismiss();
//                    }
//                });
//                dialog.getWindow().findViewById(R.id.tv_queding).setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View view) {
//                        dialog.dismiss();
//                    }
//                });
//
//            }
//        });


//
//        shouHuoXinXiAdapter.setMoRenClick(new ShouHuoXinXiAdapter.MoRenClick() {
//            @Override
//            public void morenClick(int position) {
//
//            }
//        });


    }



    @OnClick({R.id.image_back, R.id.image_add})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.image_back:
                finish();
                break;
            case R.id.image_add:
                Intent intent = new Intent(ShouHuoXinXiActivity.this,TianJiaShouHuoXinXiActivity.class);
                startActivity(intent);
                break;
        }
    }

//    @OnClick({R.id.image_back, R.id.image_add, R.id.image_false, R.id.image_bianji, R.id.image_delete})
//    public void onViewClicked(View view) {
//        switch (view.getId()) {
////            返回按钮
//            case R.id.image_back:
//                finish();
//                break;
////            添加收货地址
//            case R.id.image_add:
//                Intent intent = new Intent(ShouHuoXinXiActivity.this,TianJiaShouHuoXinXiActivity.class);
//                startActivity(intent);
//                break;
////            默认图片
//            case R.id.image_false:
//                imageFalse.setImageResource(R.drawable.checked);
////                imageFalse1.setImageResource(R.drawable.unchecked);
//                break;
////            编辑
//            case R.id.image_bianji:
//                break;
////            删除
//            case R.id.image_delete:
//                final AlertDialog dialog = new AlertDialog.Builder(ShouHuoXinXiActivity.this).create();
//                dialog.show();
//                dialog.getWindow().setContentView(R.layout.delete_alertdialog);
//                dialog.setCancelable(false);
//                dialog.getWindow().findViewById(R.id.tv_quxiao).setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View view) {
//                        dialog.dismiss();
//                    }
//                });
//                dialog.getWindow().findViewById(R.id.tv_queding).setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View view) {
////                        relative.setVisibility(View.GONE);/
//                        dialog.dismiss();
//                    }
//                });
//
//                break;
//            case R.id.image_false1:
//                imageFalse1.setImageResource(R.drawable.checked);
//                imageFalse.setImageResource(R.drawable.unchecked);
//                break;
//            case R.id.image_bianji1:
//                break;
//            case R.id.image_delete1:
//                final AlertDialog dialog1 = new AlertDialog.Builder(ShouHuoXinXiActivity.this).create();
//                dialog1.show();
//                dialog1.getWindow().setContentView(R.layout.delete_alertdialog);
//                dialog1.setCancelable(false);
//                dialog1.getWindow().findViewById(R.id.tv_quxiao).setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View view) {
//                        dialog1.dismiss();
//                    }
//                });
//                dialog1.getWindow().findViewById(R.id.tv_queding).setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View view) {
//                        relative1.setVisibility(View.GONE);
//                        dialog1.dismiss();
//                    }
//                });
//                break;
//        }
//    }
}
