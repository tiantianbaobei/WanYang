package com.wanyangproject.shouye;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.gson.Gson;
import com.wanyangproject.R;
import com.wanyangproject.activity.DiTuActivity;
import com.wanyangproject.adapter.QiYeXiangQingAdapter;
import com.wanyangproject.entity.QiYeXiangQingEntity;
import com.wanyangproject.my.*;
import com.wanyangproject.utils.ContractUtils;
import com.zhy.http.okhttp.OkHttpUtils;
import com.zhy.http.okhttp.callback.StringCallback;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.Call;

public class QiYeXiangQingActivity extends AppCompatActivity {

    @BindView(R.id.image_back)
    ImageView imageBack;
    @BindView(R.id.image_touxiang)
    ImageView imageTouxiang;
    @BindView(R.id.tv_shop_name)
    TextView tvShopName;
    @BindView(R.id.tv_phone)
    TextView tvPhone;
    @BindView(R.id.tv_address)
    TextView tvAddress;
    @BindView(R.id.image_address)
    ImageView imageAddress;
    @BindView(R.id.tv_jianjie)
    TextView tvJianjie;
    @BindView(R.id.reecyclerView)
    RecyclerView reecyclerView;
    private QiYeXiangQingAdapter qiYeXiangQingAdapter;
    private String id;
    private QiYeXiangQingEntity qiYeXiangQingEntity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_qi_ye_xiang_qing);
        ButterKnife.bind(this);
        if (Build.VERSION.SDK_INT >= 21) {
            View dview = getWindow().getDecorView();
            dview.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN | View.SYSTEM_UI_FLAG_LAYOUT_STABLE);
            getWindow().setStatusBarColor(Color.TRANSPARENT);
        }


        Intent intent = getIntent();
        id = intent.getStringExtra("id");
        System.out.println(id+"          qiyexiangqing id");

//        企业详情的网络请求
        initQiYeXiangQingHttp();
    }


//    企业详情的网络请求
//    tiantian    enterId(应该传企业id)暂时传45
    private void initQiYeXiangQingHttp() {
        final ProgressDialog progressDialog = new ProgressDialog(QiYeXiangQingActivity.this);
        progressDialog.setTitle("提示");
        progressDialog.setMessage("请等待...");
        progressDialog.setCancelable(false);
        progressDialog.show();
        OkHttpUtils.post()
                .url(ContractUtils.LOGIN_URL+"company/getDetail")
                .addHeader("token",ContractUtils.getTOKEN(QiYeXiangQingActivity.this))
                .addParams("parkId",ContractUtils.getParkId(QiYeXiangQingActivity.this))
                .addParams("enterId", id)
                .build()
                .execute(new StringCallback() {
                    @Override
                    public void onError(Call call, Exception e, int id) {
                        System.out.println(e+"     eeeeeeeeee企业详情的网络请求");
                        progressDialog.dismiss();
                    }

                    @Override
                    public void onResponse(String response, int id) {
                        progressDialog.dismiss();
                        ContractUtils.Code500(QiYeXiangQingActivity.this,response);
                        System.out.println(response+"     企业详情的网络请求");

                        if(response.indexOf("200") != -1){
                            Gson gson = new Gson();
                            qiYeXiangQingEntity = gson.fromJson(response, QiYeXiangQingEntity.class);
                            if(qiYeXiangQingEntity == null){
                                return;
                            }

                            if(qiYeXiangQingEntity.getResponse().getInfo().getLogoUrl() == null){
                                Glide.with(QiYeXiangQingActivity.this).load(R.drawable.wanyang).into(imageTouxiang);
                            }else{ // 头像
                                Glide.with(QiYeXiangQingActivity.this).load(qiYeXiangQingEntity.getResponse().getInfo().getLogoUrl()).into(imageTouxiang);
                            }


                            if(qiYeXiangQingEntity.getResponse().getInfo().getEnterName() == null){

                            }else{ // 标题
                                tvShopName.setText(qiYeXiangQingEntity.getResponse().getInfo().getEnterName());
                            }


                            if(qiYeXiangQingEntity.getResponse().getInfo().getContact() == null){

                            }else{ // 联系电话
                                tvPhone.setText("联系电话："+qiYeXiangQingEntity.getResponse().getInfo().getContact());
                            }


                            if(qiYeXiangQingEntity.getResponse().getInfo().getAddress() == null){

                            }else{// 位置
                                tvAddress.setText("企业位置："+qiYeXiangQingEntity.getResponse().getInfo().getAddress());
                            }


                            if(qiYeXiangQingEntity.getResponse().getInfo().getDesc() == null){

                            }else{
                                String desc = qiYeXiangQingEntity.getResponse().getInfo().getDesc();
                                String replace1 = desc.replace("&nbsp;", " ");
                                tvJianjie.setText("企业简介："+replace1);
                            }

                            qiYeXiangQingAdapter = new QiYeXiangQingAdapter(QiYeXiangQingActivity.this,qiYeXiangQingEntity.getResponse().getRecruit());
                            LinearLayoutManager manager = new LinearLayoutManager(QiYeXiangQingActivity.this);
                            reecyclerView.setLayoutManager(manager);
                            reecyclerView.setAdapter(qiYeXiangQingAdapter);



                            qiYeXiangQingAdapter.setXiangQingClick(new QiYeXiangQingAdapter.XiangQingClick() {
                                @Override
                                public void xiangqingClick(int position, String id) {
                                    Intent intent = new Intent(QiYeXiangQingActivity.this, YuanQuQiYeQiYeZhaoPinXiangQingActivity.class);
                                    intent.putExtra("zhaopinid",id);
                                    System.out.println(id+"    点击招聘列表行布局传送的招聘id");
                                    startActivity(intent);
                                }
                            });
                        }
                    }
                });
    }


    @OnClick({R.id.image_back, R.id.image_touxiang, R.id.tv_shop_name, R.id.tv_phone, R.id.tv_address, R.id.image_address, R.id.tv_jianjie, R.id.reecyclerView})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.image_back:
                finish();
                break;
            case R.id.image_touxiang:
                break;
            case R.id.tv_shop_name:
                break;
            case R.id.tv_phone:
                break;
            case R.id.tv_address:
                break;
            case R.id.image_address:
                if(qiYeXiangQingEntity == null){
                    return;
                }
                    if(qiYeXiangQingEntity.getResponse().getInfo().getLat().equals("") && qiYeXiangQingEntity.getResponse().getInfo().getLng().equals("")){
                        Toast.makeText(this, "暂无地址！", Toast.LENGTH_SHORT).show();
                        return;
                    }
                    Intent intent = new Intent(QiYeXiangQingActivity.this, DiTuActivity.class);
                    intent.putExtra("lat",qiYeXiangQingEntity.getResponse().getInfo().getLat());
                    intent.putExtra("lng",qiYeXiangQingEntity.getResponse().getInfo().getLng());
                    intent.putExtra("name",qiYeXiangQingEntity.getResponse().getInfo().getEnterName());
                    startActivity(intent);

                
                break;
            case R.id.tv_jianjie:
                break;
            case R.id.reecyclerView:
                break;
        }
    }
}
