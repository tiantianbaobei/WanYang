package com.wanyangproject.shouye;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.ViewFlipper;

import com.bumptech.glide.Glide;
import com.google.gson.Gson;
import com.wanyangproject.R;
import com.wanyangproject.activity.HomeActivity;
import com.wanyangproject.activity.WenZhangXiangQingActivity;
import com.wanyangproject.activity.YuanQuZhaoShangFenLeiActivity;
import com.wanyangproject.activity.YuanQuZhaoShangXiangQingActivity;
import com.wanyangproject.activity.ZhengCeFaGuiYiJianFanKuiActivity;
import com.wanyangproject.adapter.ZhengCeFaGuiFuWuFenLeiAdapter;
import com.wanyangproject.adapter.ZhengCeFaGuiHuaDongAdapter;
import com.wanyangproject.entity.ShouYeBannerEntity;
import com.wanyangproject.entity.YuanQuZhaoShangEntity;
import com.wanyangproject.fuwuactivity.FangKeDengJiActivity;
import com.wanyangproject.fuwuactivity.YuanQuTongZhiActivity;
import com.wanyangproject.my.MyBangDingActivity;
import com.wanyangproject.my.MyShouCangActivity;
import com.wanyangproject.my.QuanBuActivity;
import com.wanyangproject.my.RuZhuShenQingActivity;
import com.wanyangproject.my.SettingActivity;
import com.wanyangproject.utils.BiaoTiTiaoZhuFuWuFragment;
import com.wanyangproject.utils.ContractUtils;
import com.wanyangproject.utils.FuWuTiaoZhuFragment;
import com.wanyangproject.utils.FuWuTiaoZhuan;
import com.wanyangproject.utils.LunTanTiaoZhuan;
import com.wanyangproject.utils.TiaoZhuanFragment;
import com.youth.banner.Banner;
import com.youth.banner.BannerConfig;
import com.youth.banner.listener.OnBannerListener;
import com.youth.banner.loader.ImageLoader;
import com.zhy.http.okhttp.OkHttpUtils;
import com.zhy.http.okhttp.callback.StringCallback;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.Call;

public class ZhengCeFaGuiActivity extends AppCompatActivity implements ViewPager.OnPageChangeListener {

    @BindView(R.id.image_back)
    ImageView imageBack;
    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    @BindView(R.id.recyclerView_fuwufenlei)
    RecyclerView recyclerViewFuwufenlei;
    @BindView(R.id.viewflipper)
    ViewFlipper viewflipper;
    @BindView(R.id.banner)
    Banner banner;
    @BindView(R.id.tv_fankui)
    TextView tvFankui;
    @BindView(R.id.relative_fankui)
    RelativeLayout relativeFankui;
    @BindView(R.id.image)
    ImageView image;
    //    private ViewPager viewPager;
    private TextView tv_scroll_one;

    private ZhengCeFaGuiHuaDongAdapter zhengCeFaGuiHuaDongAdapter;
    private List<View> list = new ArrayList<>();
    private Boolean isOk = true;
    private ZhengCeFaGuiFuWuFenLeiAdapter zhengCeFaGuiFuWuFenLeiAdapter;
    private YuanQuZhaoShangEntity yuanQuZhaoShangEntity;
    private ShouYeBannerEntity shouYeBannerEntity;
//    private TiaoZhuanFragment tiaoZhuanFragment;
//    private FuWuTiaoZhuFragment fuWuTiaoZhuFragment;
//    private BiaoTiTiaoZhuFuWuFragment biaoTiTiaoZhuFuWuFragment;


    //    public void setFuWuTiaoZhuFragment(FuWuTiaoZhuFragment fuWuTiaoZhuFragment) {
//        this.fuWuTiaoZhuFragment = fuWuTiaoZhuFragment;
//    }
//
//    public void setTiaoZhuanFragment(TiaoZhuanFragment tiaoZhuanFragment) {
//        this.tiaoZhuanFragment = tiaoZhuanFragment;
//    }


//    private Handler handler = new Handler() {
//        @Override
//        public void handleMessage(Message msg) {
//            super.handleMessage(msg);
//            switch (msg.what) {
//                case 1:
//                    viewPager.setCurrentItem(msg.arg1);
//                    break;
//                case 2:
//                    viewPager.setCurrentItem(msg.arg1);
//                    break;
//            }
//        }
//    };


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_zheng_ce_fa_gui);
        ButterKnife.bind(this);
        initView();

//        viewPager = (ViewPager) findViewById(R.id.viewPager);
//
//        View viewOne = LayoutInflater.from(ZhengCeFaGuiActivity.this).inflate(R.layout.item_one, null);
//        View viewTwo = LayoutInflater.from(ZhengCeFaGuiActivity.this).inflate(R.layout.item_two, null);
//        View viewThree = LayoutInflater.from(ZhengCeFaGuiActivity.this).inflate(R.layout.item_three, null);
//
//        list.add(viewOne);
//        list.add(viewTwo);
//        list.add(viewThree);
//
//        MyAdapter adapter = new MyAdapter(list);
//        viewPager.setAdapter(adapter);
//
//        initThread();
//
//
//        viewPager.addOnPageChangeListener(this);


//        政策法规的网络请求
        initZhengCeFaGuiHttp();


//        政策法规banner网络请求
        initBannerHttp();


    }


    private Bitmap comp(Bitmap image) {

        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        image.compress(Bitmap.CompressFormat.JPEG, 100, baos);
        if( baos.toByteArray().length / 1024>1024) {//判断如果图片大于1M,进行压缩避免在生成图片（BitmapFactory.decodeStream）时溢出
            baos.reset();//重置baos即清空baos
            image.compress(Bitmap.CompressFormat.JPEG, 50, baos);//这里压缩50%，把压缩后的数据存放到baos中
        }
        ByteArrayInputStream isBm = new ByteArrayInputStream(baos.toByteArray());
        BitmapFactory.Options newOpts = new BitmapFactory.Options();
        //开始读入图片，此时把options.inJustDecodeBounds 设回true了
        newOpts.inJustDecodeBounds = true;
        Bitmap bitmap = BitmapFactory.decodeStream(isBm, null, newOpts);
        newOpts.inJustDecodeBounds = false;
        int w = newOpts.outWidth;
        int h = newOpts.outHeight;
        //现在主流手机比较多是800*480分辨率，所以高和宽我们设置为
        float hh = 800f;//这里设置高度为800f
        float ww = 480f;//这里设置宽度为480f
        //缩放比。由于是固定比例缩放，只用高或者宽其中一个数据进行计算即可
        int be = 1;//be=1表示不缩放
        if (w > h && w > ww) {//如果宽度大的话根据宽度固定大小缩放
            be = (int) (newOpts.outWidth / ww);
        } else if (w < h && h > hh) {//如果高度高的话根据宽度固定大小缩放
            be = (int) (newOpts.outHeight / hh);
        }
        if (be <= 0)
            be = 1;
        newOpts.inSampleSize = be;//设置缩放比例
        //重新读入图片，注意此时已经把options.inJustDecodeBounds 设回false了
        isBm = new ByteArrayInputStream(baos.toByteArray());
        bitmap = BitmapFactory.decodeStream(isBm, null, newOpts);
        return compressImage(bitmap);//压缩好比例大小后再进行质量压缩
    }




    private Bitmap compressImage(Bitmap image) {

        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        image.compress(Bitmap.CompressFormat.JPEG, 100, baos);//质量压缩方法，这里100表示不压缩，把压缩后的数据存放到baos中
        int options = 100;
        while ( baos.toByteArray().length / 1024>100) { //循环判断如果压缩后图片是否大于100kb,大于继续压缩
            baos.reset();//重置baos即清空baos
            image.compress(Bitmap.CompressFormat.JPEG, options, baos);//这里压缩options%，把压缩后的数据存放到baos中
            options -= 10;//每次都减少10
        }
        ByteArrayInputStream isBm = new ByteArrayInputStream(baos.toByteArray());//把压缩后的数据baos存放到ByteArrayInputStream中
        Bitmap bitmap = BitmapFactory.decodeStream(isBm, null, null);//把ByteArrayInputStream数据生成图片
        return bitmap;
    }






    //    政策法规banner网络请求
    private void initBannerHttp() {
        final ProgressDialog progressDialog = new ProgressDialog(ZhengCeFaGuiActivity.this);
        progressDialog.setTitle("提示");
        progressDialog.setMessage("请等待...");
        progressDialog.setCancelable(false);
        progressDialog.show();
        OkHttpUtils.post()
                .url(ContractUtils.LOGIN_URL + "news/getBanner")
                .addHeader("token", ContractUtils.getTOKEN(ZhengCeFaGuiActivity.this))
                .addParams("parkId", ContractUtils.getParkId(ZhengCeFaGuiActivity.this))
                .addParams("typeId", "3")
                .build()
                .execute(new StringCallback() {
                    @Override
                    public void onError(Call call, Exception e, int id) {
                        progressDialog.dismiss();
                    }

                    @Override
                    public void onResponse(String response, int id) {
                        System.out.println(response + "         政策法规banner网络请求");
                        ContractUtils.Code500(ZhengCeFaGuiActivity.this, response);
                        progressDialog.dismiss();
                        if (response.indexOf("200") != -1) {
                            Gson gson = new Gson();
                            shouYeBannerEntity = gson.fromJson(response, ShouYeBannerEntity.class);

                            //     banner轮播
                            ArrayList<String> list = new ArrayList<String>();
                            for (int i = 0; i < shouYeBannerEntity.getResponse().size(); i++) {
//                                YuanQuZhaoShangEntity.ResponseBean.BannerArrBean bannerArrBean = yuanQuZhaoShangEntity.getResponse().getBannerArr().get(i);
//                            list.add(ContractUtils.YUANQUZHAOSHANG_PHOTO_URL + bannerArrBean.getLogo());
                                String image = shouYeBannerEntity.getResponse().get(i).getImage();
                                list.add(image);
                            }
                            banner.setBannerStyle(BannerConfig.CIRCLE_INDICATOR);
                            banner.setImageLoader(new MyImageLoader());
                            banner.setImages(list);
                            banner.setDelayTime(2000);
                            banner.setOnBannerListener(new OnBannerListener() {
                                @Override
                                public void OnBannerClick(int position) {
//                                    Intent intent = new Intent(ZhengCeFaGuiActivity.this, ZhengCeFaGuiXiangQingActivity.class);
//                                    intent.putExtra("id", yuanQuZhaoShangEntity.getResponse().getBannerArr().get(position).getId());
//                                    System.out.println(yuanQuZhaoShangEntity.getResponse().getBannerArr().get(position).getId() + "     点击banner详情的传送的id");
//                                    startActivity(intent);

                                    if (shouYeBannerEntity.getResponse().get(position).getHrefType().equals("0")) {
                                        Intent intenttongzhi = new Intent(ZhengCeFaGuiActivity.this, YuanQuTongZhiActivity.class);
                                        intenttongzhi.putExtra("zhi","zhi");
                                        startActivity(intenttongzhi);

//                                        Intent intent1 = new Intent(ZhengCeFaGuiActivity.this, ChanYeJinRongActivity.class);
//                                        startActivity(intent1);
                                    } else if (shouYeBannerEntity.getResponse().get(position).getHrefType().equals("1")) {
                                        Intent intent1 = new Intent(ZhengCeFaGuiActivity.this, YuanQuShouCeActivity.class);
                                        startActivity(intent1);
                                    } else if (shouYeBannerEntity.getResponse().get(position).getHrefType().equals("2")) {
                                        Intent intent1 = new Intent(ZhengCeFaGuiActivity.this, YuanQuQiYeActivity.class);
                                        startActivity(intent1);
                                    } else if (shouYeBannerEntity.getResponse().get(position).getHrefType().equals("3")) {
                                        Intent intent1 = new Intent(ZhengCeFaGuiActivity.this, ZhengCeFaGuiActivity.class);
                                        startActivity(intent1);
                                    } else if (shouYeBannerEntity.getResponse().get(position).getHrefType().equals("4")) {
                                        Intent intent1 = new Intent(ZhengCeFaGuiActivity.this, ZhiNengZhiZaoActivity.class);
                                        startActivity(intent1);
                                    } else if (shouYeBannerEntity.getResponse().get(position).getHrefType().equals("5")) {
                                        Intent intent1 = new Intent(ZhengCeFaGuiActivity.this, YuanQuZhaoShangActivity.class);
                                        startActivity(intent1);
                                    } else if (shouYeBannerEntity.getResponse().get(position).getHrefType().equals("6")) {
                                        Intent intent2 = new Intent(ZhengCeFaGuiActivity.this, HomeActivity.class);
                                        intent2.putExtra("luntan","luntan");
                                        startActivity(intent2);
//                                        if (lunTanTiaoZhuan != null) {
//                                            lunTanTiaoZhuan.onClicklunatn();
//                                        }
                                    } else if (shouYeBannerEntity.getResponse().get(position).getHrefType().equals("7")) {
                                        Intent intent2 = new Intent(ZhengCeFaGuiActivity.this, HomeActivity.class);
                                        intent2.putExtra("fuwu","fuwu");
                                        startActivity(intent2);
//                                        if (fuWuTiaoZhuan != null) {
//                                            fuWuTiaoZhuan.onClickfuwu();
//                                        }
                                    } else if (shouYeBannerEntity.getResponse().get(position).getHrefType().equals("8")) {
                                        Intent intent = new Intent(ZhengCeFaGuiActivity.this, HomeActivity.class);
                                        startActivity(intent);
                                    } else if (shouYeBannerEntity.getResponse().get(position).getHrefType().equals("9")) {
                                        if (shouYeBannerEntity.getResponse().get(position).getContent() != null) {
                                            Intent intent = new Intent(ZhengCeFaGuiActivity.this, WenZhangXiangQingActivity.class);
                                            intent.putExtra("content", shouYeBannerEntity.getResponse().get(position).getContent());
                                            startActivity(intent);
                                        }
                                    } else if (shouYeBannerEntity.getResponse().get(position).getHrefType().equals("10")) {
                                        if (shouYeBannerEntity.getResponse().get(position).getContent() != null) {
                                            Intent intent = new Intent(ZhengCeFaGuiActivity.this, YuanQuShengHuoActivity.class);
                                            startActivity(intent);
                                        }
                                    } else if (shouYeBannerEntity.getResponse().get(position).getHrefType().equals("11")) {
                                        if (shouYeBannerEntity.getResponse().get(position).getContent() != null) {
                                            return;
                                        }
                                    } else if (shouYeBannerEntity.getResponse().get(position).getHrefType().equals("12")) {
                                        if (shouYeBannerEntity.getResponse().get(position).getContent() != null) {
                                            Intent intent = new Intent(ZhengCeFaGuiActivity.this, SettingActivity.class);
                                            startActivity(intent);
                                        }
                                    } else if (shouYeBannerEntity.getResponse().get(position).getHrefType().equals("13")) {
                                        if (shouYeBannerEntity.getResponse().get(position).getContent() != null) {
                                            Intent intent = new Intent(ZhengCeFaGuiActivity.this, QiuZhiZhaoPinActivity.class);
                                            startActivity(intent);
                                        }
                                    } else if (shouYeBannerEntity.getResponse().get(position).getHrefType().equals("14")) {
                                        if (shouYeBannerEntity.getResponse().get(position).getContent() != null) {
                                            Intent intent = new Intent(ZhengCeFaGuiActivity.this, ChanYeJinRongActivity.class);
                                            startActivity(intent);
                                        }
                                    } else if (shouYeBannerEntity.getResponse().get(position).getHrefType().equals("15")) {
                                        if (shouYeBannerEntity.getResponse().get(position).getContent() != null) {
                                            Intent intent = new Intent(ZhengCeFaGuiActivity.this, YuanQuTongZhiActivity.class);
                                            startActivity(intent);
                                        }
                                    } else if (shouYeBannerEntity.getResponse().get(position).getHrefType().equals("16")) {
                                        if (shouYeBannerEntity.getResponse().get(position).getContent() != null) {
                                            Intent intent = new Intent(ZhengCeFaGuiActivity.this, FangKeDengJiActivity.class);
                                            startActivity(intent);
                                        }
                                    } else if (shouYeBannerEntity.getResponse().get(position).getHrefType().equals("17")) {
                                        if (shouYeBannerEntity.getResponse().get(position).getContent() != null) {
                                            Intent intent = new Intent(ZhengCeFaGuiActivity.this, QuanBuActivity.class);
                                            intent.putExtra("daizhifu","0");
                                            startActivity(intent);
                                        }
                                    } else if (shouYeBannerEntity.getResponse().get(position).getHrefType().equals("18")) {
                                        if (shouYeBannerEntity.getResponse().get(position).getContent() != null) {
                                            Intent intent = new Intent(ZhengCeFaGuiActivity.this, MyShouCangActivity.class);
                                            startActivity(intent);
                                        }
                                    } else if (shouYeBannerEntity.getResponse().get(position).getHrefType().equals("19")) {
                                        if (shouYeBannerEntity.getResponse().get(position).getContent() != null) {
                                            Intent intent = new Intent(ZhengCeFaGuiActivity.this, ShouHuoXinXiActivity.class);
                                            startActivity(intent);
                                        }
                                    } else if (shouYeBannerEntity.getResponse().get(position).getHrefType().equals("20")) {
                                        if (shouYeBannerEntity.getResponse().get(position).getContent() != null) {
                                            Intent intent = new Intent(ZhengCeFaGuiActivity.this, RuZhuShenQingActivity.class);
                                            startActivity(intent);
                                        }
                                    } else if (shouYeBannerEntity.getResponse().get(position).getHrefType().equals("21")) {
                                        if (shouYeBannerEntity.getResponse().get(position).getContent() != null) {
                                            Intent intent = new Intent(ZhengCeFaGuiActivity.this, MyBangDingActivity.class);
                                            startActivity(intent);
                                        }
                                    }
                                }
                            });
                            banner.start();
                        }
                    }
                });
    }


    //    政策法规的网络请求
    private void initZhengCeFaGuiHttp() {
        final ProgressDialog progressDialog = new ProgressDialog(ZhengCeFaGuiActivity.this);
        progressDialog.setTitle("提示");
        progressDialog.setMessage("请等待...");
        progressDialog.setCancelable(false);
        progressDialog.show();
        OkHttpUtils.post()
                .url(ContractUtils.LOGIN_URL + "news/getCategory")
                .addHeader("token", ContractUtils.getTOKEN(ZhengCeFaGuiActivity.this))
                .addParams("parkId", ContractUtils.getParkId(ZhengCeFaGuiActivity.this))
                .addParams("typeId", "3")    //1:园区招商，2：企服中心，3:政策法规
                .build()
                .execute(new StringCallback() {
                    @Override
                    public void onError(Call call, Exception e, int id) {
                        progressDialog.dismiss();
                    }

                    @Override
                    public void onResponse(String response, int id) {
                        progressDialog.dismiss();
                        ContractUtils.Code500(ZhengCeFaGuiActivity.this, response);
                        System.out.println(response + "    政策法规的网络请求");
                        if (response.indexOf("200") != -1) {
                            Gson gson = new Gson();
                            yuanQuZhaoShangEntity = gson.fromJson(response, YuanQuZhaoShangEntity.class);


                            if(yuanQuZhaoShangEntity.getResponse().getBackground() == null){

                            }else{
                                Glide.with(ZhengCeFaGuiActivity.this).load(yuanQuZhaoShangEntity.getResponse().getBackground()).into(image);
                            }


                            zhengCeFaGuiHuaDongAdapter = new ZhengCeFaGuiHuaDongAdapter(ZhengCeFaGuiActivity.this, yuanQuZhaoShangEntity.getResponse().getUpTjArr());
                            LinearLayoutManager manager = new LinearLayoutManager(ZhengCeFaGuiActivity.this);
                            //设置为横向滑动
                            manager.setOrientation(LinearLayoutManager.HORIZONTAL);
                            recyclerView.setLayoutManager(manager);
                            recyclerView.setAdapter(zhengCeFaGuiHuaDongAdapter);

                            zhengCeFaGuiHuaDongAdapter.setXingQingClick(new ZhengCeFaGuiHuaDongAdapter.XingQingClick() {
                                @Override
                                public void xiangqingClick(int position) {
//                                    Intent intent = new Intent(ZhengCeFaGuiActivity.this, ZhengCeFaGuiXiangQingActivity.class);
//                                    intent.putExtra("id", yuanQuZhaoShangEntity.getResponse().getUpTjArr().get(position).getId());
//                                    System.out.println(yuanQuZhaoShangEntity.getResponse().getUpTjArr().get(position).getId() + "       点击上面滑动的传送的文章的id");
//                                    startActivity(intent);




                                    if (yuanQuZhaoShangEntity.getResponse().getUpTjArr().get(position).getHrefType().equals("0")) {
                                        Intent intenttongzhi = new Intent(ZhengCeFaGuiActivity.this, YuanQuTongZhiActivity.class);
                                        intenttongzhi.putExtra("zhi","zhi");
                                        startActivity(intenttongzhi);

//                                        Intent intent1 = new Intent(YuanQuZhaoShangActivity.this, ChanYeJinRongActivity.class);
//                                        startActivity(intent1);
                                    } else if (yuanQuZhaoShangEntity.getResponse().getUpTjArr().get(position).getHrefType().equals("1")) {
                                        Intent intent1 = new Intent(ZhengCeFaGuiActivity.this, YuanQuShouCeActivity.class);
                                        startActivity(intent1);
                                    } else if (yuanQuZhaoShangEntity.getResponse().getUpTjArr().get(position).getHrefType().equals("2")) {
                                        Intent intent1 = new Intent(ZhengCeFaGuiActivity.this, YuanQuQiYeActivity.class);
                                        startActivity(intent1);
                                    } else if (yuanQuZhaoShangEntity.getResponse().getUpTjArr().get(position).getHrefType().equals("3")) {
                                        Intent intent1 = new Intent(ZhengCeFaGuiActivity.this, ZhengCeFaGuiActivity.class);
                                        startActivity(intent1);
                                    } else if (yuanQuZhaoShangEntity.getResponse().getUpTjArr().get(position).getHrefType().equals("4")) {
                                        Intent intent1 = new Intent(ZhengCeFaGuiActivity.this, ZhiNengZhiZaoActivity.class);
                                        startActivity(intent1);
                                    } else if (yuanQuZhaoShangEntity.getResponse().getUpTjArr().get(position).getHrefType().equals("5")) {
                                        Intent intent1 = new Intent(ZhengCeFaGuiActivity.this, YuanQuZhaoShangActivity.class);
                                        startActivity(intent1);
                                    } else if (yuanQuZhaoShangEntity.getResponse().getUpTjArr().get(position).getHrefType().equals("6")) {
                                        Intent intent2 = new Intent(ZhengCeFaGuiActivity.this, HomeActivity.class);
                                        intent2.putExtra("luntan","luntan");
                                        startActivity(intent2);
//                                        if (lunTanTiaoZhuan != null) {
//                                            lunTanTiaoZhuan.onClicklunatn();
//                                        }
                                    } else if (yuanQuZhaoShangEntity.getResponse().getUpTjArr().get(position).getHrefType().equals("7")) {
                                        Intent intent2 = new Intent(ZhengCeFaGuiActivity.this, HomeActivity.class);
                                        intent2.putExtra("fuwu","fuwu");
                                        startActivity(intent2);
//                                        if (fuWuTiaoZhuan != null) {
//                                            fuWuTiaoZhuan.onClickfuwu();
//                                        }
                                    } else if (yuanQuZhaoShangEntity.getResponse().getUpTjArr().get(position).getHrefType().equals("8")) {
                                        Intent intent = new Intent(ZhengCeFaGuiActivity.this, HomeActivity.class);
                                        startActivity(intent);
                                    } else if (yuanQuZhaoShangEntity.getResponse().getUpTjArr().get(position).getHrefType().equals("9")) {
                                        if (yuanQuZhaoShangEntity.getResponse().getUpTjArr().get(position).getContent() != null) {
                                            Intent intent = new Intent(ZhengCeFaGuiActivity.this, WenZhangXiangQingActivity.class);
                                            intent.putExtra("content", yuanQuZhaoShangEntity.getResponse().getUpTjArr().get(position).getContent());
                                            startActivity(intent);
                                        }
                                    } else if (yuanQuZhaoShangEntity.getResponse().getUpTjArr().get(position).getHrefType().equals("10")) {
                                        if (yuanQuZhaoShangEntity.getResponse().getUpTjArr().get(position).getContent() != null) {
                                            Intent intent = new Intent(ZhengCeFaGuiActivity.this, YuanQuShengHuoActivity.class);
                                            startActivity(intent);
                                        }
                                    } else if (yuanQuZhaoShangEntity.getResponse().getUpTjArr().get(position).getHrefType().equals("11")) {
                                        if (yuanQuZhaoShangEntity.getResponse().getUpTjArr().get(position).getContent() != null) {
                                            return;
                                        }
                                    } else if (yuanQuZhaoShangEntity.getResponse().getUpTjArr().get(position).getHrefType().equals("12")) {
                                        if (yuanQuZhaoShangEntity.getResponse().getUpTjArr().get(position).getContent() != null) {
                                            Intent intent = new Intent(ZhengCeFaGuiActivity.this, SettingActivity.class);
                                            startActivity(intent);
                                        }
                                    } else if (yuanQuZhaoShangEntity.getResponse().getUpTjArr().get(position).getHrefType().equals("13")) {
                                        if (yuanQuZhaoShangEntity.getResponse().getUpTjArr().get(position).getContent() != null) {
                                            Intent intent = new Intent(ZhengCeFaGuiActivity.this, QiuZhiZhaoPinActivity.class);
                                            startActivity(intent);
                                        }
                                    } else if (yuanQuZhaoShangEntity.getResponse().getUpTjArr().get(position).getHrefType().equals("14")) {
                                        if (yuanQuZhaoShangEntity.getResponse().getUpTjArr().get(position).getContent() != null) {
                                            Intent intent = new Intent(ZhengCeFaGuiActivity.this, ChanYeJinRongActivity.class);
                                            startActivity(intent);
                                        }
                                    } else if (yuanQuZhaoShangEntity.getResponse().getUpTjArr().get(position).getHrefType().equals("15")) {
                                        if (yuanQuZhaoShangEntity.getResponse().getUpTjArr().get(position).getContent() != null) {
                                            Intent intent = new Intent(ZhengCeFaGuiActivity.this, YuanQuTongZhiActivity.class);
                                            startActivity(intent);
                                        }
                                    } else if (yuanQuZhaoShangEntity.getResponse().getUpTjArr().get(position).getHrefType().equals("16")) {
                                        if (yuanQuZhaoShangEntity.getResponse().getUpTjArr().get(position).getContent() != null) {
                                            Intent intent = new Intent(ZhengCeFaGuiActivity.this, FangKeDengJiActivity.class);
                                            startActivity(intent);
                                        }
                                    } else if (yuanQuZhaoShangEntity.getResponse().getUpTjArr().get(position).getHrefType().equals("17")) {
                                        if (yuanQuZhaoShangEntity.getResponse().getUpTjArr().get(position).getContent() != null) {
                                            Intent intent = new Intent(ZhengCeFaGuiActivity.this, QuanBuActivity.class);
                                            intent.putExtra("daizhifu","0");
                                            startActivity(intent);
                                        }
                                    } else if (yuanQuZhaoShangEntity.getResponse().getUpTjArr().get(position).getHrefType().equals("18")) {
                                        if (yuanQuZhaoShangEntity.getResponse().getUpTjArr().get(position).getContent() != null) {
                                            Intent intent = new Intent(ZhengCeFaGuiActivity.this, MyShouCangActivity.class);
                                            startActivity(intent);
                                        }
                                    } else if (yuanQuZhaoShangEntity.getResponse().getUpTjArr().get(position).getHrefType().equals("19")) {
                                        if (yuanQuZhaoShangEntity.getResponse().getUpTjArr().get(position).getContent() != null) {
                                            Intent intent = new Intent(ZhengCeFaGuiActivity.this, ShouHuoXinXiActivity.class);
                                            startActivity(intent);
                                        }
                                    } else if (yuanQuZhaoShangEntity.getResponse().getUpTjArr().get(position).getHrefType().equals("20")) {
                                        if (yuanQuZhaoShangEntity.getResponse().getUpTjArr().get(position).getContent() != null) {
                                            Intent intent = new Intent(ZhengCeFaGuiActivity.this, RuZhuShenQingActivity.class);
                                            startActivity(intent);
                                        }
                                    } else if (yuanQuZhaoShangEntity.getResponse().getUpTjArr().get(position).getHrefType().equals("21")) {
                                        if (yuanQuZhaoShangEntity.getResponse().getUpTjArr().get(position).getContent() != null) {
                                            Intent intent = new Intent(ZhengCeFaGuiActivity.this, MyBangDingActivity.class);
                                            startActivity(intent);
                                        }
                                    }





//                                    if (yuanQuZhaoShangEntity.getResponse().getUpTjArr().get(position).getHrefType().equals("0")) {
//                                        Intent intent1 = new Intent(ZhengCeFaGuiActivity.this, ChanYeJinRongActivity.class);
//                                        startActivity(intent1);
//                                    } else if (yuanQuZhaoShangEntity.getResponse().getUpTjArr().get(position).getHrefType().equals("1")) {
//                                        Intent intent1 = new Intent(ZhengCeFaGuiActivity.this, YuanQuShouCeActivity.class);
//                                        startActivity(intent1);
//                                    } else if (yuanQuZhaoShangEntity.getResponse().getUpTjArr().get(position).getHrefType().equals("2")) {
//                                        Intent intent1 = new Intent(ZhengCeFaGuiActivity.this, YuanQuQiYeActivity.class);
//                                        startActivity(intent1);
//                                    } else if (yuanQuZhaoShangEntity.getResponse().getUpTjArr().get(position).getHrefType().equals("3")) {
//                                        Intent intent1 = new Intent(ZhengCeFaGuiActivity.this, ZhengCeFaGuiActivity.class);
//                                        startActivity(intent1);
//                                    } else if (yuanQuZhaoShangEntity.getResponse().getUpTjArr().get(position).getHrefType().equals("4")) {
//                                        Intent intent1 = new Intent(ZhengCeFaGuiActivity.this, ZhiNengZhiZaoActivity.class);
//                                        startActivity(intent1);
//                                    } else if (yuanQuZhaoShangEntity.getResponse().getUpTjArr().get(position).getHrefType().equals("5")) {
//                                        Intent intent1 = new Intent(ZhengCeFaGuiActivity.this, YuanQuZhaoShangActivity.class);
//                                        startActivity(intent1);
//                                    } else if (yuanQuZhaoShangEntity.getResponse().getUpTjArr().get(position).getHrefType().equals("6")) {
//                                        if (tiaoZhuanFragment != null) {
//                                            tiaoZhuanFragment.onClick();
//                                        }
//                                    } else if (yuanQuZhaoShangEntity.getResponse().getUpTjArr().get(position).getHrefType().equals("7")) {
//                                        if (fuWuTiaoZhuFragment != null) {
//                                            fuWuTiaoZhuFragment.onClick1();
//                                        }
//                                    } else if (yuanQuZhaoShangEntity.getResponse().getUpTjArr().get(position).getHrefType().equals("8")) {
//                                        Intent intent = new Intent(ZhengCeFaGuiActivity.this, HomeActivity.class);
//                                        startActivity(intent);
//                                    } else if (yuanQuZhaoShangEntity.getResponse().getUpTjArr().get(position).getHrefType().equals("9")) {
//                                        if (yuanQuZhaoShangEntity.getResponse().getUpTjArr().get(position).getContent() != null) {
//                                            Intent intent = new Intent(ZhengCeFaGuiActivity.this, WenZhangXiangQingActivity.class);
//                                            intent.putExtra("content", yuanQuZhaoShangEntity.getResponse().getUpTjArr().get(position).getContent());
//                                            startActivity(intent);
//                                        }
//                                    }


                                }
                            });


                            for (int i = 0; i < yuanQuZhaoShangEntity.getResponse().getWordArr().size(); i++) {
                                View view = View.inflate(ZhengCeFaGuiActivity.this, R.layout.scroll_tv, null);
                                tv_scroll_one = (TextView) view.findViewById(R.id.tv_scroll_one);
                                tv_scroll_one.setText(yuanQuZhaoShangEntity.getResponse().getWordArr().get(i).getTitle());
                                viewflipper.addView(view);
                            }
                            viewflipper.startFlipping();
                            viewflipper.setInAnimation(AnimationUtils.loadAnimation(ZhengCeFaGuiActivity.this, R.anim.push_down_in));
                            viewflipper.setOutAnimation(AnimationUtils.loadAnimation(ZhengCeFaGuiActivity.this, R.anim.push_down_out));


                            viewflipper.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View arg0) {
                                    // TODO Auto-generated method stub
//                                        startActivity(new Intent(MainActivity.this, Art.class));
//                                Intent intent = new Intent(getContext(), QiYeXiangQingActivity.class);
////                                    应该传企业的id   tiantian
//                                System.out.println(viewflipper.getDisplayedChild()+"              zzzzzzzzzzzzzzzzz");
//                                intent.putExtra("id",shouYeTuiJianQiYeEntity.getResponse().get(viewflipper.getDisplayedChild()).getEnterId());
//                                startActivity(intent);


                                    Intent intent = new Intent(ZhengCeFaGuiActivity.this, YuanQuZhaoShangXiangQingActivity.class);
                                    intent.putExtra("id", yuanQuZhaoShangEntity.getResponse().getWordArr().get(viewflipper.getDisplayedChild()).getId());
                                    intent.putExtra("typeId","4");///0 园区手册   1 智能制造   2 产业金融   3 园区资讯   4 政策法规    5企服中心    6 园区招商
                                    System.out.println(yuanQuZhaoShangEntity.getResponse().getWordArr().get(viewflipper.getDisplayedChild()).getId() + "     点击banner详情的传送的id");
                                    startActivity(intent);


//                                    if (yuanQuZhaoShangEntity.getResponse().getWordArr().get(viewflipper.getDisplayedChild()).getHrefType() != null) {
//                                        if (yuanQuZhaoShangEntity.getResponse().getWordArr().get(viewflipper.getDisplayedChild()).getHrefType().equals("0")) {
//                                            Intent intent1 = new Intent(ZhengCeFaGuiActivity.this, ChanYeJinRongActivity.class);
//                                            startActivity(intent1);
//                                        } else if (yuanQuZhaoShangEntity.getResponse().getWordArr().get(viewflipper.getDisplayedChild()).getHrefType().equals("1")) {
//                                            Intent intent1 = new Intent(ZhengCeFaGuiActivity.this, YuanQuShouCeActivity.class);
//                                            startActivity(intent1);
//                                        } else if (yuanQuZhaoShangEntity.getResponse().getWordArr().get(viewflipper.getDisplayedChild()).getHrefType().equals("2")) {
//                                            Intent intent1 = new Intent(ZhengCeFaGuiActivity.this, YuanQuQiYeActivity.class);
//                                            startActivity(intent1);
//                                        } else if (yuanQuZhaoShangEntity.getResponse().getWordArr().get(viewflipper.getDisplayedChild()).getHrefType().equals("3")) {
//                                            Intent intent1 = new Intent(ZhengCeFaGuiActivity.this, ZhengCeFaGuiActivity.class);
//                                            startActivity(intent1);
//                                        } else if (yuanQuZhaoShangEntity.getResponse().getWordArr().get(viewflipper.getDisplayedChild()).getHrefType().equals("4")) {
//                                            Intent intent1 = new Intent(ZhengCeFaGuiActivity.this, ZhiNengZhiZaoActivity.class);
//                                            startActivity(intent1);
//                                        } else if (yuanQuZhaoShangEntity.getResponse().getWordArr().get(viewflipper.getDisplayedChild()).getHrefType().equals("5")) {
//                                            Intent intent1 = new Intent(ZhengCeFaGuiActivity.this, YuanQuZhaoShangActivity.class);
//                                            startActivity(intent1);
//                                        } else if (yuanQuZhaoShangEntity.getResponse().getWordArr().get(viewflipper.getDisplayedChild()).getHrefType().equals("6")) {
//                                            if (tiaoZhuanFragment != null) {
//                                                tiaoZhuanFragment.onClick();
//                                            }
//                                        } else if (yuanQuZhaoShangEntity.getResponse().getWordArr().get(viewflipper.getDisplayedChild()).getHrefType().equals("7")) {
//                                            if (fuWuTiaoZhuFragment != null) {
//                                                fuWuTiaoZhuFragment.onClick1();
//                                            }
//                                        } else if (yuanQuZhaoShangEntity.getResponse().getWordArr().get(viewflipper.getDisplayedChild()).getHrefType().equals("8")) {
//                                            Intent intent = new Intent(ZhengCeFaGuiActivity.this, HomeActivity.class);
//                                            startActivity(intent);
//                                        } else if (yuanQuZhaoShangEntity.getResponse().getWordArr().get(viewflipper.getDisplayedChild()).getHrefType().equals("9")) {
//                                            if (yuanQuZhaoShangEntity.getResponse().getWordArr().get(viewflipper.getDisplayedChild()).getContent() != null) {
//                                                Intent intent = new Intent(ZhengCeFaGuiActivity.this, WenZhangXiangQingActivity.class);
//                                                intent.putExtra("content", yuanQuZhaoShangEntity.getResponse().getWordArr().get(viewflipper.getDisplayedChild()).getContent());
//                                                startActivity(intent);
//                                            }
//                                        }
//
//                                    }


                                }
                            });


//                            //                        banner轮播
//                            ArrayList<String> list = new ArrayList<String>();
//                            for (int i = 0; i < yuanQuZhaoShangEntity.getResponse().getBannerArr().size(); i++) {
//                                YuanQuZhaoShangEntity.ResponseBean.BannerArrBean bannerArrBean = yuanQuZhaoShangEntity.getResponse().getBannerArr().get(i);
////                            list.add(ContractUtils.YUANQUZHAOSHANG_PHOTO_URL + bannerArrBean.getLogo());
//                                list.add(bannerArrBean.getImage());
//                            }
//                            banner.setBannerStyle(BannerConfig.CIRCLE_INDICATOR);
//                            banner.setImageLoader(new MyImageLoader());
//                            banner.setImages(list);
//                            banner.setDelayTime(2000);
//                            banner.setOnBannerListener(new OnBannerListener() {
//                                @Override
//                                public void OnBannerClick(int position) {
//                                    Intent intent = new Intent(ZhengCeFaGuiActivity.this, ZhengCeFaGuiXiangQingActivity.class);
//                                    intent.putExtra("id", yuanQuZhaoShangEntity.getResponse().getBannerArr().get(position).getId());
//                                    System.out.println(yuanQuZhaoShangEntity.getResponse().getBannerArr().get(position).getId() + "     点击banner详情的传送的id");
//                                    startActivity(intent);
//                                }
//                            });
//                            banner.start();


                            //        服务分类
                            zhengCeFaGuiFuWuFenLeiAdapter = new ZhengCeFaGuiFuWuFenLeiAdapter(ZhengCeFaGuiActivity.this, yuanQuZhaoShangEntity.getResponse().getCategory());
                            LinearLayoutManager manager1 = new LinearLayoutManager(ZhengCeFaGuiActivity.this);
                            recyclerViewFuwufenlei.setLayoutManager(manager1);
                            recyclerViewFuwufenlei.setAdapter(zhengCeFaGuiFuWuFenLeiAdapter);

                            zhengCeFaGuiFuWuFenLeiAdapter.setFenLeiXiangQingClick(new ZhengCeFaGuiFuWuFenLeiAdapter.FenLeiXiangQingClick() {
                                @Override
                                public void fenleixiangqingClick(String id) {
//                                    Intent intent = new Intent(ZhengCeFaGuiActivity.this, ZhengCeFaGuiFenLeiActivity.class);
                                    Intent intent = new Intent(ZhengCeFaGuiActivity.this, YuanQuZhaoShangFenLeiActivity.class);
                                    intent.putExtra("id", id);
                                    intent.putExtra("typeid", "2");
                                    intent.putExtra("zixuntypeid","4");//0 园区手册   1 智能制造   2 产业金融   3 园区资讯   4 政策法规    5企服中心    6 园区招商
                                    System.out.println(id + "     园区招商服务分类传送的id");
                                    startActivity(intent);
                                }
                            });

                        }

                    }
                });
    }


    public class MyImageLoader extends ImageLoader {

        @Override
        public void displayImage(Context context, Object path, ImageView imageView) {
            Glide.with(context).load(path).into(imageView);
        }
    }

//
//    private void initThread() {
//        new Thread() {
//            @Override
//            public void run() {
//                super.run();
//                try {
//                    while (isOk) {
//                        Thread.sleep(3000);
//                        Message message = Message.obtain();
//                        int currentItem = viewPager.getCurrentItem();
//                        if (currentItem == 2) {
//                            currentItem = 0;
//                            message.what = 1;
//                            message.arg1 = currentItem;
//                            handler.sendMessage(message);
//                        } else {
//                            message.what = 2;
//                            message.arg1 = currentItem + 1;
//                            handler.sendMessage(message);
//                        }
//                    }
//                } catch (InterruptedException e) {
//                    e.printStackTrace();
//                }
//            }
//        }.start();
//    }


    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {

    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }


    private void initView() {
//        zhengCeFaGuiHuaDongAdapter = new ZhengCeFaGuiHuaDongAdapter(this);
//        LinearLayoutManager manager = new LinearLayoutManager(this);
//        //设置为横向滑动
//        manager.setOrientation(LinearLayoutManager.HORIZONTAL);
//        recyclerView.setLayoutManager(manager);
//        recyclerView.setAdapter(zhengCeFaGuiHuaDongAdapter);


////        服务分类
//        zhengCeFaGuiFuWuFenLeiAdapter = new ZhengCeFaGuiFuWuFenLeiAdapter(this);
//        LinearLayoutManager manager1 = new LinearLayoutManager(this);
//        recyclerViewFuwufenlei.setLayoutManager(manager1);
//        recyclerViewFuwufenlei.setAdapter(zhengCeFaGuiFuWuFenLeiAdapter);

    }


    @OnClick({R.id.image_back, R.id.recyclerView, R.id.tv_fankui, R.id.relative_fankui})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.image_back:
                finish();
                break;
            case R.id.recyclerView:
                break;
            case R.id.tv_fankui:
                Intent intent = new Intent(ZhengCeFaGuiActivity.this, ZhengCeFaGuiYiJianFanKuiActivity.class);
                startActivity(intent);
                break;
            case R.id.relative_fankui:
                Intent intent1 = new Intent(ZhengCeFaGuiActivity.this, ZhengCeFaGuiYiJianFanKuiActivity.class);
                startActivity(intent1);
                break;
        }
    }
}
