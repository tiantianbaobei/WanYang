package com.wanyangproject.shouye;

import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.androidkun.xtablayout.XTabLayout;
import com.bumptech.glide.Glide;
import com.google.gson.Gson;
import com.wanyangproject.R;
import com.wanyangproject.adapter.ShangJiaPingLunAdapter;
import com.wanyangproject.adapter.WaiMaiAdapter;
import com.wanyangproject.entity.JieSuanEntity;
import com.wanyangproject.entity.NumberEntity;
import com.wanyangproject.entity.ShangJiaPingFenEntity;
import com.wanyangproject.entity.ShangJiaShangPinEntity;
import com.wanyangproject.entity.ShouCangEntity;
import com.wanyangproject.entity.ShouCangShangJiaXianShiEntity;
import com.wanyangproject.fragment.PingLunFragment;
import com.wanyangproject.fragment.ShangJiaFragment;
import com.wanyangproject.fragment.ShangPinFragment;
import com.wanyangproject.myadapter.MyShangJiaAdapter;
import com.wanyangproject.popuwindow.SharePopupWindow;
import com.wanyangproject.utils.ContractUtils;
import com.zhy.http.okhttp.OkHttpUtils;
import com.zhy.http.okhttp.callback.StringCallback;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.Call;

//  园区生活
public class WaiMaiActivity extends AppCompatActivity {

    @BindView(R.id.image_back)
    ImageView imageBack;
    //    @BindView(R.id.act_recyclerView)
//    RecyclerView act_recyclerView;
    @BindView(R.id.relative_jiesuan)
    RelativeLayout relative_jiesuan;
    @BindView(R.id.btn_jiesuan)
    Button btnJiesuan;
    @BindView(R.id.image_shop)
    ImageView imageShop;
    @BindView(R.id.tv_shop_name)
    TextView tvShopName;
    @BindView(R.id.tv_shoucang)
    TextView tvShoucang;
    @BindView(R.id.tv_qisongjia)
    TextView tvQisongjia;
    @BindView(R.id.tv_peisongfei)
    TextView tvPeisongfei;
    @BindView(R.id.tv_yingye_shijian)
    TextView tvYingyeShijian;
    @BindView(R.id.tv_gouwuche_number)
    TextView tvGouwucheNumber;
    @BindView(R.id.tv_zongjine)
    TextView tvZongjine;
    @BindView(R.id.tv_qisong_money)
    TextView tvQisongMoney;
    @BindView(R.id.tv_peisong_money)
    TextView tvPeisongMoney;
    @BindView(R.id.image_shoucang)
    ImageView imageShoucang;
    @BindView(R.id.tabLayout)
    XTabLayout tabLayout;
    @BindView(R.id.viewPager)
    ViewPager viewPager;
    @BindView(R.id.image_share)
    ImageView imageShare;
    @BindView(R.id.image_beijing)
    ImageView imageBeijing;
    private String token;
    private ShangJiaShangPinEntity shangJiaShangPinEntity;
    private NetWork netWork;
    private String number;
    private NumberEntity numberEntity;
    private ArrayList<NumberEntity> list = new ArrayList<>();
    private String shangjiaId;
    float jiage = 0;
    private SharePopupWindow sharePopupWindow;
    private ShangJiaPingFenEntity shangJiaPingFenEntity;
    private String finish;
    private String bidian;




    public ArrayList<NumberEntity> getList() {
        return list;
    }

    public void setList(ArrayList<NumberEntity> list) {
        this.list = list;
    }



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wai_mai_sssssss);
        ButterKnife.bind(this);
        if (Build.VERSION.SDK_INT >= 21) {
            View dview = getWindow().getDecorView();
            dview.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN | View.SYSTEM_UI_FLAG_LAYOUT_STABLE);
            getWindow().setStatusBarColor(Color.TRANSPARENT);
        }

        SharedPreferences sharedPreferences = getSharedPreferences("token", Context.MODE_PRIVATE);
        token = sharedPreferences.getString("token", "token");
        System.out.println(token + "     token");

        NumberEntity.lists = list;
        //            下面复制
        initFuZhi();

        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("11");
        netWork = new NetWork();
        registerReceiver(netWork, intentFilter);

        Intent intent = getIntent();
        shangjiaId = intent.getStringExtra("id");
        System.out.println(shangjiaId + "   接收商家推荐的商家id");


        bidian = intent.getStringExtra("bidian");
        System.out.println(bidian+"       闭店");


        IntentFilter intentFilter4 = new IntentFilter();
        intentFilter4.addAction("finish");
        netWork = new NetWork();
        registerReceiver(netWork, intentFilter4);




        initViewPager();

        //        商家商品的网络请求
        initShangJiaShangPinHttp();



        //        商家评分的网络请求
        initShangJiaPingFenHttp();








        //        收藏商家的显示的网络请求
        initShouCangShangJiaXianShiHttp();




    }












    //    收藏商家的显示的网络请求
    private void initShouCangShangJiaXianShiHttp() {
        OkHttpUtils.post()
                .url(ContractUtils.LOGIN_URL+"user/collectionlist")
                .addHeader("token",ContractUtils.getTOKEN(WaiMaiActivity.this))
                .addParams("parkId",ContractUtils.getParkId(WaiMaiActivity.this))
                .addParams("type","1")//1：用户收藏商家
                .build()
                .execute(new StringCallback() {
                    @Override
                    public void onError(Call call, Exception e, int id) {
                        System.out.println(e+"      eeee用户收藏商家显示");
                    }

                    @Override
                    public void onResponse(String response, int id) {
                        ContractUtils.Code500(WaiMaiActivity.this,response);
                        System.out.println(response+"          用户收藏商家显示网络请求");

                        if(response.indexOf("200") != -1) {
                            Gson gson = new Gson();
                            ShouCangShangJiaXianShiEntity shouCangShangJiaXianShiEntity = gson.fromJson(response, ShouCangShangJiaXianShiEntity.class);

                            Boolean isOK = false;
                            for (int i = 0; i < shouCangShangJiaXianShiEntity.getResponse().size(); i++) {
                                ShouCangShangJiaXianShiEntity.ResponseBean responseBean = shouCangShangJiaXianShiEntity.getResponse().get(i);
                                if(responseBean.getId().equals(shangjiaId)){
                                    isOK = true;
                                }
                            }

                              if(isOK == true){
                                 imageShoucang.setImageResource(R.drawable.quxia_shoucang);
                                }




//                            try {
//                                JSONObject jsonObject = new JSONObject(response);
//                                JSONArray response1 = jsonObject.getJSONArray("response");
//
//                                Boolean isOK = false;
//                                if(response1 != null){
//                                    for (int i = 0; i < response1.length(); i++) {
//                                        JSONObject json = (JSONObject) response1.get(i);
//                                        if(json.getString("0").equals(shangjiaId)){
//                                            isOK = true;
//                                        }
//                                    }
//
//                                    if(isOK == true){
//                                        imageShoucang.setImageResource(R.drawable.quxia_shoucang);
//                                    }
//
//                                }
//
//                            } catch (JSONException e) {
//                                e.printStackTrace();
//                            }



                        }
                    }
                });
    }










    @Override
    protected void onResume() {
        super.onResume();
        //            下面复制
        initFuZhi();




    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(netWork);
    }

    class NetWork extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {

            //            结束页面
            finish = intent.getStringExtra("finish");
            if(finish != null) {
                NumberEntity.lists.clear();
                initFuZhi();
                return;
            }


            System.out.println("eeeeeeeeeeeeeeeeeeeeeeeeeeeeee");

            number = intent.getStringExtra("number");// 数量
            String id = intent.getStringExtra("id");// id
            String money = intent.getStringExtra("money"); // 价格
            String name = intent.getStringExtra("name");//商品名称
            int num = Integer.parseInt(number);//数量

            System.out.println(number + "   数量");
            System.out.println(id + "   id");
            System.out.println(money + "   价格");

            int tt = -1;
            for (int i = 0; i < list.size(); i++) {
                NumberEntity kk = list.get(i);
                if (kk.getId().equals(id)) {
                    tt = i;
                }
            }
            if (tt == -1) {
                NumberEntity numberEnti = new NumberEntity();
                numberEnti.setId(id);
                numberEnti.setNumber(number + "");
                numberEnti.setPic(money);
                numberEnti.setName(name);
                System.out.println(numberEnti.getNumber() + "        hahahahaha");
                list.add(numberEnti);
            } else {
                NumberEntity kk = list.get(tt);
                kk.setNumber(number + "");
                System.out.println(number + "   number  ");
            }

            NumberEntity.lists = list;

//            下面复制
            initFuZhi();

        }
    }


    private void initFuZhi() {
        Integer shumu = 0;
        float jiage = 0;
        for (int i = 0; i < list.size(); i++) {
            NumberEntity nn = list.get(i);
            System.out.println(nn.getNumber() + "          ccccccccccccc");
            shumu += Integer.parseInt(nn.getNumber());
            jiage += Integer.parseInt(nn.getNumber()) * Float.parseFloat(nn.getPic());
            System.out.println(Integer.parseInt(nn.getNumber()) + "          00000000000000000000");
        }


        if (shumu <= 0) {
            tvGouwucheNumber.setVisibility(View.GONE);
//            float price=1.2;
            DecimalFormat decimalFormat=new DecimalFormat("0.00");//构造方法的字符格式这里如果小数不足2位,会以0补足.
            String jiage1 = decimalFormat.format(jiage);//format 返回的是字符串

//            float  jiage1   =  (float)(Math.round(jiage*100))/100;//(这里的100就是2位小数点,如果要其它位,如4位,这里两个100改成10000)
            tvZongjine.setText("¥ " + jiage1 + "");
            btnJiesuan.setText("去结算(" + shumu + ")");
        } else {
            tvGouwucheNumber.setVisibility(View.VISIBLE);
            tvGouwucheNumber.setText(shumu + "");
            DecimalFormat decimalFormat=new DecimalFormat("0.00");//构造方法的字符格式这里如果小数不足2位,会以0补足.
            String jiage1 = decimalFormat.format(jiage);//format 返回的是字符串

//            float  jiage1   =  (float)(Math.round(jiage*100))/100;//(这里的100就是2位小数点,如果要其它位,如4位,这里两个100改成10000)
            tvZongjine.setText("¥ " + jiage1 + "");
            btnJiesuan.setText("去结算(" + shumu + ")");
        }
    }


    //       商家商品的网络请求
//    tiantian
//    .addParams("parkId", ContractUtils.getParkId(WaiMaiActivity.this))
    private void initShangJiaShangPinHttp() {
        final ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setTitle("提示");
        progressDialog.setMessage("请等待...");
        progressDialog.setCancelable(false);
        progressDialog.show();
        OkHttpUtils.post()
                .url(ContractUtils.LOGIN_URL + "Life/shop")
                .addHeader("token", ContractUtils.getTOKEN(WaiMaiActivity.this))
                .addParams("parkId", ContractUtils.getParkId(WaiMaiActivity.this))
                .addParams("shopId", shangjiaId) // 暂时 ，应该商家id tiantian
                .build()
                .execute(new StringCallback() {
                    @Override
                    public void onError(Call call, Exception e, int id) {
                        progressDialog.dismiss();
                    }

                    @Override
                    public void onResponse(String response, int id) {
                        progressDialog.dismiss();
                        ContractUtils.Code500(WaiMaiActivity.this, response);
                        System.out.println(response + "    商品商家信息的网络请求");

                        if (response.indexOf("200") != -1) {
                            Gson gson = new Gson();
                            shangJiaShangPinEntity = gson.fromJson(response, ShangJiaShangPinEntity.class);

                            if (shangJiaShangPinEntity.getResponse().getShop().getShop().getCost().equals("")) {
                                shangJiaShangPinEntity.getResponse().getShop().getShop().setCost("0");
                            }
//                            店名
                            if (shangJiaShangPinEntity.getResponse().getShop().getShop().getNickname() == null) {

                            } else {
                                tvShopName.setText(shangJiaShangPinEntity.getResponse().getShop().getShop().getNickname());
                            }

//                            店家头像
                            if(ContractUtils.PHOTO_URL + shangJiaShangPinEntity.getResponse().getShop().getShop().getPhoto() != null){
                                if(shangJiaShangPinEntity.getResponse().getShop().getShop().getPhoto().equals("park.hostop.net/")){
                                    Glide.with(WaiMaiActivity.this).load(R.drawable.shangdian).into(imageShop);
                                }else{
                                    Glide.with(WaiMaiActivity.this).load(ContractUtils.PHOTO_URL + shangJiaShangPinEntity.getResponse().getShop().getShop().getPhoto()).into(imageShop);
                                }
                            }


//                            if (ContractUtils.PHOTO_URL + shangJiaShangPinEntity.getResponse().getShop().getShop().getPhoto() == null) {
//
//                            } else {
//                                Glide.with(WaiMaiActivity.this).load(ContractUtils.PHOTO_URL + shangJiaShangPinEntity.getResponse().getShop().getShop().getPhoto()).into(imageShop);
//                            }

//                        收藏
                            if (shangJiaShangPinEntity.getResponse().getShop().getShop().getCollection() == null) {

                            } else {
                                tvShoucang.setText(shangJiaShangPinEntity.getResponse().getShop().getShop().getCollection() + "收藏");
                            }

//                        起送价
                            if (shangJiaShangPinEntity.getResponse().getShop().getShop().getStarting() == null) {

                            } else {
                                tvQisongjia.setText("起送价¥" + shangJiaShangPinEntity.getResponse().getShop().getShop().getStarting());
                            }

//                        配送费
                            if (shangJiaShangPinEntity.getResponse().getShop().getShop().getCost() == null) {

                            } else {
                                tvPeisongfei.setText("配送费¥" + shangJiaShangPinEntity.getResponse().getShop().getShop().getCost());
                            }

//                        营业时间
                            if (shangJiaShangPinEntity.getResponse().getShop().getShop().getStar_time() == null &&
                                    shangJiaShangPinEntity.getResponse().getShop().getShop().getEnd_time() == null) {

                            } else {
                                tvYingyeShijian.setText("营业时间：" + shangJiaShangPinEntity.getResponse().getShop().getShop().getStar_time() + "-" + shangJiaShangPinEntity.getResponse().getShop().getShop().getEnd_time());
                            }

//                        头像
                            if (ContractUtils.PHOTO_URL + shangJiaShangPinEntity.getResponse().getShop().getShop().getPhoto() == null) {

                            } else {
                                Glide.with(WaiMaiActivity.this).load(ContractUtils.PHOTO_URL + shangJiaShangPinEntity.getResponse().getShop().getShop().getPhoto()).into(imageShop);
                            }


//                        最下面起送价
                            if (shangJiaShangPinEntity.getResponse().getShop().getShop().getStarting() == null) {

                            } else {
                                tvQisongMoney.setText(" ¥ " + shangJiaShangPinEntity.getResponse().getShop().getShop().getStarting());
                            }

//                        最下面配送费
                            if (shangJiaShangPinEntity.getResponse().getShop().getShop().getCost() == null) {

                            } else {
                                tvPeisongMoney.setText(" ¥ " + shangJiaShangPinEntity.getResponse().getShop().getShop().getCost());
                            }



                            if(ContractUtils.PHOTO_URL+shangJiaShangPinEntity.getResponse().getShop().getShop().getShophoto() == null){

                            }else{
                                Glide.with(WaiMaiActivity.this).load(ContractUtils.PHOTO_URL+shangJiaShangPinEntity.getResponse().getShop().getShop().getShophoto()).into(imageBeijing);
                            }
                        }
                    }
                });
    }


//    private void initViewPager() {
//        List<Fragment> fragments = new ArrayList<>();
//        fragments.add(new ShangPinFragment());
//        fragments.add(new PingLunFragment());
//        fragments.add(new ShangJiaYuanQuShouCeFragment());
//        // 创建ViewPager适配器
//        WaiMaiAdapter waiMaiAdapter = new WaiMaiAdapter(getSupportFragmentManager());
//        waiMaiAdapter.setFragments(fragments);
//        // 给ViewPager设置适配器
//        viewPager.setAdapter(waiMaiAdapter);
//        // TabLayout 指示器 (记得自己手动创建4个Fragment,注意是 app包下的Fragment 还是 V4包下的 Fragment)
//        tabLayout.addTab(tabLayout.newTab());
//        tabLayout.addTab(tabLayout.newTab());
//        tabLayout.addTab(tabLayout.newTab());
//        // 使用 TabLayout 和 ViewPager 相关联
//        tabLayout.setupWithViewPager(viewPager);
//        // TabLayout指示器添加文本
//        tabLayout.getTabAt(0).setText("商品");
//        tabLayout.getTabAt(1).setText("评论");
//        tabLayout.getTabAt(2).setText("商家");
//    }

//    @OnClick({R.id.image_back, R.id.tabLayout, R.id.viewPager})
//    public void onViewClicked(View view) {
//        switch (view.getId()) {
//            case R.id.image_back:
//                finish();
//                break;
//            case R.id.tabLayout:
//                break;
//            case R.id.viewPager:
//                break;
//        }
//    }








    @OnClick({R.id.image_back, R.id.btn_jiesuan, R.id.image_shoucang, R.id.image_share, R.id.image_shop, R.id.tv_shop_name, R.id.tv_shoucang, R.id.tv_qisongjia, R.id.tv_peisongfei, R.id.tv_yingye_shijian})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.image_back:
                finish();
                break;
//            收藏
            case R.id.image_shoucang:
//                收藏商家的网络请求
                initShouCangShangJiaHttp();
                break;
//            分享
            case R.id.image_share:
                Object itemsOnClick = null;
                Bitmap bitmap = BitmapFactory.decodeResource(getResources(), R.drawable.loginbaibian);
                sharePopupWindow = new SharePopupWindow(WaiMaiActivity.this, (View.OnClickListener) itemsOnClick, shangJiaShangPinEntity.getResponse().getShop().getShop().getNickname(), ContractUtils.YUANQUZHAOSHANG_PHOTO_URL + "index/index/merchantdetails?id=" + shangjiaId, bitmap);
                sharePopupWindow.showAtLocation(findViewById(R.id.relative_Layout), Gravity.BOTTOM | Gravity.CENTER_HORIZONTAL, 0, 0);
                break;
            case R.id.btn_jiesuan:
//                //                    结算添加订单网络请求   结算
//                initJieSuanHttp();

                Integer shumu = 0;
                float jiage = 0;
                float qisongjia = 0;
                for (int i = 0; i < list.size(); i++) {
                    NumberEntity nn = list.get(i);
                    System.out.println(nn.getNumber() + "          ccccccccccccc");
                    shumu += Integer.parseInt(nn.getNumber());
                    jiage += Integer.parseInt(nn.getNumber()) * Float.parseFloat(nn.getPic());
                }
                qisongjia = Float.parseFloat(shangJiaShangPinEntity.getResponse().getShop().getShop().getStarting());

                if (jiage < qisongjia) {
                    Toast.makeText(this, "不满足配送条件", Toast.LENGTH_SHORT).show();
                } else if (shumu == 0) {
                    Toast.makeText(this, "请先选择商品", Toast.LENGTH_SHORT).show();
                } else {


                    List<Map<String, String>> list_post = new ArrayList();
                    Gson gson = new Gson();
                    for (int i = 0; i < list.size(); i++) {
                        HashMap map = new HashMap();

                        NumberEntity nn = list.get(i);
                        System.out.println(nn.getNumber() + "          ccccccccccccc");
                        map.put("id", nn.getId());
                        map.put("num", nn.getNumber());
                        list_post.add(map);
                    }
                    String jsonString = gson.toJson(list_post);
                    System.out.println(jsonString + "                    hhhhhhhhhhhhhhh");


//                    1 营业    2 休息
                    if(bidian != null){
                        if(bidian.equals("1")){
                            Intent intent = new Intent(WaiMaiActivity.this, DingDanXiangQingActivity.class);
                            intent.putExtra("name",shangJiaShangPinEntity.getResponse().getShop().getShop().getNickname());
                            intent.putExtra("peisongfei", shangJiaShangPinEntity.getResponse().getShop().getShop().getCost());
                            System.out.println(shangJiaShangPinEntity.getResponse().getShop().getShop().getCost() + "   点击结算传送的配送费");
                            intent.putExtra("zongjiage", jiage + "");
                            System.out.println(jiage + "    总价格");
                            intent.putExtra("shopid", shangjiaId);
                            System.out.println(shangjiaId + "     传送的商家id");
                            intent.putExtra("json",jsonString);
                            System.out.println(jsonString+"        jsonString");
                            startActivity(intent);
                        }else if(bidian.equals("2")){
                            Toast.makeText(this, "商家已闭店！", Toast.LENGTH_SHORT).show();
                        }
                    }

//                    Intent intent = new Intent(WaiMaiActivity.this, DingDanXiangQingActivity.class);
//                    intent.putExtra("name",shangJiaShangPinEntity.getResponse().getShop().getShop().getNickname());
//                    intent.putExtra("peisongfei", shangJiaShangPinEntity.getResponse().getShop().getShop().getCost());
//                    System.out.println(shangJiaShangPinEntity.getResponse().getShop().getShop().getCost() + "   点击结算传送的配送费");
//                    intent.putExtra("zongjiage", jiage + "");
//                    System.out.println(jiage + "    总价格");
//                    intent.putExtra("shopid", shangjiaId);
//                    System.out.println(shangjiaId + "     传送的商家id");
//                    intent.putExtra("json",jsonString);
//                    System.out.println(jsonString+"        jsonString");
//                    startActivity(intent);







                    //       结算添加订单网络请求   结算
//                    initJieSuanHttp();
                }
                break;
            case R.id.image_shop:
                break;
            case R.id.tv_shop_name:
                break;
            case R.id.tv_shoucang:
                break;
            case R.id.tv_qisongjia:
                break;
            case R.id.tv_peisongfei:
                break;
            case R.id.tv_yingye_shijian:
                break;
        }
    }


//    分享的网络请求
//    private void initFenXiangHttp() {


//        OkHttpUtils.post()
//                .url(ContractUtils.LOGIN_URL+"index/index/merchantdetails")
//                .addHeader("token",ContractUtils.getTOKEN(WaiMaiActivity.this))
//                .addParams("id",shangjiaId)
//                .build()
//                .execute(new StringCallback() {
//                    @Override
//                    public void onError(Call call, Exception e, int id) {
//
//                    }
//
//                    @Override
//                    public void onResponse(String response, int id) {
//                        ContractUtils.Code500(WaiMaiActivity.this,response);
//                        System.out.println(response+"         外卖分享的网络请求");
//                        if(response.indexOf("200") != -1){
//                            Gson gson = new Gson();
//                            ZhaoPinFenXiangEntity zhaoPinFenXiangEntity = gson.fromJson(response, ZhaoPinFenXiangEntity.class);
//                            Object itemsOnClick = null;
//                            sharePopupWindow = new SharePopupWindow(WaiMaiActivity.this,(View.OnClickListener) itemsOnClick,));
//                            sharePopupWindow.showAtLocation(findViewById(R.id.relative_Layout), Gravity.BOTTOM | Gravity.CENTER_HORIZONTAL, 0, 0);
////                            ChuanZhiEntity.fenxiang = zhaoPinFenXiangEntity.getResponse();//传值
//                        }else if(response.indexOf("400") != -1){
//                            Gson gson = new Gson();
//                            FenXiangShiBaiEntity fenXiangShiBaiEntity = gson.fromJson(response, FenXiangShiBaiEntity.class);
//                            Toast.makeText(WaiMaiActivity.this, fenXiangShiBaiEntity.getMsg(), Toast.LENGTH_SHORT).show();
//                        }
//                    }
//                });
//    }

    //    收藏商家的网络请求
    private void initShouCangShangJiaHttp() {
        System.out.println(ContractUtils.getTOKEN(WaiMaiActivity.this) + "        token");
        System.out.println(ContractUtils.getParkId(WaiMaiActivity.this) + "        parkId ");
        System.out.println("1" + "        type ");
        System.out.println(shangjiaId + "        id ");
        OkHttpUtils.post()
                .url(ContractUtils.LOGIN_URL + "user/collection")//shop/evaluatelist
                .addHeader("token", ContractUtils.getTOKEN(WaiMaiActivity.this))
                .addParams("parkId", ContractUtils.getParkId(WaiMaiActivity.this))
                .addParams("type", shangjiaId)//1:收藏商家
                .addParams("id", shangjiaId)
                .build()
                .execute(new StringCallback() {
                    @Override
                    public void onError(Call call, Exception e, int id) {
                        System.out.println(e + "   shangjia eeee");
                    }

                    @Override
                    public void onResponse(String response, int id) {
                        ContractUtils.Code500(WaiMaiActivity.this, response);
                        System.out.println(response + "        收藏商家的网络请求");


                        if (response.indexOf("200") != -1) {
                            Gson gson = new Gson();
                            ShouCangEntity shouCangEntity = gson.fromJson(response, ShouCangEntity.class);
                            Toast.makeText(WaiMaiActivity.this, shouCangEntity.getResponse().getMes(), Toast.LENGTH_SHORT).show();

                            if (shouCangEntity.getResponse().getMes().equals("收藏成功！")) {
                                imageShoucang.setImageResource(R.drawable.quxia_shoucang);
                                //        商家商品的网络请求
                                initShangJiaShangPinHttp();


                            } else if (shouCangEntity.getResponse().getMes().equals("取消收藏成功！")) {
                                imageShoucang.setImageResource(R.drawable.shoucang);
                                //        商家商品的网络请求
                                initShangJiaShangPinHttp();


                            }
                        }

//                        else{
//                            Gson gson = new Gson();
//                            ShouCangShangJiaShiBaiEntity shouCangShangJiaShiBaiEntity = gson.fromJson(response, ShouCangShangJiaShiBaiEntity.class);
//                            Toast.makeText(WaiMaiActivity.this, shouCangShangJiaShiBaiEntity.getMsg(), Toast.LENGTH_SHORT).show();
//                        }
                    }
                });
    }


//    //    结算网络请求
////    tiantian
//    private void initJieSuanHttp() {
//        jiage = 0;
//        List<Map<String, String>> list_post = new ArrayList();
//        Gson gson = new Gson();
//
//        Integer shumu = 0;
////        float jiage = 0;
//        for (int i = 0; i < list.size(); i++) {
//            HashMap map = new HashMap();
//
//            NumberEntity nn = list.get(i);
//            System.out.println(nn.getNumber() + "          ccccccccccccc");
//            shumu += Integer.parseInt(nn.getNumber()); //数量
//            jiage += Integer.parseInt(nn.getNumber()) * Float.parseFloat(nn.getPic());// 价格
//            map.put("id", nn.getId());
//            map.put("num", nn.getNumber());
//            list_post.add(map);
//        }
//        String jsonString = gson.toJson(list_post);
//        System.out.println(jsonString + "                    hhhhhhhhhhhhhhh");

////        final float peisongfei = Float.parseFloat(shangJiaShangPinEntity.getResponse().getShop().getShop().getCost()); // 配送费
////        final float zongji = jiage + peisongfei;// 总计的钱
//
//
//        System.out.println(jiage + "         价格价格价格价格价格");
//        System.out.println(System.currentTimeMillis() + "     时间一");
//        final ProgressDialog progressDialog = new ProgressDialog(WaiMaiActivity.this);
//        progressDialog.setTitle("提示");
//        progressDialog.setMessage("请等待...");
//        progressDialog.setCancelable(false);
//        progressDialog.show();
//        OkHttpUtils.post()
//                .url(ContractUtils.LOGIN_URL + "Life/add_order")
//                .addHeader("token", ContractUtils.getTOKEN(WaiMaiActivity.this))
//                .addParams("goods", jsonString)
//                .addParams("money", jiage + "")   //    .addParams("money", jiage+"")
//                .addParams("parkId", ContractUtils.getParkId(WaiMaiActivity.this))
//                .addParams("shopId", shangjiaId)  // 暂时  tiantian   商家id
//                .build()
//                .execute(new StringCallback() {
//                    @Override
//                    public void onError(Call call, Exception e, int id) {
//                        progressDialog.dismiss();
//                    }
//
//                    @Override
//                    public void onResponse(String response, int id) {
//                        ContractUtils.Code500(WaiMaiActivity.this, response);
//                        System.out.println(jiage + "      jiajiajiajiajiajia");
//                        progressDialog.dismiss();
//                        System.out.println(response + "          结算网络请求");
//                        System.out.println(System.currentTimeMillis() + "         时间二");
//                        ContractUtils.Code500(WaiMaiActivity.this, response);
//                        if (response.indexOf("200") != -1) {
//                            Gson gson = new Gson();
//                            JieSuanEntity jieSuanEntity = gson.fromJson(response, JieSuanEntity.class);
//                            if (shangJiaShangPinEntity.getResponse().getShop().getShop().getCost().equals("")) {
//                                shangJiaShangPinEntity.getResponse().getShop().getShop().setCost("0");
//                            }
//
//                            Intent intent = new Intent(WaiMaiActivity.this, DingDanXiangQingActivity.class);
//                            intent.putExtra("peisongfei", shangJiaShangPinEntity.getResponse().getShop().getShop().getCost());
//                            System.out.println(shangJiaShangPinEntity.getResponse().getShop().getShop().getCost() + "   点击结算传送的配送费");
//                            intent.putExtra("zongjiage", jiage + "");
//                            System.out.println(jiage + "    总价格");
//                            intent.putExtra("dingdanhao", jieSuanEntity.getResponse().getOrder_sn());
//                            System.out.println(jieSuanEntity.getResponse().getOrder_sn() + "    点击结算传送的订单号");
//                            intent.putExtra("shopid", shangjiaId);
//                            System.out.println(shangjiaId + "     传送的商家id");
//                            startActivity(intent);
////                            finish();
//                        } else if(response.indexOf("400") != -1){
//                            ContractUtils.Code400(WaiMaiActivity.this,response);
//                        }
//                    }
//                });
//    }
//
//
//












    //    商家评分的网络请求
    private void initShangJiaPingFenHttp() {
        OkHttpUtils.post()
                .url(ContractUtils.LOGIN_URL + "shop/evaluatelist")
                .addHeader("token", ContractUtils.getTOKEN((WaiMaiActivity.this)))
                .addParams("parkId", ContractUtils.getParkId((WaiMaiActivity.this)))
                .addParams("shopId", shangjiaId)
                .build()
                .execute(new StringCallback() {
                    @Override
                    public void onError(Call call, Exception e, int id) {
                        System.out.println(e + "      商家评分eee");
                    }

                    @Override
                    public void onResponse(String response, int id) {
                        System.out.println(response + "     商家评分的网络请求");
                        ContractUtils.Code500shibai(WaiMaiActivity.this,response);
                        if (response.indexOf("200") != -1) {
                            Gson gson = new Gson();
                            shangJiaPingFenEntity = gson.fromJson(response, ShangJiaPingFenEntity.class);
                            tabLayout.getTabAt(1).setText("评论("+shangJiaPingFenEntity.getResponse().getList().size()+")");

//                            float ping = 0;
//                            if(shangJiaPingFenEntity.getResponse().getPing() != null){
//                                ping = Float.parseFloat(shangJiaPingFenEntity.getResponse().getPing());
//                            }
                        }


                    }
//                    }
                });
    }













    private void initViewPager() {
        List<Fragment> fragments = new ArrayList<>();
        ShangPinFragment shangPinFragment = new ShangPinFragment();
        PingLunFragment pingLunFragment = new PingLunFragment();
        ShangJiaFragment shangJiaFragment = new ShangJiaFragment();
        shangPinFragment.setShopid(shangjiaId);
        shangPinFragment.setBidian(bidian);
        shangJiaFragment.setShopid(shangjiaId);
        shangPinFragment.setWaimai(WaiMaiActivity.this);
        pingLunFragment.setShopid(shangjiaId);
        fragments.add(shangPinFragment);
        fragments.add(pingLunFragment);
        fragments.add(shangJiaFragment);


        // 创建ViewPager适配器
        WaiMaiAdapter waiMaiAdapter = new WaiMaiAdapter(getSupportFragmentManager());
        waiMaiAdapter.setFragments(fragments);
        // 给ViewPager设置适配器
        viewPager.setAdapter(waiMaiAdapter);
        // TabLayout 指示器 (记得自己手动创建4个Fragment,注意是 app包下的Fragment 还是 V4包下的 Fragment)
        tabLayout.addTab(tabLayout.newTab());
        tabLayout.addTab(tabLayout.newTab());
        tabLayout.addTab(tabLayout.newTab());
        // 使用 TabLayout 和 ViewPager 相关联
        tabLayout.setupWithViewPager(viewPager);
        // TabLayout指示器添加文本
        tabLayout.getTabAt(0).setText("商品");
//        tabLayout.getTabAt(1).setText("评论");
        tabLayout.getTabAt(1).setText("评论(0)");
        tabLayout.getTabAt(2).setText("商家");

        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                if (position == 0) {
                    relative_jiesuan.setVisibility(View.VISIBLE);
                } else if (position == 1) {
                    relative_jiesuan.setVisibility(View.GONE);
                } else {
                    relative_jiesuan.setVisibility(View.GONE);
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }


//    class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerViewAdapter.ViewHolder> {
//
//        @Override
//        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
//            View view = LayoutInflater.from(WaiMaiActivity.this).inflate(R.layout.recyclerview_item111, parent, false);
//            ViewHolder viewHolder = new ViewHolder(view);
//            return viewHolder;
//        }
//
//        @Override
//        public void onBindViewHolder(ViewHolder holder, int position) {
//            List<Fragment> fragments = new ArrayList<>();
//            ShangPinFragment shangPinFragment = new ShangPinFragment();
//            shangPinFragment.setShopid(shangjiaId);
//            shangPinFragment.setWaimai(WaiMaiActivity.this);
//            fragments.add(shangPinFragment);
//            fragments.add(new PingLunFragment());
//            fragments.add(new ShangJiaFragment());
//
//
//            // 创建ViewPager适配器
//            WaiMaiAdapter waiMaiAdapter = new WaiMaiAdapter(getSupportFragmentManager());
//            waiMaiAdapter.setFragments(fragments);
//            // 给ViewPager设置适配器
//            holder.viewPager.setAdapter(waiMaiAdapter);
//            // TabLayout 指示器 (记得自己手动创建4个Fragment,注意是 app包下的Fragment 还是 V4包下的 Fragment)
//            holder.tabLayout.addTab(holder.tabLayout.newTab());
//            holder.tabLayout.addTab(holder.tabLayout.newTab());
//            holder.tabLayout.addTab(holder.tabLayout.newTab());
//            // 使用 TabLayout 和 ViewPager 相关联
//            holder.tabLayout.setupWithViewPager(holder.viewPager);
//            // TabLayout指示器添加文本
//            holder.tabLayout.getTabAt(0).setText("商品");
//            holder.tabLayout.getTabAt(1).setText("评论");
//            holder.tabLayout.getTabAt(2).setText("商家");
//
//            holder.viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
//                @Override
//                public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
//
//                }
//
//                @Override
//                public void onPageSelected(int position) {
//                    if (position == 0) {
//                        relative_jiesuan.setVisibility(View.VISIBLE);
//                    } else if (position == 1) {
//                        relative_jiesuan.setVisibility(View.GONE);
//                    } else {
//                        relative_jiesuan.setVisibility(View.GONE);
//                    }
//                }
//
//                @Override
//                public void onPageScrollStateChanged(int state) {
//
//                }
//            });
//        }
//
//        @Override
//        public int getItemCount() {
//            return 1;
//        }
//
//        public class ViewHolder extends RecyclerView.ViewHolder {
//            //            private TabLayout tabLayout;
//            private XTabLayout tabLayout;
//            private ViewPager viewPager;
//
//            public ViewHolder(View itemView) {
//                super(itemView);
//                tabLayout = itemView.findViewById(R.id.tabLayout);
//                viewPager = itemView.findViewById(R.id.viewPager);
//            }
//        }
//    }


}
