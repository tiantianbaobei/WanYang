package com.wanyangproject.shouye;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.wanyangproject.R;
import com.wanyangproject.entity.YouHuiQuanShiYongEntity;
import com.wanyangproject.utils.ContractUtils;
import com.zhy.http.okhttp.OkHttpUtils;
import com.zhy.http.okhttp.callback.StringCallback;

import java.text.SimpleDateFormat;
import java.util.Date;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.Call;

public class YouHuiQuanXinXiActivity extends AppCompatActivity {

    @BindView(R.id.image_back)
    ImageView imageBack;
    @BindView(R.id.tv_title)
    TextView tvTitle;
    @BindView(R.id.tv_money)
    TextView tvMoney;
    @BindView(R.id.tv_time)
    TextView tvTime;
    @BindView(R.id.btn_queren)
    Button btnQueren;
    private String name;
    private String money;
    private String time;
    private String code;
    private String yongjiu;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_you_hui_quan_xin_xi);
        ButterKnife.bind(this);


        Intent intent = getIntent();
        name = intent.getStringExtra("name");
        money = intent.getStringExtra("money");
        time = intent.getStringExtra("time");
        code = intent.getStringExtra("code");
        yongjiu = intent.getStringExtra("yongjiu");
        System.out.println(time+"         time");


        if (name.equals("")) {

        } else {
            tvTitle.setText(name);
        }


        if (money.equals("")) {

        } else {
            tvMoney.setText(money);
        }


//        if (yongjiu.equals("")) {
//
//        } else {
            if (yongjiu.equals("1")) { // 1 有效期  2 永久
                tvTime.setText(time);
            } else if (yongjiu.equals("2")) {
                tvTime.setText("永久使用");
            }
//        }


    }



    /*
* 将时间戳转换为时间
*/
    public String stampToDate(String s) {
        if(s == null){
            return "";
        }

        if(s.length() == 0){
            return "";
        }

        if(s.indexOf("-") != -1){
            return "";
        }
        String res;
//        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("YY-MM-dd HH:mm:ss");
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm");
        long lt = new Long(s);
        Date date = new Date(lt * 1000);
        res = simpleDateFormat.format(date);
        return res;
    }


    @OnClick({R.id.image_back, R.id.tv_title, R.id.tv_money, R.id.tv_time,R.id.btn_queren})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.image_back:
                finish();
                break;
            case R.id.tv_title:
                break;
            case R.id.tv_money:
                break;
            case R.id.tv_time:
                break;
            case R.id.btn_queren:
//                确认使用的网络请求
                initQueRenShiYongHttp();
                break;
        }
    }


//    确认使用的网络请求
    private void initQueRenShiYongHttp() {
        OkHttpUtils.post()
                .url(ContractUtils.LOGIN_URL+"Shop/queren")
                .addHeader("token",ContractUtils.getTOKEN(YouHuiQuanXinXiActivity.this))
                .addParams("phone",ContractUtils.getPhone(YouHuiQuanXinXiActivity.this))
                .addParams("parkId",ContractUtils.getParkId(YouHuiQuanXinXiActivity.this))
                .addParams("code",code)
                .build()
                .execute(new StringCallback() {
                    @Override
                    public void onError(Call call, Exception e, int id) {

                    }

                    @Override
                    public void onResponse(String response, int id) {
                        System.out.println(response+"           优惠券确认使用的网络请求 ");
                        ContractUtils.Code500(YouHuiQuanXinXiActivity.this,response);
                        if(response.indexOf("200") != -1){
                            Gson gson = new Gson();
                            YouHuiQuanShiYongEntity youHuiQuanShiYongEntity = gson.fromJson(response, YouHuiQuanShiYongEntity.class);
                            Toast.makeText(YouHuiQuanXinXiActivity.this, youHuiQuanShiYongEntity.getResponse().getMes(), Toast.LENGTH_SHORT).show();
                            finish();
                        }
                    }
                });
    }


}
