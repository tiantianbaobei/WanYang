package com.wanyangproject.shouye;

import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.wanyangproject.R;
import com.wanyangproject.activity.HomeActivity;
import com.wanyangproject.entity.BangDingEntity;
import com.wanyangproject.entity.BangDingShiBaiEntity;
import com.wanyangproject.utils.ContractUtils;
import com.zhy.http.okhttp.OkHttpUtils;
import com.zhy.http.okhttp.callback.StringCallback;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.Call;

public class ShenFenBangDingActivity extends AppCompatActivity {


    @BindView(R.id.tv_fuwu_xiangqing)
    TextView tvFuwuXiangqing;
    @BindView(R.id.relative_title)
    RelativeLayout relativeTitle;
    @BindView(R.id.tv_xuanze)
    TextView tvXuanze;
    @BindView(R.id.image_fanhui)
    ImageView imageFanhui;
    @BindView(R.id.tv_xuanzeyuanqu)
    TextView tvXuanzeyuanqu;
    @BindView(R.id.view)
    View view;
    @BindView(R.id.tv_shenfenzheng)
    TextView tvShenfenzheng;
    @BindView(R.id.relative)
    RelativeLayout relative;
    @BindView(R.id.btn_tijiao)
    Button btnTijiao;

    @BindView(R.id.et_card)
    EditText etCard;
    @BindView(R.id.image_finish)
    ImageView imageFinish;
    private NetWork netWork;

    private String yuanqu;
    private String parkId;
    private String registertoken;
    private String card;
    private  String bangding;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shen_fen_bang_ding);
        ButterKnife.bind(this);
        if (Build.VERSION.SDK_INT >= 21) {
            View dview = getWindow().getDecorView();
            dview.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN | View.SYSTEM_UI_FLAG_LAYOUT_STABLE);
            getWindow().setStatusBarColor(Color.TRANSPARENT);
        }


        Intent intent = getIntent();
        card = intent.getStringExtra("card");
        bangding = intent.getStringExtra("bangding");
        if(card == null){

        }else{
            etCard.setText(card);
        }



//        获取园区id接收广播
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("yuanquid");
        netWork = new NetWork();
        registerReceiver(netWork, intentFilter);


        SharedPreferences sharedPreferences = getSharedPreferences("registertoken", Context.MODE_PRIVATE);
        registertoken = sharedPreferences.getString("registertoken", "token");
        System.out.println(registertoken + "     注册的registertoken");


    }

    @OnClick(R.id.image_finish)
    public void onViewClicked() {
        finish();
    }


    class NetWork extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            parkId = intent.getStringExtra("parkId");
            System.out.println(parkId + "    接收的园区id");
        }
    }





    @Override
    protected void onResume() {
        super.onResume();
        SharedPreferences sharedPreferences = getSharedPreferences("yuanqu", Context.MODE_PRIVATE);
        yuanqu = sharedPreferences.getString("yuanqu", "");
        if (yuanqu.equals("")) {

        } else {
            tvXuanzeyuanqu.setText(yuanqu);
            System.out.println(yuanqu + "    选择的园区名称");
        }
    }

    @OnClick({R.id.tv_fuwu_xiangqing, R.id.relative_title, R.id.tv_xuanze, R.id.image_fanhui, R.id.tv_xuanzeyuanqu, R.id.view, R.id.tv_shenfenzheng, R.id.relative, R.id.btn_tijiao})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.tv_fuwu_xiangqing:
                break;
            case R.id.relative_title:
                break;
            case R.id.tv_xuanze:
                break;
            case R.id.image_fanhui:
                break;
            case R.id.tv_xuanzeyuanqu:
                Intent intent = new Intent(ShenFenBangDingActivity.this, XuanZeYuanQuActivity.class);
                intent.putExtra("xuanze","2");
                startActivity(intent);

//                Intent intent = new Intent(ShenFenBangDingActivity.this, QieHuanYuanQuActivity.class);
//                intent.putExtra("biaoti","1");
//                startActivity(intent);
                break;
            case R.id.view:
                break;
            case R.id.tv_shenfenzheng:
                break;
            case R.id.relative:
                break;
            case R.id.btn_tijiao:
                if(tvXuanzeyuanqu.getText().toString().trim().equals("")){
                    Toast.makeText(this, "请选择园区！", Toast.LENGTH_SHORT).show();
                }else if(etCard.getText().toString().trim().equals("")){
                    Toast.makeText(this, "请输入身份证号", Toast.LENGTH_SHORT).show();
                }else{
                    //                身份绑定的网络请求
                    initShenFenBangDingHttp();
                }
                break;
        }
    }

    //    身份绑定的网络请求
//    tiantian
    private void initShenFenBangDingHttp() {

//        if(ContractUtils.verForm(etCard.getText().toString().trim()) ==false){
//            Toast.makeText(this, "请输入正确的身份证号！", Toast.LENGTH_SHORT).show();
//            return;
//        }

        if(!(etCard.getText().toString().trim().length() == 18)){
            Toast.makeText(this, "请输入正确的身份证号！", Toast.LENGTH_SHORT).show();
            return;
        }

        System.out.println(ContractUtils.getTOKEN(ShenFenBangDingActivity.this)+"         token");
        System.out.println(ContractUtils.getParkId(ShenFenBangDingActivity.this)+"      parkid");
        System.out.println(etCard.getText().toString().trim()+"          idCard");

        final ProgressDialog progressDialog = new ProgressDialog(ShenFenBangDingActivity.this);
        progressDialog.setTitle("提示");
        progressDialog.setMessage("请等待...");
        progressDialog.setCancelable(false);
        progressDialog.show();
        OkHttpUtils.post()
                .url(ContractUtils.LOGIN_URL + "user/binding")
                .addHeader("token", ContractUtils.getTOKEN(ShenFenBangDingActivity.this))
                .addParams("parkId", ContractUtils.getParkId(ShenFenBangDingActivity.this))
                .addParams("idCard", etCard.getText().toString().trim())
                .build()
                .execute(new StringCallback() {
                    @Override
                    public void onError(Call call, Exception e, int id) {
                        System.out.println(e+"    eeebangding");
                        progressDialog.dismiss();
                    }

                    @Override
                    public void onResponse(String response, int id) {
                        System.out.println(response + "    绑定园区的网络请求");
                        ContractUtils.Code500(ShenFenBangDingActivity.this,response);
                        progressDialog.dismiss();
                        if(response.indexOf("200") != -1){
                            Gson gson = new Gson();
                            BangDingEntity bangDingEntity = gson.fromJson(response, BangDingEntity.class);
                            card = bangDingEntity.getRequest().getIdCard();

                            if(bangDingEntity.getResponse().getTypeId().equals("0") && bangDingEntity.getResponse().getTypeId2().equals("0")){
                                Toast.makeText(ShenFenBangDingActivity.this, "绑定失败！", Toast.LENGTH_SHORT).show();
                                return;
                            }else {
                                Toast.makeText(ShenFenBangDingActivity.this, "绑定成功！", Toast.LENGTH_SHORT).show();
                            }


                            ContractUtils.setParkId(bangDingEntity.getResponse().getParkInfo().getParkId());
                            ContractUtils.setPhone(bangDingEntity.getResponse().getUsername());
                            ContractUtils.setYuanquname(bangDingEntity.getResponse().getParkInfo().getParkName());
                            ContractUtils.setTypeId(bangDingEntity.getResponse().getTypeId());
                            ContractUtils.setTypeId22(bangDingEntity.getResponse().getTypeId2());


//                            Toast.makeText(ShenFenBangDingActivity.this, "绑定成功", Toast.LENGTH_SHORT).show();
//                            Toast.makeText(ShenFenBangDingActivity.this, bangDingEntity.getMsg(), Toast.LENGTH_SHORT).show();

                            if(bangding.equals("1")){
                                Intent intent = new Intent(ShenFenBangDingActivity.this, HomeActivity.class);
                                startActivity(intent);
                                finish();
                            }else if(bangding.equals("2")){
                                finish();
                            }
                        }else if(response.indexOf("400") != -1){
                            Gson gson = new Gson();
                            BangDingShiBaiEntity bangDingShiBaiEntity = gson.fromJson(response, BangDingShiBaiEntity.class);
                            Toast.makeText(ShenFenBangDingActivity.this, bangDingShiBaiEntity.getMsg(), Toast.LENGTH_SHORT).show();
                            card = bangDingShiBaiEntity.getRequest().getIdCard();
                        }
                    }
                });
    }
}
