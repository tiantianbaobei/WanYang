package com.wanyangproject.shouye;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebResourceResponse;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.gson.Gson;
import com.wanyangproject.R;
import com.wanyangproject.entity.NumberEntity;
import com.wanyangproject.entity.ShangPinXiangQingEntity;
import com.wanyangproject.utils.ContractUtils;
import com.wanyangproject.utils.WebViewUtils;
import com.youth.banner.Banner;
import com.youth.banner.BannerConfig;
import com.youth.banner.loader.ImageLoader;
import com.zhy.http.okhttp.OkHttpUtils;
import com.zhy.http.okhttp.callback.StringCallback;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.Call;

public class ShangPinXiangQingActivity extends AppCompatActivity {

    @BindView(R.id.image_back)
    ImageView imageBack;
    @BindView(R.id.image_add)
    ImageView imageAdd;
    @BindView(R.id.tv_number)
    TextView tvNumber;
    @BindView(R.id.image_jian)
    ImageView imageJian;
    @BindView(R.id.btn_jiesuan)
    Button btnJiesuan;
    @BindView(R.id.tv_gouwuche_number)
    TextView tvGouwucheNumber;
    @BindView(R.id.tv_zongjine)
    TextView tvZongjine;
    @BindView(R.id.tv_qisong_money)
    TextView tvQisongMoney;
    @BindView(R.id.tv_peisong_money)
    TextView tvPeisongMoney;
//        @BindView(R.id.image)
//    ImageView image;
    @BindView(R.id.tv_title)
    TextView tvTitle;
    @BindView(R.id.tv_xiaoliang)
    TextView tvXiaoliang;
    @BindView(R.id.tv_money)
    TextView tvMoney;
    @BindView(R.id.webView)
    WebView webView;
    @BindView(R.id.tv_zhekou)
    TextView tvZhekou;
    @BindView(R.id.tv_zhe)
    TextView tvZhe;
    @BindView(R.id.banner)
    Banner banner;
    //    @BindView(R.id.tv_shangpinxinxi)
//    TextView tvShangpinxinxi;
    private int number;
    private String shangjiaid;
    private String peisong;
    private String qisongjia;
    private ArrayList<NumberEntity> list = new ArrayList<>();
    private String shangpinid;
    private WebSettings mWebSettings;
    private String content;
    float jiage = 0;
    private String kucun;
    private int iii;
    private int position;
    private String dianpuname;
    private String http;

    private ShangPinXiangQingEntity shangPinXiangQingEntity;
    private String bidian;

    private static Boolean isOK = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shang_pin_xiang_qing);
        ButterKnife.bind(this);
        if (Build.VERSION.SDK_INT >= 21) {
            View dview = getWindow().getDecorView();
            dview.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN | View.SYSTEM_UI_FLAG_LAYOUT_STABLE);
            getWindow().setStatusBarColor(Color.TRANSPARENT);
        }


        Intent intent = getIntent();
        peisong = intent.getStringExtra("peisongfei");
        qisongjia = intent.getStringExtra("qisongjia");
        shangjiaid = intent.getStringExtra("shangjiaid");
        shangpinid = intent.getStringExtra("shangpinid");
        kucun = intent.getStringExtra("kucun");
        position = intent.getIntExtra("position", 0);
        dianpuname = intent.getStringExtra("name");
        bidian = intent.getStringExtra("bidian");
        System.out.println(bidian + "      商品详情闭店");

//        kucun = intent.getIntExtra("kucun",0);

        System.out.println(kucun + "       接收库存");

        System.out.println(shangpinid + "        商品详情id");

        System.out.println(shangjiaid + "       接收商家id");
        System.out.println(peisong + "     接收配送费");
        System.out.println(qisongjia + "   接收起送价");


        mWebSettings = webView.getSettings();
        mWebSettings.setJavaScriptEnabled(true);  //允许加载javascript
        webView.getSettings().setLayoutAlgorithm(WebSettings.LayoutAlgorithm.SINGLE_COLUMN);
        webView.getSettings().setLoadWithOverviewMode(true);
        mWebSettings.setSupportZoom(false);     //允许缩放
        mWebSettings.setBuiltInZoomControls(false); //原网页基础上缩放
        mWebSettings.setUseWideViewPort(false);   //任意比例缩放
//        mWebSettings.setUseWideViewPort(true); //将图片调整到适合webview的大小
//        mWebSettings.setLoadWithOverviewMode(true); // 缩放至屏幕的大小
//        mWebSettings.setTextSize(WebSettings.TextSize.SMALLEST);


        initView();


//        商品详情的网络请求
        final ProgressDialog progressDialog = new ProgressDialog(ShangPinXiangQingActivity.this);
        progressDialog.setTitle("提示");
        progressDialog.setMessage("请等待...");
        progressDialog.setCancelable(false);
        progressDialog.show();
        OkHttpUtils.post()
                .url(ContractUtils.LOGIN_URL + "Life/goods")
                .addHeader("token", ContractUtils.getTOKEN(ShangPinXiangQingActivity.this))
                .addParams("parkId", ContractUtils.getParkId(ShangPinXiangQingActivity.this))
                .addParams("goodsid", shangpinid)
                .build()
                .execute(new StringCallback() {
                    @Override
                    public void onError(Call call, Exception e, int id) {
                        progressDialog.dismiss();
                    }

                    @Override
                    public void onResponse(String response, int id) {
                        ContractUtils.Code500(ShangPinXiangQingActivity.this, response);
                        System.out.println(response + "            商品详情的网络请求");
                        progressDialog.dismiss();

                        if (response.indexOf("200") != -1) {
                            Gson gson = new Gson();
                            shangPinXiangQingEntity = gson.fromJson(response, ShangPinXiangQingEntity.class);


                            if (shangPinXiangQingEntity.getResponse().getGoods().getDiscount().equals("")) {
                                tvZhe.setVisibility(View.GONE);
                            } else {
                                tvZhe.setVisibility(View.VISIBLE);
                                tvZhe.setText(shangPinXiangQingEntity.getResponse().getGoods().getDiscount());
                            }


                            //                        banner轮播
                            ArrayList<String> list = new ArrayList<String>();
                            for (int i = 0; i < shangPinXiangQingEntity.getResponse().getGoods().getMaster().size(); i++) {
//                                YuanQuZhaoShangEntity.ResponseBean.BannerArrBean bannerArrBean = yuanQuZhaoShangEntity.getResponse().getBannerArr().get(i);
                                String tupian = shangPinXiangQingEntity.getResponse().getGoods().getMaster().get(i);
                                if (tupian.indexOf("http") != -1) {
                                    http = tupian.substring(tupian.indexOf("http"));
                                    System.out.println(http + "     http");
                                    list.add(http);
                                } else if(tupian.indexOf("http://") != -1){
                                    list.add(ContractUtils.PHOTO_URL + http);
                                }

                                banner.setBannerStyle(BannerConfig.CIRCLE_INDICATOR);
                                banner.setImageLoader(new MyImageLoader());
                                banner.setImages(list);
                                //设置自动轮播，默认为true
                                banner.isAutoPlay(false);
                                //banner设置方法全部调用完毕时最后调用
                                banner.start();
                            }





//                            if (shangPinXiangQingEntity.getResponse().getGoods().getMaster() != null) {
//                                String master = shangPinXiangQingEntity.getResponse().getGoods().getMaster();
//                                if (master.indexOf("http") != -1) {
//                                    String http = master.substring(master.indexOf("http"));
//                                    System.out.println(http + "     http");
//                                    Glide.with(ShangPinXiangQingActivity.this).load(http).into(image);
//                                } else {
//                                    Glide.with(ShangPinXiangQingActivity.this).load(ContractUtils.PHOTO_URL + shangPinXiangQingEntity.getResponse().getGoods().getMaster()).into(image);
//                                }
//                            }



                            if (shangPinXiangQingEntity.getResponse().getGoods().getGoods_name() == null) {

                            } else {
                                tvTitle.setText(shangPinXiangQingEntity.getResponse().getGoods().getGoods_name());
                            }


                            if (shangPinXiangQingEntity.getResponse().getGoods().getPic() == null) {

                            } else {
                                tvMoney.setText("¥ " + shangPinXiangQingEntity.getResponse().getGoods().getPic());
                            }


                            if (shangPinXiangQingEntity.getResponse().getGoods().getContent() == null) {

                            } else {
                                //                            内容
                                content = shangPinXiangQingEntity.getResponse().getGoods().getContent();
                                webView.loadDataWithBaseURL("http://www.baidu.com", getNewContent(content), "text/html", "UTF-8", null);
                            }


                            if (shangPinXiangQingEntity.getResponse().getGoods().getVolume() == null) {

                            } else {
                                tvXiaoliang.setText("月销:" + shangPinXiangQingEntity.getResponse().getGoods().getVolume());
                            }


                            if (shangPinXiangQingEntity.getResponse().getGoods().getMarket() == null) {

                            } else {
                                tvZhekou.setText("¥" + shangPinXiangQingEntity.getResponse().getGoods().getMarket());
                            }
                        }
                    }
                });

        initXiaMian();

    }


    private void initXiaMian() {
//        中间数量
        for (int i = 0; i < NumberEntity.lists.size(); i++) {
            NumberEntity numberEntity = NumberEntity.lists.get(i);
            if (numberEntity.getId().equals(shangpinid)) {
                tvNumber.setText(numberEntity.getNumber());
            }
        }


        if (qisongjia.equals("")) {
            qisongjia = "0";
        } else {
            tvQisongMoney.setText(" ¥ " + qisongjia);
        }

        if (peisong.equals("")) {
            peisong = "0";
        } else {
            tvPeisongMoney.setText(" ¥ " + peisong);
        }

        Integer shumu = 0;
        float jiage = 0;
        list = NumberEntity.lists;
        for (int i = 0; i < NumberEntity.lists.size(); i++) {
            NumberEntity nn = NumberEntity.lists.get(i);
            System.out.println(nn.getNumber() + "          ccccccccccccc");
            shumu += Integer.parseInt(nn.getNumber());
            jiage += Integer.parseInt(nn.getNumber()) * Float.parseFloat(nn.getPic());
        }

        if (shumu <= 0) {
            tvGouwucheNumber.setVisibility(View.GONE);
        } else {
            DecimalFormat decimalFormat = new DecimalFormat("0.00");//构造方法的字符格式这里如果小数不足2位,会以0补足.
            String jiage1 = decimalFormat.format(jiage);//format 返回的是字符串
            tvGouwucheNumber.setVisibility(View.VISIBLE);
            tvGouwucheNumber.setText(shumu + "");
            tvZongjine.setText("¥ " + jiage1 + "");
//            tvZongjine.setText("¥ " + jiage + "");
        }

    }










    public class MyImageLoader extends ImageLoader {
        @Override
        public void displayImage(Context context, Object path, ImageView imageView) {
            Glide.with(context).load(path).into(imageView);
        }
    }











    private void initView() {
        webView.setWebViewClient(new WebViewClient() {
            /**
             * 拦截 url 跳转,在里边添加点击链接跳转或者操作
             */
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, WebResourceRequest request) {
                return WebViewUtils.shouldOverrideUrlLoading(view, request, ShangPinXiangQingActivity.this);
            }


            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                System.out.println("66666666666666666666");
                return WebViewUtils.shouldOverrideUrlLoading(view, url, ShangPinXiangQingActivity.this);

            }


            /**
             * 在开始加载网页时会回调
             */
            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                super.onPageStarted(view, url, favicon);
                System.out.println("111111111111111111");
            }

            /**
             * 加载错误的时候会回调，在其中可做错误处理，比如再请求加载一次，或者提示404的错误页面
             */
            @Override
            public void onReceivedError(WebView view, WebResourceRequest request, WebResourceError error) {
                super.onReceivedError(view, request, error);

            }


            /**
             * 在结束加载网页时会回调
             */
            @Override
            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(view, url);
                System.out.println("chenggongchenggongchenggong");
            }

            /**
             * 当接收到https错误时，会回调此函数，在其中可以做错误处理
             */
            @Override
            public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
                super.onReceivedError(view, errorCode, description, failingUrl);
            }

            /**
             * 在每一次请求资源时，都会通过这个函数来回调
             */
            public WebResourceResponse shouldInterceptRequest(WebView view, String url) {
                return null;
            }
        });
    }


    @OnClick({R.id.tv_gouwuche_number, R.id.image_back, R.id.tv_title, R.id.image_add, R.id.image_jian, R.id.tv_number, R.id.tv_xiaoliang, R.id.tv_money, R.id.btn_jiesuan})
    public void onViewClicked(View view) {
        switch (view.getId()) {
//            购物车的数量
            case R.id.tv_gouwuche_number:

                break;
//            返回按钮
            case R.id.image_back:
                finish();
                break;
//            标题
            case R.id.tv_title:
                break;
//            销量
            case R.id.tv_xiaoliang:
                break;
//            价格
            case R.id.tv_money:
                break;
//            添加按钮
            case R.id.image_add:
                iii = Integer.parseInt(kucun);
                if (iii == 0) {
                    Toast.makeText(ShangPinXiangQingActivity.this, "库存不足", Toast.LENGTH_SHORT).show();
                    return;
                }
                iii--;

                kucun = iii + "";


                Intent intent = new Intent();
                intent.putExtra("kucun", iii);
                intent.putExtra("position", position);
                System.out.println(iii + "      加号发送广播库存");
                System.out.println(peisong + "      加号下标发送广播库存");
                intent.setAction("kucun");
                sendBroadcast(intent);


                int add = -1;
                Integer content = 0;
                for (int i = 0; i < NumberEntity.lists.size(); i++) {
                    NumberEntity addnumberEntity = NumberEntity.lists.get(i);
                    if (addnumberEntity.getId().equals(shangpinid)) {
                        add = i;
                        content = Integer.parseInt(addnumberEntity.getNumber());
                    }
                }
                content++;

                if (add == -1) {
                    NumberEntity numberEntity11 = new NumberEntity();
                    numberEntity11.setId(shangpinid);
                    numberEntity11.setPic(shangPinXiangQingEntity.getResponse().getGoods().getPic());
                    numberEntity11.setNumber(content + "");
                    numberEntity11.setName(shangPinXiangQingEntity.getResponse().getGoods().getGoods_name());
                    NumberEntity.lists.add(numberEntity11);
                } else {
                    NumberEntity numberEntity11 = NumberEntity.lists.get(add);
                    numberEntity11.setNumber(content + "");
                }

                initXiaMian();

//                int i = Integer.parseInt(tvNumber.getText().toString());
//                i++;
//                tvNumber.setText(i + "");
//                tvGouwucheNumber.setVisibility(View.VISIBLE);
//                tvGouwucheNumber.setText(i + "");
                break;
//            数量
            case R.id.tv_number:
                break;
//            减少按钮
            case R.id.image_jian:
                int ii = Integer.parseInt(tvNumber.getText().toString());
                if (ii == 0) {
                    return;
                }


                iii = Integer.parseInt(kucun);
                iii++;
                kucun = iii + "";


                Intent intent1 = new Intent();
                intent1.putExtra("kucun", iii);
                intent1.putExtra("position", position);
                System.out.println(iii + "      减号发送广播库存");
                System.out.println(position + "      减号下标发送广播库存");
                intent1.setAction("kucun");
                sendBroadcast(intent1);
                ii--;
                for (int i = 0; i < NumberEntity.lists.size(); i++) {
                    NumberEntity jiannumberEntity = NumberEntity.lists.get(i);
                    if (jiannumberEntity.getId().equals(shangpinid)) {
                        jiannumberEntity.setNumber(ii + "");
                    }
                }

                initXiaMian();

//                if (ii <= 0) {
//                    ii = 0;
//                    tvGouwucheNumber.setVisibility(View.GONE);
//                }
//                tvNumber.setText(ii + "");
//                tvGouwucheNumber.setText(ii + "");

                break;
            case R.id.btn_jiesuan:
                System.out.println(list + "                   list");

                System.out.println(list.size() + "          list.size()");

                Integer shumu = 0;
                float jiage = 0;
                float qisongjia = 0;
                for (int i = 0; i < list.size(); i++) {
                    NumberEntity nn = list.get(i);
                    System.out.println(nn.getNumber() + "          ccccccccccccc");
                    shumu += Integer.parseInt(nn.getNumber());
                    jiage += Integer.parseInt(nn.getNumber()) * Float.parseFloat(nn.getPic());
                }

                System.out.println("     wwwwwwwwwwwwwwwwwwwwwwwwwwwwwwww");

                qisongjia = Float.parseFloat(qisongjia + "");

                if (shumu == 0) {
                    Toast.makeText(this, "请先选择商品！", Toast.LENGTH_SHORT).show();
                    return;
                }

                if (jiage < qisongjia) {
                    Toast.makeText(this, "不满足配送条件", Toast.LENGTH_SHORT).show();
                } else if (shumu == 0) {
                    Toast.makeText(this, "请先选择商品", Toast.LENGTH_SHORT).show();
                } else {

                    List<Map<String, String>> list_post = new ArrayList();
                    Gson gson = new Gson();

                    jiage = 0;

                    for (int i = 0; i < list.size(); i++) {
                        HashMap map = new HashMap();
                        NumberEntity nn = list.get(i);
                        System.out.println(nn.getNumber() + "          ccccccccccccc");
                        shumu += Integer.parseInt(nn.getNumber()); //数量
                        jiage += Integer.parseInt(nn.getNumber()) * Float.parseFloat(nn.getPic());// 价格
                        map.put("id", nn.getId());
                        map.put("num", nn.getNumber());
                        list_post.add(map);
                    }
                    System.out.println(list_post.size() + "            list_post");
                    final String jsonString = gson.toJson(list_post);
                    System.out.println(jsonString + "                    hhhhhhhhhhhhhhh");


                    //                    1 营业    2 休息
                    if (bidian != null) {
                        if (bidian.equals("1")) {
                            Intent intent11 = new Intent(ShangPinXiangQingActivity.this, DingDanXiangQingActivity.class);
                            intent11.putExtra("name", dianpuname);
                            intent11.putExtra("json", jsonString);
                            intent11.putExtra("peisongfei", peisong);
                            System.out.println(peisong + "   点击结算传送的配送费");
                            intent11.putExtra("zongjiage", jiage + "");
                            System.out.println(jiage + "    总价格");
                            intent11.putExtra("shopid", shangjiaid);
                            System.out.println(shangjiaid + "     传送的商家id");
                            startActivity(intent11);
                        } else if (bidian.equals("2")) {
                            Toast.makeText(this, "商家已闭店！", Toast.LENGTH_SHORT).show();
                        }
                    }


//                    Intent intent11 = new Intent(ShangPinXiangQingActivity.this, DingDanXiangQingActivity.class);
//                    intent11.putExtra("name",dianpuname);
//                    intent11.putExtra("json", jsonString);
//                    intent11.putExtra("peisongfei", peisong);
//                    System.out.println(peisong + "   点击结算传送的配送费");
//                    intent11.putExtra("zongjiage", jiage + "");
//                    System.out.println(jiage + "    总价格");
//                    intent11.putExtra("shopid", shangjiaid);
//                    System.out.println(shangjiaid + "     传送的商家id");
//                    startActivity(intent11);


                }


//                else {
////                    结算添加订单网络请求   结算
//                    initJieSuanHttp();
//                    System.out.println(" qqqqqqqqqqqqqqqqqqqq");
//                }

//

//
//                Intent intent = new Intent(ShangPinXiangQingActivity.this, DingDanXiangQingActivity.class);
//                startActivity(intent);
                break;
        }
    }


    //    结算网络请求
////    tiantian
//    private void initJieSuanHttp() {
//
//        List<Map<String, String>> list_post = new ArrayList();
//        Gson gson = new Gson();
//
//        Integer shumu = 0;
//        jiage = 0;
//
//        for (int i = 0; i < list.size(); i++) {
//            HashMap map = new HashMap();
//            NumberEntity nn = list.get(i);
//            System.out.println(nn.getNumber() + "          ccccccccccccc");
//            shumu += Integer.parseInt(nn.getNumber()); //数量
//            jiage += Integer.parseInt(nn.getNumber()) * Float.parseFloat(nn.getPic());// 价格
//            map.put("id", nn.getId());
//            map.put("num", nn.getNumber());
//            list_post.add(map);
//        }
//        System.out.println(list_post.size() + "            list_post");
//        final String jsonString = gson.toJson(list_post);
//        System.out.println(jsonString + "                    hhhhhhhhhhhhhhh");
//
//        float peisongfei = Float.parseFloat(peisong);// 配送费
////        final float zongji = jiage + peisongfei;// 总计的钱
//
//
//        System.out.println(System.currentTimeMillis() + "     时间一");
//        final ProgressDialog progressDialog = new ProgressDialog(ShangPinXiangQingActivity.this);
//        progressDialog.setTitle("提示");
//        progressDialog.setMessage("请等待...");
//        progressDialog.setCancelable(false);
//        progressDialog.show();
//        OkHttpUtils.post()
//                .url(ContractUtils.LOGIN_URL + "Life/add_order")
//                .addHeader("token", ContractUtils.getTOKEN(ShangPinXiangQingActivity.this))
//                .addParams("goods", jsonString)
//                .addParams("money", jiage + "")   //    .addParams("money", jiage+"")
//                .addParams("parkId", ContractUtils.getParkId(ShangPinXiangQingActivity.this))
//                .addParams("shopId", shangjiaid)  // 暂时  tiantian   商家id
//                .build()
//                .execute(new StringCallback() {
//                    @Override
//                    public void onError(Call call, Exception e, int id) {
//                        progressDialog.dismiss();
//                        System.out.println(e + "            结算eeeeee");
//                    }
//
//                    @Override
//                    public void onResponse(String response, int id) {
//                        ContractUtils.Code500(ShangPinXiangQingActivity.this, response);
//                        progressDialog.dismiss();
//                        System.out.println(response + "          结算网络请求");
//                        System.out.println(System.currentTimeMillis() + "         时间二");
//                        ContractUtils.Code500(ShangPinXiangQingActivity.this, response);
//
//                        if (response.indexOf("200") != -1) {
//                            Gson gson = new Gson();
//                            JieSuanEntity jieSuanEntity = gson.fromJson(response, JieSuanEntity.class);
//                            Intent intent = new Intent(ShangPinXiangQingActivity.this, DingDanXiangQingActivity.class);
////                            intent.putExtra("json",jieSuanEntity.getResponse().);
//                            intent.putExtra("peisongfei", peisong);
//                            System.out.println(peisong + "   点击结算传送的配送费");
//                            intent.putExtra("zongjiage", jiage + "");
//                            System.out.println(jiage + "    总价格");
//                            intent.putExtra("dingdanhao", jieSuanEntity.getResponse().getOrder_sn());
//                            System.out.println(jieSuanEntity.getResponse().getOrder_sn() + "    点击结算传送的订单号");
//                            intent.putExtra("shopid", shangjiaid);
//                            System.out.println(shangjiaid + "     传送的商家id");
//                            startActivity(intent);
////                            finish();
//                        }
//                    }
//                });
//    }


    /**
     * 将html文本内容中包含img标签的图片，宽度变为屏幕宽度，高度根据宽度比例自适应
     **/
    public static String getNewContent(String htmltext) {

//        if(isOK == false){
//            return htmltext;
//        }
//
//        isOK = true;
        try {
            Document doc = Jsoup.parse(htmltext);
            Elements elements = doc.getElementsByTag("img");
            for (Element element : elements) {
                element.attr("width", "100%").attr("height", "auto");
            }

            return doc.toString();
        } catch (Exception e) {
            return htmltext;
        }
    }


}
