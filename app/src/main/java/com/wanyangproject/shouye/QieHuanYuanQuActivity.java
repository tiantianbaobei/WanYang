package com.wanyangproject.shouye;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.wanyangproject.R;
import com.wanyangproject.adapter.ChangYongYuanQuAdapter;
import com.wanyangproject.adapter.MoRenYuanQuAdapter;
import com.wanyangproject.entity.SheZhiChangYongYuanQuEntity;
import com.wanyangproject.entity.SheZhiYuanQuNameEntity;
import com.wanyangproject.utils.ContractUtils;
import com.zhy.http.okhttp.OkHttpUtils;
import com.zhy.http.okhttp.callback.StringCallback;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.Call;

public class QieHuanYuanQuActivity extends AppCompatActivity {


    @BindView(R.id.image_back)
    ImageView imageBack;
    @BindView(R.id.image_add)
    ImageView imageAdd;
    //    @BindView(R.id.recyclerView_moren)
//    RecyclerView recyclerViewMoren;
    @BindView(R.id.recyclerView_changyong)
    RecyclerView recyclerViewChangyong;
    @BindView(R.id.tv_dangqian)
    TextView tvDangqian;
    @BindView(R.id.tv_morenyuanqu)
    TextView tvMorenyuanqu;
    @BindView(R.id.tv_qiehuan_yuanqu)
    TextView tvQiehuanYuanqu;
    private MoRenYuanQuAdapter moRenYuanQuAdapter;
    private ChangYongYuanQuAdapter changYongYuanQuAdapter;
    private String yuanqu;
    private String biaoti;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_qie_huan_yuan_qu);
        ButterKnife.bind(this);

        initView();

        Intent intent = getIntent();
        biaoti = intent.getStringExtra("biaoti");

            if (biaoti.equals("1")) {
                tvQiehuanYuanqu.setText("选择园区");
            }else if(biaoti.equals("2")){
                tvQiehuanYuanqu.setText("切换园区");
            }



        if (ContractUtils.getYuanquname(QieHuanYuanQuActivity.this) != null) {
            tvMorenyuanqu.setText(ContractUtils.getYuanquname(QieHuanYuanQuActivity.this));
        }


        if (ContractUtils.getYuanquname(QieHuanYuanQuActivity.this) != null) {
            tvDangqian.setText("当前：" + ContractUtils.getYuanquname(QieHuanYuanQuActivity.this));
        }


//        常用园区的网络请求
        initChangYongYuanQuHttp();


    }


    @Override
    protected void onResume() {
        super.onResume();
        //        常用园区的网络请求
        initChangYongYuanQuHttp();

        SharedPreferences sharedPreferences = getSharedPreferences("yuanqu", Context.MODE_PRIVATE);
        yuanqu = sharedPreferences.getString("yuanqu", "");
        if (yuanqu.equals("")) {

        } else {
            tvMorenyuanqu.setText(yuanqu);
            tvDangqian.setText(yuanqu);
            System.out.println(yuanqu + "    选择的园区名称");
        }
    }


    //    常用园区的网络请求
    private void initChangYongYuanQuHttp() {
        OkHttpUtils.post()
                .url(ContractUtils.LOGIN_URL + "park/userPark")
                .addHeader("token", ContractUtils.getTOKEN(QieHuanYuanQuActivity.this))
                .build()
                .execute(new StringCallback() {
                    @Override
                    public void onError(Call call, Exception e, int id) {

                    }

                    @Override
                    public void onResponse(String response, int id) {
                        ContractUtils.Code500(QieHuanYuanQuActivity.this, response);
                        System.out.println(response + "          常用园区的网络请求");
                        if (response.indexOf("200") != -1) {
                            Gson gson = new Gson();
                            SheZhiChangYongYuanQuEntity sheZhiChangYongYuanQuEntity = gson.fromJson(response, SheZhiChangYongYuanQuEntity.class);
                            changYongYuanQuAdapter = new ChangYongYuanQuAdapter(QieHuanYuanQuActivity.this, sheZhiChangYongYuanQuEntity.getResponse());
                            LinearLayoutManager manager1 = new LinearLayoutManager(QieHuanYuanQuActivity.this);
                            recyclerViewChangyong.setLayoutManager(manager1);
                            recyclerViewChangyong.setAdapter(changYongYuanQuAdapter);


                            changYongYuanQuAdapter.setChangYongYuanQuClick(new ChangYongYuanQuAdapter.ChangYongYuanQuClick() {
                                @Override
                                public void changyongyuanquClick(int position, String name,String id) {
                                    tvDangqian.setText("当前：" + name);
                                    tvMorenyuanqu.setText(name);
//                                    设置园区接口
                                    initSheZhiYuanQuHttp(id);
                                }
                            });
                        }
                    }
                });
    }






//    设置园区接口
    private void initSheZhiYuanQuHttp(String id) {
        OkHttpUtils.post()
                .url(ContractUtils.LOGIN_URL+"user/setDefaultPark")
                .addHeader("token",ContractUtils.getTOKEN(QieHuanYuanQuActivity.this))
                .addParams("parkId",id)
                .build()
                .execute(new StringCallback() {
                    @Override
                    public void onError(Call call, Exception e, int id) {

                    }

                    @Override
                    public void onResponse(String response, int id) {
                        System.out.println(response+"               设置园区接口");
                        if(response.indexOf("200") != -1){
                            Gson gson = new Gson();
                            SheZhiYuanQuNameEntity sheZhiYuanQuNameEntity = gson.fromJson(response, SheZhiYuanQuNameEntity.class);
                            ContractUtils.setParkId(sheZhiYuanQuNameEntity.getResponse().getParkInfo().getParkId());
                            ContractUtils.setPhone(sheZhiYuanQuNameEntity.getResponse().getUsername());
                            ContractUtils.setYuanquname(sheZhiYuanQuNameEntity.getResponse().getParkInfo().getParkName());
                            ContractUtils.setTypeId(sheZhiYuanQuNameEntity.getResponse().getTypeId());
                            ContractUtils.setTypeId22(sheZhiYuanQuNameEntity.getResponse().getTypeId2());
                            finish();
                        }
                    }
                });
    }





    private void initView() {
//        moRenYuanQuAdapter = new MoRenYuanQuAdapter(QieHuanYuanQuActivity.this, ContractUtils.getYuanquname(QieHuanYuanQuActivity.this));
//        LinearLayoutManager manager = new LinearLayoutManager(QieHuanYuanQuActivity.this);
//        recyclerViewMoren.setLayoutManager(manager);
//        recyclerViewMoren.setAdapter(moRenYuanQuAdapter);


//        changYongYuanQuAdapter = new ChangYongYuanQuAdapter(QieHuanYuanQuActivity.this);
//        LinearLayoutManager manager1 = new LinearLayoutManager(QieHuanYuanQuActivity.this);
//        recyclerViewChangyong.setLayoutManager(manager1);
//        recyclerViewChangyong.setAdapter(changYongYuanQuAdapter);


    }

    @OnClick({R.id.image_back, R.id.image_add, R.id.recyclerView_changyong, R.id.tv_dangqian, R.id.tv_morenyuanqu})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.image_back:
                finish();
                break;
            case R.id.image_add:
                Intent intent = new Intent(QieHuanYuanQuActivity.this, XuanZeYuanQuActivity.class);
                intent.putExtra("xuanze","1");
                startActivity(intent);
                finish();
                break;
//            case R.id.recyclerView_moren:
//                break;
            case R.id.recyclerView_changyong:
                break;
            case R.id.tv_dangqian:
                break;
            case R.id.tv_morenyuanqu:
                tvDangqian.setText("当前：" + ContractUtils.getYuanquname(QieHuanYuanQuActivity.this));
                tvMorenyuanqu.setText(ContractUtils.getYuanquname(QieHuanYuanQuActivity.this));
//                                    设置园区接口
                String id = ContractUtils.getParkId(QieHuanYuanQuActivity.this);
                initSheZhiYuanQuHttp(id);
                break;
        }
    }
}
