package com.wanyangproject.shouye;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.NumberPicker;
import android.widget.TextView;
import android.widget.Toast;

import com.bigkoo.pickerview.OptionsPickerView;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.wanyangproject.R;
import com.wanyangproject.entity.ProvinceEntity;
import com.wanyangproject.entity.QuXiaoMoRenEntity;
import com.wanyangproject.entity.TianJiaShouHuoDiZhiEntity;
import com.wanyangproject.entity.XiuGaiDiZhiEntity;
import com.wanyangproject.popuwindow.AddressPopupWindow;
import com.wanyangproject.utils.ContractUtils;
import com.wanyangproject.utils.GetJsonDataUtil;
import com.wanyangproject.utils.JsonBean;
import com.wanyangproject.utils.SharepreferencesDiZhi;
import com.zhy.http.okhttp.OkHttpUtils;
import com.zhy.http.okhttp.callback.StringCallback;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.Call;

//添加收货信息
public class TianJiaShouHuoXinXiActivity extends AppCompatActivity {

    @BindView(R.id.image_back)
    ImageView imageBack;
    @BindView(R.id.et_lianxiren)
    EditText etLianxiren;
    @BindView(R.id.et_phone)
    EditText etPhone;
    @BindView(R.id.image_cuo)
    ImageView imageCuo;
    @BindView(R.id.tv_address)
    TextView tvAddress;
    @BindView(R.id.image_to)
    ImageView imageTo;
    @BindView(R.id.btn_wancheng)
    Button btnWancheng;
    @BindView(R.id.et_xiangxidizhi)
    EditText etXiangxidizhi;
    @BindView(R.id.image_moren)
    ImageView imageMoren;
    private AddressPopupWindow addressPopupWindow;
    private List<ProvinceEntity> provinceEntityList;
    private String[] province;
    private int i = 0;
    ////    private Boolean isOK = false;
//    private String moren;
//    private String response;
    private String name;
    private String phone;
    private String xuanzedizhi;
    private String xiangxidizhi;
    private String morendizhi = "2";
    private String id;
    private ArrayList<JsonBean> options1Items = new ArrayList<>();
    private ArrayList<ArrayList<String>> options2Items = new ArrayList<>();
    private ArrayList<ArrayList<ArrayList<String>>> options3Items = new ArrayList<>();
    private Thread thread;
    private static final int MSG_LOAD_DATA = 0x0001;
    private static final int MSG_LOAD_SUCCESS = 0x0002;
    private static final int MSG_LOAD_FAILED = 0x0003;

    private boolean isLoaded = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tian_jia_shou_huo_xin_xi);
        ButterKnife.bind(this);
        if (Build.VERSION.SDK_INT >= 21) {
            View dview = getWindow().getDecorView();
            dview.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN | View.SYSTEM_UI_FLAG_LAYOUT_STABLE);
            getWindow().setStatusBarColor(Color.TRANSPARENT);
        }


        Intent intent = getIntent();
        name = intent.getStringExtra("name");
        phone = intent.getStringExtra("phone");
        xuanzedizhi = intent.getStringExtra("xuanzedizhi");
        xiangxidizhi = intent.getStringExtra("xiangxidizhi");
        morendizhi = intent.getStringExtra("morendizhi");
        if(morendizhi == null){
            morendizhi = "2";
        }
        id = intent.getStringExtra("id");

        if(name != null){
            etLianxiren.setText(name);
        }

        if(phone != null){
            etPhone.setText(phone);
        }

        if(tvAddress != null){
            tvAddress.setText(xuanzedizhi);
        }

        if(xiangxidizhi != null){
            etXiangxidizhi.setText(xiangxidizhi);
        }


        if(morendizhi != null){
            if(morendizhi.equals("2")){
                imageMoren.setImageResource(R.drawable.unchecked);
            }else if(morendizhi.equals("1")){
                imageMoren.setImageResource(R.drawable.checked);
            }
        }


        System.out.println(name+"     name");
        System.out.println(phone+"     phone");
        System.out.println(xuanzedizhi+"     xuanzedizhi");
        System.out.println(xiangxidizhi+"     xiangxidizhi");
        System.out.println(morendizhi+"     morendizhi");



//        //        选择省份的网络请求
//        initProvinceHttp();
    }

    @SuppressLint("HandlerLeak")
    private Handler mHandler = new Handler() {
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case MSG_LOAD_DATA:
                    if (thread == null) {//如果已创建就不再重新创建子线程了

//                        Toast.makeText(TianJiaShouHuoXinXiActivity.this, "Begin Parse Data", Toast.LENGTH_SHORT).show();
                        thread = new Thread(new Runnable() {
                            @Override
                            public void run() {
                                // 子线程中解析省市区数据
                                initJsonData();
                            }
                        });
                        thread.start();
                    }
                    break;

                case MSG_LOAD_SUCCESS:
//                    Toast.makeText(TianJiaShouHuoXinXiActivity.this, "Parse Succeed", Toast.LENGTH_SHORT).show();
                    isLoaded = true;
                    initDiZhiTanChuang();
                    break;

                case MSG_LOAD_FAILED:
//                    Toast.makeText(TianJiaShouHuoXinXiActivity.this, "Parse Failed", Toast.LENGTH_SHORT).show();
                    break;
            }
        }
    };

//    //    选择地省份的网络请求
//    private void initProvinceHttp() {
//        OkHttpUtils.get()
//                .url(ContractUtils.URL_PROVINCE)
//                .build()
//                .execute(new StringCallback() {
//                    @Override
//                    public void onError(Call call, Exception e, int id) {
//                        Toast.makeText(TianJiaShouHuoXinXiActivity.this, "请检查您的网络连接", Toast.LENGTH_SHORT).show();
//                    }
//
//                    @Override
//                    public void onResponse(String response, int id) {
//                        Gson gson = new Gson();
//                        provinceEntityList = gson.fromJson(response, new TypeToken<List<ProvinceEntity>>() {
//                        }.getType());
//                        province = new String[provinceEntityList.size()];
//
//                        for (ProvinceEntity provinceEntity : provinceEntityList) {
//                            province[i] = provinceEntity.getName();
//                            i++;
//                        }
//                        System.out.println(response + "省份的网络请求");
//                    }
//                });
//    }
//
//
//
//
////    市
//
//    //                        选择城市的网络请求
//    private void initCityHttp() {
//        OkHttpUtils.get()
//                .url(ContractUtils.URL_CITY + province_picker.getValue())
//                .build()
//                .execute(new StringCallback() {
//                    @Override
//                    public void onError(Call call, Exception e, int id) {
//
//                    }
//
//                    @Override
//                    public void onResponse(String response, int id) {
//                        i = 0;
//                        Gson gson = new Gson();
//                        citylist = gson.fromJson(response, new TypeToken<List<ProvinceEntity>>() {
//                        }.getType());
//
//
//    }
//
//
//
//
//
//                    //    区的网络请求
//                    private void initCountyHttp(int i1) {
//                        OkHttpUtils.get()
//                                .url(ContractUtils.URL_CITY +province_picker.getValue()+"/"+ i1)
//                                .build()
//                                .execute(new StringCallback() {
//                                    @Override
//                                    public void onError(Call call, Exception e, int id) {
//
//                                    }
//
//                                    @Override
//                                    public void onResponse(String response, int id) {
//                                        System.out.println(response);
//                                        i = 0;
//                                        Gson gson = new Gson();
//                                        countryList = gson.fromJson(response, new TypeToken<List<ProvinceEntity>>() {
//                                        }.getType());
//                                        if (countryList.get(0).getName().equals("济南")) {
//                                            countryList.add(0, new ProvinceEntity("历下"));
//                                            countryList.add(0, new ProvinceEntity("市中"));
//                                            countryList.add(0, new ProvinceEntity("天桥"));
//                                            countryList.add(0, new ProvinceEntity("槐荫"));
//                                            countryList.add(0, new ProvinceEntity("历城"));
//                                        }
//                                        if (countryList.get(0).getName().equals("泰安")) {
//                                            countryList.add(0, new ProvinceEntity("泰山"));
//                                            countryList.add(0, new ProvinceEntity("岱岳"));
//                                        }
//                                        if (countryList.get(0).getName().equals("青岛")) {
//                                            countryList.add(0, new ProvinceEntity("市南"));
//                                            countryList.add(0, new ProvinceEntity("市北"));
//                                            countryList.add(0, new ProvinceEntity("城阳"));
//                                            countryList.add(0, new ProvinceEntity("四方"));
//                                            countryList.add(0, new ProvinceEntity("李沧"));
//                                            countryList.add(0, new ProvinceEntity("胶南"));
//                                        }
//                                        if (countryList.get(0).getName().equals("淄博")) {
//                                            countryList.add(0, new ProvinceEntity("张店"));
//                                        }
//                                        if (countryList.get(0).getName().equals("枣庄")) {
//                                            countryList.add(0, new ProvinceEntity("市中"));
//                                            countryList.add(0, new ProvinceEntity("山亭"));
//                                        }
//                                        if (countryList.get(0).getName().equals("烟台")) {
//                                            countryList.add(0, new ProvinceEntity("芝罘"));
//                                            countryList.add(0, new ProvinceEntity("莱山"));
//                                            countryList.add(0, new ProvinceEntity("莱州"));
//                                        }
//                                        if (countryList.get(0).getName().equals("潍坊")) {
//                                            countryList.add(0, new ProvinceEntity("寒亭"));
//                                            countryList.add(0, new ProvinceEntity("坊子"));
//                                            countryList.add(0, new ProvinceEntity("奎文"));
//                                        }
//                                        if (countryList.get(0).getName().equals("济宁")) {
//                                            countryList.add(0, new ProvinceEntity("市中"));
//                                            countryList.add(0, new ProvinceEntity("任城"));
//                                        }
//                                        if (countryList.get(0).getName().equals("日照")) {
//                                            countryList.add(0, new ProvinceEntity("东港"));
//                                            countryList.add(0, new ProvinceEntity("岚山"));
//                                        }
//                                        if (countryList.get(0).getName().equals("莱芜")) {
//                                            countryList.add(0, new ProvinceEntity("钢城"));
//                                        }
//                                        if (countryList.get(0).getName().equals("临沂")) {
//                                            countryList.add(0, new ProvinceEntity("兰山"));
//                                            countryList.add(0, new ProvinceEntity("罗庄"));
//                                            countryList.add(0, new ProvinceEntity("河东"));
//                                            countryList.add(0, new ProvinceEntity("苍山"));
//                                        }
//                                        if (countryList.get(0).getName().equals("德州")) {
//                                            countryList.add(0, new ProvinceEntity("德城"));
//                                        }
//                                        if (countryList.get(0).getName().equals("聊城")) {
//                                            countryList.add(0, new ProvinceEntity("东昌府"));
//                                        }
//                                        if (countryList.get(0).getName().equals("滨州")) {
//                                            countryList.add(0, new ProvinceEntity("滨城"));
//                                        }
//                                        if (countryList.get(0).getName().equals("菏泽")) {
//                                            countryList.add(0, new ProvinceEntity("牡丹"));
//                                        }
//
//                                    }
//                                });
//                    }


    private void initJsonData() {//解析数据

        /**
         * 注意：assets 目录下的Json文件仅供参考，实际使用可自行替换文件
         * 关键逻辑在于循环体
         *
         * */
        String JsonData = new GetJsonDataUtil().getJson(this, "province.json");//获取assets目录下的json文件数据

        ArrayList<JsonBean> jsonBean = parseData(JsonData);//用Gson 转成实体

        /**
         * 添加省份数据
         *
         * 注意：如果是添加的JavaBean实体，则实体类需要实现 IPickerViewData 接口，
         * PickerView会通过getPickerViewText方法获取字符串显示出来。
         */
        options1Items = jsonBean;

        for (int i = 0; i < jsonBean.size(); i++) {//遍历省份
            ArrayList<String> CityList = new ArrayList<>();//该省的城市列表（第二级）
            ArrayList<ArrayList<String>> Province_AreaList = new ArrayList<>();//该省的所有地区列表（第三极）

            for (int c = 0; c < jsonBean.get(i).getCityList().size(); c++) {//遍历该省份的所有城市
                String CityName = jsonBean.get(i).getCityList().get(c).getName();
                CityList.add(CityName);//添加城市
                ArrayList<String> City_AreaList = new ArrayList<>();//该城市的所有地区列表

                //如果无地区数据，建议添加空字符串，防止数据为null 导致三个选项长度不匹配造成崩溃
                if (jsonBean.get(i).getCityList().get(c).getArea() == null
                        || jsonBean.get(i).getCityList().get(c).getArea().size() == 0) {
                    City_AreaList.add("");
                } else {
                    City_AreaList.addAll(jsonBean.get(i).getCityList().get(c).getArea());
                }
                Province_AreaList.add(City_AreaList);//添加该省所有地区数据
            }

            /**
             * 添加城市数据
             */
            options2Items.add(CityList);

            /**
             * 添加地区数据
             */
            options3Items.add(Province_AreaList);
        }

        mHandler.sendEmptyMessage(MSG_LOAD_SUCCESS);

    }

    public ArrayList<JsonBean> parseData(String result) {//Gson 解析
        ArrayList<JsonBean> detail = new ArrayList<>();
        try {
            JSONArray data = new JSONArray(result);
            Gson gson = new Gson();
            for (int i = 0; i < data.length(); i++) {
                JsonBean entity = gson.fromJson(data.optJSONObject(i).toString(), JsonBean.class);
                detail.add(entity);
            }
        } catch (Exception e) {
            e.printStackTrace();
            mHandler.sendEmptyMessage(MSG_LOAD_FAILED);
        }
        return detail;
    }

    private void initDiZhiTanChuang(){
        OptionsPickerView pvOptions = new  OptionsPickerView.Builder(this, new OptionsPickerView.OnOptionsSelectListener() {
            @Override
            public void onOptionsSelect(int options1, int option2, int options3 ,View v) {
                //返回的分别是三个级别的选中位置
                String tx = options1Items.get(options1).getPickerViewText() +
                        options2Items.get(options1).get(option2) +
                        options3Items.get(options1).get(option2).get(options3);
                tvAddress.setText(tx);
            }
        })
                .setSubmitText("确定")//确定按钮文字
                .setCancelText("取消")//取消按钮文字
                .setTitleText("选择地址")//标题
                .setSubCalSize(16)//确定和取消文字大小
                .setTitleSize(16)//标题文字大小
                .setTitleColor(Color.BLACK)//标题文字颜色
                .setSubmitColor(Color.RED)//确定按钮文字颜色
                .setCancelColor(Color.parseColor("#AEAAAA"))//取消按钮文字颜色
                .setTitleBgColor(Color.parseColor("#f5f5f5"))//标题背景颜色 Night mode
                .setBgColor(Color.parseColor("#ffffff"))//滚轮背景颜色 Night mode
                .setContentTextSize(16)//滚轮文字大小
//                .setLinkage(false)//设置是否联动，默认true
                .setLabels("", "", "")//设置选择的三级单位
                .isCenterLabel(false) //是否只显示中间选中项的label文字，false则每项item全部都带有label。
                .setCyclic(false, false, false)//循环与否
                .setSelectOptions(0, 0, 0)  //设置默认选中项
                .setOutSideCancelable(false)//点击外部dismiss default true
                .isDialog(false)//是否显示为对话框样式
                .build();

        pvOptions.setPicker(options1Items, options2Items, options3Items);//添加数据源
        pvOptions.show();
    }

    @OnClick({R.id.image_back, R.id.et_lianxiren, R.id.et_phone, R.id.image_cuo, R.id.tv_address, R.id.image_to, R.id.image_moren, R.id.btn_wancheng})
    public void onViewClicked(View view) {
        switch (view.getId()) {
//            返回按钮
            case R.id.image_back:
                finish();
                break;
//            输入联系人
            case R.id.et_lianxiren:
                break;
//            输入手机号
            case R.id.et_phone:
                break;
//            错号
            case R.id.image_cuo:
                etPhone.setText("");
                break;
//            选择地址
            case R.id.tv_address:

                if(isLoaded){
                    initDiZhiTanChuang();
                }else{
                    mHandler.sendEmptyMessage(MSG_LOAD_DATA);
                }

                break;
//            返回按钮
            case R.id.image_to:
                break;
//            默认图片
            case R.id.image_moren:

                if(morendizhi.equals("1")){
                    imageMoren.setImageResource(R.drawable.unchecked);
                    //           取消默认接口的网络请求
                    initQuXiaoMoRenHttp();
                    morendizhi = "2";
                }else if(morendizhi.equals("2")){
                    imageMoren.setImageResource(R.drawable.checked);
                    morendizhi = "1";
                }
                break;
//            完成按钮
            case R.id.btn_wancheng:


                if(ContractUtils.isChinaPhoneLegal(etPhone.getText().toString().trim()) == false){
                    Toast.makeText(this, "请输入正确的手机号", Toast.LENGTH_SHORT).show();
                    return;
                }
                ContractUtils.isChinaPhoneLegal(etPhone.getText().toString().trim());

                if(name != null){
                    System.out.println(morendizhi+"               morendizhi11111111111111");

//                    编辑收货地址的网络请求
                    if(morendizhi.equals("1")){
                        SharepreferencesDiZhi.saveDiZhi(TianJiaShouHuoXinXiActivity.this,id);
                    }else{
                        SharepreferencesDiZhi.saveDiZhi(TianJiaShouHuoXinXiActivity.this,"000");
                    }
                    initBianJiShouHuoDiZhiHttp();
                }else{
//                    if(etXiangxidizhi.getText().toString().equals("")){
//                        Toast.makeText(this, "请输入联系人", Toast.LENGTH_SHORT).show();
//                        return;
//                    }
//
//                    if(etPhone.getText().toString().trim().equals("")){
//                        Toast.makeText(this, "请输入手机号", Toast.LENGTH_SHORT).show();
//                        return;
//                    }
//
//
//                    if(tvAddress.getText().toString().trim().equals("")){
//                        Toast.makeText(this, "请选择地址", Toast.LENGTH_SHORT).show();
//                        return;
//                    }
//
//                    if(etXiangxidizhi.getText().toString().trim().equals("")){
//                        Toast.makeText(this, "请输入详细地址", Toast.LENGTH_SHORT).show();
//                        return;
//                    }


                    if(etLianxiren.getText().toString().trim().equals("")){
                        Toast.makeText(this, "请输入联系人", Toast.LENGTH_SHORT).show();
                    } else if (etPhone.getText().toString().trim().equals("")) {
                        Toast.makeText(this, "请输入手机号", Toast.LENGTH_SHORT).show();
                    }else if(tvAddress.getText().toString().equals("")){
                        Toast.makeText(this, "请选择地址", Toast.LENGTH_SHORT).show();
                    }else if(etXiangxidizhi.getText().toString().trim().equals("")){
                        Toast.makeText(this, "请输入详细地址", Toast.LENGTH_SHORT).show();
                    }else{

//                        if(morendizhi.equals("1")){
//                            SharepreferencesDiZhi.saveDiZhi(TianJiaShouHuoXinXiActivity.this,id);
//                        }else{
//                            SharepreferencesDiZhi.saveDiZhi(TianJiaShouHuoXinXiActivity.this,"000");
//                        }
                        ////                                    添加收货地址的网络请求
                        initTianJiaShouHuoXInXiHttp();
                    }
                }
                break;
        }
    }


//    取消默认接口的网络请求
    private void initQuXiaoMoRenHttp() {
        OkHttpUtils.post()
                .url(ContractUtils.LOGIN_URL+"Life/quressmoren")
                .addHeader("token",ContractUtils.getTOKEN(TianJiaShouHuoXinXiActivity.this))
                .addParams("addressid",id)
                .addParams("parkId",ContractUtils.getParkId(TianJiaShouHuoXinXiActivity.this))
                .build()
                .execute(new StringCallback() {
                    @Override
                    public void onError(Call call, Exception e, int id) {

                    }

                    @Override
                    public void onResponse(String response, int id) {
                        ContractUtils.Code500(TianJiaShouHuoXinXiActivity.this,response);
                        System.out.println(response+"       取消默认的网络请求");
                        Gson gson = new Gson();
                        QuXiaoMoRenEntity quXiaoMoRenEntity = gson.fromJson(response, QuXiaoMoRenEntity.class);
                        if(response.indexOf("200") != -1){
//                            if(quXiaoMoRenEntity.getRequest().getType() != null){
//                                if(quXiaoMoRenEntity.getRequest().getType().equals("2")){
//                                    imageMoren.setImageResource(R.drawable.unchecked);
//                                }
//                            }

                        }
                    }
                });
    }

    //    编辑收货地址的网络请求
    private void initBianJiShouHuoDiZhiHttp() {
        System.out.println(morendizhi+"        morendizhihhhhhhhhhhhh");
        System.out.println(etXiangxidizhi.getText().toString().trim()+"      修改详细地址");
        final ProgressDialog progressDialog = new ProgressDialog(TianJiaShouHuoXinXiActivity.this);
        progressDialog.setTitle("提示");
        progressDialog.setMessage("请等待...");
        progressDialog.setCancelable(false);
        progressDialog.show();
        OkHttpUtils.post()
                .url(ContractUtils.LOGIN_URL+"Life/eitadd")
                .addHeader("token",ContractUtils.getTOKEN(TianJiaShouHuoXinXiActivity.this))
                .addParams("name",etLianxiren.getText().toString().trim())
                .addParams("phone",etPhone.getText().toString().trim())
                .addParams("region",tvAddress.getText().toString().trim())
                .addParams("addres",etXiangxidizhi.getText().toString().trim())
                .addParams("parkId",ContractUtils.getParkId(TianJiaShouHuoXinXiActivity.this))
                .addParams("type",morendizhi)
                .addParams("addres_id",id)
                .build()
                .execute(new StringCallback() {
                    @Override
                    public void onError(Call call, Exception e, int id) {
                        progressDialog.dismiss();
                    }

                    @Override
                    public void onResponse(String response, int id) {
                        ContractUtils.Code500(TianJiaShouHuoXinXiActivity.this,response);
                        System.out.println(response+"        修改地址的网络请求");
                        progressDialog.dismiss();

                        if(response.indexOf("200") != -1){
                            Gson gson = new Gson();
                            XiuGaiDiZhiEntity xiuGaiDiZhiEntity = gson.fromJson(response, XiuGaiDiZhiEntity.class);
                            Toast.makeText(TianJiaShouHuoXinXiActivity.this, xiuGaiDiZhiEntity.getResponse().getMes(), Toast.LENGTH_SHORT).show();
                            finish();
                        }else{
                            ContractUtils.Code400(TianJiaShouHuoXinXiActivity.this,response);
//                            Toast.makeText(TianJiaShouHuoXinXiActivity.this, xiuGaiDiZhiEntity.getResponse().getMes(), Toast.LENGTH_SHORT).show();
                        }
                    }
                });
    }



    //    添加收货地址的网络请求
    private void initTianJiaShouHuoXInXiHttp() {
        if(ContractUtils.isChinaPhoneLegal(etPhone.getText().toString().trim()) == false){
            Toast.makeText(this, "请输入正确的手机号", Toast.LENGTH_SHORT).show();
            return;
        }

        System.out.println(etXiangxidizhi.getText().toString().trim()+"           添加 详细地址");

        final ProgressDialog progressDialog = new ProgressDialog(TianJiaShouHuoXinXiActivity.this);
        progressDialog.setTitle("提示");
        progressDialog.setMessage("请等待...");
        progressDialog.setCancelable(false);
        progressDialog.show();
        OkHttpUtils.post()
                .url(ContractUtils.LOGIN_URL + "Life/addaddress")
                .addHeader("token", ContractUtils.getTOKEN(TianJiaShouHuoXinXiActivity.this))
                .addParams("name", etLianxiren.getText().toString().trim())
                .addParams("phone", etPhone.getText().toString().trim())
                .addParams("region", tvAddress.getText().toString().trim())
                .addParams("addres",etXiangxidizhi.getText().toString().trim())
                .addParams("parkId", ContractUtils.getParkId(TianJiaShouHuoXinXiActivity.this))
                .addParams("type", morendizhi)// 默认是1，否2
                .build()
                .execute(new StringCallback() {
                    @Override
                    public void onError(Call call, Exception e, int id) {
                        progressDialog.dismiss();
                    }

                    @Override
                    public void onResponse(String response, int id) {
                        ContractUtils.Code500(TianJiaShouHuoXinXiActivity.this,response);
                        System.out.println(response + "      添加收货地址的网络请求");
                        progressDialog.dismiss();

                        if(response.indexOf("200") != -1){
                            Gson gson = new Gson();
                            TianJiaShouHuoDiZhiEntity tianJiaShouHuoDiZhiEntity = gson.fromJson(response, TianJiaShouHuoDiZhiEntity.class);
                            if(morendizhi.equals("1")){
                                SharepreferencesDiZhi.saveDiZhi(TianJiaShouHuoXinXiActivity.this,tianJiaShouHuoDiZhiEntity.getResponse().getId());
                            }
//                            SharepreferencesDiZhi.saveDiZhi(TianJiaShouHuoXinXiActivity.this,tianJiaShouHuoDiZhiEntity.getResponse().getId());
                            Toast.makeText(TianJiaShouHuoXinXiActivity.this, "添加成功！", Toast.LENGTH_SHORT).show();
                            finish();
                        }else{
                            ContractUtils.Code400(TianJiaShouHuoXinXiActivity.this,response);
                        }
                    }
                });
    }
}
