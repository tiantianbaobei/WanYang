package com.wanyangproject.shouye;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebResourceResponse;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.ViewFlipper;

import com.bumptech.glide.Glide;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.google.gson.Gson;
import com.wanyangproject.R;
import com.wanyangproject.activity.HomeActivity;
import com.wanyangproject.activity.QiFuZhongXinYiJianFanKuiActivity;
import com.wanyangproject.activity.WenZhangXiangQingActivity;
import com.wanyangproject.activity.YuanQuZhaoShangFenLeiActivity;
import com.wanyangproject.activity.YuanQuZhaoShangXiangQingActivity;
import com.wanyangproject.activity.ZhengCeFaGuiXiangQingActivity;
import com.wanyangproject.adapter.MoKuaiFenLaiAdapter;
import com.wanyangproject.adapter.QiFuZhongXinAdapter;
import com.wanyangproject.adapter.QiFuZhongXinTuiGuangAdapter;
import com.wanyangproject.entity.QiFuZhongXinEntity;
import com.wanyangproject.entity.ShouYeBannerEntity;
import com.wanyangproject.entity.YuanQuZhaoShangXiangQingEntity;
import com.wanyangproject.fuwuactivity.FangKeDengJiActivity;
import com.wanyangproject.fuwuactivity.YuanQuTongZhiActivity;
import com.wanyangproject.my.MyBangDingActivity;
import com.wanyangproject.my.MyShouCangActivity;
import com.wanyangproject.my.QuanBuActivity;
import com.wanyangproject.my.RuZhuShenQingActivity;
import com.wanyangproject.my.SettingActivity;
import com.wanyangproject.utils.ContractUtils;
import com.wanyangproject.utils.FuWuTiaoZhuFragment;
import com.wanyangproject.utils.TiaoZhuanFragment;
import com.wanyangproject.utils.WebViewUtils;
import com.youth.banner.Banner;
import com.youth.banner.BannerConfig;
import com.youth.banner.listener.OnBannerListener;
import com.youth.banner.loader.ImageLoader;
import com.zhy.http.okhttp.OkHttpUtils;
import com.zhy.http.okhttp.callback.StringCallback;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.Call;

public class ZhiNengZhiZaoActivity extends AppCompatActivity implements ViewPager.OnPageChangeListener {


    @BindView(R.id.image_back)
    ImageView imageBack;
    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    //    @BindView(R.id.tv_jieshao)
//    TextView tvJieshao;
    @BindView(R.id.recyclerView_fenlei)
    RecyclerView recyclerViewFenlei;
    @BindView(R.id.tv_wenti_fankui)
    TextView tvWentiFankui;
    //    @BindView(R.id.viewPager)
//    ViewPager viewPager;
//    @BindView(R.id.img_one)
//    ImageView imgOne;
//    @BindView(R.id.img_two)
//    ImageView imgTwo;
//    @BindView(R.id.img_three)
//    ImageView imgThree;
    @BindView(R.id.recyclerView_tuiguang)
    RecyclerView recyclerViewTuiguang;
    @BindView(R.id.viewflipper)
    ViewFlipper viewflipper;
    @BindView(R.id.banner)
    Banner banner;
    @BindView(R.id.webView)
    WebView webView;
    @BindView(R.id.image)
    ImageView image;
    private QiFuZhongXinAdapter qiFuZhongXinAdapter;
    private MoKuaiFenLaiAdapter moKuaiFenLaiAdapter;
    private List<View> list = new ArrayList<>();
    private Boolean isOk = true;
    //    private YuanQuZhaoShangEntity yuanQuZhaoShangEntity;
    private TextView tv_scroll_one;
    private WebSettings mWebSettings;
    private String content;
    private YuanQuZhaoShangXiangQingEntity yuanQuZhaoShangXiangQingEntity;
    private QiFuZhongXinEntity qiFuZhongXinEntity;
    private QiFuZhongXinTuiGuangAdapter qiFuZhongXinTuiGuangAdapter;
    private ShouYeBannerEntity shouYeBannerEntity;
    private TiaoZhuanFragment tiaoZhuanFragment;
    private FuWuTiaoZhuFragment fuWuTiaoZhuFragment;
    private ProgressDialog progressDialog;
    private static Boolean isOK = false;



//    private Handler handler = new Handler() {
//        @Override
//        public void handleMessage(Message msg) {
//            super.handleMessage(msg);
//            switch (msg.what) {
//                case 1:
//                    viewPager.setCurrentItem(msg.arg1);
//                    break;
//                case 2:
//                    viewPager.setCurrentItem(msg.arg1);
//                    break;
//            }
//        }
//    };


    public void setFuWuTiaoZhuFragment(FuWuTiaoZhuFragment fuWuTiaoZhuFragment) {
        this.fuWuTiaoZhuFragment = fuWuTiaoZhuFragment;
    }

    public void setTiaoZhuanFragment(TiaoZhuanFragment tiaoZhuanFragment) {
        this.tiaoZhuanFragment = tiaoZhuanFragment;
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_zhi_neng_zhi_zao);
        ButterKnife.bind(this);
        if (Build.VERSION.SDK_INT >= 21) {
            View dview = getWindow().getDecorView();
            dview.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN | View.SYSTEM_UI_FLAG_LAYOUT_STABLE);
            getWindow().setStatusBarColor(Color.TRANSPARENT);
        }


        mWebSettings = webView.getSettings();
        mWebSettings.setJavaScriptEnabled(true);  //允许加载javascript
        webView.getSettings().setLayoutAlgorithm(WebSettings.LayoutAlgorithm.SINGLE_COLUMN);
        webView.getSettings().setLoadWithOverviewMode(true);
        mWebSettings.setSupportZoom(false);     //允许缩放
        mWebSettings.setBuiltInZoomControls(false); //原网页基础上缩放
        mWebSettings.setUseWideViewPort(false);   //任意比例缩放
//        mWebSettings.setUseWideViewPort(true); //将图片调整到适合webview的大小
//        mWebSettings.setLoadWithOverviewMode(true); // 缩放至屏幕的大小
//        mWebSettings.setTextSize(WebSettings.TextSize.SMALLER);


//        viewPager = (ViewPager) findViewById(R.id.viewPager);
//
//        View viewOne = LayoutInflater.from(ZhiNengZhiZaoActivity.this).inflate(R.layout.item_one, null);
//        View viewTwo = LayoutInflater.from(ZhiNengZhiZaoActivity.this).inflate(R.layout.item_two, null);
//        View viewThree = LayoutInflater.from(ZhiNengZhiZaoActivity.this).inflate(R.layout.item_three, null);
//
//        list.add(viewOne);
//        list.add(viewTwo);
//        list.add(viewThree);
//
//        MyAdapter adapter = new MyAdapter(list);
//        viewPager.setAdapter(adapter);
//
//        initThread();
//
//
//        viewPager.addOnPageChangeListener(this);

        //        企服中心的网络请求
        initQiFuZhongXinHttp();


//        企服中心banner的网络请求
        initBannerHttp();


        initView();

    }








    //    企服中心banner的网络请求
    private void initBannerHttp() {
        final ProgressDialog progressDialog = new ProgressDialog(ZhiNengZhiZaoActivity.this);
        progressDialog.setTitle("提示");
        progressDialog.setMessage("请等待...");
        progressDialog.setCancelable(false);
        progressDialog.show();
        OkHttpUtils.post()
                .url(ContractUtils.LOGIN_URL + "news/getBanner")
                .addHeader("token", ContractUtils.getTOKEN(ZhiNengZhiZaoActivity.this))
                .addParams("parkId", ContractUtils.getParkId(ZhiNengZhiZaoActivity.this))
                .addParams("typeId", "2")
                .build()
                .execute(new StringCallback() {
                    @Override
                    public void onError(Call call, Exception e, int id) {
                        progressDialog.dismiss();
                    }

                    @Override
                    public void onResponse(String response, int id) {
                        System.out.println(response + "         智能制造banner网络请求");
                        ContractUtils.Code500(ZhiNengZhiZaoActivity.this, response);
                        progressDialog.dismiss();
                        if (response.indexOf("200") != -1) {
                            Gson gson = new Gson();
                            shouYeBannerEntity = gson.fromJson(response, ShouYeBannerEntity.class);

                            //                        banner轮播
                            ArrayList<String> list = new ArrayList<String>();
                            for (int i = 0; i < shouYeBannerEntity.getResponse().size(); i++) {
//                                QiFuZhongXinEntity.ResponseBean.BannerArrBean bannerArrBean = qiFuZhongXinEntity.getResponse().getBannerArr().get(i);
//                            list.add(ContractUtils.YUANQUZHAOSHANG_PHOTO_URL + bannerArrBean.getLogo());
                                String image = shouYeBannerEntity.getResponse().get(i).getImage();
                                list.add(image);
                            }
                            banner.setBannerStyle(BannerConfig.CIRCLE_INDICATOR);
                            banner.setImageLoader(new MyImageLoader());
                            banner.setImages(list);
                            banner.setDelayTime(2000);
                            banner.setOnBannerListener(new OnBannerListener() {
                                @Override
                                public void OnBannerClick(int position) {
//                                    Intent intent = new Intent(ZhiNengZhiZaoActivity.this, YuanQuZhaoShangBannerXiangQingActivity.class);
//                                    intent.putExtra("id", qiFuZhongXinEntity.getResponse().getBannerArr().get(position).getId());
//                                    System.out.println(qiFuZhongXinEntity.getResponse().getBannerArr().get(position).getId() + "     点击banner详情的传送的id");
//                                    startActivity(intent);
                                    if (shouYeBannerEntity.getResponse().get(position).getHrefType().equals("0")) {
                                        Intent intenttongzhi = new Intent(ZhiNengZhiZaoActivity.this, YuanQuTongZhiActivity.class);
                                        intenttongzhi.putExtra("zhi","zhi");
                                        startActivity(intenttongzhi);

//                                        Intent intent1 = new Intent(ZhiNengZhiZaoActivity.this, ChanYeJinRongActivity.class);
//                                        startActivity(intent1);
                                    } else if (shouYeBannerEntity.getResponse().get(position).getHrefType().equals("1")) {
                                        Intent intent1 = new Intent(ZhiNengZhiZaoActivity.this, YuanQuShouCeActivity.class);
                                        startActivity(intent1);
                                    } else if (shouYeBannerEntity.getResponse().get(position).getHrefType().equals("2")) {
                                        Intent intent1 = new Intent(ZhiNengZhiZaoActivity.this, YuanQuQiYeActivity.class);
                                        startActivity(intent1);
                                    } else if (shouYeBannerEntity.getResponse().get(position).getHrefType().equals("3")) {
                                        Intent intent1 = new Intent(ZhiNengZhiZaoActivity.this, ZhengCeFaGuiActivity.class);
                                        startActivity(intent1);
                                    } else if (shouYeBannerEntity.getResponse().get(position).getHrefType().equals("4")) {
                                        Intent intent1 = new Intent(ZhiNengZhiZaoActivity.this, ZhiNengZhiZaoActivity.class);
                                        startActivity(intent1);
                                    } else if (shouYeBannerEntity.getResponse().get(position).getHrefType().equals("5")) {
                                        Intent intent1 = new Intent(ZhiNengZhiZaoActivity.this, YuanQuZhaoShangActivity.class);
                                        startActivity(intent1);
                                    } else if (shouYeBannerEntity.getResponse().get(position).getHrefType().equals("6")) {
                                        Intent intent2 = new Intent(ZhiNengZhiZaoActivity.this, HomeActivity.class);
                                        intent2.putExtra("luntan","luntan");
                                        startActivity(intent2);
                                    } else if (shouYeBannerEntity.getResponse().get(position).getHrefType().equals("7")) {
                                        Intent intent2 = new Intent(ZhiNengZhiZaoActivity.this, HomeActivity.class);
                                        intent2.putExtra("fuwu","fuwu");
                                        startActivity(intent2);
                                    } else if (shouYeBannerEntity.getResponse().get(position).getHrefType().equals("8")) {
                                        Intent intent = new Intent(ZhiNengZhiZaoActivity.this, HomeActivity.class);
                                        startActivity(intent);
                                    } else if (qiFuZhongXinEntity.getResponse().getUpTjArr().get(position).getHrefType().equals("9")) {
                                        if (qiFuZhongXinEntity.getResponse().getUpTjArr().get(position).getContent() != null) {
                                            Intent intent = new Intent(ZhiNengZhiZaoActivity.this, WenZhangXiangQingActivity.class);
                                            intent.putExtra("content", qiFuZhongXinEntity.getResponse().getUpTjArr().get(position).getContent());
                                            startActivity(intent);
                                        }
                                    } else if (shouYeBannerEntity.getResponse().get(position).getHrefType().equals("10")) {
                                        if (shouYeBannerEntity.getResponse().get(position).getContent() != null) {
                                            Intent intent = new Intent(ZhiNengZhiZaoActivity.this, YuanQuShengHuoActivity.class);
                                            startActivity(intent);
                                        }
                                    } else if (shouYeBannerEntity.getResponse().get(position).getHrefType().equals("11")) {
                                        if (shouYeBannerEntity.getResponse().get(position).getContent() != null) {
                                            return;
                                        }
                                    } else if (shouYeBannerEntity.getResponse().get(position).getHrefType().equals("12")) {
                                        if (shouYeBannerEntity.getResponse().get(position).getContent() != null) {
                                            Intent intent = new Intent(ZhiNengZhiZaoActivity.this, SettingActivity.class);
                                            startActivity(intent);
                                        }
                                    } else if (shouYeBannerEntity.getResponse().get(position).getHrefType().equals("13")) {
                                        if (shouYeBannerEntity.getResponse().get(position).getContent() != null) {
                                            Intent intent = new Intent(ZhiNengZhiZaoActivity.this, QiuZhiZhaoPinActivity.class);
                                            startActivity(intent);
                                        }
                                    } else if (shouYeBannerEntity.getResponse().get(position).getHrefType().equals("14")) {
                                        if (shouYeBannerEntity.getResponse().get(position).getContent() != null) {
                                            Intent intent = new Intent(ZhiNengZhiZaoActivity.this, ChanYeJinRongActivity.class);
                                            startActivity(intent);
                                        }
                                    } else if (shouYeBannerEntity.getResponse().get(position).getHrefType().equals("15")) {
                                        if (shouYeBannerEntity.getResponse().get(position).getContent() != null) {
                                            Intent intent = new Intent(ZhiNengZhiZaoActivity.this, YuanQuTongZhiActivity.class);
                                            startActivity(intent);
                                        }
                                    } else if (shouYeBannerEntity.getResponse().get(position).getHrefType().equals("16")) {
                                        if (shouYeBannerEntity.getResponse().get(position).getContent() != null) {
                                            Intent intent = new Intent(ZhiNengZhiZaoActivity.this, FangKeDengJiActivity.class);
                                            startActivity(intent);
                                        }
                                    } else if (shouYeBannerEntity.getResponse().get(position).getHrefType().equals("17")) {
                                        if (shouYeBannerEntity.getResponse().get(position).getContent() != null) {
                                            Intent intent = new Intent(ZhiNengZhiZaoActivity.this, QuanBuActivity.class);
                                            intent.putExtra("daizhifu","0");
                                            startActivity(intent);
                                        }
                                    } else if (shouYeBannerEntity.getResponse().get(position).getHrefType().equals("18")) {
                                        if (shouYeBannerEntity.getResponse().get(position).getContent() != null) {
                                            Intent intent = new Intent(ZhiNengZhiZaoActivity.this, MyShouCangActivity.class);
                                            startActivity(intent);
                                        }
                                    } else if (shouYeBannerEntity.getResponse().get(position).getHrefType().equals("19")) {
                                        if (shouYeBannerEntity.getResponse().get(position).getContent() != null) {
                                            Intent intent = new Intent(ZhiNengZhiZaoActivity.this, ShouHuoXinXiActivity.class);
                                            startActivity(intent);
                                        }
                                    } else if (shouYeBannerEntity.getResponse().get(position).getHrefType().equals("20")) {
                                        if (shouYeBannerEntity.getResponse().get(position).getContent() != null) {
                                            Intent intent = new Intent(ZhiNengZhiZaoActivity.this, RuZhuShenQingActivity.class);
                                            startActivity(intent);
                                        }
                                    } else if (shouYeBannerEntity.getResponse().get(position).getHrefType().equals("21")) {
                                        if (shouYeBannerEntity.getResponse().get(position).getContent() != null) {
                                            Intent intent = new Intent(ZhiNengZhiZaoActivity.this, MyBangDingActivity.class);
                                            startActivity(intent);
                                        }
                                    }
                                }
                            });
                            banner.start();
                        }
                    }
                });
    }





    private void initView() {
        webView.setWebViewClient(new WebViewClient() {
            /**
             * 拦截 url 跳转,在里边添加点击链接跳转或者操作
             */
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, WebResourceRequest request) {
                return WebViewUtils.shouldOverrideUrlLoading(view, request, ZhiNengZhiZaoActivity.this);
            }

            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                System.out.println("66666666666666666666");
                return WebViewUtils.shouldOverrideUrlLoading(view, url, ZhiNengZhiZaoActivity.this);

            }

            /**
             * 在开始加载网页时会回调
             */
            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                super.onPageStarted(view, url, favicon);
                System.out.println("111111111111111111");
            }

            /**
             * 加载错误的时候会回调，在其中可做错误处理，比如再请求加载一次，或者提示404的错误页面
             */
            @Override
            public void onReceivedError(WebView view, WebResourceRequest request, WebResourceError error) {
                super.onReceivedError(view, request, error);

            }

            /**
             * 在结束加载网页时会回调
             */
            @Override
            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(view, url);
                System.out.println("chenggongchenggongchenggong");
                if(progressDialog != null){
                    progressDialog.dismiss();
                }
            }

            /**
             * 当接收到https错误时，会回调此函数，在其中可以做错误处理
             */
            @Override
            public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
                super.onReceivedError(view, errorCode, description, failingUrl);
            }

            /**
             * 在每一次请求资源时，都会通过这个函数来回调
             */
            public WebResourceResponse shouldInterceptRequest(WebView view, String url) {
                return null;
            }

            @Override
            public void onPageCommitVisible(WebView view, String url) {
                super.onPageCommitVisible(view, url);
                if(progressDialog != null){
                    progressDialog.dismiss();
                }
            }
        });
    }



    //    企服中心的网络请求
    private void initQiFuZhongXinHttp() {
        progressDialog = new ProgressDialog(ZhiNengZhiZaoActivity.this);
        progressDialog.setTitle("提示");
        progressDialog.setMessage("请等待...");
        progressDialog.setCancelable(false);
        progressDialog.show();
        OkHttpUtils.post()
                .url(ContractUtils.LOGIN_URL + "news/getCategory")
                .addHeader("token", ContractUtils.getTOKEN(ZhiNengZhiZaoActivity.this))
                .addParams("parkId", ContractUtils.getParkId(ZhiNengZhiZaoActivity.this))
                .addParams("typeId", "2")//1:园区招商，2：企服中心，3:政策法规
                .build()
                .execute(new StringCallback() {
                    @Override
                    public void onError(Call call, Exception e, int id) {
                        progressDialog.dismiss();
                    }

                    @Override
                    public void onResponse(String response, int id) {
                        ContractUtils.Code500(ZhiNengZhiZaoActivity.this, response);
                        System.out.println(response + "      企服中心的网络请求");

                        if(response.indexOf("200") != -1){
                            Gson gson = new Gson();
                            qiFuZhongXinEntity = gson.fromJson(response, QiFuZhongXinEntity.class);

                            if(qiFuZhongXinEntity.getResponse().getBackground() == null){

                            }else{
                                Glide.with(ZhiNengZhiZaoActivity.this).load(qiFuZhongXinEntity.getResponse().getBackground()).into(image);
                            }


                            //        快捷入口
                            qiFuZhongXinAdapter = new QiFuZhongXinAdapter(ZhiNengZhiZaoActivity.this, qiFuZhongXinEntity.getResponse().getUpTjArr());
                            LinearLayoutManager manager = new LinearLayoutManager(ZhiNengZhiZaoActivity.this);
                            //设置为横向滑动
                            manager.setOrientation(LinearLayoutManager.HORIZONTAL);
                            recyclerView.setLayoutManager(manager);
                            recyclerView.setAdapter(qiFuZhongXinAdapter);

                            qiFuZhongXinAdapter.setXingQingClick(new QiFuZhongXinAdapter.XingQingClick() {
                                @Override
                                public void xiangqingClick(int position) {
//                                Intent intent = new Intent(ZhiNengZhiZaoActivity.this, YuanQuZhaoShangBannerXiangQingActivity.class);
//                                intent.putExtra("id", qiFuZhongXinEntity.getResponse().getUpTjArr().get(position).getId());
//                                System.out.println(qiFuZhongXinEntity.getResponse().getUpTjArr().get(position).getId() + "       点击上面滑动的传送的文章的id");
//                                startActivity(intent);


                                    if (qiFuZhongXinEntity.getResponse().getUpTjArr().get(position).getHrefType().equals("0")) {
                                        Intent intenttongzhi = new Intent(ZhiNengZhiZaoActivity.this, YuanQuTongZhiActivity.class);
                                        intenttongzhi.putExtra("zhi","zhi");
                                        startActivity(intenttongzhi);

//                                        Intent intent1 = new Intent(ZhiNengZhiZaoActivity.this, ChanYeJinRongActivity.class);
//                                        startActivity(intent1);
                                    } else if (qiFuZhongXinEntity.getResponse().getUpTjArr().get(position).getHrefType().equals("1")) {
                                        Intent intent1 = new Intent(ZhiNengZhiZaoActivity.this, YuanQuShouCeActivity.class);
                                        startActivity(intent1);
                                    } else if (qiFuZhongXinEntity.getResponse().getUpTjArr().get(position).getHrefType().equals("2")) {
                                        Intent intent1 = new Intent(ZhiNengZhiZaoActivity.this, YuanQuQiYeActivity.class);
                                        startActivity(intent1);
                                    } else if (qiFuZhongXinEntity.getResponse().getUpTjArr().get(position).getHrefType().equals("3")) {
                                        Intent intent1 = new Intent(ZhiNengZhiZaoActivity.this, ZhengCeFaGuiActivity.class);
                                        startActivity(intent1);
                                    } else if (qiFuZhongXinEntity.getResponse().getUpTjArr().get(position).getHrefType().equals("4")) {
                                        Intent intent1 = new Intent(ZhiNengZhiZaoActivity.this, ZhiNengZhiZaoActivity.class);
                                        startActivity(intent1);
                                    } else if (qiFuZhongXinEntity.getResponse().getUpTjArr().get(position).getHrefType().equals("5")) {
                                        Intent intent1 = new Intent(ZhiNengZhiZaoActivity.this, YuanQuZhaoShangActivity.class);
                                        startActivity(intent1);
                                    } else if (qiFuZhongXinEntity.getResponse().getUpTjArr().get(position).getHrefType().equals("6")) {
                                        Intent intent2 = new Intent(ZhiNengZhiZaoActivity.this, HomeActivity.class);
                                        intent2.putExtra("luntan","luntan");
                                        startActivity(intent2);
                                    } else if (qiFuZhongXinEntity.getResponse().getUpTjArr().get(position).getHrefType().equals("7")) {
                                        Intent intent2 = new Intent(ZhiNengZhiZaoActivity.this, HomeActivity.class);
                                        intent2.putExtra("fuwu","fuwu");
                                        startActivity(intent2);
                                    } else if (qiFuZhongXinEntity.getResponse().getUpTjArr().get(position).getHrefType().equals("8")) {
                                        Intent intent = new Intent(ZhiNengZhiZaoActivity.this, HomeActivity.class);
                                        startActivity(intent);
                                    } else if (qiFuZhongXinEntity.getResponse().getUpTjArr().get(position).getHrefType().equals("9")) {
                                        if (qiFuZhongXinEntity.getResponse().getUpTjArr().get(position).getContent() != null) {
                                            Intent intent = new Intent(ZhiNengZhiZaoActivity.this, WenZhangXiangQingActivity.class);
                                            intent.putExtra("content", qiFuZhongXinEntity.getResponse().getUpTjArr().get(position).getContent());
                                            startActivity(intent);
                                        }
                                    } else if (qiFuZhongXinEntity.getResponse().getUpTjArr().get(position).getHrefType().equals("10")) {
                                        if (qiFuZhongXinEntity.getResponse().getUpTjArr().get(position).getContent() != null) {
                                            Intent intent = new Intent(ZhiNengZhiZaoActivity.this, YuanQuShengHuoActivity.class);
                                            startActivity(intent);
                                        }
                                    } else if (qiFuZhongXinEntity.getResponse().getUpTjArr().get(position).getHrefType().equals("11")) {
                                        if (qiFuZhongXinEntity.getResponse().getUpTjArr().get(position).getContent() != null) {
                                            return;
                                        }
                                    } else if (qiFuZhongXinEntity.getResponse().getUpTjArr().get(position).getHrefType().equals("12")) {
                                        if (qiFuZhongXinEntity.getResponse().getUpTjArr().get(position).getContent() != null) {
                                            Intent intent = new Intent(ZhiNengZhiZaoActivity.this, SettingActivity.class);
                                            startActivity(intent);
                                        }
                                    } else if (qiFuZhongXinEntity.getResponse().getUpTjArr().get(position).getHrefType().equals("13")) {
                                        if (qiFuZhongXinEntity.getResponse().getUpTjArr().get(position).getContent() != null) {
                                            Intent intent = new Intent(ZhiNengZhiZaoActivity.this, QiuZhiZhaoPinActivity.class);
                                            startActivity(intent);
                                        }
                                    } else if (qiFuZhongXinEntity.getResponse().getUpTjArr().get(position).getHrefType().equals("14")) {
                                        if (qiFuZhongXinEntity.getResponse().getUpTjArr().get(position).getContent() != null) {
                                            Intent intent = new Intent(ZhiNengZhiZaoActivity.this, ChanYeJinRongActivity.class);
                                            startActivity(intent);
                                        }
                                    } else if (qiFuZhongXinEntity.getResponse().getUpTjArr().get(position).getHrefType().equals("15")) {
                                        if (qiFuZhongXinEntity.getResponse().getUpTjArr().get(position).getContent() != null) {
                                            Intent intent = new Intent(ZhiNengZhiZaoActivity.this, YuanQuTongZhiActivity.class);
                                            startActivity(intent);
                                        }
                                    } else if (qiFuZhongXinEntity.getResponse().getUpTjArr().get(position).getHrefType().equals("16")) {
                                        if (qiFuZhongXinEntity.getResponse().getUpTjArr().get(position).getContent() != null) {
                                            Intent intent = new Intent(ZhiNengZhiZaoActivity.this, FangKeDengJiActivity.class);
                                            startActivity(intent);
                                        }
                                    } else if (qiFuZhongXinEntity.getResponse().getUpTjArr().get(position).getHrefType().equals("17")) {
                                        if (qiFuZhongXinEntity.getResponse().getUpTjArr().get(position).getContent() != null) {
                                            Intent intent = new Intent(ZhiNengZhiZaoActivity.this, QuanBuActivity.class);
                                            intent.putExtra("daizhifu","0");
                                            startActivity(intent);
                                        }
                                    } else if (qiFuZhongXinEntity.getResponse().getUpTjArr().get(position).getHrefType().equals("18")) {
                                        if (qiFuZhongXinEntity.getResponse().getUpTjArr().get(position).getContent() != null) {
                                            Intent intent = new Intent(ZhiNengZhiZaoActivity.this, MyShouCangActivity.class);
                                            startActivity(intent);
                                        }
                                    } else if (qiFuZhongXinEntity.getResponse().getUpTjArr().get(position).getHrefType().equals("19")) {
                                        if (qiFuZhongXinEntity.getResponse().getUpTjArr().get(position).getContent() != null) {
                                            Intent intent = new Intent(ZhiNengZhiZaoActivity.this, ShouHuoXinXiActivity.class);
                                            startActivity(intent);
                                        }
                                    } else if (qiFuZhongXinEntity.getResponse().getUpTjArr().get(position).getHrefType().equals("20")) {
                                        if (qiFuZhongXinEntity.getResponse().getUpTjArr().get(position).getContent() != null) {
                                            Intent intent = new Intent(ZhiNengZhiZaoActivity.this, RuZhuShenQingActivity.class);
                                            startActivity(intent);
                                        }
                                    } else if (qiFuZhongXinEntity.getResponse().getUpTjArr().get(position).getHrefType().equals("21")) {
                                        if (qiFuZhongXinEntity.getResponse().getUpTjArr().get(position).getContent() != null) {
                                            Intent intent = new Intent(ZhiNengZhiZaoActivity.this, MyBangDingActivity.class);
                                            startActivity(intent);
                                        }
                                    }












//                                    if (qiFuZhongXinEntity.getResponse().getUpTjArr().get(position).getHrefType().equals("0")) {
//                                        Intent intent1 = new Intent(ZhiNengZhiZaoActivity.this, ChanYeJinRongActivity.class);
//                                        startActivity(intent1);
//                                    } else if (qiFuZhongXinEntity.getResponse().getUpTjArr().get(position).getHrefType().equals("1")) {
//                                        Intent intent1 = new Intent(ZhiNengZhiZaoActivity.this, YuanQuShouCeActivity.class);
//                                        startActivity(intent1);
//                                    } else if (qiFuZhongXinEntity.getResponse().getUpTjArr().get(position).getHrefType().equals("2")) {
//                                        Intent intent1 = new Intent(ZhiNengZhiZaoActivity.this, YuanQuQiYeActivity.class);
//                                        startActivity(intent1);
//                                    } else if (qiFuZhongXinEntity.getResponse().getUpTjArr().get(position).getHrefType().equals("3")) {
//                                        Intent intent1 = new Intent(ZhiNengZhiZaoActivity.this, ZhengCeFaGuiActivity.class);
//                                        startActivity(intent1);
//                                    } else if (qiFuZhongXinEntity.getResponse().getUpTjArr().get(position).getHrefType().equals("4")) {
//                                        Intent intent1 = new Intent(ZhiNengZhiZaoActivity.this, ZhiNengZhiZaoActivity.class);
//                                        startActivity(intent1);
//                                    } else if (qiFuZhongXinEntity.getResponse().getUpTjArr().get(position).getHrefType().equals("5")) {
//                                        Intent intent1 = new Intent(ZhiNengZhiZaoActivity.this, YuanQuZhaoShangActivity.class);
//                                        startActivity(intent1);
//                                    } else if (qiFuZhongXinEntity.getResponse().getUpTjArr().get(position).getHrefType().equals("6")) {
//                                        if (tiaoZhuanFragment != null) {
//                                            tiaoZhuanFragment.onClick();
//                                        }
//                                    } else if (qiFuZhongXinEntity.getResponse().getUpTjArr().get(position).getHrefType().equals("7")) {
//                                        if (fuWuTiaoZhuFragment != null) {
//                                            fuWuTiaoZhuFragment.onClick1();
//                                        }
//                                    } else if (qiFuZhongXinEntity.getResponse().getUpTjArr().get(position).getHrefType().equals("8")) {
//                                        Intent intent = new Intent(ZhiNengZhiZaoActivity.this, HomeActivity.class);
//                                        startActivity(intent);
//                                    } else if (qiFuZhongXinEntity.getResponse().getUpTjArr().get(position).getHrefType().equals("9")) {
//                                        if (qiFuZhongXinEntity.getResponse().getUpTjArr().get(position).getContent() != null) {
//                                            Intent intent = new Intent(ZhiNengZhiZaoActivity.this, WenZhangXiangQingActivity.class);
//                                            intent.putExtra("content", qiFuZhongXinEntity.getResponse().getUpTjArr().get(position).getContent());
//                                            startActivity(intent);
//                                        }
//                                    }


                                }
                            });


//                        企服中心介绍
                            if (qiFuZhongXinEntity.getResponse().getInfo().getContent() == null) {

                            } else {
                                //                            内容
                                content = qiFuZhongXinEntity.getResponse().getInfo().getContent();
                                webView.loadDataWithBaseURL("http://www.baidu.com", getNewContent(content), "text/html", "UTF-8", null);
                            }

                            for (int i = 0; i < qiFuZhongXinEntity.getResponse().getWordArr().size(); i++) {
                                View view = View.inflate(ZhiNengZhiZaoActivity.this, R.layout.scroll_tv, null);
                                tv_scroll_one = (TextView) view.findViewById(R.id.tv_scroll_one);
                                tv_scroll_one.setText(qiFuZhongXinEntity.getResponse().getWordArr().get(i).getTitle());
                                viewflipper.addView(view);
                            }
                            viewflipper.startFlipping();
                            viewflipper.setInAnimation(AnimationUtils.loadAnimation(ZhiNengZhiZaoActivity.this, R.anim.push_down_in));
                            viewflipper.setOutAnimation(AnimationUtils.loadAnimation(ZhiNengZhiZaoActivity.this, R.anim.push_down_out));

                            viewflipper.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View arg0) {
                                    // TODO Auto-generated method stub
//                                        startActivity(new Intent(MainActivity.this, Art.class));
//                                Intent intent = new Intent(getContext(), QiYeXiangQingActivity.class);
////                                    应该传企业的id   tiantian
//                                System.out.println(viewflipper.getDisplayedChild()+"              zzzzzzzzzzzzzzzzz");
//                                intent.putExtra("id",shouYeTuiJianQiYeEntity.getResponse().get(viewflipper.getDisplayedChild()).getEnterId());
//                                startActivity(intent);


//                                    Intent intent = new Intent(ZhiNengZhiZaoActivity.this, YuanQuZhaoShangXiangQingActivity.class);
                                    Intent intent = new Intent(ZhiNengZhiZaoActivity.this, YuanQuZhaoShangXiangQingActivity.class);
                                    intent.putExtra("id", qiFuZhongXinEntity.getResponse().getWordArr().get(viewflipper.getDisplayedChild()).getId());
                                    intent.putExtra("typeId","5");///0 园区手册   1 智能制造   2 产业金融   3 园区资讯   4 政策法规    5企服中心    6 园区招商
                                    System.out.println(qiFuZhongXinEntity.getResponse().getWordArr().get(viewflipper.getDisplayedChild()).getId() + "     点击banner详情的传送的id");
                                    startActivity(intent);


//                                if(qiFuZhongXinEntity.getResponse().getWordArr().get(viewflipper.getDisplayedChild()).getHrefType() != null){
//                                    if (qiFuZhongXinEntity.getResponse().getWordArr().get(viewflipper.getDisplayedChild()).getHrefType().equals("0")){
//                                        Intent intent1 = new Intent(ZhiNengZhiZaoActivity.this,ChanYeJinRongActivity.class);
//                                        startActivity(intent1);
//                                    }else if(qiFuZhongXinEntity.getResponse().getWordArr().get(viewflipper.getDisplayedChild()).getHrefType().equals("1")){
//                                        Intent intent1 = new Intent(ZhiNengZhiZaoActivity.this,YuanQuShouCeActivity.class);
//                                        startActivity(intent1);
//                                    }else if(qiFuZhongXinEntity.getResponse().getWordArr().get(viewflipper.getDisplayedChild()).getHrefType().equals("2")){
//                                        Intent intent1 = new Intent(ZhiNengZhiZaoActivity.this,YuanQuQiYeActivity.class);
//                                        startActivity(intent1);
//                                    }else if(qiFuZhongXinEntity.getResponse().getWordArr().get(viewflipper.getDisplayedChild()).getHrefType().equals("3")){
//                                        Intent intent1 = new Intent(ZhiNengZhiZaoActivity.this,ZhengCeFaGuiActivity.class);
//                                        startActivity(intent1);
//                                    }else if(qiFuZhongXinEntity.getResponse().getWordArr().get(viewflipper.getDisplayedChild()).getHrefType().equals("4")){
//                                        Intent intent1 = new Intent(ZhiNengZhiZaoActivity.this,ZhiNengZhiZaoActivity.class);
//                                        startActivity(intent1);
//                                    }else if(qiFuZhongXinEntity.getResponse().getWordArr().get(viewflipper.getDisplayedChild()).getHrefType().equals("5")){
//                                        Intent intent1 = new Intent(ZhiNengZhiZaoActivity.this,YuanQuZhaoShangActivity.class);
//                                        startActivity(intent1);
//                                    }else if(qiFuZhongXinEntity.getResponse().getWordArr().get(viewflipper.getDisplayedChild()).getHrefType().equals("6")) {
//                                        if (tiaoZhuanFragment != null) {
//                                            tiaoZhuanFragment.onClick();
//                                        }
//                                    } else if (qiFuZhongXinEntity.getResponse().getWordArr().get(viewflipper.getDisplayedChild()).getHrefType().equals("7")) {
//                                        if (fuWuTiaoZhuFragment != null) {
//                                            fuWuTiaoZhuFragment.onClick1();
//                                        }
//                                    } else if (qiFuZhongXinEntity.getResponse().getWordArr().get(viewflipper.getDisplayedChild()).getHrefType().equals("8")) {
//                                        Intent intent9 = new Intent(ZhiNengZhiZaoActivity.this, HomeActivity.class);
//                                        startActivity(intent9);
//                                    }  else if(qiFuZhongXinEntity.getResponse().getWordArr().get(viewflipper.getDisplayedChild()).getHrefType().equals("9")){
//                                        if(qiFuZhongXinEntity.getResponse().getWordArr().get(viewflipper.getDisplayedChild()).getContent() != null){
//                                            Intent intent0 = new Intent(ZhiNengZhiZaoActivity.this, WenZhangXiangQingActivity.class);
//                                            intent.putExtra("content",qiFuZhongXinEntity.getResponse().getWordArr().get(viewflipper.getDisplayedChild()).getContent());
//                                            startActivity(intent);
//                                        }
//                                    }
//                                }
                                }
                            });


//                        //                        banner轮播
//                        ArrayList<String> list = new ArrayList<String>();
//                        for (int i = 0; i < qiFuZhongXinEntity.getResponse().getBannerArr().size(); i++) {
//                            QiFuZhongXinEntity.ResponseBean.BannerArrBean bannerArrBean = qiFuZhongXinEntity.getResponse().getBannerArr().get(i);
////                            list.add(ContractUtils.YUANQUZHAOSHANG_PHOTO_URL + bannerArrBean.getLogo());
//                            list.add(bannerArrBean.getImage());
//                        }
//                        banner.setBannerStyle(BannerConfig.CIRCLE_INDICATOR);
//                        banner.setImageLoader(new MyImageLoader());
//                        banner.setImages(list);
//                        banner.setDelayTime(2000);
//                        banner.setOnBannerListener(new OnBannerListener() {
//                            @Override
//                            public void OnBannerClick(int position) {
//                                Intent intent = new Intent(ZhiNengZhiZaoActivity.this, YuanQuZhaoShangBannerXiangQingActivity.class);
//                                intent.putExtra("id", qiFuZhongXinEntity.getResponse().getBannerArr().get(position).getId());
//                                System.out.println(qiFuZhongXinEntity.getResponse().getBannerArr().get(position).getId() + "     点击banner详情的传送的id");
//                                startActivity(intent);
//                            }
//                        });
//                        banner.start();


                            //        模块分类
                            moKuaiFenLaiAdapter = new MoKuaiFenLaiAdapter(ZhiNengZhiZaoActivity.this, qiFuZhongXinEntity.getResponse().getCategory());
                            LinearLayoutManager manager1 = new LinearLayoutManager(ZhiNengZhiZaoActivity.this);
                            recyclerViewFenlei.setLayoutManager(manager1);
                            recyclerViewFenlei.setAdapter(moKuaiFenLaiAdapter);

                            moKuaiFenLaiAdapter.setFenLeiXiangQingClick(new MoKuaiFenLaiAdapter.FenLeiXiangQingClick() {
                                @Override
                                public void fenleixiangqingClick(String id) {
//                                Intent intent = new Intent(ZhiNengZhiZaoActivity.this, QiFuZhongXinFenLeiActivity.class);
                                    Intent intent = new Intent(ZhiNengZhiZaoActivity.this, YuanQuZhaoShangFenLeiActivity.class);
                                    intent.putExtra("id", id);
                                    intent.putExtra("typeid", "3");
                                    intent.putExtra("zixuntypeid","5");//0 园区手册   1 智能制造   2 产业金融   3 园区资讯   4 政策法规    5企服中心    6 园区招商
                                    System.out.println(id + "     企服中心服务分类传送的id");
                                    startActivity(intent);
                                }
                            });


                            qiFuZhongXinEntity = gson.fromJson(response, QiFuZhongXinEntity.class);

                            List<QiFuZhongXinEntity.ResponseBean.TjIndexBean> tjIndex = qiFuZhongXinEntity.getResponse().getTjIndex();

                            for (QiFuZhongXinEntity.ResponseBean.TjIndexBean bean : tjIndex) {
                                if (bean.getImgArr().size() == 0) {
                                    bean.setItemType(0);
                                } else if (bean.getImgArr().size() == 1) {
                                    bean.setItemType(1);
                                } else if (bean.getImgArr().size() == 2) {
                                    bean.setItemType(2);
                                } else if (bean.getImgArr().size() == 3) {
                                    bean.setItemType(3);
                                } else if (bean.getImgArr().size() == 6) {
                                    bean.setItemType(6);
                                } else if (bean.getImgArr().size() >= 9) {
                                    bean.setItemType(9);
                                }
                            }

                            qiFuZhongXinTuiGuangAdapter = new QiFuZhongXinTuiGuangAdapter(qiFuZhongXinEntity.getResponse().getTjIndex());
                            LinearLayoutManager manager0 = new LinearLayoutManager(ZhiNengZhiZaoActivity.this);
                            recyclerViewTuiguang.setLayoutManager(manager0);
                            recyclerViewTuiguang.setAdapter(qiFuZhongXinTuiGuangAdapter);

                            qiFuZhongXinTuiGuangAdapter.setOnItemChildClickListener(new BaseQuickAdapter.OnItemChildClickListener() {
                                @Override
                                public void onItemChildClick(BaseQuickAdapter adapter, View view, int position) {
                                    Intent intent = new Intent(ZhiNengZhiZaoActivity.this, YuanQuZhaoShangXiangQingActivity.class);
                                    intent.putExtra("id", qiFuZhongXinEntity.getResponse().getTjIndex().get(position).getId());
                                    intent.putExtra("typeId", "5");//0 园区手册   1 智能制造   2 产业金融   3 园区资讯   4 政策法规    5企服中心    6 园区招商
                                    startActivity(intent);
                                }
                            });
                        }

                    }
                });
    }


    public class MyImageLoader extends ImageLoader {

        @Override
        public void displayImage(Context context, Object path, ImageView imageView) {
            Glide.with(context).load(path).into(imageView);
        }
    }


//    private void initView() {
////        快捷入口
//        qiFuZhongXinAdapter = new QiFuZhongXinAdapter(this);
//        LinearLayoutManager manager = new LinearLayoutManager(this);
//        //设置为横向滑动
//        manager.setOrientation(LinearLayoutManager.HORIZONTAL);
//        recyclerView.setLayoutManager(manager);
//        recyclerView.setAdapter(qiFuZhongXinAdapter);


////        模块分类
//        moKuaiFenLaiAdapter = new MoKuaiFenLaiAdapter(this);
//        LinearLayoutManager manager1 = new LinearLayoutManager(this);
//        recyclerViewFenlei.setLayoutManager(manager1);
//        recyclerViewFenlei.setAdapter(moKuaiFenLaiAdapter);
//    }


//    private void initThread() {
//        new Thread() {
//            @Override
//            public void run() {
//                super.run();
//                try {
//                    while (isOk) {
//                        Thread.sleep(3000);
//                        Message message = Message.obtain();
//                        int currentItem = viewPager.getCurrentItem();
//                        if (currentItem == 2) {
//                            currentItem = 0;
//                            message.what = 1;
//                            message.arg1 = currentItem;
//                            handler.sendMessage(message);
//                        } else {
//                            message.what = 2;
//                            message.arg1 = currentItem + 1;
//                            handler.sendMessage(message);
//                        }
//                    }
//                } catch (InterruptedException e) {
//                    e.printStackTrace();
//                }
//            }
//        }.start();
//    }


    @OnClick({R.id.image_back, R.id.recyclerView, R.id.recyclerView_fenlei, R.id.tv_wenti_fankui, R.id.recyclerView_tuiguang})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.image_back:
                finish();
                break;
            case R.id.recyclerView:
                break;
//            case R.id.tv_jieshao:
//                break;
            case R.id.recyclerView_fenlei:
                break;
            case R.id.tv_wenti_fankui:
                Intent intent = new Intent(ZhiNengZhiZaoActivity.this, QiFuZhongXinYiJianFanKuiActivity.class);
                startActivity(intent);
                break;
//            case R.id.viewPager:
//                break;
//            case R.id.img_one:
//                break;
//            case R.id.img_two:
//                break;
//            case R.id.img_three:
//                break;
            case R.id.recyclerView_tuiguang:
                break;
        }
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {

    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }


    /**
     * 将html文本内容中包含img标签的图片，宽度变为屏幕宽度，高度根据宽度比例自适应
     **/
    public static String getNewContent(String htmltext) {

//
//        if(isOK == false){
//            return htmltext;
//        }
//
//        isOK = true;
        try {
            Document doc = Jsoup.parse(htmltext);
            Elements elements = doc.getElementsByTag("img");
            for (Element element : elements) {
                element.attr("width", "100%").attr("height", "auto");
            }

            return doc.toString();
        } catch (Exception e) {
            return htmltext;
        }
    }
}
