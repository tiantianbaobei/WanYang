package com.wanyangproject.shouye;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.Gravity;
import android.view.View;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebResourceResponse;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.wanyangproject.R;
import com.wanyangproject.entity.ShangJiaHuoDongXiangQingEntity;
import com.wanyangproject.my.ShangJiaShangPinGuanLiActivity;
import com.wanyangproject.my.WoDeZiLiaoActivity;
import com.wanyangproject.popuwindow.SharePopupWindow;
import com.wanyangproject.popuwindow.TakePhotoPopWin;
import com.wanyangproject.utils.ContractUtils;
import com.wanyangproject.utils.ShareUtils;
import com.wanyangproject.utils.WebViewUtils;
import com.zhy.http.okhttp.OkHttpUtils;
import com.zhy.http.okhttp.callback.StringCallback;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.Call;
import cn.sharesdk.framework.PlatformActionListener;


public class ShangJiaHuoDongXiangQingActivity extends AppCompatActivity {

    @BindView(R.id.image_back)
    ImageView imageBack;
    @BindView(R.id.image_shoucang)
    ImageView imageShoucang;
    @BindView(R.id.image_fenxiang)
    ImageView imageFenxiang;
    @BindView(R.id.tv_title)
    TextView tvTitle;
    @BindView(R.id.tv_time)
    TextView tvTime;
    @BindView(R.id.webView)
    WebView webView;
    private String huodongId;
    private WebSettings mWebSettings;
    private String content;
    private SharePopupWindow sharePopupWindow;
    private View mView;
    private Context context;
    private TextView tv_weixin_haoyou,tv_qq_haoyou,tv_weixin_pengyouquan,tv_xinlang_weibo,tv_quxiao;
    private ProgressDialog progressDialog;
    private static Boolean isOK = false;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shang_jia_huo_dong_xiang_qing);
        ButterKnife.bind(this);

        Intent intent = getIntent();
        huodongId = intent.getStringExtra("id");
        System.out.println(huodongId + "   接收商家活动的活动id");

        mWebSettings = webView.getSettings();
        mWebSettings.setJavaScriptEnabled(true);  //允许加载javascript
        webView.getSettings().setLayoutAlgorithm(WebSettings.LayoutAlgorithm.SINGLE_COLUMN);
        webView.getSettings().setLoadWithOverviewMode(true);
        mWebSettings.setSupportZoom(false);     //允许缩放
        mWebSettings.setBuiltInZoomControls(false); //原网页基础上缩放
        mWebSettings.setUseWideViewPort(false);   //任意比例缩放
//        mWebSettings.setUseWideViewPort(true); //将图片调整到适合webview的大小
//        mWebSettings.setLoadWithOverviewMode(true); // 缩放至屏幕的大小
//        mWebSettings.setTextSize(WebSettings.TextSize.SMALLEST);

//        商家活动详情的网络请求
        initShangJiaHuoDongXiangQingHttp();


        initView();

    }

    private void initView() {
        webView.setWebViewClient(new WebViewClient() {
            /**
             * 拦截 url 跳转,在里边添加点击链接跳转或者操作
             */
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, WebResourceRequest request) {
                return WebViewUtils.shouldOverrideUrlLoading(view,request,ShangJiaHuoDongXiangQingActivity.this);
            }


            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                System.out.println("66666666666666666666");
                return WebViewUtils.shouldOverrideUrlLoading(view,url,ShangJiaHuoDongXiangQingActivity.this);

            }




            /**
             * 在开始加载网页时会回调
             */
            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                super.onPageStarted(view, url, favicon);
                System.out.println("111111111111111111");
            }

            /**
             * 加载错误的时候会回调，在其中可做错误处理，比如再请求加载一次，或者提示404的错误页面
             */
            @Override
            public void onReceivedError(WebView view, WebResourceRequest request, WebResourceError error) {
                super.onReceivedError(view, request, error);

            }


            /**
             * 在结束加载网页时会回调
             */
            @Override
            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(view, url);
                System.out.println("chenggongchenggongchenggong");

                if(progressDialog != null){
                    progressDialog.dismiss();
                }

            }

            /**
             * 当接收到https错误时，会回调此函数，在其中可以做错误处理
             */
            @Override
            public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
                super.onReceivedError(view, errorCode, description, failingUrl);
            }

            /**
             * 在每一次请求资源时，都会通过这个函数来回调
             */
            public WebResourceResponse shouldInterceptRequest(WebView view, String url) {
                return null;
            }


            @Override
            public void onPageCommitVisible(WebView view, String url) {
                super.onPageCommitVisible(view, url);

                if(progressDialog != null){
                    progressDialog.dismiss();
                }


            }
        });
    }









    //    商家活动详情的网络请求
//    tiantian
//    .addParams("parkId",ContractUtils.getParkId(ShangJiaHuoDongXiangQingActivity.this))
//      .addParams("Preferences_id",huodongId)
    private void initShangJiaHuoDongXiangQingHttp() {
        progressDialog = new ProgressDialog(ShangJiaHuoDongXiangQingActivity.this);
        progressDialog.setTitle("提示");
        progressDialog.setMessage("请等待...");
        progressDialog.setCancelable(false);
        progressDialog.show();
        OkHttpUtils.post()
                .url(ContractUtils.LOGIN_URL + "Life/preferential")
                .addHeader("token", ContractUtils.getTOKEN(ShangJiaHuoDongXiangQingActivity.this))
                .addParams("parkId", ContractUtils.getParkId(ShangJiaHuoDongXiangQingActivity.this))
                .addParams("Preferences_id", huodongId)
                .build()
                .execute(new StringCallback() {
                    @Override
                    public void onError(Call call, Exception e, int id) {
                        progressDialog.dismiss();
                    }

                    @Override
                    public void onResponse(String response, int id) {
                        ContractUtils.Code500(ShangJiaHuoDongXiangQingActivity.this,response);
                        System.out.println(response + "     商家活动详情的网络请求");
                        Gson gson = new Gson();
                        ShangJiaHuoDongXiangQingEntity shangJiaHuoDongXiangQingEntity = gson.fromJson(response, ShangJiaHuoDongXiangQingEntity.class);
                        if(shangJiaHuoDongXiangQingEntity.getCode() == 200){
                            if(shangJiaHuoDongXiangQingEntity.getResponse().getShopPreferences().getAdd_time() == null){

                            }else{  //    时间
                                tvTime.setText(shangJiaHuoDongXiangQingEntity.getResponse().getShopPreferences().getAdd_time());
                            }


                            if(shangJiaHuoDongXiangQingEntity.getResponse().getShopPreferences().getTitle() == null){

                            }else{   //   标题
                                tvTitle.setText(shangJiaHuoDongXiangQingEntity.getResponse().getShopPreferences().getTitle());
                            }

                            if(shangJiaHuoDongXiangQingEntity.getResponse().getShopPreferences().getContent() == null){

                            }else{
                                //                            内容
                                content = shangJiaHuoDongXiangQingEntity.getResponse().getShopPreferences().getContent();
                                webView.loadDataWithBaseURL("http://www.baidu.com", getNewContent(content),"text/html", "UTF-8", null);
                            }
                        }
                    }
                });
    }


    @OnClick({R.id.image_back, R.id.image_shoucang, R.id.image_fenxiang,R.id.tv_title, R.id.tv_time, R.id.webView})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.image_back:
                finish();
                break;
//            收藏
            case R.id.image_shoucang:
                break;
//            分享
            case R.id.image_fenxiang:
//                Object itemsOnClick = null;
//                sharePopupWindow = new SharePopupWindow(ShangJiaHuoDongXiangQingActivity.this,(View.OnClickListener) itemsOnClick);
//                sharePopupWindow.showAtLocation(findViewById(R.id.relative_Layout), Gravity.BOTTOM | Gravity.CENTER_HORIZONTAL, 0, 0);
//                AlertDialog alertDialog = new AlertDialog.Builder(ShangJiaHuoDongXiangQingActivity.this).create();
//                alertDialog.show();
//                alertDialog.getWindow().setContentView(R.layout.share_alertdialog);
////                微信好友
//                alertDialog.getWindow().findViewById(R.id.tv_weixin_haoyou).setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View view) {
//                        ShareUtils.shareWechat(share_title,share_url,share_desc,share_img,platformActionListener);
//                    }
//                });
//
////                微信朋友圈
//                alertDialog.getWindow().findViewById(R.id.tv_weixin_pengyouquan).setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View view) {
//
//                    }
//                });
//
////                QQ好友
//                alertDialog.getWindow().findViewById(R.id.tv_qq_haoyou).setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View view) {
//
//                    }
//                });
//
////                新浪微博
//                alertDialog.getWindow().findViewById(R.id.tv_xinlang_weibo).setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View view) {
//
//                    }
//                });

                break;
            case R.id.tv_title:
                break;
            case R.id.tv_time:
                break;
            case R.id.webView:
                break;
        }
    }



    /**
     * 将html文本内容中包含img标签的图片，宽度变为屏幕宽度，高度根据宽度比例自适应
     **/
    public static String getNewContent(String htmltext){

//        if(isOK == false){
//            return htmltext;
//        }
//
//        isOK = true;
        try {
            Document doc= Jsoup.parse(htmltext);
            Elements elements=doc.getElementsByTag("img");
            for (Element element : elements) {
                element.attr("width","100%").attr("height","auto");
            }

            return doc.toString();
        } catch (Exception e) {
            return htmltext;
        }
    }
}
