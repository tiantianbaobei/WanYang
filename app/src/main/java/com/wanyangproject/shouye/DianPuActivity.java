package com.wanyangproject.shouye;

import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.androidkun.xtablayout.XTabLayout;
import com.wanyangproject.R;
import com.wanyangproject.adapter.TabLayoutAdapter;
import com.wanyangproject.shouyefragment.ShouYePaiXuFragment;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class DianPuActivity extends AppCompatActivity {

    @BindView(R.id.image_back)
    ImageView imageBack;
    //    @BindView(R.id.tabLayout)
//    TabLayout tabLayout;
    @BindView(R.id.viewPager)
    ViewPager viewPager;
    @BindView(R.id.tabLayout)
    XTabLayout tabLayout;
    @BindView(R.id.tv_title)
    TextView tvTitle;
    //    @BindView(R.id.recyclerView)
//    RecyclerView recyclerView;
    private ShouYePaiXuFragment shouYePaiXuFragment;
    private String id;
    List<Fragment> fragments = new ArrayList<>();
    private String name;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dian_pu);
        ButterKnife.bind(this);
        if (Build.VERSION.SDK_INT >= 21) {
            View dview = getWindow().getDecorView();
            dview.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN | View.SYSTEM_UI_FLAG_LAYOUT_STABLE);
            getWindow().setStatusBarColor(Color.TRANSPARENT);
        }


        Intent intent = getIntent();
        id = intent.getStringExtra("id");
        name = intent.getStringExtra("name");
        if(tvTitle.equals("")){
            tvTitle.setText("");
        }else{
            tvTitle.setText(name);
        }

        System.out.println(name + "         接收name");
        System.out.println(id + "    ......接收分类的id");

        initViewPager();
    }


    private void initViewPager() {
        fragments.add(new ShouYePaiXuFragment());
        fragments.add(new ShouYePaiXuFragment());
        fragments.add(new ShouYePaiXuFragment());

        for (int i = 0; i < fragments.size(); i++) {
            ShouYePaiXuFragment jj = (ShouYePaiXuFragment) fragments.get(i);
//            tiantian
            jj.setStr(id);
            jj.setPaixu(i);
        }

        // 创建ViewPager适配器
        TabLayoutAdapter tabLayoutAdapter = new TabLayoutAdapter(getSupportFragmentManager());
        tabLayoutAdapter.setFragments(fragments);
        // 给ViewPager设置适配器
        viewPager.setAdapter(tabLayoutAdapter);
//        // TabLayout 指示器 (记得自己手动创建4个Fragment,注意是 app包下的Fragment 还是 V4包下的 Fragment)
        tabLayout.addTab(tabLayout.newTab());
        tabLayout.addTab(tabLayout.newTab());
        tabLayout.addTab(tabLayout.newTab());
//        // 使用 TabLayout 和 ViewPager 相关联
        tabLayout.setupWithViewPager(viewPager);
        // TabLayout指示器添加文本
        tabLayout.getTabAt(0).setText("综合排序");
        tabLayout.getTabAt(1).setText("销量最高");
        tabLayout.getTabAt(2).setText("评价最高");


    }

    @OnClick({R.id.image_back, R.id.tabLayout})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.image_back:
                finish();
                break;
            case R.id.tabLayout:
                break;
        }
    }
}
