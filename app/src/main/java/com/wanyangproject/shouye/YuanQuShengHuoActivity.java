package com.wanyangproject.shouye;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.google.gson.Gson;
import com.wanyangproject.R;
import com.wanyangproject.activity.HomeActivity;
import com.wanyangproject.activity.WenZhangXiangQingActivity;
import com.wanyangproject.adapter.BannerAdapter;
import com.wanyangproject.adapter.HengAdapter;
import com.wanyangproject.adapter.YuanQuShengHuoShangJiaTuiJianAdapter;
import com.wanyangproject.adapter.YuanQuShengHuoShangJiaWaiMaiAdapter;
import com.wanyangproject.entity.ShouYeBannerEntity;
import com.wanyangproject.entity.YuanQuShengHuoShangJiaYouHUiHuoDongEntity;
import com.wanyangproject.entity.YuanQuShengHuoTuiJianShangJiaEntity;
import com.wanyangproject.fragment.ShouYeFragment;
import com.wanyangproject.fuwuactivity.FangKeDengJiActivity;
import com.wanyangproject.fuwuactivity.YuanQuTongZhiActivity;
import com.wanyangproject.my.MyBangDingActivity;
import com.wanyangproject.my.MyShouCangActivity;
import com.wanyangproject.my.QuanBuActivity;
import com.wanyangproject.my.RuZhuShenQingActivity;
import com.wanyangproject.my.SettingActivity;
import com.wanyangproject.utils.ContractUtils;
import com.wanyangproject.utils.FuWuTiaoZhuFragment;
import com.wanyangproject.utils.TiaoZhuanFragment;
import com.zhy.http.okhttp.OkHttpUtils;
import com.zhy.http.okhttp.callback.StringCallback;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;
import okhttp3.Call;

public class YuanQuShengHuoActivity extends AppCompatActivity implements ViewPager.OnPageChangeListener{


    @BindView(R.id.recyclerView_shangjiahuodong)
    RecyclerView recyclerViewShangjiahuodong;
    @BindView(R.id.recyclerView_tuijianshangjia)
    RecyclerView recyclerViewTuijianshangjia;
    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    @BindView(R.id.image_back)
    ImageView imageBack;
    @BindView(R.id.image_shenghuo_sheshi)
    CircleImageView imageShenghuoSheshi;
    @BindView(R.id.tv_shenghuo_sheshi)
    TextView tvShenghuoSheshi;
    @BindView(R.id.viewPager)
    ViewPager viewPager;
    @BindView(R.id.recyclerView_banner)
    RecyclerView recyclerViewBanner;
    private YuanQuShengHuoShangJiaWaiMaiAdapter yuanQuShengHuoShangJiaWaiMaiAdapter;
    private YuanQuShengHuoShangJiaTuiJianAdapter yuanQuShengHuoShangJiaTuiJianAdapter;
    private YuanQuShengHuoTuiJianShangJiaEntity yuanQuShengHuoTuiJianShangJiaEntity;
    private HengAdapter hengAdapter;
    private ShouYeBannerEntity shouYeBannerEntity;
    private List<View> list = new ArrayList<>();
    private List<String> contentList = new ArrayList<>();
    private BannerAdapter1 adapter1;
    private TiaoZhuanFragment tiaoZhuanFragment;
    private FuWuTiaoZhuFragment fuWuTiaoZhuFragment;
    private LinearLayoutManager manager;
    private Boolean isOk = true;
    private YuanQuShengHuoShangJiaYouHUiHuoDongEntity yuanQuShengHuoShangJiaYouHUiHuoDongEntity;



    public void setFuWuTiaoZhuFragment(FuWuTiaoZhuFragment fuWuTiaoZhuFragment) {
        this.fuWuTiaoZhuFragment = fuWuTiaoZhuFragment;
    }

    public void setTiaoZhuanFragment(TiaoZhuanFragment tiaoZhuanFragment) {
        this.tiaoZhuanFragment = tiaoZhuanFragment;
    }




    private Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            switch (msg.what) {
                case 1:
                    viewPager.setCurrentItem(msg.arg1);
                    break;
                case 2:
                    viewPager.setCurrentItem(msg.arg1);
                    break;
            }
            if (adapter1 != null) {
                adapter1.notifyDataSetChanged();
            }
        }
    };


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_yuan_qu_sheng_huo);
//        setContentView(R.layout.activity_wai_mai_aaaa);
        ButterKnife.bind(this);



//        园区生活界面的网络请求
        initTuiJianShangJiaHttp();



//        首页总banner网络请求
        initBannerHttp();


//        商家优惠活动的接口的网络请求
        initYouHuiHuoDongHttp();


        initThread();
        viewPager.addOnPageChangeListener(this);

    }




//    商家优惠活动的接口的网络请求
    private void initYouHuiHuoDongHttp() {
        OkHttpUtils.post()
                .url(ContractUtils.LOGIN_URL+"index/shopinfo")
                .addHeader("token",ContractUtils.getTOKEN(YuanQuShengHuoActivity.this))
                .build()
                .execute(new StringCallback() {
                    @Override
                    public void onError(Call call, Exception e, int id) {

                    }

                    @Override
                    public void onResponse(String response, int id) {
                        System.out.println(response+"               商家优惠活动的接口的网络请求 ");
                        ContractUtils.Code400(YuanQuShengHuoActivity.this,response);
                        if(response.indexOf("200") != -1){
                            Gson gson = new Gson();
                            yuanQuShengHuoShangJiaYouHUiHuoDongEntity = gson.fromJson(response, YuanQuShengHuoShangJiaYouHUiHuoDongEntity.class);
                            //                        商家活动
                            yuanQuShengHuoShangJiaWaiMaiAdapter = new YuanQuShengHuoShangJiaWaiMaiAdapter(YuanQuShengHuoActivity.this, yuanQuShengHuoShangJiaYouHUiHuoDongEntity.getResponse());
                            LinearLayoutManager manager2 = new LinearLayoutManager(YuanQuShengHuoActivity.this);
                            recyclerViewShangjiahuodong.setLayoutManager(manager2);
                            recyclerViewShangjiahuodong.setAdapter(yuanQuShengHuoShangJiaWaiMaiAdapter);

                            yuanQuShengHuoShangJiaWaiMaiAdapter.setXiangQingClick(new YuanQuShengHuoShangJiaWaiMaiAdapter.XiangQingClick() {
                                @Override
                                public void xiangqingClick(int position) {
                                    Intent intent = new Intent(YuanQuShengHuoActivity.this, ShangJiaHuoDongXiangQingActivity.class);
                                    intent.putExtra("id", yuanQuShengHuoShangJiaYouHUiHuoDongEntity.getResponse().get(position).getId());
                                    System.out.println(yuanQuShengHuoShangJiaYouHUiHuoDongEntity.getResponse().get(position).getId() + "  传送商家活动的活动id");
                                    startActivity(intent);
                                }
                            });
                        }
                    }
                });
    }


    //    首页banner图的网络请求
    private void initBannerHttp() {
        final ProgressDialog progressDialog = new ProgressDialog(YuanQuShengHuoActivity.this);
        progressDialog.setTitle("提示");
        progressDialog.setMessage("请等待...");
        progressDialog.setCancelable(false);
        progressDialog.show();
        OkHttpUtils.post()
                .url(ContractUtils.LOGIN_URL + "news/getBanner")
                .addHeader("token", ContractUtils.getTOKEN(YuanQuShengHuoActivity.this))
                .addParams("parkId", ContractUtils.getParkId(YuanQuShengHuoActivity.this))
                .addParams("typeId","5")
                .build()
                .execute(new StringCallback() {
                    @Override
                    public void onError(Call call, Exception e, int id) {
                        progressDialog.dismiss();
                    }

                    @Override
                    public void onResponse(String response, int id) {
                        ContractUtils.Code500(YuanQuShengHuoActivity.this,response);
                        System.out.println(response + "      首页banner图的网络请求");
                        progressDialog.dismiss();

                        if (response.indexOf("200") != -1) {
                            Gson gson = new Gson();
                            shouYeBannerEntity = gson.fromJson(response, ShouYeBannerEntity.class);
                            for (int i = 0; i < shouYeBannerEntity.getResponse().size(); i++) {
                                View view = LayoutInflater.from(YuanQuShengHuoActivity.this).inflate(R.layout.item_one, null);
                                TextView textView = view.findViewById(R.id.tv_wenzi);
                                textView.setText(shouYeBannerEntity.getResponse().get(i).getId());
                                Glide.with(YuanQuShengHuoActivity.this).load(shouYeBannerEntity.getResponse().get(i).getImage()).into((ImageView) view.findViewById(R.id.viewpager_image));
                                view.setTag(shouYeBannerEntity.getResponse().get(i).getId());
                                list.add(view);

                                view.findViewById(R.id.viewpager_image).setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
////                                        TextView textView = view.findViewById(R.id.tv_wenzi);
////                                        String trim = textView.getText().toString().trim();
////                                        System.out.println(textView.getId()+"         text");

//                                        System.out.println(pos+"           pos");
//                                        Intent intent = new Intent(getContext(), ZiXunXiangQingActivity.class);
//                                        intent.putExtra("id",shouYeBannerEntity.getResponse().get(pos).getId());
//                                        startActivity(intent);

                                        if (shouYeBannerEntity.getResponse().get(viewPager.getCurrentItem()).getHrefType().equals("0")){
                                            Intent intenttongzhi = new Intent(YuanQuShengHuoActivity.this, YuanQuTongZhiActivity.class);
                                            intenttongzhi.putExtra("zhi","zhi");
                                            startActivity(intenttongzhi);
//                                            Intent intent1 = new Intent(YuanQuShengHuoActivity.this,ChanYeJinRongActivity.class);
//                                            startActivity(intent1);
                                        }else if(shouYeBannerEntity.getResponse().get(viewPager.getCurrentItem()).getHrefType().equals("1")){
                                            Intent intent1 = new Intent(YuanQuShengHuoActivity.this,YuanQuShouCeActivity.class);
                                            startActivity(intent1);
                                        }else if(shouYeBannerEntity.getResponse().get(viewPager.getCurrentItem()).getHrefType().equals("2")){
                                            Intent intent1 = new Intent(YuanQuShengHuoActivity.this,YuanQuQiYeActivity.class);
                                            startActivity(intent1);
                                        }else if(shouYeBannerEntity.getResponse().get(viewPager.getCurrentItem()).getHrefType().equals("3")){
                                            Intent intent1 = new Intent(YuanQuShengHuoActivity.this,ZhengCeFaGuiActivity.class);
                                            startActivity(intent1);
                                        }else if(shouYeBannerEntity.getResponse().get(viewPager.getCurrentItem()).getHrefType().equals("4")){
                                            Intent intent1 = new Intent(YuanQuShengHuoActivity.this,ZhiNengZhiZaoActivity.class);
                                            startActivity(intent1);
                                        }else if(shouYeBannerEntity.getResponse().get(viewPager.getCurrentItem()).getHrefType().equals("5")){
                                            Intent intent1 = new Intent(YuanQuShengHuoActivity.this,YuanQuZhaoShangActivity.class);
                                            startActivity(intent1);
                                        }else if(shouYeBannerEntity.getResponse().get(viewPager.getCurrentItem()).getHrefType().equals("6")){
                                            if (tiaoZhuanFragment!=null){
                                                tiaoZhuanFragment.onClick();
                                            }
                                        }else if(shouYeBannerEntity.getResponse().get(viewPager.getCurrentItem()).getHrefType().equals("7")){
                                            if(fuWuTiaoZhuFragment != null){
                                                fuWuTiaoZhuFragment.onClick1();
                                            }
                                        }else if(shouYeBannerEntity.getResponse().get(viewPager.getCurrentItem()).getHrefType().equals("8")){
                                            Intent intent = new Intent(YuanQuShengHuoActivity.this, HomeActivity.class);
                                            startActivity(intent);
                                        }else if(shouYeBannerEntity.getResponse().get(viewPager.getCurrentItem()).getHrefType().equals("9")){
                                            if(shouYeBannerEntity.getResponse().get(viewPager.getCurrentItem()).getContent() != null){
                                                Intent intent = new Intent(YuanQuShengHuoActivity.this, WenZhangXiangQingActivity.class);
                                                intent.putExtra("content",shouYeBannerEntity.getResponse().get(viewPager.getCurrentItem()).getContent());
                                                startActivity(intent);
                                            }
                                        }else if(shouYeBannerEntity.getResponse().get(viewPager.getCurrentItem()).getHrefType().equals("10")){
                                            if(shouYeBannerEntity.getResponse().get(viewPager.getCurrentItem()).getContent() != null){
                                                Intent intent = new Intent(YuanQuShengHuoActivity.this, YuanQuShengHuoActivity.class);
                                                startActivity(intent);
                                            }
                                        }else if(shouYeBannerEntity.getResponse().get(viewPager.getCurrentItem()).getHrefType().equals("11")){
                                            if(shouYeBannerEntity.getResponse().get(viewPager.getCurrentItem()).getContent() != null){
                                                return;
                                            }
                                        }else if(shouYeBannerEntity.getResponse().get(viewPager.getCurrentItem()).getHrefType().equals("12")){
                                            if(shouYeBannerEntity.getResponse().get(viewPager.getCurrentItem()).getContent() != null){
                                                Intent intent = new Intent(YuanQuShengHuoActivity.this,SettingActivity.class);
                                                startActivity(intent);
                                            }
                                        }else if(shouYeBannerEntity.getResponse().get(viewPager.getCurrentItem()).getHrefType().equals("13")){
                                            if(shouYeBannerEntity.getResponse().get(viewPager.getCurrentItem()).getContent() != null){
                                                Intent intent = new Intent(YuanQuShengHuoActivity.this,QiuZhiZhaoPinActivity.class);
                                                startActivity(intent);
                                            }
                                        }else if(shouYeBannerEntity.getResponse().get(viewPager.getCurrentItem()).getHrefType().equals("14")){
                                            if(shouYeBannerEntity.getResponse().get(viewPager.getCurrentItem()).getContent() != null){
                                                Intent intent = new Intent(YuanQuShengHuoActivity.this,ChanYeJinRongActivity.class);
                                                startActivity(intent);
                                            }
                                        }else if(shouYeBannerEntity.getResponse().get(viewPager.getCurrentItem()).getHrefType().equals("15")){
                                            if(shouYeBannerEntity.getResponse().get(viewPager.getCurrentItem()).getContent() != null){
                                                Intent intent = new Intent(YuanQuShengHuoActivity.this,YuanQuTongZhiActivity.class);
                                                startActivity(intent);
                                            }
                                        }else if(shouYeBannerEntity.getResponse().get(viewPager.getCurrentItem()).getHrefType().equals("16")){
                                            if(shouYeBannerEntity.getResponse().get(viewPager.getCurrentItem()).getContent() != null){
                                                Intent intent = new Intent(YuanQuShengHuoActivity.this,FangKeDengJiActivity.class);
                                                startActivity(intent);
                                            }
                                        }else if(shouYeBannerEntity.getResponse().get(viewPager.getCurrentItem()).getHrefType().equals("17")){
                                            if(shouYeBannerEntity.getResponse().get(viewPager.getCurrentItem()).getContent() != null){
                                                Intent intent = new Intent(YuanQuShengHuoActivity.this,QuanBuActivity.class);
                                                intent.putExtra("daizhifu","0");
                                                startActivity(intent);
                                            }
                                        }else if(shouYeBannerEntity.getResponse().get(viewPager.getCurrentItem()).getHrefType().equals("18")){
                                            if(shouYeBannerEntity.getResponse().get(viewPager.getCurrentItem()).getContent() != null){
                                                Intent intent = new Intent(YuanQuShengHuoActivity.this,MyShouCangActivity.class);
                                                startActivity(intent);
                                            }
                                        }else if(shouYeBannerEntity.getResponse().get(viewPager.getCurrentItem()).getHrefType().equals("19")){
                                            if(shouYeBannerEntity.getResponse().get(viewPager.getCurrentItem()).getContent() != null){
                                                Intent intent = new Intent(YuanQuShengHuoActivity.this,ShouHuoXinXiActivity.class);
                                                startActivity(intent);
                                            }
                                        }else if(shouYeBannerEntity.getResponse().get(viewPager.getCurrentItem()).getHrefType().equals("20")){
                                            if(shouYeBannerEntity.getResponse().get(viewPager.getCurrentItem()).getContent() != null){
                                                Intent intent = new Intent(YuanQuShengHuoActivity.this,RuZhuShenQingActivity.class);
                                                startActivity(intent);
                                            }
                                        }else if(shouYeBannerEntity.getResponse().get(viewPager.getCurrentItem()).getHrefType().equals("21")){
                                            if(shouYeBannerEntity.getResponse().get(viewPager.getCurrentItem()).getContent() != null){
                                                Intent intent = new Intent(YuanQuShengHuoActivity.this,MyBangDingActivity.class);
                                                startActivity(intent);
                                            }
                                        }
                                    }
                                });
                            }

                            BannerAdapter adapter = new BannerAdapter(YuanQuShengHuoActivity.this, list);
                            viewPager.setAdapter(adapter);
                            adapter1 = new BannerAdapter1();
                            manager = new LinearLayoutManager(YuanQuShengHuoActivity.this);
                            manager.setOrientation(LinearLayoutManager.HORIZONTAL);
                            recyclerViewBanner.setLayoutManager(manager);
                            recyclerViewBanner.setAdapter(adapter1);
                        }
                    }
                });
    }





    class BannerAdapter1 extends RecyclerView.Adapter<BannerAdapter1.ViewHolder> {

        @Override
        public BannerAdapter1.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(YuanQuShengHuoActivity.this).inflate(R.layout.item_banner_dian, parent, false);
            BannerAdapter1.ViewHolder viewHolder = new BannerAdapter1.ViewHolder(view);
            return viewHolder;
        }

        @Override
        public void onBindViewHolder(BannerAdapter1.ViewHolder holder, int position) {
            if (viewPager.getCurrentItem() == position) {
                holder.img_two.setImageResource(R.drawable.shape_yuan);
            } else {
                holder.img_two.setImageResource(R.drawable.yuan_false);
            }
        }

        @Override
        public int getItemCount() {
            return list.size();
        }

        public class ViewHolder extends RecyclerView.ViewHolder {
            private ImageView img_two;

            public ViewHolder(View itemView) {
                super(itemView);
                img_two = itemView.findViewById(R.id.img_two);
            }
        }
    }







    private void initThread() {
        new Thread() {
            @Override
            public void run() {
                super.run();
                try {
                    while (isOk) {
                        Thread.sleep(3000);
                        Message message = Message.obtain();
                        int currentItem = viewPager.getCurrentItem();
                        if (currentItem == list.size() - 1) {
                            currentItem = 0;
                            message.what = 1;
                            message.arg1 = currentItem;
                            handler.sendMessage(message);
                        } else {
                            message.what = 2;
                            message.arg1 = currentItem + 1;
                            handler.sendMessage(message);
                        }
                    }
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }.start();
    }





    //    园区生活界面的网络请求
//    tiantian
//    .addParams("parkId", ContractUtils.getParkId(YuanQuShengHuoActivity.this))
    private void initTuiJianShangJiaHttp() {
        final ProgressDialog progressDialog = new ProgressDialog(YuanQuShengHuoActivity.this);
        progressDialog.setTitle("提示");
        progressDialog.setMessage("请等待...");
        progressDialog.setCancelable(false);
        progressDialog.show();
        OkHttpUtils.post()
                .url(ContractUtils.LOGIN_URL + "Life/index")
                .addHeader("token", ContractUtils.getTOKEN(YuanQuShengHuoActivity.this))
                .addParams("parkId", ContractUtils.getParkId(YuanQuShengHuoActivity.this))
                .build()
                .execute(new StringCallback() {
                    @Override
                    public void onError(Call call, Exception e, int id) {
                        progressDialog.dismiss();
                    }

                    @Override
                    public void onResponse(String response, int id) {
                        System.out.println(response + "     园区生活界面的网络请求");
                        ContractUtils.Code500(YuanQuShengHuoActivity.this, response);
                        progressDialog.dismiss();
                        if(response.indexOf("200") != -1){
                            Gson gson = new Gson();
                            yuanQuShengHuoTuiJianShangJiaEntity = gson.fromJson(response, YuanQuShengHuoTuiJianShangJiaEntity.class);

//                        商家推荐
                            yuanQuShengHuoShangJiaTuiJianAdapter = new YuanQuShengHuoShangJiaTuiJianAdapter(YuanQuShengHuoActivity.this, yuanQuShengHuoTuiJianShangJiaEntity.getResponse().getShop());
                            LinearLayoutManager manager1 = new LinearLayoutManager(YuanQuShengHuoActivity.this);
                            recyclerViewTuijianshangjia.setLayoutManager(manager1);
                            recyclerViewTuijianshangjia.setAdapter(yuanQuShengHuoShangJiaTuiJianAdapter);

//                        生活设施横向滑动
                            hengAdapter = new HengAdapter(YuanQuShengHuoActivity.this, yuanQuShengHuoTuiJianShangJiaEntity.getResponse().getMerchantClass());
                            LinearLayoutManager manager = new LinearLayoutManager(YuanQuShengHuoActivity.this);
                            //设置为横向滑动
                            manager.setOrientation(LinearLayoutManager.HORIZONTAL);
                            recyclerView.setLayoutManager(manager);
                            recyclerView.setAdapter(hengAdapter);
//                        点击进入销量
                            hengAdapter.setXiangQingClick(new HengAdapter.XiangQingClick() {
                                @Override
                                public void xiangqingClick(int position) {
//                                分类的id
                                    Intent intent = new Intent(YuanQuShengHuoActivity.this, DianPuActivity.class);
                                    intent.putExtra("id", yuanQuShengHuoTuiJianShangJiaEntity.getResponse().getMerchantClass().get(position).getId());
                                    System.out.println(yuanQuShengHuoTuiJianShangJiaEntity.getResponse().getMerchantClass().get(position).getId() + "    传发送分类的id");
                                    intent.putExtra("name",yuanQuShengHuoTuiJianShangJiaEntity.getResponse().getMerchantClass().get(position).getName());
                                    startActivity(intent);
                                }
                            });


//                        商家推荐
                            yuanQuShengHuoShangJiaTuiJianAdapter.setShangJiaTuiJianClick(new YuanQuShengHuoShangJiaTuiJianAdapter.ShangJiaTuiJianClick() {
                                @Override
                                public void shangjiatuijianClick(int position) {
//                                tiantian 应该传分类id
                                    Intent intent = new Intent(YuanQuShengHuoActivity.this, WaiMaiActivity.class);
                                    intent.putExtra("id", yuanQuShengHuoTuiJianShangJiaEntity.getResponse().getShop().get(position).getId());
                                    System.out.println(yuanQuShengHuoTuiJianShangJiaEntity.getResponse().getShop().get(position).getId() + "    传送商家推荐的商家id");
                                    intent.putExtra("bidian",yuanQuShengHuoTuiJianShangJiaEntity.getResponse().getShop().get(position).getBusiness());
                                    startActivity(intent);
                                }
                            });


////                        商家活动
//                        yuanQuShengHuoShangJiaWaiMaiAdapter = new YuanQuShengHuoShangJiaWaiMaiAdapter(YuanQuShengHuoActivity.this, yuanQuShengHuoTuiJianShangJiaEntity.getResponse().getShopPreferences());
//                        LinearLayoutManager manager2 = new LinearLayoutManager(YuanQuShengHuoActivity.this);
//                        recyclerViewShangjiahuodong.setLayoutManager(manager2);
//                        recyclerViewShangjiahuodong.setAdapter(yuanQuShengHuoShangJiaWaiMaiAdapter);
//
//                        yuanQuShengHuoShangJiaWaiMaiAdapter.setXiangQingClick(new YuanQuShengHuoShangJiaWaiMaiAdapter.XiangQingClick() {
//                            @Override
//                            public void xiangqingClick(int position) {
//                                Intent intent = new Intent(YuanQuShengHuoActivity.this, ShangJiaHuoDongXiangQingActivity.class);
//                                intent.putExtra("id", yuanQuShengHuoTuiJianShangJiaEntity.getResponse().getShopPreferences().get(position).getId());
//                                System.out.println(yuanQuShengHuoTuiJianShangJiaEntity.getResponse().getShopPreferences().get(position).getId() + "  传送商家活动的活动id");
//                                startActivity(intent);
//                            }
//                        });
                        }

                    }
                });
    }









    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {

        if (adapter1!=null){
            adapter1.notifyDataSetChanged();
        }


    }

    @Override
    public void onPageScrollStateChanged(int state) {


    }




    //
    @OnClick({R.id.image_back, R.id.image_shenghuo_sheshi, R.id.tv_shenghuo_sheshi})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.image_back:
                finish();
                break;
            case R.id.image_shenghuo_sheshi:
                Intent intent = new Intent(YuanQuShengHuoActivity.this, ShengHuoSheShiActivity.class);
                startActivity(intent);
                break;
            case R.id.tv_shenghuo_sheshi:
                Intent intent1 = new Intent(YuanQuShengHuoActivity.this, ShengHuoSheShiActivity.class);
                startActivity(intent1);
                break;
        }
    }
}
