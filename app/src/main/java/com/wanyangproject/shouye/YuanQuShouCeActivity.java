package com.wanyangproject.shouye;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.androidkun.xtablayout.XTabLayout;
import com.google.gson.Gson;
import com.wanyangproject.R;
import com.wanyangproject.adapter.TabAdapter;
import com.wanyangproject.entity.YuanQuShouCeEntity;
import com.wanyangproject.shouyefragment.YuanGongFragment;
import com.wanyangproject.utils.ContractUtils;
import com.zhy.http.okhttp.OkHttpUtils;
import com.zhy.http.okhttp.callback.StringCallback;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.Call;

public class YuanQuShouCeActivity extends AppCompatActivity {

    @BindView(R.id.image_back)
    ImageView imageBack;
    //    @BindView(R.id.tabLayout)
//    TabLayout tabLayout;
    @BindView(R.id.viewPager)
    ViewPager viewPager;
    @BindView(R.id.tabLayout)
    XTabLayout tabLayout;
    private List<String> stringList = new ArrayList<>();
    private YuanQuShouCeEntity yuanQuShouCeEntity;
    private static int TAB_MARGIN_DIP = 11;
    private YuanGongFragment yuanGongFragment;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_yuan_qu_shou_ce);
        ButterKnife.bind(this);
//        if (Build.VERSION.SDK_INT >= 21) {
//            View dview = getWindow().getDecorView();
//            dview.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN | View.SYSTEM_UI_FLAG_LAYOUT_STABLE);
//            getWindow().setStatusBarColor(Color.TRANSPARENT);
//        }
        initViewPager();
//        园区手册的网络请求
        initYuanQuSHouCeOneHttp();

    }

    //    园区手册的网络请求
//    tiantian    bqId暂时
    private void initYuanQuSHouCeOneHttp() {
        OkHttpUtils.post()
                .url(ContractUtils.LOGIN_URL + "park/getmanual")
                .addHeader("token", ContractUtils.getTOKEN(YuanQuShouCeActivity.this))
                .addParams("parkId", ContractUtils.getParkId(YuanQuShouCeActivity.this))
                .addParams("bqId", "1")
                .build()
                .execute(new StringCallback() {
                    @Override
                    public void onError(Call call, Exception e, int id) {
                        System.out.println(e + "     园区手册eeeeee");

                    }

                    @Override
                    public void onResponse(String response, int id) {
                        ContractUtils.Code500(YuanQuShouCeActivity.this,response);
                        System.out.println(response + "    园区手册的网络请求");

                        if (response.indexOf("200") != -1) {
                            Gson gson = new Gson();
                            yuanQuShouCeEntity = gson.fromJson(response, YuanQuShouCeEntity.class);
//                            园区手册的第二次网络请求
                            List<Fragment> fragments = new ArrayList<>();
                            for (int i = 0; i < yuanQuShouCeEntity.getResponse().getBqArr().size(); i++) {
                                stringList.add(yuanQuShouCeEntity.getResponse().getBqArr().get(i).getTitle());
                                yuanGongFragment = new YuanGongFragment();
                                yuanGongFragment.setIsOK(yuanQuShouCeEntity.getResponse().getBqArr().get(i).getId());
                                fragments.add(yuanGongFragment);
                            }

                            // 创建ViewPager适配器
                            TabAdapter tabAdapter = new TabAdapter(getSupportFragmentManager(), fragments, stringList);
//                          luntanAdapter.setFragments(fragments);
                            // 给ViewPager设置适配器
                            viewPager.setAdapter(tabAdapter);
                            // 使用 TabLayout 和 ViewPager 相关联
                            tabLayout.setupWithViewPager(viewPager);
//                            setIndicator(YuanQuShouCeActivity.this, tabLayout, TAB_MARGIN_DIP, TAB_MARGIN_DIP);






                            Intent intent = getIntent();
                            String stringExtra = intent.getStringExtra("shouce");
                            if(stringExtra != null) {
                                int i = Integer.parseInt(stringExtra);
                                viewPager.setCurrentItem(i);

                                //                           1 游客 2员工  3 企业  4 商家
//                            typeId: 0 游客 1 普通员工 2企业用户 3商家 4 门卫 5 物业
                                if (i == 0) {
                                    if (yuanQuShouCeEntity.getResponse().getBqArr().get(i).getId().equals("2")) {
                                        if (ContractUtils.getTypeId(YuanQuShouCeActivity.this).equals("1") || ContractUtils.getTypeId22(YuanQuShouCeActivity.this).equals("1")
                                                || ContractUtils.getTypeId(YuanQuShouCeActivity.this).equals("2") || ContractUtils.getTypeId22(YuanQuShouCeActivity.this).equals("2")
                                                || ContractUtils.getTypeId(YuanQuShouCeActivity.this).equals("4") || ContractUtils.getTypeId22(YuanQuShouCeActivity.this).equals("4")) {
                                        } else {
                                            Toast.makeText(YuanQuShouCeActivity.this, "您当前不是员工、门卫或企业身份！", Toast.LENGTH_SHORT).show();
                                        }
                                    } else if (yuanQuShouCeEntity.getResponse().getBqArr().get(i).getId().equals("3")) {
                                        if (ContractUtils.getTypeId(YuanQuShouCeActivity.this).equals("2") || ContractUtils.getTypeId22(YuanQuShouCeActivity.this).equals("2")) {

                                        } else {
                                            Toast.makeText(YuanQuShouCeActivity.this, "您当前不是企业身份！", Toast.LENGTH_SHORT).show();
                                        }
                                    } else if (yuanQuShouCeEntity.getResponse().getBqArr().get(i).getId().equals("4")) {
                                        if (ContractUtils.getTypeId(YuanQuShouCeActivity.this).equals("3") || ContractUtils.getTypeId22(YuanQuShouCeActivity.this).equals("3")) {

                                        } else {
                                            Toast.makeText(YuanQuShouCeActivity.this, "您当前不是商家身份！", Toast.LENGTH_SHORT).show();
                                        }
                                    } else if (yuanQuShouCeEntity.getResponse().getBqArr().get(i).getId().equals("1")) {
                                        if (ContractUtils.getTypeId(YuanQuShouCeActivity.this).equals("0") || ContractUtils.getTypeId22(YuanQuShouCeActivity.this).equals("0")) {

                                        } else {
                                            Toast.makeText(YuanQuShouCeActivity.this, "您当前不是游客身份！", Toast.LENGTH_SHORT).show();
                                        }
                                    }
                                } else if (i == 1) {
                                    if (yuanQuShouCeEntity.getResponse().getBqArr().get(i).getId().equals("2")) {
                                        if (ContractUtils.getTypeId(YuanQuShouCeActivity.this).equals("1") || ContractUtils.getTypeId22(YuanQuShouCeActivity.this).equals("1")
                                                || ContractUtils.getTypeId(YuanQuShouCeActivity.this).equals("2") || ContractUtils.getTypeId22(YuanQuShouCeActivity.this).equals("2")
                                                || ContractUtils.getTypeId(YuanQuShouCeActivity.this).equals("4") || ContractUtils.getTypeId22(YuanQuShouCeActivity.this).equals("4")) {
                                        } else {
                                            Toast.makeText(YuanQuShouCeActivity.this, "您当前不是员工、门卫或企业身份！", Toast.LENGTH_SHORT).show();
                                        }
                                    } else if (yuanQuShouCeEntity.getResponse().getBqArr().get(i).getId().equals("3")) {
                                        if (ContractUtils.getTypeId(YuanQuShouCeActivity.this).equals("2") || ContractUtils.getTypeId22(YuanQuShouCeActivity.this).equals("2")) {

                                        } else {
                                            Toast.makeText(YuanQuShouCeActivity.this, "您当前不是企业身份！", Toast.LENGTH_SHORT).show();
                                        }
                                    } else if (yuanQuShouCeEntity.getResponse().getBqArr().get(i).getId().equals("4")) {
                                        if (ContractUtils.getTypeId(YuanQuShouCeActivity.this).equals("3") || ContractUtils.getTypeId22(YuanQuShouCeActivity.this).equals("3")) {

                                        } else {
                                            Toast.makeText(YuanQuShouCeActivity.this, "您当前不是商家身份！", Toast.LENGTH_SHORT).show();
                                        }
                                    } else if (yuanQuShouCeEntity.getResponse().getBqArr().get(i).getId().equals("1")) {
                                        if (ContractUtils.getTypeId(YuanQuShouCeActivity.this).equals("0") || ContractUtils.getTypeId22(YuanQuShouCeActivity.this).equals("0")) {

                                        } else {
                                            Toast.makeText(YuanQuShouCeActivity.this, "您当前不是游客身份！", Toast.LENGTH_SHORT).show();
                                        }
                                    }
                                } else if (i == 2) {
                                    if (yuanQuShouCeEntity.getResponse().getBqArr().get(i).getId().equals("2")) {
                                        if (ContractUtils.getTypeId(YuanQuShouCeActivity.this).equals("1") || ContractUtils.getTypeId22(YuanQuShouCeActivity.this).equals("1")
                                                || ContractUtils.getTypeId(YuanQuShouCeActivity.this).equals("2") || ContractUtils.getTypeId22(YuanQuShouCeActivity.this).equals("2")
                                                || ContractUtils.getTypeId(YuanQuShouCeActivity.this).equals("4") || ContractUtils.getTypeId22(YuanQuShouCeActivity.this).equals("4")) {
                                        } else {
                                            Toast.makeText(YuanQuShouCeActivity.this, "您当前不是员工、门卫或企业身份！", Toast.LENGTH_SHORT).show();
                                        }
                                    } else if (yuanQuShouCeEntity.getResponse().getBqArr().get(i).getId().equals("3")) {
                                        if (ContractUtils.getTypeId(YuanQuShouCeActivity.this).equals("2") || ContractUtils.getTypeId22(YuanQuShouCeActivity.this).equals("2")) {

                                        } else {
                                            Toast.makeText(YuanQuShouCeActivity.this, "您当前不是企业身份！", Toast.LENGTH_SHORT).show();
                                        }
                                    } else if (yuanQuShouCeEntity.getResponse().getBqArr().get(i).getId().equals("4")) {
                                        if (ContractUtils.getTypeId(YuanQuShouCeActivity.this).equals("3") || ContractUtils.getTypeId22(YuanQuShouCeActivity.this).equals("3")) {

                                        } else {
                                            Toast.makeText(YuanQuShouCeActivity.this, "您当前不是商家身份！", Toast.LENGTH_SHORT).show();
                                        }
                                    } else if (yuanQuShouCeEntity.getResponse().getBqArr().get(i).getId().equals("1")) {
                                        if (ContractUtils.getTypeId(YuanQuShouCeActivity.this).equals("0") || ContractUtils.getTypeId22(YuanQuShouCeActivity.this).equals("0")) {

                                        } else {
                                            Toast.makeText(YuanQuShouCeActivity.this, "您当前不是游客身份！", Toast.LENGTH_SHORT).show();
                                        }
                                    }
                                } else if (i == 3) {
                                    if (yuanQuShouCeEntity.getResponse().getBqArr().get(i).getId().equals("2")) {
                                        if (ContractUtils.getTypeId(YuanQuShouCeActivity.this).equals("1") || ContractUtils.getTypeId22(YuanQuShouCeActivity.this).equals("1")
                                                || ContractUtils.getTypeId(YuanQuShouCeActivity.this).equals("2") || ContractUtils.getTypeId22(YuanQuShouCeActivity.this).equals("2")
                                                || ContractUtils.getTypeId(YuanQuShouCeActivity.this).equals("4") || ContractUtils.getTypeId22(YuanQuShouCeActivity.this).equals("4")) {
                                        } else {
                                            Toast.makeText(YuanQuShouCeActivity.this, "您当前不是员工、门卫或企业身份！", Toast.LENGTH_SHORT).show();
                                        }
                                    } else if (yuanQuShouCeEntity.getResponse().getBqArr().get(i).getId().equals("3")) {
                                        if (ContractUtils.getTypeId(YuanQuShouCeActivity.this).equals("2") || ContractUtils.getTypeId22(YuanQuShouCeActivity.this).equals("2")) {

                                        } else {
                                            Toast.makeText(YuanQuShouCeActivity.this, "您当前不是企业身份！", Toast.LENGTH_SHORT).show();
                                        }
                                    } else if (yuanQuShouCeEntity.getResponse().getBqArr().get(i).getId().equals("4")) {
                                        if (ContractUtils.getTypeId(YuanQuShouCeActivity.this).equals("3") || ContractUtils.getTypeId22(YuanQuShouCeActivity.this).equals("3")) {

                                        } else {
                                            Toast.makeText(YuanQuShouCeActivity.this, "您当前不是商家身份！", Toast.LENGTH_SHORT).show();
                                        }
                                    } else if (yuanQuShouCeEntity.getResponse().getBqArr().get(i).getId().equals("1")) {
                                        if (ContractUtils.getTypeId(YuanQuShouCeActivity.this).equals("0") || ContractUtils.getTypeId22(YuanQuShouCeActivity.this).equals("0")) {

                                        } else {
                                            Toast.makeText(YuanQuShouCeActivity.this, "您当前不是游客身份！", Toast.LENGTH_SHORT).show();
                                        }
                                    }
                                }


                                viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
                                    @Override
                                    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

                                    }

                                    @Override
                                    public void onPageSelected(int position) {
                                        if (position==0){
                                            if(yuanQuShouCeEntity.getResponse().getBqArr().get(position).getId().equals("2")){
                                                if(ContractUtils.getTypeId(YuanQuShouCeActivity.this).equals("1") || ContractUtils.getTypeId22(YuanQuShouCeActivity.this).equals("1")
                                                        || ContractUtils.getTypeId(YuanQuShouCeActivity.this).equals("2") || ContractUtils.getTypeId22(YuanQuShouCeActivity.this).equals("2")
                                                        ||  ContractUtils.getTypeId(YuanQuShouCeActivity.this).equals("4") || ContractUtils.getTypeId22(YuanQuShouCeActivity.this).equals("4")){
                                                }else{
                                                    Toast.makeText(YuanQuShouCeActivity.this, "您当前不是员工、门卫或企业身份！", Toast.LENGTH_SHORT).show();
                                                }
                                            }else if(yuanQuShouCeEntity.getResponse().getBqArr().get(position).getId().equals("3")){
                                                if(ContractUtils.getTypeId(YuanQuShouCeActivity.this).equals("2") || ContractUtils.getTypeId22(YuanQuShouCeActivity.this).equals("2")){

                                                }else {
                                                    Toast.makeText(YuanQuShouCeActivity.this, "您当前不是企业身份！", Toast.LENGTH_SHORT).show();
                                                }
                                            }else if(yuanQuShouCeEntity.getResponse().getBqArr().get(position).getId().equals("4")){
                                                if(ContractUtils.getTypeId(YuanQuShouCeActivity.this).equals("3") || ContractUtils.getTypeId22(YuanQuShouCeActivity.this).equals("3")){

                                                }else {
                                                    Toast.makeText(YuanQuShouCeActivity.this, "您当前不是商家身份！", Toast.LENGTH_SHORT).show();
                                                }
                                            }else if(yuanQuShouCeEntity.getResponse().getBqArr().get(position).getId().equals("1")){
                                                if(ContractUtils.getTypeId(YuanQuShouCeActivity.this).equals("0") || ContractUtils.getTypeId22(YuanQuShouCeActivity.this).equals("0")){

                                                }else{
                                                    Toast.makeText(YuanQuShouCeActivity.this, "您当前不是游客身份！", Toast.LENGTH_SHORT).show();
                                                }
                                            }
                                        }else if (position==1){
                                            if(yuanQuShouCeEntity.getResponse().getBqArr().get(position).getId().equals("2")){
                                                if(ContractUtils.getTypeId(YuanQuShouCeActivity.this).equals("1") || ContractUtils.getTypeId22(YuanQuShouCeActivity.this).equals("1")
                                                        || ContractUtils.getTypeId(YuanQuShouCeActivity.this).equals("2") || ContractUtils.getTypeId22(YuanQuShouCeActivity.this).equals("2")
                                                        ||  ContractUtils.getTypeId(YuanQuShouCeActivity.this).equals("4") || ContractUtils.getTypeId22(YuanQuShouCeActivity.this).equals("4")){
                                                }else{
                                                    Toast.makeText(YuanQuShouCeActivity.this, "您当前不是员工、门卫或企业身份！", Toast.LENGTH_SHORT).show();
                                                }
                                            }else if(yuanQuShouCeEntity.getResponse().getBqArr().get(position).getId().equals("3")){
                                                if(ContractUtils.getTypeId(YuanQuShouCeActivity.this).equals("2") || ContractUtils.getTypeId22(YuanQuShouCeActivity.this).equals("2")){
                                                }else {
                                                    Toast.makeText(YuanQuShouCeActivity.this, "您当前不是企业身份！", Toast.LENGTH_SHORT).show();
                                                }
                                            }else if(yuanQuShouCeEntity.getResponse().getBqArr().get(position).getId().equals("4")){
                                                if(ContractUtils.getTypeId(YuanQuShouCeActivity.this).equals("3") || ContractUtils.getTypeId22(YuanQuShouCeActivity.this).equals("3")){

                                                }else {
                                                    Toast.makeText(YuanQuShouCeActivity.this, "您当前不是商家身份！", Toast.LENGTH_SHORT).show();
                                                }
                                            }else if(yuanQuShouCeEntity.getResponse().getBqArr().get(position).getId().equals("1")){
                                                if(ContractUtils.getTypeId(YuanQuShouCeActivity.this).equals("0") || ContractUtils.getTypeId22(YuanQuShouCeActivity.this).equals("0")){

                                                }else{
                                                    Toast.makeText(YuanQuShouCeActivity.this, "您当前不是游客身份！", Toast.LENGTH_SHORT).show();
                                                }
                                            }
                                        }else if (position==2){
                                            if(yuanQuShouCeEntity.getResponse().getBqArr().get(position).getId().equals("2")){
                                                if(ContractUtils.getTypeId(YuanQuShouCeActivity.this).equals("1") || ContractUtils.getTypeId22(YuanQuShouCeActivity.this).equals("1")
                                                        || ContractUtils.getTypeId(YuanQuShouCeActivity.this).equals("2") || ContractUtils.getTypeId22(YuanQuShouCeActivity.this).equals("2")
                                                        ||  ContractUtils.getTypeId(YuanQuShouCeActivity.this).equals("4") || ContractUtils.getTypeId22(YuanQuShouCeActivity.this).equals("4")){
                                                }else{
                                                    Toast.makeText(YuanQuShouCeActivity.this, "您当前不是员工、门卫或企业身份！", Toast.LENGTH_SHORT).show();
                                                }
                                            }else if(yuanQuShouCeEntity.getResponse().getBqArr().get(position).getId().equals("3")){
                                                if(ContractUtils.getTypeId(YuanQuShouCeActivity.this).equals("2") || ContractUtils.getTypeId22(YuanQuShouCeActivity.this).equals("2")){
                                                }else {
                                                    Toast.makeText(YuanQuShouCeActivity.this, "您当前不是企业身份！", Toast.LENGTH_SHORT).show();
                                                }
                                            }else if(yuanQuShouCeEntity.getResponse().getBqArr().get(position).getId().equals("4")){
                                                if(ContractUtils.getTypeId(YuanQuShouCeActivity.this).equals("3") || ContractUtils.getTypeId22(YuanQuShouCeActivity.this).equals("3")){

                                                }else {
                                                    Toast.makeText(YuanQuShouCeActivity.this, "您当前不是商家身份！", Toast.LENGTH_SHORT).show();
                                                }
                                            }else if(yuanQuShouCeEntity.getResponse().getBqArr().get(position).getId().equals("1")){
                                                if(ContractUtils.getTypeId(YuanQuShouCeActivity.this).equals("0") || ContractUtils.getTypeId22(YuanQuShouCeActivity.this).equals("0")){

                                                }else{
                                                    Toast.makeText(YuanQuShouCeActivity.this, "您当前不是游客身份！", Toast.LENGTH_SHORT).show();
                                                }
                                            }
                                        }else if (position==3){
                                            if(yuanQuShouCeEntity.getResponse().getBqArr().get(position).getId().equals("2")){
                                                if(ContractUtils.getTypeId(YuanQuShouCeActivity.this).equals("1") || ContractUtils.getTypeId22(YuanQuShouCeActivity.this).equals("1")
                                                        || ContractUtils.getTypeId(YuanQuShouCeActivity.this).equals("2") || ContractUtils.getTypeId22(YuanQuShouCeActivity.this).equals("2")
                                                        ||  ContractUtils.getTypeId(YuanQuShouCeActivity.this).equals("4") || ContractUtils.getTypeId22(YuanQuShouCeActivity.this).equals("4")){
                                                }else{
                                                    Toast.makeText(YuanQuShouCeActivity.this, "您当前不是员工、门卫或企业身份！", Toast.LENGTH_SHORT).show();
                                                }
                                            }else if(yuanQuShouCeEntity.getResponse().getBqArr().get(position).getId().equals("3")){
                                                if(ContractUtils.getTypeId(YuanQuShouCeActivity.this).equals("2") || ContractUtils.getTypeId22(YuanQuShouCeActivity.this).equals("2")){
                                                }else {
                                                    Toast.makeText(YuanQuShouCeActivity.this, "您当前不是企业身份！", Toast.LENGTH_SHORT).show();
                                                }
                                            }else if(yuanQuShouCeEntity.getResponse().getBqArr().get(position).getId().equals("4")){
                                                if(ContractUtils.getTypeId(YuanQuShouCeActivity.this).equals("3") || ContractUtils.getTypeId22(YuanQuShouCeActivity.this).equals("3")){

                                                }else {
                                                    Toast.makeText(YuanQuShouCeActivity.this, "您当前不是商家身份！", Toast.LENGTH_SHORT).show();
                                                }
                                            }else if(yuanQuShouCeEntity.getResponse().getBqArr().get(position).getId().equals("1")){
                                                if(ContractUtils.getTypeId(YuanQuShouCeActivity.this).equals("0") || ContractUtils.getTypeId22(YuanQuShouCeActivity.this).equals("0")){

                                                }else{
                                                    Toast.makeText(YuanQuShouCeActivity.this, "您当前不是游客身份！", Toast.LENGTH_SHORT).show();
                                                }
                                            }
                                        }


                                    }

                                    @Override
                                    public void onPageScrollStateChanged(int state) {

                                    }
                                });
                            }
                            }
                            }
                });
    }


//    public static void setIndicator(Context context, TabLayout tabs, int leftDip, int rightDip) {
//        Class<?> tabLayout = tabs.getClass();
//        Field tabStrip = null;
//        try {
//            tabStrip = tabLayout.getDeclaredField("mTabStrip");
//        } catch (NoSuchFieldException e) {
//            e.printStackTrace();
//        }
//
//        tabStrip.setAccessible(true);
//        LinearLayout ll_tab = null;
//        try {
//            ll_tab = (LinearLayout) tabStrip.get(tabs);
//        } catch (IllegalAccessException e) {
//            e.printStackTrace();
//        }
//
//        int left = (int) (getDisplayMetrics(context).density * leftDip);
//        int right = (int) (getDisplayMetrics(context).density * rightDip);
//
//        for (int i = 0; i < ll_tab.getChildCount(); i++) {
//            View child = ll_tab.getChildAt(i);
//            child.setPadding(0, 0, 0, 0);
//            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(0, LinearLayout.LayoutParams.MATCH_PARENT, 1);
//            params.leftMargin = left;
//            params.rightMargin = right;
//            child.setLayoutParams(params);
//            child.invalidate();
//        }
//    }


    public static DisplayMetrics getDisplayMetrics(Context context) {
        DisplayMetrics metric = new DisplayMetrics();
        ((Activity) context).getWindowManager().getDefaultDisplay().getMetrics(metric);
        return metric;
    }

    public static float getPXfromDP(float value, Context context) {
        return TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, value,
                context.getResources().getDisplayMetrics());
    }


//    园区手册的第二次网络请求
//    private void initYuanQuSHouCeTwoHttp(String id1) {
//        OkHttpUtils.post()
//                .url(ContractUtils.LOGIN_URL+"park/getmanual")
//                .addHeader("token",ContractUtils.getTOKEN(YuanQuShouCeActivity.this))
//                .addParams("parkId",ContractUtils.getParkId(YuanQuShouCeActivity.this))
//                .addParams("bqId",id1)
//                .build()
//                .execute(new StringCallback() {
//                    @Override
//                    public void onError(Call call, Exception e, int id) {
//                        System.out.println(e+"     园区手册eeeeee");
//
//                    }
//                    @Override
//                    public void onResponse(String response, int id) {
//                        System.out.println(response+"    园区手册的网络请求");
//                        Gson gson = new Gson();
//                        yuanQuShouCeEntity = gson.fromJson(response, YuanQuShouCeEntity.class);
//                        if(response.indexOf("200") != -1){
//                            List<Fragment> fragments = new ArrayList<>();
//                            for (int i = 0; i < yuanQuShouCeEntity.getResponse().getBqArr().size(); i++) {
//                                stringList.add(yuanQuShouCeEntity.getResponse().getBqArr().get(i).getTitle());
//                                fragments.add(new YuanGongFragment());
//                            }
//                            // 创建ViewPager适配器
//                            TabAdapter tabAdapter = new TabAdapter(getSupportFragmentManager(),fragments,stringList);
////                          luntanAdapter.setFragments(fragments);
//                            // 给ViewPager设置适配器
//                            viewPager.setAdapter(tabAdapter);
//                            // 使用 TabLayout 和 ViewPager 相关联
//                            tabLayout.setupWithViewPager(viewPager);
//                        }
//                    }
//
//
//                });
//    }


    private void initViewPager() {
//        List<Fragment> fragments = new ArrayList<>();
//        for (int i = 0; i < 4; i++) {
//            stringList.add("销售"+i);
//            fragments.add(new YuanGongFragment());
//        }
//
//        // 创建ViewPager适配器
//        TabAdapter tabAdapter = new TabAdapter(getSupportFragmentManager(),fragments,stringList);
////        luntanAdapter.setFragments(fragments);
//        // 给ViewPager设置适配器
//        viewPager.setAdapter(tabAdapter);
//        // 使用 TabLayout 和 ViewPager 相关联
//        tabLayout.setupWithViewPager(viewPager);


//        List<Fragment> fragments = new ArrayList<>();
//        fragments.add(new YuanGongFragment());
//        fragments.add(new QiYeFragment());
//        fragments.add(new ShangJiaYuanQuShouCeFragment());
//        fragments.add(new YouKeFragment());
//
//
//        // 创建ViewPager适配器
//        TabLayoutAdapter tabLayoutAdapter = new TabLayoutAdapter(getSupportFragmentManager());
//        tabLayoutAdapter.setFragments(fragments);
//        // 给ViewPager设置适配器
//        viewPager.setAdapter(tabLayoutAdapter);
//        // TabLayout 指示器 (记得自己手动创建4个Fragment,注意是 app包下的Fragment 还是 V4包下的 Fragment)
//        tabLayout.addTab(tabLayout.newTab());
//        tabLayout.addTab(tabLayout.newTab());
//        tabLayout.addTab(tabLayout.newTab());
//        tabLayout.addTab(tabLayout.newTab());
//        // 使用 TabLayout 和 ViewPager 相关联
//        tabLayout.setupWithViewPager(viewPager);
//        // TabLayout指示器添加文本
//        tabLayout.getTabAt(0).setText("员工");
//        tabLayout.getTabAt(1).setText("企业");
//        tabLayout.getTabAt(2).setText("商家");
//        tabLayout.getTabAt(3).setText("游客");
    }

    @OnClick({R.id.image_back, R.id.tabLayout, R.id.viewPager})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.image_back:
                finish();
                break;
            case R.id.tabLayout:
                break;
            case R.id.viewPager:
                break;
        }
    }
}
