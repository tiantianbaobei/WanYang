package com.wanyangproject.shouye;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Gravity;
import android.view.View;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebResourceResponse;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.wanyangproject.R;
import com.wanyangproject.activity.YuanQuZhaoShangXiangQingActivity;
import com.wanyangproject.entity.FenXiangShiBaiEntity;
import com.wanyangproject.entity.WenZhangShouCangEntity;
import com.wanyangproject.entity.YuanQuZhaoShangXiangQingEntity;
import com.wanyangproject.entity.ZhaoPinFenXiangEntity;
import com.wanyangproject.my.ZhaoPinXiangQingActivity;
import com.wanyangproject.popuwindow.SharePopupWindow;
import com.wanyangproject.utils.ContractUtils;
import com.wanyangproject.utils.WebViewUtils;
import com.zhy.http.okhttp.OkHttpUtils;
import com.zhy.http.okhttp.callback.StringCallback;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.Call;

public class YuanQuZhaoShangBannerXiangQingActivity extends AppCompatActivity {

    @BindView(R.id.image_back)
    ImageView imageBack;
    @BindView(R.id.image_shoucang)
    ImageView imageShoucang;
    @BindView(R.id.image_fenxiang)
    ImageView imageFenxiang;
    @BindView(R.id.tv_title)
    TextView tvTitle;
    @BindView(R.id.tv_time)
    TextView tvTime;
    @BindView(R.id.webView)
    WebView webView;
    private String wenzhangid;
    private WebSettings mWebSettings;
    private String content;
    private YuanQuZhaoShangXiangQingEntity yuanQuZhaoShangXiangQingEntity;
    private SharePopupWindow sharePopupWindow;

    private ProgressDialog progressDialog;

    private static Boolean isOK = false;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_yuan_qu_zhao_shang_banner_xiang_qing);
        ButterKnife.bind(this);
        Intent intent = getIntent();
        wenzhangid = intent.getStringExtra("id");
        System.out.println(wenzhangid + "     banner接收的文章id");
        mWebSettings = webView.getSettings();
        mWebSettings.setJavaScriptEnabled(true);  //允许加载javascript
        webView.getSettings().setLayoutAlgorithm(WebSettings.LayoutAlgorithm.SINGLE_COLUMN);
        webView.getSettings().setLoadWithOverviewMode(true);
        mWebSettings.setSupportZoom(false);     //允许缩放
        mWebSettings.setBuiltInZoomControls(false); //原网页基础上缩放
        mWebSettings.setUseWideViewPort(false);   //任意比例缩放
//        mWebSettings.setUseWideViewPort(true); //将图片调整到适合webview的大小
//        mWebSettings.setLoadWithOverviewMode(true); // 缩放至屏幕的大小

        //      点击banner的详情的网络请求
        initYuaQuZhaoShangBannerXiangQingHttp();
//




        initView();



    }


//    企服中心分享的网络请求
    private void initQiFuFenXiangHttp() {
        OkHttpUtils.post()
                .url(ContractUtils.LOGIN_URL+"news/share")
                .addHeader("token",ContractUtils.getTOKEN(YuanQuZhaoShangBannerXiangQingActivity.this))
                .addParams("id",wenzhangid)
                .addParams("typeId","5")//0 =>园区手册1 => 智能制造2=>产品金融 3首页园区资讯, 4 政策法规, 5企服中心, 6园区招商
                .build()
                .execute(new StringCallback() {
                    @Override
                    public void onError(Call call, Exception e, int id) {

                    }

                    @Override
                    public void onResponse(String response, int id) {
                        System.out.println(response+"         企服中心分享的网络请求 ");
                        ContractUtils.Code500(YuanQuZhaoShangBannerXiangQingActivity.this,response);
                        if(response.indexOf("200") != -1){
                            Gson gson = new Gson();
                            ZhaoPinFenXiangEntity zhaoPinFenXiangEntity = gson.fromJson(response, ZhaoPinFenXiangEntity.class);
                            Object itemsOnClick = null;
                            Bitmap bitmap = BitmapFactory.decodeResource(getResources(), R.drawable.loginbaibian);
                            sharePopupWindow = new SharePopupWindow(YuanQuZhaoShangBannerXiangQingActivity.this,(View.OnClickListener) itemsOnClick,zhaoPinFenXiangEntity.getResponse().getTitle(),zhaoPinFenXiangEntity.getResponse().getUrl(),bitmap);
                            sharePopupWindow.showAtLocation(findViewById(R.id.relative_Layout), Gravity.BOTTOM | Gravity.CENTER_HORIZONTAL, 0, 0);
                        }else if(response.indexOf("400") != -1){
                            Gson gson = new Gson();
                            FenXiangShiBaiEntity fenXiangShiBaiEntity = gson.fromJson(response, FenXiangShiBaiEntity.class);
                            Toast.makeText(YuanQuZhaoShangBannerXiangQingActivity.this, fenXiangShiBaiEntity.getMsg(), Toast.LENGTH_SHORT).show();
                        }
                    }
                });
    }


    private void initView() {
        webView.setWebViewClient(new WebViewClient() {
            /**
             * 拦截 url 跳转,在里边添加点击链接跳转或者操作
             */
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, WebResourceRequest request) {
                return WebViewUtils.shouldOverrideUrlLoading(view,request,YuanQuZhaoShangBannerXiangQingActivity.this);
            }


            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                System.out.println("66666666666666666666");
                return WebViewUtils.shouldOverrideUrlLoading(view,url,YuanQuZhaoShangBannerXiangQingActivity.this);

            }




            /**
             * 在开始加载网页时会回调
             */
            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                super.onPageStarted(view, url, favicon);
                System.out.println("111111111111111111");
            }

            /**
             * 加载错误的时候会回调，在其中可做错误处理，比如再请求加载一次，或者提示404的错误页面
             */
            @Override
            public void onReceivedError(WebView view, WebResourceRequest request, WebResourceError error) {
                super.onReceivedError(view, request, error);

            }


            /**
             * 在结束加载网页时会回调
             */
            @Override
            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(view, url);
                System.out.println("chenggongchenggongchenggong");

                if(progressDialog != null){
                    progressDialog.dismiss();
                }


            }

            /**
             * 当接收到https错误时，会回调此函数，在其中可以做错误处理
             */
            @Override
            public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
                super.onReceivedError(view, errorCode, description, failingUrl);
            }

            /**
             * 在每一次请求资源时，都会通过这个函数来回调
             */
            public WebResourceResponse shouldInterceptRequest(WebView view, String url) {
                return null;
            }


            @Override
            public void onPageCommitVisible(WebView view, String url) {
                super.onPageCommitVisible(view, url);

                if(progressDialog != null){
                    progressDialog.dismiss();
                }

            }
        });
    }
















    //    点击banner的详情的网络请求
    private void initYuaQuZhaoShangBannerXiangQingHttp() {
        progressDialog = new ProgressDialog(YuanQuZhaoShangBannerXiangQingActivity.this);
        progressDialog.setTitle("提示");
        progressDialog.setMessage("请等待...");
        progressDialog.setCancelable(false);
        progressDialog.show();

        OkHttpUtils.post()
                .url(ContractUtils.LOGIN_URL + "news/getCateDetail")
                .addHeader("token", ContractUtils.getTOKEN(YuanQuZhaoShangBannerXiangQingActivity.this))
                .addParams("id", wenzhangid)
                .build()
                .execute(new StringCallback() {
                    @Override
                    public void onError(Call call, Exception e, int id) {
                        progressDialog.dismiss();
                    }

                    @Override
                    public void onResponse(String response, int id) {
                        ContractUtils.Code500(YuanQuZhaoShangBannerXiangQingActivity.this,response);
                        System.out.println(response + "     园区招商文章详情的网络请求");

                        if(response.indexOf("200") != -1){
                            Gson gson = new Gson();
                            yuanQuZhaoShangXiangQingEntity = gson.fromJson(response, YuanQuZhaoShangXiangQingEntity.class);
                            if(yuanQuZhaoShangXiangQingEntity.getResponse().getTitle() == null){

                            }else{ // 标题
                                tvTitle.setText(yuanQuZhaoShangXiangQingEntity.getResponse().getTitle());
                            }


                            if(yuanQuZhaoShangXiangQingEntity.getResponse().getAdd_time() == null){

                            }else{ // 时间
                                tvTime.setText(yuanQuZhaoShangXiangQingEntity.getResponse().getAdd_time());
                            }

                            if(yuanQuZhaoShangXiangQingEntity.getResponse().getContent() == null){

                            }else{
                                //                            内容
                                content = yuanQuZhaoShangXiangQingEntity.getResponse().getContent();
                                webView.loadDataWithBaseURL("http://www.baidu.com", getNewContent(content),"text/html", "UTF-8", null);
                            }
                        }
                    }
                });
    }

    @OnClick({R.id.image_back, R.id.image_shoucang, R.id.image_fenxiang, R.id.tv_title, R.id.tv_time})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.image_back:
                finish();
                break;
//            收藏
            case R.id.image_shoucang:
//                收藏的网络请求
                initShouCangHttp();
                break;
//            分享
            case R.id.image_fenxiang:
                //        企服中心分享的网络请求
                initQiFuFenXiangHttp();

                
                break;
            case R.id.tv_title:
                break;
            case R.id.tv_time:
                break;
        }
    }


//    收藏的网络请求
    private void initShouCangHttp() {
        OkHttpUtils.post()
                .url(ContractUtils.LOGIN_URL+"news/shoucang")
                .addHeader("token",ContractUtils.getTOKEN(YuanQuZhaoShangBannerXiangQingActivity.this))
                .addParams("id",wenzhangid)
                .addParams("typeId","5")//0 =>园区手册1 => 智能制造2=>产品金融 3首页园区资讯, 4 政策法规, 5企服中心, 6园区招商
                .build()
                .execute(new StringCallback() {
                    @Override
                    public void onError(Call call, Exception e, int id) {

                    }

                    @Override
                    public void onResponse(String response, int id) {
                        System.out.println(response+"          收藏的网络请求");
                        ContractUtils.Code500(YuanQuZhaoShangBannerXiangQingActivity.this,response);
                        Gson gson = new Gson();
                        WenZhangShouCangEntity wenZhangShouCangEntity = gson.fromJson(response, WenZhangShouCangEntity.class);
                        if(response.indexOf("200") != 1){
                            Toast.makeText(YuanQuZhaoShangBannerXiangQingActivity.this, wenZhangShouCangEntity.getMsg(), Toast.LENGTH_SHORT).show();
                            finish();
                        }else{
                            Toast.makeText(YuanQuZhaoShangBannerXiangQingActivity.this, wenZhangShouCangEntity.getMsg(), Toast.LENGTH_SHORT).show();
                            finish();
                        }
                    }
                });
    }


    /**
     * 将html文本内容中包含img标签的图片，宽度变为屏幕宽度，高度根据宽度比例自适应
     **/
    public static String getNewContent(String htmltext){
//        if(isOK == false){
//            return htmltext;
//        }
//
//        isOK = true;
        try {
            Document doc= Jsoup.parse(htmltext);
            Elements elements=doc.getElementsByTag("img");
            for (Element element : elements) {
                element.attr("width","100%").attr("height","auto");
            }

            return doc.toString();
        } catch (Exception e) {
            return htmltext;
        }
    }
}
