package com.wanyangproject.shouye;

import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.wanyangproject.R;
import com.wanyangproject.adapter.ChanYeJinRongAdapter;
import com.wanyangproject.fragment.ChanYeJinRongFragment;
import com.wanyangproject.fragment.ZhiNengZhiZaoFragment;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ChanYeJinRongActivity extends AppCompatActivity implements View.OnClickListener {

    @BindView(R.id.image_back)
    ImageView imageBack;
//    @BindView(R.id.recyclerView)
//    RecyclerView recyclerView;
    @BindView(R.id.tv_chanye_jinrong)
    TextView tvChanyeJinrong;
    @BindView(R.id.tv_zhineng_zhizao)
    TextView tvZhinengZhizao;
    private ChanYeJinRongAdapter chanYeJinRongAdapter;
    private ChanYeJinRongFragment chanYeJinRongFragment;
    private ZhiNengZhiZaoFragment zhiNengZhiZaoFragment;
    private FragmentManager fragmentManager;
    private Fragment hideFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chan_ye_jin_rong);
        ButterKnife.bind(this);
        if (Build.VERSION.SDK_INT >= 21) {
            View dview = getWindow().getDecorView();
            dview.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN | View.SYSTEM_UI_FLAG_LAYOUT_STABLE);
            getWindow().setStatusBarColor(Color.TRANSPARENT);
        }

//        initView();
        initView();
        initDefaultFragment();
    }


    private void initDefaultFragment() {
        fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.add(R.id.container_fragment, chanYeJinRongFragment);
        fragmentTransaction.commit();
        hideFragment = chanYeJinRongFragment;
    }


    private void replaceFragment(Fragment showFragment, FragmentTransaction fragmentTransaction) {
        if (showFragment.isAdded()) {
            fragmentTransaction.hide(hideFragment).show(showFragment).commit();
        } else {
            fragmentTransaction.hide(hideFragment).add(R.id.container_fragment, showFragment).commit();
        }
        hideFragment = showFragment;
    }

    //    初始化
    private void initView() {
        chanYeJinRongFragment = new ChanYeJinRongFragment();
        zhiNengZhiZaoFragment = new ZhiNengZhiZaoFragment();
        tvChanyeJinrong.setOnClickListener(this);
        tvZhinengZhizao.setOnClickListener(this);
    }

//    private void initView() {
//        chanYeJinRongAdapter = new ChanYeJinRongAdapter(image);
//        LinearLayoutManager manager = new LinearLayoutManager(ChanYeJinRongActivity.this);
//        recyclerView.setLayoutManager(manager);
//        recyclerView.setAdapter(chanYeJinRongAdapter);
//    }

    @OnClick({R.id.image_back})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.image_back:
                finish();
                break;
        }
    }

    @Override
    public void onClick(View view) {
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        switch (view.getId()){
            case R.id.tv_chanye_jinrong:
                tvChanyeJinrong.setBackgroundResource(R.drawable.yuanqu_tongzi_white_background);
                tvChanyeJinrong.setTextColor(getResources().getColor(R.color.red_color));
                tvZhinengZhizao.setBackgroundResource(R.drawable.yuanqu_tongzi_red_background);
                tvZhinengZhizao.setTextColor(getResources().getColor(R.color.white_color));
                replaceFragment(chanYeJinRongFragment, fragmentTransaction);
                break;
            case R.id.tv_zhineng_zhizao:
                tvZhinengZhizao.setBackgroundResource(R.drawable.yuanqu_tongzi_white_background_right);
                tvZhinengZhizao.setTextColor(getResources().getColor(R.color.red_color));
                tvChanyeJinrong.setBackgroundResource(R.drawable.yuanqu_tongzi_red_background_left);
                tvChanyeJinrong.setTextColor(getResources().getColor(R.color.white_color));
                replaceFragment(zhiNengZhiZaoFragment, fragmentTransaction);
                break;
        }
    }
}
