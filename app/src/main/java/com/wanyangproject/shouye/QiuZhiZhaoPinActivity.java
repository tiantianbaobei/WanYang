package com.wanyangproject.shouye;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;

import com.androidkun.xtablayout.XTabLayout;
import com.google.gson.Gson;
import com.wanyangproject.R;
import com.wanyangproject.adapter.TabAdapter;
import com.wanyangproject.entity.QiuZhiZhaoPinTitleEntity;
import com.wanyangproject.fragment.ShouYeZhaoPinFragment;
import com.wanyangproject.utils.ContractUtils;
import com.zhy.http.okhttp.OkHttpUtils;
import com.zhy.http.okhttp.callback.StringCallback;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.Call;

public class QiuZhiZhaoPinActivity extends AppCompatActivity {


    @BindView(R.id.image_back)
    ImageView imageBack;
    //    @BindView(R.id.tabLayout)
//    TabLayout tabLayout;
    @BindView(R.id.viewPager)
    ViewPager viewPager;
    @BindView(R.id.tabLayout)
    XTabLayout tabLayout;
    private List<String> stringList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_qiu_zhi_zhao_pin);
        ButterKnife.bind(this);
        initViewPager();

//        求职招聘的标题的网络请求
        initZhaoPinTiTleHttp();

    }

    //    求职招聘的标题的网络请求
    private void initZhaoPinTiTleHttp() {
        OkHttpUtils.post()
                .url(ContractUtils.LOGIN_URL + "zhaopin/getType")
                .addHeader("token", ContractUtils.getTOKEN(QiuZhiZhaoPinActivity.this))
                .build()
                .execute(new StringCallback() {
                    @Override
                    public void onError(Call call, Exception e, int id) {

                    }

                    @Override
                    public void onResponse(String response, int id) {
                        ContractUtils.Code500(QiuZhiZhaoPinActivity.this, response);
                        System.out.println(response + "      求职招聘标题的网络请求");

                        if (response.indexOf("200") != -1) {
                            Gson gson = new Gson();
                            QiuZhiZhaoPinTitleEntity qiuZhiZhaoPinTitleEntity = gson.fromJson(response, QiuZhiZhaoPinTitleEntity.class);
                            List<Fragment> fragments = new ArrayList<>();
                            for (int i = 0; i < qiuZhiZhaoPinTitleEntity.getResponse().size(); i++) {
                                stringList.add(qiuZhiZhaoPinTitleEntity.getResponse().get(i).getTitle());
                                ShouYeZhaoPinFragment shouYeZhaoPinFragment = new ShouYeZhaoPinFragment();
                                shouYeZhaoPinFragment.setTypeid(qiuZhiZhaoPinTitleEntity.getResponse().get(i).getId());
                                fragments.add(shouYeZhaoPinFragment);
                            }

                            // 创建ViewPager适配器
                            TabAdapter tabAdapter = new TabAdapter(getSupportFragmentManager(), fragments, stringList);
//                          luntanAdapter.setFragments(fragments);
                            // 给ViewPager设置适配器
                            viewPager.setAdapter(tabAdapter);
                            // 使用 TabLayout 和 ViewPager 相关联
                            tabLayout.setupWithViewPager(viewPager);
                        }
                    }
                });
    }

    private void initViewPager() {
//        List<Fragment> fragments = new ArrayList<>();
//        for (int i = 0; i < 30; i++) {
//            stringList.add("销售"+i);
//            fragments.add(new ShouYeZhaoPinFragment());
//        }
//
//        // 创建ViewPager适配器
//        TabAdapter tabAdapter = new TabAdapter(getSupportFragmentManager(),fragments,stringList);
////        luntanAdapter.setFragments(fragments);
//        // 给ViewPager设置适配器
//        viewPager.setAdapter(tabAdapter);
//        // 使用 TabLayout 和 ViewPager 相关联
//        tabLayout.setupWithViewPager(viewPager);
    }


    @OnClick({R.id.image_back, R.id.tabLayout, R.id.viewPager})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.image_back:
                finish();
                break;
            case R.id.tabLayout:
                break;
            case R.id.viewPager:
                break;
        }
    }
}
