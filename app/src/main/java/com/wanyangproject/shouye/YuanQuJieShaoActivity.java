package com.wanyangproject.shouye;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Gravity;
import android.view.View;
import android.webkit.JavascriptInterface;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebResourceResponse;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.wanyangproject.R;
import com.wanyangproject.entity.FenXiangShiBaiEntity;
import com.wanyangproject.entity.PanDuanShouCangEntity;
import com.wanyangproject.entity.WenZhangShouCangEntity;
import com.wanyangproject.entity.YuanQuShouCeXiangQingEntity;
import com.wanyangproject.entity.ZhaoPinFenXiangEntity;
import com.wanyangproject.my.ZhaoPinXiangQingActivity;
import com.wanyangproject.popuwindow.SharePopupWindow;
import com.wanyangproject.utils.ContractUtils;
import com.wanyangproject.utils.WebViewUtils;
import com.zhy.http.okhttp.OkHttpUtils;
import com.zhy.http.okhttp.callback.StringCallback;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.text.Collator;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.Call;

public class YuanQuJieShaoActivity extends AppCompatActivity {

    @BindView(R.id.image_back)
    ImageView imageBack;
    @BindView(R.id.image_shoucang)
    ImageView imageShoucang;
    @BindView(R.id.image_fenxiang)
    ImageView imageFenxiang;
    @BindView(R.id.tv_title)
    TextView tvTitle;
    @BindView(R.id.tv_time)
    TextView tvTime;
    @BindView(R.id.webView)
    WebView webView;
    private String id;
    private String content;
    private WebSettings mWebSettings;
    private String string;
    private String substring;
    private SharePopupWindow sharePopupWindow;
    private Boolean isLoadUrl = false;
    private YuanQuShouCeXiangQingEntity yuanQuShouCeXiangQingEntity;
    private ProgressDialog progressDialog;
    private static Boolean isOK = false;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_yuan_qu_jie_shao);
        ButterKnife.bind(this);
        if (Build.VERSION.SDK_INT >= 21) {
            View dview = getWindow().getDecorView();
            dview.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN | View.SYSTEM_UI_FLAG_LAYOUT_STABLE);
            getWindow().setStatusBarColor(Color.TRANSPARENT);
        }


        Intent intent = getIntent();
        id = intent.getStringExtra("id");
        System.out.println(id + "     接收的手册id");




        mWebSettings = webView.getSettings();
        mWebSettings.setJavaScriptEnabled(true);  //允许加载javascript
        webView.getSettings().setLayoutAlgorithm(WebSettings.LayoutAlgorithm.SINGLE_COLUMN);
        webView.getSettings().setLoadWithOverviewMode(true);
        mWebSettings.setSupportZoom(false);     //允许缩放
        mWebSettings.setBuiltInZoomControls(false); //原网页基础上缩放
        mWebSettings.setUseWideViewPort(false);   //任意比例缩放
//        mWebSettings.setUseWideViewPort(true); //将图片调整到适合webview的大小
//        mWebSettings.setLoadWithOverviewMode(true); // 缩放至屏幕的大小
        // 支持通过JS打开新窗口
        mWebSettings.setJavaScriptCanOpenWindowsAutomatically(true);
        mWebSettings.setLoadWithOverviewMode(true);
        //不显示webview缩放按钮
        mWebSettings.setDisplayZoomControls(false);
        webView.setLayerType(View.LAYER_TYPE_HARDWARE,null);//开启硬件加速
//        String cacheDirPath = getFilesDir().getAbsolutePath() + APP_CACAHE_DIRNAME;
//      String cacheDirPath = getCacheDir().getAbsolutePath()+Constant.APP_DB_DIRNAME;
        //设置数据库缓存路径
//        webView.getSettings().setDatabasePath(cacheDirPath);
        //设置  Application Caches 缓存目录
//        webView.getSettings().setAppCachePath(cacheDirPath);
        //开启 Application Caches 功能
        webView.getSettings().setAppCacheEnabled(true);
        webView.setWebViewClient(new WebViewClient());
        mWebSettings.setDomStorageEnabled(true);
        mWebSettings.setDatabaseEnabled(true);

        //        webView.setWebChromeClient(new WebChromeClient());
        webView.addJavascriptInterface(new JavaScriptInterface(), "JSInterface");


//        园区手册详情的id的网络请求
        initYuanQuShouCeXiangQingHttp();


//        是否收藏过文章的网络请求
        initShiFouShouCangHttp();


        //        我的收藏文章收藏的网络请求
//        initMyShouCangHttp();

        initView();
    }








//    是否收藏过文章的网络请求
    private void initShiFouShouCangHttp() {
        OkHttpUtils.post()
                .url(ContractUtils.LOGIN_URL+"news/shoucangjil")
                .addHeader("token",ContractUtils.getTOKEN(YuanQuJieShaoActivity.this))
                .addParams("id",id)
                .addParams("typeId","0")//0 园区手册   1 智能制造   2 产业金融   3 园区资讯   4 政策法规    5企服中心    6 园区招商
                .build()
                .execute(new StringCallback() {
                    @Override
                    public void onError(Call call, Exception e, int id) {
                        System.out.println(e+"      eeeeeee 园区手册是否收藏过文章的");
                    }

                    @Override
                    public void onResponse(String response, int id) {
                        System.out.println(response+"        园区手册是否收藏过文章的网络请求 ");
                        ContractUtils.Code500(YuanQuJieShaoActivity.this,response);
                        if(response.indexOf("200") != -1){
                            Gson gson = new Gson();
                            PanDuanShouCangEntity panDuanShouCangEntity = gson.fromJson(response, PanDuanShouCangEntity.class);
                            if(panDuanShouCangEntity.getResponse().getType().equals("1")){// 1 收藏    2 未收藏
                                imageShoucang.setImageResource(R.drawable.quxia_shoucang);
                            }else if(panDuanShouCangEntity.getResponse().getType().equals("2")){
                                imageShoucang.setImageResource(R.drawable.shoucang);
                            }
                        }
                    }
                });
    }






    //    我的收藏文章收藏的网络请求
    private void initMyShouCangHttp() {
        OkHttpUtils.post()
                .url(ContractUtils.LOGIN_URL+"user/myShoucang")
                .addHeader("token",ContractUtils.getTOKEN(YuanQuJieShaoActivity.this))
                .build()
                .execute(new StringCallback() {
                    @Override
                    public void onError(Call call, Exception e, int id) {
                        System.out.println(e+"         我的收藏文章eeeeeee ");
                    }

                    @Override
                    public void onResponse(String response, int id) {
                        System.out.println(response+"           我的收藏文章收藏的网络请求 ");
                        ContractUtils.Code500(YuanQuJieShaoActivity.this,response);

                        if(response.indexOf("200") != -1) {
                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                JSONArray response1 = jsonObject.getJSONArray("response");

                                Boolean isOK = false;
                                if(response1 != null){
                                    for (int i = 0; i < response1.length(); i++) {
                                        JSONObject json = (JSONObject) response1.get(i);
                                        if(json.getString("0").equals(id)){
                                            isOK = true;
                                        }
                                    }

                                    if(isOK == true){
                                        imageShoucang.setImageResource(R.drawable.quxia_shoucang);
                                    }
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                });
    }










    //    园区手册分享的网络请求
    private void initYuanQuShouCeFenXiangHttp() {
        OkHttpUtils.post()
                .url(ContractUtils.LOGIN_URL+"news/share")
                .addHeader("token",ContractUtils.getTOKEN(YuanQuJieShaoActivity.this))
                .addParams("id",id)
                .addParams("typeId","0")//0 =>园区手册1 => 智能制造2=>产品金融 3首页园区资讯, 4 政策法规, 5企服中心, 6园区招商
                .build()
                .execute(new StringCallback() {
                    @Override
                    public void onError(Call call, Exception e, int id) {
                        System.out.println(e+"          园区手册eeeee");
                    }

                    @Override
                    public void onResponse(String response, int id) {
                        ContractUtils.Code500(YuanQuJieShaoActivity.this,response);
                        System.out.println(response+"         园区手册分享的网络请求");
                        if(response.indexOf("200") != -1){
                            Gson gson = new Gson();
                            ZhaoPinFenXiangEntity zhaoPinFenXiangEntity = gson.fromJson(response, ZhaoPinFenXiangEntity.class);
                            Object itemsOnClick = null;

                            Bitmap bitmap = BitmapFactory.decodeResource(getResources(), R.drawable.loginbaibian);
                            sharePopupWindow = new SharePopupWindow(YuanQuJieShaoActivity.this,(View.OnClickListener) itemsOnClick,zhaoPinFenXiangEntity.getResponse().getTitle(),zhaoPinFenXiangEntity.getResponse().getUrl(),bitmap);
                            sharePopupWindow.showAtLocation(findViewById(R.id.relative_Layout), Gravity.BOTTOM | Gravity.CENTER_HORIZONTAL, 0, 0);
//                            ChuanZhiEntity.fenxiang = zhaoPinFenXiangEntity.getResponse();//传值
                        }else if(response.indexOf("400") != -1){
                            Gson gson = new Gson();
                            FenXiangShiBaiEntity fenXiangShiBaiEntity = gson.fromJson(response, FenXiangShiBaiEntity.class);
                            Toast.makeText(YuanQuJieShaoActivity.this, fenXiangShiBaiEntity.getMsg(), Toast.LENGTH_SHORT).show();
                        }
                    }
                });
    }



    private void initView() {
        webView.setWebViewClient(new WebViewClient() {
            /**
             * 拦截 url 跳转,在里边添加点击链接跳转或者操作
             */
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, WebResourceRequest request) {
                System.out.println("xxxxxxxxxxxxxxxxxxxxxxxxxx");
                return WebViewUtils.shouldOverrideUrlLoading(view,request,YuanQuJieShaoActivity.this);
            }


            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                System.out.println("66666666666666666666");
                return WebViewUtils.shouldOverrideUrlLoading(view,url,YuanQuJieShaoActivity.this);
            }


            /**
             * 在开始加载网页时会回调
             */
            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                super.onPageStarted(view, url, favicon);
                System.out.println("7777777777777");
                System.out.println(url+"          url");
                System.out.println(view+"           view");
                System.out.println(favicon+"             favicon");
            }

            /**
             * 加载错误的时候会回调，在其中可做错误处理，比如再请求加载一次，或者提示404的错误页面
             */
            @Override
            public void onReceivedError(WebView view, WebResourceRequest request, WebResourceError error) {
                super.onReceivedError(view, request, error);
            }


            /**
             * 在结束加载网页时会回调
             */
            @Override
            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(view, url);
                System.out.println("chenggongchenggongchenggong");
                if(progressDialog != null){
                    progressDialog.dismiss();
                }
            }

            /**
             * 当接收到https错误时，会回调此函数，在其中可以做错误处理
             */
            @Override
            public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
                super.onReceivedError(view, errorCode, description, failingUrl);
            }

            /**
             * 在每一次请求资源时，都会通过这个函数来回调
             */
            public WebResourceResponse shouldInterceptRequest(WebView view, String url) {
                return null;
            }

            @Override
            public void onPageCommitVisible(WebView view, String url) {
                super.onPageCommitVisible(view, url);
                if(progressDialog != null){
                    progressDialog.dismiss();
                }
            }
        });
    }














    //    园区手册详情的id的网络请求
    public void initYuanQuShouCeXiangQingHttp() {
        progressDialog = new ProgressDialog(YuanQuJieShaoActivity.this);
        progressDialog.setTitle("提示");
        progressDialog.setMessage("请等待...");
        progressDialog.setCancelable(false);
        progressDialog.show();

        OkHttpUtils.post()
                .url(ContractUtils.LOGIN_URL + "park/manualDetail")
                .addHeader("token", ContractUtils.getTOKEN(YuanQuJieShaoActivity.this))
                .addParams("id", id)
                .build()
                .execute(new StringCallback() {
                    @Override
                    public void onError(Call call, Exception e, int id) {
                        progressDialog.dismiss();
                    }

                    @Override
                    public void onResponse(String response, int id) {
//                        progressDialog.dismiss();
                        ContractUtils.Code500(YuanQuJieShaoActivity.this,response);
                        System.out.println(response + "         园区手册详情的网络请求");

                        if (response.indexOf("200") != -1) {
                            Gson gson = new Gson();
                            yuanQuShouCeXiangQingEntity = gson.fromJson(response, YuanQuShouCeXiangQingEntity.class);
                            if (yuanQuShouCeXiangQingEntity.getResponse().getTitle() == null) {

                            } else {
                                tvTitle.setText(yuanQuShouCeXiangQingEntity.getResponse().getTitle());
                            }


                            if (yuanQuShouCeXiangQingEntity.getResponse().getAdd_time() == null) {

                            } else {
                                tvTime.setText(yuanQuShouCeXiangQingEntity.getResponse().getAdd_time());
                            }

                            if (yuanQuShouCeXiangQingEntity.getResponse().getContent() == null) {

                            } else {
                                //                            内容
                                content = yuanQuShouCeXiangQingEntity.getResponse().getContent();
                                webView.loadDataWithBaseURL("http://www.baidu.com", getNewContent(content),"text/html", "UTF-8", null);
                            }
                        }
                    }
                });
    }


    /**
     * 将html文本内容中包含img标签的图片，宽度变为屏幕宽度，高度根据宽度比例自适应
     **/
    public static String getNewContent(String htmltext) {

//        if(isOK == false){
//            return htmltext;
//        }
//
//        isOK = true;
        try {
            Document doc = Jsoup.parse(htmltext);
            Elements elements = doc.getElementsByTag("img");
            for (Element element : elements) {
                element.attr("width", "100%").attr("height", "auto");
            }

            return doc.toString();
        } catch (Exception e) {
            return htmltext;
        }
    }



    @OnClick({R.id.image_back, R.id.image_shoucang, R.id.image_fenxiang})
    public void onViewClicked(View view) {
        switch (view.getId()) {
//            返回按钮
            case R.id.image_back:
                finish();
                break;
//            收藏
            case R.id.image_shoucang:
//            收藏的网络请求
                initShouCangHttp();
                break;
//            分享
            case R.id.image_fenxiang:
//        园区手册分享的网络请求
                initYuanQuShouCeFenXiangHttp();
                break;
        }
    }




//    园区手册收藏的网络请求
    private void initShouCangHttp() {
        OkHttpUtils.post()
                .url(ContractUtils.LOGIN_URL+"news/shoucang")
                .addHeader("token",ContractUtils.getTOKEN(YuanQuJieShaoActivity.this))
                .addParams("typeId","0")//0 =>园区手册1 => 智能制造2=>产品金融 3首页园区资讯, 4 政策法规, 5企服中心, 6园区招商
                .addParams("id",id)
                .addParams("title","手册:"+yuanQuShouCeXiangQingEntity.getResponse().getTitle())
                .build()
                .execute(new StringCallback() {
                    @Override
                    public void onError(Call call, Exception e, int id) {
                        System.out.println(e+"            shoucangeeeeeeeeee");
                    }

                    @Override
                    public void onResponse(String response, int id) {
                        ContractUtils.Code500(YuanQuJieShaoActivity.this,response);
                        System.out.println(response+"          园区手册收藏的网络请求");

                        if(response.indexOf("200") != -1){
                            Gson gson = new Gson();
                            WenZhangShouCangEntity wenZhangShouCangEntity = gson.fromJson(response, WenZhangShouCangEntity.class);
                            Toast.makeText(YuanQuJieShaoActivity.this, wenZhangShouCangEntity.getMsg(), Toast.LENGTH_SHORT).show();
                            if(wenZhangShouCangEntity.getMsg().equals("收藏成功")){
                                imageShoucang.setImageResource(R.drawable.quxia_shoucang);
                            }else if(wenZhangShouCangEntity.getMsg().equals("取消收藏成功")){
                                imageShoucang.setImageResource(R.drawable.shoucang);
                            }
                        }
                    }
                });
    }










    class JavaScriptInterface {
        @JavascriptInterface
        public void processFormInputs(String html) {
//            LogUtils.e("html-->" + html);
            System.out.println(html);
        }
    }
}
