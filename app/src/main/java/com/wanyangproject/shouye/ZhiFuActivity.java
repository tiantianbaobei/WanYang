package com.wanyangproject.shouye;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.alipay.sdk.app.PayTask;
import com.google.gson.Gson;
import com.tencent.mm.opensdk.constants.ConstantsAPI;
import com.tencent.mm.opensdk.modelpay.PayReq;
import com.tencent.mm.opensdk.openapi.IWXAPI;
import com.tencent.mm.opensdk.openapi.WXAPIFactory;
import com.wanyangproject.R;
import com.wanyangproject.entity.ZhiFuBaoEntity;
import com.wanyangproject.entity.ZhiFuEntity;
import com.wanyangproject.fragment.ShngJiaPeiSongFragment;
import com.wanyangproject.utils.AuthResult;
import com.wanyangproject.utils.ContractUtils;
import com.wanyangproject.utils.OrderInfoUtil2_0;
import com.wanyangproject.utils.PayResult;
import com.zhy.http.okhttp.OkHttpUtils;
import com.zhy.http.okhttp.callback.StringCallback;

import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.Call;

//支付界面
public class ZhiFuActivity extends AppCompatActivity {

    @BindView(R.id.image_back)
    ImageView imageBack;
    @BindView(R.id.image_zhifubao)
    ImageView imageZhifubao;
    @BindView(R.id.image_weixin)
    ImageView imageWeixin;
    @BindView(R.id.btn_zhifu)
    Button btnZhifu;
    @BindView(R.id.tv_money)
    TextView tvMoney;
    //    微信的APP ID
    private static final String APP_ID_WEChat = "wx363ae6178db5e958";
    //    IWXAPI是第三方app和微信通信的openapi接口
    public static IWXAPI iwxapi;
//    private float shifukuan;
    private String dingdanhao;
    private ZhiFuEntity zhiFuEntity;
    private String format;
    private String shifukuan;
    private String addressid;
    private int isOK = 0;
    private NetWork netWork;
    private String cheng;
    private Boolean panduan = true;   //true为支付宝
    private String time;
    private String beizhu;
    private String info = "";
    private String money;



    /** 支付宝支付业务：入参app_id */
    public static final String APPID = "";
    public static final String RSA2_PRIVATE = "fBn6nNyiXEOH3MSP9Y2JVebFLCHP9h90T6t/zehqaklI3RZzuObJZiXEg55AKqc/VHkn9HJrklvZgh/qHXk3Cpz7lCfAFW60way63021M873u4oC/IMiTWw8arP4nBS1AViqN+IgKr93WZ9OYYM0p28pwhAstSSSzN+t/SFpa+7jNO1sVYFxDKdMmU0gzwgrGqspuUlSvPd0C+TUBqYyRGpI1m2PTnxlc+oJevkgeHO0Kwg0VdflTafJFs7LCsF6yhUKTjoxu4xr3Ty70/Si/aDk6aPoNt419nAbR4mqq6VGg68APBpuvrZTEGL4Jowv4BUWXZEBVVGgge8Hsah5/g==";
    public static final String RSA_PRIVATE = "jkyhgggg23";

    private static final int SDK_PAY_FLAG = 1;
    private static final int SDK_AUTH_FLAG = 2;





    private Handler mHandler = new Handler() {
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case SDK_PAY_FLAG: {
//                    PayResult payResult = new PayResult((Map<String, String>) msg.obj);

                    String s = msg.obj.toString();
                    String[] split = s.split("\\;");
                    System.out.println(s);

                    /**
                     对于支付结果，请商户依赖服务端的异步通知结果。同步通知结果，仅作为支付结束的通知。
                     */
//                    String resultInfo = payResult.getResult();// 同步返回需要验证的信息
//                    String resultStatus = payResult.getResultStatus();
                    // 判断resultStatus 为9000则代表支付成功
                    if (split[0].contains("9000")) {
                        // 该笔订单是否真实支付成功，需要依赖服务端的异步通知。
                        Toast.makeText(ZhiFuActivity.this, "支付成功", Toast.LENGTH_SHORT).show();
                        Intent intent1=new Intent();
                        intent1.putExtra("finish","finish");
                        intent1.setAction("finish");
                        sendBroadcast(intent1);

                        finish();
                    } else {
                        // 该笔订单真实的支付结果，需要依赖服务端的异步通知。
                        Toast.makeText(ZhiFuActivity.this, "支付失败", Toast.LENGTH_SHORT).show();
                    }
                    break;
                }
                default:
                    break;
            }
        }
    };





    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_zhi_fu);
        ButterKnife.bind(this);
        if (Build.VERSION.SDK_INT >= 21) {
            View dview = getWindow().getDecorView();
            dview.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN | View.SYSTEM_UI_FLAG_LAYOUT_STABLE);
            getWindow().setStatusBarColor(Color.TRANSPARENT);
        }


//        System.out.println(shifukuan+"     实付款");
//        System.out.println(dingdanhao+"     订单号");
//        System.out.println(addressid  +"          地址id");
//        System.out.println(time+"         时间");
//        System.out.println(beizhu+"      备注");


        Intent intent = getIntent();
        dingdanhao = intent.getStringExtra("dingdanhao");
        money = intent.getStringExtra("money");
        float zongmoney = Float.parseFloat(money);
        String zongjine = String.format("%.2f",zongmoney);

//        shifukuan = intent.getFloatExtra("shifikuan", 0);
//        shifukuan = intent.getStringExtra("shifikuan");
//        dingdanhao = intent.getStringExtra("dingdanhao");
//        addressid = intent.getStringExtra("addressid");
//        time = intent.getStringExtra("time");
//        beizhu = intent.getStringExtra("beizhu");
//        format = String.format("%.2f", shifukuan/1000.0);
//        System.out.println(format+"    format");

//        System.out.println(dingdanhao+"      支付界面接收的订单号");
//        System.out.println(shifukuan+"    支付界面接收的实付款");
//        System.out.println(addressid+"        addressid");
        if(zongjine.equals("")){

        }else{
            tvMoney.setText(zongjine+"元");
        }



        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("wxpay");
        netWork = new NetWork();
        registerReceiver(netWork, intentFilter);
        imageZhifubao.setImageResource(R.drawable.checked);
        imageWeixin.setImageResource(R.drawable.unchecked);

        //        微信
        regToWx();
    }



    class NetWork extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {

            cheng = intent.getStringExtra("cheng");
            if(cheng.equals("0")){
                finish();
            } else{

            }
        }
    }





//    微信
    private void regToWx() {
        //        通过WXAPOFactory工厂，获取IWXAPI的实例
        iwxapi = WXAPIFactory.createWXAPI(this, APP_ID_WEChat);
//        将应用的APP ID注册到微信
        iwxapi.registerApp(APP_ID_WEChat);
    }

    @OnClick({R.id.image_back, R.id.image_zhifubao, R.id.image_weixin, R.id.btn_zhifu,R.id.tv_money})
    public void onViewClicked(View view) {
        switch (view.getId()) {
//            实付款
            case R.id.tv_money:
                break;
//            返回按钮
            case R.id.image_back:
                finish();
                break;
//            支付宝按钮
            case R.id.image_zhifubao:
                panduan = true; //支付宝
//                isOK = 0;
                imageZhifubao.setImageResource(R.drawable.checked);
                imageWeixin.setImageResource(R.drawable.unchecked);
                break;
//            微信按钮
            case R.id.image_weixin:
                panduan = false;
//                isOK = 1;
                imageWeixin.setImageResource(R.drawable.checked);
                imageZhifubao.setImageResource(R.drawable.unchecked);
                break;
//            支付按钮
            case R.id.btn_zhifu:
                initZhiFuHttp();
                break;
        }
    }








    //    微信支付的网络请求
    private void initZhiFuHttp() {
        if(panduan){ // isOK = 0是支付宝支付
            final ProgressDialog progressDialog = new ProgressDialog(ZhiFuActivity.this);
            progressDialog.setTitle("提示");
            progressDialog.setMessage("请等待...");
            progressDialog.setCancelable(false);
            progressDialog.show();
                OkHttpUtils.post()
                        .url(ContractUtils.LOGIN_URL+"order/toxml")
                        .addHeader("token",ContractUtils.getTOKEN(ZhiFuActivity.this))
                        .addParams("order_sn",dingdanhao)
                        .addParams("money",money+"")
                        .addParams("type","1")//支付方式 1：支付宝2：微信3：余额
                        .build()
                        .execute(new StringCallback() {
                            @Override
                            public void onError(Call call, Exception e, int id) {
                                System.out.println(e+"     支付宝eee");
                                progressDialog.dismiss();
                            }

                            @Override
                            public void onResponse(String response, int id) {
                                ContractUtils.Code500(ZhiFuActivity.this,response);
                                System.out.println(response+"         支付宝地址");
                                progressDialog.dismiss();

                                if(response.indexOf("200") != -1){
                                    Gson gson = new Gson();
                                    zhiFuEntity = gson.fromJson(response, ZhiFuEntity.class);
                                    System.out.println(zhiFuEntity.getResponse().getSign()+"                     zhiFuEntity.getResponse().getSign()");
                                    final String orderInfo = zhiFuEntity.getResponse().getSign();   // 订单信息
//                                    final String orderInfo = "alipay_sdk=alipay-sdk-php-20180705&app_id=2018081761082719&biz_content=%7B%22body%22%3A%22%5Cu4e07%5Cu9633%5Cu5546%5Cu54c1%5Cu652f%5Cu4ed8%22%2C%22subject%22%3A%22%5Cu4e07%5Cu9633%5Cu5546%5Cu54c1%5Cu652f%5Cu4ed8%22%2C%22out_trade_no%22%3A%22pk2sn20180915161517%22%2C%22timeout_express%22%3A%2230m%22%2C%22total_amount%22%3A13%2C%22product_code%22%3A%22QUICK_MSECURITY_PAY%22%7D&charset=UTF-8&format=json&method=od=alipay.trade.app.pay&not&notify_url=http%3A%2F%2F%2Fpark.hostop.net%2Fa%2Fapi%2FCallback%2FaliPayBack&sign_type=RSA2&timestamp=2018-09-15+16%3A16%3A40&version=1.0&sign=YlXWgZzSI69B6%2BQQyZSiGp9EAQNDPIBG%2BXUIr8lto9YI9qKK7CVH7P8PAGGfHwPi0p2twj0ad0LEuRafhUC%2BxfTUVxhlMRt3UuH%2Br3cpqjgC6f1KA11YpDwt%2FApO7Z%2BKM1S%2BFcqMCkAu8sfrrEPpuIDNTeDaNyQRefSPeuj%2FuL1WwgKpgX3Os758ip40F5POtoDdmWHr3%2Bj7yXXtqcQiTi63zwsNBWthLq8Z7scVWIqNlGpDXoPYBXaWqBFGOts0UhDGBpcAC7Nd0LjgXeB4gIA5DpDeQKn%2BNqxMjIiZO4JBSZP9O%2F7eN4Bd7eTKkNMqX7V%2FlpMo5IhyLZ%2B7GTQE5g%3D%3D";   // 订单信息


                                    Runnable payRunnable = new Runnable() {
                                        @Override
                                        public void run() {
                                            System.out.println(zhiFuEntity.getResponse().getSign()+"                       System.out.printl11111111111111111111111");
                                            PayTask alipay = new PayTask(ZhiFuActivity.this);
                                            String result = alipay.pay(orderInfo,true);
                                            Message msg = new Message();
                                            msg.what = SDK_PAY_FLAG;
                                            msg.obj = result;
                                            mHandler.sendMessage(msg);
                                        }
                                    };
                                    // 必须异步调用
                                    Thread payThread = new Thread(payRunnable);
                                    payThread.start();
                                }
                            }
                        });

        }else{ // isOK = 1是微信支付

            final ProgressDialog progressDialog = new ProgressDialog(ZhiFuActivity.this);
            progressDialog.setTitle("提示");
            progressDialog.setMessage("请等待...");
            progressDialog.setCancelable(false);
            progressDialog.show();
                OkHttpUtils.post()
                        .url(ContractUtils.LOGIN_URL+"order/toxml")
                        .addHeader("token",ContractUtils.getTOKEN(ZhiFuActivity.this))
                        .addParams("order_sn",dingdanhao)
                        .addParams("money",money+"")
                        .addParams("type","2")//支付方式 1：支付宝2：微信3：余额
                        .build()
                        .execute(new StringCallback() {
                            @Override
                            public void onError(Call call, Exception e, int id) {
                                System.out.println(e+"     微信eeee");
                                progressDialog.dismiss();
                            }

                            @Override
                            public void onResponse(String response, int id) {
                                ContractUtils.Code500(ZhiFuActivity.this,response);
                                System.out.println(response+"     微信支付的网络请求");
                                progressDialog.dismiss();

                                if(response.indexOf("200") != -1){
                                    Gson gson = new Gson();
                                    zhiFuEntity = gson.fromJson(response, ZhiFuEntity.class);
                                    IWXAPI api;
                                    PayReq request = new PayReq();
                                    request.appId = zhiFuEntity.getResponse().getAppid();
                                    request.partnerId = zhiFuEntity.getResponse().getPartnerid();
                                    request.prepayId= zhiFuEntity.getResponse().getPrepayid();
                                    request.packageValue = zhiFuEntity.getResponse().getPackageX();
                                    request.nonceStr= zhiFuEntity.getResponse().getNoncestr();
                                    request.timeStamp= zhiFuEntity.getResponse().getTimestamp();
                                    request.sign= zhiFuEntity.getResponse().getSign();
                                    iwxapi.sendReq(request);
                                }
                            }
                        });
        }






















////    微信支付的网络请求
//    private void initZhiFuHttp() {
//        if(panduan){ // isOK = 0是支付宝支付
//            if(addressid != null){
//                OkHttpUtils.post()
//                        .url(ContractUtils.LOGIN_URL+"Life/toxml")
//                        .addHeader("token",ContractUtils.getTOKEN(ZhiFuActivity.this))
//                        .addParams("order_sn",dingdanhao)
//                        .addParams("money",shifukuan+"")
//                        .addParams("adders_id",addressid)
//                        .addParams("Remarks", beizhu)
//                        .addParams("type","1")//支付方式 1：支付宝2：微信3：余额
//                        .build()
//                        .execute(new StringCallback() {
//                            @Override
//                            public void onError(Call call, Exception e, int id) {
//                                System.out.println(e+"     支付宝eee");
//                            }
//
//                            @Override
//                            public void onResponse(String response, int id) {
//                                ContractUtils.Code500(ZhiFuActivity.this,response);
//                                System.out.println(response+"         支付宝地址");
//
//                                if(response.indexOf("200") != -1){
//                                    Gson gson = new Gson();
//                                    zhiFuEntity = gson.fromJson(response, ZhiFuEntity.class);
//                                    System.out.println(zhiFuEntity.getResponse().getSign()+"                     zhiFuEntity.getResponse().getSign()");
//                                    final String orderInfo = zhiFuEntity.getResponse().getSign();   // 订单信息
////                                    final String orderInfo = "alipay_sdk=alipay-sdk-php-20180705&app_id=2018081761082719&biz_content=%7B%22body%22%3A%22%5Cu4e07%5Cu9633%5Cu5546%5Cu54c1%5Cu652f%5Cu4ed8%22%2C%22subject%22%3A%22%5Cu4e07%5Cu9633%5Cu5546%5Cu54c1%5Cu652f%5Cu4ed8%22%2C%22out_trade_no%22%3A%22pk2sn20180915161517%22%2C%22timeout_express%22%3A%2230m%22%2C%22total_amount%22%3A13%2C%22product_code%22%3A%22QUICK_MSECURITY_PAY%22%7D&charset=UTF-8&format=json&method=od=alipay.trade.app.pay&not&notify_url=http%3A%2F%2F%2Fpark.hostop.net%2Fa%2Fapi%2FCallback%2FaliPayBack&sign_type=RSA2&timestamp=2018-09-15+16%3A16%3A40&version=1.0&sign=YlXWgZzSI69B6%2BQQyZSiGp9EAQNDPIBG%2BXUIr8lto9YI9qKK7CVH7P8PAGGfHwPi0p2twj0ad0LEuRafhUC%2BxfTUVxhlMRt3UuH%2Br3cpqjgC6f1KA11YpDwt%2FApO7Z%2BKM1S%2BFcqMCkAu8sfrrEPpuIDNTeDaNyQRefSPeuj%2FuL1WwgKpgX3Os758ip40F5POtoDdmWHr3%2Bj7yXXtqcQiTi63zwsNBWthLq8Z7scVWIqNlGpDXoPYBXaWqBFGOts0UhDGBpcAC7Nd0LjgXeB4gIA5DpDeQKn%2BNqxMjIiZO4JBSZP9O%2F7eN4Bd7eTKkNMqX7V%2FlpMo5IhyLZ%2B7GTQE5g%3D%3D";   // 订单信息
//
//
//                                    Runnable payRunnable = new Runnable() {
//                                        @Override
//                                        public void run() {
//                                            System.out.println(zhiFuEntity.getResponse().getSign()+"                       System.out.printl11111111111111111111111");
//                                            PayTask alipay = new PayTask(ZhiFuActivity.this);
//                                            String result = alipay.pay(orderInfo,true);
//                                            Message msg = new Message();
//                                            msg.what = SDK_PAY_FLAG;
//                                            msg.obj = result;
//                                            mHandler.sendMessage(msg);
//                                        }
//                                    };
//                                    // 必须异步调用
//                                    Thread payThread = new Thread(payRunnable);
//                                    payThread.start();
//                                }
//                            }
//                        });
//            }else if(time != null){
//                System.out.println(ContractUtils.getTOKEN(ZhiFuActivity.this)+"          token ");
//                System.out.println(dingdanhao+"        order_sn ");
//                System.out.println(shifukuan+"          money");
//                System.out.println(time+"           Extraction ");
//                System.out.println(beizhu+"          Remarks ");
//                OkHttpUtils.post()
//                        .url(ContractUtils.LOGIN_URL+"Life/toxml")
//                        .addHeader("token",ContractUtils.getTOKEN(ZhiFuActivity.this))
//                        .addParams("order_sn",dingdanhao)
//                        .addParams("money",shifukuan+"")
//                        .addParams("Extraction",time)
//                        .addParams("Remarks", beizhu)
//                        .addParams("type","1")//支付方式 1：支付宝2：微信3：余额
//                        .build()
//                        .execute(new StringCallback() {
//                            @Override
//                            public void onError(Call call, Exception e, int id) {
//                                System.out.println(e+"     支付宝eeeee");
//                            }
//
//                            @Override
//                            public void onResponse(String response, int id) {
//                                System.out.println(response+"        支付宝时间");
//                                ContractUtils.Code500(ZhiFuActivity.this,response);
//                                if(response.indexOf("200") != -1){
//                                    Gson gson = new Gson();
//                                    zhiFuEntity = gson.fromJson(response, ZhiFuEntity.class);
//                                    System.out.println(zhiFuEntity.getResponse().getSign()+"                     zhiFuEntity.getResponse().getSign()");
//                                    final String orderInfo = zhiFuEntity.getResponse().getSign();   // 订单信息
////                                    final String orderInfo ="alipay_sdk=alipay-sdk-php-20180705&app_id=2018081761082719&biz_content=%7B%22body%22%3A%22%5Cu4e07%5Cu9633%5Cu5546%5Cu54c1%5Cu652f%5Cu4ed8%22%2C%22subject%22%3A%22%5Cu4e07%5Cu9633%5Cu5546%5Cu54c1%5Cu652f%5Cu4ed8%22%2C%22out_trade_no%22%3A%22pk2sn20180915161517%22%2C%22timeout_express%22%3A%2230m%22%2C%22total_amount%22%3A13%2C%22product_code%22%3A%22QUICK_MSECURITY_PAY%22%7D&charset=UTF-8&format=json&method=od=alipay.trade.app.pay&not&notify_url=http%3A%2F%2F%2Fpark.hostop.net%2Fa%2Fapi%2FCallback%2FaliPayBack&sign_type=RSA2&timestamp=2018-09-15+16%3A16%3A40&version=1.0&sign=YlXWgZzSI69B6%2BQQyZSiGp9EAQNDPIBG%2BXUIr8lto9YI9qKK7CVH7P8PAGGfHwPi0p2twj0ad0LEuRafhUC%2BxfTUVxhlMRt3UuH%2Br3cpqjgC6f1KA11YpDwt%2FApO7Z%2BKM1S%2BFcqMCkAu8sfrrEPpuIDNTeDaNyQRefSPeuj%2FuL1WwgKpgX3Os758ip40F5POtoDdmWHr3%2Bj7yXXtqcQiTi63zwsNBWthLq8Z7scVWIqNlGpDXoPYBXaWqBFGOts0UhDGBpcAC7Nd0LjgXeB4gIA5DpDeQKn%2BNqxMjIiZO4JBSZP9O%2F7eN4Bd7eTKkNMqX7V%2FlpMo5IhyLZ%2B7GTQE5g%3D%3D";   // 订单信息
//
//                                    Runnable payRunnable = new Runnable() {
//                                        @Override
//                                        public void run() {
//                                            System.out.println(zhiFuEntity.getResponse().getSign()+"                       System.out.2222222222222222222");
//                                            PayTask alipay = new PayTask(ZhiFuActivity.this);
//                                            String result = alipay.pay(orderInfo,true);
//                                            Message msg = new Message();
//                                            msg.what = SDK_PAY_FLAG;
//                                            msg.obj = result;
//                                            mHandler.sendMessage(msg);
//                                        }
//                                    };
//                                    // 必须异步调用
//                                    Thread payThread = new Thread(payRunnable);
//                                    payThread.start();
//                                }
//                            }
//                        });
//            }
//        }else{ // isOK = 1是微信支付
//
//            System.out.println(ContractUtils.getTOKEN(ZhiFuActivity.this)+"          token ");
//            System.out.println(dingdanhao+"        order_sn ");
//            System.out.println(shifukuan+"          money");
//            System.out.println(addressid+"           adders_id ");
//            System.out.println(beizhu+"          Remarks ");
//
//            if(addressid != null){
//                OkHttpUtils.post()
//                        .url(ContractUtils.LOGIN_URL+"Life/toxml")
//                        .addHeader("token",ContractUtils.getTOKEN(ZhiFuActivity.this))
//                        .addParams("order_sn",dingdanhao)
//                        .addParams("money",shifukuan+"")
//                        .addParams("adders_id",addressid)
//                        .addParams("Remarks", beizhu)
//                        .addParams("type","2")//支付方式 1：支付宝2：微信3：余额
//                        .build()
//                        .execute(new StringCallback() {
//                            @Override
//                            public void onError(Call call, Exception e, int id) {
//                                System.out.println(e+"     微信eeee");
//                            }
//
//                            @Override
//                            public void onResponse(String response, int id) {
//                                ContractUtils.Code500(ZhiFuActivity.this,response);
//                                System.out.println(response+"     微信支付的网络请求");
//
//                                if(response.indexOf("200") != -1){
//                                    Gson gson = new Gson();
//                                    zhiFuEntity = gson.fromJson(response, ZhiFuEntity.class);
//                                    IWXAPI api;
//                                    PayReq request = new PayReq();
//                                    request.appId = zhiFuEntity.getResponse().getAppid();
//                                    request.partnerId = zhiFuEntity.getResponse().getPartnerid();
//                                    request.prepayId= zhiFuEntity.getResponse().getPrepayid();
//                                    request.packageValue = zhiFuEntity.getResponse().getPackageX();
//                                    request.nonceStr= zhiFuEntity.getResponse().getNoncestr();
//                                    request.timeStamp= zhiFuEntity.getResponse().getTimestamp();
//                                    request.sign= zhiFuEntity.getResponse().getSign();
//                                    iwxapi.sendReq(request);
//                                }
//                            }
//                        });
//            }else if(time != null){
//                System.out.println(ContractUtils.getTOKEN(ZhiFuActivity.this)+"          token ");
//                System.out.println(dingdanhao+"        order_sn ");
//                System.out.println(shifukuan+"          money");
//                System.out.println(time+"           Extraction ");
//                System.out.println(beizhu+"          Remarks ");
//                OkHttpUtils.post()
//                        .url(ContractUtils.LOGIN_URL+"Life/toxml")
//                        .addHeader("token",ContractUtils.getTOKEN(ZhiFuActivity.this))
//                        .addParams("order_sn",dingdanhao)
//                        .addParams("money",shifukuan+"")
//                        .addParams("Extraction",time)
//                        .addParams("Remarks", beizhu)
//                        .addParams("type","2")//支付方式 1：支付宝2：微信3：余额
//                        .build()
//                        .execute(new StringCallback() {
//                            @Override
//                            public void onError(Call call, Exception e, int id) {
//                                System.out.println(e+"     微信eeee");
//                            }
//
//                            @Override
//                            public void onResponse(String response, int id) {
//                                ContractUtils.Code500(ZhiFuActivity.this,response);
//                                System.out.println(response+"     微信支付的网络请求");
//
//                                if(response.indexOf("200") != -1){
//                                    Gson gson = new Gson();
//                                    zhiFuEntity = gson.fromJson(response, ZhiFuEntity.class);
//                                    IWXAPI api;
//                                    PayReq request = new PayReq();
//                                    request.appId = zhiFuEntity.getResponse().getAppid();
//                                    request.partnerId = zhiFuEntity.getResponse().getPartnerid();
//                                    request.prepayId= zhiFuEntity.getResponse().getPrepayid();
//                                    request.packageValue = zhiFuEntity.getResponse().getPackageX();
//                                    request.nonceStr= zhiFuEntity.getResponse().getNoncestr();
//                                    request.timeStamp= zhiFuEntity.getResponse().getTimestamp();
//                                    request.sign= zhiFuEntity.getResponse().getSign();
//                                    iwxapi.sendReq(request);
//
//                                }
//                            }
//                        });
//            }
//        }

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (netWork!=null){
            unregisterReceiver(netWork);
        }
    }

    //
//    @SuppressLint("HandlerLeak")
//    private Handler mHandler = new Handler() {
//        @SuppressWarnings("unused")
//        public void handleMessage(Message msg) {
//            switch (msg.what) {
//                case SDK_PAY_FLAG: {
//                    @SuppressWarnings("unchecked")
//                    PayResult payResult = new PayResult((Map<String, String>) msg.obj);
//                    System.out.println(payResult+"            payResultpayResultpayResult");
//                    /**
//                     对于支付结果，请商户依赖服务端的异步通知结果。同步通知结果，仅作为支付结束的通知。
//                     */
//                    String resultInfo = payResult.getResult();// 同步返回需要验证的信息
//                    String resultStatus = payResult.getResultStatus();
//                    // 判断resultStatus 为9000则代表支付成功
//                    if (TextUtils.equals(resultStatus, "9000")) {
//                        // 该笔订单是否真实支付成功，需要依赖服务端的异步通知。
//                        Toast.makeText(ZhiFuActivity.this, "支付成功", Toast.LENGTH_SHORT).show();
//                    } else {
//                        // 该笔订单真实的支付结果，需要依赖服务端的异步通知。
//                        Toast.makeText(ZhiFuActivity.this, "支付失败", Toast.LENGTH_SHORT).show();
//                    }
//                    break;
//                }
//                case SDK_AUTH_FLAG: {
//                    @SuppressWarnings("unchecked")
//                    AuthResult authResult = new AuthResult((Map<String, String>) msg.obj, true);
//                    String resultStatus = authResult.getResultStatus();
//                    System.out.println(resultStatus +"                resultStatusresultStatusresultStatus");
//
//                    // 判断resultStatus 为“9000”且result_code
//                    // 为“200”则代表授权成功，具体状态码代表含义可参考授权接口文档
//                    if (TextUtils.equals(resultStatus, "9000") && TextUtils.equals(authResult.getResultCode(), "200")) {
//                        // 获取alipay_open_id，调支付时作为参数extern_token 的value
//                        // 传入，则支付账户为该授权账户
//                        Toast.makeText(ZhiFuActivity.this,
//                                "授权成功\n" + String.format("authCode:%s", authResult.getAuthCode()), Toast.LENGTH_SHORT)
//                                .show();
//                    } else {
//                        // 其他状态值则为授权失败
//                        Toast.makeText(ZhiFuActivity.this,
//                                "授权失败" + String.format("authCode:%s", authResult.getAuthCode()), Toast.LENGTH_SHORT).show();
//                    }
//                    break;
//                }
//                default:
//                    break;
//            }
//        };
//    };
//



//




}
