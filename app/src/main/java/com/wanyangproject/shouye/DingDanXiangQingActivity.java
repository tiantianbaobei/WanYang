package com.wanyangproject.shouye;

import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.NumberPicker;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bigkoo.pickerview.TimePickerView;
import com.google.gson.Gson;
import com.wanyangproject.R;
import com.wanyangproject.adapter.DingDanXiangQingAdapter;
import com.wanyangproject.entity.NumberEntity;
import com.wanyangproject.entity.TiJiaoDingDanEntity;
import com.wanyangproject.entity.TiJiaoDingDanXiangQingEntity;
import com.wanyangproject.fragment.DaoDianZiQuFragment;
import com.wanyangproject.fragment.ShngJiaPeiSongFragment;
import com.wanyangproject.utils.ContractUtils;
import com.zhy.http.okhttp.OkHttpUtils;
import com.zhy.http.okhttp.callback.StringCallback;

import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.Call;

//  订单详情页
public class DingDanXiangQingActivity extends AppCompatActivity implements View.OnClickListener {

    @BindView(R.id.image_back)
    ImageView imageBack;
    //    @BindView(R.id.tabLayout)
//    TabLayout tabLayout;
//    @BindView(R.id.viewPager)
//    ViewPager viewPager;
//    @BindView(R.id.et_beizhu)
//    EditText etBeizhu;
    @BindView(R.id.tv_time)
    TextView tvTime;
    @BindView(R.id.tv_jiesuan)
    TextView tvJiesuan;
    @BindView(R.id.relative_time)
    RelativeLayout relativeTime;
    @BindView(R.id.tv_shangjia_peisong)
    TextView tvShangjiaPeisong;
    @BindView(R.id.tv_daodian_ziqu)
    TextView tvDaodianZiqu;
    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    @BindView(R.id.tv_dianming)
    TextView tvDianming;
    @BindView(R.id.tv_peisongfei_money)
    TextView tvPeisongfeiMoney;
    @BindView(R.id.tv_zongji_money)
    TextView tvZongjiMoney;
    @BindView(R.id.tv_shifu_money)
    TextView tvShifuMoney;
    @BindView(R.id.tv_shijifukuan)
    TextView tvShijifukuan;
    @BindView(R.id.tv_shiji_money)
    TextView tvShijiMoney;
    @BindView(R.id.et_beizhu)
    EditText etBeizhu;
    private TextView tv_quxiao, tv_queding;
    private NumberPicker numberPicker_year, numberPicker_month, numberPicker_day, numberPicker_hour, numberPicker_minute, province_picker, city_picker, qu_picker;
    private int year, month, day, hour, minute;
    private String token;
    private ShngJiaPeiSongFragment shngJiaPeiSongFragment;
    private DaoDianZiQuFragment daoDianZiQuFragment;
    private FragmentManager fragmentManager;
    private Fragment hideFragment;
    private DingDanXiangQingAdapter dingDanXiangQingAdapter;
    private String peisongfei;
    //    private float zongji;
    private String dingdanhao;
    private String shopid;
    private NetWork netWork;
    private String addressid;
    private String ziqu;
    private Boolean isOK = true;
    private String zongji;
    private String zongjine;
    private String phone;
    private String address;
    private Boolean daodianziqu = true;
    private boolean ziqupanduan = true;
    private String finish;
    private String jsonstring;
    private ArrayList<NumberEntity> list = new ArrayList<>();//商品的信息
    private float zongji1;
    private String name;
    private int Year, Month, Day,Hour,Minute;


    float zong = 0;
    float pei = 0;

//    float zong = Float.parseFloat(zongji);
//    float pei = Float.parseFloat(peisongfei);


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ding_dan_xiang_qing);
        ButterKnife.bind(this);
        if (Build.VERSION.SDK_INT >= 21) {
            View dview = getWindow().getDecorView();
            dview.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN | View.SYSTEM_UI_FLAG_LAYOUT_STABLE);
            getWindow().setStatusBarColor(Color.TRANSPARENT);
        }

        SharedPreferences sharedPreferences = getSharedPreferences("token", Context.MODE_PRIVATE);
        token = sharedPreferences.getString("token", "token");
        System.out.println(token + "     token");


        Intent intent = getIntent();
        peisongfei = intent.getStringExtra("peisongfei");
//        zongji = intent.getFloatExtra("zongjiage", 0);
        zongji = intent.getStringExtra("zongjiage");
        zong = Float.parseFloat(zongji);//总金额
        pei = Float.parseFloat(peisongfei);//配送费
        shopid = intent.getStringExtra("shopid");
        jsonstring = intent.getStringExtra("json");
        name = intent.getStringExtra("name");

        tvDianming.setText(name);



        System.out.println(shopid + "      商家id");
        System.out.println(peisongfei + "     订单详情页接收的配送费");
        System.out.println(zongji + "    订单详情页接收的总价格");
        tvPeisongfeiMoney.setText("¥ " + peisongfei); //配送费
//
//        DecimalFormat decimalFormat=new DecimalFormat("0.00");//构造方法的字符格式这里如果小数不足2位,会以0补足.
//        zongji1 = decimalFormat.format(zong);//format 返回的是字符串


        zongji1 = (float)(Math.round(zong*100))/100;  //100指的是要保留的小数位数

//        zongji1 = String.format("%.2f",zongji);
//
        System.out.println(zongji1+"         zongji1");
        tvZongjiMoney.setText(" ¥ " + zongji1);//总计
        tvShijiMoney.setText(" ¥ " + zongji1);//实付
        tvShijifukuan.setText("¥ " + zongji1);//实付



//
//        tvZongjiMoney.setText(" ¥ " + zongji);//总计
//        tvShijiMoney.setText(" ¥ " + zongji);//实付
//        tvShijifukuan.setText("¥ " + zongji);//实付




//        dingdanhao = intent.getStringExtra("dingdanhao");
//        System.out.println(dingdanhao + "      详情接收的订单号");

//        initViewPager();
        DataShow();

        initView();
        initDefaultFragment();

////        提交订单的网络请求
//        TiJiaoDingDanHttp();


        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("dizhiid");
        netWork = new NetWork();
        registerReceiver(netWork, intentFilter);

        IntentFilter intentFilter1 = new IntentFilter();
        intentFilter1.addAction("111");
        netWork = new NetWork();
        registerReceiver(netWork, intentFilter1);


        IntentFilter intentFilter2 = new IntentFilter();
        intentFilter2.addAction("dizhixxx");
        netWork = new NetWork();
        registerReceiver(netWork, intentFilter2);


        IntentFilter intentFilter3 = new IntentFilter();
        intentFilter3.addAction("daodianqiziqi");
        netWork = new NetWork();
        registerReceiver(netWork, intentFilter3);


        IntentFilter intentFilter4 = new IntentFilter();
        intentFilter4.addAction("finish");
        netWork = new NetWork();
        registerReceiver(netWork, intentFilter4);



        zongjine = zong + pei+"";
        float v = Float.parseFloat(zongjine);
        String zongjine1 = String.format("%.2f",v);
//        zongjine1 = (float)(Math.round(zongjine*100))/100;  //100指的是要保留的小数位数
//
        tvShijifukuan.setText("¥ "+zongjine1);
        tvShijiMoney.setText(" ¥ "+zongjine1);
        tvZongjiMoney.setText(" ¥ "+zongjine1);


//        tvShijifukuan.setText("¥ "+zongjine);
//        tvShijiMoney.setText(" ¥ "+zongjine);
//        tvZongjiMoney.setText(" ¥ "+zongjine);
//        System.out.println(zongjine + "        zongjine11111111111");
        System.out.println(pei+"               peo");


        int numbser = 0;
        for (int i = 0; i < NumberEntity.lists.size(); i++) {
            NumberEntity numberEntity = NumberEntity.lists.get(i);
            String number = numberEntity.getNumber();
            int num = Integer.parseInt(number);
            if(num != 0){
                list.add(numberEntity);
            }
            numbser += num;
        }

        tvJiesuan.setText("去结算("+numbser+")");




        dingDanXiangQingAdapter = new DingDanXiangQingAdapter(DingDanXiangQingActivity.this,list);
        LinearLayoutManager manager = new LinearLayoutManager(DingDanXiangQingActivity.this);
        recyclerView.setLayoutManager(manager);
        recyclerView.setAdapter(dingDanXiangQingAdapter);







    }


    class NetWork extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {

            if (intent.getStringExtra("addrid") != null) {
                addressid = intent.getStringExtra("addrid");
                System.out.println(addressid + "     广播接收");
            }


            phone = intent.getStringExtra("phone");
            name = intent.getStringExtra("name");
            address = intent.getStringExtra("address");




            if (intent.getStringExtra("addressid") != null) {
                addressid = intent.getStringExtra("addressid");
            }


//            到店自取tongyi
            ziqupanduan = intent.getBooleanExtra("ziqupanduan", true);



            //            结束页面
            finish = intent.getStringExtra("finish");
            if(finish != null){
                finish();
            }


//            ziqu = intent.getStringExtra("ziqu");
//            System.out.println(ziqu+"   ziqu");
//            ChuanZhiEntity.obj = "id"+addressid;
//            System.out.println(ChuanZhiEntity.obj+"      ChuanZhiEntity.obj");
//            System.out.println(addressid+"    addressid接收");

        }

    }


    private void initDefaultFragment() {
        fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.add(R.id.container_fragment, shngJiaPeiSongFragment);
        fragmentTransaction.commit();
        hideFragment = shngJiaPeiSongFragment;
    }

    //    初始化
    private void initView() {
        shngJiaPeiSongFragment = new ShngJiaPeiSongFragment();
        daoDianZiQuFragment = new DaoDianZiQuFragment();
        tvShangjiaPeisong.setOnClickListener(this);
        tvDaodianZiqu.setOnClickListener(this);
    }


    //    商家配送和到店自取的监听
    @Override
    public void onClick(View view) {
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        switch (view.getId()) {
//            商家配送
            case R.id.tv_shangjia_peisong:
                zongjine = zong + pei + "";
                float v = Float.parseFloat(zongjine);

                String zongjine1 = String.format("%.2f",v);
                tvShijifukuan.setText("¥ " + zongjine1);
                tvShijiMoney.setText(" ¥ " + zongjine1);
                tvZongjiMoney.setText(" ¥ " + zongjine1);

//                tvShijifukuan.setText("¥ " + zongjine);
//                tvShijiMoney.setText(" ¥ " + zongjine);
//                tvZongjiMoney.setText(" ¥ " + zongjine);
                isOK = true;
                tvShangjiaPeisong.setBackgroundResource(R.drawable.shape_shangjiapeisong);
                tvShangjiaPeisong.setTextColor(getResources().getColor(R.color.shangjie_peisong_textcolor));
                tvDaodianZiqu.setBackgroundResource(R.drawable.shape_daodianziqu);
                tvDaodianZiqu.setTextColor(getResources().getColor(R.color.white_color));
                relativeTime.setVisibility(View.GONE);
                replaceFragment(shngJiaPeiSongFragment, fragmentTransaction);
                tvPeisongfeiMoney.setText("¥ " + peisongfei); //配送费
                break;
//            到店自取
            case R.id.tv_daodian_ziqu:

                DecimalFormat decimalFormat=new DecimalFormat("0.00");//构造方法的字符格式这里如果小数不足2位,会以0补足.
                String zongji1 = decimalFormat.format(zong);//format 返回的是字符串
                tvShijifukuan.setText("¥ " + zongji1 + "");
                tvShijiMoney.setText(" ¥ " + zongji1 + "");
                tvZongjiMoney.setText(" ¥ " + zongji1 + "");

//                tvShijifukuan.setText("¥ " + zong + "");
//                tvShijiMoney.setText(" ¥ " + zong + "");
//                tvZongjiMoney.setText(" ¥ " + zong + "");
//                System.out.println(zongjine + "        zongjine11111111111");
                isOK = false;
                daoDianZiQuFragment.setShopid(shopid);
                tvDaodianZiqu.setBackgroundResource(R.drawable.shape_shangjiapeisong_left);
                tvDaodianZiqu.setTextColor(getResources().getColor(R.color.shangjie_peisong_textcolor));
                tvShangjiaPeisong.setBackgroundResource(R.drawable.shape_daodianziqu_right);
                tvShangjiaPeisong.setTextColor(getResources().getColor(R.color.white_color));
                relativeTime.setVisibility(View.VISIBLE);
                replaceFragment(daoDianZiQuFragment, fragmentTransaction);
                tvPeisongfeiMoney.setText("¥ 0");
                break;
        }
    }


    private void replaceFragment(Fragment showFragment, FragmentTransaction fragmentTransaction) {
        if (showFragment.isAdded()) {
            fragmentTransaction.hide(hideFragment).show(showFragment).commit();
        } else {
            fragmentTransaction.hide(hideFragment).add(R.id.container_fragment, showFragment).commit();
        }
        hideFragment = showFragment;
    }




//    //    提交订单的网络请求
////    tiantian
//    private void TiJiaoDingDanHttp() {
//        final ProgressDialog progressDialog = new ProgressDialog(this);
//        progressDialog.setTitle("提示");
//        progressDialog.setMessage("请等待...");
//        progressDialog.setCancelable(false);
//        progressDialog.show();
//        OkHttpUtils.post()
//                .url(ContractUtils.LOGIN_URL + "Life/order_goods")
//                .addHeader("token", ContractUtils.getTOKEN(DingDanXiangQingActivity.this))
//                .addParams("parkId", ContractUtils.getParkId(DingDanXiangQingActivity.this))
//                .addParams("shopId", shopid)
//                .addParams("order_sn", dingdanhao)
//                .build()
//                .execute(new StringCallback() {
//                    @Override
//                    public void onError(Call call, Exception e, int id) {
//                        progressDialog.dismiss();
//                    }
//
//                    @Override
//                    public void onResponse(String response, int id) {
//                        progressDialog.dismiss();
//                        ContractUtils.Code500shibai(DingDanXiangQingActivity.this, response);
//                        System.out.println(response + "   提交订单的网络请求");
//
//                        if (response.indexOf("200") != -1) {
//                            Gson gson = new Gson();
//                            TiJiaoDingDanEntity tiJiaoDingDanEntity = gson.fromJson(response, TiJiaoDingDanEntity.class);
//                            dingDanXiangQingAdapter = new DingDanXiangQingAdapter(DingDanXiangQingActivity.this, NumberEntity.);
//                            LinearLayoutManager manager = new LinearLayoutManager(DingDanXiangQingActivity.this);
//                            recyclerView.setLayoutManager(manager);
//                            recyclerView.setAdapter(dingDanXiangQingAdapter);
////
//                            if (tiJiaoDingDanEntity.getResponse().getShop().getNickname() == null) {
//
//                            } else {
//                                tvDianming.setText(tiJiaoDingDanEntity.getResponse().getShop().getNickname());
//                            }

//
//
//                            int numbser = 0;
//                            for (int i = 0; i < tiJiaoDingDanEntity.getResponse().getGoods().size(); i++) {
//                                String num = tiJiaoDingDanEntity.getResponse().getGoods().get(i).getNum();
//                                int number = Integer.parseInt(num);
//                                numbser += number;
//                            }
//                            tvJiesuan.setText("去结算("+numbser+")");
//                        }
//                    }
//                });
//    }

    private void DataShow() {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy年MM月dd日 HH:mm:ss");// HH:mm:ss
//获取当前时间
        Date date = new Date(System.currentTimeMillis());
        System.out.println("Date获取当前日期时间" + simpleDateFormat.format(date));
//                                    2018年03月14日 11:39:29
        String str = simpleDateFormat.format(date);

        year = Integer.parseInt(str.substring(0, 4));
        month = Integer.parseInt(str.substring(5, 7));
        day = Integer.parseInt(str.substring(8, 10));
        hour = Integer.parseInt(str.substring(12, 14));
        minute = Integer.parseInt(str.substring(15, 17));
    }

//    private void initViewPager() {
//        List<Fragment> fragments = new ArrayList<>();
//        fragments.add(new ShngJiaPeiSongFragment());
//        fragments.add(new DaoDianZiQuFragment());
//        // 创建ViewPager适配器
//        MyPagerAdapter myPagerAdapter = new MyPagerAdapter(getSupportFragmentManager());
//        myPagerAdapter.setFragments(fragments);
//        // 给ViewPager设置适配器
//        viewPager.setAdapter(myPagerAdapter);
//        // TabLayout 指示器 (记得自己手动创建4个Fragment,注意是 app包下的Fragment 还是 V4包下的 Fragment)
//        tabLayout.addTab(tabLayout.newTab());
//        tabLayout.addTab(tabLayout.newTab());
//        // 使用 TabLayout 和 ViewPager 相关联
//        tabLayout.setupWithViewPager(viewPager);
//        // TabLayout指示器添加文本
//        tabLayout.getTabAt(0).setText("商家配送");
//        tabLayout.getTabAt(1).setText("到店自取");
//
//        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
//            @Override
//            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
//
//            }
//
//            @Override
//            public void onPageSelected(int position) {
//                if (position == 0) {
//                    relativeTime.setVisibility(View.GONE);
//                } else if (position == 1) {
//                    relativeTime.setVisibility(View.VISIBLE);
//                }
//            }
//
//            @Override
//            public void onPageScrollStateChanged(int state) {
//
//            }
//        });
//    }


    //    R.id.et_beizhu,
    @OnClick({R.id.tv_jiesuan, R.id.image_back, R.id.tv_time, R.id.relative_time, R.id.tv_dianming, R.id.tv_peisongfei_money, R.id.tv_zongji_money, R.id.tv_shifu_money, R.id.tv_shijifukuan})
    public void onViewClicked(View view) {
        switch (view.getId()) {
//            结算
            case R.id.tv_jiesuan:
//                System.out.println(ChuanZhiEntity.obj+"     ChuanZhiEntity.obj");
//                if(!(ChuanZhiEntity.obj instanceof String)){
//                    Toast.makeText(this, "请先选择地址", Toast.LENGTH_SHORT).show();
//                    return;
//
//                }
//                String id = (String) ChuanZhiEntity.obj;
//                if(id.indexOf("id") != -1){
//                    Toast.makeText(this, "请先选择地址", Toast.LENGTH_SHORT).show();
//                    return;
//                }

//                String substring = id.substring(1, id.length() - 2);
//                System.out.println(substring+"         地址id");


                System.out.println(addressid + "         addressid111");
                System.out.println(isOK + "              isoklkkkkkkkkkkkkkkkkk");
                System.out.println(tvTime.getText().toString().trim() + "           tvtttttttttttttttt");


                Calendar instance = Calendar.getInstance();

                int year = instance.get(Calendar.YEAR);
                int month = instance.get(Calendar.MONTH)+1;// 获取当前月份
                int day = instance.get(Calendar.DAY_OF_MONTH);// 获取当日期
                int hour = instance.get(Calendar.HOUR_OF_DAY);
                int minute = instance.get(Calendar.MINUTE);
                System.out.println(month+"      month");
                System.out.println(day+"      day");
                System.out.println(hour+"       hour");
                System.out.println(minute+"         minute");
                System.out.println(tvTime.getText().toString().trim()+"         tvTime");


                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm");
                try {
                    Date date = sdf.parse(year+"-"+tvTime.getText().toString().trim());
                    System.out.println(date+"    date");
                    System.out.println(date.getTime()+"            date.getTime()");
                    Date date1 = sdf.parse(year+"-"+month+"-"+day+" "+hour+":"+minute);
                    System.out.println(year+"-"+month+"-"+day+" "+hour+":"+minute+"         时间时间");

                    if(date.getTime() <= date1.getTime()){
                        Toast.makeText(this, "取货时间不能小于当前时间！", Toast.LENGTH_SHORT).show();
                        return;
                    }
                } catch (ParseException e) {
                    e.printStackTrace();
                }

//
//
//                SimpleDateFormat sdf = new SimpleDateFormat("HH:mm");
//                try {
//                    Date date = sdf.parse(tvTime.getText().toString());
//                    Date date1 = sdf.parse(hour+":"+minute);
//                    if(date.getTime() < date1.getTime()){
//                        Toast.makeText(this, "取货时间不能小于当前时间！", Toast.LENGTH_SHORT).show();
//                        return;
//                    }
//                } catch (ParseException e) {
//                    e.printStackTrace();
//                }


                if (addressid == null && isOK == true) {
                    Toast.makeText(this, "请先选择地址", Toast.LENGTH_SHORT).show();
                    return;
                }



                if(ziqupanduan == false && isOK == false){
                    Toast.makeText(this, "是否同意到店自取服务协议", Toast.LENGTH_SHORT).show();
                    return;
                }



//                if(addressid == null && isOK == true){
//                    Toast.makeText(this, "请先选择地址", Toast.LENGTH_SHORT).show();
//                    return;
//                }
                if (tvTime.getText().toString().trim().length() == 0 && isOK == false) {
                    Toast.makeText(this, "请选择取货时间", Toast.LENGTH_SHORT).show();
                    return;
                }


//                提交订单接口的网络请求
                initTiJiaoHttp();

//
//                Intent intent = new Intent(DingDanXiangQingActivity.this, ZhiFuActivity.class);
//                intent.putExtra("dingdanhao", dingdanhao);
//                if(isOK == false){
//
//                }else {
//                    intent.putExtra("addressid", addressid);
//                }
//
//                if (isOK == true) {
//                    String format = String.format("%.2f", zong + pei);
//                    intent.putExtra("shifikuan", format);
//                } else if (isOK == false) {
//                    String format1 = String.format("%.2f", zong);
//                    intent.putExtra("shifikuan", format1);
//                }
//                intent.putExtra("beizhu", etBeizhu.getText().toString().trim());
////              intent.putExtra("shifikuan", zongjine); //   测试  intent.putExtra("shifikuan","0.01");
//                intent.putExtra("time", tvTime.getText().toString().trim());
//                System.out.println(dingdanhao + "     跳转支付传送的订单号");
//                System.out.println(addressid + "      chuanaddressid");
//                startActivity(intent);
////                finish();
                break;
//            、返回按钮
            case R.id.image_back:
                finish();
                break;
//            店名
            case R.id.tv_dianming:
                break;
//            配送费
            case R.id.tv_peisongfei_money:
                break;
//            、总计
            case R.id.tv_zongji_money:
                break;
//            实付
            case R.id.tv_shifu_money:
                break;
//            最下面实付
            case R.id.tv_shijifukuan:
                break;
//            case R.id.tabLayout:
//
//                break;
//            case R.id.viewPager:
//                break;
//            备注
//            case R.id.et_beizhu:
//                break;
            case R.id.tv_time:
//                Calendar instance = Calendar.getInstance();
//
//                int hour = instance.get(Calendar.HOUR_OF_DAY);
//                int minute = instance.get(Calendar.MINUTE);
//                System.out.println(hour+"       hour");
//                System.out.println(minute+"         minute");
//
//


                Calendar selectedDate = Calendar.getInstance();

                Year = selectedDate.get(Calendar.YEAR); // 获取当前年份
                Month = selectedDate.get(Calendar.MONTH);// 获取当前月份
                Day = selectedDate.get(Calendar.DAY_OF_MONTH);// 获取当日期
                Hour = selectedDate.get(Calendar.HOUR_OF_DAY);//时
                Minute = selectedDate.get(Calendar.MINUTE);//分
                System.out.println(Hour+"   时");
                System.out.println(Minute+"    分");
                Calendar startDate = Calendar.getInstance();
                startDate.set(Year,Month,Day,Hour,Minute);//设置起始年份
                Calendar endDate = Calendar.getInstance();
                endDate.set(2100,1,1);//设置结束年份





                Calendar calendar = Calendar.getInstance();
                TimePickerView timePickerView = new TimePickerView.Builder(DingDanXiangQingActivity.this, new TimePickerView.OnTimeSelectListener() {
                    @Override
                    public void onTimeSelect(Date date, View v) {
//                        SimpleDateFormat simpleDateFormat1=new SimpleDateFormat("yyyy-MM-dd HH:mm");
                        SimpleDateFormat simpleDateFormat1=new SimpleDateFormat("MM-dd HH:mm");
//                        SimpleDateFormat simpleDateFormat1 = new SimpleDateFormat("HH:mm");
                        tvTime.setText(simpleDateFormat1.format(date));
                    }
                })
                        .setType(new boolean[]{false, true, true, true, true, false})//默认全部显示
                        .setCancelText("取消")//取消按钮文字
                        .setSubmitText("确定")//确认按钮文字
                        .setTextXOffset(0, 0, 0, 0, 0, 0)
                        .setLunarCalendar(false)
//                        .setContentSize(20)//滚轮文字大小
                        .gravity(Gravity.CENTER)
                        .setTitleSize(16)//标题文字大小
                        .setTitleText("取货时间")//标题文字
                        .setOutSideCancelable(true)//点击屏幕，点在控件外部范围时，是否取消显示
                        .isCyclic(false)//是否循环滚动
                        .setTextColorCenter(Color.BLACK)//设置选中项的颜色
                        .setTextColorOut(Color.parseColor("#AEAAAA"))
                        .setTitleColor(Color.BLACK)//标题文字颜色
                        .setSubmitColor(Color.RED)//确定按钮文字颜色
                        .setCancelColor(Color.parseColor("#AEAAAA"))//取消按钮文字颜色
                        .setTitleBgColor(Color.parseColor("#f5f5f5"))//标题背景颜色 Night mode
                        .setBgColor(Color.parseColor("#ffffff"))//滚轮背景颜色 Night mode
//                        .setRange(calendar.get(Calendar.YEAR), 2100)//默认是1900-2100年
//                        .setRange(calendar.get(Calendar.YEAR) - 20, calendar.get(Calendar.YEAR) + 20)//默认是1900-2100年
//                        .setDate(selectedDate)// 如果不设置的话，默认是系统时间*/
                        .setRangDate(startDate,endDate)//起始终止年月日设定
                        .setLabel("","月","日",":","","")
//                        .setLabel("", "", "", ":", "", "")
                        .isCenterLabel(true) //是否只显示中间选中项的label文字，false则每项item全部都带有label。
                        .isDialog(false)//是否显示为对话框样式
                        .build();
                timePickerView.setDate(Calendar.getInstance());//注：根据需求来决定是否使用该方法（一般是精确到秒的情况），此项可以在弹出选择器的时候重新设置当前时间，避免在初始化之后由于时间已经设定，导致选中时间与当前时间不匹配的问题。
                timePickerView.show();
                break;
//            取货时间
            case R.id.relative_time:



                Calendar selectedDate1 = Calendar.getInstance();

                Year = selectedDate1.get(Calendar.YEAR); // 获取当前年份
                Month = selectedDate1.get(Calendar.MONTH);// 获取当前月份
                Day = selectedDate1.get(Calendar.DAY_OF_MONTH);// 获取当日期
                Hour = selectedDate1.get(Calendar.HOUR_OF_DAY);//时
                Minute = selectedDate1.get(Calendar.MINUTE);//分
                Calendar startDate1 = Calendar.getInstance();
                startDate1.set(Year,Month,Day,Hour,Minute);//设置起始年份
                Calendar endDate1 = Calendar.getInstance();
                endDate1.set(2100,1,1);//设置结束年份



                Calendar calendar1 = Calendar.getInstance();
                TimePickerView timePickerView1 = new TimePickerView.Builder(DingDanXiangQingActivity.this, new TimePickerView.OnTimeSelectListener() {
                    @Override
                    public void onTimeSelect(Date date, View v) {
                        SimpleDateFormat simpleDateFormat1=new SimpleDateFormat("MM-dd HH:mm");
                        tvTime.setText(simpleDateFormat1.format(date));
                    }
                })
                        .setType(new boolean[]{false, true, true, true, true, false})//默认全部显示
                        .setCancelText("取消")//取消按钮文字
                        .setSubmitText("确定")//确认按钮文字
                        .setTextXOffset(0, 0, 0, 0, 0, 0)
                        .setLunarCalendar(false)
//                        .setContentSize(20)//滚轮文字大小
                        .gravity(Gravity.CENTER)
                        .setTitleSize(16)//标题文字大小
                        .setTitleText("取货时间")//标题文字
                        .setOutSideCancelable(true)//点击屏幕，点在控件外部范围时，是否取消显示
                        .isCyclic(false)//是否循环滚动
                        .setTextColorCenter(Color.BLACK)//设置选中项的颜色
                        .setTextColorOut(Color.parseColor("#AEAAAA"))
                        .setTitleColor(Color.BLACK)//标题文字颜色
                        .setSubmitColor(Color.RED)//确定按钮文字颜色
                        .setCancelColor(Color.parseColor("#AEAAAA"))//取消按钮文字颜色
                        .setTitleBgColor(Color.parseColor("#f5f5f5"))//标题背景颜色 Night mode
                        .setBgColor(Color.parseColor("#ffffff"))//滚轮背景颜色 Night mode
//                        .setRange(calendar1.get(Calendar.YEAR),2100)//默认是1900-2100年
//                        .setDate(selectedDate)// 如果不设置的话，默认是系统时间*/
                        .setRangDate(startDate1,endDate1)//起始终止年月日设定
                        .setLabel("","月","日",":","","")
//                        .setLabel("", "", "", ":", "", "")
                        .isCenterLabel(true) //是否只显示中间选中项的label文字，false则每项item全部都带有label。
                        .isDialog(false)//是否显示为对话框样式
                        .build();
                timePickerView1.setDate(Calendar.getInstance());//注：根据需求来决定是否使用该方法（一般是精确到秒的情况），此项可以在弹出选择器的时候重新设置当前时间，避免在初始化之后由于时间已经设定，导致选中时间与当前时间不匹配的问题。
                timePickerView1.show();


//
                break;
        }
    }



//    提交订单接口的网络请求
    private void initTiJiaoHttp() {
        System.out.println(jsonstring+"         jsonstring");
        System.out.println(zongjine+"            zongjine");
        System.out.println(shopid+"             shopid");
        System.out.println(addressid+"              addressid");
        System.out.println(etBeizhu.getText().toString().trim()+"                etBeizhu.getText().toString().trim()");
        System.out.println(tvTime.getText().toString().trim()+"                 tvTime.getText().toString().trim()");


        String good = "";
        List<Map<String, String>> list_post = new ArrayList();
        Gson gson = new Gson();
        for (int i = 0; i < list.size(); i++) {
            HashMap map = new HashMap();

            NumberEntity nn = list.get(i);
            System.out.println(nn.getNumber() + "          ccccccccccccc");
            map.put("id", nn.getId());
            map.put("num", nn.getNumber());
            list_post.add(map);
        }
        good = gson.toJson(list_post);


        if(addressid != null && isOK == true){
            OkHttpUtils.post()
                    .url(ContractUtils.LOGIN_URL+"order/add_order")
                    .addHeader("token",ContractUtils.getTOKEN(DingDanXiangQingActivity.this))
//                    .addParams("goods",jsonstring)
                    .addParams("goods",good)
                    .addParams("money",zongjine)//加配送费
                    .addParams("parkId",ContractUtils.getParkId(DingDanXiangQingActivity.this))
                    .addParams("shopId",shopid)
                    .addParams("adders_id",addressid)
                    .addParams("Remarks",etBeizhu.getText().toString().trim())
                    .build()
                    .execute(new StringCallback() {
                        @Override
                        public void onError(Call call, Exception e, int id) {

                        }

                        @Override
                        public void onResponse(String response, int id) {
                            System.out.println(response+"           提交订单商家配送的网络请求");
                            ContractUtils.Code500(DingDanXiangQingActivity.this,response);
                            if(response.indexOf("200") != -1){
                                Gson gson = new Gson();
                                TiJiaoDingDanXiangQingEntity tiJiaoDingDanXiangQingEntity = gson.fromJson(response, TiJiaoDingDanXiangQingEntity.class);
                                Intent intent = new Intent(DingDanXiangQingActivity.this,ZhiFuActivity.class);
                                intent.putExtra("dingdanhao",tiJiaoDingDanXiangQingEntity.getResponse().getOrder_sn());
                                intent.putExtra("money",tiJiaoDingDanXiangQingEntity.getRequest().getMoney());
                                startActivity(intent);
                            }else{
                                ContractUtils.Code400(DingDanXiangQingActivity.this,response);
                            }

                        }
                    });
        }else{
            OkHttpUtils.post()
                    .url(ContractUtils.LOGIN_URL+"order/add_order")
                    .addHeader("token",ContractUtils.getTOKEN(DingDanXiangQingActivity.this))
//                    .addParams("goods",jsonstring)
                    .addParams("goods",good)
                    .addParams("money",zongji)//不加配送费
                    .addParams("parkId",ContractUtils.getParkId(DingDanXiangQingActivity.this))
                    .addParams("shopId",shopid)
//                    .addParams("adders_id",addressid)
                    .addParams("Extraction",tvTime.getText().toString().trim())
                    .addParams("Remarks",etBeizhu.getText().toString().trim())
                    .build()
                    .execute(new StringCallback() {
                        @Override
                        public void onError(Call call, Exception e, int id) {

                        }

                        @Override
                        public void onResponse(String response, int id) {
                            System.out.println(response+"        提交订单到店自取的网络请求");
                            ContractUtils.Code500(DingDanXiangQingActivity.this,response);
                            if(response.indexOf("200") != -1){
                                Gson gson = new Gson();
                                TiJiaoDingDanXiangQingEntity tiJiaoDingDanXiangQingEntity = gson.fromJson(response, TiJiaoDingDanXiangQingEntity.class);
                                Intent intent = new Intent(DingDanXiangQingActivity.this,ZhiFuActivity.class);
                                intent.putExtra("dingdanhao",tiJiaoDingDanXiangQingEntity.getResponse().getOrder_sn());
                                intent.putExtra("money",tiJiaoDingDanXiangQingEntity.getRequest().getMoney());
                                startActivity(intent);
                            }else{
                                ContractUtils.Code400(DingDanXiangQingActivity.this,response);
                            }
                        }
                    });
        }

    }



}
