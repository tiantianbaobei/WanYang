package com.wanyangproject.fuwuactivity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.wanyangproject.R;
import com.wanyangproject.activity.WuYeFuWuXiangQingActivity;
import com.wanyangproject.adapter.WuYeLieBiaoAdapter;
import com.wanyangproject.entity.WuYeFuWuLieBiaoEntity;
import com.wanyangproject.utils.ContractUtils;
import com.zhy.http.okhttp.OkHttpUtils;
import com.zhy.http.okhttp.callback.StringCallback;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.Call;

public class WuYeFuWuLieBiaoActivity extends AppCompatActivity {

    @BindView(R.id.image_back)
    ImageView imageBack;
    @BindView(R.id.tv_title)
    TextView tvTitle;
    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    private WuYeLieBiaoAdapter wuYeLieBiaoAdapter;
    private WuYeFuWuLieBiaoEntity wuYeFuWuLieBiaoEntity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wu_ye_fu_wu_lie_biao);
        ButterKnife.bind(this);



//        物业服务列表的网络请求
        initWuYeLieBiaoHttp();


    }

//    物业服务列表的网络请求
    private void initWuYeLieBiaoHttp() {
        System.out.println(ContractUtils.getTOKEN(WuYeFuWuLieBiaoActivity.this)+"       token");
        System.out.println(ContractUtils.getParkId(WuYeFuWuLieBiaoActivity.this)+"            parkId");
        System.out.println(1+"            typeId");
        OkHttpUtils.post()
                .url(ContractUtils.LOGIN_URL+"fuwu/index")
                .addHeader("token",ContractUtils.getTOKEN(WuYeFuWuLieBiaoActivity.this))
                .addParams("parkId",ContractUtils.getParkId(WuYeFuWuLieBiaoActivity.this))
                .addParams("typeId","1")//1:物业服务，2：企业服务
                .build()
                .execute(new StringCallback() {
                    @Override
                    public void onError(Call call, Exception e, int id) {
                        System.out.println(e+"     eee物业服务列表");
                    }

                    @Override
                    public void onResponse(String response, int id) {
                        ContractUtils.Code500(WuYeFuWuLieBiaoActivity.this,response);
                        System.out.println(response+"          物业服务列表的网络请求");

                        if(response.indexOf("200") != -1){
                            Gson gson = new Gson();
                            wuYeFuWuLieBiaoEntity = gson.fromJson(response, WuYeFuWuLieBiaoEntity.class);


                            if(wuYeFuWuLieBiaoEntity.getResponse().getContent() == null){

                            }else{
                                String content = wuYeFuWuLieBiaoEntity.getResponse().getContent();
                                String replace = content.replace("--", "\n");
                                String replace1 = replace.replace("&nbsp;", " ");
                                tvTitle.setText(replace1);
                            }


                            wuYeLieBiaoAdapter = new WuYeLieBiaoAdapter(WuYeFuWuLieBiaoActivity.this,wuYeFuWuLieBiaoEntity.getResponse().getData());
                            LinearLayoutManager manager = new LinearLayoutManager(WuYeFuWuLieBiaoActivity.this);
                            recyclerView.setLayoutManager(manager);
                            recyclerView.setAdapter(wuYeLieBiaoAdapter);

                            wuYeLieBiaoAdapter.setWuYeLieBiaoClick(new WuYeLieBiaoAdapter.WuYeLieBiaoClick() {
                                @Override
                                public void wueLieViaoClick(int position,String id) {
                                    Intent intent = new Intent(WuYeFuWuLieBiaoActivity.this, WuYeFuWuXiangQingActivity.class);
                                    intent.putExtra("id",id);
                                    intent.putExtra("futitle",wuYeFuWuLieBiaoEntity.getResponse().getData().get(position).getDesc());
                                    startActivity(intent);
                                }
                            });
                        }else if(response.indexOf("400") != -1){
                            ContractUtils.Code400(WuYeFuWuLieBiaoActivity.this,response);
                        }
                    }
                });
    }


    @OnClick({R.id.image_back, R.id.tv_title, R.id.recyclerView})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.image_back:
                finish();
                break;
            case R.id.tv_title:
                break;
            case R.id.recyclerView:
                break;
        }
    }
}
