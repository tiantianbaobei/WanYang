package com.wanyangproject.fuwuactivity;

import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;

import com.androidkun.xtablayout.XTabLayout;
import com.wanyangproject.R;
import com.wanyangproject.adapter.MyPagerAdapter;
import com.wanyangproject.fragment.JinXingFragment;
import com.wanyangproject.fragment.WanChengFragment;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

//  服务申请记录
public class FuWuShenQingJiLuActivity extends AppCompatActivity {

    @BindView(R.id.image_back)
    ImageView imageBack;
    //    @BindView(R.id.tabLayout)
//    TabLayout tabLayout;
    @BindView(R.id.viewPager)
    ViewPager viewPager;
    @BindView(R.id.tabLayout)
    XTabLayout tabLayout;
    private String wode;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fu_wu_shen_qing_ji_lu);
        ButterKnife.bind(this);
        if (Build.VERSION.SDK_INT >= 21) {
            View dview = getWindow().getDecorView();
            dview.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN | View.SYSTEM_UI_FLAG_LAYOUT_STABLE);
            getWindow().setStatusBarColor(Color.TRANSPARENT);
        }

        Intent intent = getIntent();
        wode = intent.getStringExtra("wode");
        System.out.println(wode+"         我的");

        initViewPager();
    }


    private void initViewPager() {
        List<Fragment> fragments = new ArrayList<>();
        JinXingFragment jinXingFragment = new JinXingFragment();
        WanChengFragment wanChengFragment = new WanChengFragment();
        jinXingFragment.setIsOK(wode);
        wanChengFragment.setIsOK(wode);
        fragments.add(jinXingFragment);
        fragments.add(wanChengFragment);
        // 创建ViewPager适配器
        MyPagerAdapter myPagerAdapter = new MyPagerAdapter(getSupportFragmentManager());
        myPagerAdapter.setFragments(fragments);
        // 给ViewPager设置适配器
        viewPager.setAdapter(myPagerAdapter);
        // TabLayout 指示器 (记得自己手动创建4个Fragment,注意是 app包下的Fragment 还是 V4包下的 Fragment)
        tabLayout.addTab(tabLayout.newTab());
        tabLayout.addTab(tabLayout.newTab());
        // 使用 TabLayout 和 ViewPager 相关联
        tabLayout.setupWithViewPager(viewPager);
        // TabLayout指示器添加文本
        tabLayout.getTabAt(0).setText("进行中");
        tabLayout.getTabAt(1).setText("已完成");
    }



    @OnClick({R.id.image_back, R.id.tabLayout, R.id.viewPager})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.image_back:
                finish();
                break;
            case R.id.tabLayout:
                break;
            case R.id.viewPager:
                break;
        }
    }
}
