package com.wanyangproject.fuwuactivity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebResourceResponse;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.wanyangproject.R;
import com.wanyangproject.entity.FangKeDengJiXinXiEntity;
import com.wanyangproject.shouye.YuanQuJieShaoActivity;
import com.wanyangproject.utils.ContractUtils;
import com.wanyangproject.utils.WebViewUtils;
import com.zhy.http.okhttp.OkHttpUtils;
import com.zhy.http.okhttp.callback.StringCallback;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.Call;

//  访客登记
public class FangKeDengJiActivity extends AppCompatActivity {

    @BindView(R.id.image_back)
    ImageView imageBack;
    @BindView(R.id.tv_fangke_xinix)
    TextView tvFangkeXinix;
    @BindView(R.id.webView)
    WebView webView;
    private WebSettings mWebSettings;
    private String content;
    private FangKeDengJiXinXiEntity fangKeDengJiXinXiEntity;
    private ProgressDialog progressDialog;
    private static Boolean isOK = false;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fang_ke_deng_ji);
        ButterKnife.bind(this);
        if (Build.VERSION.SDK_INT >= 21) {
            View dview = getWindow().getDecorView();
            dview.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN | View.SYSTEM_UI_FLAG_LAYOUT_STABLE);
            getWindow().setStatusBarColor(Color.TRANSPARENT);
        }


        mWebSettings = webView.getSettings();
        mWebSettings.setJavaScriptEnabled(true);  //允许加载javascript
        webView.getSettings().setLayoutAlgorithm(WebSettings.LayoutAlgorithm.SINGLE_COLUMN);
        webView.getSettings().setLoadWithOverviewMode(true);
        mWebSettings.setSupportZoom(false);     //允许缩放
        mWebSettings.setBuiltInZoomControls(false); //原网页基础上缩放
        mWebSettings.setUseWideViewPort(false);   //任意比例缩放
//        mWebSettings.setUseWideViewPort(true); //将图片调整到适合webview的大小
//        mWebSettings.setLoadWithOverviewMode(true); // 缩放至屏幕的大小
//        mWebSettings.setTextSize(WebSettings.TextSize.SMALLEST);

//        访客登记主页信息介绍的网络请求
        initFangKeDengjiXinXiHttp();


        initView();



    }

    private void initView() {
        webView.setWebViewClient(new WebViewClient() {
            /**
             * 拦截 url 跳转,在里边添加点击链接跳转或者操作
             */
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, WebResourceRequest request) {
                return WebViewUtils.shouldOverrideUrlLoading(view,request,FangKeDengJiActivity.this);
            }


            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                System.out.println("66666666666666666666");
                return WebViewUtils.shouldOverrideUrlLoading(view,url,FangKeDengJiActivity.this);

            }


            /**
             * 在开始加载网页时会回调
             */
            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                super.onPageStarted(view, url, favicon);
                System.out.println("111111111111111111");
            }

            /**
             * 加载错误的时候会回调，在其中可做错误处理，比如再请求加载一次，或者提示404的错误页面
             */
            @Override
            public void onReceivedError(WebView view, WebResourceRequest request, WebResourceError error) {
                super.onReceivedError(view, request, error);

            }


            /**
             * 在结束加载网页时会回调
             */
            @Override
            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(view, url);
                System.out.println("chenggongchenggongchenggong");

                if(progressDialog != null){
                    progressDialog.dismiss();
                }

            }

            /**
             * 当接收到https错误时，会回调此函数，在其中可以做错误处理
             */
            @Override
            public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
                super.onReceivedError(view, errorCode, description, failingUrl);
            }

            /**
             * 在每一次请求资源时，都会通过这个函数来回调
             */
            public WebResourceResponse shouldInterceptRequest(WebView view, String url) {
                return null;
            }


            @Override
            public void onPageCommitVisible(WebView view, String url) {
                super.onPageCommitVisible(view, url);

                if(progressDialog != null){
                    progressDialog.dismiss();
                }

            }
        });
    }












    //    访客登记主页信息介绍的网络请求
    private void initFangKeDengjiXinXiHttp() {
        progressDialog = new ProgressDialog(FangKeDengJiActivity.this);
        progressDialog.setTitle("提示");
        progressDialog.setMessage("请等待...");
        progressDialog.setCancelable(false);
        progressDialog.show();
        OkHttpUtils.post()
                .url(ContractUtils.LOGIN_URL + "dengji/index")
                .addHeader("token", ContractUtils.getTOKEN(FangKeDengJiActivity.this))
                .build()
                .execute(new StringCallback() {
                    @Override
                    public void onError(Call call, Exception e, int id) {
                        progressDialog.dismiss();
                    }

                    @Override
                    public void onResponse(String response, int id) {
                        ContractUtils.Code500(FangKeDengJiActivity.this,response);
                        System.out.println(response + "        访客登记主页信息介绍的网络请求 ");

                        if (response.indexOf("200") != -1) {
                            Gson gson = new Gson();
                            fangKeDengJiXinXiEntity = gson.fromJson(response, FangKeDengJiXinXiEntity.class);
                            if(fangKeDengJiXinXiEntity.getResponse().getContent() == null){

                            }else{
                                //                            内容
                                content = fangKeDengJiXinXiEntity.getResponse().getContent();
                                webView.loadDataWithBaseURL("http://www.baidu.com", getNewContent(content),"text/html", "UTF-8", null);
                            }
                        }
                    }
                });
    }


    @OnClick({R.id.image_back, R.id.tv_fangke_xinix})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.image_back:
                finish();
                break;
            case R.id.tv_fangke_xinix:
//                typeId: 0 游客 1 普通员工 2企业用户 3商家 4 门卫 5 物业
                if(ContractUtils.getTypeId(FangKeDengJiActivity.this).equals("2") || ContractUtils.getTypeId22(FangKeDengJiActivity.this).equals("2")){
                    Intent intent = new Intent(FangKeDengJiActivity.this, TianJiaFangKeActivity.class);
                    startActivity(intent);
                }else{
                    Toast.makeText(this, "您还不是企业身份！", Toast.LENGTH_SHORT).show();
                }
                break;
        }
    }



    /**
     * 将html文本内容中包含img标签的图片，宽度变为屏幕宽度，高度根据宽度比例自适应
     **/
    public static String getNewContent(String htmltext){
//        if(isOK == false){
//            return htmltext;
//        }
//
//        isOK = true;
        try {
            Document doc= Jsoup.parse(htmltext);
            Elements elements=doc.getElementsByTag("img");
            for (Element element : elements) {
                element.attr("width","100%").attr("height","auto");
            }
            return doc.toString();
        } catch (Exception e) {
            return htmltext;
        }
    }
}
