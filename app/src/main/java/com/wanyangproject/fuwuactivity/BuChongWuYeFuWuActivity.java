package com.wanyangproject.fuwuactivity;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.lzy.imagepicker.ImagePicker;
import com.lzy.imagepicker.bean.ImageItem;
import com.lzy.imagepicker.ui.ImageGridActivity;
import com.wanyangproject.R;
import com.wanyangproject.adapter.BuChongAdapter;
import com.wanyangproject.adapter.BuChongEntity;
import com.wanyangproject.entity.FuWuJiLuShenQingXiangQingEntity;
import com.wanyangproject.entity.PhotoEntity;
import com.wanyangproject.popuwindow.TakePhotoPopWin;
import com.wanyangproject.utils.ContractUtils;
import com.wanyangproject.utils.GlideImageLoaderForPicker;
import com.zhy.http.okhttp.OkHttpUtils;
import com.zhy.http.okhttp.callback.StringCallback;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import top.zibin.luban.Luban;
import top.zibin.luban.OnCompressListener;

//  补充
public class BuChongWuYeFuWuActivity extends AppCompatActivity {

    @BindView(R.id.image_back)
    ImageView imageBack;
    @BindView(R.id.tv_bianji)
    TextView tvBianji;
    @BindView(R.id.tv_title)
    TextView tvTitle;
    @BindView(R.id.tv_time)
    TextView tvTime;
    @BindView(R.id.et_neirong)
    EditText etNeirong;
    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    @BindView(R.id.tv_name)
    TextView tvName;
    @BindView(R.id.tv_phone)
    TextView tvPhone;
    @BindView(R.id.image_add_one)
    ImageView imageAddOne;
    @BindView(R.id.image_delete_one)
    ImageView imageDeleteOne;
    @BindView(R.id.image_add_two)
    ImageView imageAddTwo;
    @BindView(R.id.image_delete_two)
    ImageView imageDeleteTwo;
    @BindView(R.id.image_add_three)
    ImageView imageAddThree;
    @BindView(R.id.image_delete_three)
    ImageView imageDeleteThree;
    @BindView(R.id.tv_fuwu_jieshao)
    TextView tvFuwuJieshao;
    @BindView(R.id.tv_jujueliyou)
    TextView tvJujueliyou;
    @BindView(R.id.tv_biaoti)
    TextView tvBiaoti;
    private String id;
    private BuChongAdapter buChongAdapter;
    private FuWuJiLuShenQingXiangQingEntity fuWuJiLuShenQingXiangQingEntity;
    private String entype;
    private int ISOK = 0;
    //    private ArrayList<ImageItem> list = new ArrayList<>();// 图片
    private ArrayList<String> list1 = new ArrayList<>();// 图片
    private ArrayList<ImageView> add = new ArrayList(); //  带加号的原图
    private ArrayList<String> imageUrl = new ArrayList<>(); // 图片网址
    private ArrayList<ImageView> shanchu = new ArrayList<>();// 删除的按钮
    private PhotoEntity photoEntity;
    private TakePhotoPopWin takePhotoPopWin;
    private String jieshao;
    private String jujueliyou;
    private Uri photoUri;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bu_chong_wu_ye_fu_wu);
        ButterKnife.bind(this);
        if (Build.VERSION.SDK_INT >= 21) {
            View dview = getWindow().getDecorView();
            dview.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN | View.SYSTEM_UI_FLAG_LAYOUT_STABLE);
            getWindow().setStatusBarColor(Color.TRANSPARENT);
        }

        Intent intent = getIntent();
        id = intent.getStringExtra("id");
        entype = intent.getStringExtra("entype");
        jieshao = intent.getStringExtra("jieshao");
        jujueliyou = intent.getStringExtra("jujueliyou");

        System.out.println(entype + "       entype");
        System.out.println(id + "      接收的id");
        System.out.println(jieshao + "          服务介绍");
        System.out.println(jujueliyou + "      拒绝理由");

        if (jieshao != null) {
            tvFuwuJieshao.setText(jieshao);
        }

        if (jujueliyou != null) {
            tvJujueliyou.setText(jujueliyou);
        }


//        企业服务补充的网络请求
        initBuChongXiangQingHttp();


        add.add(imageAddOne);
        add.add(imageAddTwo);
        add.add(imageAddThree);

        shanchu.add(imageDeleteOne);
        shanchu.add(imageDeleteTwo);
        shanchu.add(imageDeleteThree);


////        补充的网络请求
//        initBuChongHttp();
//
//
////        补充界面审核状态的网络请求
//        initBuChongShenHeZhuangTaiHttp();
    }


    //    企业服务补充的网络请求
    private void initBuChongXiangQingHttp() {
        OkHttpUtils.post()
                .url(ContractUtils.LOGIN_URL + "fuwu/myFuwuxiang")
                .addHeader("token", ContractUtils.getTOKEN(BuChongWuYeFuWuActivity.this))
                .addParams("entype", entype)
                .addParams("id", id)
                .addParams("pk_id",ContractUtils.getParkId(BuChongWuYeFuWuActivity.this))
                .build()
                .execute(new StringCallback() {
                    @Override
                    public void onError(Call call, Exception e, int id) {

                    }

                    @Override
                    public void onResponse(String response, int id) {
                        System.out.println(response + "              企业服务补充的网络请求 ");
                        ContractUtils.Code500(BuChongWuYeFuWuActivity.this, response);
                        if (response.indexOf("200") != -1) {
                            Gson gson = new Gson();
                            response = response.replace(" ","");
                            fuWuJiLuShenQingXiangQingEntity = gson.fromJson(response, FuWuJiLuShenQingXiangQingEntity.class);
                            if (fuWuJiLuShenQingXiangQingEntity.getResponse().getEntype().equals("")) {

                            } else {
                                if (fuWuJiLuShenQingXiangQingEntity.getResponse().getEntype().equals("1")) {
                                    tvBiaoti.setText("企业补充服务申请");
                                } else if (fuWuJiLuShenQingXiangQingEntity.getResponse().getEntype().equals("2")) {
                                    tvBiaoti.setText("优惠券补充服务申请");
                                } else if (fuWuJiLuShenQingXiangQingEntity.getResponse().getEntype().equals("3")) {
                                    tvBiaoti.setText("物业补充服务申请");
                                }
                            }



                            if (fuWuJiLuShenQingXiangQingEntity.getResponse().getServicename() != null) {
                                tvTitle.setText(fuWuJiLuShenQingXiangQingEntity.getResponse().getServicename());
                            }


                            if (fuWuJiLuShenQingXiangQingEntity.getResponse().getAdd_time() != null) {
                                String add_time = fuWuJiLuShenQingXiangQingEntity.getResponse().getAdd_time();
                                String substring = add_time.substring(0, 10);
                                String substring1 = add_time.substring(10, add_time.length());
                                System.out.println(substring+"      substring");
                                System.out.println(substring1+"     substring1substring1substring1");
                                tvTime.setText(substring+" "+substring1);

//                                tvTime.setText(fuWuJiLuShenQingXiangQingEntity.getResponse().getAdd_time());
                            }


                            if (fuWuJiLuShenQingXiangQingEntity.getResponse().getRemarks() != null) {
                                etNeirong.setText(fuWuJiLuShenQingXiangQingEntity.getResponse().getRemarks());
                            } else if(fuWuJiLuShenQingXiangQingEntity.getResponse().getContent() != null){
                                etNeirong.setText(fuWuJiLuShenQingXiangQingEntity.getResponse().getContent());
                            }


                            if (fuWuJiLuShenQingXiangQingEntity.getResponse().getSqname() != null) {
                                tvName.setText(fuWuJiLuShenQingXiangQingEntity.getResponse().getSqname());
                            }


                            if (fuWuJiLuShenQingXiangQingEntity.getResponse().getApplyPhone() != null) {
//                                tvPhone.setText(fuWuJiLuShenQingXiangQingEntity.getResponse().getApplyPhone());
                            }else if (fuWuJiLuShenQingXiangQingEntity.getResponse().getName() != null) {
//                                tvPhone.setText(fuWuJiLuShenQingXiangQingEntity.getResponse().getName());
                            }


                            buChongAdapter = new BuChongAdapter(BuChongWuYeFuWuActivity.this, fuWuJiLuShenQingXiangQingEntity.getResponse(), fuWuJiLuShenQingXiangQingEntity.getResponse().getBuc());
                            LinearLayoutManager manager = new LinearLayoutManager(BuChongWuYeFuWuActivity.this);
                            recyclerView.setLayoutManager(manager);
                            recyclerView.setAdapter(buChongAdapter);
                        }
                    }
                });
    }


////    补充界面审核状态的网络请求
//    private void initBuChongShenHeZhuangTaiHttp() {
//        OkHttpUtils.post()
//                .url(ContractUtils.LOGIN_URL+"fuwu/myFuwuDetail")
//                .addHeader("token",ContractUtils.getTOKEN(BuChongWuYeFuWuActivity.this))
//                .addParams("id",id)
//                .build()
//                .execute(new StringCallback() {
//                    @Override
//                    public void onError(Call call, Exception e, int id) {
//
//                    }
//
//                    @Override
//                    public void onResponse(String response, int id) {
//                        ContractUtils.Code500(BuChongWuYeFuWuActivity.this,response);
//                        System.out.println(response+"          ");
//                        Gson gson = new Gson();
//                        fuWuJiLuShenQingXiangQingEntity = gson.fromJson(response, FuWuJiLuShenQingXiangQingEntity.class);
//                        if(response.indexOf("200") != -1){
//                            buChongAdapter = new BuChongAdapter(BuChongWuYeFuWuActivity.this,fuWuJiLuShenQingXiangQingEntity.getResponse().getFankui());
//                            LinearLayoutManager manager = new LinearLayoutManager(BuChongWuYeFuWuActivity.this);
//                            recyclerView.setLayoutManager(manager);
//                            recyclerView.setAdapter(buChongAdapter);
//                        }
//                    }
//                });
//    }


//    //    补充的网络请求
//    private void initBuChongHttp() {
//        OkHttpUtils.post()
//                .url(ContractUtils.LOGIN_URL + "fuwu/myFuwuInfo")
//                .addHeader("token", ContractUtils.getTOKEN(BuChongWuYeFuWuActivity.this))
//                .addParams("id", id)
//                .build()
//                .execute(new StringCallback() {
//                    @Override
//                    public void onError(Call call, Exception e, int id) {
//
//                    }
//
//                    @Override
//                    public void onResponse(String response, int id) {
//                        ContractUtils.Code500(BuChongWuYeFuWuActivity.this,response);
//                        System.out.println(response + "       补充的网络请求");
//                        Gson gson = new Gson();
//                        BuChongEntity buChongEntity = gson.fromJson(response, BuChongEntity.class);
//                        if (response.indexOf("200") != -1) {
//                            if (buChongEntity.getResponse().getTitle() == null) {
//
//                            } else {
//                                tvTitle.setText(buChongEntity.getResponse().getTitle());
//                            }
//
//                            if (buChongEntity.getResponse().getAdd_time() == null) {
//
//                            } else {
//                                tvTime.setText(buChongEntity.getResponse().getAdd_time());
//                            }
//
//
//                            if (buChongEntity.getResponse().getName() == null) {
//
//                            } else {
//                                tvName.setText(buChongEntity.getResponse().getName());
//                            }
//
//
//                            if (buChongEntity.getResponse().getPhone() == null) {
//
//                            } else {
//                                tvPhone.setText(buChongEntity.getResponse().getPhone());
//                            }
//
//
//
//                        }
//                    }
//                });
//    }


    @OnClick({R.id.image_back, R.id.tv_bianji, R.id.tv_title, R.id.tv_time, R.id.et_neirong, R.id.image_add_one, R.id.image_delete_one, R.id.image_add_two, R.id.image_delete_two, R.id.image_add_three, R.id.image_delete_three})
    public void onViewClicked(View view) {
        switch (view.getId()) {
//            返回按钮
            case R.id.image_back:
                finish();
                break;
//            编辑
            case R.id.tv_bianji:
                if (etNeirong.getText().toString().trim().equals("")) {
                    Toast.makeText(this, "请输入内容", Toast.LENGTH_SHORT).show();
                } else {
                    if (list1.size() == 0) {
//                        没有图片的
                        //   确认编辑的网络请求
                        initBianJiHttp();
                    }
                    for (int i = 0; i < list1.size(); i++) {
                        uploadFile(list1.get(i));
                    }


                }
                break;
            case R.id.tv_title:
                break;
            case R.id.tv_time:
                break;
            case R.id.et_neirong:
                break;
            case R.id.image_add_one://第一张图片添加
                ISOK = 0;
                //                弹出上传图片的弹窗
                initTanChu();
                break;
            case R.id.image_add_two://第二张图片添加
                ISOK = 1;
                //                弹出上传图片的弹窗
                initTanChu();
                break;
            case R.id.image_add_three://第三张图片添加
                ISOK = 2;
                //                弹出上传图片的弹窗
                initTanChu();
                break;
            case R.id.image_delete_one://删除第一张
                list1.remove(0);
                TuPian();
                break;
            case R.id.image_delete_two://删除第二张
                list1.remove(1);
                TuPian();
                break;
            case R.id.image_delete_three://删除第三张
                list1.remove(2);
                TuPian();
                break;
        }
    }

    //    确认编辑的网络请求
    private void initBianJiHttp() {
        System.out.println(ContractUtils.getTOKEN(BuChongWuYeFuWuActivity.this)+"      token ");
        System.out.println(id+"       id");
        System.out.println(entype+"         entype");
        System.out.println(etNeirong.getText().toString().trim()    +"     Remarks");

        OkHttpUtils.post()
                .url(ContractUtils.LOGIN_URL + "fuwu/buchong")
                .addHeader("token", ContractUtils.getTOKEN(BuChongWuYeFuWuActivity.this))
                .addParams("id", id)
                .addParams("entype", entype)
                .addParams("Remarks", etNeirong.getText().toString().trim())
                .build()
                .execute(new StringCallback() {
                    @Override
                    public void onError(Call call, Exception e, int id) {
                        System.out.println(e + "        eeee编辑");
                    }

                    @Override
                    public void onResponse(String response, int id) {
                        ContractUtils.Code500(BuChongWuYeFuWuActivity.this, response);
                        System.out.println(response + "       编辑的网络请求");
                        if (response.indexOf("200") != -1) {
                            Gson gson = new Gson();
                            BuChongEntity buChongEntity = gson.fromJson(response, BuChongEntity.class);
                            Toast.makeText(BuChongWuYeFuWuActivity.this, buChongEntity.getResponse().getMes(), Toast.LENGTH_SHORT).show();
                            finish();
                        } else {
                           ContractUtils.Code400(BuChongWuYeFuWuActivity.this,response);
                        }
                    }
                });
    }


    //    图片显示与隐藏
    private void TuPian() {
        for (int i = 0; i < add.size(); i++) {
            ImageView delete = shanchu.get(i);
            ImageView image = add.get(i);
            if (i <= list1.size() - 1) {
                image.setImageURI(Uri.parse(list1.get(i)));
            }
            if (i <= list1.size() - 1) {
                image.setVisibility(View.VISIBLE);
                delete.setVisibility(View.VISIBLE);
            } else if (i == list1.size()) {
                image.setVisibility(View.VISIBLE);
                delete.setVisibility(View.GONE);
                image.setImageResource(R.drawable.add);
            } else {
                image.setVisibility(View.GONE);
                delete.setVisibility(View.GONE);
            }
        }


//        for (int i = 0; i < add.size(); i++) {
//            ImageView delete = shanchu.get(i);
//            ImageView image = add.get(i);
//            if (i <= list.size() - 1) {
//                image.setImageURI(Uri.parse(list.get(i).path));
//            }
//            if (i <= list.size() - 1) {
//                image.setVisibility(View.VISIBLE);
//                delete.setVisibility(View.VISIBLE);
//            } else if (i == list.size()) {
//                image.setVisibility(View.VISIBLE);
//                delete.setVisibility(View.GONE);
//                image.setImageResource(R.drawable.add);
//            } else {
//                image.setVisibility(View.GONE);
//                delete.setVisibility(View.GONE);
//            }
//        }
    }


    //    弹出上传图片的弹窗
    private void initTanChu() {
        Object itemsOnClick = null;
        takePhotoPopWin = new TakePhotoPopWin(BuChongWuYeFuWuActivity.this, (View.OnClickListener) itemsOnClick);
        takePhotoPopWin.showAtLocation(findViewById(R.id.relative_buchong), Gravity.BOTTOM | Gravity.CENTER_HORIZONTAL, 0, 0);


        //        点击相机

        takePhotoPopWin.setOnClickXiangji(new TakePhotoPopWin.OnClickXiangji() {
            @Override
            public void onClick(int id) {
//                        Intent open = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
//                        startActivityForResult(open,100);


                //                判断当前SDK版本号
                if (Build.VERSION.SDK_INT >= 23) {
//                    当前拨打电话权限是没有给的
                    if (ContextCompat.checkSelfPermission(BuChongWuYeFuWuActivity.this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
//                        我要请求权限
                        ActivityCompat.requestPermissions(BuChongWuYeFuWuActivity.this, new String[]{Manifest.permission.CAMERA}, 1);
                    } else {
//                                SimpleDateFormat timeStampFormat = new SimpleDateFormat("yyyyMMddHHmmss");
//                                String filename = timeStampFormat.format(new Date());
//                                ContentValues values = new ContentValues(); //使用本地相册保存拍摄照片
//                                values.put(MediaStore.Images.Media.TITLE, filename);
//                                photoUri = getContentResolver().insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);
//
//                                // 设置 intent.putExtra(MediaStore.EXTRA_OUTPUT, photoUri); photoUri = 保存图片得uri
//                                Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
//                                intent.putExtra(MediaStore.EXTRA_OUTPUT, photoUri);
//                                startActivityForResult(intent, 100);

                        if (Environment.MEDIA_MOUNTED.equals(Environment.getExternalStorageState())) {
                            //创建文件夹 在本地文件Pictures 目录下创建 自己得文件夹
                            File dir = new File(Environment.getExternalStorageDirectory() + "/Pictures/image");
                            if (!dir.exists()) {
                                dir.mkdirs();
                            }
                            //照片名称
                            File file = new File(dir, new SimpleDateFormat("yyyyMMddHHmmss").format(new Date()) + ".jpg");
                            photoUri = Uri.parse(file.getPath());

                            startActivityForResult(new Intent(MediaStore.ACTION_IMAGE_CAPTURE)
                                    .putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(file)), 100);

                        } else {
//                                    showToast("请检查SDCard！");
                        }
                    }
                } else {
//                            SimpleDateFormat timeStampFormat = new SimpleDateFormat("yyyyMMddHHmmss");
//                            String filename = timeStampFormat.format(new Date());
//                            ContentValues values = new ContentValues(); //使用本地相册保存拍摄照片
//                            values.put(MediaStore.Images.Media.TITLE, filename);
//                            photoUri = getContentResolver().insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);
//
//                            // 设置 intent.putExtra(MediaStore.EXTRA_OUTPUT, photoUri); photoUri = 保存图片得uri
//                            Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
//                            intent.putExtra(MediaStore.EXTRA_OUTPUT, photoUri);
//                            startActivityForResult(intent, 100);


                    if (Environment.MEDIA_MOUNTED.equals(Environment.getExternalStorageState())) {
                        //创建文件夹 在本地文件Pictures 目录下创建 自己得文件夹
                        File dir = new File(Environment.getExternalStorageDirectory() + "/Pictures/image");
                        if (!dir.exists()) {
                            dir.mkdirs();
                        }
                        //照片名称
                        File file = new File(dir, new SimpleDateFormat("yyyyMMddHHmmss").format(new Date()) + ".jpg");
                        photoUri = Uri.parse(file.getPath());

                        startActivityForResult(new Intent(MediaStore.ACTION_IMAGE_CAPTURE)
                                .putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(file)), 100);

                    } else {
//                                    showToast("请检查SDCard！");
                    }
                }


//
//                        SimpleDateFormat timeStampFormat = new SimpleDateFormat("yyyyMMddHHmmss");
//                        String filename = timeStampFormat.format(new Date());
//                        ContentValues values = new ContentValues(); //使用本地相册保存拍摄照片
//                        values.put(MediaStore.Images.Media.TITLE, filename);
//                        photoUri = getContentResolver().insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);
//
//                        // 设置 intent.putExtra(MediaStore.EXTRA_OUTPUT, photoUri); photoUri = 保存图片得uri
//                        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
//                        intent.putExtra(MediaStore.EXTRA_OUTPUT, photoUri);
//                        startActivityForResult(intent, 100);


//                        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
//                        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {//判断是否有相机应用
//                            startActivityForResult(takePictureIntent, REQ_THUMB);
//
//                        }
            }
        });


        //                点击相册
        takePhotoPopWin.setOnClickXiangce(new TakePhotoPopWin.OnClickXiangce() {
            @Override
            public void onClick(int id) {
//                        Intent local = new Intent();
//                        local.setType("image/*");
//                        local.setAction(Intent.ACTION_GET_CONTENT);
//                        startActivityForResult(local, 2);

                ImagePicker imagePicker = ImagePicker.getInstance();
                imagePicker.setImageLoader(new GlideImageLoaderForPicker());   //设置图片加载器
                imagePicker.setShowCamera(true);  //显示拍照按钮
                imagePicker.setCrop(false);        //允许裁剪（单选才有效）
                imagePicker.setSaveRectangle(true);//是否按矩形区域保存
                imagePicker.setFocusHeight(169);
                imagePicker.setFocusWidth(169);
                imagePicker.setCrop(false);
                imagePicker.setSelectLimit(1);    //选中数量限制
                imagePicker.setOutPutX(800);//保存文件的宽度。单位像素
                imagePicker.setOutPutY(800);//保存文件的高度。单位像素

                Intent intent = new Intent(BuChongWuYeFuWuActivity.this, ImageGridActivity.class);
                startActivityForResult(intent, 1);

            }
        });
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);


        //        相机回调
        if (requestCode == 100) {
            String string = photoUri.toString();
            File file = new File(string);
            System.out.println(file.exists()+"      file");
            if(file.exists() == false){
                return;
            }

            if (list1.size() - 1 >= ISOK) {
                list1.set(ISOK, string);
            } else {
                list1.add(string);
            }
            TuPian();

        }


        if (data != null && requestCode == 1) {
            ArrayList<ImageItem> images = (ArrayList<ImageItem>) data.getSerializableExtra(ImagePicker.EXTRA_RESULT_ITEMS);
            if (images != null) {
                if (list1.size() - 1 >= ISOK) {
                    list1.set(ISOK, images.get(0).path);
                } else {
                    list1.add(images.get(0).path);
                }
                TuPian();


//                base转64
//                String path = images.get(0).path;
//                imageToBase64(path);
//                System.out.println(imageToBase64(path)+"   path");


            } else {
//                Toast.makeText(this, "图片解析失败", Toast.LENGTH_SHORT).show();
            }
        } else {
//            Toast.makeText(this, "没有数据", Toast.LENGTH_SHORT).show();
        }
    }





    //    确认编辑的网络请求
    private void initBianJiYouTuPianHttp() {
        String url = "";
        for (int i = 0; i < imageUrl.size(); i++) {
            url = url + imageUrl.get(i) + ",";
        }
        url = url.substring(0, url.length() - 1);


        OkHttpUtils.post()
                .url(ContractUtils.LOGIN_URL + "fuwu/buchong")
                .addHeader("token", ContractUtils.getTOKEN(BuChongWuYeFuWuActivity.this))
                .addParams("id", id)
                .addParams("entype", entype)
                .addParams("Remarks", etNeirong.getText().toString().trim())
                .addParams("phone", url)
                .build()
                .execute(new StringCallback() {
                    @Override
                    public void onError(Call call, Exception e, int id) {
                        System.out.println(e + "        eeee编辑");
                    }

                    @Override
                    public void onResponse(String response, int id) {
                        ContractUtils.Code500(BuChongWuYeFuWuActivity.this, response);
                        System.out.println(response + "       编辑的网络请求");
                        if (response.indexOf("200") != -1) {
                            Gson gson = new Gson();
                            BuChongEntity buChongEntity = gson.fromJson(response, BuChongEntity.class);
                            Toast.makeText(BuChongWuYeFuWuActivity.this, buChongEntity.getResponse().getMes(), Toast.LENGTH_SHORT).show();
                            finish();
                        } else {
                            ContractUtils.Code400(BuChongWuYeFuWuActivity.this, response);
                        }
                    }
                });
    }


    //
    private void uploadFile(String imageItem) {
        final ProgressDialog progressDialog = new ProgressDialog(BuChongWuYeFuWuActivity.this);
        progressDialog.setTitle("提示");
        progressDialog.setMessage("请等待...");
        progressDialog.setCancelable(false);
        progressDialog.show();

        File file = new File(imageItem);
        Luban.with(this)
                .load(file)
                .ignoreBy(100)
                //  .setTargetDir(AppConfig.DEFAULT_SAVE_IMAGE_PATH)
                .setCompressListener(new OnCompressListener() {
                    @Override
                    public void onStart() {
                        // TODO 压缩开始前调用，可以在方法内启动 loading UI
                    }

                    @Override
                    public void onSuccess(File file) {
//                        String url = ContractUtils.LOGIN_URL+"ceshi/ce_upload.html";
                        OkHttpClient okHttpClient = new OkHttpClient();
//                        RequestBody requestBody1 =new FormBody.Builder()
//                                .add("driverid",substring)
//                                .add("driver_img_type","1")
//                                .build();
                        RequestBody requestBody = new MultipartBody.Builder()
                                .setType(MultipartBody.FORM)
                                .addFormDataPart("file", file.getName(), RequestBody.create(MediaType.parse("image/*"), file))
                                .build();

                        Request request = new Request.Builder()
                                .url(ContractUtils.LOGIN_URL + "index/upload")
                                .post(requestBody)
                                .addHeader("token", ContractUtils.getTOKEN(BuChongWuYeFuWuActivity.this))
                                .build();

                        okHttpClient.newCall(request).enqueue(new Callback() {
                            @Override
                            public void onFailure(Call call, IOException e) {
                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
//                                        Toast.makeText(BuChongWuYeFuWuActivity.this, "网络异常", Toast.LENGTH_SHORT).show();
                                    }
                                });
                                progressDialog.dismiss();
                            }

                            @Override
                            public void onResponse(Call call, final Response response) throws IOException {
                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
//                                        Toast.makeText(PeopleInfoActivity.this, "上传成功", Toast.LENGTH_SHORT).show();
                                    }
                                });


                                progressDialog.dismiss();
                                String string = response.body().string();
                                System.out.println(string + "   记录服务上传图片");
                                Gson gson = new Gson();
                                photoEntity = gson.fromJson(string, PhotoEntity.class);
                                imageUrl.add(photoEntity.getResponse().getFileurl());
                                if (imageUrl.size() == list1.size()) {

//                                    申请的网络请求 tiantian
//                                    有图片的确认编辑的网络请求
                                    initBianJiYouTuPianHttp();

                                }
                                System.out.println(string + "   记记服务上传图片");
                            }
                        });
                    }

                    @Override
                    public void onError(Throwable e) {

                    }
                }).launch();
    }


}
