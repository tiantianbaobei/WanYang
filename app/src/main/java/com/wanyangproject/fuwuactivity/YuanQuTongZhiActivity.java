package com.wanyangproject.fuwuactivity;

import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.wanyangproject.R;
import com.wanyangproject.fragment.FirstFragment;
import com.wanyangproject.fragment.SecondFragment;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

//  园区通知界面
public class YuanQuTongZhiActivity extends AppCompatActivity implements View.OnClickListener {

    @BindView(R.id.image_back)
    ImageView imageBack;
    @BindView(R.id.tv_yuanqu_tongzhi)
    TextView tvYuanquTongzhi;
    @BindView(R.id.tv_yuanqu_zixun)
    TextView tvYuanquzixun;
    //    @BindView(R.id.tabLayout)
//    TabLayout tabLayout;
//    @BindView(R.id.viewPager)
//    ViewPager viewPager;
    private FirstFragment firstFragment;
    private SecondFragment secondFragment;
    private FragmentManager fragmentManager;
    private Fragment hideFragment;
    private String zhi;
    private FragmentTransaction fragmentTransaction;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_yuan_qu_tong_zhi);
        ButterKnife.bind(this);
        if (Build.VERSION.SDK_INT >= 21) {
            View dview = getWindow().getDecorView();
            dview.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN | View.SYSTEM_UI_FLAG_LAYOUT_STABLE);
            getWindow().setStatusBarColor(Color.TRANSPARENT);
        }

//        initViewPager();
        initView();
        initDefaultFragment();






        Intent intent = getIntent();
        zhi = intent.getStringExtra("zhi");
        if(zhi != null){
            tvYuanquzixun.setBackgroundResource(R.drawable.yuanqu_tongzi_white_background_right);
            tvYuanquzixun.setTextColor(getResources().getColor(R.color.red_color));
            tvYuanquTongzhi.setBackgroundResource(R.drawable.yuanqu_tongzi_red_background_left);
            tvYuanquTongzhi.setTextColor(getResources().getColor(R.color.white_color));
            replaceFragment1(secondFragment);
        }
    }



    private void initDefaultFragment() {
        fragmentManager = getSupportFragmentManager();
        fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.add(R.id.container_fragment, firstFragment);
        fragmentTransaction.commit();
        hideFragment = firstFragment;
    }



    private void replaceFragment(Fragment showFragment, FragmentTransaction fragmentTransaction) {
        if (showFragment.isAdded()) {
            fragmentTransaction.hide(hideFragment).show(showFragment).commit();
        } else {
            fragmentTransaction.hide(hideFragment).add(R.id.container_fragment, showFragment).commit();
        }
        hideFragment = showFragment;
    }



    private void replaceFragment1(Fragment showFragment) {
        fragmentTransaction = fragmentManager.beginTransaction();
        if (showFragment.isAdded()) {
            fragmentTransaction.hide(hideFragment).show(showFragment).commit();
        } else {
            fragmentTransaction.hide(hideFragment).add(R.id.container_fragment, showFragment).commit();
        }
        hideFragment = showFragment;
    }




    //    初始化
    private void initView() {
        firstFragment = new FirstFragment();
        secondFragment = new SecondFragment();
        tvYuanquTongzhi.setOnClickListener(this);
        tvYuanquzixun.setOnClickListener(this);




    }

//    private void initViewPager() {
//        List<Fragment> fragments = new ArrayList<>();
//        fragments.add(new FirstFragment());
//        fragments.add(new SecondFragment());
//        // 创建ViewPager适配器
//        MyPagerAdapter myPagerAdapter = new MyPagerAdapter(getSupportFragmentManager());
//        myPagerAdapter.setFragments(fragments);
//        // 给ViewPager设置适配器
//        viewPager.setAdapter(myPagerAdapter);
//        // TabLayout 指示器 (记得自己手动创建4个Fragment,注意是 app包下的Fragment 还是 V4包下的 Fragment)
//        tabLayout.addTab(tabLayout.newTab());
//        tabLayout.addTab(tabLayout.newTab());
//        // 使用 TabLayout 和 ViewPager 相关联
//        tabLayout.setupWithViewPager(viewPager);
//        // TabLayout指示器添加文本
//        tabLayout.getTabAt(0).setText("园区通知");
//        tabLayout.getTabAt(1).setText("园区资讯");
//    }


    @OnClick({R.id.image_back})
    public void onViewClicked(View view) {
        switch (view.getId()) {
//            返回按钮
            case R.id.image_back:
                finish();
                break;
        }
    }


    @Override
    public void onClick(View view) {
        fragmentTransaction = fragmentManager.beginTransaction();
        switch (view.getId()){
            case R.id.tv_yuanqu_tongzhi:
                tvYuanquTongzhi.setBackgroundResource(R.drawable.yuanqu_tongzi_white_background);
                tvYuanquTongzhi.setTextColor(getResources().getColor(R.color.red_color));
                tvYuanquzixun.setBackgroundResource(R.drawable.yuanqu_tongzi_red_background);
                tvYuanquzixun.setTextColor(getResources().getColor(R.color.white_color));
                replaceFragment(firstFragment, fragmentTransaction);
                break;
            case R.id.tv_yuanqu_zixun:
                tvYuanquzixun.setBackgroundResource(R.drawable.yuanqu_tongzi_white_background_right);
                tvYuanquzixun.setTextColor(getResources().getColor(R.color.red_color));
                tvYuanquTongzhi.setBackgroundResource(R.drawable.yuanqu_tongzi_red_background_left);
                tvYuanquTongzhi.setTextColor(getResources().getColor(R.color.white_color));
                replaceFragment(secondFragment, fragmentTransaction);
                break;
        }
    }
}
