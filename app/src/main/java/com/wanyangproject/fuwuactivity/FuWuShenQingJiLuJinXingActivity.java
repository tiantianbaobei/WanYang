package com.wanyangproject.fuwuactivity;

import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.wanyangproject.R;
import com.wanyangproject.adapter.FuWuJiLuShenHeZhuangTaiAdapter;
import com.wanyangproject.entity.FuWuJiLuShenQingXiangQingEntity;
import com.wanyangproject.entity.JieShuShenQingEntity;
import com.wanyangproject.utils.ContractUtils;
import com.zhy.http.okhttp.OkHttpUtils;
import com.zhy.http.okhttp.callback.StringCallback;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.Call;

//服务界面 申请记录进行中
public class FuWuShenQingJiLuJinXingActivity extends AppCompatActivity {

    @BindView(R.id.image_back)
    ImageView imageBack;
    @BindView(R.id.tv_leixing)
    TextView tvLeixing;
    @BindView(R.id.tv_time)
    TextView tvTime;
    @BindView(R.id.tv_beizhu)
    TextView tvBeizhu;
    @BindView(R.id.tv_name)
    TextView tvName;
    @BindView(R.id.tv_phone)
    TextView tvPhone;
    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    @BindView(R.id.tv_jieshu)
    TextView tvJieshu;
    @BindView(R.id.tv_title)
    TextView tvTitle;
    private String id;
    private FuWuJiLuShenHeZhuangTaiAdapter fuWuJiLuShenHeZhuangTaiAdapter;
    private FuWuJiLuShenQingXiangQingEntity fuWuJiLuShenQingXiangQingEntity;
    private String entype;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fu_wu_shen_qing_ji_lu_jin_xing);
        ButterKnife.bind(this);
        if (Build.VERSION.SDK_INT >= 21) {
            View dview = getWindow().getDecorView();
            dview.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN | View.SYSTEM_UI_FLAG_LAYOUT_STABLE);
            getWindow().setStatusBarColor(Color.TRANSPARENT);
        }


        Intent intent = getIntent();
        id = intent.getStringExtra("id");
        entype = intent.getStringExtra("entype");
        System.out.println();
        System.out.println(id + "        申请服务的id");


//        1企业  2优惠券 3物业
        if(entype.equals("3")){
            tvJieshu.setVisibility(View.VISIBLE);
        }else if(entype.equals("2")){
            tvJieshu.setVisibility(View.GONE);
        }else if(entype.equals("1")){
            tvJieshu.setVisibility(View.VISIBLE);
        }

//        申请服务记录的申请详情的网络请求
        initFuWuShenQingJiLuXiangQingHttp();
    }



    @Override
    protected void onResume() {
        super.onResume();

//        申请服务记录的申请详情的网络请求
        initFuWuShenQingJiLuXiangQingHttp();
    }




    //    申请服务记录的申请详情的网络请求
    private void initFuWuShenQingJiLuXiangQingHttp() {
        OkHttpUtils.post()
                .url(ContractUtils.LOGIN_URL + "fuwu/myFuwuxiang")
                .addHeader("token", ContractUtils.getTOKEN(FuWuShenQingJiLuJinXingActivity.this))
                .addParams("id", id)
                .addParams("entype", entype)
                .addParams("pk_id",ContractUtils.getParkId(FuWuShenQingJiLuJinXingActivity.this))
                .build()
                .execute(new StringCallback() {
                    @Override
                    public void onError(Call call, Exception e, int id) {

                    }

                    @Override
                    public void onResponse(String response, int id) {
                        ContractUtils.Code500(FuWuShenQingJiLuJinXingActivity.this, response);
                        System.out.println(response + "         申请服务记录的申请详情的网络请求");

                        if (response.indexOf("200") != -1) {
                            Gson gson = new Gson();
                            response = response.replace(" ","");
                            fuWuJiLuShenQingXiangQingEntity = gson.fromJson(response, FuWuJiLuShenQingXiangQingEntity.class);



//                              0:待处理 1:处理中 2:完成 3:退回 4:关闭
                            if(fuWuJiLuShenQingXiangQingEntity.getResponse().getEntype().equals("1")){
                                if(fuWuJiLuShenQingXiangQingEntity.getResponse().getBuc().get(0).getEntype().equals("2") || fuWuJiLuShenQingXiangQingEntity.getResponse().getBuc().get(0).getEntype().equals("4")){
                                    tvJieshu.setVisibility(View.GONE);
                                }else {
                                    tvJieshu.setVisibility(View.VISIBLE);
                                }
                            }

//                              0:待处理 1:处理中 2:完成 3:退回 4:关闭
                            if(fuWuJiLuShenQingXiangQingEntity.getResponse().getEntype().equals("3")){
                                if(fuWuJiLuShenQingXiangQingEntity.getResponse().getBuc().get(0).getEntype().equals("2") || fuWuJiLuShenQingXiangQingEntity.getResponse().getBuc().get(0).getEntype().equals("4")){
                                    tvJieshu.setVisibility(View.GONE);
                                }else {
                                    tvJieshu.setVisibility(View.VISIBLE);
                                }
                            }



//                            原来
//                            if(fuWuJiLuShenQingXiangQingEntity.getResponse().getBuc() != null){
////                                fuWuJiLuShenQingXiangQingEntity.getResponse().getBuc().clear();
//                            }



                            if(fuWuJiLuShenQingXiangQingEntity.getResponse().getEntype() != null){
                                if(fuWuJiLuShenQingXiangQingEntity.getResponse().getBuc() == null){
                                    ArrayList<FuWuJiLuShenQingXiangQingEntity.ResponseBean.BucBean> list = new ArrayList<FuWuJiLuShenQingXiangQingEntity.ResponseBean.BucBean>();
                                    fuWuJiLuShenQingXiangQingEntity.getResponse().setBuc(list);
                                }
                                if(fuWuJiLuShenQingXiangQingEntity.getResponse().getBuc() != null){
                                    if(fuWuJiLuShenQingXiangQingEntity.getResponse().getBuc().size() == 0){
                                        FuWuJiLuShenQingXiangQingEntity.ResponseBean.BucBean bucBean = new FuWuJiLuShenQingXiangQingEntity.ResponseBean.BucBean();
                                        bucBean.setEntype("0");
                                        bucBean.setGuan_id("1");
                                        bucBean.setId("1");
                                        bucBean.setTime(fuWuJiLuShenQingXiangQingEntity.getResponse().getAdd_time());
                                        bucBean.setRemarks("备注");
                                        bucBean.setPhoto("");
                                        bucBean.setRefusal("您的申请待处理");
                                        fuWuJiLuShenQingXiangQingEntity.getResponse().getBuc().add(bucBean);
                                    }
                                }

//                                if(fuWuJiLuShenQingXiangQingEntity.getResponse().getEntype().equals("1")){
////                                    0:待处理 1:处理中 2:完成 3:退回 4:关闭
//                                }else{
//
//
//                                    if(fuWuJiLuShenQingXiangQingEntity.getResponse().getBuc().size() > 0){
//
//                                    }else{
//                                        if(fuWuJiLuShenQingXiangQingEntity.getResponse().getEntype().equals("6") || fuWuJiLuShenQingXiangQingEntity.getResponse().getEntype().equals("1")){
//
//                                        }else{
//
//                                        }
//                                    }
//                                }
                            }



                            if (fuWuJiLuShenQingXiangQingEntity.getResponse().getServicename() != null) {
                                tvLeixing.setText(fuWuJiLuShenQingXiangQingEntity.getResponse().getServicename());
                            }




                            if(fuWuJiLuShenQingXiangQingEntity.getResponse().getEntype() != null){
                                if (fuWuJiLuShenQingXiangQingEntity.getResponse().getEntype().equals("1")) {
                                    tvTitle.setText("企业申请服务");
                                }else if(fuWuJiLuShenQingXiangQingEntity.getResponse().getEntype().equals("2")){
                                    tvTitle.setText("优惠券申请服务");
                                }else if(fuWuJiLuShenQingXiangQingEntity.getResponse().getEntype().equals("3")){
                                    tvTitle.setText("物业申请服务");
                                }
                            }


                            if (fuWuJiLuShenQingXiangQingEntity.getResponse().getAdd_time() != null) {
                                String add_time = fuWuJiLuShenQingXiangQingEntity.getResponse().getAdd_time();
                                String substring = add_time.substring(0, 10);
                                String substring1 = add_time.substring(10, add_time.length());
                                System.out.println(substring+"      substring");
                                System.out.println(substring1+"     substring1substring1substring1");
                                tvTime.setText(substring+" "+substring1);


//                                tvTime.setText(fuWuJiLuShenQingXiangQingEntity.getResponse().getAdd_time());
                            }


                            if (fuWuJiLuShenQingXiangQingEntity.getResponse().getRemarks() != null) {
                                tvBeizhu.setText(fuWuJiLuShenQingXiangQingEntity.getResponse().getRemarks());
                            }else if(fuWuJiLuShenQingXiangQingEntity.getResponse().getContent() != null){
                                tvBeizhu.setText(fuWuJiLuShenQingXiangQingEntity.getResponse().getContent());
                            }


                            if (fuWuJiLuShenQingXiangQingEntity.getResponse().getNickname() != null) {
                                tvName.setText(fuWuJiLuShenQingXiangQingEntity.getResponse().getNickname());
                            }


//                            if (fuWuJiLuShenQingXiangQingEntity.getResponse().getUsername() != null) {
//                                tvPhone.setText(fuWuJiLuShenQingXiangQingEntity.getResponse().getUsername());
//                            }


                            if (fuWuJiLuShenQingXiangQingEntity.getResponse().getSqname() != null) {
                                tvName.setText(fuWuJiLuShenQingXiangQingEntity.getResponse().getSqname());
                            }


//                            if (fuWuJiLuShenQingXiangQingEntity.getResponse().getName() != null) {
//                                tvPhone.setText(fuWuJiLuShenQingXiangQingEntity.getResponse().getName());
//                            }

                            if (fuWuJiLuShenQingXiangQingEntity.getResponse().getApplyPhone() != null) {
                                tvPhone.setText(fuWuJiLuShenQingXiangQingEntity.getResponse().getApplyPhone());
                            }else if(fuWuJiLuShenQingXiangQingEntity.getResponse().getName() != null){
                                tvPhone.setText(fuWuJiLuShenQingXiangQingEntity.getResponse().getName());
                            }



                            fuWuJiLuShenHeZhuangTaiAdapter = new FuWuJiLuShenHeZhuangTaiAdapter(FuWuShenQingJiLuJinXingActivity.this, fuWuJiLuShenQingXiangQingEntity.getResponse(),
                                    fuWuJiLuShenQingXiangQingEntity.getResponse().getBuc(), entype);
                            LinearLayoutManager manager = new LinearLayoutManager(FuWuShenQingJiLuJinXingActivity.this);
                            recyclerView.setLayoutManager(manager);
                            recyclerView.setAdapter(fuWuJiLuShenHeZhuangTaiAdapter);
                        }
                    }
                });
    }


    @OnClick({R.id.image_back, R.id.tv_leixing, R.id.tv_time, R.id.tv_beizhu, R.id.tv_name, R.id.tv_phone, R.id.tv_jieshu})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            //            返回按钮
            case R.id.image_back:
                finish();
                break;
            case R.id.tv_leixing:
                break;
            case R.id.tv_time:
                break;
            case R.id.tv_beizhu:
                break;
            case R.id.tv_name:
                break;
            case R.id.tv_phone:
                break;
//            结束申请
            case R.id.tv_jieshu:
//                0:待处理 1:处理中 2:完成 3:退回 4:关闭
                if(fuWuJiLuShenQingXiangQingEntity.getResponse().getBuc().get(0).getEntype().equals("0") ||fuWuJiLuShenQingXiangQingEntity.getResponse().getBuc().get(0).getEntype().equals("1") ||
                        fuWuJiLuShenQingXiangQingEntity.getResponse().getBuc().get(0).getEntype().equals("3")){
                    // 结束申请的网络请求
                    initJieShuHttp();
                }else{
                    Toast.makeText(this, "当前不能结束！", Toast.LENGTH_SHORT).show();
                    return;
                }

//                if (fuWuJiLuShenQingXiangQingEntity.getResponse().getType().equals("4") || fuWuJiLuShenQingXiangQingEntity.getResponse().getType().equals("5")) {
//                    Toast.makeText(this, "您的申请已完成，不能操作！", Toast.LENGTH_SHORT).show();
//                    return;
//                } else {
//                    if(fuWuJiLuShenQingXiangQingEntity.getResponse().getType().equals("")){
//                        //                结束申请的网络请求
//                        initJieShuHttp();
//                    }
//                }
                break;
            //            补充
//            case R.id.btn_buchong:
//                Intent intent = new Intent(FuWuShenQingJiLuJinXingActivity.this, BuChongWuYeFuWuActivity.class);
//                startActivity(intent);
//                break;
        }
    }


    //    结束申请的网络请求
    private void initJieShuHttp() {
        OkHttpUtils.post()
                .url(ContractUtils.LOGIN_URL + "fuwu/shenhe")
                .addHeader("token", ContractUtils.getTOKEN(FuWuShenQingJiLuJinXingActivity.this))
                .addParams("id", id)
                .addParams("entype",fuWuJiLuShenQingXiangQingEntity.getResponse().getEntype())
                .build()
                .execute(new StringCallback() {
                    @Override
                    public void onError(Call call, Exception e, int id) {

                    }

                    @Override
                    public void onResponse(String response, int id) {
                        System.out.println(response + "             结束申请的网络请求 ");
                        ContractUtils.Code500(FuWuShenQingJiLuJinXingActivity.this, response);
                        if (response.indexOf("200") != -1) {
                            Gson gson = new Gson();
                            JieShuShenQingEntity jieShuShenQingEntity = gson.fromJson(response, JieShuShenQingEntity.class);
                            Toast.makeText(FuWuShenQingJiLuJinXingActivity.this, jieShuShenQingEntity.getResponse().getMes(), Toast.LENGTH_SHORT).show();

                            //       申请服务记录的申请详情的网络请求
                            initFuWuShenQingJiLuXiangQingHttp();
                        }else {
                            ContractUtils.Code400(FuWuShenQingJiLuJinXingActivity.this,response);
                        }
                    }
                });
    }
}
