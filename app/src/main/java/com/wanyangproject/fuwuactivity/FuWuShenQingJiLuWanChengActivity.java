package com.wanyangproject.fuwuactivity;

import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;

import com.wanyangproject.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

//服务界面 申请记录已完成
public class FuWuShenQingJiLuWanChengActivity extends AppCompatActivity {

    @BindView(R.id.image_back)
    ImageView imageBack;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fu_wu_shen_qing_ji_lu_wan_cheng);
        ButterKnife.bind(this);
        if (Build.VERSION.SDK_INT >= 21) {
            View dview = getWindow().getDecorView();
            dview.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN | View.SYSTEM_UI_FLAG_LAYOUT_STABLE);
            getWindow().setStatusBarColor(Color.TRANSPARENT);
        }

    }

    @OnClick(R.id.image_back)
    public void onViewClicked() {
        finish();
    }
}
