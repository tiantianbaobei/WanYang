package com.wanyangproject.fuwuactivity;

import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.wanyangproject.R;
import com.wanyangproject.adapter.QiYeFuWuYouHuiHuoDongAdapter;
import com.wanyangproject.entity.QiYeFuWuEntity;
import com.wanyangproject.entity.QiYeFuWuYouHuiHuoDongEntity;
import com.wanyangproject.utils.ContractUtils;
import com.zhy.http.okhttp.OkHttpUtils;
import com.zhy.http.okhttp.callback.StringCallback;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.Call;

//  企业服务
public class QiYeFuWuActivity extends AppCompatActivity {


    //    @BindView(R.id.relative_huodong)
//    RelativeLayout relativeHuodong;
//    @BindView(R.id.relative_youhuiquan)
//    RelativeLayout relativeYouhuiquan;
    @BindView(R.id.image_back)
    ImageView imageBack;
    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    @BindView(R.id.relative_rl)
    RelativeLayout relativeRl;
    @BindView(R.id.tv_title)
    TextView tvTitle;
    private QiYeFuWuYouHuiHuoDongAdapter qiYeFuWuYouHuiHuoDongAdapter;
    private QiYeFuWuYouHuiHuoDongEntity qiYeFuWuYouHuiHuoDongEntity;
    private QiYeFuWuEntity qiYeFuWuEntity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_qi_ye_fu_wu);
        ButterKnife.bind(this);
        if (Build.VERSION.SDK_INT >= 21) {
            View dview = getWindow().getDecorView();
            dview.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN | View.SYSTEM_UI_FLAG_LAYOUT_STABLE);
            getWindow().setStatusBarColor(Color.TRANSPARENT);
        }

////          企业服务的优惠获活动列表的网络请求
//        initYouHuiHuoDongHttp();


//        企业服务列表的网络请求
        initQiYeFuWuLieBiaoHttp();

        initView();
    }

    //    企业服务列表的网络请求
    private void initQiYeFuWuLieBiaoHttp() {
        OkHttpUtils.post()
                .url(ContractUtils.LOGIN_URL + "fuwu/index")
                .addHeader("token", ContractUtils.getTOKEN(QiYeFuWuActivity.this))
                .addParams("parkId", ContractUtils.getParkId(QiYeFuWuActivity.this))
                .addParams("typeId", "2")//1:物业服务，2：企业服务
                .build()
                .execute(new StringCallback() {
                    @Override
                    public void onError(Call call, Exception e, int id) {

                    }

                    @Override
                    public void onResponse(String response, int id) {
                        ContractUtils.Code500(QiYeFuWuActivity.this,response);
                        System.out.println(response + "     企业服务的网络请求");
                        Gson gson = new Gson();
                        qiYeFuWuEntity = gson.fromJson(response, QiYeFuWuEntity.class);
                        if (response.indexOf("200") != -1) {
                            if(qiYeFuWuEntity.getResponse().getContent() == null){

                            }else{
                                String content = qiYeFuWuEntity.getResponse().getContent();
                                String replace = content.replace("--", "\n");
                                String replace1 = replace.replace("&nbsp;", " ");
                                tvTitle.setText(replace1);
                            }

                            qiYeFuWuYouHuiHuoDongAdapter = new QiYeFuWuYouHuiHuoDongAdapter(QiYeFuWuActivity.this, qiYeFuWuEntity.getResponse().getData());
                            LinearLayoutManager manager = new LinearLayoutManager(QiYeFuWuActivity.this);
                            recyclerView.setLayoutManager(manager);
                            recyclerView.setAdapter(qiYeFuWuYouHuiHuoDongAdapter);

                            qiYeFuWuYouHuiHuoDongAdapter.setXiangQingClick(new QiYeFuWuYouHuiHuoDongAdapter.XiangQingClick() {
                                @Override
                                public void xiangqingClick(int position) {
                                    if(qiYeFuWuEntity.getResponse().getData().get(position).getType().equals("1")){
                                        Intent intent = new Intent(QiYeFuWuActivity.this, QiYeFuWuXiangQingActivity.class);
                                        intent.putExtra("id", qiYeFuWuEntity.getResponse().getData().get(position).getId());
                                        System.out.println(qiYeFuWuEntity.getResponse().getData().get(position).getId() + "   列表点击进入详情传入的id");
                                        startActivity(intent);
                                    }else if(qiYeFuWuEntity.getResponse().getData().get(position).getType().equals("2")){
                                        Toast.makeText(QiYeFuWuActivity.this, "该服务暂未开放！", Toast.LENGTH_SHORT).show();
                                        return;
                                    }

                                }
                            });
                        }else if(response.indexOf("400") != -1){
                            ContractUtils.Code400(QiYeFuWuActivity.this,response);
                        }
                    }
                });
    }

    private void initView() {

    }

//    企业服务的优惠获活动列表的网络请求
//    tiantian
//    private void initYouHuiHuoDongHttp() {
//        OkHttpUtils.post()
//                .url(ContractUtils.LOGIN_URL + "Life/prefe_list")
//                .addHeader("token", ContractUtils.getTOKEN(QiYeFuWuActivity.this))
//                .addParams("parkId", ContractUtils.getParkId(QiYeFuWuActivity.this))
//                .build()
//                .execute(new StringCallback() {
//                    @Override
//                    public void onError(Call call, Exception e, int id) {
//                        System.out.println(e + "     企业服务的优惠获活动列表的网络请求eeeeeeeeeee");
//                    }
//
//                    @Override
//                    public void onResponse(String response, int id) {
//                        System.out.println(response + "     企业服务的优惠获活动列表的网络请求");
//                        Gson gson = new Gson();
//                        qiYeFuWuYouHuiHuoDongEntity = gson.fromJson(response, QiYeFuWuYouHuiHuoDongEntity.class);
//                        if (response.indexOf("200") != -1) {
//                            qiYeFuWuYouHuiHuoDongAdapter = new QiYeFuWuYouHuiHuoDongAdapter(QiYeFuWuActivity.this, qiYeFuWuYouHuiHuoDongEntity.getResponse().getShopPreferences());
//                            LinearLayoutManager manager = new LinearLayoutManager(QiYeFuWuActivity.this);
//                            recyclerView.setLayoutManager(manager);
//                            recyclerView.setAdapter(qiYeFuWuYouHuiHuoDongAdapter);
//
//                            qiYeFuWuYouHuiHuoDongAdapter.setXiangQingClick(new QiYeFuWuYouHuiHuoDongAdapter.XiangQingClick() {
//                                @Override
//                                public void xiangqingClick(int position) {
//                                    Intent intent = new Intent(QiYeFuWuActivity.this, QiYeFuWuXiangQingActivity.class);
//                                    intent.putExtra("id", qiYeFuWuYouHuiHuoDongEntity.getResponse().getShopPreferences().get(position).getId());
//                                    System.out.println(qiYeFuWuYouHuiHuoDongEntity.getResponse().getShopPreferences().get(position).getId() + "   列表点击进入详情传入的id");
//                                    startActivity(intent);
//                                }
//                            });
//
//
//                        }
//                    }
//                });
//    }

    @OnClick({R.id.relative_rl, R.id.image_back})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.relative_rl:
                break;
            case R.id.image_back:
                finish();
                break;
        }
    }

//    @OnClick({R.id.relative_huodong, R.id.relative_youhuiquan,R.id.image_back})
//    public void onViewClicked(View view) {
//        switch (view.getId()) {
//            case R.id.image_back:
//                finish();
//                break;
////            商家优惠活动
//            case R.id.relative_huodong:
//                Intent intent = new Intent(QiYeFuWuActivity.this, QiYeFuWuXiangQingActivity.class);
//                startActivity(intent);
//                break;
////            企业优惠政策
//            case R.id.relative_youhuiquan:
//                Intent intent1 = new Intent(QiYeFuWuActivity.this, QiYeFuWuXiangQingActivity.class);
//                startActivity(intent1);
//                break;
//        }
//    }
}
