package com.wanyangproject.fuwuactivity;

import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.scwang.smartrefresh.layout.listener.OnLoadmoreListener;
import com.scwang.smartrefresh.layout.listener.OnRefreshListener;
import com.wanyangproject.R;
import com.wanyangproject.adapter.FuWuMessageAdapter;
import com.wanyangproject.entity.XiTongXiaoXiEntity;
import com.wanyangproject.utils.ContractUtils;
import com.zhy.http.okhttp.OkHttpUtils;
import com.zhy.http.okhttp.callback.StringCallback;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.Call;

public class FuWuMessageActivity extends AppCompatActivity implements OnRefreshListener, OnLoadmoreListener {

    @BindView(R.id.image_back)
    ImageView imageBack;
    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    @BindView(R.id.tv_quanbu)
    TextView tvQuanbu;
    @BindView(R.id.image_quanxuan)
    CheckBox imageQuanxuan;
    @BindView(R.id.relative)
    RelativeLayout relative;
    @BindView(R.id.tv_delete)
    TextView tvDelete;
    @BindView(R.id.image_delete)
    ImageView imageDelete;
    private FuWuMessageAdapter fuWuMessageAdapter;
    private List<String> list = new ArrayList<>();
    private Boolean isOK = false;
    private int page = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fu_wu_message);
        ButterKnife.bind(this);
        if (Build.VERSION.SDK_INT >= 21) {
            View dview = getWindow().getDecorView();
            dview.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN | View.SYSTEM_UI_FLAG_LAYOUT_STABLE);
            getWindow().setStatusBarColor(Color.TRANSPARENT);
        }


//        系统消息的网络请求
        initXiTongXiaoXiHttp();

        initView();
    }

//    系统消息的网络请求
    private void initXiTongXiaoXiHttp() {
        OkHttpUtils.post()
                .url(ContractUtils.LOGIN_URL+"life/message")
                .addHeader("token",ContractUtils.getTOKEN(FuWuMessageActivity.this))
                .addParams("parkId",ContractUtils.getParkId(FuWuMessageActivity.this))
                .addParams("page","1")
                .build()
                .execute(new StringCallback() {
                    @Override
                    public void onError(Call call, Exception e, int id) {

                    }

                    @Override
                    public void onResponse(String response, int id) {
                        ContractUtils.Code500(FuWuMessageActivity.this,response);
                        System.out.println(response+"          服务消息的网络请求");
                        if(response.indexOf("200") != -1){
                            Gson gson = new Gson();
                            XiTongXiaoXiEntity xiTongXiaoXiEntity = gson.fromJson(response, XiTongXiaoXiEntity.class);
                            fuWuMessageAdapter = new FuWuMessageAdapter(FuWuMessageActivity.this,xiTongXiaoXiEntity.getResponse(),tvDelete,imageDelete,imageQuanxuan,relative);
                            LinearLayoutManager manager = new LinearLayoutManager(FuWuMessageActivity.this);
                            recyclerView.setLayoutManager(manager);
                            recyclerView.setAdapter(fuWuMessageAdapter);
                            fuWuMessageAdapter.setFuWuMessageOnelick(new FuWuMessageAdapter.FuWuMessageOnelick() {
                                @Override
                                public void FuWuMessageOneClick() {
                                    //        系统消息的网络请求
                                    initXiTongXiaoXiHttp();
                                }
                            });
                        }
                    }
                });
    }


    private void initView() {
//        fuWuMessageAdapter = new FuWuMessageAdapter(FuWuMessageActivity.this,imageDelete,tvDelete);
//        LinearLayoutManager manager = new LinearLayoutManager(this);
//        recyclerView.setLayoutManager(manager);
//        recyclerView.setAdapter(fuWuMessageAdapter);

//        fuWuMessageAdapter.setFuWuMessageOnelick(new FuWuMessageAdapter.FuWuMessageOnelick() {
//            @Override
//            public void FuWuMessageOneClick(int position) {
//
//            }
//        });
    }


    @OnClick({R.id.image_back, R.id.recyclerView, R.id.tv_quanbu,R.id.image_delete, R.id.tv_delete, R.id.relative})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.tv_quanbu:
                break;
            case R.id.image_back:
                finish();
                break;
            case R.id.image_delete:
                imageDelete.setVisibility(View.GONE);
                relative.setVisibility(View.VISIBLE);
                tvDelete.setVisibility(View.VISIBLE);
                break;
            case R.id.recyclerView:

                break;
            case R.id.relative:
                break;
            case R.id.tv_delete:
//                删除系统消息
//                tvDelete.setVisibility(View.GONE);
//                imageDelete.setVisibility(View.VISIBLE);
//                if (!isOK) {
//                    relative.setVisibility(View.GONE);
//                    isOK = false;
//                } else {
//                    relative.setVisibility(View.VISIBLE);
//                    isOK = true;
//                }
                break;
        }
    }






    @Override
    public void onRefresh(RefreshLayout refreshlayout) {
        page = 0;
        //        系统消息的网络请求
        initXiTongXiaoXiHttp();
        if (refreshlayout.isRefreshing()) {
            refreshlayout.finishRefresh();
        }
    }


    @Override
    public void onLoadmore(RefreshLayout refreshlayout) {
        page++;
        //        系统消息的网络请求
        initXiTongXiaoXiHttp();
        refreshlayout.finishLoadmore();

    }



}
