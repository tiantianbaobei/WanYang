package com.wanyangproject.activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.IdRes;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.view.KeyEvent;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.umeng.message.PushAgent;
import com.umeng.message.UTrack;
import com.wanyangproject.R;
import com.wanyangproject.fragment.FuWuFragment;
import com.wanyangproject.fragment.LunTanFragment;
import com.wanyangproject.fragment.LunTanTwoFragment;
import com.wanyangproject.fragment.ShouYeFragment;
import com.wanyangproject.myfragment.MineFragment;
import com.wanyangproject.shouye.YuanQuShengHuoActivity;
import com.wanyangproject.shouye.YuanQuZhaoShangActivity;
import com.wanyangproject.shouye.ZhengCeFaGuiActivity;
import com.wanyangproject.shouye.ZhiNengZhiZaoActivity;
import com.wanyangproject.utils.BiaoTiTiaoZhuFuWuFragment;
import com.wanyangproject.utils.ContractUtils;
import com.wanyangproject.utils.FuWuTiaoZhuFragment;
import com.wanyangproject.utils.FuWuZiXunTiaoZhuanFragment;
import com.wanyangproject.utils.TiaoZhuanFragment;

import butterknife.BindView;
import butterknife.ButterKnife;

public class HomeActivity extends AppCompatActivity implements RadioGroup.OnCheckedChangeListener, TiaoZhuanFragment ,FuWuTiaoZhuFragment,FuWuZiXunTiaoZhuanFragment,BiaoTiTiaoZhuFuWuFragment {

    @BindView(R.id.container_fragment)
    FrameLayout containerFragment;
    @BindView(R.id.home_radioGroup)
    RadioGroup homeRadioGroup;
    @BindView(R.id.tv_fuwu)
    RadioButton tvFuwu;
    @BindView(R.id.tv_luntan)
    RadioButton tvLuntan;
    private ShouYeFragment shouYeFragment;
    private FuWuFragment fuWuFragment;
    private LunTanFragment lunTanFragment;
    private LunTanTwoFragment lunTanTwoFragment;
    //    private MyFragment myFragment;
    private FragmentManager fragmentManager;
    private Fragment hideFragment;
    private MineFragment mineFragment;
    private String parkname;

    private long exitTime=0;
    private float zitidaxiao;
    private YuanQuZhaoShangActivity yuanQuZhaoShangActivity;
    private ZhengCeFaGuiActivity zhengCeFaGuiActivity;
    private ZhiNengZhiZaoActivity zhiNengZhiZaoActivity;
    private YuanQuShengHuoActivity yuanQuShengHuoActivity;
    private String luntan;
    private FragmentTransaction fragmentTransaction;
    private String fuwu;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        ButterKnife.bind(this);
        if (Build.VERSION.SDK_INT >= 21) {
            View dview = getWindow().getDecorView();
            dview.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN | View.SYSTEM_UI_FLAG_LAYOUT_STABLE);
            getWindow().setStatusBarColor(Color.TRANSPARENT);
        }


        SharedPreferences sharedPreferences1 = getSharedPreferences("zitidaxiao", Context.MODE_PRIVATE);
        zitidaxiao = sharedPreferences1.getFloat("zitidaxiao", 1);
        System.out.println(zitidaxiao + "     选中的那个字体");

        changeTextSize(HomeActivity.this,zitidaxiao);


//        //获取包管理者对象
//        PackageManager pm = getPackageManager();
//        //获取包的详细信息
//        try {
//            PackageInfo info = pm.getPackageInfo(getPackageName(), 0);
//            //获取版本号和版本名称
//            System.out.println("版本号："+info.versionCode);
//            System.out.println("版本名称："+info.versionName);
//        } catch (PackageManager.NameNotFoundException e) {
//            e.printStackTrace();
//        }

//        初始化
        initView();
        initDefaultFragment();


        Intent intent = getIntent();
        parkname = intent.getStringExtra("parkname");
        System.out.println(parkname + "           parkname");

        shouYeFragment.setTiaoZhuanFragment(this);
        shouYeFragment.setFuWuTiaoZhuFragment(this);



        luntan = intent.getStringExtra("luntan");
        if(luntan != null){
//            replaceFragmentluntan(lunTanFragment);
            tvLuntan.setChecked(true);
        }


        fuwu = intent.getStringExtra("fuwu");
        if(fuwu != null){
//            replaceFragmentfuwu(fuWuFragment);
            tvFuwu.setChecked(true);
        }




//        yuanQuZhaoShangActivity.setTiaoZhuanFragment(this);
//        yuanQuZhaoShangActivity.setFuWuTiaoZhuFragment(this);
//
//        zhengCeFaGuiActivity.setTiaoZhuanFragment(this);
//        zhengCeFaGuiActivity.setFuWuTiaoZhuFragment(this);
//
//        zhiNengZhiZaoActivity.setTiaoZhuanFragment(this);
//        zhiNengZhiZaoActivity.setFuWuTiaoZhuFragment(this);
//
//        yuanQuShengHuoActivity.setTiaoZhuanFragment(this);
//        yuanQuShengHuoActivity.setFuWuTiaoZhuFragment(this);



    }







    //声明一个long类型变量：用于存放上一点击“返回键”的时刻
    private long mExitTime;
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        //判断用户是否点击了“返回键”
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            //与上次点击返回键时刻作差
            if ((System.currentTimeMillis() - mExitTime) > 2000) {
                //大于2000ms则认为是误操作，使用Toast进行提示
                Toast.makeText(this, "再按一次退出程序", Toast.LENGTH_SHORT).show();
                //并记录下本次点击“返回键”的时刻，以便下次进行判断
                mExitTime = System.currentTimeMillis();
            } else {
                //小于2000ms则认为是用户确实希望退出程序-调用System.exit()方法进行退出
                System.exit(0);
            }
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }









    public  void changeTextSize(Activity activity, float multiple) {
        Configuration configuration = getResources().getConfiguration();
        configuration.fontScale = multiple;    //1为标准字体，multiple为放大的倍数
        DisplayMetrics displayMetrics = new DisplayMetrics();
        activity.getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        displayMetrics.scaledDensity = configuration.fontScale * displayMetrics.density;
        getBaseContext().getResources().updateConfiguration(configuration, displayMetrics);
    }




    @Override
    protected void onSaveInstanceState(Bundle outState) {
        //super.onSaveInstanceState(outState);
    }



//
//    public boolean onKeyDown(int keyCode, KeyEvent event) {
//        if (keyCode == KeyEvent.KEYCODE_BACK) {
//            return true;
//        }
//        return false;
//    }


//
//    public boolean onKeyDown(int keyCode, KeyEvent event) {
//                         if (keyCode == KeyEvent.KEYCODE_BACK) {
//                                     if ((System.currentTimeMillis() - exitTime) > 2000) {
//                                                 Object mHelperUtils;
//                                                Toast.makeText(this, "再按一次退出程序", Toast.LENGTH_SHORT).show();
//                                         exitTime = System.currentTimeMillis();
//
//                                         } else {
//                                                finish();
//                                         }
//                                    return true;
//                            }
//                        return super.onKeyDown(keyCode, event);
//                 }





    private void initDefaultFragment() {
        fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.add(R.id.container_fragment, shouYeFragment);
        fragmentTransaction.commit();
        hideFragment = shouYeFragment;
    }

    private void replaceFragment(Fragment showFragment, FragmentTransaction fragmentTransaction) {
        if (showFragment.isAdded()) {
            fragmentTransaction.hide(hideFragment).show(showFragment).commit();
        } else {
            fragmentTransaction.hide(hideFragment).add(R.id.container_fragment, showFragment).commit();
        }
        hideFragment = showFragment;
    }





    private void replaceFragmentluntan(Fragment showFragment) {
        fragmentTransaction = fragmentManager.beginTransaction();
        if (showFragment.isAdded()) {
            fragmentTransaction.hide(hideFragment).show(showFragment).commit();
        } else {
            fragmentTransaction.hide(hideFragment).add(R.id.container_fragment, showFragment).commit();
        }
        hideFragment = showFragment;
    }






    private void replaceFragmentfuwu(Fragment showFragment) {
        fragmentTransaction = fragmentManager.beginTransaction();
        if (showFragment.isAdded()) {
            fragmentTransaction.hide(hideFragment).show(showFragment).commit();
        } else {
            fragmentTransaction.hide(hideFragment).add(R.id.container_fragment, showFragment).commit();
        }
        hideFragment = showFragment;
    }




    //    初始化
    private void initView() {
        shouYeFragment = new ShouYeFragment();
        fuWuFragment = new FuWuFragment();
        lunTanFragment = new LunTanFragment();
//        lunTanTwoFragment = new LunTanTwoFragment();
//        myFragment = new MyFragment();
        mineFragment = new MineFragment();

//
//        yuanQuZhaoShangActivity = new YuanQuZhaoShangActivity();
//        zhengCeFaGuiActivity = new ZhengCeFaGuiActivity();
//        zhiNengZhiZaoActivity = new ZhiNengZhiZaoActivity();
//        yuanQuShengHuoActivity = new YuanQuShengHuoActivity();

        homeRadioGroup.setOnCheckedChangeListener(this);
    }


    @Override
    public void onCheckedChanged(RadioGroup radioGroup, @IdRes int checkedId) {
        fragmentTransaction = fragmentManager.beginTransaction();
        switch (checkedId) {
            case R.id.tv_shouye:
                replaceFragment(shouYeFragment, fragmentTransaction);
                break;
            case R.id.tv_fuwu:
                replaceFragment(fuWuFragment, fragmentTransaction);
                break;
            case R.id.tv_luntan:
                replaceFragment(lunTanFragment, fragmentTransaction);
//                replaceFragment(lunTanTwoFragment, fragmentTransaction);
                break;
            case R.id.tv_my:
//                replaceFragment(myFragment, fragmentTransaction);
                replaceFragment(mineFragment, fragmentTransaction);
                break;
        }
    }








    @Override
    public void onClick() {
        tvLuntan.setChecked(true);
    }

    @Override
    public void onClick1() {
        tvFuwu.setChecked(true);
    }

    @Override
    public void onClickzixun() {
        tvFuwu.setChecked(true);
    }

    @Override
    public void onClickfuwubiaoti() {
        tvFuwu.setChecked(true);
    }
}
