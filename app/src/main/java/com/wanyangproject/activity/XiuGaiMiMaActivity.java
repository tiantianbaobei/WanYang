package com.wanyangproject.activity;

import android.app.ProgressDialog;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.wanyangproject.R;
import com.wanyangproject.entity.XiuGaiMiMaEntity;
import com.wanyangproject.utils.ContractUtils;
import com.zhy.http.okhttp.OkHttpUtils;
import com.zhy.http.okhttp.callback.StringCallback;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.Call;

public class XiuGaiMiMaActivity extends AppCompatActivity {

    @BindView(R.id.image_back)
    ImageView imageBack;
    @BindView(R.id.et_yuanmima)
    EditText etYuanmima;
    @BindView(R.id.et_xinmima)
    EditText etXinmima;
    @BindView(R.id.et_queren_xinmima)
    EditText etQuerenXinmima;
    @BindView(R.id.btn_login)
    Button btnLogin;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_xiu_gai_mi_ma);
        ButterKnife.bind(this);
        if (Build.VERSION.SDK_INT >= 21) {
            View dview = getWindow().getDecorView();
            dview.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN | View.SYSTEM_UI_FLAG_LAYOUT_STABLE);
            getWindow().setStatusBarColor(Color.TRANSPARENT);
        }

    }

    @OnClick({R.id.image_back, R.id.et_yuanmima, R.id.et_xinmima, R.id.et_queren_xinmima, R.id.btn_login})
    public void onViewClicked(View view) {
        switch (view.getId()) {
//            返回按钮
            case R.id.image_back:
                finish();
                break;
//            原密码
            case R.id.et_yuanmima:
                break;
//            新密码
            case R.id.et_xinmima:
                break;
//            确认新密码
            case R.id.et_queren_xinmima:

                break;
//            确认按钮
            case R.id.btn_login:
//                修改密码的网络请求
                initXiuGaiMiMaHttp();
                break;
        }
    }

//    修改密码的网络请求
    private void initXiuGaiMiMaHttp() {


        if(!(etXinmima.getText().toString().trim().length() >= 6 && etXinmima.getText().toString().trim().length() <= 20)){
            Toast.makeText(this, "请输入6-20位的密码", Toast.LENGTH_SHORT).show();
            return;
        }
        if( !etQuerenXinmima.getText().toString().trim().equals(etXinmima.getText().toString().trim()) ){
            Toast.makeText(this, "两次密码输入不一样", Toast.LENGTH_SHORT).show();
            return;
        }
        System.out.println(etYuanmima.getText().toString().trim() +"        原始密码");
        System.out.println(etXinmima.getText().toString().trim()+"          新密码");


        final ProgressDialog progressDialog = new ProgressDialog(XiuGaiMiMaActivity.this);
        progressDialog.setTitle("提示");
        progressDialog.setMessage("请等待...");
        progressDialog.setCancelable(false);
        progressDialog.show();
        OkHttpUtils.post()
                .url(ContractUtils.LOGIN_URL+"user/editPwd")
                .addHeader("token",ContractUtils.getTOKEN(XiuGaiMiMaActivity.this))
                .addParams("oldPwd",etYuanmima.getText().toString().trim())
                .addParams("pwd",etXinmima.getText().toString().trim())
                .build()
                .execute(new StringCallback() {
                    @Override
                    public void onError(Call call, Exception e, int id) {
                        System.out.println(e+"        修啊改密码eeee");
                        progressDialog.dismiss();
                    }

                    @Override
                    public void onResponse(String response, int id) {
                        ContractUtils.Code500(XiuGaiMiMaActivity.this,response);
                        System.out.println(response+"       修改密码的网络请求");
                        progressDialog.dismiss();

                        if(response.indexOf("200") != -1){
                            Gson gson = new Gson();
                            XiuGaiMiMaEntity xiuGaiMiMaEntity = gson.fromJson(response, XiuGaiMiMaEntity.class);
                            Toast.makeText(XiuGaiMiMaActivity.this, xiuGaiMiMaEntity.getResponse().getMes(), Toast.LENGTH_SHORT).show();
                            finish();
                        }else{
                            ContractUtils.Code400(XiuGaiMiMaActivity.this,response);
                        }
                    }
                });
    }
}
