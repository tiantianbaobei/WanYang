package com.wanyangproject.activity;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Gravity;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bigkoo.pickerview.OptionsPickerView;
import com.google.gson.Gson;
import com.lzy.imagepicker.ImagePicker;
import com.lzy.imagepicker.bean.ImageItem;
import com.lzy.imagepicker.ui.ImageGridActivity;
import com.wanyangproject.R;
import com.wanyangproject.entity.FaTieEntity;
import com.wanyangproject.entity.FaTieShiBaiEntity;
import com.wanyangproject.entity.HuoQuYuanQuLieBiaoEntity;
import com.wanyangproject.entity.LunTanEntity;
import com.wanyangproject.entity.PhotoEntity;
import com.wanyangproject.popuwindow.TakePhotoPopWin;
import com.wanyangproject.popuwindow.XuanZeFeiLeiPopupWindow;
import com.wanyangproject.popuwindow.XuanZeYuanQuPopupWindow;
import com.wanyangproject.utils.ContractUtils;
import com.wanyangproject.utils.GlideImageLoaderForPicker;
import com.zhy.http.okhttp.OkHttpUtils;
import com.zhy.http.okhttp.callback.StringCallback;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import top.zibin.luban.Luban;
import top.zibin.luban.OnCompressListener;


public class FaTieActivity extends AppCompatActivity {

    @BindView(R.id.image_back)
    ImageView imageBack;
    @BindView(R.id.tv_fatie)
    TextView tvFatie;
    @BindView(R.id.tv_xuanzeyuanqu)
    TextView tvXuanzeyuanqu;
    @BindView(R.id.tv_xuazefenlei)
    TextView tvXuazefenlei;
    @BindView(R.id.tv_yuanqu_xuanze)
    TextView tvYuanquXuanze;
    @BindView(R.id.tv_fenlei_xuanze)
    TextView tvFenleiXuanze;
    @BindView(R.id.et_neirong)
    EditText etNeirong;
    @BindView(R.id.image_add_one)
    ImageView imageAddOne;
    @BindView(R.id.image_add_two)
    ImageView imageAddTwo;
    @BindView(R.id.image_add_three)
    ImageView imageAddThree;
    @BindView(R.id.image_delete_one)
    ImageView imageDeleteOne;
    @BindView(R.id.image_delete_two)
    ImageView imageDeleteTwo;
    @BindView(R.id.image_delete_three)
    ImageView imageDeleteThree;
    @BindView(R.id.et_title)
    EditText etTitle;
    private TakePhotoPopWin takePhotoPopWin;
    private XuanZeYuanQuPopupWindow xuanZeYuanQuPopupWindow;
    private HuoQuYuanQuLieBiaoEntity huoQuYuanQuLieBiaoEntity;
    private XuanZeFeiLeiPopupWindow xuanZeFeiLeiPopupWindow;
    private LunTanEntity lunTanEntity;
    private String yqid;
    private String flid;
    private PhotoEntity photoEntity;
    //    private ArrayList<ImageItem> list = new ArrayList<>();// 图片
    private ArrayList<String> list1 = new ArrayList<>();// 图片
    private int ISOK = 0;
    private ArrayList<ImageView> add = new ArrayList(); //  带加号的原图
    private ArrayList<String> imageUrl = new ArrayList<>(); // 图片网址
    private ArrayList<ImageView> shanchu = new ArrayList<>();// 删除的按钮
    private ArrayList<String> list11 = new ArrayList<>();//选择园区id
    private ArrayList<String> list22 = new ArrayList<>();//选择园区title
    private ArrayList<String> listfnlei11 = new ArrayList<>();//选择分类id
    private ArrayList<String> listfenlei22 = new ArrayList<>();//选择分类title
    private Uri photoUri;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fa_tie);
        ButterKnife.bind(this);
        if (Build.VERSION.SDK_INT >= 21) {
            View dview = getWindow().getDecorView();
            dview.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN | View.SYSTEM_UI_FLAG_LAYOUT_STABLE);
            getWindow().setStatusBarColor(Color.TRANSPARENT);
        }

        //                园区列表的网络请求
        initYuanQuLieBiaoHttp();
        //        论坛标题的网络请求
        initLunTanTitleHttp();

        add.add(imageAddOne);
        add.add(imageAddTwo);
        add.add(imageAddThree);

        shanchu.add(imageDeleteOne);
        shanchu.add(imageDeleteTwo);
        shanchu.add(imageDeleteThree);




//        etTitle.addTextChangedListener(new TextWatcher() {
//            @Override  //   输入文字之前的状态
//            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
//
//            }
//
//            @Override // 输入文字中的状态
//            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
//                if(etTitle.getText().toString().trim().length() > 20){
//                    Toast.makeText(FaTieActivity.this, "最多只能输入20个文字！", Toast.LENGTH_SHORT).show();
//                }
//            }
//
//            @Override  // 输入文字之后的状态
//            public void afterTextChanged(Editable editable) {
//
//            }
//        });
//



    }

    //    获取园区列表的网络请求
    private void initYuanQuLieBiaoHttp() {
        final ProgressDialog progressDialog = new ProgressDialog(FaTieActivity.this);
        progressDialog.setTitle("提示");
        progressDialog.setMessage("请等待...");
        progressDialog.setCancelable(false);
        progressDialog.show();
        OkHttpUtils.post()
                .url(ContractUtils.LOGIN_URL + "park/getPark")
                .build()
                .execute(new StringCallback() {
                    @Override
                    public void onError(Call call, Exception e, int id) {
                        progressDialog.dismiss();
                    }

                    @Override
                    public void onResponse(String response, int id) {
                        progressDialog.dismiss();
                        System.out.println(response + "     获取园区列表的网络请求");
                        ContractUtils.Code500(FaTieActivity.this, response);
                        Gson gson = new Gson();
                        huoQuYuanQuLieBiaoEntity = gson.fromJson(response, HuoQuYuanQuLieBiaoEntity.class);
                        if (response.indexOf("200") != -1) {
                            for (int i = 0; i < huoQuYuanQuLieBiaoEntity.getResponse().size(); i++) {
                                list11.add(huoQuYuanQuLieBiaoEntity.getResponse().get(i).getParkId());
                                list22.add(huoQuYuanQuLieBiaoEntity.getResponse().get(i).getParkName());
                            }

//                            View.OnClickListener itemsOnClick = null;
//                            HuoQuYuanQuLieBiaoEntity.mmm = (huoQuYuanQuLieBiaoEntity.getMsg());
//                            xuanZeYuanQuPopupWindow = new XuanZeYuanQuPopupWindow(FaTieActivity.this, itemsOnClick);
//                            System.out.println(huoQuYuanQuLieBiaoEntity.getMsg().size() + "     sizezzzzzzzzz");
                        }
                    }
                });
    }


    //    论坛标题的网络请求
    private void initLunTanTitleHttp() {
        final ProgressDialog progressDialog = new ProgressDialog(FaTieActivity.this);
        progressDialog.setTitle("提示");
        progressDialog.setMessage("请等待...");
        progressDialog.setCancelable(false);
        progressDialog.show();
        OkHttpUtils.post()
                .url(ContractUtils.LOGIN_URL + "forum/typeInfo")
                .addHeader("token", ContractUtils.getTOKEN(this))
                .build()
                .execute(new StringCallback() {
                    @Override
                    public void onError(Call call, Exception e, int id) {
                        progressDialog.dismiss();

                    }

                    @Override
                    public void onResponse(String response, int id) {
                        progressDialog.dismiss();
                        ContractUtils.Code500(FaTieActivity.this, response);
                        System.out.println(response + "   论坛标题的网络请求");
                        Gson gson = new Gson();
                        lunTanEntity = gson.fromJson(response, LunTanEntity.class);
                        if (response.indexOf("200") != -1) {

//                            Object itemsOnClick = null;
//                            LunTanEntity.fenlei = lunTanEntity.getResponse();
//                            xuanZeFeiLeiPopupWindow = new XuanZeFeiLeiPopupWindow(FaTieActivity.this, (View.OnClickListener) itemsOnClick);
                            for (int i = 0; i < lunTanEntity.getResponse().size(); i++) {
                                listfnlei11.add(lunTanEntity.getResponse().get(i).getId());
                                listfenlei22.add(lunTanEntity.getResponse().get(i).getTitle());
                            }
                        }
                    }
                });
    }


    @OnClick({R.id.image_delete_one, R.id.et_title,R.id.image_delete_two, R.id.image_delete_three, R.id.image_back, R.id.tv_fatie, R.id.tv_yuanqu_xuanze, R.id.tv_fenlei_xuanze, R.id.image_add_one, R.id.image_add_two, R.id.image_add_three})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.image_back:
                finish();
                break;
            case R.id.et_title:

                break;
            case R.id.image_add_one:
                ISOK = 0;
                //                弹出上传图片的弹窗
                initTanChu();
                break;
            case R.id.image_add_two:
                ISOK = 1;
                //                弹出上传图片的弹窗
                initTanChu();
                break;
            case R.id.image_add_three:
                ISOK = 2;
//                弹出上传图片的弹窗
                initTanChu();
                break;
            case R.id.tv_fatie:
                if(etTitle.getText().toString().equals("")){
                    Toast.makeText(this, "请输入标题", Toast.LENGTH_SHORT).show();
                } else if (etNeirong.getText().toString().equals("")) {
                    Toast.makeText(this, "请输入内容", Toast.LENGTH_SHORT).show();
                } else if (tvXuanzeyuanqu.getText().toString().equals("")) {
                    Toast.makeText(this, "请选择园区", Toast.LENGTH_SHORT).show();
                } else if (tvFenleiXuanze.getText().toString().equals("")) {
                    Toast.makeText(this, "请选择分类", Toast.LENGTH_SHORT).show();
                } else {
                    if (list1.size() == 0) {
//                        没有图片的
                        initFaTieNoImageHttp();
                    }
                    for (int i = 0; i < list1.size(); i++) {
                        uploadFile(list1.get(i));
                    }
                }
                break;
            case R.id.image_delete_one:
                list1.remove(0);
                TuPian();
                break;
            case R.id.image_delete_two:
                list1.remove(1);
                TuPian();
                break;
            case R.id.image_delete_three:
                list1.remove(2);
                TuPian();
                break;
//            园区选择
            case R.id.tv_yuanqu_xuanze:
                OptionsPickerView pvOptions = new OptionsPickerView.Builder(this, new OptionsPickerView.OnOptionsSelectListener() {
                    @Override
                    public void onOptionsSelect(int options1, int option2, int options3, View v) {
                        //返回的分别是三个级别的选中位置
                        String tx = list22.get(options1);
                        yqid = list11.get(options1);
                        tvYuanquXuanze.setText(tx);
                    }
                })
                        .setSubmitText("确定")//确定按钮文字
                        .setCancelText("取消")//取消按钮文字
                        .setTitleText("选择园区")//标题
                        .setSubCalSize(16)//确定和取消文字大小
                        .setTitleSize(16)//标题文字大小
                        .setTitleColor(Color.BLACK)//标题文字颜色
                        .setSubmitColor(Color.RED)//确定按钮文字颜色
                        .setCancelColor(Color.parseColor("#AEAAAA"))//取消按钮文字颜色
                        .setTitleBgColor(Color.parseColor("#f5f5f5"))//标题背景颜色 Night mode
                        .setBgColor(Color.parseColor("#ffffff"))//滚轮背景颜色 Night mode
                        .setContentTextSize(16)//滚轮文字大小
                        .setLinkage(false)//设置是否联动，默认true
                        .setLabels("", "", "")//设置选择的三级单位
                        .isCenterLabel(false) //是否只显示中间选中项的label文字，false则每项item全部都带有label。
                        .setCyclic(false, false, false)//循环与否
                        .setSelectOptions(1, 1, 1)  //设置默认选中项
                        .setOutSideCancelable(false)//点击外部dismiss default true
                        .isDialog(false)//是否显示为对话框样式
                        .build();

                pvOptions.setPicker(list22);//添加数据源
                pvOptions.show();


//                System.out.println(xuanZeYuanQuPopupWindow + "    xuanZeYuanQuPopupWindow");
//                xuanZeYuanQuPopupWindow.showAtLocation(findViewById(R.id.relative_fatie), Gravity.BOTTOM | Gravity.CENTER_HORIZONTAL, 0, 0);
//                xuanZeYuanQuPopupWindow.setXuanZeYuanQuClick(new XuanZeYuanQuPopupWindow.XuanZeYuanQuClick() {
//                    @Override
//                    public void xuanzeyuanquClick(String title, String yuanid) {
//                        tvYuanquXuanze.setText(title);
//                        yqid = yuanid;
//                    }
//                });
                break;
//            分类选择
            case R.id.tv_fenlei_xuanze:
                OptionsPickerView pvOptions1 = new OptionsPickerView.Builder(this, new OptionsPickerView.OnOptionsSelectListener() {
                    @Override
                    public void onOptionsSelect(int options1, int option2, int options3, View v) {
                        //返回的分别是三个级别的选中位置
                        String tx = listfenlei22.get(options1);
                        flid = listfnlei11.get(options1);
                        tvFenleiXuanze.setText(tx);
                    }
                })
                        .setSubmitText("确定")//确定按钮文字
                        .setCancelText("取消")//取消按钮文字
                        .setTitleText("选择分类")//标题
                        .setSubCalSize(16)//确定和取消文字大小
                        .setTitleSize(16)//标题文字大小
                        .setTitleColor(Color.BLACK)//标题文字颜色
                        .setSubmitColor(Color.RED)//确定按钮文字颜色
                        .setCancelColor(Color.parseColor("#AEAAAA"))//取消按钮文字颜色
                        .setTitleBgColor(Color.parseColor("#f5f5f5"))//标题背景颜色 Night mode
                        .setBgColor(Color.parseColor("#ffffff"))//滚轮背景颜色 Night mode
                        .setContentTextSize(16)//滚轮文字大小
                        .setLinkage(false)//设置是否联动，默认true
                        .setLabels("", "", "")//设置选择的三级单位
                        .isCenterLabel(false) //是否只显示中间选中项的label文字，false则每项item全部都带有label。
                        .setCyclic(false, false, false)//循环与否
                        .setSelectOptions(1, 1, 1)  //设置默认选中项
                        .setOutSideCancelable(false)//点击外部dismiss default true
                        .isDialog(false)//是否显示为对话框样式
                        .build();

                pvOptions1.setPicker(listfenlei22);//添加数据源
                pvOptions1.show();


//                xuanZeFeiLeiPopupWindow.showAtLocation(findViewById(R.id.relative_fatie), Gravity.BOTTOM | Gravity.CENTER_HORIZONTAL, 0, 0);
//
//                xuanZeFeiLeiPopupWindow.setXuanZeFenLeiClick(new XuanZeFeiLeiPopupWindow.XuanZeFenLeiClick() {
//                    @Override
//                    public void xuanzefenleiClick(String title, String fenleiid) {
//                        tvFenleiXuanze.setText(title);
//                        flid = fenleiid;
//                    }
//                });
                break;
        }
    }

    private void initFaTieNoImageHttp() {
        System.out.println("没有图片11111111111111");

        if (yqid == null || flid == null) {
            Toast.makeText(this, "请选择园区和分类", Toast.LENGTH_SHORT).show();
            return;
        }
        System.out.println(yqid + "   yuanquid");
        System.out.println(flid + "    fenleiid");

        OkHttpUtils.post()
                .url(ContractUtils.LOGIN_URL + "forum/publish")
                .addHeader("token", ContractUtils.getTOKEN(this))
                .addParams("parkId", yqid)// yqid不能修改
                .addParams("typeId", flid)
                .addParams("content", etNeirong.getText().toString().trim())
                .addParams("title",etTitle.getText().toString().trim())
                .build()
                .execute(new StringCallback() {
                    @Override
                    public void onError(Call call, Exception e, int id) {
                    }

                    @Override
                    public void onResponse(String response, int id) {
                        ContractUtils.Code500(FaTieActivity.this, response);
                        System.out.println(response + "     发表帖子的网络请求");
                        if (response.indexOf("200") != -1) {
                            Gson gson = new Gson();
                            FaTieEntity faTieEntity = gson.fromJson(response, FaTieEntity.class);
                            finish();
                            Toast.makeText(FaTieActivity.this, faTieEntity.getMsg(), Toast.LENGTH_SHORT).show();
                        } else if (response.indexOf("400") != -1) {
                            Gson gson = new Gson();
                            FaTieShiBaiEntity faTieShiBaiEntity = gson.fromJson(response, FaTieShiBaiEntity.class);
                            Toast.makeText(FaTieActivity.this, faTieShiBaiEntity.getMsg(), Toast.LENGTH_SHORT).show();
                        }
                    }
                });

    }

    //    弹出上传图片的弹窗
    private void initTanChu() {
        Object itemsOnClick = null;
        takePhotoPopWin = new TakePhotoPopWin(FaTieActivity.this, (View.OnClickListener) itemsOnClick);
        takePhotoPopWin.showAtLocation(findViewById(R.id.relative_fatie), Gravity.BOTTOM | Gravity.CENTER_HORIZONTAL, 0, 0);


//        点击相机

        takePhotoPopWin.setOnClickXiangji(new TakePhotoPopWin.OnClickXiangji() {
            @Override
            public void onClick(int id) {
//                        Intent open = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
//                        startActivityForResult(open,100);


                //                判断当前SDK版本号
                if (Build.VERSION.SDK_INT >= 23) {
//                    当前拨打电话权限是没有给的
                    if (ContextCompat.checkSelfPermission(FaTieActivity.this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
//                        我要请求权限
                        ActivityCompat.requestPermissions(FaTieActivity.this, new String[]{Manifest.permission.CAMERA}, 1);
                    } else {
//                                SimpleDateFormat timeStampFormat = new SimpleDateFormat("yyyyMMddHHmmss");
//                                String filename = timeStampFormat.format(new Date());
//                                ContentValues values = new ContentValues(); //使用本地相册保存拍摄照片
//                                values.put(MediaStore.Images.Media.TITLE, filename);
//                                photoUri = getContentResolver().insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);
//
//                                // 设置 intent.putExtra(MediaStore.EXTRA_OUTPUT, photoUri); photoUri = 保存图片得uri
//                                Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
//                                intent.putExtra(MediaStore.EXTRA_OUTPUT, photoUri);
//                                startActivityForResult(intent, 100);

                        if (Environment.MEDIA_MOUNTED.equals(Environment.getExternalStorageState())) {
                            //创建文件夹 在本地文件Pictures 目录下创建 自己得文件夹
                            File dir = new File(Environment.getExternalStorageDirectory() + "/Pictures/image");
                            if (!dir.exists()) {
                                dir.mkdirs();
                            }
                            //照片名称
                            File file = new File(dir, new SimpleDateFormat("yyyyMMddHHmmss").format(new Date()) + ".jpg");

                            photoUri = Uri.parse(file.getPath());

                            startActivityForResult(new Intent(MediaStore.ACTION_IMAGE_CAPTURE)
                                    .putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(file)), 100);

                        } else {
//                                    showToast("请检查SDCard！");
                        }
                    }
                } else {
//                            SimpleDateFormat timeStampFormat = new SimpleDateFormat("yyyyMMddHHmmss");
//                            String filename = timeStampFormat.format(new Date());
//                            ContentValues values = new ContentValues(); //使用本地相册保存拍摄照片
//                            values.put(MediaStore.Images.Media.TITLE, filename);
//                            photoUri = getContentResolver().insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);
//
//                            // 设置 intent.putExtra(MediaStore.EXTRA_OUTPUT, photoUri); photoUri = 保存图片得uri
//                            Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
//                            intent.putExtra(MediaStore.EXTRA_OUTPUT, photoUri);
//                            startActivityForResult(intent, 100);


                    if (Environment.MEDIA_MOUNTED.equals(Environment.getExternalStorageState())) {
                        //创建文件夹 在本地文件Pictures 目录下创建 自己得文件夹
                        File dir = new File(Environment.getExternalStorageDirectory() + "/Pictures/image");
                        if (!dir.exists()) {
                            dir.mkdirs();
                        }
                        //照片名称
                        File file = new File(dir, new SimpleDateFormat("yyyyMMddHHmmss").format(new Date()) + ".jpg");
                        photoUri = Uri.parse(file.getPath());

                        startActivityForResult(new Intent(MediaStore.ACTION_IMAGE_CAPTURE)
                                .putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(file)), 100);

                    } else {
//                                    showToast("请检查SDCard！");
                    }
                }


//
//                        SimpleDateFormat timeStampFormat = new SimpleDateFormat("yyyyMMddHHmmss");
//                        String filename = timeStampFormat.format(new Date());
//                        ContentValues values = new ContentValues(); //使用本地相册保存拍摄照片
//                        values.put(MediaStore.Images.Media.TITLE, filename);
//                        photoUri = getContentResolver().insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);
//
//                        // 设置 intent.putExtra(MediaStore.EXTRA_OUTPUT, photoUri); photoUri = 保存图片得uri
//                        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
//                        intent.putExtra(MediaStore.EXTRA_OUTPUT, photoUri);
//                        startActivityForResult(intent, 100);


//                        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
//                        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {//判断是否有相机应用
//                            startActivityForResult(takePictureIntent, REQ_THUMB);
//
//                        }
            }
        });


        //                点击相册
        takePhotoPopWin.setOnClickXiangce(new TakePhotoPopWin.OnClickXiangce() {
            @Override
            public void onClick(int id) {
                ImagePicker imagePicker = ImagePicker.getInstance();
                imagePicker.setImageLoader(new GlideImageLoaderForPicker());   //设置图片加载器
                imagePicker.setShowCamera(true);  //显示拍照按钮
                imagePicker.setCrop(false);        //允许裁剪（单选才有效）
                imagePicker.setSaveRectangle(true);//是否按矩形区域保存
                imagePicker.setFocusHeight(169);
                imagePicker.setFocusWidth(169);
                imagePicker.setCrop(false);
                imagePicker.setSelectLimit(1);    //选中数量限制
                imagePicker.setOutPutX(800);//保存文件的宽度。单位像素
                imagePicker.setOutPutY(800);//保存文件的高度。单位像素
                Intent intent = new Intent(FaTieActivity.this, ImageGridActivity.class);
                startActivityForResult(intent, 1);

            }
        });
    }

    //    发帖的网络请求
    private void initFaTieHttp() {


        System.out.println("有图片的2222222222222");


        String url = "";
        for (int i = 0; i < imageUrl.size(); i++) {
            url = url + imageUrl.get(i) + ",";
        }
        url = url.substring(0, url.length() - 1);
        System.out.println(url + "  urlurlurlurlurl ");

        if (yqid == null || flid == null) {
            Toast.makeText(this, "请选择园区和分类", Toast.LENGTH_SHORT).show();
            return;
        }
        System.out.println(yqid + "   yuanquid");
        System.out.println(flid + "    fenleiid");

        OkHttpUtils.post()
                .url(ContractUtils.LOGIN_URL + "forum/publish")
                .addHeader("token", ContractUtils.getTOKEN(FaTieActivity.this))
                .addParams("parkId", yqid)//  不能修改
                .addParams("typeId", flid)
                .addParams("content", etNeirong.getText().toString().trim())
                .addParams("imgArr", url)
                .addParams("title",etTitle.getText().toString().trim())
                .build()
                .execute(new StringCallback() {
                    @Override
                    public void onError(Call call, Exception e, int id) {
                        System.out.println(e + "    eeeeeeeeeee");
                    }

                    @Override
                    public void onResponse(String response, int id) {
                        ContractUtils.Code500(FaTieActivity.this, response);
                        System.out.println(response + "     发表帖子的网络请求");
                        if (response.indexOf("200") != -1) {
                            Gson gson = new Gson();
                            FaTieEntity faTieEntity = gson.fromJson(response, FaTieEntity.class);
                            finish();
                            Toast.makeText(FaTieActivity.this, faTieEntity.getMsg(), Toast.LENGTH_SHORT).show();
                        } else if (response.indexOf("400") != -1) {
                            Gson gson = new Gson();
                            FaTieShiBaiEntity faTieShiBaiEntity = gson.fromJson(response, FaTieShiBaiEntity.class);
                            Toast.makeText(FaTieActivity.this, faTieShiBaiEntity.getMsg(), Toast.LENGTH_SHORT).show();
                        }
                    }
                });
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

//        相机回调
        if (requestCode == 100) {
            String string = photoUri.toString();
            File file = new File(string);
            System.out.println(file.exists() + "      file");
            if (file.exists() == false) {
                return;
            }


            System.out.println(string + "        string");
            if (list1.size() - 1 >= ISOK) {
                list1.set(ISOK, string);
            } else {
                list1.add(string);
            }
            TuPian();

        }
//        String string = photoUri.toString();
//        imageTouxiang.setImageURI(Uri.parse(string));
//        uploadFile1(string);


//        相册回调
        if (data != null && requestCode == 1) {
            ArrayList<ImageItem> images = (ArrayList<ImageItem>) data.getSerializableExtra(ImagePicker.EXTRA_RESULT_ITEMS);
            if (images != null) {
                if (list1.size() - 1 >= ISOK) {
                    list1.set(ISOK, images.get(0).path);
                } else {
                    list1.add(images.get(0).path);
                }
                TuPian();
            } else {
                Toast.makeText(this, "图片解析失败", Toast.LENGTH_SHORT).show();
            }


//            if (images != null) {
//                if (list.size() - 1 >= ISOK) {
//                    list.set(ISOK, images.get(0));
//                } else {
//                    list.add(images.get(0));
//                }
//                TuPian();
//            } else {
//                Toast.makeText(this, "图片解析失败", Toast.LENGTH_SHORT).show();
//            }
        }
//        else {
//            Toast.makeText(this, "没有数据", Toast.LENGTH_SHORT).show();
//        }
    }


    //    图片显示与隐藏
    private void TuPian() {
        for (int i = 0; i < add.size(); i++) {
            ImageView delete = shanchu.get(i);
            ImageView image = add.get(i);
            if (i <= list1.size() - 1) {
                image.setImageURI(Uri.parse(list1.get(i)));
            }
            if (i <= list1.size() - 1) {
                image.setVisibility(View.VISIBLE);
                delete.setVisibility(View.VISIBLE);
            } else if (i == list1.size()) {
                image.setVisibility(View.VISIBLE);
                delete.setVisibility(View.GONE);
                image.setImageResource(R.drawable.add);
            } else {
                image.setVisibility(View.GONE);
                delete.setVisibility(View.GONE);
            }
        }


//        for (int i = 0; i < add.size(); i++) {
//            ImageView delete = shanchu.get(i);
//            ImageView image = add.get(i);
//            if (i <= list.size() - 1) {
//                image.setImageURI(Uri.parse(list.get(i).path));
//            }
//            if (i <= list.size() - 1) {
//                image.setVisibility(View.VISIBLE);
//                delete.setVisibility(View.VISIBLE);
//            } else if (i == list.size()) {
//                image.setVisibility(View.VISIBLE);
//                delete.setVisibility(View.GONE);
//                image.setImageResource(R.drawable.add);
//            } else {
//                image.setVisibility(View.GONE);
//                delete.setVisibility(View.GONE);
//            }
//        }
    }


    private void uploadFile1(String imageItem) {
        final ProgressDialog progressDialog = new ProgressDialog(FaTieActivity.this);
        progressDialog.setTitle("提示");
        progressDialog.setMessage("请等待...");
        progressDialog.setCancelable(false);
        progressDialog.show();

        File file = new File(imageItem);
        Luban.with(this)
                .load(file)
                .ignoreBy(100)
                //  .setTargetDir(AppConfig.DEFAULT_SAVE_IMAGE_PATH)
                .setCompressListener(new OnCompressListener() {
                    @Override
                    public void onStart() {
                        // TODO 压缩开始前调用，可以在方法内启动 loading UI
                    }

                    @Override
                    public void onSuccess(File file) {
//                        String url = ContractUtils.LOGIN_URL+"ceshi/ce_upload.html";
                        OkHttpClient okHttpClient = new OkHttpClient();
//                        RequestBody requestBody1 =new FormBody.Builder()
//                                .add("driverid",substring)
//                                .add("driver_img_type","1")
//                                .build();
                        RequestBody requestBody = new MultipartBody.Builder()
                                .setType(MultipartBody.FORM)
                                .addFormDataPart("file", file.getName(), RequestBody.create(MediaType.parse("image/*"), file))
                                .build();

                        Request request = new Request.Builder()
                                .url(ContractUtils.LOGIN_URL + "index/upload")
                                .addHeader("token", ContractUtils.getTOKEN(FaTieActivity.this))
                                .post(requestBody)
                                .build();

                        okHttpClient.newCall(request).enqueue(new Callback() {
                            @Override
                            public void onFailure(Call call, IOException e) {
                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        progressDialog.dismiss();
//                                        Toast.makeText(FaTieActivity.this, "网络异常", Toast.LENGTH_SHORT).show();
                                    }
                                });
                                progressDialog.dismiss();
                            }

                            @Override
                            public void onResponse(Call call, final Response response) throws IOException {
                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        Toast.makeText(FaTieActivity.this, "上传成功", Toast.LENGTH_SHORT).show();
                                    }
                                });


                                progressDialog.dismiss();
                                String string = response.body().string();
                                Gson gson = new Gson();
                                photoEntity = gson.fromJson(string, PhotoEntity.class);

                                System.out.println(string + "     我的资料上传头像");
                            }
                        });
                    }

                    @Override
                    public void onError(Throwable e) {

                    }
                }).launch();
    }


    private void uploadFile(String imageItem) {
        final ProgressDialog progressDialog = new ProgressDialog(FaTieActivity.this);
        progressDialog.setTitle("提示");
        progressDialog.setMessage("请等待...");
        progressDialog.setCancelable(false);
        progressDialog.show();
        File file = new File(imageItem);
        Luban.with(this)
                .load(file)
                .ignoreBy(100)
                //  .setTargetDir(AppConfig.DEFAULT_SAVE_IMAGE_PATH)
                .setCompressListener(new OnCompressListener() {
                    @Override
                    public void onStart() {
                        // TODO 压缩开始前调用，可以在方法内启动 loading UI
                    }

                    @Override
                    public void onSuccess(File file) {
                        OkHttpClient okHttpClient = new OkHttpClient();
                        RequestBody requestBody = new MultipartBody.Builder()
                                .setType(MultipartBody.FORM)
                                .addFormDataPart("file", file.getName(), RequestBody.create(MediaType.parse("image/*"), file))
                                .build();
                        Request request = new Request.Builder()
                                .url(ContractUtils.LOGIN_URL + "index/upload")
                                .post(requestBody)
                                .addHeader("token", ContractUtils.getTOKEN(FaTieActivity.this))
                                .build();
                        okHttpClient.newCall(request).enqueue(new Callback() {
                            @Override
                            public void onFailure(Call call, IOException e) {
                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
//                                        progressDialog.dismiss();
//                                        Toast.makeText(FaTieActivity.this, "网络异常", Toast.LENGTH_SHORT).show();
                                    }
                                });
                                progressDialog.dismiss();
                            }

                            @Override
                            public void onResponse(Call call, final Response response) throws IOException {
                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        Toast.makeText(FaTieActivity.this, "上传成功", Toast.LENGTH_SHORT).show();
                                    }
                                });

                                progressDialog.dismiss();
                                String string = response.body().string();
                                Gson gson = new Gson();
                                photoEntity = gson.fromJson(string, PhotoEntity.class);
                                imageUrl.add(photoEntity.getResponse().getFileurl());
                                System.out.println(imageUrl + "              imageUrl");
                                if (imageUrl.size() == list1.size()) {
//                                    发帖的网络请求
                                    initFaTieHttp();
                                }
                                System.out.println(string + "   发帖上传图片");
                            }
                        });
                    }

                    @Override
                    public void onError(Throwable e) {

                    }
                }).launch();
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case 1:
                if (permissions.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
//                    SimpleDateFormat timeStampFormat = new SimpleDateFormat("yyyyMMddHHmmss");
//                    String filename = timeStampFormat.format(new Date());
//                    ContentValues values = new ContentValues(); //使用本地相册保存拍摄照片
//                    values.put(MediaStore.Images.Media.TITLE, filename);
//                    photoUri = getContentResolver().insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);
//
//                    // 设置 intent.putExtra(MediaStore.EXTRA_OUTPUT, photoUri); photoUri = 保存图片得uri
//                    Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
//                    intent.putExtra(MediaStore.EXTRA_OUTPUT, photoUri);
//                    startActivityForResult(intent, 100);

                    if (Environment.MEDIA_MOUNTED.equals(Environment.getExternalStorageState())) {
                        //创建文件夹 在本地文件Pictures 目录下创建 自己得文件夹
                        File dir = new File(Environment.getExternalStorageDirectory() + "/Pictures/image");
                        if (!dir.exists()) {
                            dir.mkdirs();
                        }
                        //照片名称
                        File file = new File(dir, new SimpleDateFormat("yyyyMMddHHmmss").format(new Date()) + ".jpg");
                        photoUri = Uri.parse(file.getPath());

                        startActivityForResult(new Intent(MediaStore.ACTION_IMAGE_CAPTURE)
                                .putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(file)), 100);

                    } else {
//                                    showToast("请检查SDCard！");
                    }
                } else {
//                    Toast.makeText(WoDeZiLiaoActivity.this, "", Toast.LENGTH_SHORT).show();
                }
                break;
        }
    }


}
