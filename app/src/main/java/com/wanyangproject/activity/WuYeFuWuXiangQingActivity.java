package com.wanyangproject.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebResourceResponse;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.wanyangproject.R;
import com.wanyangproject.entity.QiYeFuWuXiangQingEntity;
import com.wanyangproject.fuwuactivity.WuYeFuWuActivity;
import com.wanyangproject.net.util.ToastUtil;
import com.wanyangproject.utils.ContractUtils;
import com.wanyangproject.utils.WebViewUtils;
import com.zhy.http.okhttp.OkHttpUtils;
import com.zhy.http.okhttp.callback.StringCallback;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.text.SimpleDateFormat;
import java.util.Date;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.Call;

public class WuYeFuWuXiangQingActivity extends AppCompatActivity {


    @BindView(R.id.image_back)
    ImageView imageBack;
    @BindView(R.id.tv_title)
    TextView tvTitle;
    @BindView(R.id.tv_neirong)
    TextView tvNeirong;
    @BindView(R.id.tv_time)
    TextView tvTime;
    @BindView(R.id.webView)
    WebView webView;
    @BindView(R.id.tv_liji_shenqing)
    TextView tvLijiShenqing;
    @BindView(R.id.relative_shenqing)
    RelativeLayout relativeShenqing;
    @BindView(R.id.tv_fujian_lianjie)
    TextView tvFujianLianjie;
    @BindView(R.id.relative_xiazai)
    RelativeLayout relativeXiazai;
    private String id;
    private WebSettings mWebSettings;
    private String content;
    private QiYeFuWuXiangQingEntity qiYeFuWuXiangQingEntity;
    private String futitle;
    private ProgressDialog progressDialog;
    private static Boolean isOK = false;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wu_ye_fu_wu_xiang_qing);
        ButterKnife.bind(this);


        mWebSettings = webView.getSettings();
        mWebSettings.setJavaScriptEnabled(true);  //允许加载javascript
        webView.getSettings().setLayoutAlgorithm(WebSettings.LayoutAlgorithm.SINGLE_COLUMN);
        webView.getSettings().setLoadWithOverviewMode(true);
        mWebSettings.setSupportZoom(false);     //允许缩放
        mWebSettings.setBuiltInZoomControls(false); //原网页基础上缩放
        mWebSettings.setUseWideViewPort(false);   //任意比例缩放
//        mWebSettings.setUseWideViewPort(true); //将图片调整到适合webview的大小
//        mWebSettings.setLoadWithOverviewMode(true); // 缩放至屏幕的大小


        Intent intent = getIntent();
        id = intent.getStringExtra("id");
        futitle = intent.getStringExtra("futitle");
        System.out.println(id + "          接收物业服务id");


        //        物业服务详情的网络请求
        initQiYeFuWuHttp();

        initView();
    }


    private void initView() {
        webView.setWebViewClient(new WebViewClient() {
            /**
             * 拦截 url 跳转,在里边添加点击链接跳转或者操作
             */
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, WebResourceRequest request) {
                return WebViewUtils.shouldOverrideUrlLoading(view, request, WuYeFuWuXiangQingActivity.this);
            }


            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                System.out.println("66666666666666666666");
                return WebViewUtils.shouldOverrideUrlLoading(view, url, WuYeFuWuXiangQingActivity.this);

            }


            /**
             * 在开始加载网页时会回调
             */
            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                super.onPageStarted(view, url, favicon);
                System.out.println("111111111111111111");
            }

            /**
             * 加载错误的时候会回调，在其中可做错误处理，比如再请求加载一次，或者提示404的错误页面
             */
            @Override
            public void onReceivedError(WebView view, WebResourceRequest request, WebResourceError error) {
                super.onReceivedError(view, request, error);

            }


            /**
             * 在结束加载网页时会回调
             */
            @Override
            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(view, url);
                System.out.println("chenggongchenggongchenggong");

                if(progressDialog != null){
                    progressDialog.dismiss();
                }


            }

            /**
             * 当接收到https错误时，会回调此函数，在其中可以做错误处理
             */
            @Override
            public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
                super.onReceivedError(view, errorCode, description, failingUrl);
            }

            /**
             * 在每一次请求资源时，都会通过这个函数来回调
             */
            public WebResourceResponse shouldInterceptRequest(WebView view, String url) {
                return null;
            }

            @Override
            public void onPageCommitVisible(WebView view, String url) {
                super.onPageCommitVisible(view, url);

                if(progressDialog != null){
                    progressDialog.dismiss();
                }

            }
        });
    }


    //    物业服务详情的网络请求
    private void initQiYeFuWuHttp() {
        progressDialog = new ProgressDialog(WuYeFuWuXiangQingActivity.this);
        progressDialog.setTitle("提示");
        progressDialog.setMessage("请等待...");
        progressDialog.setCancelable(false);
        progressDialog.show();
        OkHttpUtils.post()
                .url(ContractUtils.LOGIN_URL + "fuwu/detail")
                .addHeader("token", ContractUtils.getTOKEN(WuYeFuWuXiangQingActivity.this))
                .addParams("id", id)
                .addParams("parkId", ContractUtils.getParkId(WuYeFuWuXiangQingActivity.this))
                .addParams("typeId", "1")//1:物业服务，2：企业服务
                .build()
                .execute(new StringCallback() {
                    @Override
                    public void onError(Call call, Exception e, int id) {
                        progressDialog.dismiss();
                    }

                    @Override
                    public void onResponse(String response, int id) {
                        System.out.println(response + "          物业服务详情的网络请求");
                        ContractUtils.Code500(WuYeFuWuXiangQingActivity.this, response);
                        if (response.indexOf("200") != -1) {
                            Gson gson = new Gson();
                            qiYeFuWuXiangQingEntity = gson.fromJson(response, QiYeFuWuXiangQingEntity.class);
//
//                            if(qiYeFuWuXiangQingEntity.getResponse().getFujian_name() != null){
//                                if(qiYeFuWuXiangQingEntity.getResponse().getFujian_name().equals("") ||qiYeFuWuXiangQingEntity.getResponse().getFujian().equals("null")){
//                                    tvFujianLianjie.setText("未命名");
//                                    relativeXiazai.setVisibility(View.GONE);
//                                }else{
//                                    relativeXiazai.setVisibility(View.VISIBLE);
//                                    tvFujianLianjie.setText(qiYeFuWuXiangQingEntity.getResponse().getFujian_name());
//                                }
//                            }



                            if(qiYeFuWuXiangQingEntity.getResponse().getFujian_name() != null){
                                if(qiYeFuWuXiangQingEntity.getResponse().getFujian_name().equals("")){
                                    tvFujianLianjie.setText("未命名");
                                }else{
                                    tvFujianLianjie.setText(qiYeFuWuXiangQingEntity.getResponse().getFujian_name());
                                }
                            }


                            if(qiYeFuWuXiangQingEntity.getResponse().getFujian().equals("") || qiYeFuWuXiangQingEntity.getResponse().getFujian().equals("null")){
                                relativeXiazai.setVisibility(View.GONE);
                            }else{
                                relativeXiazai.setVisibility(View.VISIBLE);
                            }




                            if (qiYeFuWuXiangQingEntity.getResponse().getTitle() == null) {

                            } else { // 标题
                                tvTitle.setText(qiYeFuWuXiangQingEntity.getResponse().getTitle());
                            }


                            if (qiYeFuWuXiangQingEntity.getResponse().getDesc().equals("")) {
                                tvNeirong.setText(futitle);
                            } else { // 内容
                                tvNeirong.setText(qiYeFuWuXiangQingEntity.getResponse().getDesc());
                            }


                            if (qiYeFuWuXiangQingEntity.getResponse().getAdd_time() == null) {

                            } else { // 时间
                                tvTime.setText(stampToDate(qiYeFuWuXiangQingEntity.getResponse().getAdd_time()));
                            }


                            if (qiYeFuWuXiangQingEntity.getResponse().getContent() == null) {

                            } else {
                                //    详情html
                                content = qiYeFuWuXiangQingEntity.getResponse().getContent();
                                webView.loadDataWithBaseURL("http://www.baidu.com", getNewContent(content), "text/html", "UTF-8", null);
                            }

                        }else if(response.indexOf("400") != -1){
                            progressDialog.dismiss();
                            ContractUtils.Code400(WuYeFuWuXiangQingActivity.this,response);
                        }
                    }
                });
    }


    /*
    * 将时间戳转换为时间
     */
    public String stampToDate(String s) {
        if(s == null){
            return "";
        }

        if(s.length() == 0){
            return "";
        }

        if(s.indexOf("-") != -1){
            return "";
        }
        String res;
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm");
        long lt = new Long(s);
        Date date = new Date(lt * 1000);
        res = simpleDateFormat.format(date);
        return res;
    }


    @OnClick({R.id.image_back, R.id.tv_fujian_lianjie, R.id.tv_title, R.id.tv_neirong, R.id.tv_time, R.id.webView, R.id.tv_liji_shenqing, R.id.relative_shenqing})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.image_back:
                finish();
                break;
//            附加连接
            case R.id.tv_fujian_lianjie:
                String fujian = qiYeFuWuXiangQingEntity.getResponse().getFujian();
                if (fujian.equals("null")) {
//                    ToastUtil.show(WuYeFuWuXiangQingActivity.this, "附件地址不存在");
                    return;
                } else {
                    EnclosureActivity.start(WuYeFuWuXiangQingActivity.this, fujian);
                }
                break;
            case R.id.tv_title:
                break;
            case R.id.tv_neirong:
                break;
            case R.id.tv_time:
                break;
            case R.id.webView:
                break;
            case R.id.tv_liji_shenqing:
//                typeId: 0 游客 1 普通员工 2企业用户 3商家 4 门卫 5 物业
                if (ContractUtils.getTypeId(WuYeFuWuXiangQingActivity.this).equals("2") || ContractUtils.getTypeId22(WuYeFuWuXiangQingActivity.this).equals("2") ||
                        ContractUtils.getTypeId(WuYeFuWuXiangQingActivity.this).equals("1") || ContractUtils.getTypeId22(WuYeFuWuXiangQingActivity.this).equals("1") ||
                        ContractUtils.getTypeId(WuYeFuWuXiangQingActivity.this).equals("4") || ContractUtils.getTypeId22(WuYeFuWuXiangQingActivity.this).equals("4")) {
                    Intent intent1 = new Intent(WuYeFuWuXiangQingActivity.this, WuYeFuWuActivity.class);
                    startActivity(intent1);
                } else {
                    Toast.makeText(this, "您不是企业、员工或门卫身份！", Toast.LENGTH_SHORT).show();
                }
                break;
            case R.id.relative_shenqing:
                //                typeId: 0 游客 1 普通员工 2企业用户 3商家 4 门卫 5 物业
                if (ContractUtils.getTypeId(WuYeFuWuXiangQingActivity.this).equals("2") || ContractUtils.getTypeId22(WuYeFuWuXiangQingActivity.this).equals("2") ||
                        ContractUtils.getTypeId(WuYeFuWuXiangQingActivity.this).equals("1") || ContractUtils.getTypeId22(WuYeFuWuXiangQingActivity.this).equals("1") ||
                        ContractUtils.getTypeId(WuYeFuWuXiangQingActivity.this).equals("4") || ContractUtils.getTypeId22(WuYeFuWuXiangQingActivity.this).equals("4")) {

                    Intent intent1 = new Intent(WuYeFuWuXiangQingActivity.this, WuYeFuWuActivity.class);
                    startActivity(intent1);
                } else {
                    Toast.makeText(this, "您不是企业、员工或门卫身份！", Toast.LENGTH_SHORT).show();
                }
                break;
        }
    }


    /**
     * 将html文本内容中包含img标签的图片，宽度变为屏幕宽度，高度根据宽度比例自适应
     **/
    public static String getNewContent(String htmltext) {
//        if(isOK == false){
//            return htmltext;
//        }
//
//        isOK = true;
        try {
            Document doc = Jsoup.parse(htmltext);
            Elements elements = doc.getElementsByTag("img");
            for (Element element : elements) {
                element.attr("width", "100%").attr("height", "auto");
            }

            return doc.toString();
        } catch (Exception e) {
            return htmltext;
        }
    }


}
