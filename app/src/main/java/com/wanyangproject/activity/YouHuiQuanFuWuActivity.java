package com.wanyangproject.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.wanyangproject.R;
import com.wanyangproject.adapter.QiYeFuWuYouHuiHuoDongAdapter;
import com.wanyangproject.adapter.YouHuiQuanAdapter;
import com.wanyangproject.entity.QiYeFuWuEntity;
import com.wanyangproject.entity.QiYeFuWuYouHuiHuoDongEntity;
import com.wanyangproject.entity.YouHuiQuanLieBiaoEntity;
import com.wanyangproject.utils.ContractUtils;
import com.zhy.http.okhttp.OkHttpUtils;
import com.zhy.http.okhttp.callback.StringCallback;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.Call;

public class YouHuiQuanFuWuActivity extends AppCompatActivity {


    @BindView(R.id.image_back)
    ImageView imageBack;
    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    @BindView(R.id.tv_title)
    TextView tvTitle;
    private QiYeFuWuYouHuiHuoDongAdapter qiYeFuWuYouHuiHuoDongAdapter;
    private QiYeFuWuYouHuiHuoDongEntity qiYeFuWuYouHuiHuoDongEntity;
    private QiYeFuWuEntity qiYeFuWuEntity;
    private YouHuiQuanAdapter youHuiQuanAdapter;
    private YouHuiQuanLieBiaoEntity youHuiQuanLieBiaoEntity;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_you_hui_quan_fu_wu);
        ButterKnife.bind(this);
////        企业服务列表的网络请求
//        initQiYeFuWuLieBiaoHttp();


//        企业服务的优惠获活动列表的网络请求
        initYouHuiHuoDongHttp();
    }


    //    企业服务的优惠获活动列表的网络请求
//    tiantian
    private void initYouHuiHuoDongHttp() {
        OkHttpUtils.post()
                .url(ContractUtils.LOGIN_URL + "Life/prefe_list")
                .addHeader("token", ContractUtils.getTOKEN(YouHuiQuanFuWuActivity.this))
                .addParams("parkId", ContractUtils.getParkId(YouHuiQuanFuWuActivity.this))
                .build()
                .execute(new StringCallback() {
                    @Override
                    public void onError(Call call, Exception e, int id) {
                        System.out.println(e + "     企业服务的优惠获活动列表的网络请求eeeeeeeeeee");
                    }

                    @Override
                    public void onResponse(String response, int id) {
                        ContractUtils.Code500(YouHuiQuanFuWuActivity.this, response);
                        System.out.println(response + "     企业服务的优惠获活动列表的网络请求");

                        if (response.indexOf("200") != -1) {
                            Gson gson = new Gson();
                            youHuiQuanLieBiaoEntity = gson.fromJson(response, YouHuiQuanLieBiaoEntity.class);
                            if(youHuiQuanLieBiaoEntity.getResponse().getCondition() == null){

                            }else{
                                String content = youHuiQuanLieBiaoEntity.getResponse().getCondition();
                                String replace = content.replace("--", "\n");
                                String replace1 = replace.replace("&nbsp;", " ");
                                tvTitle.setText(replace1);
//                                tvTitle.setText(youHuiQuanLieBiaoEntity.getResponse().getCondition());
                            }


                            youHuiQuanAdapter = new YouHuiQuanAdapter(YouHuiQuanFuWuActivity.this, youHuiQuanLieBiaoEntity.getResponse().getShopPreferences());
                            LinearLayoutManager manager = new LinearLayoutManager(YouHuiQuanFuWuActivity.this);
                            recyclerView.setLayoutManager(manager);
                            recyclerView.setAdapter(youHuiQuanAdapter);

                            youHuiQuanAdapter.setXiangQingClick(new YouHuiQuanAdapter.XiangQingClick() {
                                @Override
                                public void xiangqingClick(int position) {
                                    Intent intent = new Intent(YouHuiQuanFuWuActivity.this, YouHuiQuanXiangQingActivity.class);
                                    intent.putExtra("id", youHuiQuanLieBiaoEntity.getResponse().getShopPreferences().get(position).getId());
                                    System.out.println(youHuiQuanLieBiaoEntity.getResponse().getShopPreferences().get(position).getId() + "            chianid");
                                    startActivity(intent);
                                }
                            });


//                            qiYeFuWuYouHuiHuoDongAdapter = new QiYeFuWuYouHuiHuoDongAdapter(YouHuiQuanFuWuActivity.this, qiYeFuWuYouHuiHuoDongEntity.getResponse().getShopPreferences());
//                            LinearLayoutManager manager = new LinearLayoutManager(YouHuiQuanFuWuActivity.this);
//                            recyclerView.setLayoutManager(manager);
//                            recyclerView.setAdapter(qiYeFuWuYouHuiHuoDongAdapter);
//
//                            qiYeFuWuYouHuiHuoDongAdapter.setXiangQingClick(new QiYeFuWuYouHuiHuoDongAdapter.XiangQingClick() {
//                                @Override
//                                public void xiangqingClick(int position) {
//                                    Intent intent = new Intent(YouHuiQuanFuWuActivity.this, QiYeFuWuXiangQingActivity.class);
//                                    intent.putExtra("id", qiYeFuWuYouHuiHuoDongEntity.getResponse().getShopPreferences().get(position).getId());
//                                    System.out.println(qiYeFuWuYouHuiHuoDongEntity.getResponse().getShopPreferences().get(position).getId() + "   列表点击进入详情传入的id");
//                                    startActivity(intent);
//                                }
//                            });


                        }else if(response.indexOf("400") != -1){
                            ContractUtils.Code400(YouHuiQuanFuWuActivity.this,response);
                        }
                    }
                });
    }


//    //    企业服务列表的网络请求
//    private void initQiYeFuWuLieBiaoHttp() {
//        OkHttpUtils.post()
//                .url(ContractUtils.LOGIN_URL + "fuwu/index")
//                .addHeader("token", ContractUtils.getTOKEN(YouHuiQuanFuWuActivity.this))
//                .addParams("parkId", ContractUtils.getParkId(YouHuiQuanFuWuActivity.this))
//                .addParams("typeId", "2")//1:物业服务，2：企业服务
//                .build()
//                .execute(new StringCallback() {
//                    @Override
//                    public void onError(Call call, Exception e, int id) {
//
//                    }
//
//                    @Override
//                    public void onResponse(String response, int id) {
//                        System.out.println(response + "     企业服务的网络请求");
//                        Gson gson = new Gson();
//                        qiYeFuWuEntity = gson.fromJson(response, QiYeFuWuEntity.class);
//                        if (response.indexOf("200") != -1) {
//                            if (qiYeFuWuEntity.getResponse().getContent() == null) {
//
//                            } else {
//                                tvTitle.setText(qiYeFuWuEntity.getResponse().getContent());
//                            }
//
//
//                            qiYeFuWuYouHuiHuoDongAdapter = new QiYeFuWuYouHuiHuoDongAdapter(YouHuiQuanFuWuActivity.this, qiYeFuWuEntity.getResponse().getData());
//                            LinearLayoutManager manager = new LinearLayoutManager(YouHuiQuanFuWuActivity.this);
//                            recyclerView.setLayoutManager(manager);
//                            recyclerView.setAdapter(qiYeFuWuYouHuiHuoDongAdapter);
//
//                            qiYeFuWuYouHuiHuoDongAdapter.setXiangQingClick(new QiYeFuWuYouHuiHuoDongAdapter.XiangQingClick() {
//                                @Override
//                                public void xiangqingClick(int position) {
////                                    if (qiYeFuWuEntity.getResponse().getData().get(position).getType().equals("1")) {
//                                        Intent intent = new Intent(YouHuiQuanFuWuActivity.this, QiYeFuWuXiangQingActivity.class);
//                                        intent.putExtra("id", qiYeFuWuEntity.getResponse().getData().get(position).getId());
//                                        System.out.println(qiYeFuWuEntity.getResponse().getData().get(position).getId() + "   列表点击进入详情传入的id");
//                                        startActivity(intent);
////                                    }
//                                }
//                            });
//                        }
//                    }
//                });
//    }


    @OnClick({R.id.image_back, R.id.recyclerView})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.image_back:
                finish();
                break;
            case R.id.recyclerView:
                break;
        }
    }

//    @OnClick({R.id.relative_huodong, R.id.relative_youhuiquan,R.id.image_back})
//    public void onViewClicked(View view) {
//        switch (view.getId()) {
//            case R.id.image_back:
//                finish();
//                break;
////            商家优惠活动
//            case R.id.relative_huodong:
//                Intent intent = new Intent(QiYeFuWuActivity.this, QiYeFuWuXiangQingActivity.class);
//                startActivity(intent);
//                break;
////            企业优惠政策
//            case R.id.relative_youhuiquan:
//                Intent intent1 = new Intent(QiYeFuWuActivity.this, QiYeFuWuXiangQingActivity.class);
//                startActivity(intent1);
//                break;
//        }
//    }
}
