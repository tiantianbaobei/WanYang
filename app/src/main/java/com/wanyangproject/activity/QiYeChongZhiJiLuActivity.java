package com.wanyangproject.activity;

import android.content.Intent;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bigkoo.pickerview.TimePickerView;
import com.google.gson.Gson;
import com.wanyangproject.R;
import com.wanyangproject.adapter.ChongZhiAdapter;
import com.wanyangproject.entity.YiKaTongYuEEntity;
import com.wanyangproject.my.ChongZhJiLuActivity;
import com.wanyangproject.my.YiKaTongActivity;
import com.wanyangproject.utils.ContractUtils;
import com.zhy.http.okhttp.OkHttpUtils;
import com.zhy.http.okhttp.callback.StringCallback;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import butterknife.BindView;
import butterknife.OnClick;
import okhttp3.Call;

public class QiYeChongZhiJiLuActivity extends AppCompatActivity {





    @BindView(R.id.image_back)
    ImageView imageBack;
    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    @BindView(R.id.relative_rl)
    RelativeLayout relativeRl;
    @BindView(R.id.tv_time)
    TextView tvTime;
    private ChongZhiAdapter chongZhiAdapter;
    private ArrayList<JSONObject> list = new ArrayList();
    private ArrayList<JSONObject>  shanxuaihou = new ArrayList();//筛选之后的
    private String time;





    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_qi_ye_chong_zhi_ji_lu);


//        一卡通充值记录的网络请求
        initYiKaTongChongZhiHttp();
    }


    //    一卡通充值记录的网络请求
    private void initYiKaTongChongZhiHttp() {
        OkHttpUtils.post()
                .url(ContractUtils.LOGIN_URL + "user/record")
                .addHeader("token", ContractUtils.getTOKEN(QiYeChongZhiJiLuActivity.this))
                .addParams("parkId", ContractUtils.getParkId(QiYeChongZhiJiLuActivity.this))
                .addParams("page", "0")
                .addParams("type", "3")//1：水费充值记录 2：电表充值记录 3：一卡通
                .build()
                .execute(new StringCallback() {
                    @Override
                    public void onError(Call call, Exception e, int id) {

                    }

                    @Override
                    public void onResponse(String response, int id) {
                        System.out.println(response + "          一卡通充值记录的网络请求");
                        ContractUtils.Code500(QiYeChongZhiJiLuActivity.this, response);
                        if (response.indexOf("200") != -1) {

                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                JSONObject jsonObject1 = jsonObject.getJSONObject("response");
                                JSONObject data = jsonObject1.getJSONObject("data");
                                if (data != null) {
                                    JSONArray jsonArray = data.getJSONArray("list");

                                    for (int i = 0; i < jsonArray.length(); i++) {
                                        JSONObject object = (JSONObject) jsonArray.get(i);
                                        if(object != null){
                                            list.add(object);
                                        }
                                    }
                                }


                                if(time != null){
                                    shanxuaihou.clear();
                                    for (int i = 0; i < list.size(); i++) {
                                        JSONObject jsonObject2 = list.get(i);
                                        String time1 = jsonObject2.getString("time");
                                        if (time1.indexOf(time) != -1){
                                            shanxuaihou.add(jsonObject2);
                                        }
                                    }

//                                    chongZhiAdapter.notifyDataSetChanged();
                                }else{
                                    shanxuaihou.clear();
                                    shanxuaihou.addAll(list);
                                }

                                if (list == null) {
                                    Toast.makeText(QiYeChongZhiJiLuActivity.this, "暂无数据", Toast.LENGTH_SHORT).show();
                                    return;
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }


//
//                            Gson gson = new Gson();
//                            ShuiFeiJiLuEntity shuiFeiJiLuEntity = gson.fromJson(response, ShuiFeiJiLuEntity.class);

                            chongZhiAdapter = new ChongZhiAdapter(QiYeChongZhiJiLuActivity.this, list);
                            LinearLayoutManager manager = new LinearLayoutManager(QiYeChongZhiJiLuActivity.this);
                            recyclerView.setLayoutManager(manager);
                            recyclerView.setAdapter(chongZhiAdapter);

                        }
                    }
                });
    }


    private void initView() {
//        chongZhiAdapter = new ChongZhiAdapter(this);
//        LinearLayoutManager manager = new LinearLayoutManager(this);
//        recyclerView.setLayoutManager(manager);
//        recyclerView.setAdapter(chongZhiAdapter);
    }

    @OnClick({R.id.image_back, R.id.recyclerView,R.id.tv_time})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.image_back:
                finish();
                break;
            case R.id.recyclerView:
                break;
            case R.id.tv_time:
                Calendar calendar = Calendar.getInstance();

                TimePickerView timePickerView = new TimePickerView.Builder(QiYeChongZhiJiLuActivity.this, new TimePickerView.OnTimeSelectListener() {
                    @Override
                    public void onTimeSelect(Date date, View v) {
//                        SimpleDateFormat simpleDateFormat1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                        SimpleDateFormat simpleDateFormat1 = new SimpleDateFormat("yyyy-MM");
                        tvTime.setText(simpleDateFormat1.format(date));
                        shanxuaihou.clear();
                        time = simpleDateFormat1.format(date);
                        for (int i = 0; i < list.size(); i++) {
                            JSONObject jsonObject2 = list.get(i);
                            String time1 = null;
                            try {
                                time1 = jsonObject2.getString("time");
                                if (time1.indexOf(time) != -1){
                                    shanxuaihou.add(jsonObject2);
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }

                        chongZhiAdapter.notifyDataSetChanged();

//                        try {
//                            Date date1 = simpleDateFormat1.parse(tvTime.getText().toString());
//                            long time = date1.getTime();
//                            string = String.valueOf(time);
//                            System.out.println(string + "     时间戳");
//                        } catch (ParseException e) {
//                            e.printStackTrace();
//                        }
                    }
                })
                        .setType(new boolean[]{true, true, false, false, false, false})//默认全部显示
                        .setCancelText("取消")//取消按钮文字
                        .setSubmitText("确定")//确认按钮文字
                        .setTextXOffset(0, 0, 0, 0, 0, 0)
                        .setLunarCalendar(false)
                        .setContentSize(14)//滚轮文字大小
                        .gravity(Gravity.CENTER)
                        .setLineSpacingMultiplier(2.8f)//设置两横线之间的间隔倍数
                        .setTitleSize(16)//标题文字大小
                        .setTitleText("选择日期")//标题文字
                        .setOutSideCancelable(true)//点击屏幕，点在控件外部范围时，是否取消显示
                        .isCyclic(false)//是否循环滚动
                        .setTextColorCenter(Color.BLACK)//设置选中项的颜色
                        .setTextColorOut(Color.parseColor("#AEAAAA"))
                        .setTitleColor(Color.BLACK)//标题文字颜色
                        .setSubmitColor(Color.RED)//确定按钮文字颜色
                        .setCancelColor(Color.parseColor("#AEAAAA"))//取消按钮文字颜色
                        .setTitleBgColor(Color.parseColor("#f5f5f5"))//标题背景颜色 Night mode
                        .setBgColor(Color.parseColor("#ffffff"))//滚轮背景颜色 Night mode
                        .setRange(calendar.get(Calendar.YEAR), 2100)//默认是1900-2100年
//                        .setDate(selectedDate)// 如果不设置的话，默认是系统时间*/
//                        .setRangDate(startDate,endDate)//起始终止年月日设定
                        .setLabel("年", "月", "", ":", "", "")
                        .isCenterLabel(true) //是否只显示中间选中项的label文字，false则每项item全部都带有label。
                        .isDialog(false)//是否显示为对话框样式
                        .build();
                timePickerView.setDate(Calendar.getInstance());//注：根据需求来决定是否使用该方法（一般是精确到秒的情况），此项可以在弹出选择器的时候重新设置当前时间，避免在初始化之后由于时间已经设定，导致选中时间与当前时间不匹配的问题。
                timePickerView.show();


                break;
        }
    }
}
