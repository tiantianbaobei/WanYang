package com.wanyangproject.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.wanyangproject.R;
import com.wanyangproject.base.MyApp;
import com.wanyangproject.utils.BitmapUtil;
import com.wanyangproject.utils.SharedPreferencesYinDaoYe;

import java.util.ArrayList;
import java.util.List;

public class GuideActivity extends AppCompatActivity {

	private int[] mImageIds = new int[] {
			R.drawable.yindaoye_one,
			R.drawable.yindaoye_two,
			R.drawable.yindaoye_three,
			R.drawable.yindaoye_four
	};


//	private int[] mImageIds = new int[] {
//			R.drawable.small_one,
//			R.drawable.small_two,
//			R.drawable.small_three,
//			R.drawable.small_four
//	};

	private List<ImageView> list;
	private TextView tv_jinru;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_yin_dao_ye);

		ViewPager viewPager = (ViewPager) findViewById(R.id.viewPager);
		tv_jinru = (TextView) findViewById(R.id.tv_jinru);

		list = new ArrayList<>();
		list.clear();
		for (int resId : mImageIds) {
			ImageView imageView = new ImageView(this);
			imageView.setScaleType(ImageView.ScaleType.FIT_XY);
			Glide.with(GuideActivity.this).load(resId).into(imageView);
//			imageView.setImageBitmap(BitmapUtil.getFitBitmap(getResources(), resId, imageView));
			list.add(imageView);
		}

		viewPager.setAdapter(new MyAdapter());
		viewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
			@Override
			public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

			}

			@Override
			public void onPageSelected(int position) {
				if (position == list.size() - 1) {
					tv_jinru.setVisibility(View.VISIBLE);
				} else {
					tv_jinru.setVisibility(View.GONE);
				}
			}

			@Override
			public void onPageScrollStateChanged(int state) {

			}
		});
	}

	public void tiaoguo(View v) {
		switch (v.getId()) {
			case R.id.tv_tiaoguo:
			case R.id.tv_jinru:
				Intent intent = new Intent(GuideActivity.this,LoginActivity.class);
				startActivity(intent);
				SharedPreferencesYinDaoYe.saveyindaoye(MyApp.getContext(),"yindaoye");
				finish();
				break;
		}

	}

	private class MyAdapter extends PagerAdapter {

		@Override
		public int getCount() {
			return list.size();
		}

		@Override
		public boolean isViewFromObject(View view, Object object) {
			return view == object;
		}

		@Override
		public Object instantiateItem(ViewGroup container, int position) {
			ImageView imageView = list.get(position);
			container.addView(imageView);
			return imageView;
		}

		@Override
		public void destroyItem(ViewGroup container, int position, Object object) {
			container.removeView((View) object);
		}
	}

}
