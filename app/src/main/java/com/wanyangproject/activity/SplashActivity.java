package com.wanyangproject.activity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.wanyangproject.R;
import com.wanyangproject.utils.BitmapUtil;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SplashActivity extends AppCompatActivity {

    @BindView(R.id.image)
    ImageView image;
    private String response;
    private Boolean first = false;
    private String  yindaoye;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_start);
        ButterKnife.bind(this);

        Glide.with(SplashActivity.this).load(R.drawable.qidong).into(image);


        //        记住登录状态
        SharedPreferences sharedPreferences = getSharedPreferences("user", Context.MODE_PRIVATE);
        response = sharedPreferences.getString("response", "response");
//        if(!response.equals("response")){
//            System.out.println(response+"   记住登录状态");
//            Intent intent = new Intent(SplashActivity.this,HomeActivity.class);
//            startActivity(intent);
//            finish();
//        }


        SharedPreferences sharedPreferences1 = getSharedPreferences("yindaoye", Context.MODE_PRIVATE);
        yindaoye = sharedPreferences1.getString("yindaoye", "11");
        System.out.println(yindaoye+"          hhhhhhhhhhhhhhhhhhh");

        Thread myThread=new Thread(){//创建子线程
            @Override
            public void run() {
                try{
                    sleep(1000);//使程序休眠五秒
                    if(!yindaoye.equals("yindaoye")){
                        System.out.println("11111111111111");
                        Intent intent = new Intent(SplashActivity.this,GuideActivity.class);
                        startActivity(intent);
                    }else{
                        if(!response.equals("response")){
                            System.out.println("222222222222");
                            Intent intent = new Intent(SplashActivity.this,HomeActivity.class);
                            startActivity(intent);
                            finish();
                        }else{
                            System.out.println("33333333333333");
                            Intent it=new Intent(SplashActivity.this,LoginActivity.class);//启动MainActivity
                            startActivity(it);
                            finish();//关闭当前活动
                        }
                    }

                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        };
        myThread.start();//启动线程
    }

}
