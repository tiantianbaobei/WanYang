package com.wanyangproject.activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.scwang.smartrefresh.layout.SmartRefreshLayout;
import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.scwang.smartrefresh.layout.listener.OnLoadmoreListener;
import com.scwang.smartrefresh.layout.listener.OnRefreshListener;
import com.wanyangproject.R;
import com.wanyangproject.adapter.HistoryConsumptionAdapter;
import com.wanyangproject.base.BaseActivity;
import com.wanyangproject.entity.HistoryConsumptionEntity;
import com.wanyangproject.my.MyQiYeSuSheXiangQingActivity;
import com.wanyangproject.net.util.ToastUtil;
import com.wanyangproject.utils.ContractUtils;
import com.zhy.http.okhttp.OkHttpUtils;
import com.zhy.http.okhttp.callback.StringCallback;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import okhttp3.Call;

public class HistoryConsumptionActivity extends BaseActivity implements View.OnClickListener,OnRefreshListener,OnLoadmoreListener {


    public static void start(Context context,String type, String electricityUserId){
        Intent intent=new Intent(context,HistoryConsumptionActivity.class);
        intent.putExtra("type",type);
        intent.putExtra("electricityUserId",electricityUserId);
        context.startActivity(intent);
    }


    @BindView(R.id.aty_history_consumptionSmart)
    SmartRefreshLayout smartRefreshLayout;
    @BindView(R.id.aty_history_recyclerView)
    RecyclerView recyclerView;
    @BindView(R.id.bar_back)
    ImageView imageBack;
    @BindView(R.id.bar_title)
    TextView title;
    private String mToken;
    private String mParkId;
    private String mType;
    private String mElectricityUserId;
    private HistoryConsumptionAdapter historyConsumptionAdapter;
    private HistoryConsumptionEntity historyConsumptionEntity;
    private int page = 0;
    private boolean isRefresh = false;

    @Override
    public int getContentResId() {
        return R.layout.activity_history_consumption;
    }

    @Override
    public void init() {
        ButterKnife.bind(this);
        mToken= ContractUtils.getTOKEN(HistoryConsumptionActivity.this);
        mParkId=ContractUtils.getParkId(HistoryConsumptionActivity.this);
        mType=getIntent().getStringExtra("type");
        mElectricityUserId=getIntent().getStringExtra("electricityUserId");
        ButterKnife.bind(this);
        title.setText("历史用量");
        imageBack.setOnClickListener(this);
        smartRefreshLayout.setOnRefreshListener(this);
        smartRefreshLayout.setOnLoadmoreListener(this);
        historyConsumptionAdapter = new HistoryConsumptionAdapter(mContext, null);
        LinearLayoutManager manager =new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(manager);
        recyclerView.setAdapter(historyConsumptionAdapter);

        getHistoryData(mToken,mParkId,mType,mElectricityUserId,page);
    }

    /**
     * 获取历史水电使用量
     * @param token
     * @param parkId
     * @param type
     * @param electricityUserId  水或电 id
     * @param page  页数
     */
    private void getHistoryData(String token, String parkId, String type, String electricityUserId, final int page) {
        final ProgressDialog progressDialog = new ProgressDialog(HistoryConsumptionActivity.this);
        progressDialog.setTitle("提示");
        progressDialog.setMessage("请等待...");
        progressDialog.setCancelable(false);
        progressDialog.show();
        OkHttpUtils.post()
                .url(ContractUtils.LOGIN_URL + "company/hydropower")
                .addHeader("token",token)
                .addParams("parkId",parkId)
                .addParams("type", type)
                .addParams("electricityUserId",electricityUserId)
                .addParams("page",String.valueOf(page))
                .build()
                .execute(new StringCallback() {
                    @Override
                    public void onError(Call call, Exception e, int id) {
                        progressDialog.dismiss();
                    }
                    @Override
                    public void onResponse(String response, int id) {
                        progressDialog.dismiss();
                        ContractUtils.Code500(HistoryConsumptionActivity.this,response);
                        if(response.indexOf("200") != -1){
                            Gson gson = new Gson();
                            historyConsumptionEntity = gson.fromJson(response, HistoryConsumptionEntity.class);
                            if(historyConsumptionEntity != null){
                             List<HistoryConsumptionEntity.Response.Data.Consumption> resultList=
                                     historyConsumptionEntity.getResponse().getData().getList();
                             if(resultList!=null&&resultList.size()>0){
                                 if(isRefresh){
                                     historyConsumptionAdapter.setData(resultList);
                                 }else{
                                     historyConsumptionAdapter.addData(resultList);
                                 }
                                 historyConsumptionAdapter.setConsumptionType(mType);
                             }else{
                                 if(isRefresh){
                                     ToastUtil.show(mContext,"数据为空");
                                 }else{
                                     ToastUtil.show(mContext,"无更多数据");
                                 }
                             }

                            }
                        }
                    }
                });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.bar_back:
                finish();
                break;

        }
    }

    @Override
    public void onLoadmore(RefreshLayout refreshlayout) {
        isRefresh = false;
        page++;
        getHistoryData(mToken,mParkId,mType,mElectricityUserId,page);
        refreshlayout.finishLoadmore();
    }

    @Override
    public void onRefresh(RefreshLayout refreshlayout) {
        isRefresh = true;
        page = 0;
        getHistoryData(mToken,mParkId,mType,mElectricityUserId,page);
        refreshlayout.finishRefresh();
    }
}
