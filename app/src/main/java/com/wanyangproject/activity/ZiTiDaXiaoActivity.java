package com.wanyangproject.activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.DisplayMetrics;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.wanyangproject.R;
import com.wanyangproject.my.SettingActivity;
import com.wanyangproject.utils.SharedPreferencesToken;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

// 字体大小界面
public class ZiTiDaXiaoActivity extends AppCompatActivity {

    @BindView(R.id.image_back)
    ImageView imageBack;
    @BindView(R.id.image_yuandian)
    ImageView imageYuandian;
    @BindView(R.id.image_yuandian_zhong)
    ImageView imageYuandianZhong;
    @BindView(R.id.image_yuandian_da)
    ImageView imageYuandianDa;
    @BindView(R.id.image_yuandian_chaoda)
    ImageView imageYuandianChaoda;
    @BindView(R.id.tv_xiao)
    TextView tvXiao;
    @BindView(R.id.tv_zhong)
    TextView tvZhong;
    @BindView(R.id.tv_da)
    TextView tvDa;
    @BindView(R.id.tv_chaoda)
    TextView tvChaoda;
    @BindView(R.id.view_xiao)
    View viewXiao;
    @BindView(R.id.view_zhong)
    View viewZhong;
    @BindView(R.id.view_da)
    View viewDa;
    @BindView(R.id.view_chaoda)
    View viewChaoda;
    @BindView(R.id.tv_queding)
    TextView tvQueding;
    private float zitidaiao = 1.0f;
    private String ziti = "";
    private float zitidaxiaoxuanzhong = 1.0f;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_zi_ti_da_xiao);
        ButterKnife.bind(this);
        if (Build.VERSION.SDK_INT >= 21) {
            View dview = getWindow().getDecorView();
            dview.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN | View.SYSTEM_UI_FLAG_LAYOUT_STABLE);
            getWindow().setStatusBarColor(Color.TRANSPARENT);
        }



        SharedPreferences sharedPreferences1 = getSharedPreferences("zitidaxiao", Context.MODE_PRIVATE);
        zitidaxiaoxuanzhong = sharedPreferences1.getFloat("zitidaxiao", 1);
        System.out.println(zitidaxiaoxuanzhong + "     yuandian选中的那个字体");

        if(zitidaxiaoxuanzhong == 0.9f){
            imageYuandian.setVisibility(View.VISIBLE);
            imageYuandianZhong.setVisibility(View.INVISIBLE);
            imageYuandianDa.setVisibility(View.INVISIBLE);
            imageYuandianChaoda.setVisibility(View.INVISIBLE);

        }else if(zitidaxiaoxuanzhong == 1.0f){
            imageYuandian.setVisibility(View.INVISIBLE);
            imageYuandianZhong.setVisibility(View.VISIBLE);
            imageYuandianDa.setVisibility(View.INVISIBLE);
            imageYuandianChaoda.setVisibility(View.INVISIBLE);
        }else if(zitidaxiaoxuanzhong == 1.1f){
            imageYuandian.setVisibility(View.INVISIBLE);
            imageYuandianZhong.setVisibility(View.INVISIBLE);
            imageYuandianDa.setVisibility(View.VISIBLE);
            imageYuandianChaoda.setVisibility(View.INVISIBLE);
        }else if(zitidaxiaoxuanzhong == 1.2f){
            imageYuandian.setVisibility(View.INVISIBLE);
            imageYuandianZhong.setVisibility(View.INVISIBLE);
            imageYuandianDa.setVisibility(View.INVISIBLE);
            imageYuandianChaoda.setVisibility(View.VISIBLE);
        }




    }


    @OnClick({R.id.tv_queding,R.id.view_xiao, R.id.view_zhong, R.id.view_da, R.id.view_chaoda, R.id.image_back, R.id.tv_xiao, R.id.tv_zhong, R.id.tv_da, R.id.tv_chaoda})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.image_back:
                finish();
                break;
            case R.id.tv_queding:

                SharedPreferencesToken.saveziti(ZiTiDaXiaoActivity.this,ziti);
                SharedPreferencesToken.saveZiTiDaXiao(ZiTiDaXiaoActivity.this,zitidaiao);

                changeTextSize(ZiTiDaXiaoActivity.this,zitidaiao);
                Intent intent = new Intent(ZiTiDaXiaoActivity.this,HomeActivity.class);
                startActivity(intent);

                break;
            case R.id.view_xiao:
                zitidaiao = 0.9f;
                ziti = "小";
                imageYuandian.setVisibility(View.VISIBLE);
                imageYuandianZhong.setVisibility(View.INVISIBLE);
                imageYuandianDa.setVisibility(View.INVISIBLE);
                imageYuandianChaoda.setVisibility(View.INVISIBLE);
                break;
            case R.id.view_zhong:
                zitidaiao = 1.0f;
                ziti = "中";
                imageYuandian.setVisibility(View.INVISIBLE);
                imageYuandianZhong.setVisibility(View.VISIBLE);
                imageYuandianDa.setVisibility(View.INVISIBLE);
                imageYuandianChaoda.setVisibility(View.INVISIBLE);
                break;
            case R.id.view_da:
                zitidaiao = 1.1f;
                ziti = "大";
                imageYuandian.setVisibility(View.INVISIBLE);
                imageYuandianZhong.setVisibility(View.INVISIBLE);
                imageYuandianDa.setVisibility(View.VISIBLE);
                imageYuandianChaoda.setVisibility(View.INVISIBLE);
                break;
            case R.id.view_chaoda:
                zitidaiao = 1.2f;
                ziti = "超大";
                imageYuandian.setVisibility(View.INVISIBLE);
                imageYuandianZhong.setVisibility(View.INVISIBLE);
                imageYuandianDa.setVisibility(View.INVISIBLE);
                imageYuandianChaoda.setVisibility(View.VISIBLE);
                break;
            case R.id.tv_xiao:
//                imageYuandian.setVisibility(View.VISIBLE);
//                imageYuandianZhong.setVisibility(View.INVISIBLE);
//                imageYuandianDa.setVisibility(View.INVISIBLE);
//                imageYuandianChaoda.setVisibility(View.INVISIBLE);
                break;
            case R.id.tv_zhong:
//                imageYuandian.setVisibility(View.INVISIBLE);
//                imageYuandianZhong.setVisibility(View.VISIBLE);
//                imageYuandianDa.setVisibility(View.INVISIBLE);
//                imageYuandianChaoda.setVisibility(View.INVISIBLE);
                break;
            case R.id.tv_da:
//                imageYuandian.setVisibility(View.INVISIBLE);
//                imageYuandianZhong.setVisibility(View.INVISIBLE);
//                imageYuandianDa.setVisibility(View.VISIBLE);
//                imageYuandianChaoda.setVisibility(View.INVISIBLE);
                break;
            case R.id.tv_chaoda:
//                imageYuandian.setVisibility(View.INVISIBLE);
//                imageYuandianZhong.setVisibility(View.INVISIBLE);
//                imageYuandianDa.setVisibility(View.INVISIBLE);
//                imageYuandianChaoda.setVisibility(View.VISIBLE);
                break;
        }
    }






    public  void changeTextSize(Activity activity, float multiple) {
        Configuration configuration = getResources().getConfiguration();
        configuration.fontScale = multiple;    //1为标准字体，multiple为放大的倍数
        DisplayMetrics displayMetrics = new DisplayMetrics();
        activity.getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        displayMetrics.scaledDensity = configuration.fontScale * displayMetrics.density;
        getBaseContext().getResources().updateConfiguration(configuration, displayMetrics);
    }




}
