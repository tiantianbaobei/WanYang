package com.wanyangproject.activity;

import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;

import com.wanyangproject.R;


public class ChangeParkActivity extends AppCompatActivity implements View.OnClickListener{

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_park);
        if (Build.VERSION.SDK_INT >= 21) {
            View dview = getWindow().getDecorView();
            dview.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN | View.SYSTEM_UI_FLAG_LAYOUT_STABLE);
            getWindow().setStatusBarColor(Color.TRANSPARENT);
        }

        initView();
        initData();
    }
    private void initView() {
        ImageView changePark_back=(ImageView) findViewById(R.id.changePark_back);
        ImageView changePark_increase = (ImageView) findViewById(R.id.changePark_increase);
        changePark_back.setOnClickListener(this);
        changePark_increase.setOnClickListener(this);
    }

    private void initData() {

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.changePark_back:
                finish();
                break;
//            点击加号切换园区
            case R.id.changePark_increase:
//                startActivity(new Intent(ChangeParkActivity.this,ChooseParkAddRbActivity.class));
                break;
        }
    }
}
