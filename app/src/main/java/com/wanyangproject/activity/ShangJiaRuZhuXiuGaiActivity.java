package com.wanyangproject.activity;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bigkoo.pickerview.OptionsPickerView;
import com.bumptech.glide.Glide;
import com.google.gson.Gson;
import com.lzy.imagepicker.ImagePicker;
import com.lzy.imagepicker.bean.ImageItem;
import com.lzy.imagepicker.ui.ImageGridActivity;
import com.wanyangproject.R;
import com.wanyangproject.entity.DuanXinShiBaiEntity;
import com.wanyangproject.entity.PhotoEntity;
import com.wanyangproject.entity.ShangJiaRuZhuDianPuLeiXingEntity;
import com.wanyangproject.entity.ShangJiaRuZhuDuanXinEntity;
import com.wanyangproject.entity.ShangJiaRuZhuEntity;
import com.wanyangproject.entity.ShangJiaRuZhuXiuGaiEntity;
import com.wanyangproject.my.ShangJiaRuZhuActivity;
import com.wanyangproject.popuwindow.DianPuLeiXingPopupWindow;
import com.wanyangproject.popuwindow.TakePhotoPopWin;
import com.wanyangproject.utils.ContractUtils;
import com.wanyangproject.utils.GlideImageLoaderForPicker;
import com.zhy.http.okhttp.OkHttpUtils;
import com.zhy.http.okhttp.callback.StringCallback;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import top.zibin.luban.Luban;
import top.zibin.luban.OnCompressListener;

public class ShangJiaRuZhuXiuGaiActivity extends AppCompatActivity {


    @BindView(R.id.image_back)
    ImageView imageBack;
    @BindView(R.id.et_shenqingren)
    EditText etShenqingren;
    @BindView(R.id.et_phone)
    EditText etPhone;
    @BindView(R.id.tv_huoqu_yanzhengma)
    TextView tvHuoquYanzhengma;
    @BindView(R.id.et_dianpu_mingcheng)
    EditText etDianpuMingcheng;
    @BindView(R.id.tv_xuanze_dianpu)
    TextView tvXuanzeDianpu;
    @BindView(R.id.et_shuru_yanzhengma)
    EditText etShuruYanzhengma;
    @BindView(R.id.btn_ruzhu)
    Button btnRuzhu;
    @BindView(R.id.et_dianpu_dizhi)
    EditText etDianpuDizhi;
    @BindView(R.id.et_dianpu_jieshao)
    EditText etDianpuJieshao;
    @BindView(R.id.et_faren_name)
    EditText etFarenName;
    @BindView(R.id.image_zhengmian)
    ImageView imageZhengmian;
    @BindView(R.id.image_fanmian)
    ImageView imageFanmian;
    @BindView(R.id.image_yingyezhizhao)
    ImageView imageYingyezhizhao;
    private DianPuLeiXingPopupWindow dianPuLeiXingPopupWindow;
    private int num;
    private ShangJiaRuZhuDianPuLeiXingEntity shangJiaRuZhuDianPuLeiXingEntity;
//    private String lxid;
    private TakePhotoPopWin takePhotoPopWin;
    private PhotoEntity ZhengMianEntity;
    private PhotoEntity FanMianEntity;
    private PhotoEntity YingYeZhiZhaoEntity;
    private ShangJiaRuZhuDuanXinEntity shangJiaRuZhuDuanXinEntity;
    private int isOK = 0;
    private ArrayList<String> list11 = new ArrayList<>();//店铺类型id
    private ArrayList<String> list22 = new ArrayList<>();//店铺类型title
    private Uri photoUri;
    private String id;
    private ShangJiaRuZhuXiuGaiEntity shangJiaRuZhuXiuGaiEntity;
    private String dianpuid;







    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shang_jia_ru_zhu_xiu_gai);
        ButterKnife.bind(this);


        Intent intent = getIntent();
        id = intent.getStringExtra("id");
        System.out.println(id+"       接收id");



//        修改商家入驻信息的网络请求
        initShangJiaXiuGaiHttp();

    }





//    修改商家入驻信息的网络请求
    private void initShangJiaXiuGaiHttp() {
        OkHttpUtils.post()
                .url(ContractUtils.LOGIN_URL+"shop/shopdetails")
                .addHeader("token",ContractUtils.getTOKEN(ShangJiaRuZhuXiuGaiActivity.this))
                .addParams("parkId",ContractUtils.getParkId(ShangJiaRuZhuXiuGaiActivity.this))
                .addParams("id",id)
                .build()
                .execute(new StringCallback() {
                    @Override
                    public void onError(Call call, Exception e, int id) {

                    }

                    @Override
                    public void onResponse(String response, int id) {
                        System.out.println(response+"          商家信息的详情 ");
                        ContractUtils.Code500(ShangJiaRuZhuXiuGaiActivity.this,response);
                        if(response.indexOf("200") != -1){
                            Gson gson = new Gson();
                            shangJiaRuZhuXiuGaiEntity = gson.fromJson(response, ShangJiaRuZhuXiuGaiEntity.class);
                            if(shangJiaRuZhuXiuGaiEntity.getResponse().getApplicant_name() != null){
                                etShenqingren.setText(shangJiaRuZhuXiuGaiEntity.getResponse().getApplicant_name());
                            }

                            if(shangJiaRuZhuXiuGaiEntity.getResponse().getApplicant_phone() != null){
                                etPhone.setText(shangJiaRuZhuXiuGaiEntity.getResponse().getApplicant_phone());
                            }

                            if(shangJiaRuZhuXiuGaiEntity.getResponse().getName() != null){
                                etDianpuMingcheng.setText(shangJiaRuZhuXiuGaiEntity.getResponse().getName());
                            }

                            if(shangJiaRuZhuXiuGaiEntity.getResponse().getClassX() != null){
                                //        商家入驻店铺类型的网络请求
                                initShangJiaRuZhuDianPuLeiXingHttp();

//                                if(list22 != null){
//                                    dianpuid = list11.indexOf(shangJiaRuZhuXiuGaiEntity.getResponse().getClassX());
//                                    String s = list22.get(dianpuid);
//                                    tvXuanzeDianpu.setText(s);
//                                }
                            }



                            if(shangJiaRuZhuXiuGaiEntity.getResponse().getAddres() != null){
                                etDianpuDizhi.setText(shangJiaRuZhuXiuGaiEntity.getResponse().getAddres());
                            }

                            if(shangJiaRuZhuXiuGaiEntity.getResponse().getIntroduce() != null){
                                etDianpuJieshao.setText(shangJiaRuZhuXiuGaiEntity.getResponse().getIntroduce());
                            }

                            if(shangJiaRuZhuXiuGaiEntity.getResponse().getLegal_name() != null){
                                etFarenName.setText(shangJiaRuZhuXiuGaiEntity.getResponse().getLegal_name());
                            }

                            if(shangJiaRuZhuXiuGaiEntity.getResponse().getNumber_Positive() != null){
                                if(shangJiaRuZhuXiuGaiEntity.getResponse().getNumber_Positive().contains(ContractUtils.YUANQUZHAOSHANG_PHOTO_URL)){
                                    Glide.with(ShangJiaRuZhuXiuGaiActivity.this).load(shangJiaRuZhuXiuGaiEntity.getResponse().getNumber_Positive()).into(imageZhengmian);
                                }else{
                                    Glide.with(ShangJiaRuZhuXiuGaiActivity.this).load(ContractUtils.YUANQUZHAOSHANG_PHOTO_URL+shangJiaRuZhuXiuGaiEntity.getResponse().getNumber_Positive()).into(imageZhengmian);
                                }
                            }


                            if(shangJiaRuZhuXiuGaiEntity.getResponse().getNumber_side() != null){
                                if(shangJiaRuZhuXiuGaiEntity.getResponse().getNumber_side().contains(ContractUtils.YUANQUZHAOSHANG_PHOTO_URL)){
                                    Glide.with(ShangJiaRuZhuXiuGaiActivity.this).load(shangJiaRuZhuXiuGaiEntity.getResponse().getNumber_side()).into(imageFanmian);
                                }else{
                                    Glide.with(ShangJiaRuZhuXiuGaiActivity.this).load(ContractUtils.YUANQUZHAOSHANG_PHOTO_URL+shangJiaRuZhuXiuGaiEntity.getResponse().getNumber_side()).into(imageFanmian);
                                }
                            }



                            if(shangJiaRuZhuXiuGaiEntity.getResponse().getLicense() != null){
                                if(shangJiaRuZhuXiuGaiEntity.getResponse().getLicense().contains(ContractUtils.YUANQUZHAOSHANG_PHOTO_URL)){
                                    Glide.with(ShangJiaRuZhuXiuGaiActivity.this).load(shangJiaRuZhuXiuGaiEntity.getResponse().getLicense()).into(imageYingyezhizhao);
                                }else{
                                    Glide.with(ShangJiaRuZhuXiuGaiActivity.this).load(ContractUtils.YUANQUZHAOSHANG_PHOTO_URL+shangJiaRuZhuXiuGaiEntity.getResponse().getLicense()).into(imageYingyezhizhao);
                                }
                            }
                        }

                    }
                });
    }





    //    商家入驻店铺类型的网络请求
    private void initShangJiaRuZhuDianPuLeiXingHttp() {
        OkHttpUtils.post()
                .url(ContractUtils.LOGIN_URL + "shop/merchantclass")
                .addHeader("token", ContractUtils.getTOKEN(ShangJiaRuZhuXiuGaiActivity.this))
                .addParams("parkId", ContractUtils.getParkId(ShangJiaRuZhuXiuGaiActivity.this))
                .build()
                .execute(new StringCallback() {
                    @Override
                    public void onError(Call call, Exception e, int id) {
                        System.out.println(e + "      商家入驻类型eee");
                    }

                    @Override
                    public void onResponse(String response, int id) {
                        ContractUtils.Code500(ShangJiaRuZhuXiuGaiActivity.this,response);
                        System.out.println(response + "        商家入驻店铺类型的网络请求");

                        if (response.indexOf("200") != -1) {
                            Gson gson = new Gson();
                            shangJiaRuZhuDianPuLeiXingEntity = gson.fromJson(response, ShangJiaRuZhuDianPuLeiXingEntity.class);
                            for (int i = 0; i < shangJiaRuZhuDianPuLeiXingEntity.getResponse().size(); i++) {
                                list11.add(shangJiaRuZhuDianPuLeiXingEntity.getResponse().get(i).getId());
                                list22.add(shangJiaRuZhuDianPuLeiXingEntity.getResponse().get(i).getName());
                            }

                            if (shangJiaRuZhuXiuGaiEntity != null) {
                                if (list22.size() > 0) {
//                                    dianpuid = list11.indexOf(shangJiaRuZhuXiuGaiEntity.getResponse().getClassX());
                                    dianpuid = shangJiaRuZhuXiuGaiEntity.getResponse().getClassX();
                                    int xiaba = list11.indexOf(dianpuid);
                                    if(xiaba == -1){
                                        return;
                                    }

                                    System.out.println(dianpuid+"        dianpuid");
                                    String s = list22.get(xiaba);
                                    tvXuanzeDianpu.setText(s);
                                }
                            }


//                            if(shangJiaRuZhuXiuGaiEntity != null){
//                                dianpuid = list11.indexOf(shangJiaRuZhuXiuGaiEntity.getResponse().getClassX());
//                                String s = list22.get(dianpuid);
//                                tvXuanzeDianpu.setText(s);
//                            }

//                            Object itemsOnClick = null;
//                            ShangJiaRuZhuDianPuLeiXingEntity.dianpuleixing = shangJiaRuZhuDianPuLeiXingEntity.getResponse();
//                            dianPuLeiXingPopupWindow = new DianPuLeiXingPopupWindow(ShangJiaRuZhuActivity.this, (View.OnClickListener) itemsOnClick);
                        }
                    }
                });
    }

    @OnClick({R.id.et_shuru_yanzhengma, R.id.image_back, R.id.btn_ruzhu,R.id.image_zhengmian, R.id.image_fanmian, R.id.image_yingyezhizhao, R.id.et_shenqingren, R.id.et_phone, R.id.tv_huoqu_yanzhengma, R.id.et_dianpu_mingcheng, R.id.tv_xuanze_dianpu})
    public void onViewClicked(View view) {
        switch (view.getId()) {
//            返回按钮
            case R.id.image_back:
                finish();
                break;
//            申请人
            case R.id.et_shenqingren:
                break;
//            手机号
            case R.id.et_phone:

                break;
//            获取验证码
            case R.id.tv_huoqu_yanzhengma:
                if(ContractUtils.isChinaPhoneLegal(etPhone.getText().toString().trim()) == false){
                    Toast.makeText(this, "请输入正确的手机号", Toast.LENGTH_SHORT).show();
                    return;
                }
                if(etPhone.getText().toString().trim().equals("")){
                    Toast.makeText(this, "请输入手机号", Toast.LENGTH_SHORT).show();
                }else{
//                获取验证码的网络请求
                    initHuoQuYanZhengMaHttp();
                }


                break;
//            店铺名称
            case R.id.et_dianpu_mingcheng:
                break;
//            选择店铺类型
            case R.id.tv_xuanze_dianpu:
                if(list22 == null || list11 == null){
                    return;
                }

                if(list22.size() == 0){
                    Toast.makeText(this, "暂无数据！", Toast.LENGTH_SHORT).show();
                    return;
                }



                //将键盘下行
//                InputMethodManager imm = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
//                imm.toggleSoftInput(0, InputMethodManager.HIDE_NOT_ALWAYS);

                OptionsPickerView pvOptions = new  OptionsPickerView.Builder(this, new OptionsPickerView.OnOptionsSelectListener() {
                    @Override
                    public void onOptionsSelect(int options1, int option2, int options3 ,View v) {
                        //返回的分别是三个级别的选中位置
                        String tx = list22.get(options1);
//                        int i = Integer.parseInt(list11.get(options1));
                        dianpuid = list11.get(options1);
                        tvXuanzeDianpu.setText(tx);
                    }
                })
                        .setSubmitText("确定")//确定按钮文字
                        .setCancelText("取消")//取消按钮文字
                        .setTitleText("申请类型")//标题
                        .setSubCalSize(16)//确定和取消文字大小
                        .setTitleSize(16)//标题文字大小
                        .setTitleColor(Color.BLACK)//标题文字颜色
                        .setSubmitColor(Color.RED)//确定按钮文字颜色
                        .setCancelColor(Color.parseColor("#AEAAAA"))//取消按钮文字颜色
                        .setTitleBgColor(Color.parseColor("#f5f5f5"))//标题背景颜色 Night mode
                        .setBgColor(Color.parseColor("#ffffff"))//滚轮背景颜色 Night mode
                        .setContentTextSize(16)//滚轮文字大小
                        .setLinkage(false)//设置是否联动，默认true
                        .setLabels("", "", "")//设置选择的三级单位
                        .isCenterLabel(false) //是否只显示中间选中项的label文字，false则每项item全部都带有label。
                        .setCyclic(false, false, false)//循环与否
                        .setSelectOptions(1, 1, 1)  //设置默认选中项
                        .setOutSideCancelable(false)//点击外部dismiss default true
                        .isDialog(false)//是否显示为对话框样式
                        .build();

                pvOptions.setPicker(list22);//添加数据源
                pvOptions.show();


//                dianPuLeiXingPopupWindow.showAtLocation(findViewById(R.id.relative_shangjia_ruzhu), Gravity.BOTTOM | Gravity.CENTER_HORIZONTAL, 0, 0);
//
//                dianPuLeiXingPopupWindow.setDianPuLeiXingPopupWindowClick(new DianPuLeiXingPopupWindow.DianPuLeiXingPopupWindowClick() {
//                    @Override
//                    public void dianpuleixingPopupWindowClick(String title, String leixingid) {
//                        tvXuanzeDianpu.setText(title);
//                        lxid = leixingid;
//                    }
//                });


                break;
//            输入验证码
            case R.id.et_shuru_yanzhengma:
                break;
//            立即入驻
            case R.id.btn_ruzhu:

                if(etShenqingren.getText().toString().trim().equals("")){
                    Toast.makeText(this, "请输入申请人", Toast.LENGTH_SHORT).show();
                }else if(etPhone.getText().toString().trim().equals("")){
                    Toast.makeText(this, "请输入手机号", Toast.LENGTH_SHORT).show();
                }else if(etShuruYanzhengma.getText().toString().trim().equals("")){
                    Toast.makeText(this, "请输入验证码", Toast.LENGTH_SHORT).show();
                }else if(etDianpuMingcheng.getText().toString().trim().equals("")){
                    Toast.makeText(this, "请输入店铺名称", Toast.LENGTH_SHORT).show();
                }else if(tvXuanzeDianpu.getText().toString().trim().equals("")){
                    Toast.makeText(this, "请选择店铺类型", Toast.LENGTH_SHORT).show();
                }else if(etDianpuDizhi.getText().toString().equals("")){
                    Toast.makeText(this, "请输入店铺地址", Toast.LENGTH_SHORT).show();
                }else if(etDianpuJieshao.getText().toString().trim().equals("")){
                    Toast.makeText(this, "请输入店铺介绍", Toast.LENGTH_SHORT).show();
                }else if(etFarenName.getText().toString().equals("")){
                    Toast.makeText(this, "请输入法人姓名", Toast.LENGTH_SHORT).show();
//                }else if(ZhengMianEntity == null){
//                    Toast.makeText(this, "请上传身份证正面", Toast.LENGTH_SHORT).show();
//                }else if(FanMianEntity == null){
//                    Toast.makeText(this, "请上传身份证反面", Toast.LENGTH_SHORT).show();
//                }else if(YingYeZhiZhaoEntity == null){
//                    Toast.makeText(this, "请上传营业执照", Toast.LENGTH_SHORT).show();
                }else{
                    //      商家编辑立即入驻的网络请求
                    initShangJiaBianJiHttp();
                }

                break;
//            身份证正面
            case R.id.image_zhengmian:
                isOK = 0;
                Object itemsOnClick = null;
                takePhotoPopWin = new TakePhotoPopWin(ShangJiaRuZhuXiuGaiActivity.this,(View.OnClickListener) itemsOnClick);
                takePhotoPopWin.showAtLocation(findViewById(R.id.relative_shangjia_ruzhu), Gravity.BOTTOM | Gravity.CENTER_HORIZONTAL, 0, 0);





                takePhotoPopWin.setOnClickXiangji(new TakePhotoPopWin.OnClickXiangji() {
                    @Override
                    public void onClick(int id) {
//                        Intent open = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
//                        startActivityForResult(open,100);

                        //                判断当前SDK版本号
                        if (Build.VERSION.SDK_INT >= 23) {
//                    当前拨打电话权限是没有给的
                            if (ContextCompat.checkSelfPermission(ShangJiaRuZhuXiuGaiActivity.this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
//                        我要请求权限
                                ActivityCompat.requestPermissions(ShangJiaRuZhuXiuGaiActivity.this, new String[]{Manifest.permission.CAMERA}, 1);
                            } else {
                                if (Environment.MEDIA_MOUNTED.equals(Environment.getExternalStorageState())) {
                                    //创建文件夹 在本地文件Pictures 目录下创建 自己得文件夹
                                    File dir = new File(Environment.getExternalStorageDirectory()+ "/Pictures/image");
                                    if (!dir.exists()) {
                                        dir.mkdirs();
                                    }
                                    //照片名称
                                    File file = new File(dir, new SimpleDateFormat("yyyyMMddHHmmss").format(new Date()) + ".jpg");
                                    photoUri = Uri.parse(file.getPath());
                                    System.out.println(photoUri+"        photoUriphotoUri身份证正面身份证正面身份证正面1111");

                                    startActivityForResult(new Intent(MediaStore.ACTION_IMAGE_CAPTURE)
                                            .putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(file)), 100);
                                } else {
//                                    showToast("请检查SDCard！");
                                }
                            }
                        } else {

                            if (Environment.MEDIA_MOUNTED.equals(Environment.getExternalStorageState())) {
                                //创建文件夹 在本地文件Pictures 目录下创建 自己得文件夹
                                File dir = new File(Environment.getExternalStorageDirectory()+ "/Pictures/image");
                                if (!dir.exists()) {
                                    dir.mkdirs();
                                }
                                //照片名称
                                File file = new File(dir, new SimpleDateFormat("yyyyMMddHHmmss").format(new Date()) + ".jpg");
                                photoUri = Uri.parse(file.getPath());
                                System.out.println(photoUri+"        photoUriphotoUri身份证正面身份证正面身份证正面2222");

                                startActivityForResult(new Intent(MediaStore.ACTION_IMAGE_CAPTURE)
                                        .putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(file)), 100);

                            } else {
//                                    showToast("请检查SDCard！");
                            }
                        }
                    }
                });





                //                点击相册
                takePhotoPopWin.setOnClickXiangce(new TakePhotoPopWin.OnClickXiangce() {
                    @Override
                    public void onClick(int id) {
//                        Intent local = new Intent();
//                        local.setType("image/*");
//                        local.setAction(Intent.ACTION_GET_CONTENT);
//                        startActivityForResult(local, 2);

                        ImagePicker imagePicker = ImagePicker.getInstance();
                        imagePicker.setImageLoader(new GlideImageLoaderForPicker());   //设置图片加载器
                        imagePicker.setShowCamera(true);  //显示拍照按钮
                        imagePicker.setCrop(false);        //允许裁剪（单选才有效）
                        imagePicker.setSaveRectangle(true);//是否按矩形区域保存
                        imagePicker.setFocusHeight(169);
                        imagePicker.setFocusWidth(169);
                        imagePicker.setCrop(false);
                        imagePicker.setSelectLimit(1);    //选中数量限制
                        imagePicker.setOutPutX(800);//保存文件的宽度。单位像素
                        imagePicker.setOutPutY(800);//保存文件的高度。单位像素

                        Intent intent = new Intent(ShangJiaRuZhuXiuGaiActivity.this, ImageGridActivity.class);
                        startActivityForResult(intent, 1);

                    }
                });
                break;
//            身份证反面
            case R.id.image_fanmian:
                isOK = 1;
                itemsOnClick = null;
                takePhotoPopWin = new TakePhotoPopWin(ShangJiaRuZhuXiuGaiActivity.this,(View.OnClickListener) itemsOnClick);
                takePhotoPopWin.showAtLocation(findViewById(R.id.relative_shangjia_ruzhu), Gravity.BOTTOM | Gravity.CENTER_HORIZONTAL, 0, 0);




                takePhotoPopWin.setOnClickXiangji(new TakePhotoPopWin.OnClickXiangji() {
                    @Override
                    public void onClick(int id) {

                        //                判断当前SDK版本号
                        if (Build.VERSION.SDK_INT >= 23) {
//                    当前拨打电话权限是没有给的
                            if (ContextCompat.checkSelfPermission(ShangJiaRuZhuXiuGaiActivity.this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
//                        我要请求权限
                                ActivityCompat.requestPermissions(ShangJiaRuZhuXiuGaiActivity.this, new String[]{Manifest.permission.CAMERA}, 1);
                            } else {

                                if (Environment.MEDIA_MOUNTED.equals(Environment.getExternalStorageState())) {
                                    //创建文件夹 在本地文件Pictures 目录下创建 自己得文件夹
                                    File dir = new File(Environment.getExternalStorageDirectory()+ "/Pictures/image");
                                    if (!dir.exists()) {
                                        dir.mkdirs();
                                    }
                                    //照片名称
                                    File file = new File(dir, new SimpleDateFormat("yyyyMMddHHmmss").format(new Date()) + ".jpg");
                                    photoUri = Uri.parse(file.getPath());
                                    System.out.println(photoUri+"          phonturl身份证反面身份证反面身份证反面");

                                    startActivityForResult(new Intent(MediaStore.ACTION_IMAGE_CAPTURE)
                                            .putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(file)), 100);

                                } else {
//                                    showToast("请检查SDCard！");
                                }
                            }
                        } else {

                            if (Environment.MEDIA_MOUNTED.equals(Environment.getExternalStorageState())) {
                                //创建文件夹 在本地文件Pictures 目录下创建 自己得文件夹
                                File dir = new File(Environment.getExternalStorageDirectory()+ "/Pictures/image");
                                if (!dir.exists()) {
                                    dir.mkdirs();
                                }
                                //照片名称
                                File file = new File(dir, new SimpleDateFormat("yyyyMMddHHmmss").format(new Date()) + ".jpg");
                                photoUri = Uri.parse(file.getPath());

                                startActivityForResult(new Intent(MediaStore.ACTION_IMAGE_CAPTURE)
                                        .putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(file)), 100);

                            } else {
//                                    showToast("请检查SDCard！");
                            }
                        }
                    }
                });








                //                点击相册
                takePhotoPopWin.setOnClickXiangce(new TakePhotoPopWin.OnClickXiangce() {
                    @Override
                    public void onClick(int id) {
//                        Intent local = new Intent();
//                        local.setType("image/*");
//                        local.setAction(Intent.ACTION_GET_CONTENT);
//                        startActivityForResult(local, 2);

                        ImagePicker imagePicker = ImagePicker.getInstance();
                        imagePicker.setImageLoader(new GlideImageLoaderForPicker());   //设置图片加载器
                        imagePicker.setShowCamera(true);  //显示拍照按钮
                        imagePicker.setCrop(false);        //允许裁剪（单选才有效）
                        imagePicker.setSaveRectangle(true);//是否按矩形区域保存
                        imagePicker.setFocusHeight(169);
                        imagePicker.setFocusWidth(169);
                        imagePicker.setCrop(false);
                        imagePicker.setSelectLimit(1);    //选中数量限制
                        imagePicker.setOutPutX(800);//保存文件的宽度。单位像素
                        imagePicker.setOutPutY(800);//保存文件的高度。单位像素

                        Intent intent = new Intent(ShangJiaRuZhuXiuGaiActivity.this, ImageGridActivity.class);
                        startActivityForResult(intent, 1);

                    }
                });
                break;
//            、营业执照
            case R.id.image_yingyezhizhao:
                isOK = 2;
                itemsOnClick = null;
                takePhotoPopWin = new TakePhotoPopWin(ShangJiaRuZhuXiuGaiActivity.this,(View.OnClickListener) itemsOnClick);
                takePhotoPopWin.showAtLocation(findViewById(R.id.relative_shangjia_ruzhu), Gravity.BOTTOM | Gravity.CENTER_HORIZONTAL, 0, 0);


                takePhotoPopWin.setOnClickXiangji(new TakePhotoPopWin.OnClickXiangji() {
                    @Override
                    public void onClick(int id) {

                        //                判断当前SDK版本号
                        if (Build.VERSION.SDK_INT >= 23) {
//                    当前拨打电话权限是没有给的
                            if (ContextCompat.checkSelfPermission(ShangJiaRuZhuXiuGaiActivity.this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
//                        我要请求权限
                                ActivityCompat.requestPermissions(ShangJiaRuZhuXiuGaiActivity.this, new String[]{Manifest.permission.CAMERA}, 1);
                            } else {

                                if (Environment.MEDIA_MOUNTED.equals(Environment.getExternalStorageState())) {
                                    //创建文件夹 在本地文件Pictures 目录下创建 自己得文件夹
                                    File dir = new File(Environment.getExternalStorageDirectory()+ "/Pictures/image");
                                    if (!dir.exists()) {
                                        dir.mkdirs();
                                    }
                                    //照片名称
                                    File file = new File(dir, new SimpleDateFormat("yyyyMMddHHmmss").format(new Date()) + ".jpg");
                                    photoUri = Uri.parse(file.getPath());
                                    System.out.println(photoUri+"            photoUriphotoUri营业执照营业执照营业执照");

                                    startActivityForResult(new Intent(MediaStore.ACTION_IMAGE_CAPTURE)
                                            .putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(file)), 100);

                                } else {
//                                    showToast("请检查SDCard！");
                                }
                            }
                        } else {

                            if (Environment.MEDIA_MOUNTED.equals(Environment.getExternalStorageState())) {
                                //创建文件夹 在本地文件Pictures 目录下创建 自己得文件夹
                                File dir = new File(Environment.getExternalStorageDirectory()+ "/Pictures/image");
                                if (!dir.exists()) {
                                    dir.mkdirs();
                                }
                                //照片名称
                                File file = new File(dir, new SimpleDateFormat("yyyyMMddHHmmss").format(new Date()) + ".jpg");
                                photoUri = Uri.parse(file.getPath());

                                startActivityForResult(new Intent(MediaStore.ACTION_IMAGE_CAPTURE)
                                        .putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(file)), 100);

                            } else {
//                                    showToast("请检查SDCard！");
                            }
                        }
                    }
                });







                //                点击相册
                takePhotoPopWin.setOnClickXiangce(new TakePhotoPopWin.OnClickXiangce() {
                    @Override
                    public void onClick(int id) {
//                        Intent local = new Intent();
//                        local.setType("image/*");
//                        local.setAction(Intent.ACTION_GET_CONTENT);
//                        startActivityForResult(local, 2);

                        ImagePicker imagePicker = ImagePicker.getInstance();
                        imagePicker.setImageLoader(new GlideImageLoaderForPicker());   //设置图片加载器
                        imagePicker.setShowCamera(true);  //显示拍照按钮
                        imagePicker.setCrop(false);        //允许裁剪（单选才有效）
                        imagePicker.setSaveRectangle(true);//是否按矩形区域保存
                        imagePicker.setFocusHeight(169);
                        imagePicker.setFocusWidth(169);
                        imagePicker.setCrop(false);
                        imagePicker.setSelectLimit(1);    //选中数量限制
                        imagePicker.setOutPutX(800);//保存文件的宽度。单位像素
                        imagePicker.setOutPutY(800);//保存文件的高度。单位像素

                        Intent intent = new Intent(ShangJiaRuZhuXiuGaiActivity.this, ImageGridActivity.class);
                        startActivityForResult(intent, 1);

                    }
                });
                break;
        }
    }







//
//            if(shangJiaRuZhuXiuGaiEntity.getResponse().getNumber_Positive() != null){
//        if(shangJiaRuZhuXiuGaiEntity.getResponse().getNumber_Positive().contains(ContractUtils.YUANQUZHAOSHANG_PHOTO_URL)){
//            Glide.with(ShangJiaRuZhuXiuGaiActivity.this).load(shangJiaRuZhuXiuGaiEntity.getResponse().getNumber_Positive()).into(imageZhengmian);
//        }else{
//            Glide.with(ShangJiaRuZhuXiuGaiActivity.this).load(ContractUtils.YUANQUZHAOSHANG_PHOTO_URL+shangJiaRuZhuXiuGaiEntity.getResponse().getNumber_Positive()).into(imageZhengmian);
//        }
//
//    }
//
//
//                            if(shangJiaRuZhuXiuGaiEntity.getResponse().getNumber_side() != null){
//        if(shangJiaRuZhuXiuGaiEntity.getResponse().getNumber_side().contains(ContractUtils.YUANQUZHAOSHANG_PHOTO_URL)){
//            Glide.with(ShangJiaRuZhuXiuGaiActivity.this).load(shangJiaRuZhuXiuGaiEntity.getResponse().getNumber_side()).into(imageFanmian);
//        }else{
//            Glide.with(ShangJiaRuZhuXiuGaiActivity.this).load(ContractUtils.YUANQUZHAOSHANG_PHOTO_URL+shangJiaRuZhuXiuGaiEntity.getResponse().getNumber_side()).into(imageFanmian);
//        }
//    }
//
//
//
//                            if(shangJiaRuZhuXiuGaiEntity.getResponse().getLicense() != null){
//        if(shangJiaRuZhuXiuGaiEntity.getResponse().getLicense().contains(ContractUtils.YUANQUZHAOSHANG_PHOTO_URL)){
//            Glide.with(ShangJiaRuZhuXiuGaiActivity.this).load(shangJiaRuZhuXiuGaiEntity.getResponse().getLicense()).into(imageYingyezhizhao);
//        }else{
//            Glide.with(ShangJiaRuZhuXiuGaiActivity.this).load(ContractUtils.YUANQUZHAOSHANG_PHOTO_URL+shangJiaRuZhuXiuGaiEntity.getResponse().getLicense()).into(imageYingyezhizhao);
//        }
//    }


//    商家编辑立即入驻的网络请求
    private void initShangJiaBianJiHttp() {
            String zheng = "";
        if (ZhengMianEntity != null) {
            if (ZhengMianEntity.getResponse().getFileurl() != null) {
                zheng = ZhengMianEntity.getResponse().getFileurl();
            }
        } else {
            if(shangJiaRuZhuXiuGaiEntity.getResponse().getNumber_Positive().contains(ContractUtils.YUANQUZHAOSHANG_PHOTO_URL)){
                zheng = shangJiaRuZhuXiuGaiEntity.getResponse().getNumber_Positive();
            }else{
                zheng = ContractUtils.YUANQUZHAOSHANG_PHOTO_URL+shangJiaRuZhuXiuGaiEntity.getResponse().getNumber_Positive();
            }
        }

        String fanmian = "";
        if (FanMianEntity != null) {
            if (FanMianEntity.getResponse().getFileurl() != null) {
                fanmian = FanMianEntity.getResponse().getFileurl();
            }
        } else {
            if(shangJiaRuZhuXiuGaiEntity.getResponse().getNumber_side().contains(ContractUtils.YUANQUZHAOSHANG_PHOTO_URL)){
                fanmian = shangJiaRuZhuXiuGaiEntity.getResponse().getNumber_side();
            }else{
                fanmian = ContractUtils.YUANQUZHAOSHANG_PHOTO_URL+shangJiaRuZhuXiuGaiEntity.getResponse().getNumber_side();
            }
        }

        String yingyezhizhao = "";
        if (YingYeZhiZhaoEntity != null) {
            if (YingYeZhiZhaoEntity.getResponse().getFileurl() != null) {
                yingyezhizhao = YingYeZhiZhaoEntity.getResponse().getFileurl();
            }
        } else {
            if(shangJiaRuZhuXiuGaiEntity.getResponse().getLicense().contains(ContractUtils.YUANQUZHAOSHANG_PHOTO_URL)){
                yingyezhizhao = shangJiaRuZhuXiuGaiEntity.getResponse().getLicense();
            }else{
                yingyezhizhao = ContractUtils.YUANQUZHAOSHANG_PHOTO_URL+shangJiaRuZhuXiuGaiEntity.getResponse().getLicense();
            }
        }

        if(zheng.length() == 0){
            Toast.makeText(this, "请重新选择身份证正面", Toast.LENGTH_SHORT).show();
            return;
        }
        if(fanmian.length() == 0){
            Toast.makeText(this, "请重新选择身份证反面", Toast.LENGTH_SHORT).show();
            return;
        }
        if(yingyezhizhao.length() == 0){
            Toast.makeText(this, "请重新选择营业执照", Toast.LENGTH_SHORT).show();
        }


        if(shangJiaRuZhuDuanXinEntity == null){
            Toast.makeText(this, "请填写正确的验证码！", Toast.LENGTH_SHORT).show();
            return;
        }

//        String dianpu = String.valueOf(dianpuid);

        System.out.println(id+"          id");
        System.out.println(etShenqingren.getText().toString().trim()+"          Applicant_name");
        System.out.println(etDianpuMingcheng.getText().toString().trim()+"          name");
        System.out.println(etPhone.getText().toString().trim()+"          Applicant_phone");
        System.out.println(dianpuid+"          class");
        System.out.println(etDianpuDizhi.getText().toString().trim()+"          addres");
        System.out.println(etDianpuJieshao.getText().toString().trim()+"          introduce");
        System.out.println(etFarenName.getText().toString().trim()+"          legal_name");
        System.out.println(zheng+"          number_Positive");
        System.out.println(fanmian+"          number_side");
        System.out.println(yingyezhizhao+"          license");
        System.out.println(shangJiaRuZhuDuanXinEntity.getResponse().getAuthId()+"          authId");
        System.out.println(etShuruYanzhengma.getText().toString().trim()+"          code");




        final ProgressDialog progressDialog = new ProgressDialog(ShangJiaRuZhuXiuGaiActivity.this);
        progressDialog.setTitle("提示");
        progressDialog.setMessage("请等待...");
        progressDialog.setCancelable(false);
        progressDialog.show();

        OkHttpUtils.post()
                .url(ContractUtils.LOGIN_URL+"shop/Applyshop")
                .addHeader("token",ContractUtils.getTOKEN(ShangJiaRuZhuXiuGaiActivity.this))
                .addParams("id",id)
                .addParams("parkId",ContractUtils.getParkId(ShangJiaRuZhuXiuGaiActivity.this))
                .addParams("Applicant_name", etShenqingren.getText().toString().trim())
                .addParams("Applicant_phone", etPhone.getText().toString().trim())
                .addParams("name", etDianpuMingcheng.getText().toString().trim())
                .addParams("class",dianpuid+"")
                .addParams("addres", etDianpuDizhi.getText().toString().trim())
                .addParams("introduce", etDianpuJieshao.getText().toString().trim())
                .addParams("legal_name", etFarenName.getText().toString().trim())
                .addParams("number_Positive",zheng)
                .addParams("number_side",fanmian)
                .addParams("license",yingyezhizhao)
                .addParams("authId",shangJiaRuZhuDuanXinEntity.getResponse().getAuthId())
                .addParams("code",etShuruYanzhengma.getText().toString().trim())
                .build()
                .execute(new StringCallback() {
                    @Override
                    public void onError(Call call, Exception e, int id) {
                        progressDialog.dismiss();
                        System.out.println(e+"    eeee商家编辑");
                    }

                    @Override
                    public void onResponse(String response, int id) {
                        System.out.println(response+"           商家编辑立即入驻的网络请求");
                        ContractUtils.Code500(ShangJiaRuZhuXiuGaiActivity.this,response);
                        progressDialog.dismiss();
                        System.out.println(response+"        商家入驻的网络请求");
                        if(response.indexOf("200") != -1){
                            Toast.makeText(ShangJiaRuZhuXiuGaiActivity.this, "申请成功，等待审核！", Toast.LENGTH_SHORT).show();
                            finish();
                        }else{
                            Gson gson = new Gson();
                            ShangJiaRuZhuEntity shangJiaRuZhuEntity = gson.fromJson(response, ShangJiaRuZhuEntity.class);
                            Toast.makeText(ShangJiaRuZhuXiuGaiActivity.this, shangJiaRuZhuEntity.getMsg(), Toast.LENGTH_SHORT).show();
//
                        }
                    }
                });
    }












//
//    //    商家入驻的网络请求
//    private void initSahngJiaRuZhuHttp() {
//        if(ZhengMianEntity.getResponse().getFileurl().length() == 0){
//            Toast.makeText(this, "请重新选择身份证正面", Toast.LENGTH_SHORT).show();
//            return;
//        }
//        if(FanMianEntity.getResponse().getFileurl().length() == 0){
//            Toast.makeText(this, "请重新选择身份证反面", Toast.LENGTH_SHORT).show();
//            return;
//        }
//        if(YingYeZhiZhaoEntity.getResponse().getFileurl().length() == 0){
//            Toast.makeText(this, "请重新选择营业执照", Toast.LENGTH_SHORT).show();
//        }
//
//
//        if(shangJiaRuZhuDuanXinEntity == null){
//            Toast.makeText(this, "请填写正确的验证码！", Toast.LENGTH_SHORT).show();
//            return;
//        }

//
//        OkHttpUtils.post()
//                .url(ContractUtils.LOGIN_URL + "shop/Applyshop")
//                .addHeader("token", ContractUtils.getTOKEN(ShangJiaRuZhuXiuGaiActivity.this))
//                .addParams("parkId", ContractUtils.getParkId(ShangJiaRuZhuXiuGaiActivity.this))
//                .addParams("Applicant_name", etShenqingren.getText().toString().trim())
//                .addParams("Applicant_phone", etPhone.getText().toString().trim())
//                .addParams("name", etDianpuMingcheng.getText().toString().trim())
//                .addParams("class",lxid)
//                .addParams("addres", etDianpuDizhi.getText().toString().trim())
//                .addParams("introduce", etDianpuJieshao.getText().toString().trim())
//                .addParams("legal_name", etFarenName.getText().toString().trim())
//                .addParams("number_Positive",ZhengMianEntity.getResponse().getFileurl())
//                .addParams("number_side",FanMianEntity.getResponse().getFileurl())
//                .addParams("license",YingYeZhiZhaoEntity.getResponse().getFileurl())
//                .addParams("authId",shangJiaRuZhuDuanXinEntity.getResponse().getAuthId())
//                .addParams("code",etShuruYanzhengma.getText().toString().trim())
//                .build()
//                .execute(new StringCallback() {
//                    @Override
//                    public void onError(Call call, Exception e, int id) {
//
//                    }
//
//                    @Override
//                    public void onResponse(String response, int id) {
//                        ContractUtils.Code500(ShangJiaRuZhuXiuGaiActivity.this,response);
//                        System.out.println(response+"        商家入驻的网络请求");
//                        if(response.indexOf("200") != -1){
//                            Toast.makeText(ShangJiaRuZhuXiuGaiActivity.this, "申请成功，等待审核！", Toast.LENGTH_SHORT).show();
//                            finish();
//
////                            Thread myThread=new Thread(){//创建子线程
////                                @Override
////                                public void run() {
////                                    try{
////                                        sleep(1500);//使程序休眠五秒
////                                        finish();
////                                    }catch (Exception e){
////                                        e.printStackTrace();
////                                    }
////                                }
////                            };
////                            myThread.start();//启动线程
//                        }else{
//                            Gson gson = new Gson();
//                            ShangJiaRuZhuEntity shangJiaRuZhuEntity = gson.fromJson(response, ShangJiaRuZhuEntity.class);
//                            Toast.makeText(ShangJiaRuZhuXiuGaiActivity.this, shangJiaRuZhuEntity.getMsg(), Toast.LENGTH_SHORT).show();
////
//                        }
//                    }
//                });
//    }
//


    //    商家入驻获取验证码的网络请求
    private void initHuoQuYanZhengMaHttp() {
        if(ContractUtils.isChinaPhoneLegal(etPhone.getText().toString().trim()) == false){
            Toast.makeText(this, "请输入正确的手机号", Toast.LENGTH_SHORT).show();
            return;
        }
        ContractUtils.isChinaPhoneLegal(etPhone.getText().toString().trim());

        OkHttpUtils.post()
                .url(ContractUtils.LOGIN_URL + "shop/sendsms")
                .addHeader("token", ContractUtils.getTOKEN(ShangJiaRuZhuXiuGaiActivity.this))
                .addParams("phone", etPhone.getText().toString().trim()) // 输入框上的手机号
                .build()
                .execute(new StringCallback() {
                    @Override
                    public void onError(Call call, Exception e, int id) {
                        System.out.println(e + "    s商家入驻eee");
                    }

                    @Override
                    public void onResponse(String response, int id) {
                        ContractUtils.Code500(ShangJiaRuZhuXiuGaiActivity.this,response);
                        System.out.println(response + "        商家入驻获取验证码的网络请求");
                        if (response.indexOf("200") != -1) {

                            new Thread() {
                                @Override
                                public void run() {
                                    super.run();
                                    try {
                                        num = 60;
                                        while (num > 0) {
//                                           不可点击
                                            tvHuoquYanzhengma.setClickable(false);
                                            runOnUiThread(new Runnable() {
                                                @Override
                                                public void run() {
//                                        tvHuoquYanzhengma.setBackgroundResource(R.drawable.shape_huoqu_huise);
//                                        tvHuoquYanzhengma.setText("倒计时"+num+"s");
                                                    tvHuoquYanzhengma.setText(num + "s后重新获取");
                                                }
                                            });

                                            Thread.sleep(1000);
                                            num = num - 1;
                                        }

                                        runOnUiThread(new Runnable() {
                                            @Override
                                            public void run() {
//                                    tvHuoquYanzhengma.setBackgroundResource(R.drawable.shape_huoqu);
                                                tvHuoquYanzhengma.setClickable(true);
                                                tvHuoquYanzhengma.setText("重新获取");
                                            }
                                        });
                                    } catch (InterruptedException e) {
                                        e.printStackTrace();
                                    }
                                }
                            }.start();
                            Gson gson = new Gson();
                            shangJiaRuZhuDuanXinEntity = gson.fromJson(response, ShangJiaRuZhuDuanXinEntity.class);
                            Toast.makeText(ShangJiaRuZhuXiuGaiActivity.this, shangJiaRuZhuDuanXinEntity.getMsg(), Toast.LENGTH_SHORT).show();
                        }else if(response.indexOf("400") != -1){
                            Gson gson = new Gson();
                            DuanXinShiBaiEntity duanXinShiBaiEntity = gson.fromJson(response, DuanXinShiBaiEntity.class);
                            Toast.makeText(ShangJiaRuZhuXiuGaiActivity.this, duanXinShiBaiEntity.getMsg(), Toast.LENGTH_SHORT).show();
                        }
                    }
                });
    }









    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode == 100){
            String string = photoUri.toString();
            System.out.println(photoUri+"           photoUri");

            File file = new File(string);
            System.out.println(file.exists()+"      file");
            if(file.exists() == false){
                return;
            }

            if(isOK == 0){
                imageZhengmian.setImageURI(Uri.parse(string));
            }else if(isOK == 1){
                imageFanmian.setImageURI(Uri.parse(string));
            }else if(isOK ==2){
                imageYingyezhizhao.setImageURI(Uri.parse(string));
            }
//            imageTouxiang.setImageURI(Uri.parse(string));
            uploadFile1(string);

        }






        if (data != null && requestCode == 1) {
            ArrayList<ImageItem> images = (ArrayList<ImageItem>) data.getSerializableExtra(ImagePicker.EXTRA_RESULT_ITEMS);
            if (images != null) {
                if(isOK == 0){
                    imageZhengmian.setImageURI(Uri.parse(images.get(0).path));
                }else if(isOK == 1){
                    imageFanmian.setImageURI(Uri.parse(images.get(0).path));
                }else if(isOK ==2){
                    imageYingyezhizhao.setImageURI(Uri.parse(images.get(0).path));
                }
//                imageZhengmian.setImageURI(Uri.parse(images.get(0).path));
                uploadFile(images.get(0));


            } else {
//                Toast.makeText(this, "图片解析失败", Toast.LENGTH_SHORT).show();
            }
        } else {
//            Toast.makeText(this, "没有数据", Toast.LENGTH_SHORT).show();
        }
    }





    private void uploadFile1(String imageItem) {
        final ProgressDialog progressDialog = new ProgressDialog(ShangJiaRuZhuXiuGaiActivity.this);
        progressDialog.setTitle("提示");
        progressDialog.setMessage("请等待...");
        progressDialog.setCancelable(false);
        progressDialog.show();

        File file = new File(imageItem);
        Luban.with(this)
                .load(file)
                .ignoreBy(100)
                //  .setTargetDir(AppConfig.DEFAULT_SAVE_IMAGE_PATH)
                .setCompressListener(new OnCompressListener() {
                    @Override
                    public void onStart() {
                        // TODO 压缩开始前调用，可以在方法内启动 loading UI
                    }

                    @Override
                    public void onSuccess(File file) {
//                        String url = ContractUtils.LOGIN_URL+"ceshi/ce_upload.html";
                        OkHttpClient okHttpClient = new OkHttpClient();
//                        RequestBody requestBody1 =new FormBody.Builder()
//                                .add("driverid",substring)
//                                .add("driver_img_type","1")
//                                .build();
                        RequestBody requestBody = new MultipartBody.Builder()
                                .setType(MultipartBody.FORM)
                                .addFormDataPart("file", file.getName(), RequestBody.create(MediaType.parse("image/*"), file))
                                .build();

                        Request request = new Request.Builder()
                                .url(ContractUtils.LOGIN_URL+"index/upload")
                                .addHeader("token",ContractUtils.getTOKEN(ShangJiaRuZhuXiuGaiActivity.this))
                                .post(requestBody)
                                .build();

                        okHttpClient.newCall(request).enqueue(new Callback() {
                            @Override
                            public void onFailure(Call call, IOException e) {
                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
//                                        Toast.makeText(ShangJiaRuZhuActivity.this, "网络异常", Toast.LENGTH_SHORT).show();
                                    }
                                });
                                progressDialog.dismiss();
                            }

                            @Override
                            public void onResponse(Call call, final Response response) throws IOException {
                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
//                                        Toast.makeText(ShangJiaRuZhuActivity.this, "上传成功", Toast.LENGTH_SHORT).show();
                                    }
                                });


                                progressDialog.dismiss();
                                String string = response.body().string();
                                Gson gson = new Gson();
//                                photoEntity = gson.fromJson(string, PhotoEntity.class);
                                if(isOK == 0){
                                    ZhengMianEntity = gson.fromJson(string, PhotoEntity.class);
                                }else if(isOK == 1){
                                    FanMianEntity =  gson.fromJson(string, PhotoEntity.class);
                                }else if(isOK ==2){
                                    YingYeZhiZhaoEntity =  gson.fromJson(string, PhotoEntity.class);;
                                }

                                System.out.println(string + "     我的资料上传头像");
                            }
                        });
                    }

                    @Override
                    public void onError(Throwable e) {

                    }
                }).launch();
    }







    private void uploadFile(ImageItem imageItem) {
        final ProgressDialog progressDialog = new ProgressDialog(ShangJiaRuZhuXiuGaiActivity.this);
        progressDialog.setTitle("提示");
        progressDialog.setMessage("请等待...");
        progressDialog.setCancelable(false);
        progressDialog.show();

        File file = new File(imageItem.path);
        Luban.with(this)
                .load(file)
                .ignoreBy(100)
                //  .setTargetDir(AppConfig.DEFAULT_SAVE_IMAGE_PATH)
                .setCompressListener(new OnCompressListener() {
                    @Override
                    public void onStart() {
                        // TODO 压缩开始前调用，可以在方法内启动 loading UI
                    }

                    @Override
                    public void onSuccess(File file) {
//                        String url = ContractUtils.LOGIN_URL+"ceshi/ce_upload.html";
                        OkHttpClient okHttpClient = new OkHttpClient();
//                        RequestBody requestBody1 =new FormBody.Builder()
//                                .add("driverid",substring)
//                                .add("driver_img_type","1")
//                                .build();
                        RequestBody requestBody = new MultipartBody.Builder()
                                .setType(MultipartBody.FORM)
                                .addFormDataPart("file", file.getName(), RequestBody.create(MediaType.parse("image/*"), file))
                                .build();

                        Request request = new Request.Builder()
                                .url(ContractUtils.LOGIN_URL+"index/upload")
                                .addHeader("token",ContractUtils.getTOKEN(ShangJiaRuZhuXiuGaiActivity.this))
                                .post(requestBody)
                                .build();

                        okHttpClient.newCall(request).enqueue(new Callback() {
                            @Override
                            public void onFailure(Call call, IOException e) {
                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
//                                        Toast.makeText(ShangJiaRuZhuActivity.this, "网络异常", Toast.LENGTH_SHORT).show();
                                    }
                                });
                                progressDialog.dismiss();
                            }

                            @Override
                            public void onResponse(Call call, final Response response) throws IOException {
                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
//                                        Toast.makeText(PeopleInfoActivity.this, "上传成功", Toast.LENGTH_SHORT).show();
                                    }
                                });


                                progressDialog.dismiss();
                                String string = response.body().string();
                                Gson gson = new Gson();
//                                photoEntity = gson.fromJson(string, PhotoEntity.class);
                                if(isOK == 0){
                                    ZhengMianEntity = gson.fromJson(string, PhotoEntity.class);
                                }else if(isOK == 1){
                                    FanMianEntity =  gson.fromJson(string, PhotoEntity.class);
                                }else if(isOK ==2){
                                    YingYeZhiZhaoEntity =  gson.fromJson(string, PhotoEntity.class);;
                                }
                                System.out.println(string + "    上传身份证");
                            }
                        });
                    }

                    @Override
                    public void onError(Throwable e) {

                    }
                }).launch();
    }









    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case 1:
                if (permissions.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
//                    SimpleDateFormat timeStampFormat = new SimpleDateFormat("yyyyMMddHHmmss");
//                    String filename = timeStampFormat.format(new Date());
//                    ContentValues values = new ContentValues(); //使用本地相册保存拍摄照片
//                    values.put(MediaStore.Images.Media.TITLE, filename);
//                    photoUri = getContentResolver().insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);
//
//                    // 设置 intent.putExtra(MediaStore.EXTRA_OUTPUT, photoUri); photoUri = 保存图片得uri
//                    Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
//                    intent.putExtra(MediaStore.EXTRA_OUTPUT, photoUri);
//                    startActivityForResult(intent, 100);

                    if (Environment.MEDIA_MOUNTED.equals(Environment.getExternalStorageState())) {
                        //创建文件夹 在本地文件Pictures 目录下创建 自己得文件夹
                        File dir = new File(Environment.getExternalStorageDirectory()+ "/Pictures/image");
                        if (!dir.exists()) {
                            dir.mkdirs();
                        }
                        //照片名称
                        File file = new File(dir, new SimpleDateFormat("yyyyMMddHHmmss").format(new Date()) + ".jpg");
                        photoUri = Uri.parse(file.getPath());
                        System.out.println("88888888888888888888888888888888888");

                        startActivityForResult(new Intent(MediaStore.ACTION_IMAGE_CAPTURE)
                                .putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(file)), 100);

                    } else {
//                                    showToast("请检查SDCard！");
                    }
                } else {
//                    Toast.makeText(WoDeZiLiaoActivity.this, "", Toast.LENGTH_SHORT).show();
                }
                break;
        }
    }












}
