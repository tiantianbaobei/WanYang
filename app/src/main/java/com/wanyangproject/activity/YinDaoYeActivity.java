package com.wanyangproject.activity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;

import com.wanyangproject.R;

import com.wanyangproject.adapter.YiDaoYeAdapter;
import com.wanyangproject.base.MyApp;
import com.wanyangproject.utils.SharedPreferencesYinDaoYe;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class YinDaoYeActivity extends AppCompatActivity implements ViewPager.OnPageChangeListener {

    @BindView(R.id.viewPager)
    ViewPager viewPager;
    private List<View> list = new ArrayList<>();
    private String yindaoye;
    private Boolean anzhuang = true;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_yin_dao_ye);
        ButterKnife.bind(this);

            View viewOne = LayoutInflater.from(this).inflate(R.layout.item_yindaoye_one, null);
            View viewTwo = LayoutInflater.from(this).inflate(R.layout.item_yindaoye_two, null);
            View viewThree = LayoutInflater.from(this).inflate(R.layout.item_yindaoye_three, null);
            View viewFour = LayoutInflater.from(this).inflate(R.layout.item_yindaoye_four, null);

            list.add(viewOne);
            list.add(viewTwo);
            list.add(viewThree);
            list.add(viewFour);

        viewOne.findViewById(R.id.tv_tiaoguo).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(YinDaoYeActivity.this,LoginActivity.class);
                    startActivity(intent);
                    SharedPreferencesYinDaoYe.saveyindaoye(MyApp.getContext(),"yindaoye");
                    finish();
                }
            });


        viewTwo.findViewById(R.id.tv_tiaoguo).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(YinDaoYeActivity.this,LoginActivity.class);
                startActivity(intent);
                SharedPreferencesYinDaoYe.saveyindaoye(MyApp.getContext(),"yindaoye");
                finish();
            }
        });



        viewThree.findViewById(R.id.tv_tiaoguo).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(YinDaoYeActivity.this,LoginActivity.class);
                startActivity(intent);
                SharedPreferencesYinDaoYe.saveyindaoye(MyApp.getContext(),"yindaoye");
                finish();
            }
        });



        viewFour.findViewById(R.id.tv_tiaoguo).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(YinDaoYeActivity.this,LoginActivity.class);
                startActivity(intent);
                SharedPreferencesYinDaoYe.saveyindaoye(MyApp.getContext(),"yindaoye");
                finish();
            }
        });



        viewFour.findViewById(R.id.tv_jinru).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(YinDaoYeActivity.this,LoginActivity.class);
                    startActivity(intent);
                    SharedPreferencesYinDaoYe.saveyindaoye(MyApp.getContext(),"yindaoye");
                    finish();
                }
            });


            YiDaoYeAdapter adapter = new YiDaoYeAdapter(list);
            viewPager.setAdapter(adapter);

            viewPager.addOnPageChangeListener(this);
        }






//    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {

    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }
}
