package com.wanyangproject.activity;

import android.app.ProgressDialog;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.wanyangproject.R;
import com.wanyangproject.entity.HuoQuYanZhengMaEntity;
import com.wanyangproject.entity.WangJiMiMaEntity;
import com.wanyangproject.utils.ContractUtils;
import com.zhy.http.okhttp.OkHttpUtils;
import com.zhy.http.okhttp.callback.StringCallback;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.Call;

public class WangJiMiMaActivity extends AppCompatActivity {

    @BindView(R.id.tv_register_title)
    TextView tvRegisterTitle;
    @BindView(R.id.image_back)
    ImageView imageBack;
    @BindView(R.id.relative_title)
    RelativeLayout relativeTitle;
    @BindView(R.id.et_phone)
    EditText etPhone;
    @BindView(R.id.image_delete)
    ImageView imageDelete;
    @BindView(R.id.view_phone)
    View viewPhone;
    @BindView(R.id.relative_phone)
    RelativeLayout relativePhone;
    @BindView(R.id.et_duanxin_yanzhengma)
    EditText etDuanxinYanzhengma;
    @BindView(R.id.tv_huoqu_yanzhengma)
    TextView tvHuoquYanzhengma;
    @BindView(R.id.view_duanxin_yanzhengma)
    View viewDuanxinYanzhengma;
    @BindView(R.id.relative_duanxin_yanzhengma)
    RelativeLayout relativeDuanxinYanzhengma;
    @BindView(R.id.et_mima_one)
    EditText etMimaOne;
    @BindView(R.id.image_eye_one)
    ImageView imageEyeOne;
    @BindView(R.id.view_mima_one)
    View viewMimaOne;
    @BindView(R.id.relative_mima_one)
    RelativeLayout relativeMimaOne;
    @BindView(R.id.et_mima_two)
    EditText etMimaTwo;
    @BindView(R.id.image_eye_two)
    ImageView imageEyeTwo;
    @BindView(R.id.view_mima_two)
    View viewMimaTwo;
    @BindView(R.id.relative_mima_two)
    RelativeLayout relativeMimaTwo;
    @BindView(R.id.tv_tijiao)
    TextView tvTijiao;
    private Boolean isOK = false;
    private HuoQuYanZhengMaEntity huoQuYanZhengMaEntity;
    private int num;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wang_ji_mi_ma);
        ButterKnife.bind(this);
        if (Build.VERSION.SDK_INT >= 21) {
            View dview = getWindow().getDecorView();
            dview.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN | View.SYSTEM_UI_FLAG_LAYOUT_STABLE);
            getWindow().setStatusBarColor(Color.TRANSPARENT);
        }




        etPhone.addTextChangedListener(new TextWatcher() {
            @Override  //   输入文字之前的状态
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override // 输入文字中的状态
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override  // 输入文字之后的状态
            public void afterTextChanged(Editable editable) {
                imageDelete.setVisibility(View.VISIBLE);
                if(etPhone.getText().toString().trim().equals("") || etDuanxinYanzhengma.getText().toString().trim().equals("") || etMimaOne.getText().toString().trim().equals("") || etMimaTwo.getText().toString().trim().equals("")){
                    tvTijiao.setBackgroundResource(R.drawable.shape_wangjimima);
                }else{
                    tvTijiao.setBackgroundResource(R.drawable.shape_login);
                }

            }
        });










        etDuanxinYanzhengma.addTextChangedListener(new TextWatcher() {
            @Override  //   输入文字之前的状态
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override // 输入文字中的状态
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override  // 输入文字之后的状态
            public void afterTextChanged(Editable editable) {
                if(etPhone.getText().toString().trim().equals("") || etDuanxinYanzhengma.getText().toString().trim().equals("") || etMimaOne.getText().toString().trim().equals("") || etMimaTwo.getText().toString().trim().equals("")){
                    tvTijiao.setBackgroundResource(R.drawable.shape_wangjimima);
                }else{
                    tvTijiao.setBackgroundResource(R.drawable.shape_login);
                }


            }
        });



        etMimaOne.addTextChangedListener(new TextWatcher() {
            @Override  //   输入文字之前的状态
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override // 输入文字中的状态
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override  // 输入文字之后的状态
            public void afterTextChanged(Editable editable) {
                if(etPhone.getText().toString().trim().equals("") || etDuanxinYanzhengma.getText().toString().trim().equals("") || etMimaOne.getText().toString().trim().equals("") || etMimaTwo.getText().toString().trim().equals("")){
                    tvTijiao.setBackgroundResource(R.drawable.shape_wangjimima);
                }else{
                    tvTijiao.setBackgroundResource(R.drawable.shape_login);
                }


            }
        });



        etMimaTwo.addTextChangedListener(new TextWatcher() {
            @Override  //   输入文字之前的状态
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override // 输入文字中的状态
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override  // 输入文字之后的状态
            public void afterTextChanged(Editable editable) {

                if(etPhone.getText().toString().trim().equals("") || etDuanxinYanzhengma.getText().toString().trim().equals("") || etMimaOne.getText().toString().trim().equals("") || etMimaTwo.getText().toString().trim().equals("")){
                    tvTijiao.setBackgroundResource(R.drawable.shape_wangjimima);
                }else{
                    tvTijiao.setBackgroundResource(R.drawable.shape_login);
                }

            }
        });







    }

    @OnClick({R.id.tv_register_title, R.id.image_back, R.id.relative_title, R.id.et_phone, R.id.image_delete, R.id.view_phone, R.id.relative_phone, R.id.et_duanxin_yanzhengma, R.id.tv_huoqu_yanzhengma, R.id.view_duanxin_yanzhengma, R.id.relative_duanxin_yanzhengma, R.id.et_mima_one, R.id.image_eye_one, R.id.view_mima_one, R.id.relative_mima_one, R.id.et_mima_two, R.id.image_eye_two, R.id.view_mima_two, R.id.relative_mima_two, R.id.tv_tijiao})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.image_back:
                finish();
                break;
            case R.id.et_phone:
                break;
            case R.id.image_delete:
                etPhone.setText("");
                break;
            case R.id.relative_phone:
                break;
            case R.id.et_duanxin_yanzhengma:
                break;
            case R.id.tv_huoqu_yanzhengma:
                if(ContractUtils.isChinaPhoneLegal(etPhone.getText().toString().trim()) == false){
                    Toast.makeText(this, "请输入正确的手机号", Toast.LENGTH_SHORT).show();
                    return;
                }
                if(etPhone.getText().toString().trim().equals("")) {
                    Toast.makeText(this, "请输入手机号", Toast.LENGTH_SHORT).show();
                }else{
                    //                获取验证码的网络请求
                    initHuoQuYanZhengMaHttp();
                }







                break;
            case R.id.et_mima_one:
                break;
            case R.id.image_eye_one:
                if (isOK) {
//                    如果选中显示密码
                    imageEyeOne.setImageResource(R.mipmap.open_eye);
                    etMimaOne.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
                    isOK = false;
                } else {
//                    否则隐藏密码
                    imageEyeOne.setImageResource(R.mipmap.close_eye);
                    etMimaOne.setTransformationMethod(PasswordTransformationMethod.getInstance());
                    isOK = true;
                }
                break;
            case R.id.et_mima_two:
                break;
            case R.id.image_eye_two:
                if (isOK) {
//                    如果选中显示密码
                    imageEyeTwo.setImageResource(R.mipmap.open_eye);
                    etMimaTwo.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
                    isOK = false;
                } else {
//                    否则隐藏密码
                    imageEyeTwo.setImageResource(R.mipmap.close_eye);
                    etMimaTwo.setTransformationMethod(PasswordTransformationMethod.getInstance());
                    isOK = true;
                }
                break;
            case R.id.tv_tijiao:
//                if(){
//
//                }
                if(etPhone.getText().toString().toString().equals("")){
                    Toast.makeText(this, "请填写手机号", Toast.LENGTH_SHORT).show();
                }else if(etDuanxinYanzhengma.getText().toString().equals("")){
                    Toast.makeText(this, "请填写短信验证码", Toast.LENGTH_SHORT).show();
                }else if(etMimaOne.getText().toString().equals("")){
                    Toast.makeText(this, "请填写新密码", Toast.LENGTH_SHORT).show();
                }else if(etMimaTwo.getText().toString().equals("")){
                    Toast.makeText(this, "请确认新密码", Toast.LENGTH_SHORT).show();
                }else {
                    //                忘记密码的网络请求
                    initWangJiMiMaHttp();
                }

                break;
        }
    }

//    获取验证码网络请求
    private void initHuoQuYanZhengMaHttp() {
        OkHttpUtils.post()
                .url(ContractUtils.LOGIN_URL+"index/sendsmswei")
                .addParams("phone",etPhone.getText().toString().trim())
                .build()
                .execute(new StringCallback() {
                    @Override
                    public void onError(Call call, Exception e, int id) {

                    }

                    @Override
                    public void onResponse(String response, int id) {
                        ContractUtils.Code500(WangJiMiMaActivity.this,response);
                        System.out.println(response+"    忘记密码短信验证码接口");
                        if(response.indexOf("200") != -1){

                            new Thread() {
                                @Override
                                public void run() {
                                    super.run();
                                    try {
                                        num = 60;
                                        while (num > 0) {
//                                不可点击
                                            tvHuoquYanzhengma.setClickable(false);
                                            runOnUiThread(new Runnable() {
                                                @Override
                                                public void run() {
//                                        tvHuoquYanzhengma.setBackgroundResource(R.drawable.shape_huoqu_huise);
//                                        tvHuoquYanzhengma.setText("倒计时"+num+"s");
                                                    tvHuoquYanzhengma.setText(num+"s后重新获取");
                                                }
                                            });

                                            Thread.sleep(1000);
                                            num = num - 1;

                                        }

                                        runOnUiThread(new Runnable() {
                                            @Override
                                            public void run() {
//                                    tvHuoquYanzhengma.setBackgroundResource(R.drawable.shape_huoqu);
                                                tvHuoquYanzhengma.setClickable(true);
                                                tvHuoquYanzhengma.setText("重新获取");
                                            }
                                        });

                                    } catch (InterruptedException e) {
                                        e.printStackTrace();
                                    }

                                }
                            }.start();
                            Gson gson = new Gson();
                            huoQuYanZhengMaEntity = gson.fromJson(response, HuoQuYanZhengMaEntity.class);
                        }else{
                            ContractUtils.Code400(WangJiMiMaActivity.this,response);
                        }


                    }
                });
    }

    //    忘记密码的网络请求
    private void initWangJiMiMaHttp() {

        if(ContractUtils.isChinaPhoneLegal(etPhone.getText().toString().trim()) == false){
            Toast.makeText(this, "请输入正确的手机号", Toast.LENGTH_SHORT).show();
            return;
        }
        ContractUtils.isChinaPhoneLegal(etPhone.getText().toString().trim());


        if(!(etMimaOne.getText().toString().trim().length() >= 6 && etMimaOne.getText().toString().trim().length() <= 20)){
            Toast.makeText(this, "请输入6-20位的密码", Toast.LENGTH_SHORT).show();
            return;
        }
        if( !etMimaTwo.getText().toString().trim().equals(etMimaOne.getText().toString().trim()) ){
            Toast.makeText(this, "两次密码输入不一样", Toast.LENGTH_SHORT).show();
            return;
        }


        if(huoQuYanZhengMaEntity == null){
            Toast.makeText(this, "请填写正确的验证码！", Toast.LENGTH_SHORT).show();
            return;
        }





        final ProgressDialog progressDialog = new ProgressDialog(WangJiMiMaActivity.this);
        progressDialog.setTitle("提示");
        progressDialog.setMessage("请等待...");
        progressDialog.setCancelable(false);
        progressDialog.show();
        OkHttpUtils.post()
                .url(ContractUtils.LOGIN_URL+"user/findPwd")
                .addParams("phone",etPhone.getText().toString().trim())
                .addParams("authId",huoQuYanZhengMaEntity.getResponse().getAuthId())
                .addParams("code",etDuanxinYanzhengma.getText().toString().trim())
                .addParams("password",etMimaOne.getText().toString().trim())
                .build()
                .execute(new StringCallback() {
                    @Override
                    public void onError(Call call, Exception e, int id) {
                        progressDialog.dismiss();
                    }

                    @Override
                    public void onResponse(String response, int id) {
                        ContractUtils.Code500(WangJiMiMaActivity.this,response);
                        System.out.println(response+"    忘记密码的网络请求");
                        progressDialog.dismiss();

                        if(response.indexOf("200") != -1){
                            Gson gson = new Gson();
                            WangJiMiMaEntity wangJiMiMaEntity = gson.fromJson(response, WangJiMiMaEntity.class);
                            Toast.makeText(WangJiMiMaActivity.this, wangJiMiMaEntity.getMsg(), Toast.LENGTH_SHORT).show();
                            finish();
                        }else{
                            ContractUtils.Code400(WangJiMiMaActivity.this,response);
                        }
                    }
                });
    }
}
