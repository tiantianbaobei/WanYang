package com.wanyangproject.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.wanyangproject.R;
import com.wanyangproject.entity.FenLeititleEntity;
import com.wanyangproject.entity.LiuYanFanKuiEntity;
import com.wanyangproject.shouye.ZhengCeFaGuiActivity;
import com.wanyangproject.utils.ContractUtils;
import com.zhy.http.okhttp.OkHttpUtils;
import com.zhy.http.okhttp.callback.StringCallback;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.Call;

public class ZhengCeFaGuiFenLeiActivity extends AppCompatActivity {

    @BindView(R.id.image_back)
    ImageView imageBack;
    @BindView(R.id.tv_title)
    TextView tvTitle;
    @BindView(R.id.tv_xiangqing)
    TextView tvXiangqing;
    @BindView(R.id.et_liuyan_neirong)
    EditText etLiuyanNeirong;
    @BindView(R.id.tv_tijiao)
    TextView tvTijiao;
    @BindView(R.id.phone)
    RelativeLayout phone;
    @BindView(R.id.tv_address)
    TextView tvAddress;
    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    private String id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_zheng_ce_fa_gui_fen_lei);
        ButterKnife.bind(this);


        Intent intent = getIntent();
        id = intent.getStringExtra("id");


//        政策法规标题的网路请求
        initBiaoTiHttp();

    }







    //    政策法规题的网路请求
    private void initBiaoTiHttp() {
        OkHttpUtils.post()
                .url(ContractUtils.LOGIN_URL+"news/getCateInfo")
                .addHeader("token",ContractUtils.getTOKEN(ZhengCeFaGuiFenLeiActivity.this))
                .addParams("typeId",id)
                .build()
                .execute(new StringCallback() {
                    @Override
                    public void onError(Call call, Exception e, int id) {

                    }

                    @Override
                    public void onResponse(String response, int id) {
                        System.out.println(response+"        政策法规标题的网路请求 ");
                        ContractUtils.Code500(ZhengCeFaGuiFenLeiActivity.this,response);
                        if(response.indexOf("200") != -1){
                            Gson gson = new Gson();
                            FenLeititleEntity fenLeititleEntity = gson.fromJson(response, FenLeititleEntity.class);
                            if(fenLeititleEntity.getResponse().getInfo().getTitle() == null){

                            }else{
                                tvTitle.setText(fenLeititleEntity.getResponse().getInfo().getTitle());
                            }
                        }
                    }
                });
    }






    @OnClick({R.id.image_back, R.id.tv_title, R.id.tv_xiangqing, R.id.et_liuyan_neirong, R.id.tv_tijiao, R.id.phone, R.id.tv_address, R.id.recyclerView})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.image_back:
                finish();
                break;
            case R.id.tv_title:
                break;
            case R.id.tv_xiangqing:
                break;
            case R.id.et_liuyan_neirong:
                break;
            case R.id.tv_tijiao:
                if(etLiuyanNeirong.getText().toString().trim().equals("")){
                    Toast.makeText(this, "请输入留言内容", Toast.LENGTH_SHORT).show();
                }else{
//                    政策法规留言的网络请求
                    initLiuYanHttp();
                }
                break;
            case R.id.phone:
                break;
            case R.id.tv_address:
                break;
            case R.id.recyclerView:
                break;
        }
    }


//    政策法规留言的网络请求
    private void initLiuYanHttp() {
        OkHttpUtils.post()
                .url(ContractUtils.LOGIN_URL+"news/feedback")
                .addHeader("token",ContractUtils.getTOKEN(ZhengCeFaGuiFenLeiActivity.this))
                .addParams("parkId",ContractUtils.getParkId(ZhengCeFaGuiFenLeiActivity.this))
                .addParams("typeId",id)
                .addParams("content",etLiuyanNeirong.getText().toString().trim())
                .build()
                .execute(new StringCallback() {
                    @Override
                    public void onError(Call call, Exception e, int id) {

                    }

                    @Override
                    public void onResponse(String response, int id) {
                        System.out.println(response+"          政策法规留言的网络请求");
                        ContractUtils.Code500(ZhengCeFaGuiFenLeiActivity.this,response);
                        if(response.indexOf("200") != -1){
                            Gson gson = new Gson();
                            LiuYanFanKuiEntity liuYanFanKuiEntity = gson.fromJson(response, LiuYanFanKuiEntity.class);
                            Toast.makeText(ZhengCeFaGuiFenLeiActivity.this, "发表成功", Toast.LENGTH_SHORT).show();
                            finish();
                        }
                    }
                });
    }
}
