package com.wanyangproject.activity;

import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.view.Gravity;
import android.view.View;
import android.webkit.JavascriptInterface;
import android.webkit.WebChromeClient;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebResourceResponse;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.gson.Gson;
import com.wanyangproject.R;
import com.wanyangproject.entity.MyTieZiLieBiaoEntity;
import com.wanyangproject.entity.PingLunChengGongEntity;
import com.wanyangproject.entity.PingLunShiBaiEntity;
import com.wanyangproject.entity.TieZiFenXiangEntity;
import com.wanyangproject.entity.TieZiShouCangEntity;
import com.wanyangproject.entity.TieZiXiangQingEntity;
import com.wanyangproject.luntanfragment.DianZanFragment;
import com.wanyangproject.luntanfragment.LunTanPingLunFragment;
import com.wanyangproject.popuwindow.SharePopupWindow;
import com.wanyangproject.shouye.ZiXunXiangQingActivity;
import com.wanyangproject.utils.ContractUtils;
import com.wanyangproject.utils.JianTingZhuangTai;
import com.wanyangproject.utils.WebViewUtils;
import com.zhy.http.okhttp.OkHttpUtils;
import com.zhy.http.okhttp.callback.StringCallback;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.Call;

//   帖子详情
public class TieZiXiangQingActivity extends AppCompatActivity implements JianTingZhuangTai, View.OnClickListener {

    //    @BindView(R.id.tabLayout)
//    TabLayout tabLayout;
//    @BindView(R.id.viewPager)
//    ViewPager viewPager;
    @BindView(R.id.image_back)
    ImageView imageBack;
    //    @BindView(R.id.tv_pinglun)
//    RadioButton tvPinglun;
//    @BindView(R.id.tv_dianzan)
//    RadioButton tvDianzan;
//    @BindView(R.id.home_radioGroup)
//    RadioGroup homeRadioGroup;
    @BindView(R.id.image_touxiang)
    ImageView imageTouxiang;
    @BindView(R.id.tv_name)
    TextView tvName;
    @BindView(R.id.tv_neirong)
    TextView tvNeirong;
    @BindView(R.id.image)
    ImageView image;
    @BindView(R.id.image_one)
    ImageView imageOne;
    @BindView(R.id.image_two)
    ImageView imageTwo;
    @BindView(R.id.image_three)
    ImageView imageThree;
    @BindView(R.id.et_pinglun)
    EditText etPinglun;
    @BindView(R.id.tv_send)
    TextView tvSend;
    @BindView(R.id.tv_time)
    TextView tvTime;
    //    @BindView(R.id.tabLayout)
//    XTabLayout tabLayout;
//    @BindView(R.id.viewPager)
//    ViewPager viewPager;
    @BindView(R.id.linear)
    LinearLayout linear;
    @BindView(R.id.tv_pinglun)
    TextView tvPinglun;
    @BindView(R.id.viewpinglun)
    View viewpinglun;
    @BindView(R.id.tv_dianzan)
    TextView tvDianzan;
    @BindView(R.id.viewdianzan)
    View viewdianzan;
    //    @BindView(R.id.relativevvvv)
//    RelativeLayout relativevvvv;
    @BindView(R.id.relative_one)
    RelativeLayout relativeOne;
    @BindView(R.id.relative_two)
    RelativeLayout relativeTwo;
    @BindView(R.id.image_shoucang)
    ImageView imageShoucang;
    @BindView(R.id.image_zhuanfa)
    ImageView imageZhuanfa;
    @BindView(R.id.tv_title)
    TextView tvTitle;
    @BindView(R.id.webView)
    WebView webView;
    //        @BindView(R.id.recyclerView)
//        RecyclerView recyclerView;
    private LunTanPingLunFragment pingLunDianNaoFragment;
    private DianZanFragment dianZanFragment;
    private FragmentManager fragmentManager;
    private Fragment hideFragment;
    private String tieziid;
    private TieZiXiangQingEntity tieZiXiangQingEntity;
    private NetWork netWork;
    private String huifuid;
    private String huifuname;
    private boolean line = false;
    private SharePopupWindow sharePopupWindow;
    private String ispc;
    private WebSettings mWebSettings;
    private String content;
    private ProgressDialog progressDialog;




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.tiezi_xiangqing_activity);
        ButterKnife.bind(this);
        if (Build.VERSION.SDK_INT >= 21) {
            View dview = getWindow().getDecorView();
            dview.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN | View.SYSTEM_UI_FLAG_LAYOUT_STABLE);
            getWindow().setStatusBarColor(Color.TRANSPARENT);
        }




        mWebSettings = webView.getSettings();
        mWebSettings.setJavaScriptEnabled(true);  //允许加载javascript
        webView.getSettings().setLayoutAlgorithm(WebSettings.LayoutAlgorithm.SINGLE_COLUMN);
        webView.getSettings().setLoadWithOverviewMode(true);
        mWebSettings.setSupportZoom(false);     //允许缩放
        mWebSettings.setBuiltInZoomControls(false); //原网页基础上缩放
        mWebSettings.setUseWideViewPort(false);   //任意比例缩放
//        mWebSettings.setUseWideViewPort(true); //将图片调整到适合webview的大小
//        mWebSettings.setLoadWithOverviewMode(true); // 缩放至屏幕的大小
        mWebSettings.setDomStorageEnabled(true);//启用或禁用DOM缓存
        mWebSettings.setAppCacheEnabled(false);//关闭/启用应用缓存
        //settings.setBuiltInZoomControls(false);//是否显示缩放按钮，默认false
        mWebSettings.setAllowContentAccess(true);
        webView.setWebChromeClient(new WebChromeClient());
        webView.getSettings().setJavaScriptEnabled(true);



        /**
         * 启用mixed content    android 5.0以上默认不支持Mixed Content
         *
         * 5.0以上允许加载http和https混合的页面(5.0以下默认允许，5.0+默认禁止)
         * */
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            webView.getSettings().setMixedContentMode(WebSettings.MIXED_CONTENT_ALWAYS_ALLOW);
        }











        if (Build.VERSION.SDK_INT > 9) {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
        }

        Intent intent = getIntent();
        tieziid = intent.getStringExtra("tieziid");
        System.out.println(tieziid + "   接收帖子详情的id");
        ispc = intent.getStringExtra("ispc");//判断PC端发布还是APP发布


        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("huifu");
        netWork = new NetWork();
        registerReceiver(netWork, intentFilter);

        initPingLunDianZan();
        initDefaultFragment();
//        帖子详情的网络请求
        initTieZiXiangQingHttp();


        //        我的收藏帖子列表的网络请求
        initShouCangTieZiHttp();


        initView();



        //java回调js代码，不要忘了@JavascriptInterface这个注解，不然点击事件不起作用
        webView.addJavascriptInterface(new JsCallJavaObj() {
            @JavascriptInterface
            @Override
            public void showBigImg(String url) {
//                Toast.makeText(mContext, "圖片路勁"+url, Toast.LENGTH_SHORT).show();
                System.out.println(url+"       帖子详情图片url");
                Intent intent = new Intent(TieZiXiangQingActivity.this, TuPianActivity.class);
                intent.putExtra("url",url);
                startActivity(intent);
            }
        },"jsCallJavaObj");



//        initViewPager();
    }







    /**
     * 設置網頁中圖片的點擊事件
     * @param view
     */
    private  void setWebImageClick(WebView view) {
        String jsCode="javascript:(function(){" +
                "var imgs=document.getElementsByTagName(\"img\");" +
                "for(var i=0;i<imgs.length;i++){" +
                "imgs[i].onclick=function(){" +
                "window.jsCallJavaObj.showBigImg(this.src);" +
                "}}})()";
        webView.loadUrl(jsCode);
    }




    /**
     * Js調用Java接口
     */
    private interface JsCallJavaObj{
        void showBigImg(String url);
    }





    //    我的收藏帖子列表的网络请求
    private void initShouCangTieZiHttp() {
        OkHttpUtils.post()
                .url(ContractUtils.LOGIN_URL + "forum/myShoucangForum")
                .addHeader("token", ContractUtils.getTOKEN(TieZiXiangQingActivity.this))
                .build()
                .execute(new StringCallback() {
                    @Override
                    public void onError(Call call, Exception e, int id) {

                    }

                    @Override
                    public void onResponse(String response, int id) {
                        System.out.println(response + "           我的收藏帖子列表的网络请求");
                        ContractUtils.Code500(TieZiXiangQingActivity.this, response);
                        if (response.indexOf("200") != -1) {

                            Gson gson = new Gson();
                            MyTieZiLieBiaoEntity myTieZiLieBiaoEntity = gson.fromJson(response, MyTieZiLieBiaoEntity.class);

                            Boolean isOK = false;
                            for (int i = 0; i < myTieZiLieBiaoEntity.getResponse().getList().size(); i++) {
                                MyTieZiLieBiaoEntity.ResponseBean.ListBean listBean = myTieZiLieBiaoEntity.getResponse().getList().get(i);
                                if (listBean.getId().equals(tieziid)) {
                                    isOK = true;
                                }
                            }

                            if (isOK == true) {
                                imageShoucang.setImageResource(R.drawable.quxia_shoucang);
                            }


//                            try {
//                                JSONObject jsonObject = new JSONObject(response);
//                                JSONArray response1 = jsonObject.getJSONArray("response");
//
//                                Boolean isOK = false;
//                                if(response1 != null){
//                                    for (int i = 0; i < response1.length(); i++) {
//                                        JSONObject json = (JSONObject) response1.get(i);
//                                        if(json.getString("0").equals(tieziid)){
//                                            isOK = true;
//                                        }
//                                    }
//
//                                    if(isOK == true){
//                                        imageShoucang.setImageResource(R.drawable.quxia_shoucang);
//                                    }
//
//                                }
//
//                            } catch (JSONException e) {
//                                e.printStackTrace();
//                            }

                        }
                    }
                });
    }


    @Override
    public void zhuangTai() {
        //                                  给点赞传数据
        dianZanFragment.setTieZiXiangQingEntity(tieZiXiangQingEntity);
    }


//    @OnClick({R.id.tv_pinglun, R.id.viewpinglun, R.id.tv_dianzan, R.id.viewdianzan})
//    public void onViewClicked(View view) {
//        switch (view.getId()) {
//            case R.id.tv_pinglun:
//                break;
//            case R.id.viewpinglun:
//                break;
//            case R.id.tv_dianzan:
//                break;
//            case R.id.viewdianzan:
//                break;
//        }
//    }


    class NetWork extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            huifuid = intent.getStringExtra("id");
            huifuname = intent.getStringExtra("name");
            etPinglun.setHint("回复：" + huifuname);

        }
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(netWork);
    }

    //    帖子详情的网络请求
    private void initTieZiXiangQingHttp() {
        progressDialog = new ProgressDialog(TieZiXiangQingActivity.this);
        progressDialog.setTitle("提示");
        progressDialog.setMessage("请等待...");
        progressDialog.setCancelable(false);
        progressDialog.show();
        OkHttpUtils.post()
                .url(ContractUtils.LOGIN_URL + "forum/detail")
                .addHeader("token", ContractUtils.getTOKEN(this))
                .addParams("id", tieziid)
                .build()
                .execute(new StringCallback() {
                    @Override
                    public void onError(Call call, Exception e, int id) {
                        progressDialog.dismiss();
                        System.out.println(e + "         帖子详情eeeeeeeee");
                    }

                    @Override
                    public void onResponse(String response, int id) {

                        ContractUtils.Code500(TieZiXiangQingActivity.this, response);
                        System.out.println(response + "   帖子详情的网络请求");
                        if (response.indexOf("200") != -1) {
                            Gson gson = new Gson();
                            tieZiXiangQingEntity = gson.fromJson(response, TieZiXiangQingEntity.class);


                            if(ispc != null){
                                if(ispc.equals("1")){//电脑端发布
                                    tvNeirong.setVisibility(View.GONE);
                                    webView.setVisibility(View.VISIBLE);
                                    if (tieZiXiangQingEntity.getResponse().getData().getContent() != null) {
                                        //                            内容
                                        content = tieZiXiangQingEntity.getResponse().getData().getContent();
                                        String newContent = getNewContent(content);
                                        if(!newContent.endsWith("</p>")){
                                            newContent = newContent + "</p>";
                                        }
                                        webView.loadDataWithBaseURL("http://www.baidu.com", "<style>* {font-size:14px;line-height:20px;}p {color:#6f6f6f;}</style>"+newContent, "text/html", "UTF-8", null);



//                                    webView.loadDataWithBaseURL("http://www.baidu.com", getNewContent(content),"text/html", "UTF-8", null);
                                    }
                                }else if(ispc.equals("2")){//App端发布
                                    progressDialog.dismiss();
                                    tvNeirong.setVisibility(View.VISIBLE);
                                    webView.setVisibility(View.GONE);
                                    if (tieZiXiangQingEntity.getResponse().getData().getContent() != null) {
                                        tvNeirong.setText(tieZiXiangQingEntity.getResponse().getData().getContent());
                                    }
                                }

                            }




                            ArrayList<String> list = new ArrayList<String>();
                            for (int i = 0; i < tieZiXiangQingEntity.getResponse().getData().getImgArr().size(); i++) {
                                String s = tieZiXiangQingEntity.getResponse().getData().getImgArr().get(i);
                                if (s.length() > 0) {
                                    list.add(s);
                                }
                            }
                            tieZiXiangQingEntity.getResponse().getData().setImgArr(list);


//                               给评论传数据
                            pingLunDianNaoFragment.setTieZiXiangQingEntity(tieZiXiangQingEntity);
                            System.out.println(tieZiXiangQingEntity + "           评论aaaaaaaaa");


                            tvPinglun.setText("评论 " + tieZiXiangQingEntity.getResponse().getReplyArr().size());
                            tvDianzan.setText("点赞 " + tieZiXiangQingEntity.getResponse().getLikeArr().size());


                            if (tieZiXiangQingEntity.getResponse().getData().getUinfo().getAvatar() == null) {

                            } else {   //头像
                                Glide.with(TieZiXiangQingActivity.this).load(tieZiXiangQingEntity.getResponse().getData().getUinfo().getAvatar()).into(imageTouxiang);
                            }


                            if (tieZiXiangQingEntity.getResponse().getData().getTitle() == null) {

                            } else {
                                tvTitle.setText(tieZiXiangQingEntity.getResponse().getData().getTitle());
                            }


                            if (tieZiXiangQingEntity.getResponse().getData().getUinfo().getNickname() == null) {

                            } else {  // 名字
                                tvName.setText(tieZiXiangQingEntity.getResponse().getData().getUinfo().getNickname());
                            }


                            if (tieZiXiangQingEntity.getResponse().getData().getAdd_time() == null) {

                            } else {  // 时间
                                tvTime.setText(tieZiXiangQingEntity.getResponse().getData().getAdd_time());
                            }


//                            if (tieZiXiangQingEntity.getResponse().getData().getContent() == null) {
//
//                            } else { // 帖子的内容
//                                tvNeirong.setText(tieZiXiangQingEntity.getResponse().getData().getContent());
//                            }

                            if (tieZiXiangQingEntity.getResponse().getData().getImgArr() == null) {

                            } else {   //没有图片的
                                if (tieZiXiangQingEntity.getResponse().getData().getImgArr().size() == 0) {
                                    image.setVisibility(View.GONE);
                                    imageOne.setVisibility(View.GONE);
                                    imageTwo.setVisibility(View.GONE);
                                    imageThree.setVisibility(View.GONE);
//                                    一张图片的
                                } else if (tieZiXiangQingEntity.getResponse().getData().getImgArr().size() == 1) {
                                    image.setVisibility(View.VISIBLE);
                                    Glide.with(TieZiXiangQingActivity.this).load(tieZiXiangQingEntity.getResponse().getData().getImgArr().get(0).replace(" ", "")).into(image);
                                    imageOne.setVisibility(View.GONE);
                                    imageTwo.setVisibility(View.GONE);
                                    imageThree.setVisibility(View.GONE);
//                                    两张图片的
                                } else if (tieZiXiangQingEntity.getResponse().getData().getImgArr().size() == 2) {
                                    linear.setVisibility(View.VISIBLE);
                                    image.setVisibility(View.GONE);
                                    imageOne.setVisibility(View.VISIBLE);
                                    imageTwo.setVisibility(View.VISIBLE);
                                    Glide.with(TieZiXiangQingActivity.this).load(tieZiXiangQingEntity.getResponse().getData().getImgArr().get(0).replace(" ", "")).into(imageOne);
                                    Glide.with(TieZiXiangQingActivity.this).load(tieZiXiangQingEntity.getResponse().getData().getImgArr().get(1).replace(" ", "")).into(imageTwo);
                                    imageThree.setVisibility(View.GONE);
                                } else if (tieZiXiangQingEntity.getResponse().getData().getImgArr().size() >= 3) {
                                    linear.setVisibility(View.VISIBLE);
                                    image.setVisibility(View.GONE);
                                    imageOne.setVisibility(View.VISIBLE);
                                    imageTwo.setVisibility(View.VISIBLE);
                                    imageThree.setVisibility(View.VISIBLE);
                                    Glide.with(TieZiXiangQingActivity.this).load(tieZiXiangQingEntity.getResponse().getData().getImgArr().get(0).replace(" ", "")).into(imageOne);
                                    Glide.with(TieZiXiangQingActivity.this).load(tieZiXiangQingEntity.getResponse().getData().getImgArr().get(1).replace(" ", "")).into(imageTwo);
                                    Glide.with(TieZiXiangQingActivity.this).load(tieZiXiangQingEntity.getResponse().getData().getImgArr().get(2).replace(" ", "")).into(imageThree);
                                }
                            }
                        }


                        if(ispc !=null){
                            if(ispc.equals("1")){
                                image.setVisibility(View.GONE);
                            }
                        }


                        Intent intent = new Intent();
                        intent.putExtra("pinglun", tieZiXiangQingEntity.getResponse().getReplyArr().size());
                        intent.putExtra("dianzan", tieZiXiangQingEntity.getResponse().getLikeArr().size());
                        intent.setAction("luntanshuliang");
                        sendBroadcast(intent);


                    }
                });
    }








    private void initView() {
        webView.setWebViewClient(new WebViewClient() {
            /**
             * 拦截 url 跳转,在里边添加点击链接跳转或者操作
             */
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, WebResourceRequest request) {
                return WebViewUtils.shouldOverrideUrlLoading(view,request,TieZiXiangQingActivity.this);
            }


            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                System.out.println("66666666666666666666");
                return WebViewUtils.shouldOverrideUrlLoading(view,url,TieZiXiangQingActivity.this);
            }




            /**
             * 在开始加载网页时会回调
             */
            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                super.onPageStarted(view, url, favicon);
                System.out.println("111111111111111111");
            }

            /**
             * 加载错误的时候会回调，在其中可做错误处理，比如再请求加载一次，或者提示404的错误页面
             */
            @Override
            public void onReceivedError(WebView view, WebResourceRequest request, WebResourceError error) {
                super.onReceivedError(view, request, error);

            }


            /**
             * 在结束加载网页时会回调
             */
            @Override
            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(view, url);
                System.out.println("chenggongchenggongchenggong");
                if(progressDialog != null){
                    progressDialog.dismiss();
                }
                setWebImageClick(view);


            }

            /**
             * 当接收到https错误时，会回调此函数，在其中可以做错误处理
             */
            @Override
            public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
                super.onReceivedError(view, errorCode, description, failingUrl);
            }

            /**
             * 在每一次请求资源时，都会通过这个函数来回调
             */
            public WebResourceResponse shouldInterceptRequest(WebView view, String url) {
                return null;
            }


            @Override
            public void onPageCommitVisible(WebView view, String url) {
                super.onPageCommitVisible(view, url);
                if(progressDialog != null){
                    progressDialog.dismiss();
                }
            }
        });
    }










    /**
     * 将html文本内容中包含img标签的图片，宽度变为屏幕宽度，高度根据宽度比例自适应
     **/
    public static String getNewContent(String htmltext){

//        if(tupian == false){
//            System.out.println("tupian111111111");
//            return htmltext;
//        }
//        tupian = true;

        try {
            Document doc= Jsoup.parse(htmltext);
            Elements elements=doc.getElementsByTag("img");
            for (Element element : elements) {
                element.attr("width","100%").attr("height","auto");
            }
            return doc.toString();
        } catch (Exception e) {
            return htmltext;
        }
    }







    private void initDefaultFragment() {
        fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.add(R.id.container_fragment, pingLunDianNaoFragment);
        fragmentTransaction.commit();
        hideFragment = pingLunDianNaoFragment;
    }

    //    //    初始化
    private void initPingLunDianZan() {
        pingLunDianNaoFragment = new LunTanPingLunFragment();
        dianZanFragment = new DianZanFragment();
        relativeOne.setOnClickListener(this);
        relativeTwo.setOnClickListener(this);

//        homeRadioGroup.setOnCheckedChangeListener(this);
    }

    private void replaceFragment(Fragment showFragment, FragmentTransaction fragmentTransaction) {
        if (showFragment.isAdded()) {
            fragmentTransaction.hide(hideFragment).show(showFragment).commit();
        } else {
            fragmentTransaction.hide(hideFragment).add(R.id.container_fragment, showFragment).commit();
        }
        hideFragment = showFragment;

        if (line) {
            dianZanFragment.setJianTingZhuangTai(this);
        }
    }


    @Override
    public void onClick(View view) {
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        switch (view.getId()) {
//            评论
            case R.id.relative_one:
                tvPinglun.setTextColor(Color.parseColor("#ff2222"));
                tvDianzan.setTextColor(Color.parseColor("#FFC3C3C3"));
                viewpinglun.setVisibility(View.VISIBLE);
                viewdianzan.setVisibility(View.GONE);
                replaceFragment(pingLunDianNaoFragment, fragmentTransaction);
                break;
//            点赞
            case R.id.relative_two:
                line = true;
                tvPinglun.setTextColor(Color.parseColor("#FFC3C3C3"));
                tvDianzan.setTextColor(Color.parseColor("#ff2222"));
                viewpinglun.setVisibility(View.GONE);
                viewdianzan.setVisibility(View.VISIBLE);
                replaceFragment(dianZanFragment, fragmentTransaction);
                break;
        }
    }

//    @Override
//    public void onCheckedChanged(RadioGroup radioGroup, @IdRes int checkedId) {
//        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
//        switch (checkedId) {
////            评论
//            case R.id.tv_pinglun:
////                viewpinglun.setVisibility(View.VISIBLE);
////                viewdianzan.setVisibility(View.GONE);
//                replaceFragment(pingLunDianNaoFragment, fragmentTransaction);
//                break;
////            点赞
//            case R.id.tv_dianzan:
//                line=true;
////                viewpinglun.setVisibility(View.GONE);
////                viewdianzan.setVisibility(View.VISIBLE);
//                replaceFragment(dianZanFragment, fragmentTransaction);
//
//                break;
//        }
//    }


//    private void initViewPager() {
//        List<Fragment> fragments = new ArrayList<>();
//        pingLunDianNaoFragment = new LunTanPingLunFragment();
//        dianZanFragment = new DianZanFragment();
//        fragments.add(pingLunDianNaoFragment);
//        fragments.add(dianZanFragment);
//        // 创建ViewPager适配器
//        MyPagerAdapter myPagerAdapter = new MyPagerAdapter(getSupportFragmentManager());
//        myPagerAdapter.setFragments(fragments);
//        // 给ViewPager设置适配器
//        viewPager.setAdapter(myPagerAdapter);
//        // TabLayout 指示器 (记得自己手动创建4个Fragment,注意是 app包下的Fragment 还是 V4包下的 Fragment)
//        tabLayout.addTab(tabLayout.newTab());
//        tabLayout.addTab(tabLayout.newTab());
//        // 使用 TabLayout 和 ViewPager 相关联
//        tabLayout.setupWithViewPager(viewPager);
//        // TabLayout指示器添加文本
//        tabLayout.getTabAt(0).setText("评论");
//        tabLayout.getTabAt(1).setText("点赞");
//
//    }


    @OnClick({R.id.image_back, R.id.image_touxiang, R.id.image_shoucang, R.id.image_zhuanfa, R.id.tv_name, R.id.tv_neirong, R.id.image, R.id.image_one, R.id.image_two, R.id.image_three, R.id.et_pinglun, R.id.tv_send})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.image_back:
                finish();
                break;
            case R.id.image_touxiang:
                break;
            case R.id.tv_name:
                break;
            case R.id.tv_neirong:
                break;
            case R.id.image:
                if(ispc.equals("1")){
                    return;
                }else if(ispc.equals("2")){
                    Intent intent = new Intent(TieZiXiangQingActivity.this, TuPianActivity.class);
                    intent.putExtra("image", tieZiXiangQingEntity.getResponse().getData().getImgArr().get(0));
                    startActivity(intent);
                }

//                Intent intent = new Intent(TieZiXiangQingActivity.this,TuPianActivity.class);
//                intent.putExtra("image", (Serializable) tieZiXiangQingEntity.getResponse().getData().getImgArr());
//                startActivity(intent);
                break;
            case R.id.image_one:
                if(ispc.equals("1")){
                    return;
                }else if(ispc.equals("2")){
                    Intent intent1 = new Intent(TieZiXiangQingActivity.this, TuPianActivity.class);
                    intent1.putExtra("image", tieZiXiangQingEntity.getResponse().getData().getImgArr().get(0));
                    startActivity(intent1);
                }

//                Intent intent1 = new Intent(TieZiXiangQingActivity.this,TuPianActivity.class);
//                intent1.putExtra("image",(Serializable) tieZiXiangQingEntity.getResponse().getData().getImgArr());
//                startActivity(intent1);
                break;
            case R.id.image_two:
                if(ispc.equals("1")){
                    return;
                }else if(ispc.equals("2")){
                    Intent intent2 = new Intent(TieZiXiangQingActivity.this, TuPianActivity.class);
                    intent2.putExtra("image", tieZiXiangQingEntity.getResponse().getData().getImgArr().get(1));
                    startActivity(intent2);
                }


//                Intent intent2 = new Intent(TieZiXiangQingActivity.this,TuPianActivity.class);
//                intent2.putExtra("image",(Serializable) tieZiXiangQingEntity.getResponse().getData().getImgArr());
//                startActivity(intent2);
                break;
            case R.id.image_three:
                if(ispc.equals("1")){
                    return;
                }else if(ispc.equals("2")){
                    Intent intent3 = new Intent(TieZiXiangQingActivity.this, TuPianActivity.class);
                    intent3.putExtra("image", tieZiXiangQingEntity.getResponse().getData().getImgArr().get(2));
                    startActivity(intent3);
                }

//                Intent intent3 = new Intent(TieZiXiangQingActivity.this,TuPianActivity.class);
//                intent3.putExtra("image",(Serializable) tieZiXiangQingEntity.getResponse().getData().getImgArr());
//                startActivity(intent3);
                break;
            case R.id.et_pinglun:
                break;
            case R.id.tv_send:
                if (etPinglun.getText().toString().equals("")) {
                    Toast.makeText(this, "请输入评论内容", Toast.LENGTH_SHORT).show();
                } else {
                    //                发表评论的网络请求
                    initSendPingLunHttp();
                }

                break;
//            收藏
            case R.id.image_shoucang:
//                帖子收藏的网络请求
                initTieZiShouCangHttp();
                break;
//            分享
            case R.id.image_zhuanfa:
//                帖子分享的网络请求
                initTieZiFenXiangHttp();
                break;
        }
    }


    //    帖子分享的网络请求
    private void initTieZiFenXiangHttp() {
        OkHttpUtils.post()
                .url(ContractUtils.LOGIN_URL + "forum/getShareInfo")
                .addHeader("token", ContractUtils.getTOKEN(TieZiXiangQingActivity.this))
                .addParams("id", tieziid)
                .build()
                .execute(new StringCallback() {
                    @Override
                    public void onError(Call call, Exception e, int id) {

                    }

                    @Override
                    public void onResponse(String response, int id) {
                        ContractUtils.Code500(TieZiXiangQingActivity.this, response);
                        System.out.println(response + "          帖子分享的网络请求");
                        if (response.indexOf("200") != -1) {
                            Gson gson = new Gson();
                            TieZiFenXiangEntity tieZiFenXiangEntity = gson.fromJson(response, TieZiFenXiangEntity.class);
                            Object itemsOnClick = null;
                            Bitmap bitmap = BitmapFactory.decodeResource(getResources(), R.drawable.loginbaibian);
                            sharePopupWindow = new SharePopupWindow(TieZiXiangQingActivity.this, (View.OnClickListener) itemsOnClick, tieZiFenXiangEntity.getResponse().getTitle(), tieZiFenXiangEntity.getResponse().getUrl(), bitmap);
//                            sharePopupWindow = new SharePopupWindow(TieZiXiangQingActivity.this,(View.OnClickListener) itemsOnClick,tieZiFenXiangEntity.getResponse().getTitle(),tieZiFenXiangEntity.getResponse().getUrl(),ContractUtils.YUANQUZHAOSHANG_PHOTO_URL+"/"+tieZiFenXiangEntity.getResponse().getImage());
                            sharePopupWindow.showAtLocation(findViewById(R.id.relative_Layout), Gravity.BOTTOM | Gravity.CENTER_HORIZONTAL, 0, 0);
                        } else {
                            ContractUtils.Code400(TieZiXiangQingActivity.this, response);
//                            Gson gson = new Gson();
//                            FenXiangShiBaiEntity fenXiangShiBaiEntity = gson.fromJson(response, FenXiangShiBaiEntity.class);
//                            Toast.makeText(TieZiXiangQingActivity.this, fenXiangShiBaiEntity.getMsg(), Toast.LENGTH_SHORT).show();
                        }
                    }
                });
    }


    //    帖子收藏的网络请求
    private void initTieZiShouCangHttp() {
        OkHttpUtils.post()
                .url(ContractUtils.LOGIN_URL + "forum/ShoucangForum")
                .addHeader("token", ContractUtils.getTOKEN(TieZiXiangQingActivity.this))
                .addParams("id", tieziid)
                .build()
                .execute(new StringCallback() {
                    @Override
                    public void onError(Call call, Exception e, int id) {

                    }

                    @Override
                    public void onResponse(String response, int id) {
                        System.out.println(response + "            帖子收藏的网络请求");
                        ContractUtils.Code500(TieZiXiangQingActivity.this, response);
                        if (response.indexOf("200") != -1) {
                            Gson gson = new Gson();
                            TieZiShouCangEntity tieZiShouCangEntity = gson.fromJson(response, TieZiShouCangEntity.class);
                            Toast.makeText(TieZiXiangQingActivity.this, tieZiShouCangEntity.getMsg(), Toast.LENGTH_SHORT).show();
                            System.out.println(tieZiShouCangEntity.getMsg() + "          mmmmmmmsssssssggggggggg");
                            if (tieZiShouCangEntity.getMsg().equals("收藏成功！")) {
                                imageShoucang.setImageResource(R.drawable.quxia_shoucang);
                            } else if (tieZiShouCangEntity.getMsg().equals("取消收藏成功！")) {
                                imageShoucang.setImageResource(R.drawable.shoucang_white);
                            }
                        }
                    }
                });
    }


    //    发表评论的网络请求
    private void initSendPingLunHttp() {
//        tiantian
        System.out.println(ContractUtils.getTOKEN(TieZiXiangQingActivity.this) + "        token");
        System.out.println(etPinglun.getText().toString().trim() + "         content");
        System.out.println(tieziid + "        forumId  ");
        System.out.println(huifuid + "      upId ");


        if (huifuid != null) {
            OkHttpUtils.post()
                    .url(ContractUtils.LOGIN_URL + "forum/toreply")
                    .addHeader("token", ContractUtils.getTOKEN(TieZiXiangQingActivity.this))
                    .addParams("content", etPinglun.getText().toString().trim())
                    .addParams("forumId", tieziid)
                    .addParams("upId", huifuid)//如果是发表的话回复0，如果是回复的话，回复需要回复的ID
                    .build()
                    .execute(new StringCallback() {
                        @Override
                        public void onError(Call call, Exception e, int id) {
                            System.out.println(e + "         eeeeeeee回复");
                        }

                        @Override
                        public void onResponse(String response, int id) {
                            ContractUtils.Code500(TieZiXiangQingActivity.this, response);
                            System.out.println(response + "     回复评论的网络请求");
                            if (response.indexOf("200") != -1) {
                                Gson gson = new Gson();
                                PingLunChengGongEntity pingLunChengGongEntity = gson.fromJson(response, PingLunChengGongEntity.class);
                                etPinglun.setText("");
                                Toast.makeText(TieZiXiangQingActivity.this, pingLunChengGongEntity.getMsg(), Toast.LENGTH_SHORT).show();
                                //        帖子详情的网络请求
                                initTieZiXiangQingHttp();
                            } else if (response.indexOf("400") != -1) {
                                Gson gson = new Gson();
                                PingLunShiBaiEntity pingLunShiBaiEntity = gson.fromJson(response, PingLunShiBaiEntity.class);

                                Toast.makeText(TieZiXiangQingActivity.this, pingLunShiBaiEntity.getMsg(), Toast.LENGTH_SHORT).show();
                            }
                        }
                    });
        } else {
            OkHttpUtils.post()
                    .url(ContractUtils.LOGIN_URL + "forum/toreply")
                    .addHeader("token", ContractUtils.getTOKEN(TieZiXiangQingActivity.this))
                    .addParams("content", etPinglun.getText().toString().trim())
                    .addParams("forumId", tieziid)
                    .addParams("upId", "0")//如果是发表的话回复0，如果是回复的话，回复需要回复的ID
                    .build()
                    .execute(new StringCallback() {
                        @Override
                        public void onError(Call call, Exception e, int id) {
                            System.out.println(e + "         eeeeeeee回复");
                        }

                        @Override
                        public void onResponse(String response, int id) {
                            ContractUtils.Code500(TieZiXiangQingActivity.this, response);
                            System.out.println(response + "     发表评论的网络请求");
                            if (response.indexOf("200") != -1) {
                                Gson gson = new Gson();
                                PingLunChengGongEntity pingLunChengGongEntity = gson.fromJson(response, PingLunChengGongEntity.class);
                                etPinglun.setText("");
                                Toast.makeText(TieZiXiangQingActivity.this, pingLunChengGongEntity.getMsg(), Toast.LENGTH_SHORT).show();
                                //        帖子详情的网络请求
                                initTieZiXiangQingHttp();
                            } else if (response.indexOf("400") != -1) {
                                Gson gson = new Gson();
                                PingLunShiBaiEntity pingLunShiBaiEntity = gson.fromJson(response, PingLunShiBaiEntity.class);

                                Toast.makeText(TieZiXiangQingActivity.this, pingLunShiBaiEntity.getMsg(), Toast.LENGTH_SHORT).show();
                            }
                        }
                    });
        }


    }
}
