package com.wanyangproject.activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.webkit.JavascriptInterface;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;

import com.google.gson.Gson;
import com.wanyangproject.R;
import com.wanyangproject.entity.GuanYuWoMenEntity;
import com.wanyangproject.my.DingDanTongJiActivity;
import com.wanyangproject.net.util.ProgressDialog;
import com.wanyangproject.utils.ContractUtils;
import com.zhy.http.okhttp.OkHttpUtils;
import com.zhy.http.okhttp.callback.StringCallback;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.Call;

public class GuanYuWoMenActivity extends AppCompatActivity {

    @BindView(R.id.image_back)
    ImageView imageBack;
    @BindView(R.id.webView)
    WebView webView;
    private String content;
    private WebSettings mWebSettings;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_guan_yu_wo_men);
        ButterKnife.bind(this);


        mWebSettings = webView.getSettings();
        mWebSettings.setJavaScriptEnabled(true);  //允许加载javascript
        webView.getSettings().setLayoutAlgorithm(WebSettings.LayoutAlgorithm.SINGLE_COLUMN);
        webView.getSettings().setLoadWithOverviewMode(true);
        mWebSettings.setSupportZoom(false);     //允许缩放
        mWebSettings.setBuiltInZoomControls(false); //原网页基础上缩放
        mWebSettings.setUseWideViewPort(false);   //任意比例缩放
//        mWebSettings.setUseWideViewPort(true); //将图片调整到适合webview的大小
        mWebSettings.setLoadWithOverviewMode(true); // 缩放至屏幕的大小
        // 支持通过JS打开新窗口
        mWebSettings.setJavaScriptCanOpenWindowsAutomatically(true);
        mWebSettings.setLoadWithOverviewMode(true);
        //不显示webview缩放按钮
        mWebSettings.setDisplayZoomControls(false);
//        String cacheDirPath = getFilesDir().getAbsolutePath() + APP_CACAHE_DIRNAME;
//      String cacheDirPath = getCacheDir().getAbsolutePath()+Constant.APP_DB_DIRNAME;
        //设置数据库缓存路径
//        webView.getSettings().setDatabasePath(cacheDirPath);
        //设置  Application Caches 缓存目录
//        webView.getSettings().setAppCachePath(cacheDirPath);
        //开启 Application Caches 功能
        webView.getSettings().setAppCacheEnabled(true);
        webView.setWebViewClient(new WebViewClient());

        //        webView.setWebChromeClient(new WebChromeClient());
        webView.addJavascriptInterface(new JavaScriptInterface(), "JSInterface");



//        关于我们的网络请求
        initGuanYuWoMenHttp();


    }





    class JavaScriptInterface {
        @JavascriptInterface
        public void processFormInputs(String html) {
//            LogUtils.e("html-->" + html);
            System.out.println(html);
        }
    }




    /**
     * 将html文本内容中包含img标签的图片，宽度变为屏幕宽度，高度根据宽度比例自适应
     **/
//    public static String getNewContent(String htmltext){
//        try {
//            Document doc= Jsoup.parse(htmltext);
//            Elements elements=doc.getElementsByTag("img");
//            for (Element element : elements) {
//                element.attr("width","100%").attr("height","auto");
//            }
//
//            return doc.toString();
//        } catch (Exception e) {
//            return htmltext;
//        }
//    }
//



    //    关于我们的网络请求
    private void initGuanYuWoMenHttp() {
        OkHttpUtils.post()
                .url(ContractUtils.LOGIN_URL + "index/aboutUs")
                .addHeader("token", ContractUtils.getTOKEN(GuanYuWoMenActivity.this))
                .build()
                .execute(new StringCallback() {
                    @Override
                    public void onError(Call call, Exception e, int id) {

                    }

                    @Override
                    public void onResponse(String response, int id) {
                        System.out.println(response + "          关于我们的网络请求 ");
                        ContractUtils.Code500(GuanYuWoMenActivity.this, response);
                        if (response.indexOf("200") != -1) {
                            Gson gson = new Gson();
                            GuanYuWoMenEntity guanYuWoMenEntity = gson.fromJson(response, GuanYuWoMenEntity.class);

                            if(guanYuWoMenEntity.getResponse().getContent() != null){
                                content = guanYuWoMenEntity.getResponse().getContent();
//                                webView.loadDataWithBaseURL("http://www.baidu.com", getNewContent(content),"text/html", "UTF-8", null);
                                webView.loadDataWithBaseURL("http://www.baidu.com", content,"text/html", "UTF-8", null);
                            }

                        }
                    }
                });
    }

    @OnClick(R.id.image_back)
    public void onViewClicked() {
        finish();
    }
}
