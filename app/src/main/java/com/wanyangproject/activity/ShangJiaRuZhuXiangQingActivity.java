package com.wanyangproject.activity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.gson.Gson;
import com.wanyangproject.R;
import com.wanyangproject.entity.PhotoEntity;
import com.wanyangproject.entity.ShangJiaRuZhuDianPuLeiXingEntity;
import com.wanyangproject.entity.ShangJiaRuZhuDuanXinEntity;
import com.wanyangproject.entity.ShangJiaRuZhuXiuGaiEntity;
import com.wanyangproject.popuwindow.DianPuLeiXingPopupWindow;
import com.wanyangproject.popuwindow.TakePhotoPopWin;
import com.wanyangproject.utils.ContractUtils;
import com.zhy.http.okhttp.OkHttpUtils;
import com.zhy.http.okhttp.callback.StringCallback;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.Call;

public class ShangJiaRuZhuXiangQingActivity extends AppCompatActivity {

    @BindView(R.id.relative_rl)
    RelativeLayout relativeRl;
    @BindView(R.id.tv_me)
    TextView tvMe;
    @BindView(R.id.relative_title)
    RelativeLayout relativeTitle;
    @BindView(R.id.et_shenqingren)
    TextView etShenqingren;
    @BindView(R.id.view)
    View view;
    @BindView(R.id.view_phone)
    View viewPhone;
    @BindView(R.id.tv_yanzhengma)
    TextView tvYanzhengma;
    @BindView(R.id.et_shuru_yanzhengma)
    TextView etShuruYanzhengma;
    @BindView(R.id.tv_huoqu_yanzhengma)
    TextView tvHuoquYanzhengma;
    @BindView(R.id.relative)
    RelativeLayout relative;
    @BindView(R.id.tv_dianpu_mingcheng)
    TextView tvDianpuMingcheng;
    @BindView(R.id.et_dianpu_mingcheng)
    TextView etDianpuMingcheng;
    @BindView(R.id.view_dianpu_mingcheng)
    View viewDianpuMingcheng;
    @BindView(R.id.tv_dianpu_leixing)
    TextView tvDianpuLeixing;
    @BindView(R.id.tv_xuanze_dianpu)
    TextView tvXuanzeDianpu;
    @BindView(R.id.view_dianpu_leixing)
    View viewDianpuLeixing;
    @BindView(R.id.tv_dianpu_address)
    TextView tvDianpuAddress;
    @BindView(R.id.et_dianpu_dizhi)
    TextView etDianpuDizhi;
    @BindView(R.id.view_dianpu_address)
    View viewDianpuAddress;
    @BindView(R.id.tv_diapujieshao)
    TextView tvDiapujieshao;
    @BindView(R.id.et_dianpu_jieshao)
    TextView etDianpuJieshao;
    @BindView(R.id.relative_layout)
    RelativeLayout relativeLayout;
    @BindView(R.id.tv_name)
    TextView tvName;
    @BindView(R.id.et_faren_name)
    TextView etFarenName;
    @BindView(R.id.view_name)
    View viewName;
    @BindView(R.id.tv_shenfenzheng)
    TextView tvShenfenzheng;
    @BindView(R.id.image_zhengmian)
    ImageView imageZhengmian;
    @BindView(R.id.image_fanmian)
    ImageView imageFanmian;
    @BindView(R.id.linear)
    LinearLayout linear;
    @BindView(R.id.lin)
    LinearLayout lin;
    @BindView(R.id.view_v)
    View viewV;
    @BindView(R.id.tv_yingyezhizhao)
    TextView tvYingyezhizhao;
    @BindView(R.id.image_yingyezhizhao)
    ImageView imageYingyezhizhao;
    @BindView(R.id.btn_ruzhu)
    Button btnRuzhu;
    @BindView(R.id.relative_shangjia_ruzhu)
    RelativeLayout relativeShangjiaRuzhu;
    @BindView(R.id.image_back)
    ImageView imageBack;
    @BindView(R.id.tv_shenqingren)
    TextView tvShenqingren;
    @BindView(R.id.tv_phone)
    TextView tvPhone;
    @BindView(R.id.et_phone)
    TextView etPhone;

    private DianPuLeiXingPopupWindow dianPuLeiXingPopupWindow;
    private int num;
    private ShangJiaRuZhuDianPuLeiXingEntity shangJiaRuZhuDianPuLeiXingEntity;
    private String lxid;
    private TakePhotoPopWin takePhotoPopWin;
    private PhotoEntity ZhengMianEntity;
    private PhotoEntity FanMianEntity;
    private PhotoEntity YingYeZhiZhaoEntity;
    private ShangJiaRuZhuDuanXinEntity shangJiaRuZhuDuanXinEntity;
    private int isOK = 0;
    private ArrayList<String> list11 = new ArrayList<>();//店铺类型id
    private ArrayList<String> list22 = new ArrayList<>();//店铺类型title
    private Uri photoUri;
    private String id;
    private ShangJiaRuZhuXiuGaiEntity shangJiaRuZhuXiuGaiEntity;
    private int dianpuid;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shang_jia_ru_zhu_xiang_qing);
        ButterKnife.bind(this);

        Intent intent = getIntent();
        id = intent.getStringExtra("id");

        System.out.println(id + "       接收id");



//        修改商家入驻信息的网络请求
        initShangJiaXiuGaiHttp();
    }


    //    修改商家入驻信息的网络请求
    private void initShangJiaXiuGaiHttp() {
        OkHttpUtils.post()
                .url(ContractUtils.LOGIN_URL + "shop/shopdetails")
                .addHeader("token", ContractUtils.getTOKEN(ShangJiaRuZhuXiangQingActivity.this))
                .addParams("parkId", ContractUtils.getParkId(ShangJiaRuZhuXiangQingActivity.this))
                .addParams("id", id)
                .build()
                .execute(new StringCallback() {
                    @Override
                    public void onError(Call call, Exception e, int id) {

                    }

                    @Override
                    public void onResponse(String response, int id) {
                        System.out.println(response + "          商家信息的详情 ");
                        ContractUtils.Code500(ShangJiaRuZhuXiangQingActivity.this, response);
                        if (response.indexOf("200") != -1) {
                            Gson gson = new Gson();
                            shangJiaRuZhuXiuGaiEntity = gson.fromJson(response, ShangJiaRuZhuXiuGaiEntity.class);
                            if (shangJiaRuZhuXiuGaiEntity.getResponse().getApplicant_name() != null) {
                                etShenqingren.setText(shangJiaRuZhuXiuGaiEntity.getResponse().getApplicant_name());
                            }

                            if (shangJiaRuZhuXiuGaiEntity.getResponse().getApplicant_phone() != null) {
                                etPhone.setText(shangJiaRuZhuXiuGaiEntity.getResponse().getApplicant_phone());
                            }

                            if (shangJiaRuZhuXiuGaiEntity.getResponse().getName() != null) {
                                etDianpuMingcheng.setText(shangJiaRuZhuXiuGaiEntity.getResponse().getName());
                            }

                            if (shangJiaRuZhuXiuGaiEntity.getResponse().getClassX() != null) {
//                                System.out.println(list22+"       list22");
//                                System.out.println(list11+"          list11");
//                                System.out.println(shangJiaRuZhuXiuGaiEntity.getResponse().getClassX()+"           shangJiaRuZhuXiuGaiEntity");
////                                System.out.println(dianpuid+"         dianpuid");
//                                if (list22.size() > 0) {
//                                    dianpuid = list11.indexOf(shangJiaRuZhuXiuGaiEntity.getResponse().getClassX());
//                                    String s = list22.get(dianpuid);
//                                    tvXuanzeDianpu.setText(s);
//                                }
//                             商家入驻店铺类型的网络请求
                                initShangJiaRuZhuDianPuLeiXingHttp();

                            }


                            if (shangJiaRuZhuXiuGaiEntity.getResponse().getAddres() != null) {
                                etDianpuDizhi.setText(shangJiaRuZhuXiuGaiEntity.getResponse().getAddres());
                            }

                            if (shangJiaRuZhuXiuGaiEntity.getResponse().getIntroduce() != null) {
                                etDianpuJieshao.setText(shangJiaRuZhuXiuGaiEntity.getResponse().getIntroduce());
                            }

                            if (shangJiaRuZhuXiuGaiEntity.getResponse().getLegal_name() != null) {
                                etFarenName.setText(shangJiaRuZhuXiuGaiEntity.getResponse().getLegal_name());
                            }

                            if (shangJiaRuZhuXiuGaiEntity.getResponse().getNumber_Positive() != null) {
                                if (shangJiaRuZhuXiuGaiEntity.getResponse().getNumber_Positive().contains(ContractUtils.YUANQUZHAOSHANG_PHOTO_URL)) {
                                    Glide.with(ShangJiaRuZhuXiangQingActivity.this).load(shangJiaRuZhuXiuGaiEntity.getResponse().getNumber_Positive()).into(imageZhengmian);
                                } else {
                                    Glide.with(ShangJiaRuZhuXiangQingActivity.this).load(ContractUtils.YUANQUZHAOSHANG_PHOTO_URL + shangJiaRuZhuXiuGaiEntity.getResponse().getNumber_Positive()).into(imageZhengmian);
                                }

                            }


                            if (shangJiaRuZhuXiuGaiEntity.getResponse().getNumber_side() != null) {
                                if (shangJiaRuZhuXiuGaiEntity.getResponse().getNumber_side().contains(ContractUtils.YUANQUZHAOSHANG_PHOTO_URL)) {
                                    Glide.with(ShangJiaRuZhuXiangQingActivity.this).load(shangJiaRuZhuXiuGaiEntity.getResponse().getNumber_side()).into(imageFanmian);
                                } else {
                                    Glide.with(ShangJiaRuZhuXiangQingActivity.this).load(ContractUtils.YUANQUZHAOSHANG_PHOTO_URL + shangJiaRuZhuXiuGaiEntity.getResponse().getNumber_side()).into(imageFanmian);
                                }
                            }


                            if (shangJiaRuZhuXiuGaiEntity.getResponse().getLicense() != null) {
                                if (shangJiaRuZhuXiuGaiEntity.getResponse().getLicense().contains(ContractUtils.YUANQUZHAOSHANG_PHOTO_URL)) {
                                    Glide.with(ShangJiaRuZhuXiangQingActivity.this).load(shangJiaRuZhuXiuGaiEntity.getResponse().getLicense()).into(imageYingyezhizhao);
                                } else {
                                    Glide.with(ShangJiaRuZhuXiangQingActivity.this).load(ContractUtils.YUANQUZHAOSHANG_PHOTO_URL + shangJiaRuZhuXiuGaiEntity.getResponse().getLicense()).into(imageYingyezhizhao);
                                }
                            }
                        }

                    }
                });
    }


    //    商家入驻店铺类型的网络请求
    private void initShangJiaRuZhuDianPuLeiXingHttp() {
        OkHttpUtils.post()
                .url(ContractUtils.LOGIN_URL + "shop/merchantclass")
                .addHeader("token", ContractUtils.getTOKEN(ShangJiaRuZhuXiangQingActivity.this))
                .addParams("parkId", ContractUtils.getParkId(ShangJiaRuZhuXiangQingActivity.this))
                .build()
                .execute(new StringCallback() {
                    @Override
                    public void onError(Call call, Exception e, int id) {
                        System.out.println(e + "      商家入驻类型eee");
                    }

                    @Override
                    public void onResponse(String response, int id) {
                        ContractUtils.Code500(ShangJiaRuZhuXiangQingActivity.this, response);
                        System.out.println(response + "        商家入驻店铺类型的网络请求");

                        if (response.indexOf("200") != -1) {
                            Gson gson = new Gson();
                            shangJiaRuZhuDianPuLeiXingEntity = gson.fromJson(response, ShangJiaRuZhuDianPuLeiXingEntity.class);
                            for (int i = 0; i < shangJiaRuZhuDianPuLeiXingEntity.getResponse().size(); i++) {
                                list11.add(shangJiaRuZhuDianPuLeiXingEntity.getResponse().get(i).getId());
                                list22.add(shangJiaRuZhuDianPuLeiXingEntity.getResponse().get(i).getName());
                            }

                            System.out.println(list11+"      1111111111");
                            System.out.println(list22+"      2222222222");
                            System.out.println(shangJiaRuZhuXiuGaiEntity.getResponse().getClassX()+"        shangJiaRuZhuXiuGaiEntity");

                            if (shangJiaRuZhuXiuGaiEntity != null) {
                                if (list22.size() > 0) {
                                    dianpuid = list11.indexOf(shangJiaRuZhuXiuGaiEntity.getResponse().getClassX());
                                    if(dianpuid == -1){
                                        return;
                                    }
                                    System.out.println(dianpuid+"        dianpuid");
                                    String s = list22.get(dianpuid);
                                    tvXuanzeDianpu.setText(s);
                                }
                            }

//                            Object itemsOnClick = null;
//                            ShangJiaRuZhuDianPuLeiXingEntity.dianpuleixing = shangJiaRuZhuDianPuLeiXingEntity.getResponse();
//                            dianPuLeiXingPopupWindow = new DianPuLeiXingPopupWindow(ShangJiaRuZhuActivity.this, (View.OnClickListener) itemsOnClick);
                        }
                    }
                });
    }


    @OnClick({R.id.image_back, R.id.tv_shenqingren, R.id.tv_phone, R.id.et_phone})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.image_back:
                finish();
                break;
            case R.id.tv_shenqingren:
                break;
            case R.id.tv_phone:
                break;
            case R.id.et_phone:
                break;
        }
    }
}
