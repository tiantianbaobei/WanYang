package com.wanyangproject.activity;

import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.baidu.mapapi.SDKInitializer;
import com.baidu.mapapi.map.BaiduMap;
import com.baidu.mapapi.map.BitmapDescriptor;
import com.baidu.mapapi.map.BitmapDescriptorFactory;
import com.baidu.mapapi.map.MapStatusUpdate;
import com.baidu.mapapi.map.MapStatusUpdateFactory;
import com.baidu.mapapi.map.MapView;
import com.baidu.mapapi.map.MarkerOptions;
import com.baidu.mapapi.map.OverlayOptions;
import com.baidu.mapapi.map.TextOptions;
import com.baidu.mapapi.model.LatLng;
import com.wanyangproject.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;


public class DiTuActivity extends AppCompatActivity {

    @BindView(R.id.image_back)
    ImageView imageBack;
    private MapView mapView;
    private BaiduMap map;
    private String city;
    private LatLng latLng;
    private Boolean isOK = true;
    private String lat;
    private String lng;
    private double weidu;
    private double jingdu;
    private String name;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_di_tu);
        ButterKnife.bind(this);

        Intent intent = getIntent();
        lat = intent.getStringExtra("lat");
        lng = intent.getStringExtra("lng");
        name = intent.getStringExtra("name");

        System.out.println(name + "    name");

        System.out.println(lat + "         lat");
        System.out.println(lng + "           lng");

        jingdu = Double.parseDouble(lat);
        weidu = Double.parseDouble(lng);


        initView();

    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        mapView.onSaveInstanceState(outState);
    }

    @Override
    protected void onResume() {
        super.onResume();
        mapView.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
        mapView.onPause();
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        mapView.onDestroy();
        map.setMyLocationEnabled(false);
    }

    private Bitmap getBitmapFromView(View view) {
        view.destroyDrawingCache();
        view.measure(View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED), View.MeasureSpec.UNSPECIFIED);
        view.layout(0, 0, view.getMeasuredWidth(), view.getMeasuredHeight());
        view.setDrawingCacheEnabled(true);
        Bitmap bitmap = view.getDrawingCache();
        return bitmap;
    }

    private void initView() {
        mapView = (MapView) findViewById(R.id.main_map);
//        获取Bitmap对象
        map = mapView.getMap();
//        map.setMyLocationEnabled(true);
        map.setMapType(BaiduMap.MAP_TYPE_NORMAL);


        LatLng point = new LatLng(weidu, jingdu);


        //设置经纬度（参数一是纬度，参数二是经度）
        MapStatusUpdate mapstatusupdate = MapStatusUpdateFactory.newLatLng(new LatLng(weidu, jingdu));
        //对地图的中心点进行更新，
        map.setMapStatus(mapstatusupdate);


//          构建Marker图标

//        BitmapDescriptor bitmap = BitmapDescriptorFactory
//                .fromResource(R.drawable.dingwei);

        View view = LayoutInflater.from(this).inflate(R.layout.baiduditu_maker, null);
        TextView tv_name=view.findViewById(R.id.tv_name);
        tv_name.setText(name);
        BitmapDescriptor bitmap = BitmapDescriptorFactory.fromBitmap(getBitmapFromView(view));


//          构建MarkerOption，用于在地图上添加Marker

        OverlayOptions option = new MarkerOptions()
//                .title(name)
                .position(point)
                .icon(bitmap);

//          在地图上添加Marker，并显示

        map.addOverlay(option);


//        //创建InfoWindow展示的view
//        Button button = new Button(getApplicationContext());
//        button.setBackgroundResource(R.drawable.dingwei);
//
////定义用于显示该InfoWindow的坐标点
//        LatLng pt = new LatLng(weidu, jingdu);
//
////创建InfoWindow , 传入 view， 地理坐标， y 轴偏移量
//        InfoWindow mInfoWindow = new InfoWindow(button, pt, -47);
//
////显示InfoWindow
//        map.showInfoWindow(mInfoWindow);




//
//        //定义文字所显示的坐标点
//        LatLng llText = new LatLng(weidu, jingdu);
//
////      构建文字Option对象，用于在地图上添加文字
//        OverlayOptions textOption = new TextOptions()
////                .bgColor(0xAAFFFF00)
//                .fontSize(30)
////                .fontColor(0xFFFF00FF)
//                .text(name)
//                .rotate(-30)
//                .position(llText);
//
////在地图上添加该文字对象并显示
//        map.addOverlay(textOption);
    }

    @OnClick(R.id.image_back)
    public void onViewClicked() {
        finish();
    }
}
