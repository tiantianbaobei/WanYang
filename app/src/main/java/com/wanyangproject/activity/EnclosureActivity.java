package com.wanyangproject.activity;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.tencent.smtt.sdk.TbsReaderView;
import com.wanyangproject.R;
import com.wanyangproject.base.BaseActivity;
import com.wanyangproject.net.util.Constant;
import com.wanyangproject.net.util.ToastUtil;
import com.wanyangproject.service.OfficeFileService;
import com.wanyangproject.utils.FileUtil;
import com.wanyangproject.utils.Permissions;
import com.wanyangproject.widget.other.WhorlView;
import com.wanyangproject.widget.uutils.utils.FileUtils;

public class EnclosureActivity extends BaseActivity implements TbsReaderView.ReaderCallback {

    private TbsReaderView readerView;
    private FrameLayout enclosureShow;
    private WhorlView enclosureWhorl;
    private TextView enclosureText;
    private LocalBroadcastManager broadcastManager;
    private String path = null;
    private BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {
            switch (intent.getAction()) {
                case Constant.ACTION_DOWNLOAD_INIT:
                    enclosureWhorl.setVisibility(View.VISIBLE);
                    enclosureText.setVisibility(View.VISIBLE);
                    enclosureWhorl.start();
                    enclosureText.setText("正在加载... 0%");
                    break;
                case Constant.ACTION_DOWNLOAD_LOADING:
                    int size = intent.getIntExtra(Constant.EXTRA_SIZE, 0);
                    enclosureText.setText("正在加载... " + size + "%");
                    break;
                case Constant.ACTION_DOWNLOAD_END:
                    path = intent.getStringExtra(Constant.ENCLOSURE_PATH);
                    Bundle bundle = new Bundle();
                    bundle.putString("filePath", path);
                    bundle.putString("tempPath", Constant.BASE_PATH + "temp");
                    if (readerView != null && readerView.preOpen(FileUtil.getExtensionName(path), false)) {
                        readerView.openFile(bundle);
                    }
                    enclosureShow.addView(readerView);
                    enclosureWhorl.stop();
                    enclosureWhorl.setVisibility(View.GONE);
                    enclosureText.setVisibility(View.GONE);
                    break;
            }
        }
    };

    public static void start(Context context, String enclosureUrl) {
        Intent intent = new Intent(context, EnclosureActivity.class);
        intent.putExtra(Constant.ENCLOSURE_URL, enclosureUrl);
        context.startActivity(intent);
    }

    @Override
    public int getContentResId() {
        return R.layout.activity_enclosure;
    }

    @Override
    public void init() {
        String enclosureUrl = getIntent().getStringExtra(Constant.ENCLOSURE_URL);
        if (enclosureUrl != null) {
            Permissions.verifyStoragePermissions(EnclosureActivity.this);
            broadcastManager = LocalBroadcastManager.getInstance(mContext);
            registerBroadcastReceiver();
            ImageView image_back = (ImageView) findViewById(R.id.image_back);
//            TextView barTitle = (TextView) findViewById(R.id.bar_title);
            image_back.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    finish();
                }
            });
//            barTitle.setText("文件预览");
            enclosureShow = (FrameLayout) findViewById(R.id.enclosure_show);
            enclosureWhorl = (WhorlView) findViewById(R.id.enclosure_whorl);
            enclosureText = (TextView) findViewById(R.id.enclosure_text);
            readerView = new TbsReaderView(mContext, this);
            OfficeFileService.startService(mContext, enclosureUrl);
        } else {
            ToastUtil.show(mContext, "No Url");
            finish();
        }
    }

    private void registerBroadcastReceiver() {
        IntentFilter filter = new IntentFilter();
        filter.addAction(Constant.ACTION_DOWNLOAD_INIT);
        filter.addAction(Constant.ACTION_DOWNLOAD_LOADING);
        filter.addAction(Constant.ACTION_DOWNLOAD_END);
        broadcastManager.registerReceiver(broadcastReceiver, filter);
    }

    @Override
    public void onCallBackAction(Integer integer, Object o, Object o1) {

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (broadcastManager != null) {
            broadcastManager.unregisterReceiver(broadcastReceiver);
        }
        if (readerView != null) {
            readerView.onStop();
        }
        if (enclosureWhorl != null) {
            enclosureWhorl.stop();
        }
        if (path != null) {
            FileUtils.delete(path);
        }
    }
}