package com.wanyangproject.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.webkit.JavascriptInterface;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebResourceResponse;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.wanyangproject.R;
import com.wanyangproject.entity.YouHuiQuanXiangQingEntity;
import com.wanyangproject.fuwuactivity.QiYeShenQingFuWuActivity;
import com.wanyangproject.utils.ContractUtils;
import com.wanyangproject.utils.WebViewUtils;
import com.zhy.http.okhttp.OkHttpUtils;
import com.zhy.http.okhttp.callback.StringCallback;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.Call;

public class YouHuiQuanXiangQingActivity extends AppCompatActivity {


    @BindView(R.id.image_back)
    ImageView imageBack;
    @BindView(R.id.tv_title)
    TextView tvTitle;
    @BindView(R.id.tv_neirong)
    TextView tvNeirong;
    @BindView(R.id.tv_time)
    TextView tvTime;
    @BindView(R.id.webView)
    WebView webView;
    @BindView(R.id.tv_liji_shenqing)
    TextView tvLijiShenqing;
    @BindView(R.id.relative_shenqing)
    RelativeLayout relativeShenqing;
    @BindView(R.id.tv_fujian_lianjie)
    TextView tvFujianLianjie;
    @BindView(R.id.relative_xiazai)
    RelativeLayout relativeXiazai;
    private String id;
    private String content;
    private WebSettings mWebSettings;
    private YouHuiQuanXiangQingEntity youHuiQuanXiangQingEntity;
    private ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_you_hui_quan_xiang_qing);
        ButterKnife.bind(this);


        Intent intent = getIntent();
        id = intent.getStringExtra("id");
        System.out.println(id + "     优惠券的id");

        mWebSettings = webView.getSettings();
        mWebSettings.setJavaScriptEnabled(true);  //允许加载javascript
        webView.getSettings().setLayoutAlgorithm(WebSettings.LayoutAlgorithm.SINGLE_COLUMN);
        webView.getSettings().setLoadWithOverviewMode(true);
        mWebSettings.setSupportZoom(false);     //允许缩放
        mWebSettings.setBuiltInZoomControls(false); //原网页基础上缩放
        mWebSettings.setUseWideViewPort(false);   //任意比例缩放
//        mWebSettings.setUseWideViewPort(true); //将图片调整到适合webview的大小
//        mWebSettings.setLoadWithOverviewMode(true); // 缩放至屏幕的大小
        // 支持通过JS打开新窗口
        mWebSettings.setJavaScriptCanOpenWindowsAutomatically(true);
        mWebSettings.setLoadWithOverviewMode(true);
        //不显示webview缩放按钮
        mWebSettings.setDisplayZoomControls(false);
//        String cacheDirPath = getFilesDir().getAbsolutePath() + APP_CACAHE_DIRNAME;
//      String cacheDirPath = getCacheDir().getAbsolutePath()+Constant.APP_DB_DIRNAME;
        //设置数据库缓存路径
//        webView.getSettings().setDatabasePath(cacheDirPath);
        //设置  Application Caches 缓存目录
//        webView.getSettings().setAppCachePath(cacheDirPath);
        //开启 Application Caches 功能
        webView.getSettings().setAppCacheEnabled(true);
        webView.setWebViewClient(new WebViewClient());

        //        webView.setWebChromeClient(new WebChromeClient());
        webView.addJavascriptInterface(new JavaScriptInterface(), "JSInterface");


//        优惠券详情的网络请求
        initYouHuiQuanXiangQingHttp();


        initView();
    }


    //    优惠券详情的网络请求
    private void initYouHuiQuanXiangQingHttp() {

        progressDialog = new ProgressDialog(YouHuiQuanXiangQingActivity.this);
        progressDialog.setTitle("提示");
        progressDialog.setMessage("请等待...");
        progressDialog.setCancelable(false);
        progressDialog.show();

        OkHttpUtils.post()
                .url(ContractUtils.LOGIN_URL + "Life/preferential")
                .addHeader("token", ContractUtils.getTOKEN(YouHuiQuanXiangQingActivity.this))
                .addParams("parkId", ContractUtils.getParkId(YouHuiQuanXiangQingActivity.this))
                .addParams("Preferences_id", id)
                .build()
                .execute(new StringCallback() {
                    @Override
                    public void onError(Call call, Exception e, int id) {
                        progressDialog.dismiss();

                    }

                    @Override
                    public void onResponse(String response, int id) {
                        ContractUtils.Code500(YouHuiQuanXiangQingActivity.this, response);
                        System.out.println(response + "           优惠券详情的网络请求");

                        if (response.indexOf("200") != -1) {
                            Gson gson = new Gson();
                            youHuiQuanXiangQingEntity = gson.fromJson(response, YouHuiQuanXiangQingEntity.class);


                            if (youHuiQuanXiangQingEntity.getResponse().getShopPreferences().getFujian_name() != null) {
                                if (youHuiQuanXiangQingEntity.getResponse().getShopPreferences().getFujian_name().equals("")) {
                                    tvFujianLianjie.setText("未命名");
                                }else{
                                    tvFujianLianjie.setText(youHuiQuanXiangQingEntity.getResponse().getShopPreferences().getFujian_name());
                                }
                            }



                            if(youHuiQuanXiangQingEntity.getResponse().getShopPreferences().getFujian().equals("null") || youHuiQuanXiangQingEntity.getResponse().getShopPreferences().getFujian().equals("")){
                                relativeXiazai.setVisibility(View.GONE);
                            }else{
                                relativeXiazai.setVisibility(View.VISIBLE);
                            }





                            if (youHuiQuanXiangQingEntity.getResponse().getShopPreferences().getTitle() == null) {

                            } else {
                                tvTitle.setText(youHuiQuanXiangQingEntity.getResponse().getShopPreferences().getTitle());
                            }


                            if (youHuiQuanXiangQingEntity.getResponse().getShopPreferences().getIntroduction() == null) {

                            } else {
                                tvNeirong.setText(youHuiQuanXiangQingEntity.getResponse().getShopPreferences().getIntroduction());
                            }


                            if (youHuiQuanXiangQingEntity.getResponse().getShopPreferences().getAdd_time() == null) {

                            } else {
                                tvTime.setText(youHuiQuanXiangQingEntity.getResponse().getShopPreferences().getAdd_time());
                            }


                            if (youHuiQuanXiangQingEntity.getResponse().getShopPreferences().getContent() == null) {

                            } else {
                                //                            内容
                                content = youHuiQuanXiangQingEntity.getResponse().getShopPreferences().getContent();
                                webView.loadDataWithBaseURL("http://www.baidu.com", getNewContent(content), "text/html", "UTF-8", null);

                            }
                        }
                    }
                });
    }


    private void initView() {
        webView.setWebViewClient(new WebViewClient() {
            /**
             * 拦截 url 跳转,在里边添加点击链接跳转或者操作
             */
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, WebResourceRequest request) {
                return WebViewUtils.shouldOverrideUrlLoading(view, request, YouHuiQuanXiangQingActivity.this);
            }

            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                System.out.println("66666666666666666666");
                return WebViewUtils.shouldOverrideUrlLoading(view, url, YouHuiQuanXiangQingActivity.this);

            }


            /**
             * 在开始加载网页时会回调
             */
            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                super.onPageStarted(view, url, favicon);
                System.out.println("111111111111111111");
            }

            /**
             * 加载错误的时候会回调，在其中可做错误处理，比如再请求加载一次，或者提示404的错误页面
             */
            @Override
            public void onReceivedError(WebView view, WebResourceRequest request, WebResourceError error) {
                super.onReceivedError(view, request, error);

            }


            /**
             * 在结束加载网页时会回调
             */
            @Override
            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(view, url);
                System.out.println("chenggongchenggongchenggong");

                if(progressDialog != null){
                    progressDialog.dismiss();
                }

            }

            /**
             * 当接收到https错误时，会回调此函数，在其中可以做错误处理
             */
            @Override
            public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
                super.onReceivedError(view, errorCode, description, failingUrl);
            }

            /**
             * 在每一次请求资源时，都会通过这个函数来回调
             */
            public WebResourceResponse shouldInterceptRequest(WebView view, String url) {
                return null;
            }


            @Override
            public void onPageCommitVisible(WebView view, String url) {
                super.onPageCommitVisible(view, url);

                if(progressDialog != null){
                    progressDialog.dismiss();
                }

            }
        });
    }


    /**
     * 将html文本内容中包含img标签的图片，宽度变为屏幕宽度，高度根据宽度比例自适应
     **/
    public static String getNewContent(String htmltext) {
        try {
            Document doc = Jsoup.parse(htmltext);
            Elements elements = doc.getElementsByTag("img");
            for (Element element : elements) {
                element.attr("width", "100%").attr("height", "auto");
            }

            return doc.toString();
        } catch (Exception e) {
            return htmltext;
        }
    }

    @OnClick({R.id.image_back, R.id.tv_fujian_lianjie, R.id.tv_title, R.id.tv_neirong, R.id.tv_time, R.id.webView, R.id.tv_liji_shenqing, R.id.relative_shenqing})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.image_back:
                finish();
                break;
            case R.id.tv_fujian_lianjie:
                String fujian = youHuiQuanXiangQingEntity.getResponse().getShopPreferences().getFujian();
                if (fujian.equals("null")) {

//                    ToastUtil.show(YouHuiQuanXiangQingActivity.this, "附件地址不存在");
                    return;
                } else {
                    EnclosureActivity.start(YouHuiQuanXiangQingActivity.this, fujian);
                }


//                Intent intent11 = new Intent(YouHuiQuanXiangQingActivity.this,FuJianLianJieActivity.class);
//                intent11.putExtra("lianjie",youHuiQuanXiangQingEntity.getResponse().getShopPreferences().getFujian());
//                startActivity(intent11);
                break;
            case R.id.tv_title:
                break;
            case R.id.tv_neirong:
                break;
            case R.id.tv_time:
                break;
            case R.id.webView:
                break;
            case R.id.tv_liji_shenqing:
                Intent intent = new Intent(YouHuiQuanXiangQingActivity.this, QiYeShenQingFuWuActivity.class);
                intent.putExtra("id", youHuiQuanXiangQingEntity.getResponse().getShopPreferences().getId());
                intent.putExtra("title", youHuiQuanXiangQingEntity.getResponse().getShopPreferences().getTitle());
                intent.putExtra("neirong", youHuiQuanXiangQingEntity.getResponse().getShopPreferences().getIntroduction());
                startActivity(intent);
                break;
            case R.id.relative_shenqing:
                Intent intent1 = new Intent(YouHuiQuanXiangQingActivity.this, QiYeShenQingFuWuActivity.class);
                intent1.putExtra("id", youHuiQuanXiangQingEntity.getResponse().getShopPreferences().getId());
                intent1.putExtra("title", youHuiQuanXiangQingEntity.getResponse().getShopPreferences().getTitle());
                intent1.putExtra("neirong", youHuiQuanXiangQingEntity.getResponse().getShopPreferences().getIntroduction());
                startActivity(intent1);
                break;
        }
    }


    class JavaScriptInterface {
        @JavascriptInterface
        public void processFormInputs(String html) {
//            LogUtils.e("html-->" + html);
            System.out.println(html);
        }
    }


}
