package com.wanyangproject.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.Display;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.wanyangproject.R;
import com.wanyangproject.entity.DuanXinShiBaiEntity;
import com.wanyangproject.entity.HuoQuYanZhengMaEntity;
import com.wanyangproject.entity.ShangJiaRuZhuDuanXinEntity;
import com.wanyangproject.entity.WeiXinBangDingEntity;
import com.wanyangproject.my.ShangJiaRuZhuActivity;
import com.wanyangproject.utils.ContractUtils;
import com.zhy.http.okhttp.OkHttpUtils;
import com.zhy.http.okhttp.callback.StringCallback;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.Call;

public class BangDingShouJiHaoActivity extends AppCompatActivity {

    @BindView(R.id.image_back)
    ImageView imageBack;
    @BindView(R.id.et_phone)
    EditText etPhone;
    @BindView(R.id.et_yanzhengma)
    EditText etYanzhengma;
    @BindView(R.id.tv_send)
    TextView tvSend;
    @BindView(R.id.btn_wancheng)
    Button btnWancheng;
    private int num;
    private ShangJiaRuZhuDuanXinEntity shangJiaRuZhuDuanXinEntity;
    private String wxopenid;
    private HuoQuYanZhengMaEntity huoQuYanZhengMaEntity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bang_ding_shou_ji_hao);
        ButterKnife.bind(this);


        Intent intent = getIntent();
        wxopenid = intent.getStringExtra("wxopenid");
        System.out.println(wxopenid+"      接收wxopenid");

    }


    @OnClick({R.id.image_back, R.id.et_phone, R.id.et_yanzhengma, R.id.tv_send, R.id.btn_wancheng})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.image_back:
                finish();
                break;
            case R.id.et_phone:
                break;
            case R.id.et_yanzhengma:
                break;
            case R.id.tv_send:
                if(ContractUtils.isChinaPhoneLegal(etPhone.getText().toString().trim()) == false){
                    Toast.makeText(this, "请输入正确的手机号", Toast.LENGTH_SHORT).show();
                    return;
                }
                if(etPhone.getText().toString().trim().equals("")) {
                    Toast.makeText(this, "请输入手机号", Toast.LENGTH_SHORT).show();
                }else{
//                获取验证码的网络请求
                    initHuoQuYanZhengMaHttp();
                }
                break;
            case R.id.btn_wancheng:
                if(etPhone.getText().toString().trim().equals("")){
                    Toast.makeText(this, "请填写手机号", Toast.LENGTH_SHORT).show();
                }else {
                    //                完成的网络请求
                    initWanChengHttp();
                }
                break;
        }
    }


//    完成的网络请求
    private void initWanChengHttp() {

        if(ContractUtils.isChinaPhoneLegal(etPhone.getText().toString().trim()) == false){
            Toast.makeText(this, "请输入正确的手机号", Toast.LENGTH_SHORT).show();
            return;
        }
        ContractUtils.isChinaPhoneLegal(etPhone.getText().toString().trim());


        if(huoQuYanZhengMaEntity == null){
            Toast.makeText(this, "请填写正确的验证码！", Toast.LENGTH_SHORT).show();
            return;
        }


        OkHttpUtils.post()
                .url(ContractUtils.LOGIN_URL+"user/login")
                .addParams("phone",etPhone.getText().toString().trim())
                .addParams("code",etYanzhengma.getText().toString().trim())
                .addParams("authId",huoQuYanZhengMaEntity.getResponse().getAuthId())
                .addParams("wxkey",wxopenid)
                .build()
                .execute(new StringCallback() {
                    @Override
                    public void onError(Call call, Exception e, int id) {

                    }

                    @Override
                    public void onResponse(String response, int id) {
                        System.out.println(response+"              微信绑定手机号的网络请求");
                        if(response.indexOf("200") != -1){
                            Gson gson = new Gson();
                            WeiXinBangDingEntity weiXinBangDingEntity = gson.fromJson(response, WeiXinBangDingEntity.class);

                            if(weiXinBangDingEntity.getResponse().getNewX().equals("0")){//0 已注册  1 未注册
                                Toast.makeText(BangDingShouJiHaoActivity.this, "绑定成功", Toast.LENGTH_SHORT).show();
                                Intent intent = new Intent(BangDingShouJiHaoActivity.this,HomeActivity.class);
                                startActivity(intent);
//                                finish();
                            }else if(weiXinBangDingEntity.getResponse().getNewX().equals("1")){//0 已注册  1 未注册
                                initAlertDialog();
                            }
                        }else if(response.indexOf("400") != -1){
                            ContractUtils.Code400(BangDingShouJiHaoActivity.this,response);
                        }
                    }
                });
    }





    private void initAlertDialog() {
        final AlertDialog dialog = new AlertDialog.Builder(BangDingShouJiHaoActivity.this).create();
        dialog.show();
        dialog.getWindow().setContentView(R.layout.weixin_bangding_alertdialog);
        dialog.getWindow().setBackgroundDrawableResource(R.drawable.shape_my_youke);
        WindowManager windowManager1 = getWindowManager();
        Display defaultDisplay1 = windowManager1.getDefaultDisplay();
        WindowManager.LayoutParams attributes1 = dialog.getWindow().getAttributes();
        attributes1.width= (int) (defaultDisplay1.getWidth()*0.8);
        dialog.getWindow().setAttributes(attributes1);
        dialog.setCancelable(false);
        dialog.getWindow().findViewById(R.id.tv_queding).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(BangDingShouJiHaoActivity.this,HomeActivity.class);
                startActivity(intent);
                dialog.dismiss();
            }
        });
    }





    //   获取验证码的网络请求
    private void initHuoQuYanZhengMaHttp() {
        if(ContractUtils.isChinaPhoneLegal(etPhone.getText().toString().trim()) == false){
            Toast.makeText(this, "请输入正确的手机号", Toast.LENGTH_SHORT).show();
            return;
        }
        ContractUtils.isChinaPhoneLegal(etPhone.getText().toString().trim());


        OkHttpUtils.post()
                .url(ContractUtils.LOGIN_URL+"index/sendsms")
                .addParams("phone",etPhone.getText().toString().trim())
                .build()
                .execute(new StringCallback() {
                    @Override
                    public void onError(Call call, Exception e, int id) {

                    }

                    @Override
                    public void onResponse(String response, int id) {
                        System.out.println(response+"     获取验证码的网络请求");
                        if(response.indexOf("200") != -1){
                            new Thread() {
                                @Override
                                public void run() {
                                    super.run();
                                    try {
                                        num = 60;
                                        while (num > 0) {
//                                不可点击
                                            tvSend.setClickable(false);
                                            runOnUiThread(new Runnable() {
                                                @Override
                                                public void run() {
//                                        tvHuoquYanzhengma.setBackgroundResource(R.drawable.shape_huoqu_huise);
//                                        tvHuoquYanzhengma.setText("倒计时"+num+"s");
                                                    tvSend.setText(num + "s后重新获取");
                                                }
                                            });

                                            Thread.sleep(1000);
                                            num = num - 1;

                                        }

                                        runOnUiThread(new Runnable() {
                                            @Override
                                            public void run() {
                                                tvSend.setClickable(true);
//                                    tvHuoquYanzhengma.setBackgroundResource(R.drawable.shape_huoqu);
                                                tvSend.setText("重新获取");
                                            }
                                        });

                                    } catch (InterruptedException e) {
                                        e.printStackTrace();
                                    }

                                }
                            }.start();

                            Gson gson = new Gson();
                            huoQuYanZhengMaEntity = gson.fromJson(response, HuoQuYanZhengMaEntity.class);
                            Toast.makeText(BangDingShouJiHaoActivity.this, huoQuYanZhengMaEntity.getMsg(), Toast.LENGTH_SHORT).show();
                        }else if(response.indexOf("400") != -1){
                            Gson gson = new Gson();
                            DuanXinShiBaiEntity duanXinShiBaiEntity = gson.fromJson(response, DuanXinShiBaiEntity.class);
                            Toast.makeText(BangDingShouJiHaoActivity.this, duanXinShiBaiEntity.getMsg(), Toast.LENGTH_SHORT).show();
                        }
                    }
                });


    }










}
