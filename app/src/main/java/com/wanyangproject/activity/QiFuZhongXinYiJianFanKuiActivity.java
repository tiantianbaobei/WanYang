package com.wanyangproject.activity;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.wanyangproject.R;
import com.wanyangproject.entity.LiuYanFanKuiEntity;
import com.wanyangproject.utils.ContractUtils;
import com.zhy.http.okhttp.OkHttpUtils;
import com.zhy.http.okhttp.callback.StringCallback;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.Call;

public class QiFuZhongXinYiJianFanKuiActivity extends AppCompatActivity {

    @BindView(R.id.image_back)
    ImageView imageBack;
    @BindView(R.id.tv_fankui)
    TextView tvFankui;
    @BindView(R.id.et_jianyi)
    EditText etJianyi;
    @BindView(R.id.btn_tijiao)
    Button btnTijiao;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_qi_fu_zhong_xin_yi_jian_fan_kui);
        ButterKnife.bind(this);
    }

    @OnClick({R.id.image_back, R.id.tv_fankui, R.id.et_jianyi, R.id.btn_tijiao})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.image_back:
                finish();
                break;
            case R.id.tv_fankui:
                break;
            case R.id.et_jianyi:
                break;
            case R.id.btn_tijiao:
                if(etJianyi.getText().toString().equals("")){
                    Toast.makeText(this, "请输入您的意见或建议", Toast.LENGTH_SHORT).show();
                }else{
                    //        企服中心的意见反馈的网络请求
                    initQiFuZhongXinYiJianFanKuiHttp();
                }

                break;
        }
    }

//    企服中心的意见反馈的网络请求
    private void initQiFuZhongXinYiJianFanKuiHttp() {
        final ProgressDialog progressDialog = new ProgressDialog(QiFuZhongXinYiJianFanKuiActivity.this);
        progressDialog.setTitle("提示");
        progressDialog.setMessage("请等待...");
        progressDialog.setCancelable(false);
        progressDialog.show();
        OkHttpUtils.post()
                .url(ContractUtils.LOGIN_URL+"news/feedback")
                .addHeader("token",ContractUtils.getTOKEN(QiFuZhongXinYiJianFanKuiActivity.this))
                .addParams("parkId",ContractUtils.getParkId(QiFuZhongXinYiJianFanKuiActivity.this))
                .addParams("typeId","2")//1:园区招商，2：企服中心，3:政策法规
                .addParams("content",etJianyi.getText().toString().trim())
                .build()
                .execute(new StringCallback() {
                    @Override
                    public void onError(Call call, Exception e, int id) {
                        System.out.println(e+"     园区招商意见反馈eee");
                        progressDialog.dismiss();
                    }

                    @Override
                    public void onResponse(String response, int id) {
                        System.out.println(response+"    园区招商的意见反馈的网络请求 ");
                        ContractUtils.Code500(QiFuZhongXinYiJianFanKuiActivity.this,response);
                        progressDialog.dismiss();
                        if(response.indexOf("200") != -1){
                            Gson gson = new Gson();
                            LiuYanFanKuiEntity yiJianFanKuiEntity = gson.fromJson(response, LiuYanFanKuiEntity.class);
                            Toast.makeText(QiFuZhongXinYiJianFanKuiActivity.this, "发表成功", Toast.LENGTH_SHORT).show();
                            finish();
                        }else{
                            Toast.makeText(QiFuZhongXinYiJianFanKuiActivity.this, "失败", Toast.LENGTH_SHORT).show();
                        }
                    }
                });
    }
}
