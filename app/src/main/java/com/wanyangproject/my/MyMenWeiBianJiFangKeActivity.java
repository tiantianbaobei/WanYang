package com.wanyangproject.my;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.wanyangproject.R;
import com.wanyangproject.entity.QueRenRuYuanEntity;
import com.wanyangproject.utils.ContractUtils;
import com.zhy.http.okhttp.OkHttpUtils;
import com.zhy.http.okhttp.callback.StringCallback;

import java.text.SimpleDateFormat;
import java.util.Date;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.Call;

public class MyMenWeiBianJiFangKeActivity extends AppCompatActivity {

    @BindView(R.id.image_back)
    ImageView imageBack;
    @BindView(R.id.tv_name)
    TextView tvName;
    @BindView(R.id.tv_phone)
    TextView tvPhone;
    @BindView(R.id.tv_number)
    TextView tvNumber;
    @BindView(R.id.tv_time)
    TextView tvTime;
    @BindView(R.id.tv_chepaihao)
    TextView tvChepaihao;
    @BindView(R.id.tv_shijidaofang_renshu)
    TextView tvShijidaofangRenshu;
    @BindView(R.id.btn_queren)
    Button btnQueren;
    @BindView(R.id.et_renshu)
    EditText etRenshu;
    private String code;
    private String name;
    private String phone;
    private String num;
    private String time;
    private String chepai;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_men_wei_bian_ji_fang_ke);
        ButterKnife.bind(this);


        Intent intent = getIntent();
        code = intent.getStringExtra("code");
        name = intent.getStringExtra("name");
        phone = intent.getStringExtra("phone");
        num = intent.getStringExtra("num");
        time = intent.getStringExtra("time");
        chepai = intent.getStringExtra("chepai");

        if(tvName != null){
            tvName.setText("访客姓名："+name);
        }

        if(phone != null){
            tvPhone.setText("访客电话："+phone);
        }

        if(num != null){
            tvNumber.setText("预计到访人数："+num);
        }


        if(time != null){
            tvTime.setText("预计到访时间："+time);
        }

        if(chepai != null){
            tvChepaihao.setText("车牌号：" +chepai);
        }

        System.out.println(code + "        code");


    }

    @OnClick({R.id.image_back, R.id.tv_name, R.id.tv_phone, R.id.tv_number, R.id.tv_time, R.id.tv_chepaihao, R.id.tv_shijidaofang_renshu, R.id.btn_queren})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.image_back:
                finish();
                break;
            case R.id.tv_name:
                break;
            case R.id.tv_phone:
                break;
            case R.id.tv_number:
                break;
            case R.id.tv_time:
                break;
            case R.id.tv_chepaihao:
                break;
            case R.id.tv_shijidaofang_renshu:
                break;
            case R.id.btn_queren:
                if(etRenshu.getText().toString().trim().equals("") || etRenshu.getText().toString().trim().equals("0")){
                    Toast.makeText(this, "请输入到访人数", Toast.LENGTH_SHORT).show();
                }else{
                    //                确定入园的网络请求
                    initiQueDingRuYuanHttp();
                }

                break;
        }
    }









    /*
    * 将时间戳转换为时间
     */
    public String stampToDate(String s) {
        String res;
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        long lt = new Long(s);
        Date date = new Date(lt);
        res = simpleDateFormat.format(date);
        return res;
    }






    //    确定入园的网络请求
    private void initiQueDingRuYuanHttp() {
        OkHttpUtils.post()
                .url(ContractUtils.LOGIN_URL + "dengji/visit")
                .addHeader("token", ContractUtils.getTOKEN(MyMenWeiBianJiFangKeActivity.this))
                .addParams("num", etRenshu.getText().toString().trim())
                .addParams("parkId",ContractUtils.getParkId(MyMenWeiBianJiFangKeActivity.this))
                .addParams("code",code)
                .build()
                .execute(new StringCallback() {
                    @Override
                    public void onError(Call call, Exception e, int id) {

                    }

                    @Override
                    public void onResponse(String response, int id) {
                        ContractUtils.Code500(MyMenWeiBianJiFangKeActivity.this,response);
                        System.out.println(response+"          确定入园的网络请求");

                        if(response.indexOf("200") != -1){
                            Gson gson = new Gson();
                            QueRenRuYuanEntity queRenRuYuanEntity = gson.fromJson(response, QueRenRuYuanEntity.class);
                            Toast.makeText(MyMenWeiBianJiFangKeActivity.this, queRenRuYuanEntity.getResponse().getMes(), Toast.LENGTH_SHORT).show();
                            finish();
                        }else{
                           ContractUtils.Code400(MyMenWeiBianJiFangKeActivity.this,response);
                        }
                    }
                });
    }
}
