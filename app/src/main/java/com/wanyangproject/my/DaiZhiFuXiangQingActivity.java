package com.wanyangproject.my;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.wanyangproject.R;
import com.wanyangproject.entity.DingDanXiangQingEntity;
import com.wanyangproject.entity.ShanChuDingDanEntity;
import com.wanyangproject.myadapter.DaiZhiFuXiangQingAdapter;
import com.wanyangproject.shouye.DingDanXiangQingActivity;
import com.wanyangproject.shouye.ZhiFuActivity;
import com.wanyangproject.utils.ContractUtils;
import com.zhy.http.okhttp.OkHttpUtils;
import com.zhy.http.okhttp.callback.StringCallback;

import java.text.SimpleDateFormat;
import java.util.Date;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.Call;

public class DaiZhiFuXiangQingActivity extends AppCompatActivity {

    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    @BindView(R.id.image_back)
    ImageView imageBack;
    @BindView(R.id.tv_heji_money)
    TextView tvHejiMoney;
    @BindView(R.id.btn_quzhifu)
    Button btnQuzhifu;
    @BindView(R.id.tv_shouhuoren)
    TextView tvShouhuoren;
    @BindView(R.id.tv_shoujihao)
    TextView tvShoujihao;
    @BindView(R.id.tv_address)
    TextView tvAddress;
    @BindView(R.id.tv_peisongfei)
    TextView tvPeisongfei;
    @BindView(R.id.tv_beizhu)
    TextView tvBeizhu;
    @BindView(R.id.tv_dingdan_bianhao)
    TextView tvDingdanBianhao;
    @BindView(R.id.tv_chuanjianshijian)
    TextView tvChuanjianshijian;
    @BindView(R.id.tv_title)
    TextView tvTitle;
    @BindView(R.id.relative_ziqu)
    RelativeLayout relativeZiqu;
    @BindView(R.id.relative_peisong)
    RelativeLayout relativePeisong;
    @BindView(R.id.tv_delete)
    TextView tvDelete;
    @BindView(R.id.tv_moren)
    TextView tvMoren;
    private DaiZhiFuXiangQingAdapter daiZhiFuXiangQingAdapter;
    private String dingdanhao;
    private DingDanXiangQingEntity dingDanXiangQingEntity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dai_zhi_fu_xiang_qing);
        ButterKnife.bind(this);

        Intent intent = getIntent();
        dingdanhao = intent.getStringExtra("dingdanhao");
        System.out.println(dingdanhao + "        详情接收的订单号");

        initView();

//        订单详情的网络请求
        initXiangQIngHttp();
    }

    //    订单详情的网络请求
    private void initXiangQIngHttp() {
        OkHttpUtils.post()
                .url(ContractUtils.LOGIN_URL + "user/orderdetails")
                .addHeader("token", ContractUtils.getTOKEN(DaiZhiFuXiangQingActivity.this))
                .addParams("parkId", ContractUtils.getParkId(DaiZhiFuXiangQingActivity.this))
                .addParams("order_sn", dingdanhao)
                .build()
                .execute(new StringCallback() {
                    @Override
                    public void onError(Call call, Exception e, int id) {
                        System.out.println(e + "            eeee订单详情");
                    }

                    @Override
                    public void onResponse(String response, int id) {
                        ContractUtils.Code500shibai(DaiZhiFuXiangQingActivity.this, response);
                        System.out.println(response + "        订单详情的网络请求");

                        if (response.indexOf("200") != -1) {
                            Gson gson = new Gson();
                            dingDanXiangQingEntity = gson.fromJson(response, DingDanXiangQingEntity.class);
                            daiZhiFuXiangQingAdapter = new DaiZhiFuXiangQingAdapter(DaiZhiFuXiangQingActivity.this, dingDanXiangQingEntity.getResponse().getGoods());
                            LinearLayoutManager manager = new LinearLayoutManager(DaiZhiFuXiangQingActivity.this);
                            recyclerView.setLayoutManager(manager);
                            recyclerView.setAdapter(daiZhiFuXiangQingAdapter);


                            if (dingDanXiangQingEntity.getResponse().getAddr_type().equals("1")) {
                                tvMoren.setVisibility(View.VISIBLE);
                            }else if(dingDanXiangQingEntity.getResponse().getAddr_type().equals("2")){
                                tvMoren.setVisibility(View.GONE);
                            }

                            if (dingDanXiangQingEntity.getResponse().getShop().getCost().equals("")) {
                                dingDanXiangQingEntity.getResponse().getShop().setCost("0");
                            }


                            if (dingDanXiangQingEntity.getResponse().getAddres_type().equals("1")) {
                                relativePeisong.setVisibility(View.VISIBLE);
                                relativeZiqu.setVisibility(View.GONE);
                                if (dingDanXiangQingEntity.getResponse().getAddres_name() == null) {

                                } else {
                                    tvShouhuoren.setText("收货人：" + dingDanXiangQingEntity.getResponse().getAddres_name());
                                }


                                if (dingDanXiangQingEntity.getResponse().getAddres_phone() == null) {

                                } else {
                                    tvShoujihao.setText(dingDanXiangQingEntity.getResponse().getAddres_phone());
                                }

                                if (dingDanXiangQingEntity.getResponse().getAddres_add() == null) {

                                } else {
                                    tvAddress.setText("收货地址：" + dingDanXiangQingEntity.getResponse().getAddres_add());
                                }

                            } else if (dingDanXiangQingEntity.getResponse().getAddres_type().equals("2")) {
                                relativeZiqu.setVisibility(View.VISIBLE);
                                relativePeisong.setVisibility(View.GONE);
                            }


                            if (dingDanXiangQingEntity.getResponse().getShop().getCost() == null) {

                            } else {
                                tvPeisongfei.setText("¥" + dingDanXiangQingEntity.getResponse().getShop().getCost());
                            }


//                            备注
                            if(dingDanXiangQingEntity.getResponse().getRemarks() != null){
                                tvBeizhu.setText("备注："+dingDanXiangQingEntity.getResponse().getRemarks());
                            }


                            if (dingDanXiangQingEntity.getResponse().getOrder_sn() == null) {

                            } else {
                                tvDingdanBianhao.setText("订单编号：" + dingDanXiangQingEntity.getResponse().getOrder_sn());
                            }


                            if (dingDanXiangQingEntity.getResponse().getAdd_time() == null) {

                            } else {
                                tvChuanjianshijian.setText("创建时间：" + dingDanXiangQingEntity.getResponse().getAdd_time());
                            }


                            if (dingDanXiangQingEntity.getResponse().getShop().getNickname() == null) {

                            } else {
                                tvTitle.setText(dingDanXiangQingEntity.getResponse().getShop().getNickname());
                            }


                            if (dingDanXiangQingEntity.getResponse().getMoney() == null) {

                            } else {
                                tvHejiMoney.setText("¥" + dingDanXiangQingEntity.getResponse().getMoney());
                            }

                        }
                    }
                });
    }


    /*
* 将时间戳转换为时间
*/
    public String stampToDate(String s) {
        String res;
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        long lt = new Long(s);
        Date date = new Date(lt);
        res = simpleDateFormat.format(date);
        return res;
    }


    private void initView() {
//        daiZhiFuXiangQingAdapter = new DaiZhiFuXiangQingAdapter(this);
//        LinearLayoutManager manager = new LinearLayoutManager(this);
//        recyclerView.setLayoutManager(manager);
//        recyclerView.setAdapter(daiZhiFuXiangQingAdapter);
    }

    @OnClick({R.id.tv_delete, R.id.image_back, R.id.tv_heji_money, R.id.btn_quzhifu, R.id.tv_shouhuoren, R.id.tv_shoujihao, R.id.tv_address, R.id.tv_peisongfei, R.id.tv_beizhu, R.id.tv_dingdan_bianhao, R.id.tv_chuanjianshijian})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.image_back:
                finish();
                break;
//            删除按钮
            case R.id.tv_delete:
//                删除待支付订单的网络请求
                initShanChuHttp();
                break;
            case R.id.tv_heji_money:
                break;
            case R.id.btn_quzhifu:
                Intent intent = new Intent(DaiZhiFuXiangQingActivity.this, ZhiFuActivity.class);
                intent.putExtra("money",dingDanXiangQingEntity.getResponse().getMoney());
                intent.putExtra("dingdanhao",dingDanXiangQingEntity.getResponse().getOrder_sn());
                startActivity(intent);
                finish();


//                if (dingDanXiangQingEntity.getResponse().getAddres_add() != null) {
//                    if (dingDanXiangQingEntity.getResponse().getAddres_add().length() > 0) {
//                        Intent intent = new Intent(DaiZhiFuXiangQingActivity.this, ZhiFuActivity.class);
//                        intent.putExtra("shifikuan", dingDanXiangQingEntity.getResponse().getMoney());
//                        intent.putExtra("dingdanhao", dingDanXiangQingEntity.getResponse().getOrder_sn());
//                        startActivity(intent);
//                    } else {
//                        Intent intent = new Intent(DaiZhiFuXiangQingActivity.this, DingDanXiangQingActivity.class);
//                        intent.putExtra("peisongfei", dingDanXiangQingEntity.getResponse().getShop().getCost());
//                        intent.putExtra("zongjiage", dingDanXiangQingEntity.getResponse().getMoney());
//                        intent.putExtra("shopid", dingDanXiangQingEntity.getResponse().getShop_id());
//                        intent.putExtra("dingdanhao", dingDanXiangQingEntity.getResponse().getOrder_sn());
//                        startActivity(intent);
//                    }
//                } else {
//                    Intent intent = new Intent(DaiZhiFuXiangQingActivity.this, DingDanXiangQingActivity.class);
//                    intent.putExtra("peisongfei", dingDanXiangQingEntity.getResponse().getShop().getCost());
//                    intent.putExtra("zongjiage", dingDanXiangQingEntity.getResponse().getMoney());
//                    intent.putExtra("shopid", dingDanXiangQingEntity.getResponse().getShop_id());
//                    intent.putExtra("dingdanhao", dingDanXiangQingEntity.getResponse().getOrder_sn());
//                    startActivity(intent);
//                }
                break;
            case R.id.tv_shouhuoren:
                break;
            case R.id.tv_shoujihao:
                break;
            case R.id.tv_address:
                break;
            case R.id.tv_peisongfei:
                break;
            case R.id.tv_beizhu:
                break;
            case R.id.tv_dingdan_bianhao:
                break;
            case R.id.tv_chuanjianshijian:
                break;
        }
    }


    //    删除待支付订单的网络请求
    private void initShanChuHttp() {
        OkHttpUtils.post()
                .url(ContractUtils.LOGIN_URL + "user/orderdel")
                .addHeader("token", ContractUtils.getTOKEN(DaiZhiFuXiangQingActivity.this))
                .addParams("parkId", ContractUtils.getParkId(DaiZhiFuXiangQingActivity.this))
                .addParams("id", dingdanhao)
                .build()
                .execute(new StringCallback() {
                    @Override
                    public void onError(Call call, Exception e, int id) {

                    }

                    @Override
                    public void onResponse(String response, int id) {
                        System.out.println(response + "             删除待支付订单的网络请求");
                        ContractUtils.Code500(DaiZhiFuXiangQingActivity.this, response);
                        if (response.indexOf("200") != -1) {
                            Gson gson = new Gson();
                            ShanChuDingDanEntity shanChuDingDanEntity = gson.fromJson(response, ShanChuDingDanEntity.class);
                            Toast.makeText(DaiZhiFuXiangQingActivity.this, "删除成功！", Toast.LENGTH_SHORT).show();
                            finish();
                        }else{
                            ContractUtils.Code400(DaiZhiFuXiangQingActivity.this,response);
                        }
                    }
                });
    }

}
