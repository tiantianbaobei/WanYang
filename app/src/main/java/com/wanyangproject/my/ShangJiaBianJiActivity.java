package com.wanyangproject.my;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.Gravity;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.NumberPicker;
import android.widget.TextView;
import android.widget.Toast;

import com.bigkoo.pickerview.TimePickerView;
import com.bumptech.glide.Glide;
import com.google.gson.Gson;
import com.lzy.imagepicker.ImagePicker;
import com.lzy.imagepicker.bean.ImageItem;
import com.lzy.imagepicker.ui.ImageGridActivity;
import com.wanyangproject.R;
import com.wanyangproject.entity.PhotoEntity;
import com.wanyangproject.entity.ShangJiaShangJiaGuanLiEntity;
import com.wanyangproject.entity.UserEntity;
import com.wanyangproject.popuwindow.EndPopupWindow;
import com.wanyangproject.popuwindow.StartPopupWindow;
import com.wanyangproject.popuwindow.TakePhotoPopWin;
import com.wanyangproject.utils.ContractUtils;
import com.wanyangproject.utils.GlideImageLoaderForPicker;
import com.zhy.http.okhttp.OkHttpUtils;
import com.zhy.http.okhttp.callback.StringCallback;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import top.zibin.luban.Luban;
import top.zibin.luban.OnCompressListener;


public class ShangJiaBianJiActivity extends AppCompatActivity {

    @BindView(R.id.image_back)
    ImageView imageBack;
    @BindView(R.id.tv_yingye)
    TextView tvYingye;
    //    @BindView(R.id.tv_xuanze_shijian)
//    TextView tvXuanzeShijian;
    @BindView(R.id.tv_start_time)
    TextView tvStartTime;
    @BindView(R.id.tv_end_time)
    TextView tvEndTime;
    @BindView(R.id.tv_start_peisong_time)
    TextView tvStartPeisongTime;
    @BindView(R.id.tv_end_peisong_time)
    TextView tvEndPeisongTime;
    @BindView(R.id.image_shop)
    ImageView imageShop;
    @BindView(R.id.et_shangjia_weizhi)
    EditText etShangjiaWeizhi;
    @BindView(R.id.et_dianhua_zixun)
    EditText etDianhuaZixun;
    @BindView(R.id.image_daodian_ziqu)
    ImageView imageDaodianZiqu;
    @BindView(R.id.image_shangjia_peisong)
    ImageView imageShangjiaPeisong;
    @BindView(R.id.tv_save)
    TextView tvSave;
//    @BindView(R.id.webView)
//    WebView webView;
    @BindView(R.id.et_shangjia_jianjie)
    EditText etShangjiaJianjie;
    private StartPopupWindow startPopupWindow;
    private EndPopupWindow endPopupWindow;
    private TextView tv_quxiao, tv_queding;
    //    private NumberPicker numberPicker_year, numberPicker_month, numberPicker_day, numberPicker_hour, numberPicker_minute, province_picker, city_picker, qu_picker;
    private NumberPicker numberPicker_hour, numberPicker_minute, province_picker, city_picker, qu_picker;
    private int year, month, day, hour, minute;
    private UserEntity userEntity;
    private String phone;
    private ShangJiaShangJiaGuanLiEntity shangJiaShangJiaGuanLiEntity;
    private WebSettings mWebSettings;
    private String content;
    private TakePhotoPopWin takePhotoPopWin;
    private Boolean ONE = true;
    private Boolean TWO = true;
    private PhotoEntity photoEntity;
    private String imageurl;
    private Uri photoUri;
    private static Boolean isOK = false;




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shang_jia_bian_ji);
        ButterKnife.bind(this);
        DataShow();
//
//
//        mWebSettings = webView.getSettings();
//        mWebSettings.setJavaScriptEnabled(true);  //允许加载javascript
//        webView.getSettings().setLayoutAlgorithm(WebSettings.LayoutAlgorithm.SINGLE_COLUMN);
//        webView.getSettings().setLoadWithOverviewMode(true);
//        mWebSettings.setSupportZoom(false);     //允许缩放
//        mWebSettings.setBuiltInZoomControls(false); //原网页基础上缩放
//        mWebSettings.setUseWideViewPort(false);   //任意比例缩放
////        mWebSettings.setUseWideViewPort(true); //将图片调整到适合webview的大小
////        mWebSettings.setLoadWithOverviewMode(true); // 缩放至屏幕的大小
//        // 支持通过JS打开新窗口
//        mWebSettings.setJavaScriptCanOpenWindowsAutomatically(true);
//        mWebSettings.setLoadWithOverviewMode(true);
//        //不显示webview缩放按钮
//        mWebSettings.setDisplayZoomControls(false);
////        String cacheDirPath = getFilesDir().getAbsolutePath() + APP_CACAHE_DIRNAME;
////      String cacheDirPath = getCacheDir().getAbsolutePath()+Constant.APP_DB_DIRNAME;
//        //设置数据库缓存路径
////        webView.getSettings().setDatabasePath(cacheDirPath);
//        //设置  Application Caches 缓存目录
////        webView.getSettings().setAppCachePath(cacheDirPath);
//        //开启 Application Caches 功能
//        webView.getSettings().setAppCacheEnabled(true);
//        webView.setWebViewClient(new WebViewClient());

//
//        imageShangjiaPeisong.setImageResource(R.drawable.checked);
//        imageDaodianZiqu.setImageResource(R.drawable.checked);

        //        商家商家管理界面的网络请求
        initShangJiaGuanLiHttp();

    }


    //    商家商家管理界面的网络请求
    private void initShangJiaGuanLiHttp() {
        OkHttpUtils.post()
                .url(ContractUtils.LOGIN_URL + "Shop/shopfind")
                .addHeader("token", ContractUtils.getTOKEN(ShangJiaBianJiActivity.this))
                .addParams("parkId", ContractUtils.getParkId(ShangJiaBianJiActivity.this))
                .addParams("phone", ContractUtils.getPhone(ShangJiaBianJiActivity.this))
                .build()
                .execute(new StringCallback() {
                    @Override
                    public void onError(Call call, Exception e, int id) {

                    }

                    @Override
                    public void onResponse(String response, int id) {
                        ContractUtils.Code500(ShangJiaBianJiActivity.this, response);
                        System.out.println(response + "        商家商家管理界面的网络请求");

                        if (response.indexOf("200") != -1) {
                            Gson gson = new Gson();
                            shangJiaShangJiaGuanLiEntity = gson.fromJson(response, ShangJiaShangJiaGuanLiEntity.class);



                            if(shangJiaShangJiaGuanLiEntity.getResponse().getShop().getPhoto() != null){
                                if (shangJiaShangJiaGuanLiEntity.getResponse().getShop().getPhoto().equals("park.hostop.net/")) {
                                    Glide.with(ShangJiaBianJiActivity.this).load(R.drawable.shangdian).into(imageShop);
                                } else {  //头像
                                    Glide.with(ShangJiaBianJiActivity.this).load(ContractUtils.PHOTO_URL + shangJiaShangJiaGuanLiEntity.getResponse().getShop().getPhoto()).into(imageShop);
                                }
                            }




//                            if (ContractUtils.PHOTO_URL + shangJiaShangJiaGuanLiEntity.getResponse().getShop().getPhoto() == null) {
//                                Glide.with(ShangJiaBianJiActivity.this).load(R.drawable.shangdian).into(imageShop);
//                            } else {  //头像
//                                Glide.with(ShangJiaBianJiActivity.this).load(ContractUtils.PHOTO_URL + shangJiaShangJiaGuanLiEntity.getResponse().getShop().getPhoto()).into(imageShop);
//                            }

                            if (shangJiaShangJiaGuanLiEntity.getResponse().getShop().getStar_time() == null) {

                            } else {// 开始时间
                                tvStartTime.setText(shangJiaShangJiaGuanLiEntity.getResponse().getShop().getStar_time());
                            }


                            if (shangJiaShangJiaGuanLiEntity.getResponse().getShop().getEnd_time() == null) {

                            } else { // 结束时间
                                tvEndTime.setText(shangJiaShangJiaGuanLiEntity.getResponse().getShop().getEnd_time());
                            }


                            if (shangJiaShangJiaGuanLiEntity.getResponse().getShop().getBaidu_address() == null) {

                            } else { // 商家位置
                                etShangjiaWeizhi.setText(shangJiaShangJiaGuanLiEntity.getResponse().getShop().getBaidu_address());
                            }


                            if (shangJiaShangJiaGuanLiEntity.getResponse().getShop().getPhone() == null) {

                            } else { // 电话咨询
                                etDianhuaZixun.setText(shangJiaShangJiaGuanLiEntity.getResponse().getShop().getPhone());
                            }


                            if (shangJiaShangJiaGuanLiEntity.getResponse().getShop().getStar_time() == null) {

                            } else {// 开始时间
                                tvStartPeisongTime.setText(shangJiaShangJiaGuanLiEntity.getResponse().getShop().getStar_time());
                            }


                            if (shangJiaShangJiaGuanLiEntity.getResponse().getShop().getEnd_time() == null) {

                            } else { // 结束时间
                                tvEndPeisongTime.setText(shangJiaShangJiaGuanLiEntity.getResponse().getShop().getEnd_time());
                            }

//                            商家服务


                            if (shangJiaShangJiaGuanLiEntity.getResponse().getShop().getContent() == null) {

                            } else { //商家简介
                                etShangjiaJianjie.setText(shangJiaShangJiaGuanLiEntity.getResponse().getShop().getContent());
//                                //                            内容
//                                content = shangJiaShangJiaGuanLiEntity.getResponse().getShop().getContent();
//                                webView.loadDataWithBaseURL("http://www.baidu.com", getNewContent(content), "text/html", "UTF-8", null);

                            }


//                            到点自取
                            if(shangJiaShangJiaGuanLiEntity.getResponse().getShop().getService().equals("1")){
                                imageDaodianZiqu.setImageResource(R.drawable.checked);
                            }else if(shangJiaShangJiaGuanLiEntity.getResponse().getShop().getService().equals("2")){//商家配送
                                imageShangjiaPeisong.setImageResource(R.drawable.checked);

                            }else if(shangJiaShangJiaGuanLiEntity.getResponse().getShop().getService().equals("3")){//都支持
                                imageDaodianZiqu.setImageResource(R.drawable.checked);
                                imageShangjiaPeisong.setImageResource(R.drawable.checked);
                            }


                        }
                    }
                });
    }


    /**
     * 将html文本内容中包含img标签的图片，宽度变为屏幕宽度，高度根据宽度比例自适应
     **/
    public static String getNewContent(String htmltext) {
//        if(isOK == false){
//            return htmltext;
//        }
//
//        isOK = true;
        try {
            Document doc = Jsoup.parse(htmltext);
            Elements elements = doc.getElementsByTag("img");
            for (Element element : elements) {
                element.attr("width", "100%").attr("height", "auto");
            }

            return doc.toString();
        } catch (Exception e) {
            return htmltext;
        }
    }


    private void DataShow() {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy年MM月dd日 HH:mm:ss");// HH:mm:ss
//获取当前时间
        Date date = new Date(System.currentTimeMillis());
        System.out.println("Date获取当前日期时间" + simpleDateFormat.format(date));
//                                    2018年03月14日 11:39:29
        String str = simpleDateFormat.format(date);

        year = Integer.parseInt(str.substring(0, 4));
        month = Integer.parseInt(str.substring(5, 7));
        day = Integer.parseInt(str.substring(8, 10));
        hour = Integer.parseInt(str.substring(12, 14));
        minute = Integer.parseInt(str.substring(15, 17));
    }

    @OnClick({R.id.image_shop, R.id.image_back, R.id.tv_yingye, R.id.image_daodian_ziqu, R.id.et_shangjia_weizhi, R.id.et_shangjia_jianjie, R.id.et_dianhua_zixun, R.id.tv_save, R.id.image_shangjia_peisong, R.id.tv_start_time, R.id.tv_end_time, R.id.tv_start_peisong_time, R.id.tv_end_peisong_time})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.image_back:
                finish();
                break;
//            商家头像
            case R.id.image_shop:
                Object itemsOnClick = null;
                takePhotoPopWin = new TakePhotoPopWin(ShangJiaBianJiActivity.this, (View.OnClickListener) itemsOnClick);
                takePhotoPopWin.showAtLocation(findViewById(R.id.relative_bianji), Gravity.BOTTOM | Gravity.CENTER_HORIZONTAL, 0, 0);


                takePhotoPopWin.setOnClickXiangji(new TakePhotoPopWin.OnClickXiangji() {
                    @Override
                    public void onClick(int id) {
//                        Intent open = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
//                        startActivityForResult(open,100);

                        //                判断当前SDK版本号
                        if (Build.VERSION.SDK_INT >= 23) {
//                    当前拨打电话权限是没有给的
                            if (ContextCompat.checkSelfPermission(ShangJiaBianJiActivity.this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
//                        我要请求权限
                                ActivityCompat.requestPermissions(ShangJiaBianJiActivity.this, new String[]{Manifest.permission.CAMERA}, 1);
                            } else {
//                                SimpleDateFormat timeStampFormat = new SimpleDateFormat("yyyyMMddHHmmss");
//                                String filename = timeStampFormat.format(new Date());
//                                ContentValues values = new ContentValues(); //使用本地相册保存拍摄照片
//                                values.put(MediaStore.Images.Media.TITLE, filename);
//                                photoUri = getContentResolver().insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);
//
//                                // 设置 intent.putExtra(MediaStore.EXTRA_OUTPUT, photoUri); photoUri = 保存图片得uri
//                                Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
//                                intent.putExtra(MediaStore.EXTRA_OUTPUT, photoUri);
//                                startActivityForResult(intent, 100);

                                if (Environment.MEDIA_MOUNTED.equals(Environment.getExternalStorageState())) {
                                    //创建文件夹 在本地文件Pictures 目录下创建 自己得文件夹
                                    File dir = new File(Environment.getExternalStorageDirectory() + "/Pictures/image");
                                    if (!dir.exists()) {
                                        dir.mkdirs();
                                    }
                                    //照片名称
                                    File file = new File(dir, new SimpleDateFormat("yyyyMMddHHmmss").format(new Date()) + ".jpg");
                                    photoUri = Uri.parse(file.getPath());

                                    startActivityForResult(new Intent(MediaStore.ACTION_IMAGE_CAPTURE)
                                            .putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(file)), 100);

                                } else {
//                                    showToast("请检查SDCard！");
                                }
                            }
                        } else {
//                            SimpleDateFormat timeStampFormat = new SimpleDateFormat("yyyyMMddHHmmss");
//                            String filename = timeStampFormat.format(new Date());
//                            ContentValues values = new ContentValues(); //使用本地相册保存拍摄照片
//                            values.put(MediaStore.Images.Media.TITLE, filename);
//                            photoUri = getContentResolver().insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);
//
//                            // 设置 intent.putExtra(MediaStore.EXTRA_OUTPUT, photoUri); photoUri = 保存图片得uri
//                            Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
//                            intent.putExtra(MediaStore.EXTRA_OUTPUT, photoUri);
//                            startActivityForResult(intent, 100);


                            if (Environment.MEDIA_MOUNTED.equals(Environment.getExternalStorageState())) {
                                //创建文件夹 在本地文件Pictures 目录下创建 自己得文件夹
                                File dir = new File(Environment.getExternalStorageDirectory() + "/Pictures/image");
                                if (!dir.exists()) {
                                    dir.mkdirs();
                                }
                                //照片名称
                                File file = new File(dir, new SimpleDateFormat("yyyyMMddHHmmss").format(new Date()) + ".jpg");
                                photoUri = Uri.parse(file.getPath());

                                startActivityForResult(new Intent(MediaStore.ACTION_IMAGE_CAPTURE)
                                        .putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(file)), 100);

                            } else {
//                                    showToast("请检查SDCard！");
                            }
                        }

//                        SimpleDateFormat timeStampFormat = new SimpleDateFormat("yyyyMMddHHmmss");
//                        String filename = timeStampFormat.format(new Date());
//                        ContentValues values = new ContentValues(); //使用本地相册保存拍摄照片
//                        values.put(MediaStore.Images.Media.TITLE, filename);
//                        photoUri = getContentResolver().insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);
//
//                        // 设置 intent.putExtra(MediaStore.EXTRA_OUTPUT, photoUri); photoUri = 保存图片得uri
//                        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
//                        intent.putExtra(MediaStore.EXTRA_OUTPUT, photoUri);
//                        startActivityForResult(intent, 100);


//                        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
//                        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {//判断是否有相机应用
//                            startActivityForResult(takePictureIntent, REQ_THUMB);
//
//                        }
                    }
                });


                //                点击相册
                takePhotoPopWin.setOnClickXiangce(new TakePhotoPopWin.OnClickXiangce() {
                    @Override
                    public void onClick(int id) {
//                        Intent local = new Intent();
//                        local.setType("image/*");
//                        local.setAction(Intent.ACTION_GET_CONTENT);
//                        startActivityForResult(local, 2);

                        ImagePicker imagePicker = ImagePicker.getInstance();
                        imagePicker.setImageLoader(new GlideImageLoaderForPicker());   //设置图片加载器
                        imagePicker.setShowCamera(true);  //显示拍照按钮
                        imagePicker.setCrop(false);        //允许裁剪（单选才有效）
                        imagePicker.setSaveRectangle(true);//是否按矩形区域保存
                        imagePicker.setFocusHeight(169);
                        imagePicker.setFocusWidth(169);
                        imagePicker.setCrop(false);
                        imagePicker.setSelectLimit(1);    //选中数量限制
                        imagePicker.setOutPutX(800);//保存文件的宽度。单位像素
                        imagePicker.setOutPutY(800);//保存文件的高度。单位像素

                        Intent intent = new Intent(ShangJiaBianJiActivity.this, ImageGridActivity.class);
                        startActivityForResult(intent, 1);

                    }
                });
                break;
            case R.id.tv_yingye:
                break;
            case R.id.et_shangjia_weizhi:
                break;
            case R.id.et_dianhua_zixun:
                break;
//            简介
//            case R.id.et_jianjie:
//                break;
//            保存
            case R.id.tv_save:
//                商家修改信息的网络请求
                initShangJiaXiuGaiHttp();
                break;
//            到店自取
            case R.id.image_daodian_ziqu:
                if (ONE) {
                    imageDaodianZiqu.setImageResource(R.drawable.unchecked);
                    ONE = false;
                } else {
                    imageDaodianZiqu.setImageResource(R.drawable.checked);
                    ONE = true;
                }


                break;
//            商家配送
            case R.id.image_shangjia_peisong:
                if (TWO) {
                    imageShangjiaPeisong.setImageResource(R.drawable.unchecked);
                    TWO = false;
                } else {
                    imageShangjiaPeisong.setImageResource(R.drawable.checked);
                    TWO = true;
                }
                break;
//            营业开始时间
            case R.id.tv_start_time:
                Calendar calendar = Calendar.getInstance();

                TimePickerView timePickerView = new TimePickerView.Builder(ShangJiaBianJiActivity.this, new TimePickerView.OnTimeSelectListener() {
                    @Override
                    public void onTimeSelect(Date date, View v) {
                        SimpleDateFormat simpleDateFormat1 = new SimpleDateFormat("HH:mm");
                        tvStartTime.setText(simpleDateFormat1.format(date));
                        initJieShu();

//                        Date date1 = null;
//                        try {
//                            date1 = simpleDateFormat1.parse(tvStartTime.getText().toString());
//                            long time = date1.getTime();
//                            string = String.valueOf(time/1000);
//                            System.out.println(string + "     时间戳");
//                        } catch (ParseException e) {
//                            e.printStackTrace();
//                        }
                    }
                })
                        .setType(new boolean[]{false, false, false, true, true, false})//默认全部显示
                        .setCancelText("取消")//取消按钮文字
                        .setSubmitText("确定")//确认按钮文字
                        .setTextXOffset(0, 0, 0, 0, 0, 0)
                        .setLunarCalendar(false)
//                        .setContentSize(20)//滚轮文字大小
                        .gravity(Gravity.CENTER)
                        .setTitleSize(16)//标题文字大小
                        .setTitleText("选择开始时间")//标题文字
                        .setOutSideCancelable(true)//点击屏幕，点在控件外部范围时，是否取消显示
                        .isCyclic(true)//是否循环滚动
                        .setTextColorCenter(Color.BLACK)//设置选中项的颜色
                        .setTextColorOut(Color.parseColor("#AEAAAA"))
                        .setTitleColor(Color.BLACK)//标题文字颜色
                        .setSubmitColor(Color.RED)//确定按钮文字颜色
                        .setCancelColor(Color.parseColor("#AEAAAA"))//取消按钮文字颜色
                        .setTitleBgColor(Color.parseColor("#f5f5f5"))//标题背景颜色 Night mode
                        .setBgColor(Color.parseColor("#ffffff"))//滚轮背景颜色 Night mode
//                        .setRange(1900, calendar.get(Calendar.YEAR))//默认是1900-2100年
//                        .setDate(selectedDate)// 如果不设置的话，默认是系统时间*/
//                        .setRangDate(startDate,endDate)//起始终止年月日设定
                        .setLabel("", "", "", ":", "", "")
                        .isCenterLabel(true) //是否只显示中间选中项的label文字，false则每项item全部都带有label。
                        .isDialog(false)//是否显示为对话框样式
                        .build();
                timePickerView.setDate(Calendar.getInstance());//注：根据需求来决定是否使用该方法（一般是精确到秒的情况），此项可以在弹出选择器的时候重新设置当前时间，避免在初始化之后由于时间已经设定，导致选中时间与当前时间不匹配的问题。
                timePickerView.show();


                break;
//            营业结束时间
            case R.id.tv_end_time:
                Calendar calendar1 = Calendar.getInstance();

                TimePickerView timePickerView1 = new TimePickerView.Builder(ShangJiaBianJiActivity.this, new TimePickerView.OnTimeSelectListener() {
                    @Override
                    public void onTimeSelect(Date date, View v) {
                        SimpleDateFormat simpleDateFormat1 = new SimpleDateFormat("HH:mm");
                        tvStartTime.setText(simpleDateFormat1.format(date));
                        initJieShu();

//                        Date date1 = null;
//                        try {
//                            date1 = simpleDateFormat1.parse(tvStartTime.getText().toString());
//                            long time = date1.getTime();
//                            string = String.valueOf(time/1000);
//                            System.out.println(string + "     时间戳");
//                        } catch (ParseException e) {
//                            e.printStackTrace();
//                        }
                    }
                })
                        .setType(new boolean[]{false, false, false, true, true, false})//默认全部显示
                        .setCancelText("取消")//取消按钮文字
                        .setSubmitText("确定")//确认按钮文字
                        .setTextXOffset(0, 0, 0, 0, 0, 0)
                        .setLunarCalendar(false)
//                        .setContentSize(20)//滚轮文字大小
                        .gravity(Gravity.CENTER)
                        .setTitleSize(16)//标题文字大小
                        .setTitleText("选择开始时间")//标题文字
                        .setOutSideCancelable(true)//点击屏幕，点在控件外部范围时，是否取消显示
                        .isCyclic(true)//是否循环滚动
                        .setTextColorCenter(Color.BLACK)//设置选中项的颜色
                        .setTextColorOut(Color.parseColor("#AEAAAA"))
                        .setTitleColor(Color.BLACK)//标题文字颜色
                        .setSubmitColor(Color.RED)//确定按钮文字颜色
                        .setCancelColor(Color.parseColor("#AEAAAA"))//取消按钮文字颜色
                        .setTitleBgColor(Color.parseColor("#f5f5f5"))//标题背景颜色 Night mode
                        .setBgColor(Color.parseColor("#ffffff"))//滚轮背景颜色 Night mode
//                        .setRange(1900, calendar.get(Calendar.YEAR))//默认是1900-2100年
//                        .setDate(selectedDate)// 如果不设置的话，默认是系统时间*/
//                        .setRangDate(startDate,endDate)//起始终止年月日设定
                        .setLabel("", "", "", ":", "", "")
                        .isCenterLabel(true) //是否只显示中间选中项的label文字，false则每项item全部都带有label。
                        .isDialog(false)//是否显示为对话框样式
                        .build();
                timePickerView1.setDate(Calendar.getInstance());//注：根据需求来决定是否使用该方法（一般是精确到秒的情况），此项可以在弹出选择器的时候重新设置当前时间，避免在初始化之后由于时间已经设定，导致选中时间与当前时间不匹配的问题。
                timePickerView1.show();


//                Calendar calendar1 = Calendar.getInstance();

//                TimePickerView timePickerView1 = new TimePickerView.Builder(ShangJiaBianJiActivity.this, new TimePickerView.OnTimeSelectListener() {
//                    @Override
//                    public void onTimeSelect(Date date, View v) {
//                        SimpleDateFormat simpleDateFormat1 = new SimpleDateFormat("yyyy-MM-dd");
//                        tvEndTime.setText(simpleDateFormat1.format(date));
//
////                        Date date1 = null;
////                        try {
////                            date1 = simpleDateFormat1.parse(tvStartTime.getText().toString());
////                            long time = date1.getTime();
////                            string = String.valueOf(time/1000);
////                            System.out.println(string + "     时间戳");
////                        } catch (ParseException e) {
////                            e.printStackTrace();
////                        }
//                    }
//                })
//                        .setType(new boolean[]{true, true, true, false, false, false})//默认全部显示
//                        .setCancelText("取消")//取消按钮文字
//                        .setSubmitText("确定")//确认按钮文字
//                        .setTextXOffset(0,0,0,0,0,0)
//                        .setLunarCalendar(false)
////                        .setContentSize(20)//滚轮文字大小
//                        .gravity(Gravity.CENTER)
//                        .setTitleSize(16)//标题文字大小
//                        .setTitleText("选择结束时间")//标题文字
//                        .setOutSideCancelable(true)//点击屏幕，点在控件外部范围时，是否取消显示
//                        .isCyclic(true)//是否循环滚动
//                        .setTextColorCenter(Color.BLACK)//设置选中项的颜色
//                        .setTextColorOut(Color.parseColor("#AEAAAA"))
//                        .setTitleColor(Color.BLACK)//标题文字颜色
//                        .setSubmitColor(Color.RED)//确定按钮文字颜色
//                        .setCancelColor(Color.parseColor("#AEAAAA"))//取消按钮文字颜色
//                        .setTitleBgColor(Color.parseColor("#f5f5f5"))//标题背景颜色 Night mode
//                        .setBgColor(Color.parseColor("#ffffff"))//滚轮背景颜色 Night mode
////                        .setRange(1900, calendar1.get(Calendar.YEAR))//默认是1900-2100年
////                        .setDate(selectedDate)// 如果不设置的话，默认是系统时间*/
////                        .setRangDate(startDate,endDate)//起始终止年月日设定
//                        .setLabel("年","月","日",":","","")
//                        .isCenterLabel(true) //是否只显示中间选中项的label文字，false则每项item全部都带有label。
//                        .isDialog(false)//是否显示为对话框样式
//                        .build();
//                timePickerView1.setDate(Calendar.getInstance());//注：根据需求来决定是否使用该方法（一般是精确到秒的情况），此项可以在弹出选择器的时候重新设置当前时间，避免在初始化之后由于时间已经设定，导致选中时间与当前时间不匹配的问题。
//                timePickerView1.show();
//
//


                break;
//            配送开始时间
            case R.id.tv_start_peisong_time:
//                Calendar calendar1 = Calendar.getInstance();

                TimePickerView timePickerView2 = new TimePickerView.Builder(ShangJiaBianJiActivity.this, new TimePickerView.OnTimeSelectListener() {
                    @Override
                    public void onTimeSelect(Date date, View v) {
                        SimpleDateFormat simpleDateFormat1 = new SimpleDateFormat("HH:mm");
                        tvStartPeisongTime.setText(simpleDateFormat1.format(date));
                        JieShuShuMu();

//                        Date date1 = null;
//                        try {
//                            date1 = simpleDateFormat1.parse(tvStartTime.getText().toString());
//                            long time = date1.getTime();
//                            string = String.valueOf(time/1000);
//                            System.out.println(string + "     时间戳");
//                        } catch (ParseException e) {
//                            e.printStackTrace();
//                        }
                    }
                })
                        .setType(new boolean[]{false, false, false, true, true, false})//默认全部显示
                        .setCancelText("取消")//取消按钮文字
                        .setSubmitText("确定")//确认按钮文字
                        .setTextXOffset(0, 0, 0, 0, 0, 0)
                        .setLunarCalendar(false)
//                        .setContentSize(20)//滚轮文字大小
                        .gravity(Gravity.CENTER)
                        .setTitleSize(16)//标题文字大小
                        .setTitleText("选择开始时间")//标题文字
                        .setOutSideCancelable(true)//点击屏幕，点在控件外部范围时，是否取消显示
                        .isCyclic(true)//是否循环滚动
                        .setTextColorCenter(Color.BLACK)//设置选中项的颜色
                        .setTextColorOut(Color.parseColor("#AEAAAA"))
                        .setTitleColor(Color.BLACK)//标题文字颜色
                        .setSubmitColor(Color.RED)//确定按钮文字颜色
                        .setCancelColor(Color.parseColor("#AEAAAA"))//取消按钮文字颜色
                        .setTitleBgColor(Color.parseColor("#f5f5f5"))//标题背景颜色 Night mode
                        .setBgColor(Color.parseColor("#ffffff"))//滚轮背景颜色 Night mode
//                        .setRange(1900, calendar1.get(Calendar.YEAR))//默认是1900-2100年
//                        .setDate(selectedDate)// 如果不设置的话，默认是系统时间*/
//                        .setRangDate(startDate,endDate)//起始终止年月日设定
                        .setLabel("年", "月", "日", ":", "", "")
                        .isCenterLabel(true) //是否只显示中间选中项的label文字，false则每项item全部都带有label。
                        .isDialog(false)//是否显示为对话框样式
                        .build();
                timePickerView2.setDate(Calendar.getInstance());//注：根据需求来决定是否使用该方法（一般是精确到秒的情况），此项可以在弹出选择器的时候重新设置当前时间，避免在初始化之后由于时间已经设定，导致选中时间与当前时间不匹配的问题。
                timePickerView2.show();


                break;
//            配送结束时间
            case R.id.tv_end_peisong_time:

                TimePickerView timePickerView22 = new TimePickerView.Builder(ShangJiaBianJiActivity.this, new TimePickerView.OnTimeSelectListener() {
                    @Override
                    public void onTimeSelect(Date date, View v) {
                        SimpleDateFormat simpleDateFormat1 = new SimpleDateFormat("HH:mm");
                        tvStartPeisongTime.setText(simpleDateFormat1.format(date));
                        JieShuShuMu();

//                        Date date1 = null;
//                        try {
//                            date1 = simpleDateFormat1.parse(tvStartTime.getText().toString());
//                            long time = date1.getTime();
//                            string = String.valueOf(time/1000);
//                            System.out.println(string + "     时间戳");
//                        } catch (ParseException e) {
//                            e.printStackTrace();
//                        }
                    }
                })
                        .setType(new boolean[]{false, false, false, true, true, false})//默认全部显示
                        .setCancelText("取消")//取消按钮文字
                        .setSubmitText("确定")//确认按钮文字
                        .setTextXOffset(0, 0, 0, 0, 0, 0)
                        .setLunarCalendar(false)
//                        .setContentSize(20)//滚轮文字大小
                        .gravity(Gravity.CENTER)
                        .setTitleSize(16)//标题文字大小
                        .setTitleText("选择开始时间")//标题文字
                        .setOutSideCancelable(true)//点击屏幕，点在控件外部范围时，是否取消显示
                        .isCyclic(true)//是否循环滚动
                        .setTextColorCenter(Color.BLACK)//设置选中项的颜色
                        .setTextColorOut(Color.parseColor("#AEAAAA"))
                        .setTitleColor(Color.BLACK)//标题文字颜色
                        .setSubmitColor(Color.RED)//确定按钮文字颜色
                        .setCancelColor(Color.parseColor("#AEAAAA"))//取消按钮文字颜色
                        .setTitleBgColor(Color.parseColor("#f5f5f5"))//标题背景颜色 Night mode
                        .setBgColor(Color.parseColor("#ffffff"))//滚轮背景颜色 Night mode
//                        .setRange(1900, calendar1.get(Calendar.YEAR))//默认是1900-2100年
//                        .setDate(selectedDate)// 如果不设置的话，默认是系统时间*/
//                        .setRangDate(startDate,endDate)//起始终止年月日设定
                        .setLabel("年", "月", "日", ":", "", "")
                        .isCenterLabel(true) //是否只显示中间选中项的label文字，false则每项item全部都带有label。
                        .isDialog(false)//是否显示为对话框样式
                        .build();
                timePickerView22.setDate(Calendar.getInstance());//注：根据需求来决定是否使用该方法（一般是精确到秒的情况），此项可以在弹出选择器的时候重新设置当前时间，避免在初始化之后由于时间已经设定，导致选中时间与当前时间不匹配的问题。
                timePickerView22.show();


//                TimePickerView timePickerView4 = new TimePickerView.Builder(ShangJiaBianJiActivity.this, new TimePickerView.OnTimeSelectListener() {
//                    @Override
//                    public void onTimeSelect(Date date, View v) {
//                        SimpleDateFormat simpleDateFormat1 = new SimpleDateFormat("yyyy-MM-dd");
//                        tvEndPeisongTime.setText(simpleDateFormat1.format(date));
//
////                        Date date1 = null;
////                        try {
////                            date1 = simpleDateFormat1.parse(tvStartTime.getText().toString());
////                            long time = date1.getTime();
////                            string = String.valueOf(time/1000);
////                            System.out.println(string + "     时间戳");
////                        } catch (ParseException e) {
////                            e.printStackTrace();
////                        }
//                    }
//                })
//                        .setType(new boolean[]{true, true, true, false, false, false})//默认全部显示
//                        .setCancelText("取消")//取消按钮文字
//                        .setSubmitText("确定")//确认按钮文字
//                        .setTextXOffset(0,0,0,0,0,0)
//                        .setLunarCalendar(false)
////                        .setContentSize(20)//滚轮文字大小
//                        .gravity(Gravity.CENTER)
//                        .setTitleSize(16)//标题文字大小
//                        .setTitleText("选择开始时间")//标题文字
//                        .setOutSideCancelable(true)//点击屏幕，点在控件外部范围时，是否取消显示
//                        .isCyclic(true)//是否循环滚动
//                        .setTextColorCenter(Color.BLACK)//设置选中项的颜色
//                        .setTextColorOut(Color.parseColor("#AEAAAA"))
//                        .setTitleColor(Color.BLACK)//标题文字颜色
//                        .setSubmitColor(Color.RED)//确定按钮文字颜色
//                        .setCancelColor(Color.parseColor("#AEAAAA"))//取消按钮文字颜色
//                        .setTitleBgColor(Color.parseColor("#f5f5f5"))//标题背景颜色 Night mode
//                        .setBgColor(Color.parseColor("#ffffff"))//滚轮背景颜色 Night mode
////                        .setRange(1900, calendar1.get(Calendar.YEAR))//默认是1900-2100年
////                        .setDate(selectedDate)// 如果不设置的话，默认是系统时间*/
////                        .setRangDate(startDate,endDate)//起始终止年月日设定
//                        .setLabel("年","月","日",":","","")
//                        .isCenterLabel(true) //是否只显示中间选中项的label文字，false则每项item全部都带有label。
//                        .isDialog(false)//是否显示为对话框样式
//                        .build();
//                timePickerView4.setDate(Calendar.getInstance());//注：根据需求来决定是否使用该方法（一般是精确到秒的情况），此项可以在弹出选择器的时候重新设置当前时间，避免在初始化之后由于时间已经设定，导致选中时间与当前时间不匹配的问题。
//                timePickerView4.show();
//


                break;
        }
    }


    //    营业结束时间
    private void initJieShu() {
        TimePickerView timePickerView1 = new TimePickerView.Builder(ShangJiaBianJiActivity.this, new TimePickerView.OnTimeSelectListener() {
            @Override
            public void onTimeSelect(Date date, View v) {
                SimpleDateFormat simpleDateFormat1 = new SimpleDateFormat("HH:mm");
                tvEndTime.setText(simpleDateFormat1.format(date));

//                        Date date1 = null;
//                        try {
//                            date1 = simpleDateFormat1.parse(tvStartTime.getText().toString());
//                            long time = date1.getTime();
//                            string = String.valueOf(time/1000);
//                            System.out.println(string + "     时间戳");
//                        } catch (ParseException e) {
//                            e.printStackTrace();
//                        }
            }
        })
                .setType(new boolean[]{false, false, false, true, true, false})//默认全部显示
                .setCancelText("取消")//取消按钮文字
                .setSubmitText("确定")//确认按钮文字
                .setTextXOffset(0, 0, 0, 0, 0, 0)
                .setLunarCalendar(false)
//                        .setContentSize(20)//滚轮文字大小
                .gravity(Gravity.CENTER)
                .setTitleSize(16)//标题文字大小
                .setTitleText("选择结束时间")//标题文字
                .setOutSideCancelable(true)//点击屏幕，点在控件外部范围时，是否取消显示
                .isCyclic(true)//是否循环滚动
                .setTextColorCenter(Color.BLACK)//设置选中项的颜色
                .setTextColorOut(Color.parseColor("#AEAAAA"))
                .setTitleColor(Color.BLACK)//标题文字颜色
                .setSubmitColor(Color.RED)//确定按钮文字颜色
                .setCancelColor(Color.parseColor("#AEAAAA"))//取消按钮文字颜色
                .setTitleBgColor(Color.parseColor("#f5f5f5"))//标题背景颜色 Night mode
                .setBgColor(Color.parseColor("#ffffff"))//滚轮背景颜色 Night mode
//                        .setRange(1900, calendar1.get(Calendar.YEAR))//默认是1900-2100年
//                        .setDate(selectedDate)// 如果不设置的话，默认是系统时间*/
//                        .setRangDate(startDate,endDate)//起始终止年月日设定
                .setLabel("年", "月", "日", ":", "", "")
                .isCenterLabel(true) //是否只显示中间选中项的label文字，false则每项item全部都带有label。
                .isDialog(false)//是否显示为对话框样式
                .build();
        timePickerView1.setDate(Calendar.getInstance());//注：根据需求来决定是否使用该方法（一般是精确到秒的情况），此项可以在弹出选择器的时候重新设置当前时间，避免在初始化之后由于时间已经设定，导致选中时间与当前时间不匹配的问题。
        timePickerView1.show();


    }


    //    配送结束时间
    private void JieShuShuMu() {
        TimePickerView timePickerView4 = new TimePickerView.Builder(ShangJiaBianJiActivity.this, new TimePickerView.OnTimeSelectListener() {
            @Override
            public void onTimeSelect(Date date, View v) {
                SimpleDateFormat simpleDateFormat1 = new SimpleDateFormat("HH:mm");
                tvEndPeisongTime.setText(simpleDateFormat1.format(date));

//                        Date date1 = null;
//                        try {
//                            date1 = simpleDateFormat1.parse(tvStartTime.getText().toString());
//                            long time = date1.getTime();
//                            string = String.valueOf(time/1000);
//                            System.out.println(string + "     时间戳");
//                        } catch (ParseException e) {
//                            e.printStackTrace();
//                        }
            }
        })
                .setType(new boolean[]{false, false, false, true, true, false})//默认全部显示
                .setCancelText("取消")//取消按钮文字
                .setSubmitText("确定")//确认按钮文字
                .setTextXOffset(0, 0, 0, 0, 0, 0)
                .setLunarCalendar(false)
//                        .setContentSize(20)//滚轮文字大小
                .gravity(Gravity.CENTER)
                .setTitleSize(16)//标题文字大小
                .setTitleText("选择开始时间")//标题文字
                .setOutSideCancelable(true)//点击屏幕，点在控件外部范围时，是否取消显示
                .isCyclic(true)//是否循环滚动
                .setTextColorCenter(Color.BLACK)//设置选中项的颜色
                .setTextColorOut(Color.parseColor("#AEAAAA"))
                .setTitleColor(Color.BLACK)//标题文字颜色
                .setSubmitColor(Color.RED)//确定按钮文字颜色
                .setCancelColor(Color.parseColor("#AEAAAA"))//取消按钮文字颜色
                .setTitleBgColor(Color.parseColor("#f5f5f5"))//标题背景颜色 Night mode
                .setBgColor(Color.parseColor("#ffffff"))//滚轮背景颜色 Night mode
//                        .setRange(1900, calendar1.get(Calendar.YEAR))//默认是1900-2100年
//                        .setDate(selectedDate)// 如果不设置的话，默认是系统时间*/
//                        .setRangDate(startDate,endDate)//起始终止年月日设定
                .setLabel("年", "月", "日", ":", "", "")
                .isCenterLabel(true) //是否只显示中间选中项的label文字，false则每项item全部都带有label。
                .isDialog(false)//是否显示为对话框样式
                .build();
        timePickerView4.setDate(Calendar.getInstance());//注：根据需求来决定是否使用该方法（一般是精确到秒的情况），此项可以在弹出选择器的时候重新设置当前时间，避免在初始化之后由于时间已经设定，导致选中时间与当前时间不匹配的问题。
        timePickerView4.show();


    }


    //    商家修改信息的网络请求
    private void initShangJiaXiuGaiHttp() {
        String service = "";
        if (ONE) {
            service = "1";
        }
        if (TWO) {
            service = "2";
        }
        if (ONE && TWO) {
            service = "3";
        }


        Map<String, String> map = new HashMap<String, String>();
        map.put("star_time", tvStartTime.getText().toString().trim());
        map.put("end_time", tvEndTime.getText().toString().trim());
        map.put("photo", imageurl);
        map.put("baidu_address", etShangjiaWeizhi.getText().toString().trim());
        map.put("longitude", "36.45515");
        map.put("latitude ", "73.556555");
        map.put("phone", etDianhuaZixun.getText().toString().trim());
        map.put("content", etShangjiaJianjie.getText().toString().trim());
        map.put("service", service);
        map.put("distribution", tvStartPeisongTime.getText().toString().trim() + "-" + tvEndPeisongTime.getText().toString().trim());

        Gson gson = new Gson();
        String s = gson.toJson(map);
        System.out.println(s + "      sssssssssssssssssss");


//        tiantian
        OkHttpUtils.post()
                .url(ContractUtils.LOGIN_URL + "Shop/uploadshop")
                .addHeader("token", ContractUtils.getTOKEN(ShangJiaBianJiActivity.this))
                .addParams("parkId", ContractUtils.getParkId(ShangJiaBianJiActivity.this))
                .addParams("phone", ContractUtils.getPhone(ShangJiaBianJiActivity.this))
                .addParams("data", s)
                .build()
                .execute(new StringCallback() {
                    @Override
                    public void onError(Call call, Exception e, int id) {
                        System.out.println(e + "       eeeeee");
                    }

                    @Override
                    public void onResponse(String response, int id) {
                        ContractUtils.Code500(ShangJiaBianJiActivity.this, response);
                        System.out.println(response + "        商家编辑的网络请求");
                        if (response.indexOf("200") != -1) {
                            finish();
                        }
                    }
                });
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);


        if (requestCode == 100) {
            String string = photoUri.toString();
            File file = new File(string);
            System.out.println(file.exists()+"      file");
            if(file.exists() == false){
                return;
            }

            imageShop.setImageURI(Uri.parse(string));
            uploadFile1(string);
        }


        if (data != null && requestCode == 1) {
            ArrayList<ImageItem> images = (ArrayList<ImageItem>) data.getSerializableExtra(ImagePicker.EXTRA_RESULT_ITEMS);
            if (images != null) {
                imageShop.setImageURI(Uri.parse(images.get(0).path));
                uploadFile(images.get(0));


            } else {
                Toast.makeText(this, "图片解析失败", Toast.LENGTH_SHORT).show();
            }
        }
//        else {
//            Toast.makeText(this, "没有数据", Toast.LENGTH_SHORT).show();
//        }
    }


    private void uploadFile(ImageItem imageItem) {
        final ProgressDialog progressDialog = new ProgressDialog(ShangJiaBianJiActivity.this);
        progressDialog.setTitle("提示");
        progressDialog.setMessage("请等待...");
        progressDialog.setCancelable(false);
        progressDialog.show();



        File file = new File(imageItem.path);
        Luban.with(this)
                .load(file)
                .ignoreBy(100)
                //  .setTargetDir(AppConfig.DEFAULT_SAVE_IMAGE_PATH)
                .setCompressListener(new OnCompressListener() {
                    @Override
                    public void onStart() {
                        // TODO 压缩开始前调用，可以在方法内启动 loading UI
                    }

                    @Override
                    public void onSuccess(File file) {
//                        String url = ContractUtils.LOGIN_URL+"ceshi/ce_upload.html";
                        OkHttpClient okHttpClient = new OkHttpClient();
//                        RequestBody requestBody1 =new FormBody.Builder()
//                                .add("driverid",substring)
//                                .add("driver_img_type","1")
//                                .build();
                        RequestBody requestBody = new MultipartBody.Builder()
                                .setType(MultipartBody.FORM)
                                .addFormDataPart("file", file.getName(), RequestBody.create(MediaType.parse("image/*"), file))
                                .build();

                        Request request = new Request.Builder()
                                .url(ContractUtils.LOGIN_URL+"index/upload")
                                .post(requestBody)
                                .addHeader("token", ContractUtils.getTOKEN(ShangJiaBianJiActivity.this))
                                .build();

                        okHttpClient.newCall(request).enqueue(new Callback() {
                            @Override
                            public void onFailure(Call call, IOException e) {
                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
//                                        Toast.makeText(ShangJiaBianJiActivity.this, "网络异常", Toast.LENGTH_SHORT).show();
                                    }
                                });
                                progressDialog.dismiss();
                            }

                            @Override
                            public void onResponse(Call call, final Response response) throws IOException {
                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        Toast.makeText(ShangJiaBianJiActivity.this, "上传成功", Toast.LENGTH_SHORT).show();
                                    }
                                });


                                progressDialog.dismiss();
                                String image = response.body().string();
                                Gson gson = new Gson();
                                photoEntity = gson.fromJson(image, PhotoEntity.class);
                                String fileurl = photoEntity.getResponse().getFileurl();
//                                http://park.hostop.net//upload/20180813/f2fbc9162ba7e567c987bfb0ccfbee45.jpg
                                imageurl = fileurl.substring(ContractUtils.YUANQUZHAOSHANG_PHOTO_URL.length());
                                System.out.println(fileurl + "        ffffffffffffffffff");
                                System.out.println(image + "     我的资料上传头像");
                            }
                        });
                    }

                    @Override
                    public void onError(Throwable e) {

                    }
                }).launch();
    }


    private void uploadFile1(String imageItem) {
        final ProgressDialog progressDialog = new ProgressDialog(ShangJiaBianJiActivity.this);
        progressDialog.setTitle("提示");
        progressDialog.setMessage("请等待...");
        progressDialog.setCancelable(false);
        progressDialog.show();

        File file = new File(imageItem);
        Luban.with(this)
                .load(file)
                .ignoreBy(100)
                //  .setTargetDir(AppConfig.DEFAULT_SAVE_IMAGE_PATH)
                .setCompressListener(new OnCompressListener() {
                    @Override
                    public void onStart() {
                        // TODO 压缩开始前调用，可以在方法内启动 loading UI
                    }

                    @Override
                    public void onSuccess(File file) {
//                        String url = ContractUtils.LOGIN_URL+"ceshi/ce_upload.html";
                        OkHttpClient okHttpClient = new OkHttpClient();
//                        RequestBody requestBody1 =new FormBody.Builder()
//                                .add("driverid",substring)
//                                .add("driver_img_type","1")
//                                .build();
                        RequestBody requestBody = new MultipartBody.Builder()
                                .setType(MultipartBody.FORM)
                                .addFormDataPart("file", file.getName(), RequestBody.create(MediaType.parse("image/*"), file))
                                .build();

                        Request request = new Request.Builder()
                                .url(ContractUtils.LOGIN_URL + "index/upload")
                                .addHeader("token", ContractUtils.getTOKEN(ShangJiaBianJiActivity.this))
                                .post(requestBody)
                                .build();

                        okHttpClient.newCall(request).enqueue(new Callback() {
                            @Override
                            public void onFailure(Call call, IOException e) {
                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
//                                        Toast.makeText(ShangJiaBianJiActivity.this, "网络异常", Toast.LENGTH_SHORT).show();
                                    }
                                });
                                progressDialog.dismiss();
                            }

                            @Override
                            public void onResponse(Call call, final Response response) throws IOException {
                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        Toast.makeText(ShangJiaBianJiActivity.this, "上传成功", Toast.LENGTH_SHORT).show();
                                    }
                                });


                                progressDialog.dismiss();
                                String string = response.body().string();
                                Gson gson = new Gson();
                                photoEntity = gson.fromJson(string, PhotoEntity.class);

                                System.out.println(string + "     我的资料上传头像");
                            }
                        });
                    }

                    @Override
                    public void onError(Throwable e) {

                    }
                }).launch();
    }


}
