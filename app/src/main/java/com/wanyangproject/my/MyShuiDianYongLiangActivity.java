package com.wanyangproject.my;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;

import com.androidkun.xtablayout.XTabLayout;
import com.google.gson.Gson;
import com.wanyangproject.R;
import com.wanyangproject.adapter.MyPagerAdapter;
import com.wanyangproject.entity.MenWeiShuiDianXinXiEntity;
import com.wanyangproject.mysuipian.MyDianFragment;
import com.wanyangproject.mysuipian.MyShuiFragment;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MyShuiDianYongLiangActivity extends AppCompatActivity {

    @BindView(R.id.image_back)
    ImageView imageBack;
    //    @BindView(R.id.tabLayout)
//    TabLayout tabLayout;
    @BindView(R.id.viewPager)
    ViewPager viewPager;
    @BindView(R.id.tabLayout)
    XTabLayout tabLayout;
    private MenWeiShuiDianXinXiEntity menWeiShuiDianXinXiEntity;
    private MyShuiFragment myShuiFragment;
    private MyDianFragment myDianFragment;
    List<Fragment> fragments = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_shui_dian_yong_liang);
        ButterKnife.bind(this);


        Intent intent = getIntent();
        String response = intent.getStringExtra("response");
        System.out.println(response+"     response");
        if(response != null){
            Gson gson = new Gson();
            menWeiShuiDianXinXiEntity = gson.fromJson(response, MenWeiShuiDianXinXiEntity.class);
        }

        initPager();
    }

    private void initPager() {
//        fragments.add(new MyShuiFragment());
//        fragments.add(new MyDianFragment());
        myShuiFragment = new MyShuiFragment();
        fragments.add(myShuiFragment);
        myShuiFragment.setWeiShuiDianXinXiEntity(menWeiShuiDianXinXiEntity);
        myDianFragment = new MyDianFragment();
        fragments.add(myDianFragment);
        myDianFragment.setWeiShuiDianXinXiEntity(menWeiShuiDianXinXiEntity);

        // 创建ViewPager适配器
        MyPagerAdapter myPagerAdapter = new MyPagerAdapter(getSupportFragmentManager());
        myPagerAdapter.setFragments(fragments);
        // 给ViewPager设置适配器
        viewPager.setAdapter(myPagerAdapter);
        // TabLayout 指示器 (记得自己手动创建4个Fragment,注意是 app包下的Fragment 还是 V4包下的 Fragment)
        tabLayout.addTab(tabLayout.newTab());
        tabLayout.addTab(tabLayout.newTab());

        // 使用 TabLayout 和 ViewPager 相关联
        tabLayout.setupWithViewPager(viewPager);
        // TabLayout指示器添加文本
        tabLayout.getTabAt(0).setText("用水记录");
        tabLayout.getTabAt(1).setText("用电记录");









//        Intent intent = getIntent();
//        String stringExtra = intent.getStringExtra("yonglian");
//        if(stringExtra != null){
//            int i = Integer.parseInt(stringExtra);
//            viewPager.setCurrentItem(i);
//
//            if (i==0){
//                myShuiFragment.initYongShuiHttp();
//            }else if (i==1){
//                myDianFragment.initDianJiLuHttp();
//            }
//
//
//
//
//            viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
//                @Override
//                public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
//
//                }
//
//                @Override
//                public void onPageSelected(int position) {
//
//                    if (position==0){
//                        myShuiFragment.initYongShuiHttp();
//                    }else if (position==1){
//                        myDianFragment.initDianJiLuHttp();
//                    }
//
//
//                }
//
//                @Override
//                public void onPageScrollStateChanged(int state) {
//
//                }
//            });
//
//        }

    }

    @OnClick({R.id.image_back, R.id.tabLayout, R.id.viewPager})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.image_back:
                finish();
                break;
            case R.id.tabLayout:
                break;
            case R.id.viewPager:
                break;
        }
    }
}
