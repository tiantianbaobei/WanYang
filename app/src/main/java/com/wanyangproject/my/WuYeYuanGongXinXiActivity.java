package com.wanyangproject.my;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.wanyangproject.R;
import com.wanyangproject.entity.QiYeYuanGongXinXiEntity;
import com.wanyangproject.myadapter.WuYeYuanGongXinXiAdapter;
import com.wanyangproject.utils.ContractUtils;
import com.wanyangproject.widget.suspensionindexbar.IndexBar;
import com.zhy.http.okhttp.OkHttpUtils;
import com.zhy.http.okhttp.callback.StringCallback;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.Call;

public class WuYeYuanGongXinXiActivity extends AppCompatActivity {

    @BindView(R.id.image_back)
    ImageView imageBack;
    @BindView(R.id.image_sousuo)
    ImageView imageSousuo;
    @BindView(R.id.image_bianji)
    ImageView imageBianji;
    @BindView(R.id.recycerView)
    RecyclerView recycerView;
    @BindView(R.id.tv_a)
    TextView tvA;
    @BindView(R.id.relative_sousuo)
    RelativeLayout relativeSousuo;
    @BindView(R.id.relative_title)
    RelativeLayout relativeTitle;
    @BindView(R.id.tv_shewei_menwei)
    TextView tvSheweiMenwei;
    @BindView(R.id.et)
    EditText et;
    @BindView(R.id.zimu)
    IndexBar zimu;
    private WuYeYuanGongXinXiAdapter wuYeYuanGongXinXiAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wu_ye_yuan_gong_xin_xi);
        ButterKnife.bind(this);
        initView();


//        员工信息的网络请求
        initYuanGongXinXiHttp();

    }


    //    员工信息的网络请求
    private void initYuanGongXinXiHttp() {
        OkHttpUtils.post()
                .url(ContractUtils.LOGIN_URL + "company/getCompPeople")
                .addHeader("token", ContractUtils.getTOKEN(WuYeYuanGongXinXiActivity.this))
                .addParams("personName", et.getText().toString().trim())
                .build()
                .execute(new StringCallback() {
                    @Override
                    public void onError(Call call, Exception e, int id) {

                    }

                    @Override
                    public void onResponse(String response, int id) {
                        System.out.println(response + "       物业员工信息的网络请求");
                        ContractUtils.Code500(WuYeYuanGongXinXiActivity.this, response);
                        if (response.indexOf("200") != -1) {
                            Gson gson = new Gson();
                            QiYeYuanGongXinXiEntity qiYeYuanGongXinXiEntity = gson.fromJson(response, QiYeYuanGongXinXiEntity.class);
                            wuYeYuanGongXinXiAdapter = new WuYeYuanGongXinXiAdapter(WuYeYuanGongXinXiActivity.this, qiYeYuanGongXinXiEntity.getResponse(), imageBianji, tvSheweiMenwei, imageSousuo,zimu);
                            LinearLayoutManager manager = new LinearLayoutManager(WuYeYuanGongXinXiActivity.this);
                            recycerView.setLayoutManager(manager);
                            recycerView.setAdapter(wuYeYuanGongXinXiAdapter);
                        }
                    }
                });
    }


    private void initView() {
//        wuYeYuanGongXinXiAdapter = new WuYeYuanGongXinXiAdapter(this);
//        LinearLayoutManager manager = new LinearLayoutManager(this);
//        recycerView.setLayoutManager(manager);
//        recycerView.setAdapter(wuYeYuanGongXinXiAdapter);
    }

    @OnClick({R.id.image_back, R.id.image_sousuo, R.id.zimu,R.id.image_bianji, R.id.recycerView, R.id.tv_a, R.id.et, R.id.relative_sousuo, R.id.tv_shewei_menwei})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.image_back:
                finish();
                break;
            case R.id.image_sousuo:
                relativeSousuo.setVisibility(View.VISIBLE);
                imageSousuo.setVisibility(View.GONE);
                imageBianji.setVisibility(View.GONE);
                break;
            case R.id.image_bianji:
                imageBianji.setVisibility(View.GONE);
                imageSousuo.setVisibility(View.GONE);
                tvSheweiMenwei.setVisibility(View.VISIBLE);
                break;
            case R.id.recycerView:
                break;
            case R.id.zimu:
                break;
            case R.id.tv_a:
                break;
            case R.id.et:
                break;
            case R.id.relative_sousuo:
                break;
            case R.id.tv_shewei_menwei:
                break;
        }
    }
}
