package com.wanyangproject.my;

import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.wanyangproject.R;
import com.wanyangproject.activity.HistoryConsumptionActivity;
import com.wanyangproject.entity.MyQiYeChangFangXiangQingEntity;
import com.wanyangproject.utils.ContractUtils;
import com.zhy.http.okhttp.OkHttpUtils;
import com.zhy.http.okhttp.callback.StringCallback;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.Call;

public class MyChangFangXiangQingActivity extends AppCompatActivity {


    @BindView(R.id.tv_shuifei_money)
    TextView tvShuifeiMoney;
    @BindView(R.id.relative_shuifei)
    RelativeLayout relativeShuifei;
    @BindView(R.id.tv_dianfei_money)
    TextView tvDianfeiMoney;
    @BindView(R.id.relative_dianfei)
    RelativeLayout relativeDianfei;
    @BindView(R.id.tv_changfang)
    TextView tvChangfang;
    @BindView(R.id.image_back)
    ImageView imageBack;
    @BindView(R.id.tv_title)
    TextView tvTitle;
    @BindView(R.id.tv_yuanqu)
    TextView tvYuanqu;
    @BindView(R.id.tv_dikuai)
    TextView tvDikuai;
    @BindView(R.id.tv_loufang)
    TextView tvLoufang;
    @BindView(R.id.tv_fangjian)
    TextView tvFangjian;
    @BindView(R.id.tv_compang)
    TextView tvCompang;
    @BindView(R.id.tv_shuifei_money2)
    TextView tvShuifeiMoney2;
    @BindView(R.id.relative_shuifei2)
    RelativeLayout relativeShuifei2;
    @BindView(R.id.tv_shuifei)
    TextView tvShuifei;
    @BindView(R.id.tv_shuifei2)
    TextView tvShuifei2;
    @BindView(R.id.tv_one)
    TextView tvOne;
    @BindView(R.id.tv_two)
    TextView tvTwo;
    @BindView(R.id.tv_three)
    TextView tvThree;
    private String id;
    private String dikuai;
    private String louhao;
    private String fangjian;
    private NetWork netWork;
    private String chongzhichenggong;
    private MyQiYeChangFangXiangQingEntity myQiYeChangFangXiangQingEntity;
    private String responsechangfang;

    private String workshopId;
    private String workshopName;
    private String parkName;
    private String parcelName;
    private String roomName;
    private String buildingName;
    private String enterName;
    private String electricityUserId;
    private String electricityUserName;
    private String yudian;

    private String yu2;
    private String yu;
    private String waterUserId;
    private String waterUserName;

    private String waterUserId2;
    private String waterUserName2;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_chang_fang_xiang_qing);
        ButterKnife.bind(this);

        Intent intent = getIntent();
        id = intent.getStringExtra("id");
        dikuai = intent.getStringExtra("dikuai");
        louhao = intent.getStringExtra("louhao");
        fangjian = intent.getStringExtra("fangjian");
        System.out.println(id + "      厂房管理接收id");
        System.out.println(dikuai + "     接收地块");
        System.out.println(louhao + "     接收楼号");
        System.out.println(fangjian + "     接收房间");


        if (id != null) {
//            厂房管理详情的网络请求
            initChangFangXiangQingHttp();
        }


        IntentFilter intentFilter4 = new IntentFilter();
        intentFilter4.addAction("chongzhichenggong");
        netWork = new NetWork();
        registerReceiver(netWork, intentFilter4);
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        unregisterReceiver(netWork);
    }


    class NetWork extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {

            //            结束页面
            chongzhichenggong = intent.getStringExtra("chongzhichenggong");
            if (chongzhichenggong != null) {
                if (id != null) {
//            厂房管理详情的网络请求
                    initChangFangXiangQingHttp();
                }
            }
        }
    }


    //    厂房管理详情的网络请求
    private void initChangFangXiangQingHttp() {
        System.out.println(ContractUtils.getTOKEN(MyChangFangXiangQingActivity.this) + "      token");
        System.out.println(ContractUtils.getParkId(MyChangFangXiangQingActivity.this) + "      parkId");
        System.out.println(id + "     workshopId");
        final ProgressDialog progressDialog = new ProgressDialog(MyChangFangXiangQingActivity.this);
        progressDialog.setTitle("提示");
        progressDialog.setMessage("请等待...");
        progressDialog.setCancelable(false);
        progressDialog.show();
        OkHttpUtils.post()
                .url(ContractUtils.LOGIN_URL + "Enterprise/indexdetails")
                .addHeader("token", ContractUtils.getTOKEN(MyChangFangXiangQingActivity.this))
                .addParams("parkId", ContractUtils.getParkId(MyChangFangXiangQingActivity.this))
                .addParams("workshopId", id)
                .build()
                .connTimeOut(20000)
                .writeTimeOut(20000)
                .readTimeOut(20000)
                .execute(new StringCallback() {
                    @Override
                    public void onError(Call call, Exception e, int id) {
                        System.out.println(e + "     eeee厂房管理详情");
                        progressDialog.dismiss();
                    }

                    @Override
                    public void onResponse(String response, int id) {
                        System.out.println(response + "            厂房管理详情的网络请求");
                        ContractUtils.Code500(MyChangFangXiangQingActivity.this, response);
                        progressDialog.dismiss();

//                        40013接口查出结果为空的实体类
//                        ChangFangWuShuJuEntity


                        JSONObject jsonObject = null;
                        try {
                            jsonObject = new JSONObject(response);
                            JSONObject jsonObject1 = jsonObject.getJSONObject("response");
                            JSONObject jsonObject2 = jsonObject1.getJSONObject("data");
                            workshopId = jsonObject2.getString("workshopId");
                            workshopName = jsonObject2.getString("workshopName");
                            parkName = jsonObject2.getString("parkName");
                            parcelName = jsonObject2.getString("parcelName");
                            buildingName = jsonObject2.getString("buildingName");
                            roomName = jsonObject2.getString("roomName");
                            enterName = jsonObject2.getString("enterName");


//                            tvTitle.setText(dikuai + "地块" + louhao + "号楼" + fangjian + "室");
                            tvTitle.setText(workshopName);
                            tvYuanqu.setText(parkName);
                            tvDikuai.setText(parcelName + "地块");

                            String title = buildingName;
                            String[] split = title.split("#");
//                            tvLoufang.setText(louhao + "号楼");
                            tvLoufang.setText(buildingName);

//                            tvFangjian.setText(fangjian + "室");
                            tvFangjian.setText(roomName);
                            tvCompang.setText(enterName);


                            JSONObject shui = jsonObject1.getJSONObject("shui");
                            JSONArray data = shui.getJSONArray("data");
                            if (data != null) {
                                if (data.length() >= 1) {
                                    JSONObject object = (JSONObject) data.get(0);
                                    waterUserId = object.getString("waterUserId");
                                    waterUserName = object.getString("waterUserName");
                                    yu = object.getString("yu");
                                }
                                tvShuifeiMoney.setText("¥" + yu);
                                tvShuifei.setText(waterUserName + "：");
                            }


                            if (data != null) {
                                if (data.length() >= 2) {
                                    JSONObject object = (JSONObject) data.get(1);
                                    waterUserId2 = object.getString("waterUserId");
                                    waterUserName2 = object.getString("waterUserName");
                                    yu2 = object.getString("yu");
                                }
                                tvShuifeiMoney2.setText("¥" + yu2);
                                tvShuifei2.setText(waterUserName2 + "：");
                            }


                            JSONObject dian = jsonObject1.getJSONObject("dian");

                            if (dian != null) {
                                electricityUserId = dian.getString("electricityUserId");
                                electricityUserName = dian.getString("electricityUserName");
                                yudian = dian.getString("yu");

                                tvDianfeiMoney.setText(yudian);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }


                        if (waterUserId == null) {
                            relativeShuifei.setVisibility(View.GONE);
                        }

                        if (waterUserId2 == null) {
                            relativeShuifei2.setVisibility(View.GONE);
                        }


                        if (electricityUserId == null) {
                            relativeDianfei.setVisibility(View.GONE);
                        }


//                        if (response.indexOf("200") != -1) {
//
//                            responsechangfang = response;
//
//                            Gson gson = new Gson();
//                            myQiYeChangFangXiangQingEntity = gson.fromJson(response, MyQiYeChangFangXiangQingEntity.class);
//
//
//
//                            if(myQiYeChangFangXiangQingEntity.getResponse().getShui().getCode().indexOf("200") != -1){
//                                if(myQiYeChangFangXiangQingEntity.getResponse().getShui().getData().size() >= 1){
//                                    if(myQiYeChangFangXiangQingEntity.getResponse().getShui().getData().get(0).getYu() != null){
//                                        tvShuifeiMoney.setText(myQiYeChangFangXiangQingEntity.getResponse().getShui().getData().get(0).getYu());
//                                    }
//                                }
//                            }
//
//
//
//                            if(myQiYeChangFangXiangQingEntity.getResponse().getShui().getCode().indexOf("200") != -1){
//                                if(myQiYeChangFangXiangQingEntity.getResponse().getShui().getData().size() >= 2){
//                                    if(myQiYeChangFangXiangQingEntity.getResponse().getShui().getData().get(1).getYu() != null){
//                                        tvShuifeiMoney2.setText(myQiYeChangFangXiangQingEntity.getResponse().getShui().getData().get(1).getYu());
//                                    }
//                                }
//                            }
//
//
//
//
//
//
//                            if (myQiYeChangFangXiangQingEntity.getResponse().getData().getWorkshopName() == null) {
//
//                            } else {
//                                tvTitle.setText(dikuai + "地块" + louhao + "号楼" + fangjian + "室");
//                            }
//
//
//                            if (myQiYeChangFangXiangQingEntity.getResponse().getData().getParkName() == null) {
//
//                            } else {
//                                tvYuanqu.setText(myQiYeChangFangXiangQingEntity.getResponse().getData().getParkName());
//                            }
//
//
//                            if (myQiYeChangFangXiangQingEntity.getResponse().getData().getParcelName() == null) {
//
//                            } else {
//                                tvDikuai.setText(dikuai);
//                            }
//
//
//                            if (myQiYeChangFangXiangQingEntity.getResponse().getData().getBuildingName() == null) {
//
//                            } else {
//                                tvLoufang.setText(louhao);
//                            }
//
//
//                            if (myQiYeChangFangXiangQingEntity.getResponse().getData().getRoomName() == null) {
//
//                            } else {
//                                tvFangjian.setText(fangjian);
//                            }
//
//
//                            if (myQiYeChangFangXiangQingEntity.getResponse().getData().getEnterName() == null) {
//
//                            } else {
//                                tvCompang.setText(myQiYeChangFangXiangQingEntity.getResponse().getData().getEnterName());
//                            }
//                        }
                    }
                });
    }


    @OnClick({R.id.image_back, R.id.tv_title, R.id.tv_one, R.id.tv_two, R.id.tv_three,R.id.tv_shuifei_money2, R.id.relative_shuifei2, R.id.tv_yuanqu, R.id.tv_dikuai, R.id.tv_loufang, R.id.tv_fangjian, R.id.tv_compang, R.id.tv_shuifei_money, R.id.relative_shuifei, R.id.tv_dianfei_money, R.id.relative_dianfei})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.image_back:
                finish();
                break;
            case R.id.tv_title:
                break;
            case R.id.tv_yuanqu:
                break;
            case R.id.tv_dikuai:
                break;
            case R.id.tv_loufang:
                break;
            case R.id.tv_fangjian:
                break;
            case R.id.tv_compang:
                break;
            case R.id.tv_shuifei_money:
                break;
//            水费1
            case R.id.relative_shuifei:
                if (waterUserId == null) {
                    Toast.makeText(this, "暂无水表信息！", Toast.LENGTH_SHORT).show();
                    return;
                }


                Intent intent = new Intent(MyChangFangXiangQingActivity.this, ShuiDianFeiJiLuActivity.class);
                intent.putExtra("yue", yu);
                intent.putExtra("waterUserName", waterUserName);
                intent.putExtra("id", waterUserId);
                intent.putExtra("shuione", "0");

                intent.putExtra("yue2", yu2);
                intent.putExtra("waterUserName2", waterUserName2);
                intent.putExtra("id2", waterUserId2);
//                intent.putExtra("responsechangfang",responsechangfang);
                startActivity(intent);
                break;
            case R.id.tv_shuifei_money2:
                break;
//            水费2
            case R.id.relative_shuifei2:
                if (waterUserId2 == null) {
                    Toast.makeText(this, "暂无水表信息！", Toast.LENGTH_SHORT).show();
                    return;
                }
                Intent intent1 = new Intent(MyChangFangXiangQingActivity.this, ShuiDianFeiJiLuActivity.class);
                intent1.putExtra("yue2", yu2);
                intent1.putExtra("waterUserName2", waterUserName2);
                intent1.putExtra("id2", waterUserId2);
                intent1.putExtra("shuione", "1");

                intent1.putExtra("yue", yu);
                intent1.putExtra("waterUserName", waterUserName);
                intent1.putExtra("id", waterUserId);
//                intent1.putExtra("responsechangfang",responsechangfang);
                startActivity(intent1);


                break;
            case R.id.tv_dianfei_money:
                break;
//            电费
            case R.id.relative_dianfei:
                if (electricityUserId == null) {
                    Toast.makeText(this, "暂无电表信息！", Toast.LENGTH_SHORT).show();
                    return;
                }
                Intent intent2 = new Intent(MyChangFangXiangQingActivity.this, ShuiDianFeiJiLuActivity.class);
//                intent2.putExtra("qiyechangfang",response11);
                intent2.putExtra("electricityUserId", electricityUserId);
                intent2.putExtra("electricityUserName", electricityUserName);
                intent2.putExtra("yudian", yudian);
                intent2.putExtra("dian", "dian");
                startActivity(intent2);
                break;
            case R.id.tv_one://水表1
                HistoryConsumptionActivity.start(MyChangFangXiangQingActivity.this, "1", waterUserId);
                break;
            case R.id.tv_two://水表2
                HistoryConsumptionActivity.start(MyChangFangXiangQingActivity.this, "1", waterUserId2);
                break;
            case R.id.tv_three://电表1
                HistoryConsumptionActivity.start(MyChangFangXiangQingActivity.this, "2", electricityUserId);
                break;
        }
    }


}
