package com.wanyangproject.my;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.androidkun.xtablayout.XTabLayout;
import com.wanyangproject.R;
import com.wanyangproject.activity.FaTieActivity;
import com.wanyangproject.adapter.MyPagerAdapter;
import com.wanyangproject.mysuipian.MyDianZanFragment;
import com.wanyangproject.mysuipian.MyFaBuFragment;
import com.wanyangproject.mysuipian.MyPingLunFragment;
import com.wanyangproject.utils.NoScrollViewPager;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MyLunTanActivity extends AppCompatActivity {


    //    @BindView(R.id.tabLayout)
//    TabLayout tabLayout;
    @BindView(R.id.viewPager)
    NoScrollViewPager viewPager;
    @BindView(R.id.tv_fabu)
    TextView tvFabu;
    @BindView(R.id.image_back)
    ImageView imageBack;
    @BindView(R.id.tabLayout)
    XTabLayout tabLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_lun_tan);
        ButterKnife.bind(this);
        initPager();
    }

    private void initPager() {
        List<Fragment> fragments = new ArrayList<>();
        fragments.add(new MyFaBuFragment());
        fragments.add(new MyPingLunFragment());
        fragments.add(new MyDianZanFragment());
        // 创建ViewPager适配器
        MyPagerAdapter myPagerAdapter = new MyPagerAdapter(getSupportFragmentManager());
        myPagerAdapter.setFragments(fragments);
        // 给ViewPager设置适配器
        viewPager.setAdapter(myPagerAdapter);
        // TabLayout 指示器 (记得自己手动创建4个Fragment,注意是 app包下的Fragment 还是 V4包下的 Fragment)
        tabLayout.addTab(tabLayout.newTab());
        tabLayout.addTab(tabLayout.newTab());
        tabLayout.addTab(tabLayout.newTab());
        // 使用 TabLayout 和 ViewPager 相关联
        tabLayout.setupWithViewPager(viewPager);
        // TabLayout指示器添加文本
        tabLayout.getTabAt(0).setText("我的发布");
        tabLayout.getTabAt(1).setText("我的评论");
        tabLayout.getTabAt(2).setText("我的点赞");
    }

    @OnClick({R.id.image_back, R.id.tv_fabu, R.id.tabLayout, R.id.viewPager})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.image_back:
                finish();
                break;
            case R.id.tabLayout:
                break;
            case R.id.viewPager:
                break;
            case R.id.tv_fabu:
                Intent intent = new Intent(MyLunTanActivity.this, FaTieActivity.class);
                startActivity(intent);
                break;
        }
    }
}
