package com.wanyangproject.my;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.wanyangproject.R;
import com.wanyangproject.entity.ShangJiaShangPinGuanLiEntity;
import com.wanyangproject.entity.ShangJiaShangPinZhuangTaiEntity;
import com.wanyangproject.entity.UserEntity;
import com.wanyangproject.myadapter.LeftAdapter;
import com.wanyangproject.myadapter.RightAdapter;
import com.wanyangproject.utils.ContractUtils;
import com.zhy.http.okhttp.OkHttpUtils;
import com.zhy.http.okhttp.callback.StringCallback;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.Call;

public class ShangJiaShangPinGuanLiActivity extends AppCompatActivity {

    @BindView(R.id.image_back)
    ImageView imageBack;
    @BindView(R.id.left_recyclerView)
    RecyclerView leftRecyclerView;
    @BindView(R.id.right_recyclerView)
    RecyclerView rightRecyclerView;
    @BindView(R.id.tv_name)
    TextView tvName;
    private LeftAdapter leftAdapter;
    private RightAdapter rightAdapter;
    private UserEntity userEntity;
    private String phone;
    private ShangJiaShangPinGuanLiEntity shangJiaShangPinGuanLiEntity;
    private NetWork netWork;
    private String name;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shang_jia_shang_pin_guan_li);
        ButterKnife.bind(this);
        initView();
        //        获取用户信息的网络请求
        initYongHuXinXiHttp();


        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("shangjianame");
        netWork = new NetWork();
        registerReceiver(netWork, intentFilter);

    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        unregisterReceiver(netWork);
    }

    class NetWork extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            name = intent.getStringExtra("shangjianame");
            if (name != null) {
                tvName.setText(name);
            }
//            Toast.makeText(context, wxopenid + wxnickname + wxheadimgurl, Toast.LENGTH_SHORT).show();

        }
    }


    //    获取用户信息的网络请求
    private void initYongHuXinXiHttp() {
        OkHttpUtils.post()
                .url(ContractUtils.LOGIN_URL + "Life/user")
                .addHeader("token", ContractUtils.getTOKEN(ShangJiaShangPinGuanLiActivity.this))
                .build()
                .execute(new StringCallback() {
                    @Override
                    public void onError(Call call, Exception e, int id) {
                        System.out.println(e + "       eeee用户信息");
                    }

                    @Override
                    public void onResponse(String response, int id) {
                        ContractUtils.Code500(ShangJiaShangPinGuanLiActivity.this, response);
                        System.out.println(response + "    获取个人用户信息的网络请求");

                        if (response.indexOf("200") != -1) {
                            Gson gson = new Gson();
                            userEntity = gson.fromJson(response, UserEntity.class);
                            phone = userEntity.getResponse().getUsername();//获取手机号
                            System.out.println(phone + "     用户的手机号");
                            //        个人中心商家商品页面的网络请求
                            initShangJiaShangPinHttp();
                        }
                    }
                });
    }


    //    个人中心商家商品页面的网络请求
    private void initShangJiaShangPinHttp() {
        System.out.println(ContractUtils.getTOKEN(ShangJiaShangPinGuanLiActivity.this) + "      token ");
        System.out.println(ContractUtils.getParkId(ShangJiaShangPinGuanLiActivity.this) + "         parkId");
        System.out.println(ContractUtils.getPhone(ShangJiaShangPinGuanLiActivity.this) + "         phone");
        OkHttpUtils.post()
                .url(ContractUtils.LOGIN_URL + "Shop/shop")
                .addHeader("token", ContractUtils.getTOKEN(ShangJiaShangPinGuanLiActivity.this))
                .addParams("parkId", ContractUtils.getParkId(ShangJiaShangPinGuanLiActivity.this))
                .addParams("phone", ContractUtils.getPhone(ShangJiaShangPinGuanLiActivity.this)) //暂时  tiantian 应该传phone
                .build()
                .execute(new StringCallback() {
                    @Override
                    public void onError(Call call, Exception e, int id) {
                        System.out.println(e + "     商家商品页面eee");
                    }

                    @Override
                    public void onResponse(String response, int id) {
                        ContractUtils.Code500(ShangJiaShangPinGuanLiActivity.this, response);
                        System.out.println(response + "         个人中心商家商品页面的网络请求");

                        if (response.indexOf("200") != -1) {
                            Gson gson = new Gson();
                            shangJiaShangPinGuanLiEntity = gson.fromJson(response, ShangJiaShangPinGuanLiEntity.class);
                            if (shangJiaShangPinGuanLiEntity.getResponse().getGoods().size() > 0) {
                                shangJiaShangPinGuanLiEntity.getResponse().getGoods().get(0).setOk(true);
                            }


//                            商家商品左边的列表
                            leftAdapter = new LeftAdapter(ShangJiaShangPinGuanLiActivity.this, shangJiaShangPinGuanLiEntity.getResponse().getGoods());
                            LinearLayoutManager manager = new LinearLayoutManager(ShangJiaShangPinGuanLiActivity.this);
                            leftRecyclerView.setLayoutManager(manager);
                            leftRecyclerView.setAdapter(leftAdapter);
                            leftAdapter.setLeftClick(new LeftAdapter.LeftClick() {
                                @Override
                                public void leftClick(int position) {
                                    rightAdapter.notifyDataSetChanged();
                                }
                            });


//                            商家商品右边的列表
                            rightAdapter = new RightAdapter(ShangJiaShangPinGuanLiActivity.this, shangJiaShangPinGuanLiEntity.getResponse().getGoods());
                            LinearLayoutManager manager1 = new LinearLayoutManager(ShangJiaShangPinGuanLiActivity.this);
                            rightRecyclerView.setLayoutManager(manager1);
                            rightRecyclerView.setAdapter(rightAdapter);

//                            上架
                            rightAdapter.setShangJiaClick(new RightAdapter.ShangJiaClick() {
                                @Override
                                public void shangjiaclick(int position, String type, String id) {
                                    //           更改商品状态的网络请求
                                    initShangPinZhuangTaiHttp(position, type, id);
                                }
                            });

//                            下架
                            rightAdapter.setXiaJiaClick(new RightAdapter.XiaJiaClick() {
                                @Override
                                public void xiajiaClick(int position, String type, String id) {
                                    //           更改商品状态的网络请求
                                    initShangPinZhuangTaiHttp(position, type, id);
                                }
                            });


                        }else if(response.indexOf("400") != -1) {
                            ContractUtils.Code400(ShangJiaShangPinGuanLiActivity.this,response);
                        }
                    }
                });
    }


    //    更改商品状态的网络请求
    private void initShangPinZhuangTaiHttp(int position, String type, String id) {
        if (type.equals("1")) {
            type = "2";
        } else {
            type = "1";
        }

        OkHttpUtils.post()
                .url(ContractUtils.LOGIN_URL + "Shop/goodstype")
                .addHeader("token", ContractUtils.getTOKEN(ShangJiaShangPinGuanLiActivity.this))
                .addParams("parkId", ContractUtils.getParkId(ShangJiaShangPinGuanLiActivity.this))
                .addParams("phone", phone)
                .addParams("goods_id", id)
                .addParams("type", type)
                .build()
                .execute(new StringCallback() {
                    @Override
                    public void onError(Call call, Exception e, int id) {

                    }

                    @Override
                    public void onResponse(String response, int id) {
                        ContractUtils.Code500(ShangJiaShangPinGuanLiActivity.this, response);
                        System.out.println(response + "       更改商品状态的网络请求");
                        Gson gson = new Gson();
                        ShangJiaShangPinZhuangTaiEntity shangJiaShangPinZhuangTaiEntity = gson.fromJson(response, ShangJiaShangPinZhuangTaiEntity.class);

                    }
                });
    }

    private void initView() {
//        leftAdapter = new LeftAdapter(this);
//        LinearLayoutManager manager = new LinearLayoutManager(this);
//        leftRecyclerView.setLayoutManager(manager);
//        leftRecyclerView.setAdapter(leftAdapter);


//        rightAdapter = new RightAdapter(this);
//        LinearLayoutManager manager1 = new LinearLayoutManager(this);
//        rightRecyclerView.setLayoutManager(manager1);
//        rightRecyclerView.setAdapter(rightAdapter);

    }

    @OnClick({R.id.image_back, R.id.left_recyclerView, R.id.right_recyclerView})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.image_back:
                finish();
                break;
            case R.id.left_recyclerView:
                break;
            case R.id.right_recyclerView:
                break;
        }
    }
}
