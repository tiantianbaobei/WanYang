package com.wanyangproject.my;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.wanyangproject.R;
import com.wanyangproject.entity.FangKeGuanLiEntity;
import com.wanyangproject.myadapter.FangKeGuanLiAdapter;
import com.wanyangproject.myadapter.MenWeiFangKeGuanLiAdapter;
import com.wanyangproject.utils.ContractUtils;
import com.zhy.http.okhttp.OkHttpUtils;
import com.zhy.http.okhttp.callback.StringCallback;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.Call;

public class MyMenWeiFangKeGuanLiActivity extends AppCompatActivity {

    @BindView(R.id.image_back)
    ImageView imageBack;
    @BindView(R.id.tv_tianjia_fangke)
    TextView tvTianjiaFangke;
    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    private MenWeiFangKeGuanLiAdapter menWeiFangKeGuanLiAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_men_wei_fang_ke_guan_li);
        ButterKnife.bind(this);
        initView();

        //        访客管理的网络请求
        initFangKeGuanLiHttp();
    }



    //    访客管理的网络请求
    private void initFangKeGuanLiHttp() {
        OkHttpUtils.post()
                .url(ContractUtils.LOGIN_URL+"fuwu/fangke")
                .addHeader("token",ContractUtils.getTOKEN(MyMenWeiFangKeGuanLiActivity.this))
                .addParams("parkId",ContractUtils.getParkId(MyMenWeiFangKeGuanLiActivity.this))
                .addParams("page","1")
                .build()
                .execute(new StringCallback() {
                    @Override
                    public void onError(Call call, Exception e, int id) {
                        System.out.println(e+"          eeee企业 访客管理");
                    }

                    @Override
                    public void onResponse(String response, int id) {
                        System.out.println(response+"           企业 访客管理的网络请求");
                        ContractUtils.Code500(MyMenWeiFangKeGuanLiActivity.this,response);
                        if(response.indexOf("200") != -1){
                            Gson gson = new Gson();
                            FangKeGuanLiEntity fangKeGuanLiEntity = gson.fromJson(response, FangKeGuanLiEntity.class);
                            menWeiFangKeGuanLiAdapter = new MenWeiFangKeGuanLiAdapter(MyMenWeiFangKeGuanLiActivity.this,fangKeGuanLiEntity.getResponse());
                            LinearLayoutManager manager = new LinearLayoutManager(MyMenWeiFangKeGuanLiActivity.this);
                            recyclerView.setLayoutManager(manager);
                            recyclerView.setAdapter(menWeiFangKeGuanLiAdapter);



                        }
                    }
                });
    }






    private void initView() {
//        menWeiFangKeGuanLiAdapter = new MenWeiFangKeGuanLiAdapter(this);
//        LinearLayoutManager manager = new LinearLayoutManager(this);
//        recyclerView.setLayoutManager(manager);
//        recyclerView.setAdapter(menWeiFangKeGuanLiAdapter);
//
//        menWeiFangKeGuanLiAdapter.setBianJiClick(new MenWeiFangKeGuanLiAdapter.BianJiClick() {
//            @Override
//            public void banjiclick(int position) {
//                Intent intent = new Intent(MyMenWeiFangKeGuanLiActivity.this,MyMenWeiBianJiFangKeActivity.class);
//                startActivity(intent);
//            }
//        });
    }


    @OnClick({R.id.image_back, R.id.tv_tianjia_fangke, R.id.recyclerView})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.image_back:
                finish();
                break;
            case R.id.tv_tianjia_fangke:
//                Intent intent = new Intent(MyMenWeiFangKeGuanLiActivity.this,MyTianJiaFangKeActivity.class);
//                startActivity(intent);
                break;
            case R.id.recyclerView:
                break;
        }
    }
}
