package com.wanyangproject.my;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.wanyangproject.R;
import com.wanyangproject.entity.QiYeChangFangGuanLiEntity;
import com.wanyangproject.myadapter.ChangFangGuanLiAdapter;
import com.wanyangproject.utils.ContractUtils;
import com.zhy.http.okhttp.OkHttpUtils;
import com.zhy.http.okhttp.callback.StringCallback;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.Call;

public class MyChangFangGuanLiActivity extends AppCompatActivity {

    @BindView(R.id.image_back)
    ImageView imageBack;
    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    @BindView(R.id.image_sousuo)
    ImageView imageSousuo;
    @BindView(R.id.relative_sousuo)
    RelativeLayout relativeSousuo;
    @BindView(R.id.tv_changfang)
    TextView tvChangfang;
    @BindView(R.id.et_sousuo)
    EditText etSousuo;
    private ChangFangGuanLiAdapter changFangGuanLiAdapter;
    private ArrayList<QiYeChangFangGuanLiEntity.ResponseBean.DataBean> shaixuaihou = new ArrayList();//筛选之后的
    private QiYeChangFangGuanLiEntity qiYeChangFangGuanLiEntity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_chang_fang_guan_li);
        ButterKnife.bind(this);
        initView();


//         企业身份厂房管理的网络请求
        initChangFangGuanLiHttp();



        etSousuo.addTextChangedListener(new TextWatcher() {
            @Override  //   输入文字之前的状态
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override // 输入文字中的状态
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override  // 输入文字之后的状态
            public void afterTextChanged(Editable editable) {
//                etSousuo.getText().toString().trim();
                shaixuaihou.clear();
                if(qiYeChangFangGuanLiEntity != null){
                    if(qiYeChangFangGuanLiEntity.getResponse() != null){
                        if(qiYeChangFangGuanLiEntity.getResponse().getData() != null){
                            for (int i = 0; i < qiYeChangFangGuanLiEntity.getResponse().getData().size(); i++) {
                                QiYeChangFangGuanLiEntity.ResponseBean.DataBean dataBean = qiYeChangFangGuanLiEntity.getResponse().getData().get(i);
                                if(dataBean.getWorkshopName().indexOf(etSousuo.getText().toString().trim()) != -1){
                                    shaixuaihou.add(dataBean);
                                }
                            }
                            changFangGuanLiAdapter.notifyDataSetChanged();
                        }
                    }
                }

                
//                //         企业身份厂房管理的网络请求
//                initChangFangGuanLiHttp();
            }
        });
    }




    //    企业身份厂房管理的网络请求
    private void initChangFangGuanLiHttp() {
        OkHttpUtils.post()
                .url(ContractUtils.LOGIN_URL + "Enterprise/index")
                .addHeader("token", ContractUtils.getTOKEN(MyChangFangGuanLiActivity.this))
                .addParams("parkId", ContractUtils.getParkId(MyChangFangGuanLiActivity.this))
                .build()
                .execute(new StringCallback() {
                    @Override
                    public void onError(Call call, Exception e, int id) {
                        System.out.println(e+"      eeee企业身份厂房管理");
                    }

                    @Override
                    public void onResponse(String response, int id) {
                        System.out.println(ContractUtils.getTOKEN(MyChangFangGuanLiActivity.this) + "            厂房token");
                        System.out.println(ContractUtils.getParkId(MyChangFangGuanLiActivity.this) + "            厂房parkId");
                        System.out.println(response + "          企业身份厂房管理的网络请求");
                        ContractUtils.Code500(MyChangFangGuanLiActivity.this, response);
                        if (response.indexOf("200") != -1) {
                            try {
                                Log.d("TAG","response=="+response.toString());

                                JSONObject jsonObject = new JSONObject(response);
                                JSONObject jsonObject1 = jsonObject.getJSONObject("response");
                                JSONArray data = jsonObject1.getJSONArray("data");

                                if (data == null) {
                                    Toast.makeText(MyChangFangGuanLiActivity.this, "暂无数据！", Toast.LENGTH_SHORT).show();
                                    return;
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }




                            Gson gson = new Gson();
                            qiYeChangFangGuanLiEntity = gson.fromJson(response, QiYeChangFangGuanLiEntity.class);


                            if(qiYeChangFangGuanLiEntity != null){
                                if(qiYeChangFangGuanLiEntity.getResponse() != null){
                                    if(qiYeChangFangGuanLiEntity.getResponse().getData() != null){
                                        shaixuaihou.addAll(qiYeChangFangGuanLiEntity.getResponse().getData());
                                    }
                                }
                            }


                            changFangGuanLiAdapter = new ChangFangGuanLiAdapter(MyChangFangGuanLiActivity.this, shaixuaihou);
                            LinearLayoutManager manager = new LinearLayoutManager(MyChangFangGuanLiActivity.this);
                            recyclerView.setLayoutManager(manager);
                            recyclerView.setAdapter(changFangGuanLiAdapter);


                            changFangGuanLiAdapter.setXiangQingClick(new ChangFangGuanLiAdapter.XiangQingClick() {
                                @Override
                                public void xiangqingClick(int position, String id, String dikuai, String louhao, String fangjian) {
                                    Intent intent = new Intent(MyChangFangGuanLiActivity.this, MyChangFangXiangQingActivity.class);
                                    intent.putExtra("id", id);
                                    intent.putExtra("dikuai", dikuai);
                                    intent.putExtra("louhao", louhao);
                                    intent.putExtra("fangjian", fangjian);
                                    System.out.println(dikuai + "    地块");
                                    System.out.println(louhao + "      楼号");
                                    System.out.println(fangjian + "      房间");
                                    System.out.println(id + "      厂房管理的id");
                                    startActivity(intent);
                                }
                            });


//                            changFangGuanLiAdapter.setXiangQingClick(new ChangFangGuanLiAdapter.XiangQingClick() {
//                                @Override
//                                public void xiangqingClick(int position,String id,String dikuai) {
//                                    Intent intent = new Intent(MyChangFangGuanLiActivity.this,MyChangFangXiangQingActivity.class);
//                                    intent.putExtra("id",id);
//                                    System.out.println(id+"      厂房管理的id");
//                                    startActivity(intent);
//                                }
//                            });
                        } else {
                            ContractUtils.Code400(MyChangFangGuanLiActivity.this, response);
                        }
                    }
                });
    }


    private void initView() {
//        changFangGuanLiAdapter = new ChangFangGuanLiAdapter(this);
//        LinearLayoutManager manager = new LinearLayoutManager(this);
//        recyclerView.setLayoutManager(manager);
//        recyclerView.setAdapter(changFangGuanLiAdapter);

//        changFangGuanLiAdapter.setXiangQingClick(new ChangFangGuanLiAdapter.XiangQingClick() {
//            @Override
//            public void xiangqingClick(int position) {
//                Intent intent = new Intent(MyChangFangGuanLiActivity.this,MyChangFangXiangQingActivity.class);
//                startActivity(intent);
//            }
//        });
    }


    @OnClick({R.id.image_back, R.id.recyclerView, R.id.image_sousuo})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.image_back:
                finish();
                break;
            case R.id.recyclerView:
                break;
//            搜索
            case R.id.image_sousuo:
                tvChangfang.setVisibility(View.INVISIBLE);
                relativeSousuo.setVisibility(View.VISIBLE);
                imageSousuo.setVisibility(View.GONE);
                break;

        }
    }
}
