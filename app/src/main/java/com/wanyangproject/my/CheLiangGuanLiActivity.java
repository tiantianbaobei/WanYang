package com.wanyangproject.my;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;

import com.google.gson.Gson;
import com.wanyangproject.R;
import com.wanyangproject.entity.QiYeCheLiangGuanLiEntity;
import com.wanyangproject.myadapter.MyCheLianfAdapter;
import com.wanyangproject.utils.ContractUtils;
import com.zhy.http.okhttp.OkHttpUtils;
import com.zhy.http.okhttp.callback.StringCallback;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.Call;

public class CheLiangGuanLiActivity extends AppCompatActivity {

    @BindView(R.id.image_back)
    ImageView imageBack;
    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    private MyCheLianfAdapter myCheLianfAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_che_liang_guan_li);
        ButterKnife.bind(this);
        initView();


//        车辆管理的网络请求
        initCheLiangGuanLiHttp();

    }


//    车辆管理的网络请求
    private void initCheLiangGuanLiHttp() {
        OkHttpUtils.post()
                .url(ContractUtils.LOGIN_URL+"company/getCompCar")
                .addHeader("token",ContractUtils.getTOKEN(CheLiangGuanLiActivity.this))
                .build()
                .execute(new StringCallback() {
                    @Override
                    public void onError(Call call, Exception e, int id) {

                    }

                    @Override
                    public void onResponse(String response, int id) {
                        ContractUtils.Code500(CheLiangGuanLiActivity.this,response);
                        System.out.println(response+"          车辆管理的网络请求");
                        if(response.indexOf("200") != -1){
                            Gson gson = new Gson();
                            QiYeCheLiangGuanLiEntity qiYeCheLiangGuanLiEntity = gson.fromJson(response, QiYeCheLiangGuanLiEntity.class);
                            myCheLianfAdapter = new MyCheLianfAdapter(CheLiangGuanLiActivity.this,qiYeCheLiangGuanLiEntity.getResponse());
                            LinearLayoutManager manager = new LinearLayoutManager(CheLiangGuanLiActivity.this);
                            recyclerView.setLayoutManager(manager);
                            recyclerView.setAdapter(myCheLianfAdapter);
                        }
                    }
                });
    }



    private void initView() {
//        myCheLianfAdapter = new MyCheLianfAdapter(CheLiangGuanLiActivity.this);
//        LinearLayoutManager manager = new LinearLayoutManager(this);
//        recyclerView.setLayoutManager(manager);
//        recyclerView.setAdapter(myCheLianfAdapter);
    }

    @OnClick({R.id.image_back, R.id.recyclerView})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.image_back:
                finish();
                break;
            case R.id.recyclerView:
                break;
        }
    }
}
