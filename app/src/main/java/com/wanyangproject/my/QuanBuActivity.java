package com.wanyangproject.my;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.androidkun.xtablayout.XTabLayout;
import com.wanyangproject.R;
import com.wanyangproject.adapter.MyPagerAdapter;
import com.wanyangproject.mysuipian.DaiPeiSongFragment;
import com.wanyangproject.mysuipian.DaiPingJiaFragment;
import com.wanyangproject.mysuipian.DaiShouHuoFragment;
import com.wanyangproject.mysuipian.DaiTuiKuanFragment;
import com.wanyangproject.mysuipian.DaiZhiFuFragment;
import com.wanyangproject.mysuipian.DingDanQuanBuFragment;
import com.wanyangproject.mysuipian.FinishFragment;
import com.wanyangproject.mysuipian.YiJieDanFragment;
import com.wanyangproject.mysuipian.YiWanChengFragment;
import com.wanyangproject.net.util.Constant;
import com.wanyangproject.utils.ContractUtils;
import com.zhy.http.okhttp.OkHttpUtils;
import com.zhy.http.okhttp.callback.StringCallback;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.Call;

public class QuanBuActivity extends AppCompatActivity {


    @BindView(R.id.viewPager)
    ViewPager viewPager;
    //    @BindView(R.id.tabLayout)
//    TabLayout tabLayout;
    @BindView(R.id.image_back)
    ImageView imageBack;
    @BindView(R.id.relative_title)
    RelativeLayout relativeTitle;
    @BindView(R.id.tabLayout)
    XTabLayout tabLayout;
    List<Fragment> fragments = new ArrayList<>();
    private DingDanQuanBuFragment dingDanQuanBuFragment;
    private DaiZhiFuFragment daiZhiFuFragment;
    private DaiShouHuoFragment daiShouHuoFragment;
    private DaiPingJiaFragment daiPingJiaFragment;
    private FinishFragment finishFragment;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_quan_bu);
        ButterKnife.bind(this);
//        barTitle.setText("我的订单");
        initPager();

//        订单列表的网络请求
        initDingDanLieBiaoHttp();
    }


    //    订单列表的网络请求
    private void initDingDanLieBiaoHttp() {
        OkHttpUtils.post()
                .url(ContractUtils.LOGIN_URL + "Life/orderlist")
                .addHeader("token", ContractUtils.getTOKEN(QuanBuActivity.this))
                .addParams("parkId", ContractUtils.getParkId(QuanBuActivity.this))
                .build()
                .execute(new StringCallback() {
                    @Override
                    public void onError(Call call, Exception e, int id) {
                        System.out.println(e + "      订单详情的网络请求eeeee");
                    }

                    @Override
                    public void onResponse(String response, int id) {
                        System.out.println(response + "     订单详情的网络请求");
                        ContractUtils.Code500(QuanBuActivity.this, response);
                    }
                });
    }


    public static void start(Context context, int order) {
        Intent intent = new Intent(context, QuanBuActivity.class);
        intent.putExtra(Constant.ORDER_INDEX, order);
        context.startActivity(intent);
    }


    private void initPager() {
        dingDanQuanBuFragment = new DingDanQuanBuFragment();
        daiZhiFuFragment = new DaiZhiFuFragment();
        daiShouHuoFragment = new DaiShouHuoFragment();
        daiPingJiaFragment = new DaiPingJiaFragment();
        finishFragment = new FinishFragment();
        fragments.add(dingDanQuanBuFragment);
        fragments.add(daiZhiFuFragment);
        fragments.add(daiShouHuoFragment);
        fragments.add(daiPingJiaFragment);
        fragments.add(finishFragment);

//        List<Fragment> fragments = new ArrayList<>();
//        fragments.add(new DingDanQuanBuFragment());//quanbu
//        fragments.add(new DaiZhiFuFragment());//1
//        fragments.add(new DaiShouHuoFragment());//34567
//        fragments.add(new DaiPingJiaFragment());//8
//        fragments.add(new FinishFragment());
        // 创建ViewPager适配器
        MyPagerAdapter myPagerAdapter = new MyPagerAdapter(getSupportFragmentManager());
        myPagerAdapter.setFragments(fragments);
        // 给ViewPager设置适配器
        viewPager.setAdapter(myPagerAdapter);
        // TabLayout 指示器 (记得自己手动创建4个Fragment,注意是 app包下的Fragment 还是 V4包下的 Fragment)
        tabLayout.addTab(tabLayout.newTab());
        tabLayout.addTab(tabLayout.newTab());
        tabLayout.addTab(tabLayout.newTab());
        tabLayout.addTab(tabLayout.newTab());
        tabLayout.addTab(tabLayout.newTab());
        // 使用 TabLayout 和 ViewPager 相关联
        tabLayout.setupWithViewPager(viewPager);
        // TabLayout指示器添加文本
        tabLayout.getTabAt(0).setText("全部");
        tabLayout.getTabAt(1).setText("待支付");
        tabLayout.getTabAt(2).setText("待收货");
        tabLayout.getTabAt(3).setText("待评价");
        tabLayout.getTabAt(4).setText("已完成");


        Intent intent = getIntent();
        String stringExtra = intent.getStringExtra("daizhifu");
        if(stringExtra != null){
            int i = Integer.parseInt(stringExtra);
            viewPager.setCurrentItem(i);

            if (i==0){
                dingDanQuanBuFragment.initDingDanLieBiaoHttp();
            }else if (i==1){
                daiZhiFuFragment.initDaiZhiFuHttp();
            }else if (i==2){
                daiShouHuoFragment.initDaiShouHuoHttp();
            }else if (i==3){
                daiPingJiaFragment.initDaiPingJiaHttp();
            }else if (i==4){
                finishFragment.initFinishHttp();
        }





            viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
                @Override
                public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

                }

                @Override
                public void onPageSelected(int position) {

                    if (position==0){
                        dingDanQuanBuFragment.initDingDanLieBiaoHttp();
                    }else if (position==1){
                        daiZhiFuFragment.initDaiZhiFuHttp();
                    }else if (position==2){
                        daiShouHuoFragment.initDaiShouHuoHttp();
                    }else if (position==3){
                        daiPingJiaFragment.initDaiPingJiaHttp();
                    }else if (position==4){
                        finishFragment.initFinishHttp();
                    }


                }

                @Override
                public void onPageScrollStateChanged(int state) {

                }
            });

        }


    }

    @OnClick({R.id.image_back})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.image_back:
                finish();
                break;

        }
    }


}
