package com.wanyangproject.my;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;

import com.google.gson.Gson;
import com.wanyangproject.R;
import com.wanyangproject.entity.QiYeZhaoPinGuanLiEntity;
import com.wanyangproject.myadapter.ZhaoPinAdapter;
import com.wanyangproject.utils.ContractUtils;
import com.zhy.http.okhttp.OkHttpUtils;
import com.zhy.http.okhttp.callback.StringCallback;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.Call;

public class ZhaoPinGuanLiActivity extends AppCompatActivity {

    @BindView(R.id.image_back)
    ImageView imageBack;
    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    private ZhaoPinAdapter zhaoPinAdapter;
    private QiYeZhaoPinGuanLiEntity qiYeZhaoPinGuanLiEntity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_zhao_pin_guan_li);
        ButterKnife.bind(this);
        initView();


//        企业招聘管理的网络请求
        initQiYeZhaoPinGuanLiHttp();


    }


//    企业招聘管理的网络请求
    private void initQiYeZhaoPinGuanLiHttp() {
        OkHttpUtils.post()
                .url(ContractUtils.LOGIN_URL+"Enterprise/recruitlist")
                .addHeader("token",ContractUtils.getTOKEN(ZhaoPinGuanLiActivity.this))
                .addParams("parkId",ContractUtils.getParkId(ZhaoPinGuanLiActivity.this))
                .build()
                .execute(new StringCallback() {
                    @Override
                    public void onError(Call call, Exception e, int id) {

                    }

                    @Override
                    public void onResponse(String response, int id) {
                        System.out.println(response+"           企业招聘管理的网络请求");
                        ContractUtils.Code500(ZhaoPinGuanLiActivity.this,response);
                        if(response.indexOf("200") != -1){
                            Gson gson = new Gson();
                            qiYeZhaoPinGuanLiEntity = gson.fromJson(response, QiYeZhaoPinGuanLiEntity.class);
                            zhaoPinAdapter = new ZhaoPinAdapter(ZhaoPinGuanLiActivity.this,qiYeZhaoPinGuanLiEntity.getResponse());
                            LinearLayoutManager manager = new LinearLayoutManager(ZhaoPinGuanLiActivity.this);
                            recyclerView.setLayoutManager(manager);
                            recyclerView.setAdapter(zhaoPinAdapter);

                            zhaoPinAdapter.setZhaoPinClick(new ZhaoPinAdapter.ZhaoPinClick() {
                                @Override
                                public void zhaopinClick(int position) {
                                    Intent intent = new Intent(ZhaoPinGuanLiActivity.this,ZhaoPinXiangQingActivity.class);
                                    intent.putExtra("zhaopinid",qiYeZhaoPinGuanLiEntity.getResponse().get(position).getId());
                                    startActivity(intent);
                                }
                            });
                        }
                    }
                });
    }



    private void initView() {
//        zhaoPinAdapter = new ZhaoPinAdapter(this);
//        LinearLayoutManager manager = new LinearLayoutManager(this);
//        recyclerView.setLayoutManager(manager);
//        recyclerView.setAdapter(zhaoPinAdapter);
//
//        zhaoPinAdapter.setZhaoPinClick(new ZhaoPinAdapter.ZhaoPinClick() {
//            @Override
//            public void zhaopinClick(int position) {
//                Intent intent = new Intent(ZhaoPinGuanLiActivity.this,ZhaoPinXiangQingActivity.class);
//                startActivity(intent);
//            }
//        });

    }

    @OnClick({R.id.image_back, R.id.recyclerView})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.image_back:
                finish();
                break;
            case R.id.recyclerView:
                break;
        }
    }
}
