package com.wanyangproject.my;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.google.gson.Gson;
import com.wanyangproject.R;
import com.wanyangproject.entity.QiYeXiangQingEntity;
import com.wanyangproject.utils.ContractUtils;
import com.zhy.http.okhttp.OkHttpUtils;
import com.zhy.http.okhttp.callback.StringCallback;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.Call;

public class WuYeQiYeGuanLiXiangQingActivity extends AppCompatActivity {

    @BindView(R.id.image_back)
    ImageView imageBack;
    @BindView(R.id.image_touxiang)
    ImageView imageTouxiang;
    @BindView(R.id.tv_shop_name)
    TextView tvShopName;
    @BindView(R.id.tv_faren)
    TextView tvFaren;
    @BindView(R.id.tv_yuanqu)
    TextView tvYuanqu;
    @BindView(R.id.tv_phone)
    TextView tvPhone;
    private String id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wu_ye_qi_ye_guan_li_xiang_qing);
        ButterKnife.bind(this);


        Intent intent = getIntent();
        id = intent.getStringExtra("id");
        System.out.println(id+"    物业企业接受id");


//        物业企业详情的网络请求
        initWuYeQiYeHttp();


    }



//    物业企业详情的网络请求
    private void initWuYeQiYeHttp() {
        OkHttpUtils.post()
                .url(ContractUtils.LOGIN_URL+"company/getDetail")
                .addHeader("token",ContractUtils.getTOKEN(WuYeQiYeGuanLiXiangQingActivity.this))
                .addParams("parkId",ContractUtils.getParkId(WuYeQiYeGuanLiXiangQingActivity.this))
                .addParams("enterId",id)
                .build()
                .execute(new StringCallback() {
                    @Override
                    public void onError(Call call, Exception e, int id) {

                    }

                    @Override
                    public void onResponse(String response, int id) {
                        System.out.println(response+"       物业企业详情的网络请求");
                        ContractUtils.Code500(WuYeQiYeGuanLiXiangQingActivity.this,response);
                        if(response.indexOf("200") != -1){
                            Gson gson = new Gson();
                            QiYeXiangQingEntity qiYeXiangQingEntity = gson.fromJson(response, QiYeXiangQingEntity.class);
                            if(qiYeXiangQingEntity.getResponse().getInfo().getLogoUrl() == null){

                            }else{
                                Glide.with(WuYeQiYeGuanLiXiangQingActivity.this).load(qiYeXiangQingEntity.getResponse().getInfo().getLogoUrl()).into(imageTouxiang);
                            }



                            if(qiYeXiangQingEntity.getResponse().getInfo().getEnterName() == null){

                            }else{
                                tvShopName.setText(qiYeXiangQingEntity.getResponse().getInfo().getEnterName());
                            }



                            if(qiYeXiangQingEntity.getResponse().getInfo().getLegalPerson() == null){

                            }else{
                                tvFaren.setText("法人名称："+qiYeXiangQingEntity.getResponse().getInfo().getLegalPerson());
                            }



                            if(qiYeXiangQingEntity.getResponse().getInfo().getParkName() == null){

                            }else{
                                tvYuanqu.setText("园区名称："+qiYeXiangQingEntity.getResponse().getInfo().getParkName());
                            }



                            if(qiYeXiangQingEntity.getResponse().getInfo().getContact() == null){

                            }else{
                                tvPhone.setText("联系电话："+qiYeXiangQingEntity.getResponse().getInfo().getContact());
                            }
                        }

                    }
                });
    }

    @OnClick({R.id.image_back, R.id.image_touxiang, R.id.tv_shop_name, R.id.tv_faren, R.id.tv_yuanqu, R.id.tv_phone})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.image_back:
                finish();
                break;
            case R.id.image_touxiang:
                break;
            case R.id.tv_shop_name:
                break;
            case R.id.tv_faren:
                break;
            case R.id.tv_yuanqu:
                break;
            case R.id.tv_phone:
                break;
        }
    }
}
