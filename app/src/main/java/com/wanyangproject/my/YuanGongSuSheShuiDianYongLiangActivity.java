package com.wanyangproject.my;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;

import com.androidkun.xtablayout.XTabLayout;
import com.wanyangproject.R;
import com.wanyangproject.adapter.MyPagerAdapter;
import com.wanyangproject.mysuipian.YuanGongLengShuiFragment;
import com.wanyangproject.mysuipian.YuanGongReShuiFragment;
import com.wanyangproject.mysuipian.YuanGongYongDianFragment;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class YuanGongSuSheShuiDianYongLiangActivity extends AppCompatActivity {

    @BindView(R.id.image_back)
    ImageView imageBack;
    //    @BindView(R.id.tabLayout)
//    TabLayout tabLayout;
    @BindView(R.id.viewPager)
    ViewPager viewPager;
    @BindView(R.id.tabLayout)
    XTabLayout tabLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_yuan_gong_su_she_shui_dian_yong_liang);
        ButterKnife.bind(this);
        initPager();
    }

    private void initPager() {
        List<Fragment> fragments = new ArrayList<>();
        fragments.add(new YuanGongLengShuiFragment());
        fragments.add(new YuanGongReShuiFragment());
        fragments.add(new YuanGongYongDianFragment());

        // 创建ViewPager适配器
        MyPagerAdapter myPagerAdapter = new MyPagerAdapter(getSupportFragmentManager());
        myPagerAdapter.setFragments(fragments);
        // 给ViewPager设置适配器
        viewPager.setAdapter(myPagerAdapter);
        // TabLayout 指示器 (记得自己手动创建4个Fragment,注意是 app包下的Fragment 还是 V4包下的 Fragment)
        tabLayout.addTab(tabLayout.newTab());
        tabLayout.addTab(tabLayout.newTab());
        tabLayout.addTab(tabLayout.newTab());

        // 使用 TabLayout 和 ViewPager 相关联
        tabLayout.setupWithViewPager(viewPager);
        // TabLayout指示器添加文本
        tabLayout.getTabAt(0).setText("冷水记录");
        tabLayout.getTabAt(1).setText("热水记录");
        tabLayout.getTabAt(2).setText("用电记录");
    }

    @OnClick({R.id.image_back, R.id.tabLayout, R.id.viewPager})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.image_back:
                finish();
                break;
            case R.id.tabLayout:
                break;
            case R.id.viewPager:
                break;
        }
    }
}
