package com.wanyangproject.my;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.wanyangproject.R;
import com.wanyangproject.adapter.TuiKuanYuanYinShangMianAdapter;
import com.wanyangproject.entity.ChuanZhiEntity;
import com.wanyangproject.entity.DingDanXiangQingEntity;
import com.wanyangproject.entity.TuiKuanShiBaiEntity;
import com.wanyangproject.entity.TuiKuanYuanYinEntity;
import com.wanyangproject.myadapter.TuiKuanAdapter;
import com.wanyangproject.popuwindow.PeiSongPopupWindow;
import com.wanyangproject.popuwindow.TuiKuanPopupWindow;
import com.wanyangproject.utils.ContractUtils;
import com.zhy.http.okhttp.OkHttpUtils;
import com.zhy.http.okhttp.callback.StringCallback;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.Call;

public class TuiKuanActivity extends AppCompatActivity {

    @BindView(R.id.image_back)
    ImageView imageBack;
    @BindView(R.id.tv_tijiao)
    TextView tvTijiao;
    @BindView(R.id.recyclerVew)
    RecyclerView recyclerVew;
    @BindView(R.id.tv_xuanze)
    TextView tvXuanze;
    @BindView(R.id.tv_tuikuan_jine)
    TextView tvTuikuanJine;
    @BindView(R.id.et_zhifubao_zhanghao)
    EditText etZhifubaoZhanghao;
    @BindView(R.id.et_zhifubao_mingcheng)
    EditText etZhifubaoMingcheng;
    private TuiKuanAdapter tuiKuanAdapter;
    private TuiKuanPopupWindow tuiKuanPopupWindow;
    private String dingdanhao;
//    private NetWork netWork;
//    private String tuikuanyuanyin;
    private ChuanZhiEntity chuanZhiEntity;
    //    private DaiShouHuoEntity.ResponseBean responseBean;
//    private DingDanXiangQingEntity.ResponseBean responseBean1;
    private DingDanXiangQingEntity dingDanXiangQingEntity;

//    private ArrayList<String> listtitle = new ArrayList<>();



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tui_kuan);
        ButterKnife.bind(this);

//        listtitle.add("质量问题");
//        listtitle.add("假冒品牌");
//        listtitle.add("未按约定时间发货");
//        listtitle.add("颜色/尺寸/参数不符");


//        IntentFilter intentFilter = new IntentFilter();
//        intentFilter.addAction("tuikuan");
//        netWork = new NetWork();
//        registerReceiver(netWork, intentFilter);

//        responseBean = (DaiShouHuoEntity.ResponseBean)ChuanZhiEntity.obj;
//        responseBean1 = (DingDanXiangQingEntity.ResponseBean)ChuanZhiEntity.obj;
////


//
        Intent intent = getIntent();
        dingdanhao = intent.getStringExtra("dingdanhao");
        System.out.println(dingdanhao + "      退款界面接收的订单号");

        //        订单详情的网络请求
        initXiangQIngHttp();


//        获取支付宝账号和名称的网络请求
        initHuoQuZhangHaoHttp();


        initView();
    }



//    获取支付宝账号和名称的网络请求
    private void initHuoQuZhangHaoHttp() {
        OkHttpUtils.post()
                .url(ContractUtils.LOGIN_URL+"user/shangtui")
                .addHeader("token",ContractUtils.getTOKEN(TuiKuanActivity.this))
                .build()
                .execute(new StringCallback() {
                    @Override
                    public void onError(Call call, Exception e, int id) {

                    }

                    @Override
                    public void onResponse(String response, int id) {
                        System.out.println(response+"          获取支付宝账号和名称的网络请求");
                        ContractUtils.Code500(TuiKuanActivity.this,response);
//                        if(response.indexOf("200") != -1){
//                            Gson gson = new Gson();
//                            ZhiFuBaoZhangHaoEntity zhiFuBaoZhangHaoEntity = gson.fromJson(response, ZhiFuBaoZhangHaoEntity.class);
//                            if(zhiFuBaoZhangHaoEntity.getResponse().getRefund() == null){
//
//                            }else{
//                                etZhifubaoMingcheng.setText(zhiFuBaoZhangHaoEntity.getResponse().getRefund());
//                            }
//
//
//                            if(zhiFuBaoZhangHaoEntity.getResponse().getAccount() == null){
//
//                            }else{
//                                etZhifubaoZhanghao.setText(zhiFuBaoZhangHaoEntity.getResponse().getAccount());
//                            }


//                        }
                    }
                });
    }


    //    订单详情的网络请求
    private void initXiangQIngHttp() {
        OkHttpUtils.post()
                .url(ContractUtils.LOGIN_URL + "user/orderdetails")
                .addHeader("token", ContractUtils.getTOKEN(TuiKuanActivity.this))
                .addParams("parkId", ContractUtils.getParkId(TuiKuanActivity.this))
                .addParams("order_sn", dingdanhao)
                .build()
                .execute(new StringCallback() {
                    @Override
                    public void onError(Call call, Exception e, int id) {

                    }

                    @Override
                    public void onResponse(String response, int id) {
                        ContractUtils.Code500shibai(TuiKuanActivity.this, response);
                        System.out.println(response + "        订单详情的网络请求");

                        if (response.indexOf("200") != -1) {
                            Gson gson = new Gson();
                            dingDanXiangQingEntity = gson.fromJson(response, DingDanXiangQingEntity.class);

                            if(dingDanXiangQingEntity.getResponse().getMoney() == null){

                            }else{
                                tvTuikuanJine.setText("退款金额：¥ "+dingDanXiangQingEntity.getResponse().getMoney());
                            }

                            TuiKuanYuanYinShangMianAdapter tuiKuanYuanYinShangMianAdapter = new TuiKuanYuanYinShangMianAdapter(TuiKuanActivity.this, dingDanXiangQingEntity.getResponse());
                            LinearLayoutManager manager = new LinearLayoutManager(TuiKuanActivity.this);
                            recyclerVew.setLayoutManager(manager);
                            recyclerVew.setAdapter(tuiKuanYuanYinShangMianAdapter);
//                            tuiKuanAdapter = new TuiKuanAdapter(TuiKuanActivity.this,dingDanXiangQingEntity.getResponse());
//                            LinearLayoutManager manager = new LinearLayoutManager(TuiKuanActivity.this);
//                            recyclerVew.setLayoutManager(manager);
//                            recyclerVew.setAdapter(tuiKuanAdapter);
                        }
                    }
                });
    }



    @OnClick({R.id.image_back, R.id.tv_tijiao, R.id.recyclerVew,R.id.tv_xuanze, R.id.tv_tuikuan_jine, R.id.et_zhifubao_zhanghao, R.id.et_zhifubao_mingcheng})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.image_back:
                finish();
                break;
            case R.id.tv_tijiao:
                if (tvXuanze.getText().toString().trim().equals("")) {
                    Toast.makeText(this, "请选择退款原因", Toast.LENGTH_SHORT).show();
                }else if(etZhifubaoZhanghao.getText().toString().trim().equals("")){
                    Toast.makeText(this, "请输入支付宝账号", Toast.LENGTH_SHORT).show();
                }else if(etZhifubaoMingcheng.getText().toString().trim().equals("")){
                    Toast.makeText(this, "请输入支付宝名称", Toast.LENGTH_SHORT).show();
                } else {
                    //        用户退款的网络请求
                    initYongHuTuiKuanHttp();
                }
                break;
            case R.id.recyclerVew:
                break;
            case R.id.tv_xuanze:
                Object itemsOnClick = null;
                tuiKuanPopupWindow = new TuiKuanPopupWindow(TuiKuanActivity.this, (View.OnClickListener) itemsOnClick);
                tuiKuanPopupWindow.showAtLocation(findViewById(R.id.relative_tuikuan), Gravity.BOTTOM | Gravity.CENTER_HORIZONTAL, 0, 0);
//                tuiKuanPopupWindow.showAsDropDown(findViewById(R.id.relative_tuikuan), 0, 0, 0);

                tuiKuanPopupWindow.setTuiKuanClick(new TuiKuanPopupWindow.TuiKuanClick() {
                    @Override
                    public void tuikuanClick(String title) {
                        tvXuanze.setText(title);
                    }
                });
                break;
            case R.id.tv_shenqing_leixing:
                itemsOnClick = null;
                tuiKuanPopupWindow = new TuiKuanPopupWindow(TuiKuanActivity.this, (View.OnClickListener) itemsOnClick);
                tuiKuanPopupWindow.showAtLocation(findViewById(R.id.relative_tuikuan), Gravity.BOTTOM | Gravity.CENTER_HORIZONTAL, 0, 0);
//                tuiKuanPopupWindow.showAsDropDown(findViewById(R.id.relative_tuikuan), 0, 0, 0);

                tuiKuanPopupWindow.setTuiKuanClick(new TuiKuanPopupWindow.TuiKuanClick() {
                    @Override
                    public void tuikuanClick(String title) {
                        tvXuanze.setText(title);
                    }
                });

//                tuiKuanPopupWindow.setTuiKuanClick(new PeiSongPopupWindow.PeiSongClick() {
//                    @Override
//                    public void peisongClick(String title) {
//                        tvPeisongFangshi.setText(title);
//                    }
//                });




//                OptionsPickerView pvOptions = new  OptionsPickerView.Builder(this, new OptionsPickerView.OnOptionsSelectListener() {
//                    @Override
//                    public void onOptionsSelect(int options1, int option2, int options3 ,View v) {
//                        //返回的分别是三个级别的选中位置
//                        String tx = listtitle.get(options1);
//                        tvXuanze.setText(tx);
//                    }
//                })
//                        .setSubmitText("确定")//确定按钮文字
//                        .setCancelText("取消")//取消按钮文字
//                        .setTitleText("退款原因")//标题
//                        .setSubCalSize(14)//确定和取消文字大小
//                        .setTitleSize(17)//标题文字大小
//                        .setTitleColor(Color.BLACK)//标题文字颜色
//                        .setSubmitColor(Color.RED)//确定按钮文字颜色
//                        .setLineSpacingMultiplier(3.0f)//设置两横线之间的间隔倍数
//                        .setCancelColor(Color.parseColor("#AEAAAA"))//取消按钮文字颜色
//                        .setTitleBgColor(Color.parseColor("#fafafa"))//标题背景颜色 Night mode
//                        .setBgColor(Color.parseColor("#ffffff"))//滚轮背景颜色 Night mode
//                        .setContentTextSize(14)//滚轮文字大小
//                        .setLinkage(false)//设置是否联动，默认true
//                        .setLabels("", "", "")//设置选择的三级单位
//                        .isCenterLabel(false) //是否只显示中间选中项的label文字，false则每项item全部都带有label。
//                        .setCyclic(false, false, false)//循环与否
//                        .setSelectOptions(1, 1, 1)  //设置默认选中项
//                        .setOutSideCancelable(false)//点击外部dismiss default true
//                        .isDialog(false)//是否显示为对话框样式
//                        .build();
//
//                pvOptions.setPicker(listtitle);//添加数据源
//                pvOptions.show();

                break;
            case R.id.tv_tuikuan_jine:
                break;
            case R.id.et_zhifubao_zhanghao:
                break;
            case R.id.et_zhifubao_mingcheng:
                break;
        }
    }
//
//    class NetWork extends BroadcastReceiver {
//        @Override
//        public void onReceive(Context context, Intent intent) {
//            tuikuanyuanyin = intent.getStringExtra("tuikuanyuanyin");
//            System.out.println(tuikuanyuanyin + "       退款原因");
//        }
//    }


    //    用户退款的网络请求
    private void initYongHuTuiKuanHttp() {
        System.out.println(dingdanhao + "        订单号 ");
        System.out.println(tvXuanze.getText().toString().trim() + "        退款原因 ");
        OkHttpUtils.post()
                .url(ContractUtils.LOGIN_URL + "user/refund")
                .addHeader("token", ContractUtils.getTOKEN(TuiKuanActivity.this))
                .addParams("parkId", ContractUtils.getParkId(TuiKuanActivity.this))
                .addParams("order_sn", dingdanhao)
                .addParams("Reason", tvXuanze.getText().toString().trim())
                .addParams("account",etZhifubaoZhanghao.getText().toString().trim())
                .addParams("refund",etZhifubaoMingcheng.getText().toString().trim())
                .build()
                .execute(new StringCallback() {
                    @Override
                    public void onError(Call call, Exception e, int id) {
                        System.out.println(e + "        退款的网络请求eeeeee");

                    }

                    @Override
                    public void onResponse(String response, int id) {
                        ContractUtils.Code500shibai(TuiKuanActivity.this, response);
                        System.out.println(response + "         用户退款的网络请求");

                        if (response.indexOf("200") != -1) {
                            Gson gson = new Gson();
                            TuiKuanYuanYinEntity tuiKuanYuanYinEntity = gson.fromJson(response, TuiKuanYuanYinEntity.class);
                            Toast.makeText(TuiKuanActivity.this, "已提交！", Toast.LENGTH_SHORT).show();
                            Intent intent = new Intent();
                            intent.putExtra("tuikuan", "tuikuan");
                            intent.setAction("tuikuan");
                            sendBroadcast(intent);
                            finish();
//                            跳转退款详情页面
//                            Intent intent = new Intent(TuiKuanActivity.this, TuiKuanXiangQingActivity.class);
//                            intent.putExtra("tuikuanyuanyin",tvXuanze.getText().toString().trim());
//                            System.out.println(tvXuanze.getText().toString().trim() + "     传送的退款原因");
//                            startActivity(intent);
                        } else if (response.indexOf("400") != -1) {
                            Gson gson = new Gson();
                            TuiKuanShiBaiEntity tuiKuanShiBaiEntity = gson.fromJson(response, TuiKuanShiBaiEntity.class);
                            Toast.makeText(TuiKuanActivity.this, tuiKuanShiBaiEntity.getMsg(), Toast.LENGTH_SHORT).show();
                            finish();
                        }
                    }
                });
    }

    private void initView() {
//        tuiKuanAdapter = new TuiKuanAdapter(TuiKuanActivity.this,responseBean);
//        LinearLayoutManager manager = new LinearLayoutManager(TuiKuanActivity.this);
//        recyclerVew.setLayoutManager(manager);
//        recyclerVew.setAdapter(tuiKuanAdapter);

//        tuiKuanAdapter.setTuiKuanClick(new TuiKuanAdapter.TuiKuanClick() {
//            @Override
//            public void tuikuanClick(int position) {
//                Object itemsOnClick = null;
//                tuiKuanPopupWindow = new TuiKuanPopupWindow(TuiKuanActivity.this, (View.OnClickListener) itemsOnClick);
//                tuiKuanPopupWindow.showAtLocation(findViewById(R.id.relative_tuikuan), Gravity.BOTTOM | Gravity.CENTER_HORIZONTAL, 0, 0);
//
//                tuiKuanPopupWindow.setTuiKuanClick(new TuiKuanPopupWindow.TuiKuanClick() {
//                    @Override
//                    public void tuikuanClick(String number) {
//
//                    }
//                });
//            }
//        });
    }


}
