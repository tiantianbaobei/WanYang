package com.wanyangproject.my;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;

import com.google.gson.Gson;
import com.wanyangproject.R;
import com.wanyangproject.entity.ShangJiaYouHuiQuanEntity;
import com.wanyangproject.entity.UserEntity;
import com.wanyangproject.myadapter.YouHuiQuanAdapter;
import com.wanyangproject.utils.ContractUtils;
import com.zhy.http.okhttp.OkHttpUtils;
import com.zhy.http.okhttp.callback.StringCallback;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.Call;

public class ShangJiaYouHuiQuanActivity extends AppCompatActivity {

    @BindView(R.id.image_back)
    ImageView imageBack;
    @BindView(R.id.recyclerVew)
    RecyclerView recyclerVew;
    private YouHuiQuanAdapter youHuiQuanAdapter;
    private UserEntity userEntity;
    private String phone;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shang_jia_you_hui_quan);
        ButterKnife.bind(this);

        //        获取用户信息的网络请求
        initYongHuXinXiHttp();

        initView();
    }

    //    获取用户信息的网络请求
    private void initYongHuXinXiHttp() {
        OkHttpUtils.post()
                .url(ContractUtils.LOGIN_URL + "Life/user")
                    .addHeader("token", ContractUtils.getTOKEN(ShangJiaYouHuiQuanActivity.this))
                .build()
                .execute(new StringCallback() {
                    @Override
                    public void onError(Call call, Exception e, int id) {
                        System.out.println(e + "       eeee用户信息");
                    }

                    @Override
                    public void onResponse(String response, int id) {
                        ContractUtils.Code500(ShangJiaYouHuiQuanActivity.this,response);
                        System.out.println(response + "    获取个人用户信息的网络请求");

                        if (response.indexOf("200") != -1) {
                            Gson gson = new Gson();
                            userEntity = gson.fromJson(response, UserEntity.class);
                            phone = userEntity.getResponse().getUsername();//获取手机号
                            System.out.println(phone + "     用户的手机号");
                            //        商家商家优惠券列表的网络请求
                            initShangJiaYouHuiQuanHttp();
                        }
                    }
                });
    }



//    商家优惠券列表的网络请求
    private void initShangJiaYouHuiQuanHttp() {
        OkHttpUtils.post()
                .url(ContractUtils.LOGIN_URL+"Shop/couponslist")
                .addHeader("token",ContractUtils.getTOKEN(ShangJiaYouHuiQuanActivity.this))
                .addParams("phone",ContractUtils.getPhone(ShangJiaYouHuiQuanActivity.this)) // 暂时  tiantian
                .addParams("parkId",ContractUtils.getParkId(ShangJiaYouHuiQuanActivity.this))
                .build()
                .execute(new StringCallback() {
                    @Override
                    public void onError(Call call, Exception e, int id) {

                    }

                    @Override
                    public void onResponse(String response, int id) {
                        ContractUtils.Code500(ShangJiaYouHuiQuanActivity.this,response);
                        System.out.println(response+"       商家优惠券列表的网络请求");

                        if(response.indexOf("200") != -1){
                            Gson gson = new Gson();
                            ShangJiaYouHuiQuanEntity shangJiaYouHuiQuanEntity = gson.fromJson(response, ShangJiaYouHuiQuanEntity.class);
                            youHuiQuanAdapter = new YouHuiQuanAdapter(ShangJiaYouHuiQuanActivity.this,shangJiaYouHuiQuanEntity.getResponse());
                            LinearLayoutManager manager = new LinearLayoutManager(ShangJiaYouHuiQuanActivity.this);
                            recyclerVew.setLayoutManager(manager);
                            recyclerVew.setAdapter(youHuiQuanAdapter);
                        }
                    }
                });
    }

    private void initView() {
//        youHuiQuanAdapter = new YouHuiQuanAdapter(this);
//        LinearLayoutManager manager = new LinearLayoutManager(this);
//        recyclerVew.setLayoutManager(manager);
//        recyclerVew.setAdapter(youHuiQuanAdapter);
    }

    @OnClick({R.id.image_back, R.id.recyclerVew})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.image_back:
                finish();
                break;
            case R.id.recyclerVew:
                break;
        }
    }
}
