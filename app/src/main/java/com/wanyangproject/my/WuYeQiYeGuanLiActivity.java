package com.wanyangproject.my;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.wanyangproject.R;
import com.wanyangproject.adapter.YuanQuQiYeAdapter;
import com.wanyangproject.myadapter.WuYeQiYeGuanLiAdapter;
import com.wanyangproject.shouye.QiYeXiangQingActivity;
import com.wanyangproject.shouye.YuanQuQiYeActivity;
import com.wanyangproject.utils.ContractUtils;
import com.zhy.http.okhttp.OkHttpUtils;
import com.zhy.http.okhttp.callback.StringCallback;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Iterator;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.Call;

public class WuYeQiYeGuanLiActivity extends AppCompatActivity {

    @BindView(R.id.tv_qiye_guanli)
    TextView tvQiyeGuanli;
    @BindView(R.id.image_sousuo)
    ImageView imageSousuo;
    @BindView(R.id.image_back)
    ImageView imageBack;
    @BindView(R.id.et)
    EditText et;
    @BindView(R.id.relative_sousuo)
    RelativeLayout relativeSousuo;
    @BindView(R.id.relative_title)
    RelativeLayout relativeTitle;
    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    private WuYeQiYeGuanLiAdapter wuYeQiYeGuanLiAdapter;
    private ArrayList<JSONObject> jsonArrayFenZu = new ArrayList<>();// 分组的数据
    private int page = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wu_ye_qi_ye_guan_li);
        ButterKnife.bind(this);
        initView();


//        企业管理的网络请求
        initQiYeGuanLiHttp();







        et.addTextChangedListener(new TextWatcher() {
            @Override  //   输入文字之前的状态
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override // 输入文字中的状态
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override  // 输入文字之后的状态
            public void afterTextChanged(Editable editable) {
                page = 0;

//        企业管理的网络请求
                initQiYeGuanLiHttp();


            }
        });



    }


//    企业管理的网络请求
    private void initQiYeGuanLiHttp() {
        OkHttpUtils.post()
                .url(ContractUtils.LOGIN_URL+"company")
                .addHeader("token",ContractUtils.getTOKEN(WuYeQiYeGuanLiActivity.this))
                .addParams("parkId",ContractUtils.getParkId(WuYeQiYeGuanLiActivity.this))
                .addParams("pageIndex","0")
                .addParams("enterName",et.getText().toString().trim())
                .build()
                .execute(new StringCallback() {
                    @Override
                    public void onError(Call call, Exception e, int id) {

                    }

                    @Override
                    public void onResponse(String response, int id) {
                        System.out.println(response+"           物业企业管理的网络请求");
                        ContractUtils.Code500(WuYeQiYeGuanLiActivity.this,response);
//                        if (page == 0) {
//                            jsonArrayFenZu.clear();
//                        }
                        int aa = jsonArrayFenZu.size();

                        System.out.println(response + "    首页园区企业的网络请求");
                        try {
//                            tiantiantian
                            JSONObject jsonObject = new JSONObject(response);
                            JSONObject jsonObject1 = jsonObject.getJSONObject("response");
                            JSONObject jsonObject2 = jsonObject1.getJSONObject("list");
                            Iterator<String> keys = jsonObject2.keys();

                            while (keys.hasNext()) {
                                String key = (String) keys.next();
                                JSONArray jsonArray = jsonObject2.getJSONArray(key);
                                for (int i = 0; i < jsonArray.length(); i++) {
                                    JSONObject jj = (JSONObject) jsonArray.get(i);
                                    jsonArrayFenZu.add(jj);
                                }

                                int bb = jsonArrayFenZu.size();
                                if (aa == bb) {
                                    Toast.makeText(WuYeQiYeGuanLiActivity.this, "暂无数据", Toast.LENGTH_SHORT).show();
                                }
//                                if(suoyin.contains(key)){
//
//                                }else{
//                                    suoyin.add(key);
//                                }
//
//                                suoyin.add(key);

//                                String value = jsonObject.getString(key);
//                                Log.e("TAG", "key=" + key + "\tvalue=" + value);//键值对key value
                                System.out.println(key + "    keykey");
                            }

                            wuYeQiYeGuanLiAdapter = new WuYeQiYeGuanLiAdapter(WuYeQiYeGuanLiActivity.this,jsonArrayFenZu);
                            LinearLayoutManager manager = new LinearLayoutManager(WuYeQiYeGuanLiActivity.this);
                            recyclerView.setLayoutManager(manager);
                            recyclerView.setAdapter(wuYeQiYeGuanLiAdapter);

//                            yuanQuQiYeAdapter = new YuanQuQiYeAdapter(WuYeQiYeGuanLiActivity.this, jsonArrayFenZu);
//                            LinearLayoutManager manager = new LinearLayoutManager(WuYeQiYeGuanLiActivity.this);
//                            recyclerView.setLayoutManager(manager);
//                            recyclerView.setAdapter(yuanQuQiYeAdapter);

//                            索引
//                            int pos = 0;
//                            if(suoyin.contains(zimu)){
//                                int a = suoyin.indexOf(zimu);
//                                int sum = 0;// 之和
//                                for (int i = 0; i < a; i++) {
//                                    JSONArray jsonArray = jsonObject2.getJSONArray(suoyin.get(i));
//                                    sum += jsonArray.length();
//                                }
//                                pos = sum;
//                            }
//                            System.out.println(pos+"    pos");
//                            recyclerView.setLayoutManager(manager);
//                            manager.scrollToPosition(pos); // 自动滚动到哪一行
////                            manager.smoothScrollToPosition(this,,pos);



                            wuYeQiYeGuanLiAdapter.setQiYeGuanLiClick(new WuYeQiYeGuanLiAdapter.QiYGuanLiClick() {
                                @Override
                                public void qiyeguanliclick(int position,String id) {
                                    Intent intent = new Intent(WuYeQiYeGuanLiActivity.this,WuYeQiYeGuanLiXiangQingActivity.class);
                                    intent.putExtra("id",id);
                                    startActivity(intent);
                                }
                            });



                        } catch (JSONException e) {
                            e.printStackTrace();
                        }







                    }
                });
    }

    private void initView() {
//        wuYeQiYeGuanLiAdapter = new WuYeQiYeGuanLiAdapter(this);
//        LinearLayoutManager manager = new LinearLayoutManager(this);
//        recyclerView.setLayoutManager(manager);
//        recyclerView.setAdapter(wuYeQiYeGuanLiAdapter);

//        wuYeQiYeGuanLiAdapter.setQiYeGuanLiClick(new WuYeQiYeGuanLiAdapter.QiYGuanLiClick() {
//            @Override
//            public void qiyeguanliclick(int position) {
//                Intent intent = new Intent(WuYeQiYeGuanLiActivity.this,WuYeQiYeGuanLiXiangQingActivity.class);
//                startActivity(intent);
//            }
//        });

    }

    @OnClick({R.id.tv_qiye_guanli, R.id.image_sousuo, R.id.image_back, R.id.et, R.id.relative_sousuo, R.id.relative_title, R.id.recyclerView})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.tv_qiye_guanli:
                break;
            case R.id.image_sousuo:
                tvQiyeGuanli.setVisibility(View.INVISIBLE);
                relativeSousuo.setVisibility(View.VISIBLE);
                imageSousuo.setVisibility(View.INVISIBLE);
                break;
            case R.id.image_back:
                finish();
                break;
            case R.id.et:
                break;
            case R.id.relative_sousuo:
                break;
            case R.id.relative_title:
                break;
            case R.id.recyclerView:
                break;
        }
    }
}
