package com.wanyangproject.my;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.wanyangproject.R;
import com.wanyangproject.entity.FangKeGuanLiEntity;
import com.wanyangproject.entity.FangKeGuanLiShaChuShiBaiEntity;
import com.wanyangproject.entity.FangKeGuanLiShanChuEntity;
import com.wanyangproject.fuwuactivity.TianJiaFangKeActivity;
import com.wanyangproject.myadapter.FangKeGuanLiAdapter;
import com.wanyangproject.utils.ContractUtils;
import com.zhy.http.okhttp.OkHttpUtils;
import com.zhy.http.okhttp.callback.StringCallback;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.Call;

public class MyFangKeGuanLiActivity extends AppCompatActivity {

    @BindView(R.id.image_back)
    ImageView imageBack;
    @BindView(R.id.tv_tianjia_fangke)
    TextView tvTianjiaFangke;
    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    private FangKeGuanLiAdapter fangKeGuanLiAdapter;
    private FangKeGuanLiEntity fangKeGuanLiEntity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_fang_ke_guan_li);
        ButterKnife.bind(this);
        initView();


//        访客管理的网络请求
        initFangKeGuanLiHttp();
    }


    @Override
    protected void onResume() {
        super.onResume();
        //        访客管理的网络请求
        initFangKeGuanLiHttp();
    }




    //    访客管理的网络请求
    private void initFangKeGuanLiHttpTwo() {
        OkHttpUtils.post()
                .url(ContractUtils.LOGIN_URL+"fuwu/fangke")
                .addHeader("token",ContractUtils.getTOKEN(MyFangKeGuanLiActivity.this))
                .addParams("parkId",ContractUtils.getParkId(MyFangKeGuanLiActivity.this))
                .addParams("page","1")
                .build()
                .execute(new StringCallback() {
                    @Override
                    public void onError(Call call, Exception e, int id) {
                        System.out.println(e+"          eeee企业 访客管理");
                    }

                    @Override
                    public void onResponse(String response, int id) {
                        System.out.println(response+"           企业 访客管理的网络请求");
                        ContractUtils.Code500(MyFangKeGuanLiActivity.this,response);
                        if(response.indexOf("200") != -1){
                            Gson gson = new Gson();
                            fangKeGuanLiEntity = gson.fromJson(response, FangKeGuanLiEntity.class);
                            fangKeGuanLiAdapter = new FangKeGuanLiAdapter(MyFangKeGuanLiActivity.this,fangKeGuanLiEntity.getResponse());
                            LinearLayoutManager manager = new LinearLayoutManager(MyFangKeGuanLiActivity.this);
                            recyclerView.setLayoutManager(manager);
                            recyclerView.setAdapter(fangKeGuanLiAdapter);

//                            删除访客管理的网络请求
                            fangKeGuanLiAdapter.setShanChuClick(new FangKeGuanLiAdapter.ShanChuClick() {
                                @Override
                                public void shanchuClick(int position, String id) {
                                    initShanChuHttp(position,id);
                                }


                            });



                            fangKeGuanLiAdapter.setBianJiClick(new FangKeGuanLiAdapter.BianJiClick() {
                                @Override
                                public void banjiclick(int position) {
                                    Intent intent = new Intent(MyFangKeGuanLiActivity.this,MyBianJiFangKeActivity.class);
                                    intent.putExtra("name",fangKeGuanLiEntity.getResponse().get(position).getName());
                                    intent.putExtra("number",fangKeGuanLiEntity.getResponse().get(position).getNums());
                                    intent.putExtra("phone",fangKeGuanLiEntity.getResponse().get(position).getPhone());
                                    intent.putExtra("daodatime",fangKeGuanLiEntity.getResponse().get(position).getDaodaTime());
                                    intent.putExtra("chapai",fangKeGuanLiEntity.getResponse().get(position).getChepai());
                                    intent.putExtra("id",fangKeGuanLiEntity.getResponse().get(position).getId());
                                    startActivity(intent);
                                }
                            });

                        }else if(response.indexOf("400") != -1){
                            if(fangKeGuanLiEntity != null){
                                fangKeGuanLiEntity.getResponse().clear();

                                fangKeGuanLiAdapter.notifyDataSetChanged();
                            }
                        }
                    }
                });
    }








    //    访客管理的网络请求
    private void initFangKeGuanLiHttp() {
        OkHttpUtils.post()
                .url(ContractUtils.LOGIN_URL+"fuwu/fangke")
                .addHeader("token",ContractUtils.getTOKEN(MyFangKeGuanLiActivity.this))
                .addParams("parkId",ContractUtils.getParkId(MyFangKeGuanLiActivity.this))
                .addParams("page","1")
                .build()
                .execute(new StringCallback() {
                    @Override
                    public void onError(Call call, Exception e, int id) {
                        System.out.println(e+"          eeee企业 访客管理");
                    }

                    @Override
                    public void onResponse(String response, int id) {
                        System.out.println(response+"           企业 访客管理的网络请求");
                        ContractUtils.Code500(MyFangKeGuanLiActivity.this,response);
                        if(response.indexOf("200") != -1){
//                            访客管理的网络请求第二次
                            initFangKeGuanLiHttpTwo();
//                            Gson gson = new Gson();
//                            fangKeGuanLiEntity = gson.fromJson(response, FangKeGuanLiEntity.class);
//                            fangKeGuanLiAdapter = new FangKeGuanLiAdapter(MyFangKeGuanLiActivity.this,fangKeGuanLiEntity.getResponse());
//                            LinearLayoutManager manager = new LinearLayoutManager(MyFangKeGuanLiActivity.this);
//                            recyclerView.setLayoutManager(manager);
//                            recyclerView.setAdapter(fangKeGuanLiAdapter);
//
////                            删除访客管理的网络请求
//                            fangKeGuanLiAdapter.setShanChuClick(new FangKeGuanLiAdapter.ShanChuClick() {
//                                @Override
//                                public void shanchuClick(int position, String id) {
//                                    initShanChuHttp(position,id);
//                                }
//
//
//                            });
//
//
//
//                            fangKeGuanLiAdapter.setBianJiClick(new FangKeGuanLiAdapter.BianJiClick() {
//                                @Override
//                                public void banjiclick(int position) {
//                                    Intent intent = new Intent(MyFangKeGuanLiActivity.this,MyBianJiFangKeActivity.class);
//                                    intent.putExtra("name",fangKeGuanLiEntity.getResponse().get(position).getName());
//                                    intent.putExtra("number",fangKeGuanLiEntity.getResponse().get(position).getNums());
//                                    intent.putExtra("phone",fangKeGuanLiEntity.getResponse().get(position).getPhone());
//                                    intent.putExtra("daodatime",fangKeGuanLiEntity.getResponse().get(position).getDaodaTime());
//                                    intent.putExtra("chapai",fangKeGuanLiEntity.getResponse().get(position).getChepai());
//                                    intent.putExtra("id",fangKeGuanLiEntity.getResponse().get(position).getId());
//                                    startActivity(intent);
//                                }
//                            });

                        }else if(response.indexOf("400") != -1){
                            if(fangKeGuanLiEntity != null){
                                fangKeGuanLiEntity.getResponse().clear();

                                fangKeGuanLiAdapter.notifyDataSetChanged();
                            }
                        }
                    }
                });
    }



//    删除访客管理的网络请求
    private void initShanChuHttp(int position, String id ) {
        OkHttpUtils.post()
                .url(ContractUtils.LOGIN_URL+"fuwu/delfangke")
                .addHeader("token",ContractUtils.getTOKEN(MyFangKeGuanLiActivity.this))
                .addParams("id",id)
                .build()
                .execute(new StringCallback() {
                    @Override
                    public void onError(Call call, Exception e, int id) {
                        System.out.println(e+"            eeeeeeeeee删除访客管理的网络请求");
                    }

                    @Override
                    public void onResponse(String response, int id) {
                        System.out.println(response+"             删除访客管理的网络请求 ");
                        ContractUtils.Code500(MyFangKeGuanLiActivity.this,response);
                        if(response.indexOf("200") != -1){
                            Gson gson = new Gson();
                            FangKeGuanLiShanChuEntity fangKeGuanLiShanChuEntity = gson.fromJson(response, FangKeGuanLiShanChuEntity.class);
                            Toast.makeText(MyFangKeGuanLiActivity.this, fangKeGuanLiShanChuEntity.getResponse().getMes(), Toast.LENGTH_SHORT).show();
                            //        访客管理的网络请求
                            initFangKeGuanLiHttp();
                        }else if(response.indexOf("400") != -1){
                            Gson gson = new Gson();
                            FangKeGuanLiShaChuShiBaiEntity fangKeGuanLiShaChuShiBaiEntity = gson.fromJson(response, FangKeGuanLiShaChuShiBaiEntity.class);
                            Toast.makeText(MyFangKeGuanLiActivity.this, fangKeGuanLiShaChuShiBaiEntity.getMsg(), Toast.LENGTH_SHORT).show();
                        }
                    }
                });
    }


    private void initView() {
//        fangKeGuanLiAdapter = new FangKeGuanLiAdapter(this);
//        LinearLayoutManager manager = new LinearLayoutManager(this);
//        recyclerView.setLayoutManager(manager);
//        recyclerView.setAdapter(fangKeGuanLiAdapter);


//        fangKeGuanLiAdapter.setBianJiClick(new FangKeGuanLiAdapter.BianJiClick() {
//            @Override
//            public void banjiclick(int position) {
//                Intent intent = new Intent(MyFangKeGuanLiActivity.this,MyBianJiFangKeActivity.class);
//                startActivity(intent);
//            }
//        });

    }

    @OnClick({R.id.image_back, R.id.tv_tianjia_fangke})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.image_back:
                finish();
                break;
            case R.id.tv_tianjia_fangke:
                Intent intent = new Intent(MyFangKeGuanLiActivity.this,TianJiaFangKeActivity.class);
                startActivity(intent);
                break;
        }
    }
}
