package com.wanyangproject.my;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.wanyangproject.R;
import com.wanyangproject.activity.QiYeChongZhiJiLuActivity;
import com.wanyangproject.activity.YiKaTongZhiFuActivity;
import com.wanyangproject.entity.MenWeiShuiDianXinXiEntity;
import com.wanyangproject.entity.NumberEntity;
import com.wanyangproject.entity.YiKaTongYuEEntity;
import com.wanyangproject.shouye.WaiMaiActivity;
import com.wanyangproject.utils.ContractUtils;
import com.zhy.http.okhttp.OkHttpUtils;
import com.zhy.http.okhttp.callback.StringCallback;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.Call;

public class YiKaTongActivity extends AppCompatActivity {

    @BindView(R.id.image_back)
    ImageView imageBack;
    @BindView(R.id.tv_chongzhi_jilu)
    TextView tvChongzhiJilu;
    @BindView(R.id.tv_zhanghao)
    TextView tvZhanghao;
    @BindView(R.id.tv_yue)
    TextView tvYue;
    @BindView(R.id.btn_wushi)
    Button btnWushi;
    @BindView(R.id.btn_yibai)
    Button btnYibai;
    @BindView(R.id.btn_erbai)
    Button btnErbai;
    @BindView(R.id.btn_queren)
    Button btnQueren;
    @BindView(R.id.et_jine)
    EditText etJine;
    private String money = "0";
//    private MenWeiShuiDianXinXiEntity menWeiShuiDianXinXiEntity;
    private YiKaTongYuEEntity yiKaTongYuEEntity;
    private NetWork netWork;
    private String chongzhichenggong;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_yi_ka_tong);
        ButterKnife.bind(this);



//        Intent intent = getIntent();
//        String response = intent.getStringExtra("response");
//        System.out.println(response+"     response");
//        if(response != null){
//            Gson gson = new Gson();
//            menWeiShuiDianXinXiEntity = gson.fromJson(response, MenWeiShuiDianXinXiEntity.class);
//        }
//



//        一卡通余额的网络请求
        initYiKaTongYuEHttp();






        IntentFilter intentFilter4 = new IntentFilter();
        intentFilter4.addAction("chongzhichenggong");
        netWork = new NetWork();
        registerReceiver(netWork, intentFilter4);


    }








    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(netWork);
    }

    class NetWork extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {

            //            结束页面
            chongzhichenggong = intent.getStringExtra("chongzhichenggong");
            if(chongzhichenggong != null){
                etJine.setText("");

//        一卡通余额的网络请求
                initYiKaTongYuEHttp();
            }
        }

    }


    //    一卡通余额的网络请求
    private void initYiKaTongYuEHttp() {
        OkHttpUtils.post()
                .url(ContractUtils.LOGIN_URL + "user/getCardInfo")
                .addHeader("token", ContractUtils.getTOKEN(YiKaTongActivity.this))
                .build()
                .execute(new StringCallback() {
                    @Override
                    public void onError(Call call, Exception e, int id) {
                        System.out.println(e + "   eeee一卡通余额");
                    }

                    @Override
                    public void onResponse(String response, int id) {
                        ContractUtils.Code500(YiKaTongActivity.this, response);
                        System.out.println(response + "            一卡通余额的网络请求");
                        if (response.indexOf("200") != -1) {
                            Gson gson = new Gson();
                            yiKaTongYuEEntity = gson.fromJson(response, YiKaTongYuEEntity.class);
                            if (yiKaTongYuEEntity.getResponse().getBalance() == null) {

                            } else {
                                tvYue.setText("¥" + yiKaTongYuEEntity.getResponse().getBalance());
                            }



                            if(yiKaTongYuEEntity.getResponse().getCardNumbers() != null){
                                if(yiKaTongYuEEntity.getResponse().getCardNumbers().size() > 0){
                                    tvZhanghao.setText("一卡通账号: " + yiKaTongYuEEntity.getResponse().getCardNumbers().get(0));
                                }
                            }
                        }else {
                            ContractUtils.Code400(YiKaTongActivity.this,response);
                        }
                    }
                });
    }


    @OnClick({R.id.image_back, R.id.tv_chongzhi_jilu, R.id.tv_zhanghao, R.id.tv_yue, R.id.btn_wushi, R.id.btn_yibai, R.id.btn_erbai, R.id.btn_queren})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.image_back:
                finish();
                break;
            case R.id.tv_chongzhi_jilu:
//                Intent intent = new Intent(YiKaTongActivity.this, QiYeChongZhiJiLuActivity.class);
//                startActivity(intent);
                Intent intent = new Intent(YiKaTongActivity.this, ChongZhJiLuActivity.class);
                startActivity(intent);
                break;
            case R.id.tv_zhanghao:
                break;
            case R.id.tv_yue:
                break;
            case R.id.btn_wushi:
                money = "50";
                setFastRecharge(btnWushi, true);
                setFastRecharge(btnYibai, false);
                setFastRecharge(btnErbai, false);
                etJine.setText(money);
                break;
            case R.id.btn_yibai:
                money = "100";
                setFastRecharge(btnWushi, false);
                setFastRecharge(btnYibai, true);
                setFastRecharge(btnErbai, false);
                etJine.setText(money);
                break;
            case R.id.btn_erbai:
                money = "200";
                setFastRecharge(btnWushi, false);
                setFastRecharge(btnYibai, false);
                setFastRecharge(btnErbai, true);
                etJine.setText(money);
                break;
            case R.id.btn_queren:
                if (etJine.getText().toString().trim().equals("") && money.equals("0")) {
                    Toast.makeText(this, "请选择或输入充值金额", Toast.LENGTH_SHORT).show();
                    return;
                }
                if(etJine.getText().toString().trim().length() > 0){
                    money = etJine.getText().toString().trim();
                }


                if(yiKaTongYuEEntity.getResponse().getCardNumbers().get(0).equals("")){
                    Toast.makeText(this, "暂无一卡通账号！", Toast.LENGTH_SHORT).show();
                    return;
                }


                Intent intent1 = new Intent(YiKaTongActivity.this, YiKaTongZhiFuActivity.class);
                intent1.putExtra("money", money);
                intent1.putExtra("state","1");//1：一卡通2：水费3：电费
                intent1.putExtra("id",yiKaTongYuEEntity.getResponse().getParkPersonId());
                intent1.putExtra("name",yiKaTongYuEEntity.getResponse().getParkPersonName());
                startActivity(intent1);
//                finish();
                break;
        }
    }


    private void setFastRecharge(Button button, boolean isChecked) {
        if (isChecked) {
            button.setTextColor(getResources().getColor(R.color.white));
            button.setBackgroundResource(R.drawable.shape_red_bg);
        } else {
            button.setTextColor(getResources().getColor(R.color.red));
            button.setBackgroundResource(R.drawable.shape_red_border);
        }
    }
}
