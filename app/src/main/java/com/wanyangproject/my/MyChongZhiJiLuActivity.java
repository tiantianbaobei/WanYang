package com.wanyangproject.my;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.Gravity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

import com.androidkun.xtablayout.XTabLayout;
import com.bigkoo.pickerview.TimePickerView;
import com.wanyangproject.R;
import com.wanyangproject.activity.LoginActivity;
import com.wanyangproject.adapter.MyPagerAdapter;
import com.wanyangproject.mysuipian.MyDianFeiJiLuFragment;
import com.wanyangproject.mysuipian.MyShuiFeiJiLuFragment;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MyChongZhiJiLuActivity extends AppCompatActivity {

    @BindView(R.id.image_back)
    ImageView imageBack;
    @BindView(R.id.spinner)
    Spinner spinner;
    @BindView(R.id.tabLayout)
    XTabLayout tabLayout;
    @BindView(R.id.viewPager)
    ViewPager viewPager;
    @BindView(R.id.tv_time)
    TextView tvTime;
    private String shuiid;
    private String shuiid2;
    private String yudian;
    private MyShuiFeiJiLuFragment myShuiFeiJiLuFragment;
    private MyDianFeiJiLuFragment myDianFeiJiLuFragment;
    List<Fragment> fragments = new ArrayList<>();
    private String yuangongshuiid;
    private String yuangongshuiid2;
    private String yuangongdian;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_chong_zhi_ji_lu);
        ButterKnife.bind(this);
        initPager();



        Intent intent = getIntent();
        shuiid = intent.getStringExtra("shuiid");
//        shuiid2 = intent.getStringExtra("shuiid2");
        yudian = intent.getStringExtra("yudian");
        yuangongshuiid = intent.getStringExtra("yuangongshuiid");
//        yuangongshuiid2 = intent.getStringExtra("yuangongshuiid2");
        yuangongdian = intent.getStringExtra("yuangongdian");







        //        推荐
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                TextView tv = (TextView) view;
                tv.setTextColor(getResources().getColor(R.color.colorLunTanSpinner));    //设置颜色
                tv.setTextSize(14.0f);    //设置大小
                tv.setGravity(Gravity.CENTER_HORIZONTAL);   //设置居中
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

//        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
//            @Override
//            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
//                TextView tv = (TextView) view;
//                tv.setTextColor(getResources().getColor(R.color.colorLunTanSpinner));    //设置颜色
//                tv.setTextSize(14.0f);    //设置大小
//                tv.setGravity(Gravity.CENTER_HORIZONTAL);   //设置居中
//            }
//
//            @Override
//            public void onNothingSelected(AdapterView<?> parent) {
//            }
//        });
    }





    private void initPager() {

        myShuiFeiJiLuFragment = new MyShuiFeiJiLuFragment();
        myDianFeiJiLuFragment = new MyDianFeiJiLuFragment();
        fragments.add(myShuiFeiJiLuFragment);
        fragments.add(myDianFeiJiLuFragment);
        myDianFeiJiLuFragment.setYudian(yudian);
        myDianFeiJiLuFragment.setYuangongdian(yuangongdian);
        myShuiFeiJiLuFragment.setShuiid(shuiid);
//        myShuiFeiJiLuFragment.setShuiid2(shuiid2);
        myShuiFeiJiLuFragment.setYuangongshuiid(yuangongshuiid);
//        myShuiFeiJiLuFragment.setYuangongshuiid2(yuangongshuiid2);
//        fragments.add(new MyShuiFeiJiLuFragment());
//        fragments.add(new MyDianFeiJiLuFragment());

        // 创建ViewPager适配器
        MyPagerAdapter myPagerAdapter = new MyPagerAdapter(getSupportFragmentManager());
        myPagerAdapter.setFragments(fragments);
        // 给ViewPager设置适配器
        viewPager.setAdapter(myPagerAdapter);
        // TabLayout 指示器 (记得自己手动创建4个Fragment,注意是 app包下的Fragment 还是 V4包下的 Fragment)
        tabLayout.addTab(tabLayout.newTab());
        tabLayout.addTab(tabLayout.newTab());

        // 使用 TabLayout 和 ViewPager 相关联
        tabLayout.setupWithViewPager(viewPager);
        // TabLayout指示器添加文本
        tabLayout.getTabAt(0).setText("水费");
        tabLayout.getTabAt(1).setText("电费");
    }

    @OnClick({R.id.image_back,R.id.tv_time})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.image_back:
                finish();
                break;
            case R.id.tv_time:
                Calendar calendar = Calendar.getInstance();

                TimePickerView timePickerView = new TimePickerView.Builder(MyChongZhiJiLuActivity.this, new TimePickerView.OnTimeSelectListener() {
                    @Override
                    public void onTimeSelect(Date date, View v) {
//                        SimpleDateFormat simpleDateFormat1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                        SimpleDateFormat simpleDateFormat1 = new SimpleDateFormat("yyyy-MM");
                        tvTime.setText(simpleDateFormat1.format(date));

                        Intent intent=new Intent();
                        intent.putExtra("timejilu","timejilu");
                        intent.putExtra("time",simpleDateFormat1.format(date));
                        intent.setAction("timejilu");
                        sendBroadcast(intent);


//                        try {
//                            Date date1 = simpleDateFormat1.parse(tvTime.getText().toString());
//                            long time = date1.getTime();
//                            string = String.valueOf(time);
//                            System.out.println(string + "     时间戳");
//                        } catch (ParseException e) {
//                            e.printStackTrace();
//                        }
                    }
                })
                        .setType(new boolean[]{true, true, false, false, false, false})//默认全部显示
                        .setCancelText("取消")//取消按钮文字
                        .setSubmitText("确定")//确认按钮文字
                        .setTextXOffset(0, 0, 0, 0, 0, 0)
                        .setLunarCalendar(false)
                        .setContentSize(14)//滚轮文字大小
                        .gravity(Gravity.CENTER)
                        .setLineSpacingMultiplier(2.8f)//设置两横线之间的间隔倍数
                        .setTitleSize(16)//标题文字大小
                        .setTitleText("选择日期")//标题文字
                        .setOutSideCancelable(true)//点击屏幕，点在控件外部范围时，是否取消显示
                        .isCyclic(true)//是否循环滚动
                        .setTextColorCenter(Color.BLACK)//设置选中项的颜色
                        .setTextColorOut(Color.parseColor("#AEAAAA"))
                        .setTitleColor(Color.BLACK)//标题文字颜色
                        .setSubmitColor(Color.RED)//确定按钮文字颜色
                        .setCancelColor(Color.parseColor("#AEAAAA"))//取消按钮文字颜色
                        .setTitleBgColor(Color.parseColor("#f5f5f5"))//标题背景颜色 Night mode
                        .setBgColor(Color.parseColor("#ffffff"))//滚轮背景颜色 Night mode
                        .setRange(1900, calendar.get(Calendar.YEAR))//默认是1900-2100年
//                        .setDate(selectedDate)// 如果不设置的话，默认是系统时间*/
//                        .setRangDate(startDate,endDate)//起始终止年月日设定
                        .setLabel("", "", "", ":", "", "")
                        .isCenterLabel(true) //是否只显示中间选中项的label文字，false则每项item全部都带有label。
                        .isDialog(false)//是否显示为对话框样式
                        .build();
                timePickerView.setDate(Calendar.getInstance());//注：根据需求来决定是否使用该方法（一般是精确到秒的情况），此项可以在弹出选择器的时候重新设置当前时间，避免在初始化之后由于时间已经设定，导致选中时间与当前时间不匹配的问题。
                timePickerView.show();


                break;
        }
    }
}
