package com.wanyangproject.my;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.wanyangproject.R;
import com.wanyangproject.entity.ShangJiaShenQingLieBiaoEntity;
import com.wanyangproject.myadapter.RuZhuShenQingAdapter;
import com.wanyangproject.utils.ContractUtils;
import com.zhy.http.okhttp.OkHttpUtils;
import com.zhy.http.okhttp.callback.StringCallback;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.Call;

public class RuZhuShenQingActivity extends AppCompatActivity {

    @BindView(R.id.image_back)
    ImageView imageBack;
    @BindView(R.id.image_add)
    ImageView imageAdd;
    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    @BindView(R.id.relative_meiyou)
    RelativeLayout relativeMeiyou;
    @BindView(R.id.tv_lijiruzhu)
    TextView tvLijiruzhu;
    private RuZhuShenQingAdapter ruZhuShenQingAdapter;
    private ShangJiaShenQingLieBiaoEntity shangJiaShenQingLieBiaoEntity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ru_zhu_shen_qing);
        ButterKnife.bind(this);

        initView();


//        商家入驻申请列表的网络请求
        initShangJiaRuZhuLieBiaoHttp();
    }



    @Override
    protected void onResume() {
        super.onResume();
        //        商家入驻申请列表的网络请求
        initShangJiaRuZhuLieBiaoHttp();
    }

    //    商家入驻申请列表的网络请求
    private void initShangJiaRuZhuLieBiaoHttp() {
        OkHttpUtils.post()
                .url(ContractUtils.LOGIN_URL + "shop/appshoplist")
                .addHeader("token", ContractUtils.getTOKEN(RuZhuShenQingActivity.this))
                .addParams("parkId", ContractUtils.getParkId(RuZhuShenQingActivity.this))
                .build()
                .execute(new StringCallback() {
                    @Override
                    public void onError(Call call, Exception e, int id) {

                    }

                    @Override
                    public void onResponse(String response, int id) {
                        System.out.println(response + "          商家入驻列表的网络请求");

                        if (response.indexOf("200") != -1) {
                            recyclerView.setVisibility(View.VISIBLE);
                            relativeMeiyou.setVisibility(View.GONE);
                            Gson gson = new Gson();
                            shangJiaShenQingLieBiaoEntity = gson.fromJson(response, ShangJiaShenQingLieBiaoEntity.class);

                            ruZhuShenQingAdapter = new RuZhuShenQingAdapter(RuZhuShenQingActivity.this, shangJiaShenQingLieBiaoEntity.getResponse());
                            LinearLayoutManager manager = new LinearLayoutManager(RuZhuShenQingActivity.this);
                            recyclerView.setLayoutManager(manager);
                            recyclerView.setAdapter(ruZhuShenQingAdapter);
                        } else  {
//                            ContractUtils.Code400(RuZhuShenQingActivity.this,response);
                        }
                    }
                });
    }

    private void initView() {
//        ruZhuShenQingAdapter = new RuZhuShenQingAdapter(this);
//        LinearLayoutManager manager = new LinearLayoutManager(this);
//        recyclerView.setLayoutManager(manager);
//        recyclerView.setAdapter(ruZhuShenQingAdapter);
    }

    @OnClick({R.id.image_back, R.id.image_add,R.id.tv_lijiruzhu})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.image_back:
                finish();
                break;
            case R.id.image_add:
                if(shangJiaShenQingLieBiaoEntity != null){
                    List<ShangJiaShenQingLieBiaoEntity.ResponseBean> response = shangJiaShenQingLieBiaoEntity.getResponse();
                    for(ShangJiaShenQingLieBiaoEntity.ResponseBean bean : response) {
                        if (bean.getType().equals("1")) {//1申请中   2 审核通过   3拒绝
                            Toast.makeText(this, "您已经申请过，正在审核中！", Toast.LENGTH_SHORT).show();
                            return;
                        } else if (bean.getType().equals("2")) {//1申请中   2 审核通过   3拒绝
                            Toast.makeText(this, "您已经是商家，不能重复申请！", Toast.LENGTH_SHORT).show();
                            return;
                        }
                    }
                    Intent intent = new Intent(RuZhuShenQingActivity.this, ShangJiaRuZhuActivity.class);
                    startActivity(intent);
                }else{
                    Intent intent = new Intent(RuZhuShenQingActivity.this, ShangJiaRuZhuActivity.class);
                    startActivity(intent);
                }


//                if(ContractUtils.getTypeId(RuZhuShenQingActivity.this).equals("3") || ContractUtils.getTypeId22(RuZhuShenQingActivity.this).equals("3")){
//                    Toast.makeText(this, "您已经是商家，不能重复申请！", Toast.LENGTH_SHORT).show();
//                    return;
////                }
//
//                Intent intent = new Intent(RuZhuShenQingActivity.this, ShangJiaRuZhuActivity.class);
//                startActivity(intent);
                break;
//            立即入驻
            case R.id.tv_lijiruzhu:
                if(shangJiaShenQingLieBiaoEntity != null){
                    List<ShangJiaShenQingLieBiaoEntity.ResponseBean> response1 = shangJiaShenQingLieBiaoEntity.getResponse();
                    for(ShangJiaShenQingLieBiaoEntity.ResponseBean bean : response1) {
                        if (bean.getType().equals("1")) {//1申请中   2 审核通过   3拒绝
                            Toast.makeText(this, "您已经申请过，正在审核中！", Toast.LENGTH_SHORT).show();
                            return;
                        } else if (bean.getType().equals("2")) {//1申请中   2 审核通过   3拒绝
                            Toast.makeText(this, "您已经是商家，不能重复申请！", Toast.LENGTH_SHORT).show();
                            return;
                        }
                    }

                    Intent intent = new Intent(RuZhuShenQingActivity.this, ShangJiaRuZhuActivity.class);
                    startActivity(intent);

                }else{
                    Intent intent = new Intent(RuZhuShenQingActivity.this, ShangJiaRuZhuActivity.class);
                    startActivity(intent);
                }



//                if(ContractUtils.getTypeId(RuZhuShenQingActivity.this).equals("3") || ContractUtils.getTypeId22(RuZhuShenQingActivity.this).equals("3")){
//                    Toast.makeText(this, "您已经是商家，不能重复申请！", Toast.LENGTH_SHORT).show();
//                    return;
//                }
//                Intent intent1 = new Intent(RuZhuShenQingActivity.this, ShangJiaRuZhuActivity.class);
//                startActivity(intent1);
                break;
        }
    }
}
