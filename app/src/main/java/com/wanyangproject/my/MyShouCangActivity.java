package com.wanyangproject.my;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;

import com.androidkun.xtablayout.XTabLayout;
import com.wanyangproject.R;
import com.wanyangproject.adapter.MyPagerAdapter;
import com.wanyangproject.mysuipian.DaiPingJiaFragment;
import com.wanyangproject.mysuipian.DaiShouHuoFragment;
import com.wanyangproject.mysuipian.DaiZhiFuFragment;
import com.wanyangproject.mysuipian.DingDanQuanBuFragment;
import com.wanyangproject.mysuipian.FinishFragment;
import com.wanyangproject.mysuipian.MyDianZanFragment;
import com.wanyangproject.mysuipian.MyFaBuFragment;
import com.wanyangproject.mysuipian.MyPingLunFragment;
import com.wanyangproject.mysuipian.MyShangJiaFragment;
import com.wanyangproject.mysuipian.MyTieZiFragment;
import com.wanyangproject.mysuipian.MyWenZhangFragment;
import com.wanyangproject.mysuipian.MyZhaoPinFragment;
import com.wanyangproject.utils.NoScrollViewPager;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MyShouCangActivity extends AppCompatActivity {

    //    @BindView(R.id.bar_back)
//    ImageButton barBack;
    //    @BindView(R.id.bar_title)
//    TextView barTitle;
//    @BindView(R.id.tabLayout)
//    TabLayout tabLayout;
    @BindView(R.id.viewPager)
    NoScrollViewPager viewPager;
    @BindView(R.id.tabLayout)
    XTabLayout tabLayout;
    @BindView(R.id.image_back)
    ImageView imageBack;
//    @BindView(R.id.viewPager)
//    ViewPager viewPager;
    List<Fragment> fragments = new ArrayList<>();
    private MyWenZhangFragment myWenZhangFragment;
    private MyTieZiFragment myTieZiFragment;
    private MyZhaoPinFragment myZhaoPinFragment;
    private MyShangJiaFragment myShangJiaFragment;




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_shou_cang);
        ButterKnife.bind(this);

        initPager1();
//        barTitle.setText("我的收藏");
    }



    private void initPager1() {
//        List<Fragment> fragments = new ArrayList<>();
//        fragments.add(new MyWenZhangFragment());
//        fragments.add(new MyTieZiFragment());
//        fragments.add(new MyZhaoPinFragment());
//        fragments.add(new MyShangJiaFragment());
        myWenZhangFragment = new MyWenZhangFragment();
        myTieZiFragment = new MyTieZiFragment();
        myZhaoPinFragment = new MyZhaoPinFragment();
        myShangJiaFragment = new MyShangJiaFragment();
        fragments.add(myWenZhangFragment);
        fragments.add(myTieZiFragment);
        fragments.add(myZhaoPinFragment);
        fragments.add(myShangJiaFragment);


        // 创建ViewPager适配器
        MyPagerAdapter myPagerAdapter = new MyPagerAdapter(getSupportFragmentManager());
        myPagerAdapter.setFragments(fragments);
        // 给ViewPager设置适配器
        viewPager.setAdapter(myPagerAdapter);
        // TabLayout 指示器 (记得自己手动创建4个Fragment,注意是 app包下的Fragment 还是 V4包下的 Fragment)
        tabLayout.addTab(tabLayout.newTab());
        tabLayout.addTab(tabLayout.newTab());
        tabLayout.addTab(tabLayout.newTab());
        tabLayout.addTab(tabLayout.newTab());
        // 使用 TabLayout 和 ViewPager 相关联
        tabLayout.setupWithViewPager(viewPager);
        // TabLayout指示器添加文本
        tabLayout.getTabAt(0).setText("文章");
        tabLayout.getTabAt(1).setText("帖子");
        tabLayout.getTabAt(2).setText("招聘");
        tabLayout.getTabAt(3).setText("商家");



//        Intent intent = getIntent();
//        String stringExtra = intent.getStringExtra("shoucang");
//        if(stringExtra != null) {
//            int i = Integer.parseInt(stringExtra);
//            viewPager.setCurrentItem(i);
//
//            if (i == 0) {
//                myWenZhangFragment.initMyShouCangHttp();
//            } else if (i == 1) {
//                myTieZiFragment.initShouCangTieZiHttp();
//            } else if (i == 2) {
//                myZhaoPinFragment.initZhaoPinShouCangHttp();
//            } else if (i == 3) {
//                myShangJiaFragment.initShouCangShangJiaXianShiHttp();
//            }
//
//
//        }


//        List<Fragment> fragments = new ArrayList<>();
//        fragments.add(new MyWenZhangFragment());
//        fragments.add(new MyTieZiFragment());
//        fragments.add(new MyZhaoPinFragment());
//        fragments.add(new MyShangJiaFragment());
//        // 创建ViewPager适配器
//        MyPagerAdapter myPagerAdapter = new MyPagerAdapter(getSupportFragmentManager());
//        myPagerAdapter.setFragments(fragments);
//        // 给ViewPager设置适配器
//        viewPager.setAdapter(myPagerAdapter);
//        // TabLayout 指示器 (记得自己手动创建4个Fragment,注意是 app包下的Fragment 还是 V4包下的 Fragment)
//        tabLayout.addTab(tabLayout.newTab());
//        tabLayout.addTab(tabLayout.newTab());
//        tabLayout.addTab(tabLayout.newTab());
//        tabLayout.addTab(tabLayout.newTab());
//        // 使用 TabLayout 和 ViewPager 相关联
//        tabLayout.setupWithViewPager(viewPager);
//        // TabLayout指示器添加文本
//        tabLayout.getTabAt(0).setText("文章");
//        tabLayout.getTabAt(1).setText("帖子");
//        tabLayout.getTabAt(2).setText("招聘");
//        tabLayout.getTabAt(3).setText("商家");
    }

//
//    @Override
//    public boolean onTouchEvent(MotionEvent event) {
//        if (noScroll) {
//            return super.onTouchEvent(event);
//        } else {
//            return false;
//        }
//    }
//
//    @Override
//    public boolean dispatchTouchEvent(MotionEvent ev) {
//        if (noScroll) {
//            return super.dispatchTouchEvent(ev);
//        } else {
//            return false;
//        }
//    }


    @OnClick({R.id.tabLayout, R.id.viewPager, R.id.image_back})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.image_back:
                finish();
                break;
            case R.id.tabLayout:
                break;
            case R.id.viewPager:
                break;
        }
    }

}
