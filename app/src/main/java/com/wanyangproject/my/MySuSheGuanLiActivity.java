package com.wanyangproject.my;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.wanyangproject.R;
import com.wanyangproject.adapter.SuSheGuanLiAdapter;
import com.wanyangproject.entity.MyQiYeSuSheGuanLiEntity;
import com.wanyangproject.myadapter.ChangFangGuanLiAdapter;
import com.wanyangproject.utils.ContractUtils;
import com.zhy.http.okhttp.OkHttpUtils;
import com.zhy.http.okhttp.callback.StringCallback;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.Call;

import static android.R.attr.data;

public class MySuSheGuanLiActivity extends AppCompatActivity {

    @BindView(R.id.tv_sushe)
    TextView tvSushe;
    @BindView(R.id.image_sousuo)
    ImageView imageSousuo;
    @BindView(R.id.image_back)
    ImageView imageBack;
    @BindView(R.id.relative_sousuo)
    RelativeLayout relativeSousuo;
    @BindView(R.id.relative_title)
    RelativeLayout relativeTitle;
    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    @BindView(R.id.et_sousuo)
    EditText etSousuo;
    private SuSheGuanLiAdapter suSheGuanLiAdapter;
    private MyQiYeSuSheGuanLiEntity myQiYeSuSheGuanLiEntity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_su_she_guan_li);
        ButterKnife.bind(this);
        initView();


//        企业身份宿舍管理的网络请求
        initQiYeSuSheGuanLiHttp();


        etSousuo.addTextChangedListener(new TextWatcher() {
            @Override  //   输入文字之前的状态
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override // 输入文字中的状态
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override  // 输入文字之后的状态
            public void afterTextChanged(Editable editable) {
                //        企业身份宿舍管理的网络请求
                initQiYeSuSheGuanLiHttp();
            }
        });
    }




    //    企业身份宿舍管理的网络请求
    private void initQiYeSuSheGuanLiHttp() {
        OkHttpUtils.post()
                .url(ContractUtils.LOGIN_URL + "Enterprise/dormitory")
                .addHeader("token", ContractUtils.getTOKEN(MySuSheGuanLiActivity.this))
                .addParams("parkId", ContractUtils.getParkId(MySuSheGuanLiActivity.this))
                .addParams("dormitoryName",etSousuo.getText().toString().trim())
                .build()
                .execute(new StringCallback() {
                    @Override
                    public void onError(Call call, Exception e, int id) {
                        System.out.println(e+"       eeee 企业身份宿舍管理 ");
                    }

                    @Override
                    public void onResponse(String response, int id) {
                        System.out.println(ContractUtils.getTOKEN(MySuSheGuanLiActivity.this)+"          宿舍token ");
                        System.out.println(ContractUtils.getParkId(MySuSheGuanLiActivity.this)+"            宿舍parkId");
                        System.out.println(etSousuo.getText().toString().trim()+"          宿舍dormitoryName");
                        System.out.println(response+"         企业身份宿舍管理的网络请求");
                        ContractUtils.Code500(MySuSheGuanLiActivity.this,response);
                        if(response.indexOf("200") != -1){
//                            try {
//                                JSONObject jsonObject = new JSONObject(response);
//                                JSONObject jsonObject1 = jsonObject.getJSONObject("response");
//                                JSONArray data = jsonObject1.getJSONArray("data");
//                                if(data == null){
//                                    Toast.makeText(MySuSheGuanLiActivity.this, "暂无数据", Toast.LENGTH_SHORT).show();
//                                    return;
//                                }
//                            } catch (JSONException e) {
//                                e.printStackTrace();
//                            }
//
                            Gson gson = new Gson();
                            myQiYeSuSheGuanLiEntity = gson.fromJson(response, MyQiYeSuSheGuanLiEntity.class);
                            suSheGuanLiAdapter = new SuSheGuanLiAdapter(MySuSheGuanLiActivity.this,myQiYeSuSheGuanLiEntity.getResponse().getData());
                            LinearLayoutManager manager = new LinearLayoutManager(MySuSheGuanLiActivity.this);
                            recyclerView.setLayoutManager(manager);
                            recyclerView.setAdapter(suSheGuanLiAdapter);


                            suSheGuanLiAdapter.setXiangQingClick(new SuSheGuanLiAdapter.XiangQingClick() {
                                @Override
                                public void xiangqingClick(int position,String id) {
                                    Intent intent = new Intent(MySuSheGuanLiActivity.this, MyQiYeSuSheXiangQingActivity.class);
                                    intent.putExtra("id",id);
                                    System.out.println(id+"        宿舍管理传id");
                                    startActivity(intent);
                                }
                            });
                        }else if(response.indexOf("400") != -1){
                            ContractUtils.Code400(MySuSheGuanLiActivity.this,response);
                        }
                    }
                });
    }


    private void initView() {
//        suSheGuanLiAdapter = new SuSheGuanLiAdapter(MySuSheGuanLiActivity.this);
//        LinearLayoutManager manager = new LinearLayoutManager(MySuSheGuanLiActivity.this);
//        recyclerView.setLayoutManager(manager);
//        recyclerView.setAdapter(suSheGuanLiAdapter);
//
//
//        suSheGuanLiAdapter.setXiangQingClick(new SuSheGuanLiAdapter.XiangQingClick() {
//            @Override
//            public void xiangqingClick(int position) {
//                Intent intent = new Intent(MySuSheGuanLiActivity.this, MyChangFangXiangQingActivity.class);
//                startActivity(intent);
//            }
//        });
    }

    @OnClick({R.id.tv_sushe, R.id.image_sousuo, R.id.image_back, R.id.relative_sousuo, R.id.relative_title, R.id.recyclerView})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.tv_sushe:
                break;
            case R.id.image_sousuo:
                tvSushe.setVisibility(View.INVISIBLE);
                relativeSousuo.setVisibility(View.VISIBLE);
                imageSousuo.setVisibility(View.INVISIBLE);
                break;
            case R.id.image_back:
                finish();
                break;
            case R.id.relative_sousuo:
                break;
            case R.id.relative_title:
                break;
            case R.id.recyclerView:
                break;
        }
    }
}
