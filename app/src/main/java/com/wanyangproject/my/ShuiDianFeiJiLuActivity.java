package com.wanyangproject.my;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.wanyangproject.R;
import com.wanyangproject.entity.MenWeiShuiDianXinXiEntity;
import com.wanyangproject.entity.MyQiYeChangFangXiangQingEntity;
import com.wanyangproject.entity.QiYeSuSheXiangQingEntity;
import com.wanyangproject.entity.ShuiDianEntity;
import com.wanyangproject.fragment.FirstFragment;
import com.wanyangproject.fragment.SecondFragment;
import com.wanyangproject.mysuipian.DianFragment;
import com.wanyangproject.mysuipian.ShuiFragment;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ShuiDianFeiJiLuActivity extends AppCompatActivity implements View.OnClickListener {

    @BindView(R.id.image_back)
    ImageView imageBack;
    //    @BindView(R.id.tabLayout)
//    TabLayout tabLayout;
    @BindView(R.id.tv_chongzhi_jilu)
    TextView tvChongzhiJilu;
    @BindView(R.id.tv_shuifei)
    TextView tvShuifei;
    @BindView(R.id.tv_dianfei)
    TextView tvDianfei;
//    @BindView(R.id.viewPager)
//    ViewPager viewPager;
    private ShuiFragment shuiFragment;
    private DianFragment dianFragment;
    private FragmentManager fragmentManager;
    private Fragment hideFragment;
    private MenWeiShuiDianXinXiEntity menWeiShuiDianXinXiEntity;
    private String dian;
    private FragmentTransaction fragmentTransaction;
    private String qiyeshuifei;
    private  String response;
    private String responseresponse222;
    private String responsechangfang;
    private QiYeSuSheXiangQingEntity qiYeSuSheXiangQingEntity;
    private MyQiYeChangFangXiangQingEntity myQiYeChangFangXiangQingEntity;
    private String yue;
    private String waterUserName;
    private String shuiid;
    private String yue2;
    private String waterUserName2;
    private String shuiid2;
    private String electricityUserName;
    private String electricityUserId;
    private String yudian;
    private NetWork netWork;
    private String shuileixingid;
    private String qiyeshuigid;


    public MenWeiShuiDianXinXiEntity getMenWeiShuiDianXinXiEntity() {
        return menWeiShuiDianXinXiEntity;
    }

    public void setMenWeiShuiDianXinXiEntity(MenWeiShuiDianXinXiEntity menWeiShuiDianXinXiEntity) {
        this.menWeiShuiDianXinXiEntity = menWeiShuiDianXinXiEntity;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shui_dian_fei_ji_lu);
        ButterKnife.bind(this);


        Intent intent = getIntent();

         yue = intent.getStringExtra("yue");
         waterUserName = intent.getStringExtra("waterUserName");
         shuiid = intent.getStringExtra("id");

         yue2 = intent.getStringExtra("yue2");
         waterUserName2 = intent.getStringExtra("waterUserName2");
         shuiid2 = intent.getStringExtra("id2");

         electricityUserId = intent.getStringExtra("electricityUserId");
         electricityUserName = intent.getStringExtra("electricityUserName");
         yudian = intent.getStringExtra("yudian");



        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("shuileixingid");
        netWork = new NetWork();
        registerReceiver(netWork, intentFilter);


        IntentFilter intentFilter1 = new IntentFilter();
        intentFilter1.addAction("qiyeshuigid");
        netWork = new NetWork();
        registerReceiver(netWork, intentFilter1);




//        Intent intent = getIntent();
//        员工的水电充值
        response = intent.getStringExtra("response");
        System.out.println(response+"     response");
        if(response != null){
            Gson gson = new Gson();
            menWeiShuiDianXinXiEntity = gson.fromJson(response, MenWeiShuiDianXinXiEntity.class);
        }


//
////        企业宿舍的水电充值
//        responseresponse222 = intent.getStringExtra("responseresponse222");
//        if(responseresponse222 != null ){
//            Gson gson = new Gson();
//            qiYeSuSheXiangQingEntity = gson.fromJson(responseresponse222, QiYeSuSheXiangQingEntity.class);
//        }
//
//
//
////        企业厂房水电
//        responsechangfang = intent.getStringExtra("responsechangfang");
//        if(responsechangfang != null){
//            Gson gson = new Gson();
//            myQiYeChangFangXiangQingEntity = gson.fromJson(responsechangfang, MyQiYeChangFangXiangQingEntity.class);
//        }

//        initViewPager();
        initView();
        initDefaultFragment();


        dian = intent.getStringExtra("dian");
        if(dian != null){
            tvDianfei.setBackgroundResource(R.drawable.yuanqu_tongzi_white_background_right);
            tvDianfei.setTextColor(getResources().getColor(R.color.red_color));
            tvShuifei.setBackgroundResource(R.drawable.yuanqu_tongzi_red_background_left);
            tvShuifei.setTextColor(getResources().getColor(R.color.white_color));
            replaceFragment1(dianFragment, fragmentTransaction);
        }


    }




    class NetWork extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            shuileixingid = intent.getStringExtra("shuileixingid");
            qiyeshuigid = intent.getStringExtra("qiyeshuigid");
//            Toast.makeText(context, wxopenid + wxnickname + wxheadimgurl, Toast.LENGTH_SHORT).show();
            System.out.println(shuileixingid + "        shuileixingid");

        }
    }




    private void initDefaultFragment() {
        fragmentManager = getSupportFragmentManager();
        fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.add(R.id.container_fragment, shuiFragment);
        fragmentTransaction.commit();
        hideFragment = shuiFragment;
    }



    private void replaceFragment(Fragment showFragment, FragmentTransaction fragmentTransaction) {
        if (showFragment.isAdded()) {
            fragmentTransaction.hide(hideFragment).show(showFragment).commit();
        } else {
            fragmentTransaction.hide(hideFragment).add(R.id.container_fragment, showFragment).commit();
        }
        hideFragment = showFragment;
    }




    private void replaceFragment1(Fragment showFragment, FragmentTransaction fragmentTransaction) {
        fragmentTransaction = fragmentManager.beginTransaction();
        if (showFragment.isAdded()) {
            fragmentTransaction.hide(hideFragment).show(showFragment).commit();
        } else {
            fragmentTransaction.hide(hideFragment).add(R.id.container_fragment, showFragment).commit();
        }
        hideFragment = showFragment;
    }





//    private String yue;
//    private String waterUserName;
//    private String shuiid;
//    private String yue2;
//    private String waterUserName2;
//    private String shuiid2;
//    private String electricityUserName;
//    private String electricityUserId;
//    private String yudian;

    //    初始化
    private void initView() {
        shuiFragment = new ShuiFragment();


        if(yue != null){
            shuiFragment.setYue(yue);
        }
        if(shuiid != null){
            shuiFragment.setShuiid(shuiid);
        }

        if(waterUserName != null){
            shuiFragment.setWaterUserName(waterUserName);
        }


        if(shuiid2 != null){
            shuiFragment.setShuiid2(shuiid2);
        }

        if(waterUserName2 != null){
            shuiFragment.setWaterUserName2(waterUserName2);
        }

        if(yue2 != null){
            shuiFragment.setYue2(yue2);
        }


        if(response != null){// 员工水电
            shuiFragment.setWeiShuiDianXinXiEntity(menWeiShuiDianXinXiEntity);
        }

//
//        if(responseresponse222 != null){//企业宿舍水电
//            shuiFragment.setQiYeSuSheXiangQingEntity(qiYeSuSheXiangQingEntity);
//        }
//
//
//        if(responsechangfang != null){//企业厂房水电
//            shuiFragment.setMyQiYeChangFangXiangQingEntity(myQiYeChangFangXiangQingEntity);
//        }

        //    private String electricityUserName;
//    private String electricityUserId;
//    private String yudian;
        dianFragment = new DianFragment();
        if(electricityUserName != null){
            dianFragment.setDianname(electricityUserName);
        }

        if(electricityUserId != null){
            dianFragment.setDianid(electricityUserId);
        }

        if(yudian != null){
            dianFragment.setYuedian(yudian);
        }

        if(response != null){// 员工水电
            dianFragment.setWeiShuiDianXinXiEntity(menWeiShuiDianXinXiEntity);
        }

//        if(responseresponse222 != null){//企业宿舍水电
//            dianFragment.setQiYeSuSheXiangQingEntity(qiYeSuSheXiangQingEntity);
//        }
//
//        if(responsechangfang != null){//企业厂房水电
//            dianFragment.setMyQiYeChangFangXiangQingEntity(myQiYeChangFangXiangQingEntity);
//        }

        tvShuifei.setOnClickListener(this);
        tvDianfei.setOnClickListener(this);
    }

//    private void initViewPager() {
//        List<Fragment> fragments = new ArrayList<>();
//        fragments.add(new ShuiFragment());
//        fragments.add(new DianFragment());
//        // 创建ViewPager适配器
//        MyPagerAdapter myPagerAdapter = new MyPagerAdapter(getSupportFragmentManager());
//        myPagerAdapter.setFragments(fragments);
//        // 给ViewPager设置适配器
//        viewPager.setAdapter(myPagerAdapter);
//        // TabLayout 指示器 (记得自己手动创建4个Fragment,注意是 app包下的Fragment 还是 V4包下的 Fragment)
//        tabLayout.addTab(tabLayout.newTab());
//        tabLayout.addTab(tabLayout.newTab());
//        // 使用 TabLayout 和 ViewPager 相关联
//        tabLayout.setupWithViewPager(viewPager);
//        // TabLayout指示器添加文本
//        tabLayout.getTabAt(0).setText("水费");
//        tabLayout.getTabAt(1).setText("电费");
//    }

    @OnClick({R.id.image_back, R.id.tv_chongzhi_jilu})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.image_back:
                finish();
                break;
            case R.id.tv_chongzhi_jilu:

                if(qiyeshuigid != null){//企业水费id
                    Intent intent = new Intent(ShuiDianFeiJiLuActivity.this, MyChongZhiJiLuActivity.class);
                    intent.putExtra("shuiid",qiyeshuigid);
                    startActivity(intent);
                }



                if(yudian != null){
                    Intent intent = new Intent(ShuiDianFeiJiLuActivity.this, MyChongZhiJiLuActivity.class);
                    intent.putExtra("yudian",yudian);
                    startActivity(intent);
                }




                if(menWeiShuiDianXinXiEntity != null){//员工水费id
                    if(menWeiShuiDianXinXiEntity.getResponse() != null){
                        if(menWeiShuiDianXinXiEntity.getResponse().getDian() != null){
                            if(menWeiShuiDianXinXiEntity.getResponse().getDian().getElectricityUserId() != null){
                                Intent intent = new Intent(ShuiDianFeiJiLuActivity.this, MyChongZhiJiLuActivity.class);
                                intent.putExtra("yuangongdian", menWeiShuiDianXinXiEntity.getResponse().getDian().getElectricityUserId());
                                startActivity(intent);
                            }
                        }
                    }
                }



                if(shuileixingid != null){
                    Intent intent = new Intent(ShuiDianFeiJiLuActivity.this, MyChongZhiJiLuActivity.class);
                    intent.putExtra("yuangongshuiid", shuileixingid);
                    startActivity(intent);
                }

//
//                if(menWeiShuiDianXinXiEntity != null){
//                    if(menWeiShuiDianXinXiEntity.getResponse() != null){
//                        if(menWeiShuiDianXinXiEntity.getResponse().getData().size() >= 2){
//                            if(menWeiShuiDianXinXiEntity.getResponse().getData().get(1).getWaterUserId() != null){
//                                Intent intent = new Intent(ShuiDianFeiJiLuActivity.this, MyChongZhiJiLuActivity.class);
//                                intent.putExtra("yuangongshuiid2", menWeiShuiDianXinXiEntity.getResponse().getData().get(1).getWaterUserId());
//                                startActivity(intent);
//                            }
//                        }
//                    }
//                }
//
//

                break;
        }
    }



    @Override
    public void onClick(View view) {
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        switch (view.getId()){
            case R.id.tv_shuifei:
                tvShuifei.setBackgroundResource(R.drawable.yuanqu_tongzi_white_background);
                tvShuifei.setTextColor(getResources().getColor(R.color.red_color));
                tvDianfei.setBackgroundResource(R.drawable.yuanqu_tongzi_red_background);
                tvDianfei.setTextColor(getResources().getColor(R.color.white_color));
                replaceFragment(shuiFragment, fragmentTransaction);
                break;
            case R.id.tv_dianfei:
                tvDianfei.setBackgroundResource(R.drawable.yuanqu_tongzi_white_background_right);
                tvDianfei.setTextColor(getResources().getColor(R.color.red_color));
                tvShuifei.setBackgroundResource(R.drawable.yuanqu_tongzi_red_background_left);
                tvShuifei.setTextColor(getResources().getColor(R.color.white_color));
                replaceFragment(dianFragment, fragmentTransaction);
                break;
        }
    }
}
