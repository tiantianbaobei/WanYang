package com.wanyangproject.my;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.wanyangproject.R;
import com.wanyangproject.entity.ChuanZhiEntity;
import com.wanyangproject.entity.DaiPingJiaEntity;
import com.wanyangproject.entity.DaiShouHuoEntity;
import com.wanyangproject.entity.PingJiaEntity;
import com.wanyangproject.entity.QuanBuEntity;
import com.wanyangproject.utils.ContractUtils;
import com.zhy.http.okhttp.OkHttpUtils;
import com.zhy.http.okhttp.callback.StringCallback;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.Call;

public class PingJiaActivity extends AppCompatActivity {

    @BindView(R.id.image_back)
    ImageView imageBack;
    @BindView(R.id.tv_fabu)
    TextView tvFabu;
    @BindView(R.id.image_shop)
    ImageView imageShop;
    @BindView(R.id.et_pingjia)
    EditText etPingjia;
    @BindView(R.id.image_shangjia_one)
    ImageView imageShangjiaOne;
    @BindView(R.id.image_shangjia_two)
    ImageView imageShangjiaTwo;
    @BindView(R.id.image_shangjia_three)
    ImageView imageShangjiaThree;
    @BindView(R.id.image_shangjia_four)
    ImageView imageShangjiaFour;
    @BindView(R.id.image_shangjia_five)
    ImageView imageShangjiaFive;
    @BindView(R.id.image_peisong_one)
    ImageView imagePeisongOne;
    @BindView(R.id.image_peisong_two)
    ImageView imagePeisongTwo;
    @BindView(R.id.image_peisong_three)
    ImageView imagePeisongThree;
    @BindView(R.id.image_peisong_four)
    ImageView imagePeisongFour;
    @BindView(R.id.image_peisong_five)
    ImageView imagePeisongFive;
    @BindView(R.id.tv_title)
    TextView tvTitle;
    private String dingdanhao;
    private String name;
    private DaiPingJiaEntity.ResponseBean responseBean;
    private QuanBuEntity.ResponseBean responseBean1;
    private int one;
    private int two;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ping_jia);
        ButterKnife.bind(this);
        Object object = ChuanZhiEntity.obj;
        if(object instanceof DaiPingJiaEntity.ResponseBean){
            responseBean = (DaiPingJiaEntity.ResponseBean) ChuanZhiEntity.obj;
        }else if(object instanceof  QuanBuEntity.ResponseBean){
            responseBean1 = (QuanBuEntity.ResponseBean) ChuanZhiEntity.obj;
        }




//        Intent intent = getIntent();
//        dingdanhao = intent.getStringExtra("dingdanhao");
//        name = intent.getStringExtra("name");
//        System.out.println(dingdanhao + "       去评价的订单号");
//        System.out.println(name + "       商店名称");

        if(responseBean != null){
            tvTitle.setText(responseBean.getShop_name());
        }


        if(responseBean1 != null){
            tvTitle.setText(responseBean1.getShop_name());
        }

    }


    //    用户评价商品的网络请求
    private void initYongHuPingJiaHttp() {
        String id  = "";

        if(responseBean.getGoods().size() == 1){
            id = responseBean.getGoods().get(0).getId();
            OkHttpUtils.post()
                    .url(ContractUtils.LOGIN_URL+"user/evaluate")
                    .addHeader("token",ContractUtils.getTOKEN(PingJiaActivity.this))
                    .addParams("parkId",ContractUtils.getParkId(PingJiaActivity.this))
                    .addParams("order_sn",responseBean.getOrder_sn())
                    .addParams("Distribution",one+"") //配送星级
                    .addParams("service",two+"") // 商家服务
                    .addParams("content",etPingjia.getText().toString().trim())
                    .addParams("goods_id",id)
                    .build()
                    .execute(new StringCallback() {
                        @Override
                        public void onError(Call call, Exception e, int id) {

                        }

                        @Override
                        public void onResponse(String response, int id) {
                            ContractUtils.Code500shibai(PingJiaActivity.this,response);
                            System.out.println(response+"      评价的网络请求cccccccc");
                            if(response.indexOf("200") != -1){
                                Gson gson = new Gson();
                                PingJiaEntity pingJiaEntity = gson.fromJson(response, PingJiaEntity.class);
                                Toast.makeText(PingJiaActivity.this, pingJiaEntity.getResponse().getMes(), Toast.LENGTH_SHORT).show();

                                Intent intent = new Intent();
                                intent.putExtra("pingjia", "pingjia");
                                intent.setAction("pingjia");
                                sendBroadcast(intent);
                                finish();
                            }else if(response.indexOf("400") != -1){
                                ContractUtils.Code400(PingJiaActivity.this,response);
                            }
                        }
                    });
        }else{
            OkHttpUtils.post()
                    .url(ContractUtils.LOGIN_URL+"user/evaluate")
                    .addHeader("token",ContractUtils.getTOKEN(PingJiaActivity.this))
                    .addParams("parkId",ContractUtils.getParkId(PingJiaActivity.this))
                    .addParams("order_sn",responseBean.getOrder_sn())
                    .addParams("Distribution",one+"") //配送星级
                    .addParams("service",two+"") // 商家服务
                    .addParams("content",etPingjia.getText().toString().trim())
                    .build()
                    .execute(new StringCallback() {
                        @Override
                        public void onError(Call call, Exception e, int id) {

                        }

                        @Override
                        public void onResponse(String response, int id) {
                            ContractUtils.Code500shibai(PingJiaActivity.this,response);
                            System.out.println(response+"      评价的网络请求aaaaaaa");
                            if(response.indexOf("200") != -1){
                                Gson gson = new Gson();
                                PingJiaEntity pingJiaEntity = gson.fromJson(response, PingJiaEntity.class);
                                Toast.makeText(PingJiaActivity.this, pingJiaEntity.getResponse().getMes(), Toast.LENGTH_SHORT).show();

                                Intent intent = new Intent();
                                intent.putExtra("pingjia", "pingjia");
                                intent.setAction("pingjia");
                                sendBroadcast(intent);
                                finish();
                            }else if(response.indexOf("400") != -1){
                                ContractUtils.Code400(PingJiaActivity.this,response);
                            }
                        }
                    });
        }
    }

















    //    用户评价商品的网络请求
    private void initYongHuPingJiaHttp111() {
        String id  = "";

        if(responseBean1.getGoods().size() == 1){
            id = responseBean1.getGoods().get(0).getId();
            OkHttpUtils.post()
                    .url(ContractUtils.LOGIN_URL+"user/evaluate")
                    .addHeader("token",ContractUtils.getTOKEN(PingJiaActivity.this))
                    .addParams("parkId",ContractUtils.getParkId(PingJiaActivity.this))
                    .addParams("order_sn",responseBean1.getOrder_sn())
                    .addParams("Distribution",one+"") //配送星级
                    .addParams("service",two+"") // 商家服务
                    .addParams("content",etPingjia.getText().toString().trim())
                    .addParams("goods_id",id)
                    .build()
                    .execute(new StringCallback() {
                        @Override
                        public void onError(Call call, Exception e, int id) {

                        }

                        @Override
                        public void onResponse(String response, int id) {
                            ContractUtils.Code500shibai(PingJiaActivity.this,response);
                            if(response.indexOf("200") != -1){
                                Gson gson = new Gson();
                                PingJiaEntity pingJiaEntity = gson.fromJson(response, PingJiaEntity.class);
                                Toast.makeText(PingJiaActivity.this, pingJiaEntity.getResponse().getMes(), Toast.LENGTH_SHORT).show();

                                Intent intent = new Intent();
                                intent.putExtra("pingjia", "pingjia");
                                intent.setAction("pingjia");
                                sendBroadcast(intent);
                                finish();
                            }else if(response.indexOf("400") != -1){
                                ContractUtils.Code400(PingJiaActivity.this,response);
                            }
                        }
                    });
        }else{
            OkHttpUtils.post()
                    .url(ContractUtils.LOGIN_URL+"user/evaluate")
                    .addHeader("token",ContractUtils.getTOKEN(PingJiaActivity.this))
                    .addParams("parkId",ContractUtils.getParkId(PingJiaActivity.this))
                    .addParams("order_sn",responseBean1.getOrder_sn())
                    .addParams("Distribution",one+"") //配送星级
                    .addParams("service",two+"") // 商家服务
                    .addParams("content",etPingjia.getText().toString().trim())
                    .build()
                    .execute(new StringCallback() {
                        @Override
                        public void onError(Call call, Exception e, int id) {

                        }

                        @Override
                        public void onResponse(String response, int id) {
                            ContractUtils.Code500shibai(PingJiaActivity.this,response);
                            System.out.println(response+"      评价的网络请求");
                            if(response.indexOf("200") != -1){

                                Gson gson = new Gson();
                                PingJiaEntity pingJiaEntity = gson.fromJson(response, PingJiaEntity.class);
                                Toast.makeText(PingJiaActivity.this, pingJiaEntity.getResponse().getMes(), Toast.LENGTH_SHORT).show();

                                Intent intent = new Intent();
                                intent.putExtra("pingjia", "pingjia");
                                intent.setAction("pingjia");
                                sendBroadcast(intent);
                                finish();
                            }else if(response.indexOf("400") != -1){
                                ContractUtils.Code400(PingJiaActivity.this,response);
                            }
                        }
                    });
        }
    }











    @OnClick({R.id.image_back, R.id.tv_fabu, R.id.image_shop, R.id.et_pingjia, R.id.image_shangjia_one, R.id.image_shangjia_two,
            R.id.image_shangjia_three, R.id.image_shangjia_four, R.id.image_shangjia_five, R.id.image_peisong_one, R.id.image_peisong_two,
            R.id.image_peisong_three, R.id.image_peisong_four, R.id.image_peisong_five})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.image_back:
                finish();
                break;
//            发布
            case R.id.tv_fabu:
                if(responseBean != null){
                    if(etPingjia.getText().toString().trim().equals("")){
                        Toast.makeText(this, "请输入评价内容", Toast.LENGTH_SHORT).show();
                    }else{
                        //        用户评价商品的网络请求
                        initYongHuPingJiaHttp();
                    }

                }else if(responseBean1 != null){
                    if(etPingjia.getText().toString().trim().equals("")){
                        Toast.makeText(this, "请输入评价内容", Toast.LENGTH_SHORT).show();
                    }else{
                        //        用户评价商品的网络请求
                        initYongHuPingJiaHttp111();
                    }

                }

                break;
            case R.id.image_shop:
                break;
            case R.id.et_pingjia:
                break;
            case R.id.image_shangjia_one:
                isOK(0);
                break;
            case R.id.image_shangjia_two:
                isOK(1);
                break;
            case R.id.image_shangjia_three:
                isOK(2);
                break;
            case R.id.image_shangjia_four:
                isOK(3);
                break;
            case R.id.image_shangjia_five:
                isOK(4);
                break;
            case R.id.image_peisong_one:
                isit(0);
                break;
            case R.id.image_peisong_two:
                isit(1);
                break;
            case R.id.image_peisong_three:
                isit(2);
                break;
            case R.id.image_peisong_four:
                isit(3);
                break;
            case R.id.image_peisong_five:
                isit(4);
                break;
        }
    }


    private void isOK(int ttt) {
        one = ttt+1;
        ArrayList<ImageView> list = new ArrayList();
        list.add(imageShangjiaOne);
        list.add(imageShangjiaTwo);
        list.add(imageShangjiaThree);
        list.add(imageShangjiaFour);
        list.add(imageShangjiaFive);

        for (int i = 0; i < list.size(); i++) {
            ImageView sss = list.get(i);
            if (i <= ttt) {
                sss.setImageResource(R.drawable.xing_true);
            } else {
                sss.setImageResource(R.drawable.xing_false);
            }
        }
    }


    private void isit(int ttt) {
        two = ttt+1;
        ArrayList<ImageView> list = new ArrayList();
        list.add(imagePeisongOne);
        list.add(imagePeisongTwo);
        list.add(imagePeisongThree);
        list.add(imagePeisongFour);
        list.add(imagePeisongFive);

        for (int i = 0; i < list.size(); i++) {
            ImageView sss = list.get(i);
            if (i <= ttt) {
                sss.setImageResource(R.drawable.xing_true);
            } else {
                sss.setImageResource(R.drawable.xing_false);
            }
        }
    }
}
