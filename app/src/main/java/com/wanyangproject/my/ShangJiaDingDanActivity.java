package com.wanyangproject.my;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;

import com.androidkun.xtablayout.XTabLayout;
import com.wanyangproject.R;
import com.wanyangproject.adapter.MyPagerAdapter;
import com.wanyangproject.mysuipian.DaiPeiSongFragment;
import com.wanyangproject.mysuipian.DaiTuiKuanFragment;
import com.wanyangproject.mysuipian.YiJieDanFragment;
import com.wanyangproject.mysuipian.YiWanChengFragment;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ShangJiaDingDanActivity extends AppCompatActivity {

    @BindView(R.id.image_back)
    ImageView imageBack;
    //    @BindView(R.id.tabLayout)
//    TabLayout tabLayout;
    @BindView(R.id.viewPager)
    ViewPager viewPager;
    //    @BindView(R.id.viewPager)
//    NoScrollViewPager viewPager;
    List<Fragment> fragments = new ArrayList<>();
    @BindView(R.id.tabLayout)
    XTabLayout tabLayout;
    private YiJieDanFragment yiJieDanFragment;
    private DaiPeiSongFragment daiPeiSongFragment;
    private YiWanChengFragment yiWanChengFragment;
    private DaiTuiKuanFragment daiTuiKuanFragment;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shang_jia_ding_dan);
        ButterKnife.bind(this);
        initPager();
    }



    private void initPager() {

        yiJieDanFragment=new YiJieDanFragment();
        daiPeiSongFragment=new DaiPeiSongFragment();
        yiWanChengFragment=new YiWanChengFragment();
        daiTuiKuanFragment=new DaiTuiKuanFragment();
        fragments.add(yiJieDanFragment);
        fragments.add(daiPeiSongFragment);
        fragments.add(yiWanChengFragment);
        fragments.add(daiTuiKuanFragment);
        // 创建ViewPager适配器
        MyPagerAdapter myPagerAdapter = new MyPagerAdapter(getSupportFragmentManager());
        myPagerAdapter.setFragments(fragments);
        // 给ViewPager设置适配器
        viewPager.setAdapter(myPagerAdapter);
        // TabLayout 指示器 (记得自己手动创建4个Fragment,注意是 app包下的Fragment 还是 V4包下的 Fragment)
        tabLayout.addTab(tabLayout.newTab());
        tabLayout.addTab(tabLayout.newTab());
        tabLayout.addTab(tabLayout.newTab());
        tabLayout.addTab(tabLayout.newTab());
        // 使用 TabLayout 和 ViewPager 相关联
        tabLayout.setupWithViewPager(viewPager);
        // TabLayout指示器添加文本
        tabLayout.getTabAt(0).setText("已接单");
        tabLayout.getTabAt(1).setText("待配送");
        tabLayout.getTabAt(2).setText("已完成");
        tabLayout.getTabAt(3).setText("待退款");

        Intent intent = getIntent();
        String stringExtra = intent.getStringExtra("shangjiadingdan");
        int i = Integer.parseInt(stringExtra);

        viewPager.setCurrentItem(i);
        if (i==0){
            yiJieDanFragment.initShangJiaDingDanXianShiHttp();
        }else if (i==1){
            daiPeiSongFragment.initShangJiaDingDanXianShiHttp();
        }else if (i==2){
            yiWanChengFragment.initShangJiaDingDanXianShiHttp();
        }else if (i==3){
            daiTuiKuanFragment.initShangJiaDingDanXianShiHttp();
        }


        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                if (position==0){
                    yiJieDanFragment.initShangJiaDingDanXianShiHttp();
                }else if (position==1){
                    daiPeiSongFragment.initShangJiaDingDanXianShiHttp();
                }else if (position==2){
                    yiWanChengFragment.initShangJiaDingDanXianShiHttp();
                }else if (position==3){
                    daiTuiKuanFragment.initShangJiaDingDanXianShiHttp();
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }


    @OnClick({R.id.image_back, R.id.tabLayout, R.id.viewPager})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.image_back:
                finish();
                break;
            case R.id.tabLayout:
                break;
            case R.id.viewPager:
                break;
        }
    }
}
