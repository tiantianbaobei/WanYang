package com.wanyangproject.my;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.wanyangproject.R;
import com.wanyangproject.entity.DingDanXiangQingEntity;
import com.wanyangproject.entity.ShanChuDingDanEntity;
import com.wanyangproject.myadapter.FinishXiangQingAdapter;
import com.wanyangproject.utils.ContractUtils;
import com.zhy.http.okhttp.OkHttpUtils;
import com.zhy.http.okhttp.callback.StringCallback;

import java.text.SimpleDateFormat;
import java.util.Date;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.Call;

public class FinishXiangQingActivity extends AppCompatActivity {


    @BindView(R.id.relative_rl)
    RelativeLayout relativeRl;
    @BindView(R.id.image_back)
    ImageView imageBack;
    @BindView(R.id.relative_title)
    RelativeLayout relativeTitle;
    @BindView(R.id.image_dingwei)
    ImageView imageDingwei;
    @BindView(R.id.tv_shouhuoren)
    TextView tvShouhuoren;
    @BindView(R.id.tv_shoujihao)
    TextView tvShoujihao;
    @BindView(R.id.tv_address)
    TextView tvAddress;
    @BindView(R.id.tv_title)
    TextView tvTitle;
    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    @BindView(R.id.tv_peisongfei)
    TextView tvPeisongfei;
    @BindView(R.id.tv_beizhu)
    TextView tvBeizhu;
    @BindView(R.id.tv_dingdan_bianhao)
    TextView tvDingdanBianhao;
    @BindView(R.id.tv_chuanjianshijian)
    TextView tvChuanjianshijian;
    @BindView(R.id.tv_zhifu_shijian)
    TextView tvZhifuShijian;
    @BindView(R.id.tv_fahuo_shijian)
    TextView tvFahuoShijian;
    @BindView(R.id.tv_shouhuo_shijian)
    TextView tvShouhuoShijian;
    @BindView(R.id.tv_heji_money)
    TextView tvHejiMoney;
    @BindView(R.id.order_detail_bottom)
    LinearLayout orderDetailBottom;
    @BindView(R.id.relative_ziqu)
    RelativeLayout relativeZiqu;
    @BindView(R.id.relative_peisong)
    RelativeLayout relativePeisong;
    @BindView(R.id.tv_delete)
    TextView tvDelete;
    @BindView(R.id.tv_tuikuan)
    TextView tvTuikuan;
    @BindView(R.id.tv_moren)
    TextView tvMoren;
    @BindView(R.id.tv_tuikuan_shijian)
    TextView tvTuikuanShijian;
    private FinishXiangQingAdapter finishXiangQingAdapter;
    private String dingdanhao;
    private DingDanXiangQingEntity dingDanXiangQingEntity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_finish_xiang_qing);
        ButterKnife.bind(this);
        initView();

        Intent intent = getIntent();
        dingdanhao = intent.getStringExtra("dingdanhao");
        System.out.println(dingdanhao + "        详情接收的订单号");

        //        订单详情的网络请求
        initXiangQIngHttp();

    }


    //    订单详情的网络请求
    private void initXiangQIngHttp() {
        OkHttpUtils.post()
                .url(ContractUtils.LOGIN_URL + "user/orderdetails")
                .addHeader("token", ContractUtils.getTOKEN(FinishXiangQingActivity.this))
                .addParams("parkId", ContractUtils.getParkId(FinishXiangQingActivity.this))
                .addParams("order_sn", dingdanhao)
                .build()
                .execute(new StringCallback() {
                    @Override
                    public void onError(Call call, Exception e, int id) {

                    }

                    @Override
                    public void onResponse(String response, int id) {
                        ContractUtils.Code500shibai(FinishXiangQingActivity.this, response);
                        System.out.println(response + "        订单详情的网络请求");

                        if (response.indexOf("200") != -1) {
                            Gson gson = new Gson();
                            dingDanXiangQingEntity = gson.fromJson(response, DingDanXiangQingEntity.class);
                            finishXiangQingAdapter = new FinishXiangQingAdapter(FinishXiangQingActivity.this, dingDanXiangQingEntity.getResponse().getGoods());
                            LinearLayoutManager manager = new LinearLayoutManager(FinishXiangQingActivity.this);
                            recyclerView.setLayoutManager(manager);
                            recyclerView.setAdapter(finishXiangQingAdapter);


                            if(dingDanXiangQingEntity.getResponse().getReason_time() != null){
                                if (dingDanXiangQingEntity.getResponse().getReason_time().equals("")) {
                                    tvTuikuanShijian.setVisibility(View.GONE);
                                } else {
                                    tvTuikuanShijian.setVisibility(View.VISIBLE);
                                    tvTuikuanShijian.setText("退款时间："+dingDanXiangQingEntity.getResponse().getReason_time());
                                }
                            }


//                            备注
                            if (dingDanXiangQingEntity.getResponse().getRemarks() != null) {
                                tvBeizhu.setText("备注：" + dingDanXiangQingEntity.getResponse().getRemarks());
                            }


                            if (dingDanXiangQingEntity.getResponse().getAddr_type().equals("1")) {
                                tvMoren.setVisibility(View.VISIBLE);
                            } else if (dingDanXiangQingEntity.getResponse().getAddr_type().equals("2")) {
                                tvMoren.setVisibility(View.GONE);
                            }


                            if (dingDanXiangQingEntity.getResponse().getType().equals("12")) {
                                tvTuikuan.setVisibility(View.VISIBLE);
                                tvTuikuan.setText("已退款");
                                tvTuikuan.setTextColor(Color.parseColor("#ff0000"));
                            }


                            if (dingDanXiangQingEntity.getResponse().getAddres_type().equals("1")) {
                                relativePeisong.setVisibility(View.VISIBLE);
                                relativeZiqu.setVisibility(View.GONE);
                                if (dingDanXiangQingEntity.getResponse().getAddres_name() == null) {

                                } else {
                                    tvShouhuoren.setText("收货人：" + dingDanXiangQingEntity.getResponse().getAddres_name());
                                }


                                if (dingDanXiangQingEntity.getResponse().getAddres_phone() == null) {

                                } else {
                                    tvShoujihao.setText(dingDanXiangQingEntity.getResponse().getAddres_phone());
                                }

                                if (dingDanXiangQingEntity.getResponse().getAddres_add() == null) {

                                } else {
                                    tvAddress.setText("收货地址：" + dingDanXiangQingEntity.getResponse().getAddres_add());
                                }

                            }

                            if (dingDanXiangQingEntity.getResponse().getAddres_type().equals("2")) {
                                relativeZiqu.setVisibility(View.VISIBLE);
                                relativePeisong.setVisibility(View.GONE);
                            }


//                            if (dingDanXiangQingEntity.getResponse().getAddres_name() == null) {
//
//                            } else {
//                                tvShouhuoren.setText(dingDanXiangQingEntity.getResponse().getAddres_name());
//                            }
//
//
//                            if (dingDanXiangQingEntity.getResponse().getAddres_phone() == null) {
//
//                            } else {
//                                tvShoujihao.setText(dingDanXiangQingEntity.getResponse().getAddres_phone());
//                            }
//
//                            if (dingDanXiangQingEntity.getResponse().getAddres_add() == null) {
//
//                            } else {
//                                tvAddress.setText(dingDanXiangQingEntity.getResponse().getAddres_add());
//                            }


                            if (dingDanXiangQingEntity.getResponse().getShop().getCost() == null) {

                            } else {
                                tvPeisongfei.setText("¥" + dingDanXiangQingEntity.getResponse().getShop().getCost());
                            }


//                            备注


                            if (dingDanXiangQingEntity.getResponse().getOrder_sn() == null) {

                            } else {
                                tvDingdanBianhao.setText("订单编号：" + dingDanXiangQingEntity.getResponse().getOrder_sn());
                            }


                            if (dingDanXiangQingEntity.getResponse().getAdd_time() == null) {

                            } else {
                                if (dingDanXiangQingEntity.getResponse().getAdd_time().equals("")) {
                                    tvChuanjianshijian.setVisibility(View.GONE);
                                } else {
                                    tvChuanjianshijian.setText("创建时间：" + dingDanXiangQingEntity.getResponse().getAdd_time());
                                }

                            }


                            if (dingDanXiangQingEntity.getResponse().getPay_time() == null) {

                            } else {
                                if (dingDanXiangQingEntity.getResponse().getPay_time().equals("")) {
                                    tvZhifuShijian.setVisibility(View.GONE);
                                } else {
                                    tvZhifuShijian.setText("支付时间：" + dingDanXiangQingEntity.getResponse().getPay_time());
                                }

                            }


                            if (dingDanXiangQingEntity.getResponse().getFahuo_time() == null) {

                            } else {
                                if (dingDanXiangQingEntity.getResponse().getFahuo_time().equals("")) {
                                    tvFahuoShijian.setVisibility(View.GONE);
                                } else {
                                    tvFahuoShijian.setText("发货时间：" + dingDanXiangQingEntity.getResponse().getFahuo_time());
                                }
                            }


                            if (dingDanXiangQingEntity.getResponse().getShouhuo_time() == null) {

                            } else {
                                if (dingDanXiangQingEntity.getResponse().getShouhuo_time().equals("")) {
                                    tvShouhuoShijian.setVisibility(View.GONE);
                                } else {
                                    tvShouhuoShijian.setText("收货时间：" + dingDanXiangQingEntity.getResponse().getShouhuo_time());
                                }

                            }


                            if (dingDanXiangQingEntity.getResponse().getShop().getNickname() == null) {

                            } else {
                                tvTitle.setText(dingDanXiangQingEntity.getResponse().getShop().getNickname());
                            }


                            if (dingDanXiangQingEntity.getResponse().getMoney() == null) {

                            } else {
                                tvHejiMoney.setText("¥" + dingDanXiangQingEntity.getResponse().getMoney());
                            }

                        }
                    }
                });
    }


    /*
* 将时间戳转换为时间
*/
    public String stampToDate(String s) {
        String res;
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        long lt = new Long(s);
        Date date = new Date(lt);
        res = simpleDateFormat.format(date);
        return res;
    }

    private void initView() {
//        finishXiangQingAdapter = new FinishXiangQingAdapter(this);
//        LinearLayoutManager manager = new LinearLayoutManager(this);
//        recyclerView.setLayoutManager(manager);
//        recyclerView.setAdapter(finishXiangQingAdapter);
    }

    @OnClick({R.id.image_back, R.id.recyclerView, R.id.tv_heji_money, R.id.tv_delete})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.image_back:
                finish();
                break;
            case R.id.recyclerView:
                break;
            case R.id.tv_heji_money:
                break;
//            删除按钮
            case R.id.tv_delete:
                //                删除待支付订单的网络请求
                initShanChuHttp();
                break;
        }
    }


    //    删除待支付订单的网络请求
    private void initShanChuHttp() {
        OkHttpUtils.post()
                .url(ContractUtils.LOGIN_URL + "user/orderdel")
                .addHeader("token", ContractUtils.getTOKEN(FinishXiangQingActivity.this))
                .addParams("parkId", ContractUtils.getParkId(FinishXiangQingActivity.this))
                .addParams("id", dingdanhao)
                .build()
                .execute(new StringCallback() {
                    @Override
                    public void onError(Call call, Exception e, int id) {

                    }

                    @Override
                    public void onResponse(String response, int id) {
                        System.out.println(response + "             删除完成订单的网络请求");
                        ContractUtils.Code500(FinishXiangQingActivity.this, response);
                        if (response.indexOf("200") != -1) {
                            Gson gson = new Gson();
                            ShanChuDingDanEntity shanChuDingDanEntity = gson.fromJson(response, ShanChuDingDanEntity.class);
                            Toast.makeText(FinishXiangQingActivity.this, "删除成功！", Toast.LENGTH_SHORT).show();
                            finish();
                        } else {
                            ContractUtils.Code400(FinishXiangQingActivity.this, response);
                        }
                    }
                });
    }


}
