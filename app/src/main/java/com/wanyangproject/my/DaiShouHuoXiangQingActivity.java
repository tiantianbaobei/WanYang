package com.wanyangproject.my;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.wanyangproject.R;
import com.wanyangproject.entity.DingDanXiangQingEntity;
import com.wanyangproject.entity.QueRenShouHuoEntity;
import com.wanyangproject.entity.QueRenShouHuoShiBaiEntity;
import com.wanyangproject.myadapter.DaiShouHuoXiangQingAdapter;
import com.wanyangproject.utils.ContractUtils;
import com.zhy.http.okhttp.OkHttpUtils;
import com.zhy.http.okhttp.callback.StringCallback;

import java.text.SimpleDateFormat;
import java.util.Date;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.Call;

public class DaiShouHuoXiangQingActivity extends AppCompatActivity {

    @BindView(R.id.image_back)
    ImageView imageBack;
    @BindView(R.id.image_dingwei)
    ImageView imageDingwei;
    @BindView(R.id.tv_shouhuoren)
    TextView tvShouhuoren;
    @BindView(R.id.tv_shoujihao)
    TextView tvShoujihao;
    @BindView(R.id.tv_address)
    TextView tvAddress;
    @BindView(R.id.image_shop)
    ImageView imageShop;
    @BindView(R.id.tv_title)
    TextView tvTitle;
    @BindView(R.id.tv_tuikuan)
    TextView tvTuikuan;
    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    @BindView(R.id.tv_peisongfei)
    TextView tvPeisongfei;
    @BindView(R.id.tv_beizhu)
    TextView tvBeizhu;
    @BindView(R.id.tv_dingdan_bianhao)
    TextView tvDingdanBianhao;
    @BindView(R.id.tv_chuanjianshijian)
    TextView tvChuanjianshijian;
    @BindView(R.id.tv_zhifu_shijian)
    TextView tvZhifuShijian;
    @BindView(R.id.tv_fahuo_shijian)
    TextView tvFahuoShijian;
    @BindView(R.id.tv_heji_money)
    TextView tvHejiMoney;
    @BindView(R.id.btn_shouhuo)
    Button btnShouhuo;
    @BindView(R.id.relative_ziqu)
    RelativeLayout relativeZiqu;
    @BindView(R.id.relative_peisong)
    RelativeLayout relativePeisong;
    @BindView(R.id.tv_shenhe)
    TextView tvShenhe;
    @BindView(R.id.tv_moren)
    TextView tvMoren;
    @BindView(R.id.tv_tuikuan_shijian)
    TextView tvTuikuanShijian;
    private DaiShouHuoXiangQingAdapter daiShouHuoXiangQingAdapter;
    private String dingdanhao;
    private DingDanXiangQingEntity dingDanXiangQingEntity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dai_shou_huo_xiang_qing);
        ButterKnife.bind(this);

        Intent intent = getIntent();
        dingdanhao = intent.getStringExtra("dingdanhao");
        System.out.println(dingdanhao + "        详情接收的订单号");

        initView();

        //        订单详情的网络请求
        initXiangQIngHttp();

    }


    //    订单详情的网络请求
    private void initXiangQIngHttp() {
        OkHttpUtils.post()
                .url(ContractUtils.LOGIN_URL + "user/orderdetails")
                .addHeader("token", ContractUtils.getTOKEN(DaiShouHuoXiangQingActivity.this))
                .addParams("parkId", ContractUtils.getParkId(DaiShouHuoXiangQingActivity.this))
                .addParams("order_sn", dingdanhao)
                .build()
                .execute(new StringCallback() {
                    @Override
                    public void onError(Call call, Exception e, int id) {

                    }

                    @Override
                    public void onResponse(String response, int id) {
                        ContractUtils.Code500shibai(DaiShouHuoXiangQingActivity.this, response);
                        System.out.println(response + "        订单详情的网络请求");

                        if (response.indexOf("200") != -1) {
                            Gson gson = new Gson();
                            dingDanXiangQingEntity = gson.fromJson(response, DingDanXiangQingEntity.class);
//                            daiZhiFuXiangQingAdapter = new DaiZhiFuXiangQingAdapter(DaiShouHuoXiangQingActivity.this, dingDanXiangQingEntity.getResponse().getGoods());
//                            LinearLayoutManager manager = new LinearLayoutManager(DaiShouHuoXiangQingActivity.this);
//                            recyclerView.setLayoutManager(manager);
//                            recyclerView.setAdapter(daiZhiFuXiangQingAdapter);

                            daiShouHuoXiangQingAdapter = new DaiShouHuoXiangQingAdapter(DaiShouHuoXiangQingActivity.this, dingDanXiangQingEntity.getResponse().getGoods());
                            LinearLayoutManager manager = new LinearLayoutManager(DaiShouHuoXiangQingActivity.this);
                            recyclerView.setLayoutManager(manager);
                            recyclerView.setAdapter(daiShouHuoXiangQingAdapter);


//                            备注
                            if (dingDanXiangQingEntity.getResponse().getRemarks() != null) {
                                tvBeizhu.setText("备注：" + dingDanXiangQingEntity.getResponse().getRemarks());
                            }


                            if (dingDanXiangQingEntity.getResponse().getAddr_type().equals("1")) {
                                tvMoren.setVisibility(View.VISIBLE);
                            } else if (dingDanXiangQingEntity.getResponse().getAddr_type().equals("2")) {
                                tvMoren.setVisibility(View.GONE);
                            }


                            daiShouHuoXiangQingAdapter.setTuiKuanClick(new DaiShouHuoXiangQingAdapter.TuiKuanClick() {
                                @Override
                                public void TuiKuanclick(int position) {
                                    Intent intent = new Intent(DaiShouHuoXiangQingActivity.this, TuiKuanActivity.class);
                                    startActivity(intent);
                                }
                            });


                            if (dingDanXiangQingEntity.getResponse().getType().equals("11")) {
                                tvTuikuan.setVisibility(View.GONE);
                                tvShenhe.setVisibility(View.VISIBLE);
                                tvShenhe.setText("请耐心等待商家退款");
                                tvShenhe.setTextColor(Color.parseColor("#ff0000"));
                                btnShouhuo.setVisibility(View.GONE);
//                                tvTuikuan.setOnClickListener(new View.OnClickListener() {
//                                    @Override
//                                    public void onClick(View view) {
//                                        return;
//                                    }
//                                });
                            }


                            if (dingDanXiangQingEntity.getResponse().getAddres_type().equals("1")) {
                                relativePeisong.setVisibility(View.VISIBLE);
                                relativeZiqu.setVisibility(View.GONE);
                                if (dingDanXiangQingEntity.getResponse().getAddres_name() == null) {

                                } else {
                                    tvShouhuoren.setText("收货人：" + dingDanXiangQingEntity.getResponse().getAddres_name());
                                }


                                if (dingDanXiangQingEntity.getResponse().getAddres_phone() == null) {

                                } else {
                                    tvShoujihao.setText(dingDanXiangQingEntity.getResponse().getAddres_phone());
                                }

                                if (dingDanXiangQingEntity.getResponse().getAddres_add() == null) {

                                } else {
                                    tvAddress.setText("收货地址：" + dingDanXiangQingEntity.getResponse().getAddres_add());
                                }

                            }

                            if (dingDanXiangQingEntity.getResponse().getAddres_type().equals("2")) {
                                relativeZiqu.setVisibility(View.VISIBLE);
                                relativePeisong.setVisibility(View.GONE);
                            }


//                            if (dingDanXiangQingEntity.getResponse().getAddres_name() == null) {
//
//                            } else {
//                                tvShouhuoren.setText(dingDanXiangQingEntity.getResponse().getAddres_name());
//                            }
//
//
//                            if (dingDanXiangQingEntity.getResponse().getAddres_phone() == null) {
//
//                            } else {
//                                tvShoujihao.setText(dingDanXiangQingEntity.getResponse().getAddres_phone());
//                            }
//
//                            if (dingDanXiangQingEntity.getResponse().getAddres_add() == null) {
//
//                            } else {
//                                tvAddress.setText(dingDanXiangQingEntity.getResponse().getAddres_add());
//                            }


                            if (dingDanXiangQingEntity.getResponse().getShop().getCost() == null) {

                            } else {
                                tvPeisongfei.setText("¥" + dingDanXiangQingEntity.getResponse().getShop().getCost());
                            }


                            if (dingDanXiangQingEntity.getResponse().getOrder_sn() == null) {

                            } else {
                                tvDingdanBianhao.setText("订单编号：" + dingDanXiangQingEntity.getResponse().getOrder_sn());
                            }


                            if (dingDanXiangQingEntity.getResponse().getAdd_time() == null) {

                            } else {
                                tvChuanjianshijian.setText("创建时间：" + dingDanXiangQingEntity.getResponse().getAdd_time());
                            }


                            if (dingDanXiangQingEntity.getResponse().getPay_time() == null) {

                            } else {
                                if (dingDanXiangQingEntity.getResponse().getPay_time().equals("")) {
                                    tvZhifuShijian.setVisibility(View.GONE);
                                } else {
                                    tvZhifuShijian.setText("支付时间：" + dingDanXiangQingEntity.getResponse().getPay_time());
                                }

                            }


                            if (dingDanXiangQingEntity.getResponse().getFahuo_time() == null) {

                            } else {
                                if (dingDanXiangQingEntity.getResponse().getFahuo_time().equals("")) {
                                    tvFahuoShijian.setVisibility(View.GONE);
                                } else {
                                    tvFahuoShijian.setText("发货时间：" + dingDanXiangQingEntity.getResponse().getFahuo_time());
                                }
                            }



//                            if(dingDanXiangQingEntity.getResponse().getReason_time().equals("")){
//                                tvTuikuanShijian.setVisibility(View.GONE);
//                            }else{
//                                tvTuikuanShijian.setVisibility(View.VISIBLE);
//                            }
//

                            if(dingDanXiangQingEntity.getResponse().getReason_time() != null){
                                if (dingDanXiangQingEntity.getResponse().getReason_time().equals("")) {
                                    tvTuikuanShijian.setVisibility(View.GONE);
                                } else {
                                    tvTuikuanShijian.setVisibility(View.VISIBLE);
                                    tvTuikuanShijian.setText("退款时间："+dingDanXiangQingEntity.getResponse().getReason_time());
                                }
                            }




                            if (dingDanXiangQingEntity.getResponse().getShop().getNickname() == null) {

                            } else {
                                tvTitle.setText(dingDanXiangQingEntity.getResponse().getShop().getNickname());
                            }



                            if (dingDanXiangQingEntity.getResponse().getMoney() == null) {

                            } else {
                                tvHejiMoney.setText("¥" + dingDanXiangQingEntity.getResponse().getMoney());
                            }
                        }
                    }
                });
    }


    /*
* 将时间戳转换为时间
 */
    public String stampToDate(String s) {
        String res;
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        long lt = new Long(s);
        Date date = new Date(lt);
        res = simpleDateFormat.format(date);
        return res;
    }


    private void initView() {
//        daiShouHuoXiangQingAdapter = new DaiShouHuoXiangQingAdapter(this);
//        LinearLayoutManager manager = new LinearLayoutManager(this);
//        recyclerView.setLayoutManager(manager);
//        recyclerView.setAdapter(daiShouHuoXiangQingAdapter);
//
//        daiShouHuoXiangQingAdapter.setTuiKuanClick(new DaiShouHuoXiangQingAdapter.TuiKuanClick() {
//            @Override
//            public void TuiKuanclick(int position) {
//                Intent intent = new Intent(DaiShouHuoXiangQingActivity.this,TuiKuanActivity.class);
//                startActivity(intent);
//            }
//        });

    }

    @OnClick({R.id.image_back, R.id.recyclerView, R.id.tv_heji_money, R.id.btn_shouhuo, R.id.tv_tuikuan})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.image_back:
                finish();
                break;
            case R.id.tv_tuikuan:
                if (dingdanhao == null) {
                    return;
                }
                Intent intent = new Intent(DaiShouHuoXiangQingActivity.this, TuiKuanActivity.class);
                intent.putExtra("dingdanhao", dingdanhao);// 订单号
//                ChuanZhiEntity.obj = dingDanXiangQingEntity.getResponse();// 传值
                startActivity(intent);
                finish();
//                Intent intent = new Intent(DaiShouHuoXiangQingActivity.this,TuiKuanActivity.class);
//                startActivity(intent);
                break;
            case R.id.recyclerView:
                break;
            case R.id.tv_heji_money:
                break;
            case R.id.btn_shouhuo:
//                Intent intent1 = new Intent(DaiShouHuoXiangQingActivity.this, FinishXiangQingActivity.class);
//                startActivity(intent1);

                //           确认收货的网络请求
                initQueRenShouHuoHttp(dingdanhao);


                break;
        }
    }


    //    确认收货的网络请求
    private void initQueRenShouHuoHttp(String dingdanhao) {
        OkHttpUtils.post()
                .url(ContractUtils.LOGIN_URL + "user/userrefuse")
                .addHeader("token", ContractUtils.getTOKEN(DaiShouHuoXiangQingActivity.this))
                .addParams("parkId", ContractUtils.getParkId(DaiShouHuoXiangQingActivity.this))
                .addParams("order_sn", dingdanhao)
                .build()
                .execute(new StringCallback() {
                    @Override
                    public void onError(Call call, Exception e, int id) {

                    }

                    @Override
                    public void onResponse(String response, int id) {
                        ContractUtils.Code500shibai(DaiShouHuoXiangQingActivity.this, response);
                        System.out.println(response + "          用户确认收货的网络请求");
                        if (response.indexOf("200") != -1) {
                            Gson gson = new Gson();
                            QueRenShouHuoEntity queRenShouHuoEntity = gson.fromJson(response, QueRenShouHuoEntity.class);
                            Toast.makeText(DaiShouHuoXiangQingActivity.this, queRenShouHuoEntity.getResponse().getMes(), Toast.LENGTH_SHORT).show();

                            Intent intent = new Intent();
                            intent.putExtra("shouhuo", "shouhuo");
                            intent.setAction("shouhuo");
                            sendBroadcast(intent);

//                            //        订单详情的网络请求
//                            initXiangQIngHttp();
                            finish();

//                            Intent intent = new Intent(getContext(), FinishXiangQingActivity.class);
//                            startActivity(intent);
//                            finish();
                        } else if (response.indexOf("400") != -1) {
                            Gson gson = new Gson();
                            QueRenShouHuoShiBaiEntity queRenShouHuoShiBaiEntity = gson.fromJson(response, QueRenShouHuoShiBaiEntity.class);
                            Toast.makeText(DaiShouHuoXiangQingActivity.this, queRenShouHuoShiBaiEntity.getMsg(), Toast.LENGTH_SHORT).show();
                            finish();
                        }
                    }
                });
    }


}
