package com.wanyangproject.my;

import android.app.ProgressDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.wanyangproject.R;
import com.wanyangproject.activity.YuanQuZhaoShangYiJianFanKuiActivity;
import com.wanyangproject.entity.LiuYanFanKuiEntity;
import com.wanyangproject.utils.ContractUtils;
import com.zhy.http.okhttp.OkHttpUtils;
import com.zhy.http.okhttp.callback.StringCallback;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.Call;

public class SettingYiJianFanKuiActivity extends AppCompatActivity {


    @BindView(R.id.image_back)
    ImageView imageBack;
    @BindView(R.id.et_jianyi)
    EditText etJianyi;
    @BindView(R.id.btn_tijiao)
    Button btnTijiao;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setting_yi_jian_fan_kui);
        ButterKnife.bind(this);
    }





    //    个人中心的意见反馈的网络请求
    private void initYuanQuZhaShangYiJianFanKuiHttp() {
        final ProgressDialog progressDialog = new ProgressDialog(SettingYiJianFanKuiActivity.this);
        progressDialog.setTitle("提示");
        progressDialog.setMessage("请等待...");
        progressDialog.setCancelable(false);
        progressDialog.show();
        OkHttpUtils.post()
                .url(ContractUtils.LOGIN_URL+"news/feedback")
                .addHeader("token",ContractUtils.getTOKEN(SettingYiJianFanKuiActivity.this))
                .addParams("content",etJianyi.getText().toString().trim())
                .addParams("parkId",ContractUtils.getParkId(SettingYiJianFanKuiActivity.this))
                .addParams("typeId","0")//1:园区招商，2：企服中心，3:政策法规,  0:设置
                .build()
                .execute(new StringCallback() {
                    @Override
                    public void onError(Call call, Exception e, int id) {
                        System.out.println(e+"     园区招商意见反馈eee");
                        progressDialog.dismiss();
                    }

                    @Override
                    public void onResponse(String response, int id) {
                        ContractUtils.Code500(SettingYiJianFanKuiActivity.this,response);
                        System.out.println(response+"    个人中心的意见反馈的网络请求 ");
                        progressDialog.dismiss();
                        if(response.indexOf("200") != -1){
                            Gson gson = new Gson();
                            LiuYanFanKuiEntity yiJianFanKuiEntity = gson.fromJson(response, LiuYanFanKuiEntity.class);
                            Toast.makeText(SettingYiJianFanKuiActivity.this, "发表成功", Toast.LENGTH_SHORT).show();
                            finish();
                        }else{
//                            ContractUtils.Code400(SettingYiJianFanKuiActivity.this,response);
                            Toast.makeText(SettingYiJianFanKuiActivity.this,"失败" , Toast.LENGTH_SHORT).show();
                        }
                    }
                });
    }


    @OnClick({R.id.image_back, R.id.et_jianyi, R.id.btn_tijiao})
    public void onViewClicked(View view) {
        switch (view.getId()) {
//            返回按钮
            case R.id.image_back:
                finish();
                break;
//            意见或建议
            case R.id.et_jianyi:
                break;
//            提交
            case R.id.btn_tijiao:
//        园区招商的意见反馈的网络请求
                if(etJianyi.getText().toString().trim().equals("")){
                    Toast.makeText(this, "请输入您的意见或建议", Toast.LENGTH_SHORT).show();
                }else{
                    initYuanQuZhaShangYiJianFanKuiHttp();
                }
                break;
        }
    }







}
