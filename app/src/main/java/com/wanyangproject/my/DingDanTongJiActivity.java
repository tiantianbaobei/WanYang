package com.wanyangproject.my;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.Display;
import android.view.Gravity;
import android.view.View;
import android.view.WindowManager;
import android.webkit.JavascriptInterface;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bigkoo.pickerview.TimePickerView;
import com.google.gson.Gson;
import com.wanyangproject.R;
import com.wanyangproject.entity.DingDanChaXunEntity;
import com.wanyangproject.entity.MoRenTongJiEntity;
import com.wanyangproject.entity.UserEntity;
import com.wanyangproject.entity.WuYeFuWuLieBiaoEntity;
import com.wanyangproject.fuwuactivity.WuYeFuWuActivity;
import com.wanyangproject.popuwindow.PeiSongPopupWindow;
import com.wanyangproject.popuwindow.WuYeShenQingLeiXingPopupWindow;
import com.wanyangproject.utils.ContractUtils;
import com.zhy.http.okhttp.OkHttpUtils;
import com.zhy.http.okhttp.callback.StringCallback;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.Call;

public class DingDanTongJiActivity extends AppCompatActivity {

    @BindView(R.id.image_back)
    ImageView imageBack;
    @BindView(R.id.tv_zanshi)
    TextView tvZanshi;
    @BindView(R.id.tv_number)
    TextView tvNumber;
    @BindView(R.id.tv_peisongfei)
    TextView tvPeisongfei;
    @BindView(R.id.tv_weixin_money)
    TextView tvWeixinMoney;
    @BindView(R.id.tv_zhifubao_money)
    TextView tvZhifubaoMoney;
    @BindView(R.id.btn_chaxun)
    Button btnChaxun;
    @BindView(R.id.relative_zongjine)
    RelativeLayout relativeZongjine;
    @BindView(R.id.tv_money)
    TextView tvMoney;
    @BindView(R.id.tv_start_time)
    TextView tvStartTime;
    @BindView(R.id.tv_end_time)
    TextView tvEndTime;
    @BindView(R.id.tv_peisong_fangshi)
    TextView tvPeisongFangshi;
    @BindView(R.id.webView)
    WebView webView;
//    @BindView(R.id.linear_peisong)
//    LinearLayout linearPeisong;
    private DingDanChaXunEntity dingDanChaXunEntity;
    private String content;
    private WebSettings mWebSettings;
    private static final String APP_CACAHE_DIRNAME = "/webcache";
    private PeiSongPopupWindow peiSongPopupWindow;
    private UserEntity userEntity;
    private static Boolean isOK = false;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ding_dan_tong_ji);
        ButterKnife.bind(this);



        //    获取用户信息的网络请求

        OkHttpUtils.post()
                .url(ContractUtils.LOGIN_URL+"Life/user")
                .addHeader("token",ContractUtils.getTOKEN(DingDanTongJiActivity.this))
                .build()
                .execute(new StringCallback() {
                    @Override
                    public void onError(Call call, Exception e, int id) {
                        System.out.println(e+"       eeee用户信息");
                    }

                    @Override
                    public void onResponse(String response, int id) {
                        ContractUtils.Code500(DingDanTongJiActivity.this,response);
                        System.out.println(response+"    订单统计获取用户信息的网络请求");
                        if(response.indexOf("200") != -1) {
                            Gson gson = new Gson();
                            userEntity = gson.fromJson(response, UserEntity.class);
                            //        统计
//                            webView.loadUrl("http://park.hostop.net/index/index/tongji?id="+userEntity.getResponse().getUserid());
                            webView.loadUrl(ContractUtils.YUANQUZHAOSHANG_PHOTO_URL+"/index/index/tongji?id="+userEntity.getResponse().getUserid());
                        }
                    }
                });



        mWebSettings = webView.getSettings();
        mWebSettings.setJavaScriptEnabled(true);  //允许加载javascript
//        webView.getSettings().setLayoutAlgorithm(WebSettings.LayoutAlgorithm.SINGLE_COLUMN);
//        webView.getSettings().setLoadWithOverviewMode(true);
        mWebSettings.setSupportZoom(false);     //允许缩放
        mWebSettings.setBuiltInZoomControls(false); //原网页基础上缩放
        mWebSettings.setUseWideViewPort(false);   //任意比例缩放
        mWebSettings.setUseWideViewPort(false); //将图片调整到适合webview的大小
        mWebSettings.setLoadWithOverviewMode(true); // 缩放至屏幕的大小


        // 支持通过JS打开新窗口
        mWebSettings.setJavaScriptCanOpenWindowsAutomatically(true);
        //不显示webview缩放按钮
        mWebSettings.setDisplayZoomControls(false);
//        String cacheDirPath = getFilesDir().getAbsolutePath() + APP_CACAHE_DIRNAME;
//      String cacheDirPath = getCacheDir().getAbsolutePath()+Constant.APP_DB_DIRNAME;
        //设置数据库缓存路径
//        webView.getSettings().setDatabasePath(cacheDirPath);
        //设置  Application Caches 缓存目录
//        webView.getSettings().setAppCachePath(cacheDirPath);
        //开启 Application Caches 功能
        webView.setFocusable(false);
        webView.getSettings().setAppCacheEnabled(true);
        webView.setWebViewClient(new WebViewClient());


        webView.addJavascriptInterface(new JavaScriptInterface(), "JSInterface");


//        String finalUrl = "http://park.hostop.net/upload/20180913/3575ab1cf144418653e8663958b31738.docx";


////        统计
//        webView.loadUrl("http://park.hostop.net/index/index/tongji?id="+userEntity.getResponse().getUserid());


//        webView.loadUrl("http://park.hostop.net/upload/20180913/3575ab1cf144418653e8663958b31738.docx");



//        默认订单统计
        DingDanTongJiHttp();


//        订单统计饼图的网络请求
//        initTongJiHttp();
    }



//    默认订单统计
    private void DingDanTongJiHttp() {
        OkHttpUtils.post()
                .url(ContractUtils.LOGIN_URL + "shop/orderstatistics")
                .addHeader("token", ContractUtils.getTOKEN(DingDanTongJiActivity.this))
                .addParams("parkId", ContractUtils.getParkId(DingDanTongJiActivity.this))
                .addParams("phone", ContractUtils.getPhone(DingDanTongJiActivity.this))
                .build()
                .execute(new StringCallback() {
                    @Override
                    public void onError(Call call, Exception e, int id) {

                    }

                    @Override
                    public void onResponse(String response, int id) {
                        System.out.println(response+"            默认订单统计");
                        if(response.indexOf("200") != -1){
                            Gson gson = new Gson();
                            MoRenTongJiEntity moRenTongJiEntity = gson.fromJson(response, MoRenTongJiEntity.class);
                            if(moRenTongJiEntity.getResponse().getZong() != null){
                                tvMoney.setText("¥ "+moRenTongJiEntity.getResponse().getZong());
                            }


                            if(moRenTongJiEntity.getResponse().getShu() != null){
                                tvNumber.setText(moRenTongJiEntity.getResponse().getShu()+"份");
                            }


                            if(moRenTongJiEntity.getResponse().getPei() != null){
                                tvPeisongfei.setText("¥"+moRenTongJiEntity.getResponse().getPei());
                            }


                            if(moRenTongJiEntity.getResponse().getWei() != null){
                                tvWeixinMoney.setText("¥"+moRenTongJiEntity.getResponse().getWei());
                            }


                            if(moRenTongJiEntity.getResponse().getPay() != null){
                                tvZhifubaoMoney.setText("¥"+moRenTongJiEntity.getResponse().getPay());
                            }
                        }
                    }
                });
    }


    /**
     * 清除WebView缓存
     */
    public void clearWebViewCache() {

        //清理Webview缓存数据库
        try {
            deleteDatabase("webview.db");
            deleteDatabase("webviewCache.db");
        } catch (Exception e) {
            e.printStackTrace();
        }

        //WebView 缓存文件
        File appCacheDir = new File(getFilesDir().getAbsolutePath() + APP_CACAHE_DIRNAME);
//        Log.e(TAG, "appCacheDir path=" + appCacheDir.getAbsolutePath());

        File webviewCacheDir = new File(getCacheDir().getAbsolutePath() + "/webviewCache");
//        Log.e(TAG, "webviewCacheDir path=" + webviewCacheDir.getAbsolutePath());

        //删除webview 缓存目录
        if (webviewCacheDir.exists()) {
            deleteFile(webviewCacheDir);
        }
        //删除webview 缓存 缓存目录
        if (appCacheDir.exists()) {
            deleteFile(appCacheDir);
        }
    }


    /**
     * 递归删除 文件/文件夹
     *
     * @param file
     */
    public void deleteFile(File file) {

//        Log.i(TAG, "delete file path=" + file.getAbsolutePath());

        if (file.exists()) {
            if (file.isFile()) {
                file.delete();
            } else if (file.isDirectory()) {
                File files[] = file.listFiles();
                for (int i = 0; i < files.length; i++) {
                    deleteFile(files[i]);
                }
            }
            file.delete();
        } else {
//            Log.e(TAG, "delete file no exists " + file.getAbsolutePath());
        }
    }


//    //    订单统计饼图的网络请求
//    private void initTongJiHttp() {
//        OkHttpUtils.post()
//                .url(ContractUtils.LOGIN_URL+"user/jiexi")
//                .addHeader("token",ContractUtils.getTOKEN(DingDanTongJiActivity.this))
//                .build()
//                .execute(new StringCallback() {
//                    @Override
//                    public void onError(Call call, Exception e, int id) {
//                        System.out.println(e+"       eeee订单统计饼图的网络请求");
//                    }
//
//                    @Override
//                    public void onResponse(String response, int id) {
//                        System.out.println(response+"        订单统计饼图的网络请求 ");
//                        try {
//                            JSONObject jsonObject = new JSONObject(response);
//                            JSONObject response1 = jsonObject.getJSONObject("response");
//                            String content = response1.getString("content");
//                            if (content != null) {
//                                webView.loadDataWithBaseURL("http://www.baidu.com", getNewContent(content),"text/html", "UTF-8", null);
//                            }
//
//                        } catch (JSONException e) {
//                            e.printStackTrace();
//                        }
//                    }
//                });
//    }


    class JavaScriptInterface {
        @JavascriptInterface
        public void processFormInputs(String html) {
//            LogUtils.e("html-->" + html);
            System.out.println(html);
        }
    }


    /**
     * 将html文本内容中包含img标签的图片，宽度变为屏幕宽度，高度根据宽度比例自适应
     **/
    public static String getNewContent(String htmltext) {

//        if(isOK == false){
//            return htmltext;
//        }
//
//        isOK = true;
        try {
            Document doc = Jsoup.parse(htmltext);
            Elements elements = doc.getElementsByTag("img");
            for (Element element : elements) {
                element.attr("width", "100%").attr("height", "auto");
            }

            return doc.toString();
        } catch (Exception e) {
            return htmltext;
        }
    }


    //    订单查询的网络请求
    private void initDingDanChaXunHttp() {
        String string = "";
        if(tvPeisongFangshi.getText().toString().trim().equals("全部")){
            string = "0";
        }else if(tvPeisongFangshi.getText().toString().trim().equals("配送")){
            string = "1";
        }else if(tvPeisongFangshi.getText().toString().trim().equals("自取")){
            string = "2";
        }


        String starttime = tvStartTime.getText().toString().trim() + " 00:00:00";
        String endtime = tvEndTime.getText().toString().trim() + " 00:00:00";


        System.out.println(ContractUtils.getTOKEN(DingDanTongJiActivity.this)+"         token");
        System.out.println(ContractUtils.getParkId(DingDanTongJiActivity.this)+"         psrkid");
        System.out.println(ContractUtils.getPhone(DingDanTongJiActivity.this)+"         phone");
        System.out.println(string+"         address_type");
        System.out.println(tvStartTime.getText().toString().trim()+"         starttime");
        System.out.println(tvEndTime.getText().toString().trim()+"         endtime");
        System.out.println(string+"         配送方式");


        OkHttpUtils.post()
                .url(ContractUtils.LOGIN_URL + "shop/orderstatistics")
                .addHeader("token", ContractUtils.getTOKEN(DingDanTongJiActivity.this))
                .addParams("parkId", ContractUtils.getParkId(DingDanTongJiActivity.this))
                .addParams("phone", ContractUtils.getPhone(DingDanTongJiActivity.this))
                .addParams("addres_type",string)//	1:配送2：自取 默认不传
                .addParams("startime",starttime)//开始时间 2010-11-10 22:19:21
                .addParams("endtime",endtime)//结束时间 2010-11-10 22:19:21
                .build()
                .execute(new StringCallback() {
                    @Override
                    public void onError(Call call, Exception e, int id) {

                    }

                    @Override
                    public void onResponse(String response, int id) {
                        ContractUtils.Code500(DingDanTongJiActivity.this, response);
                        System.out.println(response + "       订单查询的网络请求");

                        if (response.indexOf("200") != -1) {

                            Gson gson = new Gson();
                            dingDanChaXunEntity = gson.fromJson(response, DingDanChaXunEntity.class);
                            Toast.makeText(DingDanTongJiActivity.this, dingDanChaXunEntity.getMsg(), Toast.LENGTH_SHORT).show();

                            if (dingDanChaXunEntity.getResponse().getZong() == null) {

                            } else {
                                tvMoney.setText("¥"+dingDanChaXunEntity.getResponse().getZong());
                            }


                            if (dingDanChaXunEntity.getResponse().getShu() == null) {

                            } else {
                                tvNumber.setText(dingDanChaXunEntity.getResponse().getShu() + "份");
                            }


                            if (dingDanChaXunEntity.getResponse().getPei() == null) {

                            } else {
                                tvPeisongfei.setText("¥ " + dingDanChaXunEntity.getResponse().getPei());
                            }

                            if (dingDanChaXunEntity.getResponse().getWei() == null) {

                            } else {
                                tvWeixinMoney.setText("¥ " + dingDanChaXunEntity.getResponse().getWei());
                            }


                            if (dingDanChaXunEntity.getResponse().getPay() == null) {

                            } else {
                                tvZhifubaoMoney.setText("¥ " + dingDanChaXunEntity.getResponse().getPay());
                            }
                        }else if(response.indexOf("400") !=-1){
                            ContractUtils.Code400(DingDanTongJiActivity.this,response);
                        }
                    }
                });
    }

    @OnClick({R.id.tv_start_time, R.id.tv_end_time, R.id.tv_peisong_fangshi, R.id.image_back, R.id.tv_zanshi, R.id.relative_zongjine, R.id.tv_number, R.id.tv_peisongfei, R.id.tv_weixin_money, R.id.tv_zhifubao_money, R.id.btn_chaxun})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.image_back:
                finish();
                break;
//            暂时按钮  跳转我的余额
            case R.id.tv_zanshi:
                Intent intent = new Intent(DingDanTongJiActivity.this, WoDeYuEActivity.class);
                startActivity(intent);
                break;
            case R.id.tv_number:
                break;
            case R.id.tv_peisongfei:
                break;
            case R.id.tv_weixin_money:
                break;
            case R.id.tv_zhifubao_money:
                break;
//            查询
            case R.id.btn_chaxun:
                if(tvStartTime.getText().toString().trim().equals("")){
                    Toast.makeText(this, "请选择开始时间！", Toast.LENGTH_SHORT).show();
                }else if(tvEndTime.getText().toString().equals("")){
                    Toast.makeText(this, "请选择结束时间！", Toast.LENGTH_SHORT).show();
                }else if(tvPeisongFangshi.getText().toString().trim().equals("")){
                    Toast.makeText(this, "请选择配送方式！", Toast.LENGTH_SHORT).show();
                }else{

//                订单查询的网络
                    initDingDanChaXunHttp();
                }

                break;
            case R.id.relative_zongjine:
                Intent intent1 = new Intent(DingDanTongJiActivity.this, WoDeYuEActivity.class);
                startActivity(intent1);
                break;
//            开始时间
            case R.id.tv_start_time:
                Calendar calendar = Calendar.getInstance();

                TimePickerView timePickerView = new TimePickerView.Builder(DingDanTongJiActivity.this, new TimePickerView.OnTimeSelectListener() {
                    @Override
                    public void onTimeSelect(Date date, View v) {
//                        SimpleDateFormat simpleDateFormat1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                        SimpleDateFormat simpleDateFormat1 = new SimpleDateFormat("yyyy-MM-dd");
                        tvStartTime.setText(simpleDateFormat1.format(date));
//                        try {
//                            Date date1 = simpleDateFormat1.parse(tvTime.getText().toString());
//                            long time = date1.getTime();
//                            string = String.valueOf(time);
//                            System.out.println(string + "     时间戳");
//                        } catch (ParseException e) {
//                            e.printStackTrace();
//                        }
                    }
                })
                        .setType(new boolean[]{true, true, true, false, false, false})//默认全部显示
                        .setCancelText("取消")//取消按钮文字
                        .setSubmitText("确定")//确认按钮文字
                        .setTextXOffset(0, 0, 0, 0, 0, 0)
                        .setLunarCalendar(false)
                        .setContentSize(14)//滚轮文字大小
                        .gravity(Gravity.CENTER)
                        .setLineSpacingMultiplier(2.8f)//设置两横线之间的间隔倍数
                        .setTitleSize(16)//标题文字大小
                        .setTitleText("开始时间")//标题文字
                        .setOutSideCancelable(true)//点击屏幕，点在控件外部范围时，是否取消显示
                        .isCyclic(true)//是否循环滚动
                        .setTextColorCenter(Color.BLACK)//设置选中项的颜色
                        .setTextColorOut(Color.parseColor("#AEAAAA"))
                        .setTitleColor(Color.BLACK)//标题文字颜色
                        .setSubmitColor(Color.RED)//确定按钮文字颜色
                        .setCancelColor(Color.parseColor("#AEAAAA"))//取消按钮文字颜色
                        .setTitleBgColor(Color.parseColor("#f5f5f5"))//标题背景颜色 Night mode
                        .setBgColor(Color.parseColor("#ffffff"))//滚轮背景颜色 Night mode
                        .setRange(1900, calendar.get(Calendar.YEAR))//默认是1900-2100年
//                        .setDate(selectedDate)// 如果不设置的话，默认是系统时间*/
//                        .setRangDate(startDate,endDate)//起始终止年月日设定
                        .setLabel("年", "月", "日", "", "", "")
                        .isCenterLabel(true) //是否只显示中间选中项的label文字，false则每项item全部都带有label。
                        .isDialog(false)//是否显示为对话框样式
                        .build();
                timePickerView.setDate(Calendar.getInstance());//注：根据需求来决定是否使用该方法（一般是精确到秒的情况），此项可以在弹出选择器的时候重新设置当前时间，避免在初始化之后由于时间已经设定，导致选中时间与当前时间不匹配的问题。
                timePickerView.show();

                break;
//            结束时间
            case R.id.tv_end_time:
                Calendar calendar1 = Calendar.getInstance();

                TimePickerView timePickerView1 = new TimePickerView.Builder(DingDanTongJiActivity.this, new TimePickerView.OnTimeSelectListener() {
                    @Override
                    public void onTimeSelect(Date date, View v) {
//                        SimpleDateFormat simpleDateFormat1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                        SimpleDateFormat simpleDateFormat1 = new SimpleDateFormat("yyyy-MM-dd");
                        tvEndTime.setText(simpleDateFormat1.format(date));
//                        try {
//                            Date date1 = simpleDateFormat1.parse(tvTime.getText().toString());
//                            long time = date1.getTime();
//                            string = String.valueOf(time);
//                            System.out.println(string + "     时间戳");
//                        } catch (ParseException e) {
//                            e.printStackTrace();
//                        }
                    }
                })
                        .setType(new boolean[]{true, true, true, false, false, false})//默认全部显示
                        .setCancelText("取消")//取消按钮文字
                        .setSubmitText("确定")//确认按钮文字
                        .setTextXOffset(0, 0, 0, 0, 0, 0)
                        .setLunarCalendar(false)
                        .setContentSize(14)//滚轮文字大小
                        .gravity(Gravity.CENTER)
                        .setLineSpacingMultiplier(2.8f)//设置两横线之间的间隔倍数
                        .setTitleSize(16)//标题文字大小
                        .setTitleText("结束时间")//标题文字
                        .setOutSideCancelable(true)//点击屏幕，点在控件外部范围时，是否取消显示
                        .isCyclic(true)//是否循环滚动
                        .setTextColorCenter(Color.BLACK)//设置选中项的颜色
                        .setTextColorOut(Color.parseColor("#AEAAAA"))
                        .setTitleColor(Color.BLACK)//标题文字颜色
                        .setSubmitColor(Color.RED)//确定按钮文字颜色
                        .setCancelColor(Color.parseColor("#AEAAAA"))//取消按钮文字颜色
                        .setTitleBgColor(Color.parseColor("#f5f5f5"))//标题背景颜色 Night mode
                        .setBgColor(Color.parseColor("#ffffff"))//滚轮背景颜色 Night mode
                        .setRange(calendar1.get(Calendar.YEAR), 2100)//默认是1900-2100年
//                        .setDate(selectedDate)// 如果不设置的话，默认是系统时间*/
//                        .setRangDate(startDate,endDate)//起始终止年月日设定
                        .setLabel("年", "月", "日", "", "", "")
                        .isCenterLabel(true) //是否只显示中间选中项的label文字，false则每项item全部都带有label。
                        .isDialog(false)//是否显示为对话框样式
                        .build();
                timePickerView1.setDate(Calendar.getInstance());//注：根据需求来决定是否使用该方法（一般是精确到秒的情况），此项可以在弹出选择器的时候重新设置当前时间，避免在初始化之后由于时间已经设定，导致选中时间与当前时间不匹配的问题。
                timePickerView1.show();

                break;
//            配送方式
            case R.id.tv_peisong_fangshi:

                Object itemsOnClick = null;
                peiSongPopupWindow = new PeiSongPopupWindow(DingDanTongJiActivity.this, (View.OnClickListener) itemsOnClick);
//                peiSongPopupWindow.showAtLocation(findViewById(R.id.relative), Gravity.TOP | Gravity.CENTER_HORIZONTAL, 0, 0);
                peiSongPopupWindow.showAsDropDown(findViewById(R.id.relative), 0, 0, 0);

                peiSongPopupWindow.setPeiSongClick(new PeiSongPopupWindow.PeiSongClick() {
                    @Override
                    public void peisongClick(String title) {
                        tvPeisongFangshi.setText(title);
                    }
                });

//
//                muQianZhuangTaiPopupWindow.setMuQianZhuangTaiPopupClick(new MuQianZhuangTaiPopupWindow.MuQianZhuangTaiPopupClick() {
//                    @Override
//                    public void muqianzhuangtaiPopupClick(String state) {
//                        tvMuqianZhuangtai.setText(state);
//                    }
//                });



////                linearPeisong.setVisibility(View.VISIBLE);
//                AlertDialog dialog = new AlertDialog.Builder(DingDanTongJiActivity.this).create();
//                dialog.show();
//                dialog.getWindow().setContentView(R.layout.alertdialog_peisong_fangshi);
//                dialog.getWindow().setGravity(Gravity.TOP);
////                dialog.getWindow().setBackgroundDrawableResource(R.drawable.shape_zhuce);
//                WindowManager windowManager=getWindowManager();
//                Display defaultDisplay = windowManager.getDefaultDisplay();
//                WindowManager.LayoutParams attributes = dialog.getWindow().getAttributes();
//                attributes.width= (int) (defaultDisplay.getWidth()*1.01);
//                attributes.y=300;
//                dialog.getWindow().setAttributes(attributes);
//                dialog.getWindow().findViewById(R.id.tv_quanbu).setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View view) {
//
//                    }
//                });
//                dialog.getWindow().findViewById(R.id.tv_ziqu).setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View view) {
//
//                    }
//                });
//                dialog.getWindow().findViewById(R.id.tv_peisong).setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View view) {
//
//                    }
//                });
                break;
        }
    }

}
