package com.wanyangproject.my;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;

import com.wanyangproject.R;
import com.wanyangproject.entity.ChuanZhiEntity;
import com.wanyangproject.entity.DaiShouHuoEntity;
import com.wanyangproject.entity.DingDanXiangQingEntity;
import com.wanyangproject.myadapter.TuiKuanXiangQingAdapter;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class TuiKuanXiangQingActivity extends AppCompatActivity {

    @BindView(R.id.image_back)
    ImageView imageBack;
    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    private ChuanZhiEntity chuanZhiEntity;
    private DingDanXiangQingEntity.ResponseBean responseBean;
    private TuiKuanXiangQingAdapter tuiKuanXiangQingAdapter;
    private String tuikuanyuanyin;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tui_kuan_xiang_qing);
        ButterKnife.bind(this);

        Intent intent = getIntent();
        tuikuanyuanyin = intent.getStringExtra("tuikuanyuanyin");
        System.out.println(tuikuanyuanyin+"            接收的退款原因");


        responseBean = (DingDanXiangQingEntity.ResponseBean)ChuanZhiEntity.obj;

        initView();
    }

    private void initView() {
        tuiKuanXiangQingAdapter = new TuiKuanXiangQingAdapter(this,responseBean,tuikuanyuanyin);
        LinearLayoutManager manager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(manager);
        recyclerView.setAdapter(tuiKuanXiangQingAdapter);

//        tuiKuanXiangQingAdapter.setTuiKuanCheXiaoClick(new TuiKuanXiangQingAdapter.TuiKuanCheXiaoClick() {
//            @Override
//            public void tuikuanchexiaoClick(int position) {
//                AlertDialog dialog = new
//            }
//        });

    }

    @OnClick({R.id.image_back, R.id.recyclerView})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.image_back:
                finish();
                break;
            case R.id.recyclerView:
                break;
        }
    }
}
