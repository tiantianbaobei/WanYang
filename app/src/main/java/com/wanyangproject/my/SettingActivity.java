package com.wanyangproject.my;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.umeng.message.PushAgent;
import com.umeng.message.UTrack;
import com.wanyangproject.R;
import com.wanyangproject.activity.GuanYuWoMenActivity;
import com.wanyangproject.activity.HomeActivity;
import com.wanyangproject.activity.LoginActivity;
import com.wanyangproject.activity.RegisterActivity;
import com.wanyangproject.activity.XiuGaiMiMaActivity;
import com.wanyangproject.activity.YuanQuZhaoShangYiJianFanKuiActivity;
import com.wanyangproject.activity.ZiTiDaXiaoActivity;
import com.wanyangproject.base.MyApp;
import com.wanyangproject.shouye.ShenFenBangDingActivity;
import com.wanyangproject.utils.ContractUtils;
import com.wanyangproject.utils.SharedPreferencesPhone;
import com.wanyangproject.utils.SharedPreferencesToken;

import java.util.HashSet;
import java.util.Random;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class SettingActivity extends AppCompatActivity {


    @BindView(R.id.relative_zitidaxiao)
    RelativeLayout relativeZitidaxiao;
    @BindView(R.id.relative_qingli_huancun)
    RelativeLayout relativeQingliHuancun;
    @BindView(R.id.tv_xiugai_mima)
    TextView tvXiugaiMima;
    @BindView(R.id.tv_yijian_fankui)
    TextView tvYijianFankui;
    @BindView(R.id.tv_guanyu_women)
    TextView tvGuanyuWomen;
    @BindView(R.id.button_tuichu)
    Button buttonTuichu;
    @BindView(R.id.tv_huancun)
    TextView tvHuancun;
    private static HashSet<Activity> hashSet = new HashSet<>();
    @BindView(R.id.image_back)
    ImageView imageBack;
    @BindView(R.id.tv_size)
    TextView tvSize;
    private float zitidaxiao;
    private String ziti = "";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setting);
        ButterKnife.bind(this);


        SharedPreferences sharedPreferences1 = getSharedPreferences("zitidaxiao", Context.MODE_PRIVATE);
        zitidaxiao = sharedPreferences1.getFloat("zitidaxiao", 1);
        System.out.println(zitidaxiao + "     首页选中的那个字体");


        SharedPreferences sharedPreferences2 = getSharedPreferences("ziti", Context.MODE_PRIVATE);
        ziti = sharedPreferences2.getString("ziti", "中");
        System.out.println(ziti + "      字体的大小");
        tvSize.setText(ziti);






//        Resources resource = getResources();
//        Configuration configuration = resource.getConfiguration();
//        configuration.fontScale = 1.5f;// 设置字体的缩放比例
//        resource.updateConfiguration(configuration, resource.getDisplayMetrics());


        SharedPreferences sharedPreferences = getSharedPreferences("huancun", MODE_PRIVATE);
        String huancun = sharedPreferences.getString("huancun", "10");
        float v = Float.parseFloat(huancun);
        Random ra = new Random();
        v += ra.nextInt(5) + 1;
        SharedPreferencesToken.savehuancun(MyApp.getContext(), v + "");

        tvHuancun.setText(v + "MB");


    }







    public static void start(Context context) {
        context.startActivity(new Intent(context, SettingActivity.class));
    }


    @OnClick({R.id.image_back, R.id.relative_zitidaxiao, R.id.relative_qingli_huancun, R.id.tv_xiugai_mima, R.id.tv_yijian_fankui, R.id.tv_guanyu_women, R.id.button_tuichu})
    public void onViewClicked(View view) {
        switch (view.getId()) {
//            返回按钮
            case R.id.image_back:
                finish();
                break;
//            字体大小
            case R.id.relative_zitidaxiao:
                Intent intent = new Intent(SettingActivity.this, ZiTiDaXiaoActivity.class);
                startActivity(intent);
                break;
//            清理缓存
            case R.id.relative_qingli_huancun:
                tvHuancun.setText("");
                break;
//            修改密码
            case R.id.tv_xiugai_mima:
                Intent intent1 = new Intent(SettingActivity.this, XiuGaiMiMaActivity.class);
                startActivity(intent1);
                break;
//            意见反馈
            case R.id.tv_yijian_fankui:
                Intent intent2 = new Intent(SettingActivity.this, SettingYiJianFanKuiActivity.class);
                startActivity(intent2);
                break;
//            关于我们
            case R.id.tv_guanyu_women:
                Intent intent3 = new Intent(SettingActivity.this, GuanYuWoMenActivity.class);
                startActivity(intent3);
                break;
//            退出登录
            case R.id.button_tuichu:
                final AlertDialog dialog = new AlertDialog.Builder(SettingActivity.this).create();
                dialog.show();
                dialog.getWindow().setContentView(R.layout.tuichu_alertdialog);
                dialog.getWindow().setBackgroundDrawableResource(R.drawable.shape_zhuce);
                WindowManager windowManager=getWindowManager();
                Display defaultDisplay = windowManager.getDefaultDisplay();
                WindowManager.LayoutParams attributes = dialog.getWindow().getAttributes();
                attributes.width= (int) (defaultDisplay.getWidth()*0.8);
                dialog.getWindow().setAttributes(attributes);
                dialog.getWindow().findViewById(R.id.tv_quxiao).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        dialog.dismiss();
                    }
                });
                dialog.getWindow().findViewById(R.id.tv_queding).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        PushAgent mPushAgent = PushAgent.getInstance(SettingActivity.this);
                        mPushAgent.deleteAlias(ContractUtils.getPhone(SettingActivity.this), "00", new UTrack.ICallBack() {
                            @Override
                            public void onMessage(boolean b, String s) {

                            }
                        });
                        SharedPreferencesPhone.clearPhone(SettingActivity.this);
                        Intent intent11 = new Intent(SettingActivity.this, LoginActivity.class);
                        startActivity(intent11);
                        finish();
                        exit();
                        dialog.dismiss();
                    }
                });

                break;
        }
    }


    /**
     * 每一个Activity 在 onCreate 方法的时候，可以装入当前this
     *
     * @param activity
     */
    public void addActivity(Activity activity) {
        try {
            hashSet.add(activity);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    /**
     * 调用此方法用于销毁所有的Activity，然后我们在调用此方法之前，调到登录的Activity
     */
    public void exit() {
        try {
            for (Activity activity : hashSet) {
                if (activity != null)
                    activity.finish();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
