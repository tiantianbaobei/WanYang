package com.wanyangproject.my;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.wanyangproject.R;
import com.wanyangproject.myadapter.YuanGongGuanLiAdapter;
import com.wanyangproject.utils.ContractUtils;
import com.wanyangproject.widget.suspensionindexbar.IndexBar;
import com.zhy.http.okhttp.OkHttpUtils;
import com.zhy.http.okhttp.callback.StringCallback;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Iterator;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.Call;


//    企业身份  的 员工信息
public class MyYuanGongXinXiActivity extends AppCompatActivity {

    @BindView(R.id.image_back)
    ImageView imageBack;
    @BindView(R.id.recycerView)
    RecyclerView recycerView;
    @BindView(R.id.zimu)
    IndexBar zimu;
    @BindView(R.id.tv_a)
    TextView tvA;
    @BindView(R.id.image_sousuo)
    ImageView imageSousuo;
    @BindView(R.id.tv_yuangong_guanli)
    TextView tvYuangongGuanli;
    @BindView(R.id.relative_sousuo)
    RelativeLayout relativeSousuo;
    @BindView(R.id.et_sousuo)
    EditText etSousuo;
    private YuanGongGuanLiAdapter yuanGongGuanLiAdapter;
    private ArrayList<String> suoyin = new ArrayList<>();// 索引A-Z
    private ArrayList<JSONObject> jsonArrayFenZu = new ArrayList<>();// 分组的数据
    private JSONObject jsonObject1;
    private LinearLayoutManager manager;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_yuan_gong_xin_xi);
        ButterKnife.bind(this);
        initView();


//        企业员工信息的网络请求
        initYuanGongXinXiHttp();


        etSousuo.addTextChangedListener(new TextWatcher() {
            @Override  //   输入文字之前的状态
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override // 输入文字中的状态
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override  // 输入文字之后的状态
            public void afterTextChanged(Editable editable) {
//               /        企业员工信息的网络请求
                initYuanGongXinXiHttp();

            }
        });


        zimu.setmOnIndexPressedListener(new IndexBar.onIndexPressedListener() {
            @Override
            public void onIndexPressed(int index, String text) {
//                System.out.println(index + "    index");
//                System.out.println(text + "     text");
//                Intent intent0 = new Intent(XuanZeYuanQuActivity.this, YuanQuQiYeActivity.class);
//                intent0.putExtra("title", text);// 点击单个字母传的标题
//                startActivity(intent0);

                int pos = 0;
                if (suoyin.contains(text)) {
                    int a = suoyin.indexOf(text);
                    int sum = 0;// 之和
                    for (int i = 0; i < a; i++) {
                        JSONArray jsonArray = null;
                        try {
                            jsonArray = jsonObject1.getJSONArray(suoyin.get(i));
                            System.out.println(jsonObject1 + "            jsonObject1");
                            sum += jsonArray.length();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                    pos = sum;
                    System.out.println(pos + "    pos");
                    System.out.println(recycerView + "           recycerView");
                    System.out.println(manager + "       manager");
                    recycerView.setLayoutManager(manager);
                    manager.scrollToPositionWithOffset(pos, 0);// 自动滚动到哪一行

//                    manager.scrollToPosition(pos);
//                    recycerView.setLayoutManager(manager);
//                    manager.scrollToPosition(pos); // 自动滚动到哪一行
                }
            }

            @Override
            public void onMotionEventEnd() {
                System.out.println("111111111111");
            }
        });


    }


    //    企业员工信息的网络请求
    private void initYuanGongXinXiHttp() {
        OkHttpUtils.post()
                .url(ContractUtils.LOGIN_URL + "company/getCompPeople")
                .addHeader("token", ContractUtils.getTOKEN(MyYuanGongXinXiActivity.this))
                .addParams("personName", etSousuo.getText().toString().trim())
                .build()
                .execute(new StringCallback() {
                    @Override
                    public void onError(Call call, Exception e, int id) {
                        System.out.println(e + "    eee企业员工信息");
                    }

                    @Override
                    public void onResponse(String response, int id) {
                        ContractUtils.Code500(MyYuanGongXinXiActivity.this, response);
                        System.out.println(response + "          企业员工信息的网络请求 ");
                        if (response.indexOf("200") != -1) {
                            int aa = jsonArrayFenZu.size();

                            jsonArrayFenZu.clear();

                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                jsonObject1 = jsonObject.getJSONObject("response");
                                Iterator<String> keys = jsonObject1.keys();
                                while (keys.hasNext()) {
                                    String key = (String) keys.next();
                                    JSONArray jsonArray = jsonObject1.getJSONArray(key);
                                    for (int i = 0; i < jsonArray.length(); i++) {
                                        JSONObject jj = (JSONObject) jsonArray.get(i);
                                        jsonArrayFenZu.add(jj);
                                    }

                                    int bb = jsonArrayFenZu.size();
                                    if (aa == bb) {
                                        Toast.makeText(MyYuanGongXinXiActivity.this, "暂无数据", Toast.LENGTH_SHORT).show();
                                    }
                                    if (suoyin.contains(key)) {

                                    } else {
                                        suoyin.add(key);
                                    }
//                                suoyin.add(key);

//                                String value = jsonObject.getString(key);
//                                Log.e("TAG", "key=" + key + "\tvalue=" + value);//键值对key value
                                    System.out.println(key + "    keykey");
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }


//                            Gson gson = new Gson();
//                            QiYeYuanGongXinXiEntity qiYeYuanGongXinXiEntity = gson.fromJson(response, QiYeYuanGongXinXiEntity.class);
                            yuanGongGuanLiAdapter = new YuanGongGuanLiAdapter(MyYuanGongXinXiActivity.this, jsonArrayFenZu);
                            manager = new LinearLayoutManager(MyYuanGongXinXiActivity.this);
                            recycerView.setLayoutManager(manager);
                            recycerView.setAdapter(yuanGongGuanLiAdapter);
                        }
                    }
                });
    }


    private void initView() {
//        yuanGongGuanLiAdapter = new YuanGongGuanLiAdapter(MyYuanGongXinXiActivity.this);
//        LinearLayoutManager manager = new LinearLayoutManager(this);
//        recycerView.setLayoutManager(manager);
//        recycerView.setAdapter(yuanGongGuanLiAdapter);

//        zimu.setmPressedShowTextView(tvA)
//                .setNeedRealIndex(true)
//                .setmLayoutManager(manager);
//        zimu.getDataHelper().sortSourceDatas(results);
//        zimu.setmSourceDatas(results).invalidate();
    }

    @OnClick({R.id.image_back, R.id.recycerView, R.id.zimu, R.id.tv_a, R.id.image_sousuo, R.id.et_sousuo})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.image_back:
                finish();
                break;
            case R.id.recycerView:
                break;
            case R.id.zimu:
                break;
            case R.id.tv_a:
                break;
            case R.id.image_sousuo:
                zimu.setVisibility(View.GONE);
                tvYuangongGuanli.setVisibility(View.INVISIBLE);
                relativeSousuo.setVisibility(View.VISIBLE);
                imageSousuo.setVisibility(View.INVISIBLE);
                break;
            case R.id.et_sousuo:
//                //        企业员工信息的网络请求
//                initYuanGongXinXiHttp();
                break;
        }
    }
}
