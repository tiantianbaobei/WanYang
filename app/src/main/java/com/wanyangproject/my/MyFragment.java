package com.wanyangproject.my;

import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.DrawableRes;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.wanyangproject.R;

/**
 * Created by 甜甜 on 2018/7/11.
 */
//  我的界面

public class MyFragment extends Fragment{



    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_my, container, false);
        return view;
    }
}


//public class MineFragment extends BaseFragment implements View.OnClickListener {
//
//    private ImageView mineHead;
//    private TextView mineMgr1;
//    private TextView mineMgr2;
//    private TextView mineMgr3;
//    private LinearLayout mineMgr;
//    private TextView mineOrderAll;
//    private TextView mineOrderPay;
//    private TextView mineOrderReceive;
//    private TextView mineOrderAppraise;
//    private TextView mineOrderFinish;
//    private RelativeLayout mineDot1;
//    private RelativeLayout mineDot2;
//    private RelativeLayout mineDot3;
//    private RelativeLayout mineDot4;
//    private LinearLayout mineOrder;
//    private TextView mineCollect;
//    private TextView mineForum;
//    private TextView mineAddress;
//    private TextView mineOther1;
//    private TextView mineOther2;
//    private TextView mineSet;
//    private int ROLE_INDEX = Constant.ROLE_TOURIST;
//    private TextView mine_dingwei;
//
//    @Override
//    public int getContentResId() {
//        return R.layout.fragment_mine;
//    }
//
//    @Override
//    public void init() {
//        EventBus.getDefault().register(this);
//
//        String roleIndex = mCache.getAsString(Constant.ROLE_INDEX);
//        if (roleIndex != null) {
//            ROLE_INDEX = Integer.parseInt(roleIndex);
//        }
//
//        initView();
//        initListener();
//    }
//
//    private void initView() {
//        mineHead = findViewById(R.id.mine_head);
//
//        mineMgr1 = findViewById(R.id.mine_mgr_1);
//        mineMgr2 = findViewById(R.id.mine_mgr_2);
//        mineMgr3 = findViewById(R.id.mine_mgr_3);
//        mineMgr = findViewById(R.id.mine_mgr);
//
//        mineOrderAll = findViewById(R.id.mine_order_all);
//        mineOrderPay = findViewById(R.id.mine_order_pay);
//        mineOrderReceive = findViewById(R.id.mine_order_receive);
//        mineOrderAppraise = findViewById(R.id.mine_order_appraise);
//        mineOrderFinish = findViewById(R.id.mine_order_finish);
//        mineDot1 = findViewById(R.id.mine_dot_1);
//        mineDot2 = findViewById(R.id.mine_dot_2);
//        mineDot3 = findViewById(R.id.mine_dot_3);
//        mineDot4 = findViewById(R.id.mine_dot_4);
//        mineOrder = findViewById(R.id.mine_order);
//
//        mineCollect = findViewById(R.id.mine_collect);
//        mineForum = findViewById(R.id.mine_forum);
//        mineAddress = findViewById(R.id.mine_address);
//        mineOther1 = findViewById(R.id.mine_other_1);
//        mineOther2 = findViewById(R.id.mine_other_2);
//        mineSet = findViewById(R.id.mine_set);
//
//        mine_dingwei = (TextView) findViewById(R.id.mine_dingwei);
//
//        setView();
//    }
//
//    private void setView() {
//
//        switch (ROLE_INDEX) {
//            case Constant.ROLE_TOURIST:
//                mineMgr.setVisibility(View.GONE);
//                mineOrder.setPadding(0, DensityUtil.dp2px(mContext, 30), 0, DensityUtil.dp2px(mContext, 10));
//                setLeftDrawableAndText(mineOther1, R.mipmap.my_shj, "商家入驻");
//                setLeftDrawableAndText(mineOther2, R.mipmap.my_bd, "绑定园区账号");
//                break;
//            case Constant.ROLE_COMPANY:
//                mineMgr.setVisibility(View.VISIBLE);
//                mineOrder.setPadding(0, DensityUtil.dp2px(mContext, 10), 0, DensityUtil.dp2px(mContext, 10));
//                setLeftDrawableAndText(mineOther1, R.mipmap.my_fuwu, "服务记录");
//                setLeftDrawableAndText(mineOther2, R.mipmap.my_shj, "商家入驻");
//                setTopDrawableAndText(mineMgr1, R.mipmap.my_yuangongxinxi, "员工信息");
//                setTopDrawableAndText(mineMgr2, R.mipmap.my_qiyeguanli, "企业管理");
//                setTopDrawableAndText(mineMgr3, R.mipmap.my_yikatong, "一卡通");
//                break;
//            case Constant.ROLE_EMPLOY:
//                mineMgr.setVisibility(View.VISIBLE);
//                mineOrder.setPadding(0, DensityUtil.dp2px(mContext, 10), 0, DensityUtil.dp2px(mContext, 10));
//                setLeftDrawableAndText(mineOther1, R.mipmap.my_fuwu, "服务记录");
//                setLeftDrawableAndText(mineOther2, R.mipmap.my_shj, "商家入驻");
//                setTopDrawableAndText(mineMgr1, R.mipmap.my_qiyeguanli, "我的宿舍");
//                setTopDrawableAndText(mineMgr2, R.mipmap.my_yikatong, "一卡通");
//                setTopDrawableAndText(mineMgr3, R.mipmap.my_qiyexinix, "企业信息");
//                break;
//            case Constant.ROLE_DOOR:
//                mineMgr.setVisibility(View.VISIBLE);
//                mineOrder.setPadding(0, DensityUtil.dp2px(mContext, 10), 0, DensityUtil.dp2px(mContext, 10));
//                setLeftDrawableAndText(mineOther1, R.mipmap.my_fuwu, "服务记录");
//                setLeftDrawableAndText(mineOther2, R.mipmap.my_shj, "商家入驻");
//                setTopDrawableAndText(mineMgr1, R.mipmap.my_qiyeguanli, "宿舍管理");
//                setTopDrawableAndText(mineMgr2, R.mipmap.my_wodecheliang, "我的车辆");
//                setTopDrawableAndText(mineMgr3, R.mipmap.my_fangkeguanli, "访客管理");
//                break;
//        }
//    }
//
//    private void initListener() {
//        mineHead.setOnClickListener(this);
//        mineMgr1.setOnClickListener(this);
//        mineMgr2.setOnClickListener(this);
//        mineMgr3.setOnClickListener(this);
//        mineOrderAll.setOnClickListener(this);
//        mineOrderPay.setOnClickListener(this);
//        mineOrderReceive.setOnClickListener(this);
//        mineOrderAppraise.setOnClickListener(this);
//        mineOrderFinish.setOnClickListener(this);
//        mineCollect.setOnClickListener(this);
//        mineForum.setOnClickListener(this);
//        mineAddress.setOnClickListener(this);
//        mineOther1.setOnClickListener(this);
//        mineOther2.setOnClickListener(this);
//        mineSet.setOnClickListener(this);
//        mine_dingwei.setOnClickListener(this);
//    }
//
//    private void setLeftDrawableAndText(TextView textView, @DrawableRes int leftResId, String title) {
//        Drawable leftDrawable = getResources().getDrawable(leftResId);
//        leftDrawable.setBounds(0, 0, leftDrawable.getMinimumWidth(), leftDrawable.getMinimumHeight());
//        Drawable rightDrawable = getResources().getDrawable(R.mipmap.to);
//        rightDrawable.setBounds(0, 0, rightDrawable.getMinimumWidth(), rightDrawable.getMinimumHeight());
//        textView.setCompoundDrawables(leftDrawable, null, rightDrawable, null);
//        textView.setText(title);
//    }
//
//    private void setTopDrawableAndText(TextView textView, @DrawableRes int resId, String title) {
//        Drawable drawable = getResources().getDrawable(resId);
//        drawable.setBounds(0, 0, drawable.getMinimumWidth(), drawable.getMinimumHeight());
//        textView.setCompoundDrawables(null, drawable, null, null);
//        textView.setText(title);
//    }
//
//    @Override
//    public void onClick(View view) {
//        switch (view.getId()) {
//            case R.id.mine_head:
//                BoxDialog.start(mContext);
//                break;
//            case R.id.mine_mgr_1:
//                switch (ROLE_INDEX) {
//                    case Constant.ROLE_COMPANY:
//                        // 员工信息
//                        EmployMgrActivity.start(mContext);
//                        break;
//                    case Constant.ROLE_EMPLOY:
//                        // 我的宿舍
//                        break;
//                    case Constant.ROLE_DOOR:
//                        // 宿舍管理
//                        DormMgrActivity.start(mContext);
//                        break;
//                }
//                break;
//            case R.id.mine_mgr_2:
//                switch (ROLE_INDEX) {
//                    case Constant.ROLE_COMPANY:
//                        // 企业管理
//                        CompanyMgrActivity.start(mContext);
//                        break;
//                    case Constant.ROLE_EMPLOY:
//                        // 一卡通
//                        OneCardActivity.start(mContext);
//                        break;
//                    case Constant.ROLE_DOOR:
//                        // 我的车辆
//                        CheLiangActivity.start(mContext);
//                        break;
//                }
//                break;
//            case R.id.mine_mgr_3:
//                switch (ROLE_INDEX) {
//                    case Constant.ROLE_COMPANY:
//                        // 一卡通
//                        OneCardActivity.start(mContext);
//                        break;
//                    case Constant.ROLE_EMPLOY:
//                        // 企业信息
//                        CompanyDetailActivity.start(mContext);
//                        break;
//                    case Constant.ROLE_DOOR:
//                        // 访客管理
//                        FangKeActivity.start(mContext);
//                        break;
//                }
//                break;
//            case R.id.mine_order_all:
//                OrderActivity.start(mContext, Constant.ORDER_ALL);
//                break;
//            case R.id.mine_order_pay:
//                mineDot1.setVisibility(View.GONE);
//                OrderActivity.start(mContext, Constant.ORDER_PAY);
//                break;
//            case R.id.mine_order_receive:
//                OrderActivity.start(mContext, Constant.ORDER_RECEIVE);
//                break;
//            case R.id.mine_order_appraise:
//                OrderActivity.start(mContext, Constant.ORDER_APPRAISE);
//                break;
//            case R.id.mine_order_finish:
//                OrderActivity.start(mContext, Constant.ORDER_FINISH);
//                break;
//            case R.id.mine_collect:
//                CollectActivity.start(mContext);
//                break;
//            case R.id.mine_forum:
//                ForumActivity.start(mContext);
//                break;
//            case R.id.mine_address:
//                AddressActivity.start(mContext);
//                break;
//            case R.id.mine_other_1:
//                if (ROLE_INDEX == Constant.ROLE_TOURIST) {
//                    CompanyJoinActivity.start(mContext);
//                } else {
//                    // 绑定园区账号
//                }
//                break;
//            case R.id.mine_other_2:
//                if (ROLE_INDEX == Constant.ROLE_TOURIST) {
//                    // 服务记录
//                } else {
//                    CompanyJoinActivity.start(mContext);
//                }
//                break;
//            case R.id.mine_set:
//                SetActivity.start(mContext);
//                break;
//            case R.id.mine_dingwei:
//                startActivity(new Intent(getActivity(), ChangeParkActivity.class));
//        }
//    }
//
//    @Subscribe(threadMode = ThreadMode.MAIN, priority = 100)
//    public void onEvent(EventModel eventModel) {
//        if (eventModel != null && (Constant.ROLE_INDEX).equals(eventModel.getString())) {
//            ROLE_INDEX = eventModel.getInteger();
//            setView();
//        }
//    }
//
//    @Override
//    public void onResume() {
//        super.onResume();
//        onEvent(new EventModel());
//    }
//
//    @Override
//    public void onDestroyView() {
//        super.onDestroyView();
//        EventBus.getDefault().unregister(this);
//    }
//}






