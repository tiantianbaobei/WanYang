package com.wanyangproject.my;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;

import com.google.gson.Gson;
import com.wanyangproject.R;
import com.wanyangproject.adapter.MyBangDingAdapter;
import com.wanyangproject.entity.MyBangDingLieBiaoEntity;
import com.wanyangproject.shouye.ShenFenBangDingActivity;
import com.wanyangproject.utils.ContractUtils;
import com.zhy.http.okhttp.OkHttpUtils;
import com.zhy.http.okhttp.callback.StringCallback;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.Call;

public class MyBangDingActivity extends AppCompatActivity {

    @BindView(R.id.image_back)
    ImageView imageBack;
    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    @BindView(R.id.image_add)
    ImageView imageAdd;
    private MyBangDingAdapter myBangDingAdapter;
    private MyBangDingLieBiaoEntity myBangDingLieBiaoEntity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_bang_ding);
        ButterKnife.bind(this);

//        我的页面绑定园区账号的网络请求
        initMineBangDingZhangHaoHttp();
    }


    @Override
    protected void onResume() {
        super.onResume();

//        我的页面绑定园区账号的网络请求
        initMineBangDingZhangHaoHttp();
    }

    //    我的页面绑定园区账号的网络请求
    private void initMineBangDingZhangHaoHttp() {
        OkHttpUtils.post()
                .url(ContractUtils.LOGIN_URL + "user/bingList")
                .addHeader("token", ContractUtils.getTOKEN(MyBangDingActivity.this))
                .build()
                .execute(new StringCallback() {
                    @Override
                    public void onError(Call call, Exception e, int id) {

                    }

                    @Override
                    public void onResponse(String response, int id) {
                        ContractUtils.Code500(MyBangDingActivity.this,response);
                        System.out.println(response + "           我的页面绑定园区账号的网络请求 ");

                        if(response.indexOf("200") != -1){
                            Gson gson = new Gson();
                            myBangDingLieBiaoEntity = gson.fromJson(response, MyBangDingLieBiaoEntity.class);
                            myBangDingAdapter = new MyBangDingAdapter(MyBangDingActivity.this,myBangDingLieBiaoEntity.getResponse().getList());
                            LinearLayoutManager manager = new LinearLayoutManager(MyBangDingActivity.this);
                            recyclerView.setLayoutManager(manager);
                            recyclerView.setAdapter(myBangDingAdapter);
                        }
                    }
                });
    }



    @OnClick({R.id.image_back,R.id.image_add})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.image_back:
                finish();
                break;
            case R.id.image_add:
                if(myBangDingLieBiaoEntity == null){
                    Intent intent = new Intent(MyBangDingActivity.this, ShenFenBangDingActivity.class);
                    intent.putExtra("bangding","2");
                    startActivity(intent);
                    return;
                }
                if(myBangDingLieBiaoEntity.getResponse().getIdCard() == null){
                    Intent intent = new Intent(MyBangDingActivity.this, ShenFenBangDingActivity.class);
                    intent.putExtra("bangding","2");
                    startActivity(intent);
                }else{
                    Intent intent = new Intent(MyBangDingActivity.this, ShenFenBangDingActivity.class);
                    intent.putExtra("card", myBangDingLieBiaoEntity.getResponse().getIdCard());
                    intent.putExtra("bangding","2");
                    startActivity(intent);
                }
                break;

        }
    }
}
