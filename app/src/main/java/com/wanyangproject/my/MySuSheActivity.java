package com.wanyangproject.my;

import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.wanyangproject.R;
import com.wanyangproject.entity.MenWeiShuiDianXinXiEntity;
import com.wanyangproject.shouye.YuanQuZhaoShangActivity;
import com.wanyangproject.utils.ContractUtils;
import com.zhy.http.okhttp.OkHttpUtils;
import com.zhy.http.okhttp.callback.StringCallback;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.Call;

//   员工  我的宿舍
public class MySuSheActivity extends AppCompatActivity {

    @BindView(R.id.image_back)
    ImageView imageBack;
    @BindView(R.id.tv_lengshui)
    TextView tvLengshui;
    @BindView(R.id.tv_reshui)
    TextView tvReshui;
    @BindView(R.id.tv_dianfei_number)
    TextView tvDianfeiNumber;
    @BindView(R.id.tv_dianfei)
    TextView tvDianfei;
    //    @BindView(R.id.tv_yikatong)
//    TextView tvYikatong;
    @BindView(R.id.tv_shuidian)
    TextView tvShuidian;
    @BindView(R.id.tv_leng_number)
    TextView tvLengNumber;
    @BindView(R.id.tv_re_number)
    TextView tvReNumber;
    @BindView(R.id.tv_sushe)
    TextView tvSushe;
    @BindView(R.id.relative_yuangong)
    RelativeLayout relativeYuangong;
    @BindView(R.id.tv_chongzhi)
    TextView tvChongzhi;
    @BindView(R.id.tv_yongliang)
    TextView tvYongliang;
    @BindView(R.id.tv_yikatong_chongzhi)
    TextView tvYikatongChongzhi;
    @BindView(R.id.relative_menwei)
    RelativeLayout relativeMenwei;
    @BindView(R.id.tv_chongzhi1)
    TextView tvChongzhi1;
    private String title;
    private MenWeiShuiDianXinXiEntity menWeiShuiDianXinXiEntity;
    //    private OkHttpClient okHttpUtils;
    private String response11;
    private NetWork netWork;
    private String chongzhichenggong;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_su_she);
        ButterKnife.bind(this);


//        okHttpUtils = new OkHttpClient.Builder()
//                .connectTimeout(10, TimeUnit.SECONDS)
//                .readTimeout(20, TimeUnit.SECONDS)
//                .build();


        Intent intent = getIntent();
        title = intent.getStringExtra("title");
        System.out.println(title + "             qqqqqqqqqqq");
        if (title.equals("yuangong")) {
            relativeYuangong.setVisibility(View.VISIBLE);
            relativeMenwei.setVisibility(View.GONE);
        } else if (title.equals("menwei")) {
            relativeMenwei.setVisibility(View.VISIBLE);
            relativeYuangong.setVisibility(View.GONE);
        }


//        获取员工门卫水电信息的网络请求
        initShuiDianXinXiHttp();







        IntentFilter intentFilter4 = new IntentFilter();
        intentFilter4.addAction("chongzhichenggong");
        netWork = new NetWork();
        registerReceiver(netWork, intentFilter4);
    }





    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(netWork);
    }

    class NetWork extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {

            //            结束页面
            chongzhichenggong = intent.getStringExtra("chongzhichenggong");
            if(chongzhichenggong != null){

//        获取员工门卫水电信息的网络请求
                initShuiDianXinXiHttp();

            }
        }
    }



    //    获取员工门卫水电信息的网络请求
    private void initShuiDianXinXiHttp() {
        final ProgressDialog progressDialog = new ProgressDialog(MySuSheActivity.this);
        progressDialog.setTitle("提示");
        progressDialog.setMessage("请等待...");
        progressDialog.setCancelable(false);
        progressDialog.show();
        OkHttpUtils.post()
                .url(ContractUtils.LOGIN_URL + "user/dormitory")
                .addHeader("token", ContractUtils.getTOKEN(MySuSheActivity.this))
                .build()
                .execute(new StringCallback() {
                    @Override
                    public void onError(Call call, Exception e, int id) {
                        System.out.println(e + "        eeeeee获取员工门卫水电信息");
                        progressDialog.dismiss();
                    }

                    @Override
                    public void onResponse(String response, int id) {
                        System.out.println(response + "         获取员工门卫水电信息的网络请求 ");
                        ContractUtils.Code500(MySuSheActivity.this, response);
                        progressDialog.dismiss();
                        if (response.indexOf("200") != -1) {

                            response11 = response;

                            Gson gson = new Gson();
                            menWeiShuiDianXinXiEntity = gson.fromJson(response, MenWeiShuiDianXinXiEntity.class);
                            if (menWeiShuiDianXinXiEntity.getResponse().getData() != null) {
                                if (menWeiShuiDianXinXiEntity.getResponse().getData().size() >= 1) {
                                    if (menWeiShuiDianXinXiEntity.getResponse().getData().get(0).getBalance() != null) {
                                        tvLengNumber.setText("¥" + menWeiShuiDianXinXiEntity.getResponse().getData().get(0).getBalance());
                                    }

                                    if (menWeiShuiDianXinXiEntity.getResponse().getData().get(0).getWaterUserName() != null) {
                                        String title = menWeiShuiDianXinXiEntity.getResponse().getData().get(0).getWaterUserName();
                                        String[] split = title.split("块");
                                        String shuibiao1 = split[1];
                                        tvLengshui.setText(shuibiao1);
//                                         tvReshui.setText(menWeiShuiDianXinXiEntity.getResponse().getData().get(0).getWaterUserName());
                                    }else{
                                        tvLengshui.setText("冷水");
                                    }
                                }
                            }


                            if (menWeiShuiDianXinXiEntity.getResponse().getData() != null) {
                                if (menWeiShuiDianXinXiEntity.getResponse().getData().size() >= 2) {
                                    if (menWeiShuiDianXinXiEntity.getResponse().getData().get(1).getBalance() != null) {
                                        tvReNumber.setText("¥" + menWeiShuiDianXinXiEntity.getResponse().getData().get(1).getBalance());
                                    }
                                    if (menWeiShuiDianXinXiEntity.getResponse().getData().get(1).getWaterUserName() != null) {
                                        String title = menWeiShuiDianXinXiEntity.getResponse().getData().get(1).getWaterUserName();
                                        String[] split = title.split("块");
                                        String shuibiao2 = split[1];
                                        tvReshui.setText(shuibiao2);
//                                        tvReshui.setText(menWeiShuiDianXinXiEntity.getResponse().getData().get(1).getWaterUserName());
                                    }else{
                                        tvReshui.setText("热水");
                                    }
                                }
                            }


                            if(menWeiShuiDianXinXiEntity != null){
                                if(menWeiShuiDianXinXiEntity.getResponse() != null){
                                    if(menWeiShuiDianXinXiEntity.getResponse().getDian() != null){
                                        if(menWeiShuiDianXinXiEntity.getResponse().getDian().getElectricityUserName() != null){
                                            tvDianfei.setText(menWeiShuiDianXinXiEntity.getResponse().getDian().getElectricityUserName());
                                        }else {
                                            tvDianfei.setText("电费");
                                        }
                                    }else{
                                        tvDianfei.setText("电费");
                                    }
                                }else {
                                    tvDianfei.setText("电费");
                                }
                            }else {
                                tvDianfei.setText("电费");
                            }
//                            if(menWeiShuiDianXinXiEntity.getResponse().getDian().getElectricityUserName() != null){
//                                tvDianfei.setText(menWeiShuiDianXinXiEntity.getResponse().getDian().getElectricityUserName());
//                            }else{
//                                tvDianfei.setText("电费");
//                            }

                            if (menWeiShuiDianXinXiEntity.getResponse().getRoomName() != null) {
                                String roomName = menWeiShuiDianXinXiEntity.getResponse().getRoomName();
                                String replace1 = roomName.replace("#", "");
                                System.out.println(replace1+"    replace1");
                                tvSushe.setText("宿舍：" + replace1);
//                                tvSushe.setText("宿舍：" + menWeiShuiDianXinXiEntity.getResponse().getRoomName());
                            }
                        } else if(response.indexOf("400") != -1){
                            ContractUtils.Code400(MySuSheActivity.this, response);
                        }
                    }
                });
    }




    @OnClick({R.id.image_back, R.id.tv_lengshui, R.id.tv_re_number, R.id.tv_reshui, R.id.tv_dianfei_number, R.id.tv_dianfei, R.id.tv_shuidian, R.id.tv_chongzhi,R.id.tv_chongzhi1, R.id.tv_yongliang, R.id.tv_yikatong_chongzhi})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.image_back:
                finish();
                break;
            case R.id.tv_lengshui:
                break;
            case R.id.tv_re_number:
                break;
            case R.id.tv_reshui:
                break;
            case R.id.tv_dianfei_number:
                break;
            case R.id.tv_dianfei:
                break;
//            case R.id.tv_yikatong:
////                Intent intent = new Intent(MySuSheActivity.this,YiKaTongActivity.class);
////                startActivity(intent);
//                Intent intent = new Intent(MySuSheActivity.this, ShuiDianFeiJiLuActivity.class);
//                startActivity(intent);
//                break;
            case R.id.tv_shuidian:
                Intent intent1 = new Intent(MySuSheActivity.this, MyShuiDianYongLiangActivity.class);
                intent1.putExtra("response", response11);
                intent1.putExtra("yonglian","0");
                startActivity(intent1);
//                Intent intent1 = new Intent(MySuSheActivity.this,YuanGongSuSheShuiDianYongLiangActivity.class);
//                startActivity(intent1);
                break;
            case R.id.tv_chongzhi1:
            case R.id.tv_chongzhi:
                if (response11 == null) {
                    Toast.makeText(this, "暂无数据！", Toast.LENGTH_SHORT).show();
                    return;
                }
                Intent intent11 = new Intent(MySuSheActivity.this, ShuiDianFeiJiLuActivity.class);
                System.out.println(response11 + "  response11");
                intent11.putExtra("response", response11);
                startActivity(intent11);
                break;
            case R.id.tv_yongliang:
                Intent intent2 = new Intent(MySuSheActivity.this, MyShuiDianYongLiangActivity.class);
                intent2.putExtra("response", response11);
                intent2.putExtra("yonglian","0");
                startActivity(intent2);
                break;
            case R.id.tv_yikatong_chongzhi:
                // 一卡通
                Intent intent22 = new Intent(MySuSheActivity.this, YiKaTongActivity.class);
                System.out.println(response11 + "  response11");
                intent22.putExtra("response", response11);
                startActivity(intent22);
                break;
        }
    }
}
