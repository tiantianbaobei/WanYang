package com.wanyangproject.my;

import android.app.Dialog;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Display;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.NumberPicker;
import android.widget.TextView;

import com.wanyangproject.R;
import com.wanyangproject.fuwuactivity.TianJiaFangKeActivity;

import java.text.SimpleDateFormat;
import java.util.Date;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MyTianJiaFangKeActivity extends AppCompatActivity {

    @BindView(R.id.image_back)
    ImageView imageBack;
    @BindView(R.id.et_name)
    EditText etName;
    @BindView(R.id.et_phone)
    EditText etPhone;
    @BindView(R.id.et_daofang_renshu)
    EditText etDaofangRenshu;
    @BindView(R.id.tv_daofang_shijian)
    TextView tvDaofangShijian;
    @BindView(R.id.et_chepaihao)
    EditText etChepaihao;
    @BindView(R.id.btn_login)
    Button btnLogin;
    private TextView tv_quxiao, tv_queding;
    private NumberPicker numberPicker_year, numberPicker_month, numberPicker_day, numberPicker_hour, numberPicker_minute, province_picker, city_picker, qu_picker;
    private int year, month, day, hour, minute;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_tian_jia_fang_ke);
        ButterKnife.bind(this);
        DataShow();
    }

    private void DataShow() {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy年MM月dd日 HH:mm:ss");// HH:mm:ss
//获取当前时间
        Date date = new Date(System.currentTimeMillis());
        System.out.println("Date获取当前日期时间"+simpleDateFormat.format(date));
//                                    2018年03月14日 11:39:29
        String str=simpleDateFormat.format(date);

        year = Integer.parseInt(str.substring(0,4));
        month = Integer.parseInt(str.substring(5,7));
        day = Integer.parseInt(str.substring(8,10));
        hour = Integer.parseInt(str.substring(12,14));
        minute = Integer.parseInt(str.substring(15,17));
    }

    @OnClick({R.id.image_back, R.id.et_name, R.id.et_phone, R.id.et_daofang_renshu, R.id.tv_daofang_shijian, R.id.et_chepaihao, R.id.btn_login})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.image_back:
                finish();
                break;
            case R.id.et_name:
                break;
            case R.id.et_phone:
                break;
            case R.id.et_daofang_renshu:
                break;
            case R.id.tv_daofang_shijian:
                final Dialog dialog = new Dialog(MyTianJiaFangKeActivity.this, R.style.ActionSheetDialogStyle);
                View inflate = LayoutInflater.from(MyTianJiaFangKeActivity.this).inflate(R.layout.fangkedengji_time_popupwindow, null);

                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy年MM月dd日 HH:mm:ss");// HH:mm:ss
//获取当前时间
                Date date = new Date(System.currentTimeMillis());
                System.out.println("Date获取当前日期时间"+simpleDateFormat.format(date));
//                                    2018年03月14日 11:39:29
                String str=simpleDateFormat.format(date);

                year = Integer.parseInt(str.substring(0,4));
                month = Integer.parseInt(str.substring(5,7));
                day = Integer.parseInt(str.substring(8,10));
                hour = Integer.parseInt(str.substring(12,14));
                minute = Integer.parseInt(str.substring(15,17));


                tv_quxiao = (TextView) inflate.findViewById(R.id.tv_quxiao);
                tv_queding = (TextView) inflate.findViewById(R.id.tv_queding);

//                            numberPicker_year.setValue(year);
//                            numberPicker_month.setValue(month);
//                            numberPicker_day.setValue(day);
//                            numberPicker_hour.setValue(hour);
//                            numberPicker_minute.setValue(minute);

                //        年
                numberPicker_year = (NumberPicker) inflate.findViewById(R.id.numberPicker_year);
                numberPicker_year.setMaxValue(2100);
                numberPicker_year.setMinValue(1900);
                year = Integer.parseInt(str.substring(0,4));
                numberPicker_year.setValue(year);
                numberPicker_year.setDescendantFocusability(NumberPicker.FOCUS_BLOCK_DESCENDANTS);//        设置numberPicker不可编辑
                numberPicker_year.setOnValueChangedListener(OnYearChangedListener);

                //        月
                numberPicker_month = (NumberPicker) inflate.findViewById(R.id.numberPicker_month);
                numberPicker_month.setMaxValue(12);
                numberPicker_month.setMinValue(1);
                month = Integer.parseInt(str.substring(5,7));
                numberPicker_month.setValue(month);
                numberPicker_month.setFormatter(formatter);  //        格式化显示数字，个位数前面加个零
                numberPicker_month.setDescendantFocusability(NumberPicker.FOCUS_BLOCK_DESCENDANTS);
                numberPicker_month.setOnValueChangedListener(OnMonthChangedListener);

                //        日
                //        判断是否闰年，从而设置二月份的天数
                numberPicker_day = (NumberPicker) inflate.findViewById(R.id.numberPicker_day);
                judgeMonth();
                day = Integer.parseInt(str.substring(8,10));
                numberPicker_day.setValue(day);
//                            numberPicker_day.setMaxValue(31);
//                            numberPicker_day.setMinValue(1);
                numberPicker_day.setFormatter(formatter);
                numberPicker_day.setDescendantFocusability(NumberPicker.FOCUS_BLOCK_DESCENDANTS);
                numberPicker_day.setOnValueChangedListener(OnDayChangedListener);

                //        小时
                numberPicker_hour = (NumberPicker) inflate.findViewById(R.id.numberPicker_hour);
                numberPicker_hour.setMaxValue(23);
                numberPicker_hour.setMinValue(0);
                hour = Integer.parseInt(str.substring(12,14));
                numberPicker_hour.setValue(hour);
                numberPicker_hour.setFormatter(formatter);
                numberPicker_hour.setDescendantFocusability(NumberPicker.FOCUS_BLOCK_DESCENDANTS);
                numberPicker_hour.setOnValueChangedListener(OnHourChangedListener);

                //        分
                numberPicker_minute = (NumberPicker) inflate.findViewById(R.id.numberPicker_minute);
                numberPicker_minute.setMaxValue(59);
                numberPicker_minute.setMinValue(0);
                minute = Integer.parseInt(str.substring(15,17));
                numberPicker_minute.setValue(minute);
                numberPicker_minute.setFormatter(formatter);
                numberPicker_minute.setDescendantFocusability(NumberPicker.FOCUS_BLOCK_DESCENDANTS);
                numberPicker_minute.setOnValueChangedListener(OnMinuteChangedListener);



                //                       点击面试时间弹窗的取消按钮
                tv_quxiao.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                    }
                });
//                        点击面试时间弹窗上的确定按钮
                tv_queding.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        String s1 = String.valueOf(numberPicker_month.getValue());
                        String s2 = String.valueOf(numberPicker_day.getValue());
                        String s3 = String.valueOf(numberPicker_hour.getValue());
                        String s4 = String.valueOf(numberPicker_minute.getValue());
                        if (numberPicker_month.getValue() < 10) {
                            s1 = "0" + String.valueOf(numberPicker_month.getValue());
                        }
                        if (numberPicker_day.getValue() < 10) {
                            s2 = "0" + String.valueOf(numberPicker_day.getValue());
                        }
                        if (numberPicker_hour.getValue() < 10) {
                            s3 = "0" + String.valueOf(numberPicker_hour.getValue());
                        }
                        if (numberPicker_minute.getValue() < 10) {
                            s4 = "0" + String.valueOf(numberPicker_minute.getValue());
                        }
                        tvDaofangShijian.setText(numberPicker_year.getValue() + "-" + s1 + "-" + s2 + " " + s3 + ":" + s4);
                        dialog.dismiss();
                    }
                });

                dialog.setContentView(inflate);
                Window dialogWindow = dialog.getWindow();
                dialogWindow.setGravity(Gravity.BOTTOM);
                //获得窗体的属性
                WindowManager.LayoutParams lp = dialogWindow.getAttributes();

                lp.y = -20;//设置Dialog距离底部的距离

                WindowManager windowManager = getWindowManager();
                Display display = windowManager.getDefaultDisplay();
                lp.width = display.getWidth();
                dialogWindow.setAttributes(lp);//       将属性设置给窗体
                dialog.show();
                break;
            case R.id.et_chepaihao:
                break;
            case R.id.btn_login:
                break;
        }
    }







    //            格式化显示数字，个位数前面加个零
    private NumberPicker.Formatter formatter = new NumberPicker.Formatter() {
        @Override
        public String format(int value) {
            String string = String.valueOf(value);
            if (value < 10) {
                string = "0" + string;
            }
            return string;
        }
    };
    //    年
    private NumberPicker.OnValueChangeListener OnYearChangedListener = new NumberPicker.OnValueChangeListener() {
        @Override
        public void onValueChange(NumberPicker picker, int oldVal, int newVal) {
            year = numberPicker_year.getValue();
            judgeYear();
        }
    };

    //    月
    private NumberPicker.OnValueChangeListener OnMonthChangedListener = new NumberPicker.OnValueChangeListener() {
        @Override
        public void onValueChange(NumberPicker picker, int oldVal, int newVal) {
            month = numberPicker_month.getValue();
            judgeMonth();
        }
    };

    //    日
    private NumberPicker.OnValueChangeListener OnDayChangedListener = new NumberPicker.OnValueChangeListener() {
        @Override
        public void onValueChange(NumberPicker picker, int oldVal, int newVal) {
            day = numberPicker_day.getValue();
        }
    };
    //    小时
    private NumberPicker.OnValueChangeListener OnHourChangedListener = new NumberPicker.OnValueChangeListener() {
        @Override
        public void onValueChange(NumberPicker picker, int oldVal, int newVal) {
            hour = numberPicker_hour.getValue();
        }
    };

    //    分
    private NumberPicker.OnValueChangeListener OnMinuteChangedListener = new NumberPicker.OnValueChangeListener() {
        @Override
        public void onValueChange(NumberPicker picker, int oldVal, int newVal) {
            minute = numberPicker_minute.getValue();
        }
    };


    //    年
    private void judgeYear() {
        if (month == 2) {
            if (year % 4 == 0) {
                if (numberPicker_day.getMaxValue() != 29) {
                    numberPicker_day.setDisplayedValues(null);
                    numberPicker_day.setMinValue(1);
                    numberPicker_day.setMaxValue(29);
                }
            } else {
                if (numberPicker_day.getMaxValue() != 28) {
                    numberPicker_day.setDisplayedValues(null);
                    numberPicker_day.setMinValue(1);
                    numberPicker_day.setMaxValue(28);
                }
            }
        }
        day = numberPicker_day.getValue();
    }

    //    月
    private void judgeMonth() {
        if (month == 2) {
            if (year % 4 == 0) {
                if (numberPicker_day.getMaxValue() != 29) {
                    numberPicker_day.setDisplayedValues(null);
                    numberPicker_day.setMinValue(1);
                    numberPicker_day.setMaxValue(29);
                }
            } else {
                if (numberPicker_day.getMaxValue() != 28) {
                    numberPicker_day.setDisplayedValues(null);
                    numberPicker_day.setMinValue(1);
                    numberPicker_day.setMaxValue(28);
                }
            }
        } else {
            switch (month) {
                case 4:
                case 6:
                case 9:
                case 11:
                    if (numberPicker_day.getMaxValue() != 30) {
                        numberPicker_day.setDisplayedValues(null);
                        numberPicker_day.setMinValue(1);
                        numberPicker_day.setMaxValue(30);
                    }
                    break;
                default:
                    if (numberPicker_day.getMaxValue() != 31) {
                        numberPicker_day.setDisplayedValues(null);
                        numberPicker_day.setMinValue(1);
                        numberPicker_day.setMaxValue(31);
                    }
            }
        }
        day = numberPicker_day.getValue();
    }





}
