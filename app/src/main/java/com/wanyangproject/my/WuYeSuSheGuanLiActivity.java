package com.wanyangproject.my;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.wanyangproject.R;
import com.wanyangproject.entity.MyQiYeSuSheGuanLiEntity;
import com.wanyangproject.myadapter.WuYeSuSheAdapter;
import com.wanyangproject.utils.ContractUtils;
import com.zhy.http.okhttp.OkHttpUtils;
import com.zhy.http.okhttp.callback.StringCallback;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.Call;

public class WuYeSuSheGuanLiActivity extends AppCompatActivity {

    @BindView(R.id.tv_sushe)
    TextView tvSushe;
    @BindView(R.id.image_sousuo)
    ImageView imageSousuo;
    @BindView(R.id.image_back)
    ImageView imageBack;
    @BindView(R.id.relative_sousuo)
    RelativeLayout relativeSousuo;
    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    @BindView(R.id.et_sousuo)
    EditText etSousuo;
    private WuYeSuSheAdapter wuYeSuSheAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wu_ye_su_she_guan_li);
        ButterKnife.bind(this);
        initView();


//        物业身份宿舍管理的网络请求
        initWuYeSuSheGuanLiHttp();


    }


    //    物业身份宿舍管理的网络请求
    private void initWuYeSuSheGuanLiHttp() {
        OkHttpUtils.post()
                .url(ContractUtils.LOGIN_URL + "Enterprise/dormitory")
                .addHeader("token", ContractUtils.getTOKEN(WuYeSuSheGuanLiActivity.this))
                .addParams("parkId", ContractUtils.getParkId(WuYeSuSheGuanLiActivity.this))
                .addParams("dormitoryName", etSousuo.getText().toString().trim())
                .build()
                .execute(new StringCallback() {
                    @Override
                    public void onError(Call call, Exception e, int id) {

                    }

                    @Override
                    public void onResponse(String response, int id) {
                        System.out.println(response+"        物业身份宿舍管理的网络请求 ");
                        ContractUtils.Code500(WuYeSuSheGuanLiActivity.this,response);
                        if(response.indexOf("200") != -1){
                            Gson gson = new Gson();
                            MyQiYeSuSheGuanLiEntity myQiYeSuSheGuanLiEntity = gson.fromJson(response, MyQiYeSuSheGuanLiEntity.class);

                            wuYeSuSheAdapter = new WuYeSuSheAdapter(WuYeSuSheGuanLiActivity.this,myQiYeSuSheGuanLiEntity.getResponse().getData());
                            LinearLayoutManager manager = new LinearLayoutManager(WuYeSuSheGuanLiActivity.this);
                            recyclerView.setLayoutManager(manager);
                            recyclerView.setAdapter(wuYeSuSheAdapter);



                            wuYeSuSheAdapter.setXiangQingClick(new WuYeSuSheAdapter.XiangQingClick() {
                                @Override
                                public void xiangqingClick(int position, String id) {
                                    Intent intent = new Intent(WuYeSuSheGuanLiActivity.this, MyQiYeSuSheXiangQingActivity.class);
                                    intent.putExtra("id",id);
                                    System.out.println(id+"        宿舍管理传id");
                                    startActivity(intent);
                                }
                            });






                        }
                    }
                });
    }


    private void initView() {
//        wuYeSuSheAdapter = new WuYeSuSheAdapter(this);
//        LinearLayoutManager manager = new LinearLayoutManager(this);
//        recyclerView.setLayoutManager(manager);
//        recyclerView.setAdapter(wuYeSuSheAdapter);
    }

    @OnClick({R.id.tv_sushe, R.id.image_sousuo, R.id.image_back, R.id.relative_sousuo, R.id.recyclerView})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.tv_sushe:
                break;
            case R.id.image_sousuo:
                tvSushe.setVisibility(View.INVISIBLE);
                relativeSousuo.setVisibility(View.VISIBLE);
                imageSousuo.setVisibility(View.INVISIBLE);
                break;
            case R.id.image_back:
                finish();
                break;
            case R.id.relative_sousuo:
                break;
            case R.id.recyclerView:
                break;
        }
    }
}
