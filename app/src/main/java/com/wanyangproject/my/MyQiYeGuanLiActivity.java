package com.wanyangproject.my;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.google.gson.Gson;
import com.wanyangproject.MenWeiQiYeXinXiActivity;
import com.wanyangproject.R;
import com.wanyangproject.entity.QiYeXinXiEntity;
import com.wanyangproject.ui.ChangFangActivity;
import com.wanyangproject.ui.ZhaoPinActivity;
import com.wanyangproject.utils.ContractUtils;
import com.zhy.http.okhttp.OkHttpUtils;
import com.zhy.http.okhttp.callback.StringCallback;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.Call;

public class MyQiYeGuanLiActivity extends AppCompatActivity {

    @BindView(R.id.image_back)
    ImageView imageBack;
    @BindView(R.id.image_touxiang)
    ImageView imageTouxiang;
    @BindView(R.id.tv_name_gongsi)
    TextView tvNameGongsi;
    @BindView(R.id.tv_address)
    TextView tvAddress;
    @BindView(R.id.tv_phone)
    TextView tvPhone;
    @BindView(R.id.tv_shuidian_yongliang)
    TextView tvShuidianYongliang;
    @BindView(R.id.tv_fangke_guanli)
    TextView tvFangkeGuanli;
    @BindView(R.id.tv_chelian_guanli)
    TextView tvChelianGuanli;
    @BindView(R.id.tv_changfang_guanli)
    TextView tvChangfangGuanli;
    @BindView(R.id.tv_sushe_guanli)
    TextView tvSusheGuanli;
    @BindView(R.id.tv_zhaopin_guanli)
    TextView tvZhaopinGuanli;
    private QiYeXinXiEntity qiYeXinXiEntity;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_qi_ye_guan_li);
        ButterKnife.bind(this);


        //        企业信息的网络请求
        initQiYeXinXiHttp();

    }




    //    企业信息的网络请求
    private void initQiYeXinXiHttp() {
        OkHttpUtils.post()
                .url(ContractUtils.LOGIN_URL+"company/getDetailyuan")
                .addHeader("token",ContractUtils.getTOKEN(MyQiYeGuanLiActivity.this))
                .addParams("parkId",ContractUtils.getParkId(MyQiYeGuanLiActivity.this))
                .build()
                .execute(new StringCallback() {
                    @Override
                    public void onError(Call call, Exception e, int id) {
                        System.out.println(e+"      eeeeeeaaaaaaaa");
                    }

                    @Override
                    public void onResponse(String response, int id) {
                        System.out.println(response+"            企业信息的网络请求");
                        ContractUtils.Code500(MyQiYeGuanLiActivity.this,response);
                        if(response.indexOf("200") != -1){
                            Gson gson = new Gson();
                            qiYeXinXiEntity = gson.fromJson(response, QiYeXinXiEntity.class);
                            if(qiYeXinXiEntity != null){
                                if(qiYeXinXiEntity.getResponse().getInfo().getEnterName() != null){
                                    tvNameGongsi.setText(qiYeXinXiEntity.getResponse().getInfo().getEnterName());
                                }

                                if(qiYeXinXiEntity.getResponse().getInfo().getContact() != null){
                                    tvPhone.setText("电话："+qiYeXinXiEntity.getResponse().getInfo().getContact());
                                }

                                if(qiYeXinXiEntity.getResponse().getInfo().getAddress() != null){
                                    tvAddress.setText("地址："+qiYeXinXiEntity.getResponse().getInfo().getAddress());
                                }
//
//                                if(qiYeXinXiEntity.getResponse().getInfo().getDesc() != null){
//                                    tvJianjie.setText(qiYeXinXiEntity.getResponse().getInfo().getDesc());
//                                }


                                if(qiYeXinXiEntity.getResponse().getInfo().getLogoUrl() != null){
                                    Glide.with(MyQiYeGuanLiActivity.this).load(qiYeXinXiEntity.getResponse().getInfo().getLogoUrl()).into(imageTouxiang);
                                }
                            }
                        }
                    }
                });
    }






    @OnClick({R.id.image_back, R.id.image_touxiang, R.id.tv_name_gongsi, R.id.tv_address, R.id.tv_phone, R.id.tv_shuidian_yongliang, R.id.tv_fangke_guanli, R.id.tv_chelian_guanli, R.id.tv_changfang_guanli, R.id.tv_sushe_guanli, R.id.tv_zhaopin_guanli})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.image_back:
                finish();
                break;
//            头像
            case R.id.image_touxiang:
                break;
//            公司名
            case R.id.tv_name_gongsi:
                break;
//            地址
            case R.id.tv_address:
                break;
//            、电话
            case R.id.tv_phone:
                break;
//            水电用量
            case R.id.tv_shuidian_yongliang:
                Intent intent = new Intent(MyQiYeGuanLiActivity.this,MyQiYeShuiDianYongLiangJiLuActivity.class);
//                Intent intent = new Intent(MyQiYeGuanLiActivity.this,MyShuiDianYongLiangActivity.class);
                startActivity(intent);
                break;
//            访客管理
            case R.id.tv_fangke_guanli:
                Intent intent1 = new Intent(MyQiYeGuanLiActivity.this,MyFangKeGuanLiActivity.class);
                startActivity(intent1);
                break;
//            车辆管理
            case R.id.tv_chelian_guanli:
                Intent intent2 = new Intent(MyQiYeGuanLiActivity.this,CheLiangGuanLiActivity.class);
                startActivity(intent2);
                break;
//            厂房管理
            case R.id.tv_changfang_guanli:
                Intent intent3 = new Intent(MyQiYeGuanLiActivity.this, MyChangFangGuanLiActivity.class);
                startActivity(intent3);
                break;
//            、宿舍管理
            case R.id.tv_sushe_guanli:
                Intent intent4 = new Intent(MyQiYeGuanLiActivity.this,MySuSheGuanLiActivity.class);
                startActivity(intent4);
                break;
//            招聘管理
            case R.id.tv_zhaopin_guanli:
                Intent intent5 = new Intent(MyQiYeGuanLiActivity.this, ZhaoPinGuanLiActivity.class);
                startActivity(intent5);
                break;
        }
    }
}
