package com.wanyangproject.my;

import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.wanyangproject.R;
import com.wanyangproject.activity.HistoryConsumptionActivity;
import com.wanyangproject.entity.QiYeSuSheXiangQingEntity;
import com.wanyangproject.utils.ContractUtils;
import com.zhy.http.okhttp.OkHttpUtils;
import com.zhy.http.okhttp.callback.StringCallback;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.Call;

public class MyQiYeSuSheXiangQingActivity extends AppCompatActivity {

    @BindView(R.id.tv_changfang)
    TextView tvChangfang;

    @BindView(R.id.image_back)
    ImageView imageBack;
    @BindView(R.id.tv_title)
    TextView tvTitle;
    @BindView(R.id.tv_yuanqu)
    TextView tvYuanqu;
    @BindView(R.id.tv_dikuai)
    TextView tvDikuai;
    @BindView(R.id.tv_loufang)
    TextView tvLoufang;
    @BindView(R.id.tv_fangjian)
    TextView tvFangjian;
    @BindView(R.id.tv_compang)
    TextView tvCompang;
    @BindView(R.id.tv_shuifei_money)
    TextView tvShuifeiMoney;
    @BindView(R.id.relative_shuifei)
    RelativeLayout relativeShuifei;
    @BindView(R.id.tv_dianfei_money)
    TextView tvDianfeiMoney;
    @BindView(R.id.relative_dianfei)
    RelativeLayout relativeDianfei;
    @BindView(R.id.tv_shuifei_money2)
    TextView tvShuifeiMoney2;
    @BindView(R.id.relative_shuifei2)
    RelativeLayout relativeShuifei2;
    @BindView(R.id.tv_shuifei)
    TextView tvShuifei;
    @BindView(R.id.tv_shuifei2)
    TextView tvShuifei2;
    @BindView(R.id.tvone)
    TextView tvone;
    @BindView(R.id.tvtwo)
    TextView tvtwo;
    @BindView(R.id.tvthree)
    TextView tvthree;
    @BindView(R.id.tv_chongzhi1)
    TextView tvChongzhi1;
    @BindView(R.id.tv_chongzhi2)
    TextView tvChongzhi2;
    @BindView(R.id.tv_chongzhi3)
    TextView tvChongzhi3;
    private String id;
    private NetWork netWork;
    private String chongzhichenggong;
    private QiYeSuSheXiangQingEntity qiYeSuSheXiangQingEntity;
    private String responseresponse222;
    private String yu2;
    private String yu;
    private String waterUserId;
    private String waterUserName;

    private String waterUserId2;
    private String waterUserName2;

    private String dormitoryId;
    private String dormitoryName;
    private String parkName;
    private String parcelName;
    private String roomName;
    private String buildingName;
    private String enterName;
    private String electricityUserId;
    private String electricityUserName;
    private String yudian;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_qi_ye_su_she_xiang_qing);
        ButterKnife.bind(this);


        Intent intent = getIntent();
        id = intent.getStringExtra("id");
        System.out.println(id + "          宿舍详情接收id");


        if (id != null) {
//            宿舍详情的网络请求
            initSuSheXiangQingHttp();
        }


        IntentFilter intentFilter4 = new IntentFilter();
        intentFilter4.addAction("chongzhichenggong");
        netWork = new NetWork();
        registerReceiver(netWork, intentFilter4);


    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        unregisterReceiver(netWork);
    }


    class NetWork extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {

            //            结束页面
            chongzhichenggong = intent.getStringExtra("chongzhichenggong");
            if (chongzhichenggong != null) {
                if (id != null) {
//                    宿舍详情的网络请求
                    initSuSheXiangQingHttp();
                }
            }
        }
    }


    //    宿舍详情的网络请求
    private void initSuSheXiangQingHttp() {
        final ProgressDialog progressDialog = new ProgressDialog(MyQiYeSuSheXiangQingActivity.this);
        progressDialog.setTitle("提示");
        progressDialog.setMessage("请等待...");
        progressDialog.setCancelable(false);
        progressDialog.show();
        OkHttpUtils.post()
                .url(ContractUtils.LOGIN_URL + "Enterprise/dormitorydetails")
                .addHeader("token", ContractUtils.getTOKEN(MyQiYeSuSheXiangQingActivity.this))
                .addParams("parkId", ContractUtils.getParkId(MyQiYeSuSheXiangQingActivity.this))
                .addParams("dormitoryId", id)
                .build()
                .connTimeOut(20000)
                .writeTimeOut(20000)
                .readTimeOut(20000)
                .execute(new StringCallback() {
                    @Override
                    public void onError(Call call, Exception e, int id) {
                        System.out.println(e + "      eeee舍管理详情");
                        progressDialog.dismiss();
                    }

                    @Override
                    public void onResponse(String response, int id) {
                        System.out.println(response + "          企业身份宿舍管理详情的网络请求");
                        progressDialog.dismiss();
                        ContractUtils.Code500(MyQiYeSuSheXiangQingActivity.this, response);


                        JSONObject jsonObject = null;
                        try {
                            jsonObject = new JSONObject(response);
                            JSONObject jsonObject1 = jsonObject.getJSONObject("response");
                            JSONObject jsonObject2 = jsonObject1.getJSONObject("data");
                            dormitoryId = jsonObject2.getString("dormitoryId");
                            dormitoryName = jsonObject2.getString("dormitoryName");
                            parkName = jsonObject2.getString("parkName");
                            parcelName = jsonObject2.getString("parcelName");
                            buildingName = jsonObject2.getString("buildingName");
                            roomName = jsonObject2.getString("roomName");
                            enterName = jsonObject2.getString("enterName");


                            tvTitle.setText(parcelName + "地块" + dormitoryName);
                            tvYuanqu.setText(parkName);
                            tvDikuai.setText(parcelName + "地块");


//                            String title = buildingName;
//                            String[] split = title.split("#");
//                            String informationId = split[1];
//                            System.out.println(informationId + "   informationId ");
//                            tvLoufang.setText(informationId);
                            tvLoufang.setText(buildingName);


//                            String fanghao = roomName;
//                            String[] fangh = fanghao.split("-");
//                            String fangjianhao = fangh[1];
//                            System.out.println(fangjianhao + "  fangjianhao ");
//                            tvFangjian.setText(fangjianhao + "寝室");
                            tvFangjian.setText(roomName + "寝室");
                            tvCompang.setText(enterName);


                            JSONObject shui = jsonObject1.getJSONObject("shui");
                            JSONArray data = shui.getJSONArray("data");
                            if (data != null) {
                                if (data.length() >= 1) {
                                    JSONObject object = (JSONObject) data.get(0);
                                    waterUserId = object.getString("waterUserId");
                                    waterUserName = object.getString("waterUserName");
                                    yu = object.getString("yu");
                                }
                                tvShuifeiMoney.setText("¥" + yu);

                                String replace1 = waterUserName.replace(" ", "");
                                tvShuifei.setText(replace1 + "：");
                            }


                            if (data != null) {
                                if (data.length() >= 2) {
                                    JSONObject object = (JSONObject) data.get(1);
                                    waterUserId2 = object.getString("waterUserId");
                                    waterUserName2 = object.getString("waterUserName");
                                    yu2 = object.getString("yu");
                                }
                                tvShuifeiMoney2.setText("¥" + yu2);

                                String replace1 = waterUserName2.replace(" ", "");
                                tvShuifei2.setText(replace1 + "：");
                            }


                            JSONObject dian = jsonObject1.getJSONObject("dian");
                            if (dian != null) {
                                electricityUserId = dian.getString("electricityUserId");
                                electricityUserName = dian.getString("electricityUserName");
                                yudian = dian.getString("yu");

                                tvDianfeiMoney.setText(yudian);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        if (waterUserId == null) {
                            relativeShuifei.setVisibility(View.GONE);
                        }


                        if (waterUserId2 == null) {
                            relativeShuifei2.setVisibility(View.GONE);
                        }


                        if (electricityUserId == null) {
                            relativeDianfei.setVisibility(View.GONE);
                        }


//                        if (response.indexOf("200") != -1) {
//
//                            responseresponse222 = response;
//
//                            Gson gson = new Gson();
//                            qiYeSuSheXiangQingEntity = gson.fromJson(response, QiYeSuSheXiangQingEntity.class);
//
//
//
//
//                            if(qiYeSuSheXiangQingEntity.getResponse().getShui().getCode().indexOf("200") != -1){
//                                if(qiYeSuSheXiangQingEntity.getResponse().getShui().getData().size() >= 1){
//                                    if(qiYeSuSheXiangQingEntity.getResponse().getShui().getData().get(0).getYu() != null){
//                                        tvShuifeiMoney.setText(qiYeSuSheXiangQingEntity.getResponse().getShui().getData().get(0).getYu());
//                                    }
//                                }
//                            }
//
//
//
//                            if(qiYeSuSheXiangQingEntity.getResponse().getShui().getCode().indexOf("200") != -1){
//                                if(qiYeSuSheXiangQingEntity.getResponse().getShui().getData().size() >= 2){
//                                    if(qiYeSuSheXiangQingEntity.getResponse().getShui().getData().get(1).getYu() != null){
//                                        tvShuifeiMoney2.setText(qiYeSuSheXiangQingEntity.getResponse().getShui().getData().get(1).getYu());
//                                    }
//                                }
//                            }
//
//
//
//
//
//
//                            if (qiYeSuSheXiangQingEntity.getResponse().getData().getDormitoryName() == null) {
//
//                            } else {
//                                tvTitle.setText(qiYeSuSheXiangQingEntity.getResponse().getData().getParcelName() + "地块" + qiYeSuSheXiangQingEntity.getResponse().getData().getDormitoryName());
//                            }
//
//
//                            if (qiYeSuSheXiangQingEntity.getResponse().getData().getParkName() == null) {
//
//                            } else {
//                                tvYuanqu.setText(qiYeSuSheXiangQingEntity.getResponse().getData().getParkName());
//                            }
//
//
//                            if (qiYeSuSheXiangQingEntity.getResponse().getData().getParcelName() == null) {
//
//                            } else {
//                                tvDikuai.setText(qiYeSuSheXiangQingEntity.getResponse().getData().getParcelName() + "地块");
//                            }
//
//
////                            楼房
//                            if (qiYeSuSheXiangQingEntity.getResponse().getData().getBuildingName() == null) {
//
//                            } else {
////                                String title = qiYeSuSheXiangQingEntity.getResponse().getData().getBuildingName();
////                                String[] split = title.split("#");
//
//                                tvLoufang.setText(qiYeSuSheXiangQingEntity.getResponse().getData().getBuildingName());
//                            }
//
//
////                            房间号
//                            if (qiYeSuSheXiangQingEntity.getResponse().getData().getRoomName() == null) {
//
//                            } else {
//                                tvFangjian.setText(qiYeSuSheXiangQingEntity.getResponse().getData().getRoomName());
//                            }
//
//
//                            if (qiYeSuSheXiangQingEntity.getResponse().getData().getEnterName() == null) {
//
//                            } else {
//                                tvCompang.setText(qiYeSuSheXiangQingEntity.getResponse().getData().getEnterName());
//                            }
//                        }
                    }
                });
    }


    @OnClick({R.id.tv_changfang, R.id.image_back,R.id.tv_chongzhi1, R.id.tv_chongzhi2, R.id.tv_chongzhi3,R.id.tv_title, R.id.tv_shuifei_money2, R.id.relative_shuifei2, R.id.tv_yuanqu, R.id.tv_dikuai, R.id.tv_loufang, R.id.tv_fangjian, R.id.tv_compang, R.id.tv_shuifei_money, R.id.relative_shuifei, R.id.tv_dianfei_money, R.id.relative_dianfei,
            R.id.tv_one, R.id.tv_two, R.id.tv_three})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.tv_changfang:
                break;
            case R.id.image_back:
                finish();
                break;
            case R.id.tv_title:
                break;
            case R.id.tv_yuanqu:
                break;
            case R.id.tv_dikuai:
                break;
            case R.id.tv_loufang:
                break;
            case R.id.tv_fangjian:
                break;
            case R.id.tv_compang:
                break;
            case R.id.tv_shuifei_money:
                break;
            case R.id.tv_chongzhi1:
                if (waterUserId == null) {
                    Toast.makeText(this, "暂无水表信息！", Toast.LENGTH_SHORT).show();
                    return;
                }
                Intent intent = new Intent(MyQiYeSuSheXiangQingActivity.this, ShuiDianFeiJiLuActivity.class);
                intent.putExtra("yue2", yu2);
                intent.putExtra("waterUserName2", waterUserName2);
                intent.putExtra("id2", waterUserId2);

                intent.putExtra("yue", yu);
                intent.putExtra("waterUserName", waterUserName);
                intent.putExtra("id", waterUserId);


//                intent.putExtra("responseresponse222",responseresponse222);
                startActivity(intent);
                break;
            case R.id.tv_chongzhi2:
                if (waterUserId2 == null) {
                    Toast.makeText(this, "暂无水表信息！", Toast.LENGTH_SHORT).show();
                }
                Intent intent1 = new Intent(MyQiYeSuSheXiangQingActivity.this, ShuiDianFeiJiLuActivity.class);
                intent1.putExtra("yue", yu2);
                intent1.putExtra("waterUserName", waterUserName2);
                intent1.putExtra("id", waterUserId2);

                intent1.putExtra("yue2", yu);
                intent1.putExtra("waterUserName2", waterUserName);
                intent1.putExtra("id2", waterUserId);

//
//                intent1.putExtra("responseresponse222",responseresponse222);
                startActivity(intent1);
                break;
            case R.id.tv_chongzhi3:
                if (electricityUserId == null) {
                    Toast.makeText(this, "暂无电表信息！", Toast.LENGTH_SHORT).show();
                    return;
                }
                Intent intent2 = new Intent(MyQiYeSuSheXiangQingActivity.this, ShuiDianFeiJiLuActivity.class);
                intent2.putExtra("electricityUserId", electricityUserId);
                intent2.putExtra("electricityUserName", electricityUserName);
                intent2.putExtra("yudian", yudian);
                intent2.putExtra("dian", "dian");
                startActivity(intent2);
                break;
//            水费1
            case R.id.relative_shuifei:
                if (waterUserId == null) {
                    Toast.makeText(this, "暂无水表信息！", Toast.LENGTH_SHORT).show();
                    return;
                }
                Intent intent5 = new Intent(MyQiYeSuSheXiangQingActivity.this, ShuiDianFeiJiLuActivity.class);
                intent5.putExtra("yue2", yu2);
                intent5.putExtra("waterUserName2", waterUserName2);
                intent5.putExtra("id2", waterUserId2);

                intent5.putExtra("yue", yu);
                intent5.putExtra("waterUserName", waterUserName);
                intent5.putExtra("id", waterUserId);


//                intent.putExtra("responseresponse222",responseresponse222);
                startActivity(intent5);
                break;
            case R.id.tv_shuifei_money2:
                break;
            //            水费2
            case R.id.relative_shuifei2:
                if (waterUserId2 == null) {
                    Toast.makeText(this, "暂无水表信息！", Toast.LENGTH_SHORT).show();
                }
                Intent intent6 = new Intent(MyQiYeSuSheXiangQingActivity.this, ShuiDianFeiJiLuActivity.class);
                intent6.putExtra("yue", yu2);
                intent6.putExtra("waterUserName", waterUserName2);
                intent6.putExtra("id", waterUserId2);

                intent6.putExtra("yue2", yu);
                intent6.putExtra("waterUserName2", waterUserName);
                intent6.putExtra("id2", waterUserId);


//                intent1.putExtra("responseresponse222",responseresponse222);
                startActivity(intent6);

                break;
            case R.id.tv_dianfei_money:
                break;
//            电费
            case R.id.relative_dianfei:
                if (electricityUserId == null) {
                    Toast.makeText(this, "暂无电表信息！", Toast.LENGTH_SHORT).show();
                    return;
                }
                Intent intent7 = new Intent(MyQiYeSuSheXiangQingActivity.this, ShuiDianFeiJiLuActivity.class);
                intent7.putExtra("electricityUserId", electricityUserId);
                intent7.putExtra("electricityUserName", electricityUserName);
                intent7.putExtra("yudian", yudian);
                intent7.putExtra("dian", "dian");
                startActivity(intent7);
                break;
            case R.id.tv_one:
                HistoryConsumptionActivity.start(MyQiYeSuSheXiangQingActivity.this, "1", waterUserId);
                break;
            case R.id.tv_two:
                HistoryConsumptionActivity.start(MyQiYeSuSheXiangQingActivity.this, "1", waterUserId2);
                break;
            case R.id.tv_three:
                HistoryConsumptionActivity.start(MyQiYeSuSheXiangQingActivity.this, "2", electricityUserId);
                break;
        }
    }




}
