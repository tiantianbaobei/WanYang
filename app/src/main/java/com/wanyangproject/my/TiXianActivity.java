package com.wanyangproject.my;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Display;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.wanyangproject.R;
import com.wanyangproject.entity.TiXianEntity;
import com.wanyangproject.entity.TiXianLieBiaoEntity;
import com.wanyangproject.fuwuactivity.QiYeFuWuLiJiShenQingActivity;
import com.wanyangproject.myadapter.TiXianAdapter;
import com.wanyangproject.utils.ContractUtils;
import com.zhy.http.okhttp.OkHttpUtils;
import com.zhy.http.okhttp.callback.StringCallback;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.Call;

public class TiXianActivity extends AppCompatActivity {

    @BindView(R.id.image_back)
    ImageView imageBack;
    @BindView(R.id.et_jine)
    EditText etJine;
    @BindView(R.id.et_zhifubao)
    EditText etZhifubao;
    @BindView(R.id.et_name)
    EditText etName;
    @BindView(R.id.et_phone)
    EditText etPhone;
    @BindView(R.id.btn_tijiao)
    Button btnTijiao;
    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    private TiXianAdapter tiXianAdapter;
    private TiXianLieBiaoEntity tiXianLieBiaoEntity;
    private String yue;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ti_xian);
        ButterKnife.bind(this);

        Intent intent = getIntent();
        yue = intent.getStringExtra("yue");


//        提现列表的网络请求
        initTiXianLieBiaoHttp();

        initView();
    }

//    提现列表的网络请求
    private void initTiXianLieBiaoHttp() {
        OkHttpUtils.post()
                .url(ContractUtils.LOGIN_URL+"user/capitallist")
                .addHeader("token",ContractUtils.getTOKEN(TiXianActivity.this))
                .addParams("parkId",ContractUtils.getParkId(TiXianActivity.this))
                .build()
                .execute(new StringCallback() {
                    @Override
                    public void onError(Call call, Exception e, int id) {

                    }

                    @Override
                    public void onResponse(String response, int id) {
                        ContractUtils.Code500(TiXianActivity.this,response);
                        System.out.println(response+"     提现列表的网络请求");
                        Gson gson = new Gson();
                        tiXianLieBiaoEntity = gson.fromJson(response, TiXianLieBiaoEntity.class);
                        if(response.indexOf("200") != -1){
                            if(response != null){
                                tiXianAdapter = new TiXianAdapter(TiXianActivity.this,tiXianLieBiaoEntity.getResponse());
                                LinearLayoutManager manager = new LinearLayoutManager(TiXianActivity.this);
                                recyclerView.setLayoutManager(manager);
                                recyclerView.setAdapter(tiXianAdapter);
                            }
                        }
                    }
                });
    }

    private void initView() {
//        tiXianAdapter = new TiXianAdapter(this);
//        LinearLayoutManager manager = new LinearLayoutManager(this);
//        recyclerView.setLayoutManager(manager);
//        recyclerView.setAdapter(tiXianAdapter);
    }

    @OnClick({R.id.image_back, R.id.et_jine, R.id.et_zhifubao, R.id.et_name, R.id.et_phone, R.id.btn_tijiao, R.id.recyclerView})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.image_back:
                finish();
                break;
            case R.id.et_jine:
                break;
            case R.id.et_zhifubao:
                break;
            case R.id.et_name:
                break;
            case R.id.et_phone:
                break;
            case R.id.btn_tijiao:
                if(etJine.getText().toString().equals("")){
                    Toast.makeText(this, "请输入提现金额", Toast.LENGTH_SHORT).show();
                }else if(etZhifubao.getText().toString().equals("")){
                    Toast.makeText(this, "请输入支付宝账号", Toast.LENGTH_SHORT).show();
                }else  if(etName.getText().toString().equals("")){
                    Toast.makeText(this, "请输入姓名", Toast.LENGTH_SHORT).show();
                }else if(etPhone.getText().toString().equals("")) {
                    Toast.makeText(this, "请输入联系电话", Toast.LENGTH_SHORT).show();
                }else {
                    //                提现金额的网络请求
                    initTiXianHttp();
                }
                break;
            case R.id.recyclerView:
                break;
        }
    }


//    提现金额的网络请求
    private void initTiXianHttp() {
        float jine = Float.parseFloat(yue);
        float shurujine = Float.parseFloat(etJine.getText().toString().trim());
        if(jine < shurujine){
            Toast.makeText(this, "金额不足！", Toast.LENGTH_SHORT).show();
            return;
        }




        if (ContractUtils.isChinaPhoneLegal(etPhone.getText().toString().trim()) == false) {
            Toast.makeText(TiXianActivity.this, "请输入正确的手机号", Toast.LENGTH_SHORT).show();
            return;
        }
        ContractUtils.isChinaPhoneLegal(etPhone.getText().toString().trim());



        OkHttpUtils.post()
                .url(ContractUtils.LOGIN_URL+"user/capital")
                .addHeader("token",ContractUtils.getTOKEN(TiXianActivity.this))
                .addParams("parkId",ContractUtils.getParkId(TiXianActivity.this))
                .addParams("money",etJine.getText().toString().trim())
                .addParams("Alipay",etZhifubao.getText().toString().trim())
                .addParams("name",etName.getText().toString().trim())
                .addParams("phone",etPhone.getText().toString().trim())
                .build()
                .execute(new StringCallback() {
                    @Override
                    public void onError(Call call, Exception e, int id) {

                    }

                    @Override
                    public void onResponse(String response, int id) {
                        ContractUtils.Code500(TiXianActivity.this,response);
                        System.out.println(response+"      提现金额的网络请求");

                        if(response.indexOf("200") != -1){
                            Gson gson = new Gson();
                            TiXianEntity tiXianEntity = gson.fromJson(response, TiXianEntity.class);
                            Toast.makeText(TiXianActivity.this, tiXianEntity.getResponse().getMes(), Toast.LENGTH_SHORT).show();

                            etJine.setText("");
                            etZhifubao.setText("");
                            etName.setText("");
                            etPhone.setText("");

                            final AlertDialog alertDialog = new AlertDialog.Builder(TiXianActivity.this).create();
                            alertDialog.show();
                            alertDialog.setContentView(R.layout.tixian_alertdialog);
                            alertDialog.getWindow().setBackgroundDrawableResource(R.drawable.shape_zhuce);
                            WindowManager windowManager=getWindowManager();
                            Display defaultDisplay = windowManager.getDefaultDisplay();
                            WindowManager.LayoutParams attributes = alertDialog.getWindow().getAttributes();
                            attributes.width= (int) (defaultDisplay.getWidth()*0.8);
                            alertDialog.getWindow().setAttributes(attributes);
                            alertDialog.getWindow().findViewById(R.id.tv_queding).setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    alertDialog.dismiss();
                                }
                            });

//                             提现列表的网络请求
                            initTiXianLieBiaoHttp();
                        }else{

                        }
                    }
                });
    }
}
