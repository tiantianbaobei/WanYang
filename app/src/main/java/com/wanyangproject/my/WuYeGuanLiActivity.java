package com.wanyangproject.my;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.wanyangproject.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class WuYeGuanLiActivity extends AppCompatActivity {

    @BindView(R.id.image_back)
    ImageView imageBack;
    @BindView(R.id.image_touxiang)
    ImageView imageTouxiang;
    @BindView(R.id.tv_name_gongsi)
    TextView tvNameGongsi;
    @BindView(R.id.tv_address)
    TextView tvAddress;
    @BindView(R.id.tv_phone)
    TextView tvPhone;
    @BindView(R.id.tv_fangke_guanli)
    TextView tvFangkeGuanli;
    @BindView(R.id.tv_chelian_guanli)
    TextView tvChelianGuanli;
    @BindView(R.id.tv_sushe_guanli)
    TextView tvSusheGuanli;
    @BindView(R.id.tv_zhaopin_guanli)
    TextView tvZhaopinGuanli;
    @BindView(R.id.tv_qiye_guanli)
    TextView tvQiyeGuanli;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wu_ye_guan_li);
        ButterKnife.bind(this);
    }

    @OnClick({R.id.image_back, R.id.image_touxiang, R.id.tv_name_gongsi, R.id.tv_address, R.id.tv_phone, R.id.tv_fangke_guanli, R.id.tv_chelian_guanli, R.id.tv_sushe_guanli, R.id.tv_zhaopin_guanli, R.id.tv_qiye_guanli})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.image_back:
                finish();
                break;
//            头像
            case R.id.image_touxiang:
                break;
//            公司名称
            case R.id.tv_name_gongsi:
                break;
//            、公司地址
            case R.id.tv_address:
                break;
//            联系电话
            case R.id.tv_phone:
                break;
//            访客管理
            case R.id.tv_fangke_guanli:
//                Intent intent1 = new Intent(WuYeGuanLiActivity.this, MyMenWeiFangKeGuanLiActivity.class);
                Intent intent1 = new Intent(WuYeGuanLiActivity.this, MyFangKeGuanLiActivity.class);
                startActivity(intent1);
                break;
//            车辆管理
            case R.id.tv_chelian_guanli:
                Intent intent2 = new Intent(WuYeGuanLiActivity.this,CheLiangGuanLiActivity.class);
                startActivity(intent2);
                break;
//            宿舍管理
            case R.id.tv_sushe_guanli:
                Intent intent = new Intent(WuYeGuanLiActivity.this,WuYeSuSheGuanLiActivity.class);
                startActivity(intent);
                break;
//            、招聘管理
            case R.id.tv_zhaopin_guanli:
                Intent intent5 = new Intent(WuYeGuanLiActivity.this, ZhaoPinGuanLiActivity.class);
                startActivity(intent5);
                break;
//            企业管理
            case R.id.tv_qiye_guanli:
                Intent intent3 = new Intent(WuYeGuanLiActivity.this,WuYeQiYeGuanLiActivity.class);
                startActivity(intent3);
                break;
        }
    }
}
