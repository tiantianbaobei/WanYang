package com.wanyangproject.my;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.wanyangproject.R;
import com.wanyangproject.entity.ShangJiaXianShiJineEntity;
import com.wanyangproject.myadapter.ShouRuMingXiAdapter;
import com.wanyangproject.utils.ContractUtils;
import com.zhy.http.okhttp.OkHttpUtils;
import com.zhy.http.okhttp.callback.StringCallback;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.Call;

public class WoDeYuEActivity extends AppCompatActivity {

    @BindView(R.id.image_back)
    ImageView imageBack;
    @BindView(R.id.tv_zhanghu_yue)
    TextView tvZhanghuYue;
    @BindView(R.id.tv_money)
    TextView tvMoney;
    @BindView(R.id.tv_tixian)
    TextView tvTixian;
    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    private ShouRuMingXiAdapter shouRuMingXiAdapter;
    private ShangJiaXianShiJineEntity shangJiaXianShiJineEntity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wo_de_yu_e);
        ButterKnife.bind(this);
        initView();

//        商家我的用户资金显示的网络请求
        initWoDeYueHttp();

    }

//    商家我的用户资金显示的网络请求
    private void initWoDeYueHttp() {
        OkHttpUtils.post()
                .url(ContractUtils.LOGIN_URL+"user/balance")
                .addHeader("token",ContractUtils.getTOKEN(WoDeYuEActivity.this))
                .addParams("parkId",ContractUtils.getParkId(WoDeYuEActivity.this))
                .build()
                .execute(new StringCallback() {
                    @Override
                    public void onError(Call call, Exception e, int id) {
                        System.out.println(e+"     用户资金显示的网络请求eeeeeee");
                    }

                    @Override
                    public void onResponse(String response, int id) {
                        ContractUtils.Code500(WoDeYuEActivity.this,response);
                        System.out.println(response+"           用户资金显示的网络请求");

                        if(response.indexOf("200") != -1){
                            Gson gson = new Gson();
                            shangJiaXianShiJineEntity = gson.fromJson(response, ShangJiaXianShiJineEntity.class);
                            if(shangJiaXianShiJineEntity.getResponse().getMoney() == null){

                            }else{
                                tvMoney.setText(shangJiaXianShiJineEntity.getResponse().getMoney());
                            }



                            //        收入明细
                            shouRuMingXiAdapter = new ShouRuMingXiAdapter(WoDeYuEActivity.this,shangJiaXianShiJineEntity.getResponse().getCapital());
                            LinearLayoutManager manager = new LinearLayoutManager(WoDeYuEActivity.this);
                            recyclerView.setLayoutManager(manager);
                            recyclerView.setAdapter(shouRuMingXiAdapter);


                        }
                    }
                });
    }




    private void initView() {
////        收入明细
//        shouRuMingXiAdapter = new ShouRuMingXiAdapter(this);
//        LinearLayoutManager manager = new LinearLayoutManager(this);
//        recyclerView.setLayoutManager(manager);
//        recyclerView.setAdapter(shouRuMingXiAdapter);
    }

    @OnClick({R.id.image_back, R.id.tv_zhanghu_yue, R.id.tv_money, R.id.tv_tixian, R.id.recyclerView})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.image_back:
                finish();
                break;
            case R.id.tv_zhanghu_yue:
                break;
            case R.id.tv_money:
                break;
            case R.id.tv_tixian:
                Intent intent = new Intent(WoDeYuEActivity.this,TiXianActivity.class);
                intent.putExtra("yue",shangJiaXianShiJineEntity.getResponse().getMoney());
                startActivity(intent);
                break;
            case R.id.recyclerView:
                break;
        }
    }
}
