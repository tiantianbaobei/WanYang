package com.wanyangproject;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.gson.Gson;
import com.wanyangproject.activity.DiTuActivity;
import com.wanyangproject.entity.QiYeXinXiEntity;
import com.wanyangproject.shouye.QiYeXiangQingActivity;
import com.wanyangproject.utils.ContractUtils;
import com.zhy.http.okhttp.OkHttpUtils;
import com.zhy.http.okhttp.callback.StringCallback;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;
import okhttp3.Call;

public class MenWeiQiYeXinXiActivity extends AppCompatActivity {

    @BindView(R.id.image_back)
    ImageView imageBack;
    @BindView(R.id.image_touxiang)
    CircleImageView imageTouxiang;
    @BindView(R.id.tv_title)
    TextView tvTitle;
    @BindView(R.id.tv_phone)
    TextView tvPhone;
    @BindView(R.id.tv_address)
    TextView tvAddress;
    @BindView(R.id.tv_jianjie)
    TextView tvJianjie;
    private QiYeXinXiEntity qiYeXinXiEntity;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_men_wei_qi_ye_xin_xi);
        ButterKnife.bind(this);


//        企业信息的网络请求
        initQiYeXinXiHttp();

    }


//    企业信息的网络请求
    private void initQiYeXinXiHttp() {
        OkHttpUtils.post()
                .url(ContractUtils.LOGIN_URL+"company/getDetailyuan")
                .addHeader("token",ContractUtils.getTOKEN(MenWeiQiYeXinXiActivity.this))
                .addParams("parkId",ContractUtils.getParkId(MenWeiQiYeXinXiActivity.this))
                .build()
                .execute(new StringCallback() {
                    @Override
                    public void onError(Call call, Exception e, int id) {
                        System.out.println(e+"      eeeeeeaaaaaaaa");
                    }

                    @Override
                    public void onResponse(String response, int id) {
                        System.out.println(response+"            企业信息的网络请求");
                        ContractUtils.Code400(MenWeiQiYeXinXiActivity.this,response);
                        if(response.indexOf("200") != -1){
                            Gson gson = new Gson();
                            qiYeXinXiEntity = gson.fromJson(response, QiYeXinXiEntity.class);
                            if(qiYeXinXiEntity != null){
                                if(qiYeXinXiEntity.getResponse().getInfo().getEnterName() != null){
                                    tvTitle.setText(qiYeXinXiEntity.getResponse().getInfo().getEnterName());
                                }

                                if(qiYeXinXiEntity.getResponse().getInfo().getContact() != null){
                                    tvPhone.setText("联系电话："+qiYeXinXiEntity.getResponse().getInfo().getContact());
                                }


                                if(qiYeXinXiEntity.getResponse().getInfo().getAddress() != null){
                                    tvAddress.setText("企业位置："+qiYeXinXiEntity.getResponse().getInfo().getAddress());
                                }


                                if(qiYeXinXiEntity.getResponse().getInfo().getDesc() != null){
                                    tvJianjie.setText(qiYeXinXiEntity.getResponse().getInfo().getDesc());
                                }


                                if(qiYeXinXiEntity.getResponse().getInfo().getLogoUrl() != null){
                                    Glide.with(MenWeiQiYeXinXiActivity.this).load(qiYeXinXiEntity.getResponse().getInfo().getLogoUrl()).into(imageTouxiang);
                                }
                            }
                        }
                    }
                });
    }




    @OnClick({R.id.image_back, R.id.image_touxiang, R.id.tv_title, R.id.tv_phone, R.id.tv_address, R.id.tv_jianjie})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.image_back:
                finish();
                break;
            case R.id.image_touxiang:
                break;
            case R.id.tv_title:
                break;
            case R.id.tv_phone:
                break;
            case R.id.tv_address:
                if(qiYeXinXiEntity == null){
                    return;
                }
                if(qiYeXinXiEntity.getResponse().getInfo().getLat().equals("") && qiYeXinXiEntity.getResponse().getInfo().getLng().equals("")){
                    Toast.makeText(this, "暂无地址！", Toast.LENGTH_SHORT).show();
                    return;
                }
                Intent intent = new Intent(MenWeiQiYeXinXiActivity.this, DiTuActivity.class);
                intent.putExtra("lat",qiYeXinXiEntity.getResponse().getInfo().getLat());
                intent.putExtra("lng",qiYeXinXiEntity.getResponse().getInfo().getLng());
                intent.putExtra("name",qiYeXinXiEntity.getResponse().getInfo().getEnterName());
                startActivity(intent);

                break;
            case R.id.tv_jianjie:
                break;
        }
    }
}
