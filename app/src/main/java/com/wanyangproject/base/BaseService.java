package com.wanyangproject.base;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.support.v4.content.LocalBroadcastManager;

public class BaseService extends Service {

    protected Service mService;
    protected LocalBroadcastManager mLBMgr;

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        mService = this;
        mLBMgr = LocalBroadcastManager.getInstance(mService);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mService = null;
    }
}
