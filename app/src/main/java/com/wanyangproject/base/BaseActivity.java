package com.wanyangproject.base;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.wanyangproject.net.HttpUtil;
import com.wanyangproject.net.util.ACache;
import com.wanyangproject.net.util.AppSession;
import com.wanyangproject.net.util.UiUtils;


public abstract class BaseActivity extends AppCompatActivity {

    protected Context mContext;
    protected AppSession mSession;
    protected HttpUtil mHttpUtil;
    protected ACache mCache;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(getContentResId());
        mContext = this;
        UiUtils.initSoftInputHide(this);
        mCache = ACache.get(mContext);
        mSession = AppSession.getInstance(mContext.getApplicationContext());
//        mHttpUtil = HttpUtil.getInstance();
        init();
    }

    public abstract int getContentResId();
    public abstract void init();

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mContext = null;
        mSession = null;
    }
}