package com.wanyangproject.base;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AppOpsManager;
import android.app.Application;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.StrictMode;
import android.provider.SyncStateContract;
import android.support.multidex.MultiDex;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.util.DisplayMetrics;
import android.util.Log;
import android.widget.Toast;

import com.baidu.mapapi.SDKInitializer;
import com.mob.MobSDK;
//import com.tencent.bugly.crashreport.CrashReport;
import com.tencent.mm.opensdk.openapi.IWXAPI;
import com.tencent.mm.opensdk.openapi.WXAPIFactory;
import com.tencent.smtt.sdk.QbSdk;
import com.umeng.commonsdk.UMConfigure;
import com.umeng.message.IUmengRegisterCallback;
import com.umeng.message.PushAgent;
import com.umeng.message.UTrack;
import com.umeng.message.UmengMessageHandler;
import com.umeng.message.UmengNotificationClickHandler;
import com.umeng.message.entity.UMessage;
import com.wanyangproject.fuwuactivity.FuWuMessageActivity;
import com.wanyangproject.service.UmengNotificationService;
import com.wanyangproject.utils.ContractUtils;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import cn.sharesdk.sina.weibo.SinaWeibo;

/**
 * Created by 甜甜 on 2018/7/11.
 */

public class MyApp extends Application{

    private static Context context;
    public static final String UPDATE_STATUS_ACTION = "com.umeng.message.example.action.UPDATE_STATUS";
    //    微信的APP ID
    private static final String APP_ID_WEChat = "wx363ae6178db5e958";
    //    IWXAPI是第三方app和微信通信的openapi接口
    public static IWXAPI iwxapi;

    @Override
    public void onCreate() {
        super.onCreate();
//        CrashReport.initCrashReport(getApplicationContext(), "aa06398ca7", false);
        context = getApplicationContext();
        MobSDK.init(this);//分享
        MultiDex.install(this);
        SDKInitializer.initialize(getApplicationContext());
        initTBS();


//        //        微信
//        regToWx();



//        友盟推送
        UMConfigure.init(context, "5b96681df29d9874ce00028d", "", UMConfigure.DEVICE_TYPE_PHONE, "461412296b6a9a8af3527b993ad2f769");
        PushAgent mPushAgent = PushAgent.getInstance(this);
//         注册推送服务，每次调用register方法都会回调该接口
        mPushAgent.register(new IUmengRegisterCallback() {

            @Override
            public void onSuccess(String deviceToken) {
                //注册成功会返回device token
                sendBroadcast(new Intent(UPDATE_STATUS_ACTION));

                System.out.println(deviceToken+"         deviceTokendeviceTokendeviceToken ");
            }

            @Override
            public void onFailure(String s, String s1) {
                System.out.println(s+"      ssssssssss");
                System.out.println(s1+"         ssssssssss1111111111111");
                sendBroadcast(new Intent(UPDATE_STATUS_ACTION));
            }
        });
        PushAgent.getInstance(context).onAppStart();





//        收到通知
        UmengMessageHandler messageHandler = new UmengMessageHandler() {
            /**
             * 通知的回调方法
             * @param context
             * @param msg
             */
            @Override
            public void dealWithNotificationMessage(Context context, UMessage msg) {
                System.out.println(msg.custom+"         customcustomcustomcustom ");
                System.out.println(msg.after_open+"          after_openafter_open ");
                //调用super则会走通知展示流程，不调用super则不展示通知
                super.dealWithNotificationMessage(context, msg);
//                msg.custom = "0";
                System.out.println(msg.toString()+"           msg111111111");

//                Intent intent00 = new Intent(getContext(), FuWuMessageActivity.class);
//                startActivity(intent00);
//                Toast.makeText(context, "2222222222", Toast.LENGTH_SHORT).show();
            }
        };
        mPushAgent.setMessageHandler(messageHandler);





        UmengNotificationClickHandler notificationClickHandler = new UmengNotificationClickHandler() {
            @Override
            public void dealWithCustomAction(Context context, UMessage msg) {
                System.out.println(msg.toString()+"      msg222222222");
                Intent intent00 = new Intent(context, FuWuMessageActivity.class);
                intent00.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent00);
//                Toast.makeText(context, msg.custom, Toast.LENGTH_LONG).show();
            }
        };
        mPushAgent.setNotificationClickHandler(notificationClickHandler);





//        if(ContractUtils.getPhone(context).equals("phone")){
//
//        }else{
////            mPushAgent.setAlias(ContractUtils.getPhone(context), "00", new UTrack.ICallBack() {
////                @Override
////                public void onMessage(boolean isSuccess, String message) {
////                    System.out.println(isSuccess+"           isSuccess");
////                    System.out.println(message+"            message");
////                }
////            });
//        }

        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());
        builder.detectFileUriExposure();
    }




//    @Override
//    public Resources getResources() {
//        Resources res = super.getResources();
//        Configuration config = res.getConfiguration();
//        config.fontScale = 1.5f; //1 设置正常字体大小的倍数，大于1就是放大，小于1就是缩小
//        res.updateConfiguration(config, res.getDisplayMetrics());
//        return res;
//    }



    private void initTBS() {
        QbSdk.initX5Environment(context, new QbSdk.PreInitCallback() {
            @Override
            public void onCoreInitFinished() {
                Log.e("www", "onCoreInitFinished: ");
            }

            @Override
            public void onViewInitFinished(boolean x5IsLoad) {
                Log.e("www", "onViewInitFinished: " + x5IsLoad);
            }
        });
    }


    public static Context getContext() {
        return context;
    }



//    private void regToWx() {
////        通过WXAPOFactory工厂，获取IWXAPI的实例
//        iwxapi = WXAPIFactory.createWXAPI(this, APP_ID_WEChat);
////        将应用的APP ID注册到微信
//        iwxapi.registerApp(APP_ID_WEChat);
//    }


}
