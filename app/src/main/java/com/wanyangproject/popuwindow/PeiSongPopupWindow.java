package com.wanyangproject.popuwindow;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.NumberPicker;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.wanyangproject.R;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * Created by 甜甜 on 2018/7/28.
 */

public class PeiSongPopupWindow extends PopupWindow implements View.OnClickListener {

    //public class PeiSongPopupWindow extends PopupWindow implements View.OnClickListener {
//    private Button btn_quxiao, btn_queding;
    private View mView;
//    private NumberPicker numberPicker;
    private TextView tv_quanbu,tv_ziqu,tv_peisong;

//    private String[] number = {"超市1","超市2","超市3","超市4","超市5","超市6","超市7","超市8"};
//    private ArrayList<String> leixingtitle = new ArrayList<>();
//    private ArrayList<String> leixingid = new ArrayList<>();
//    private String[] title;



    private PeiSongClick peiSongClick;

    public void setPeiSongClick(PeiSongClick peiSongClick) {
        this.peiSongClick = peiSongClick;
    }

    public interface PeiSongClick{
        void peisongClick(String title);
    }




    public PeiSongPopupWindow(Context context, View.OnClickListener itemsOnClick) {
        super(context);
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        mView = inflater.inflate(R.layout.peisong_popupwindow, null);
        tv_quanbu = mView.findViewById(R.id.tv_quanbu);
        tv_ziqu = mView.findViewById(R.id.tv_ziqu);
        tv_peisong = mView.findViewById(R.id.tv_peisong);

        tv_quanbu.setOnClickListener(this);
        tv_ziqu.setOnClickListener(this);
        tv_peisong.setOnClickListener(this);


//        for (int i = 0; i < ShangJiaRuZhuDianPuLeiXingEntity.dianpuleixing.size(); i++) {
//            ShangJiaRuZhuDianPuLeiXingEntity.ResponseBean responseBean = ShangJiaRuZhuDianPuLeiXingEntity.dianpuleixing.get(i);
//
//
////            WuYeFuWuLieBiaoEntity.ResponseBean.DataBean dataBean = WuYeFuWuLieBiaoEntity.ResponseBean.leixing.get(i);
//            leixingtitle.add(responseBean.getName());
//            leixingid.add(responseBean.getId());
//        }
//        title = new String[ShangJiaRuZhuDianPuLeiXingEntity.dianpuleixing.size()];
//        leixingtitle.toArray(title);
//
//


//        numberPicker = (NumberPicker) mView.findViewById(R.id.numberPicker);
//        numberPicker.setDisplayedValues(title);
//        numberPicker.setMinValue(0);
//        numberPicker.setMaxValue(title.length-1);
//
//        btn_quxiao = (Button) mView.findViewById(R.id.btn_quxiao);
//        btn_queding = (Button) mView.findViewById(R.id.btn_queding);
//        numberPicker = (NumberPicker) mView.findViewById(R.id.numberPicker);
//
//        btn_quxiao.setOnClickListener(this);
//        btn_queding.setOnClickListener(this);
//        numberPicker.setOnClickListener(this);


//        设置PopupWindow的view
        this.setContentView(mView);
//        设置PopupWindow弹出窗体的宽
        this.setWidth(RelativeLayout.LayoutParams.MATCH_PARENT);
//        设置PopupWindow弹出窗体的高
        this.setHeight(RelativeLayout.LayoutParams.WRAP_CONTENT);
//        设置PopupWindow弹出窗体可点击
        this.setFocusable(true);
//        设置SelectPicPopupWindow弹出窗体动画效果
//        this.setAnimationStyle(R.style.Animation);
//        实例化一个ColorDrawable颜色为半透明
        ColorDrawable colorDrawable = new ColorDrawable(0xb0000000);
//        设置SelectPicPopupWindow弹出窗体的背景
        this.setBackgroundDrawable(colorDrawable);
//        mView添加OnTouchListener监听判断获取触屏位置如果在选择框外面则销毁弹出框
        mView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {

                int height = mView.findViewById(R.id.linear_peisong).getTop();
                int y = (int) event.getY();
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    if (y < height) {
                        dismiss();
                    }
                }
                return true;
            }
        });
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.tv_quanbu:
                tv_quanbu.setTextColor(Color.parseColor("#ff0000"));
                tv_ziqu.setTextColor(Color.parseColor("#848484"));
                tv_peisong.setTextColor(Color.parseColor("#848484"));
                peiSongClick.peisongClick(tv_quanbu.getText().toString());
                dismiss();
                break;
            case R.id.tv_ziqu:
                tv_quanbu.setTextColor(Color.parseColor("#848484"));
                tv_ziqu.setTextColor(Color.parseColor("#ff0000"));
                tv_peisong.setTextColor(Color.parseColor("#848484"));
                peiSongClick.peisongClick(tv_ziqu.getText().toString());
                dismiss();
                break;
            case R.id.tv_peisong:
                tv_quanbu.setTextColor(Color.parseColor("#848484"));
                tv_ziqu.setTextColor(Color.parseColor("#848484"));
                tv_peisong.setTextColor(Color.parseColor("#ff0000"));
                peiSongClick.peisongClick(tv_peisong.getText().toString());
                dismiss();
                break;
        }
    }

//    @Override
//    public void onClick(View v) {
//        switch (v.getId()) {
////            点击取消
//            case R.id.btn_quxiao:
//                dismiss();
//                break;
////            点击确定
//            case R.id.btn_queding:
//                try {
//                    dianPuLeiXingPopupWindowClick.dianpuleixingPopupWindowClick(title[numberPicker.getValue()],leixingid.get(numberPicker.getValue()));
////                    xuanZeFenLeiClick.xuanzefenleiClick(title[numberPicker.getValue()],fenleiid.get(numberPicker.getValue()));
//                    dismiss();
//                } catch (Exception e) {
//                    e.printStackTrace();
//                }
//                break;
//        }
//    }
}
