package com.wanyangproject.popuwindow;

import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.NumberPicker;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;

import com.wanyangproject.R;

/**
 * Created by 甜甜 on 2018/8/3.
 */

public class ReShuiLengShuiPopupWindow extends PopupWindow implements View.OnClickListener {

    private Button btn_quxiao,btn_queding;
    private View mView;
    private NumberPicker numberPicker;
    private String[] state = {"热水","冷水"};



    private ShuiClick shuiClick;

    public void setShuiClick(ShuiClick shuiClick) {
        this.shuiClick = shuiClick;
    }

    public interface ShuiClick{
        void shuiclick(String state);
    }


    public ReShuiLengShuiPopupWindow(Context context, View.OnClickListener itemsOnClick){
        super(context);
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        mView = inflater.inflate(R.layout.item_chongzhi_leixing, null);

        numberPicker = (NumberPicker) mView.findViewById(R.id.numberPicker);
        numberPicker.setDisplayedValues(state);
        numberPicker.setMinValue(0);
        numberPicker.setMaxValue(state.length-1);

        btn_quxiao = (Button) mView.findViewById(R.id.btn_quxiao);
        btn_queding = (Button) mView.findViewById(R.id.btn_queding);
        numberPicker = (NumberPicker) mView.findViewById(R.id.numberPicker);

        btn_quxiao.setOnClickListener(this);
        btn_queding.setOnClickListener(this);
        numberPicker.setOnClickListener(this);

        //        设置PopupWindow的view
        this.setContentView(mView);
//        设置PopupWindow弹出窗体的宽
        this.setWidth(RelativeLayout.LayoutParams.MATCH_PARENT);
//        设置PopupWindow弹出窗体的高
        this.setHeight(RelativeLayout.LayoutParams.WRAP_CONTENT);
//        设置PopupWindow弹出窗体可点击
        this.setFocusable(true);
//        设置SelectPicPopupWindow弹出窗体动画效果
        this.setAnimationStyle(R.style.Animation);
//        实例化一个ColorDrawable颜色为半透明
        ColorDrawable colorDrawable = new ColorDrawable(0xb0000000);
//        设置SelectPicPopupWindow弹出窗体的背景
        this.setBackgroundDrawable(colorDrawable);
//        mView添加OnTouchListener监听判断获取触屏位置如果在选择框外面则销毁弹出框
        mView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {

                int height = mView.findViewById(R.id.relative_chongzhi).getTop();
                int y = (int) event.getY();
                if(event.getAction()==MotionEvent.ACTION_UP){
                    if(y < height){
                        dismiss();
                    }
                }
                return true;
            }
        });

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.btn_quxiao:
                dismiss();
                break;
            case R.id.btn_queding:
                shuiClick.shuiclick(String.valueOf(state[numberPicker.getValue()]));
                dismiss();
                break;
        }
    }







}
