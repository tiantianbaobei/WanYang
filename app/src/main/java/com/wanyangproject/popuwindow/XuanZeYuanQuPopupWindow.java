package com.wanyangproject.popuwindow;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.NumberPicker;
import android.widget.PopupMenu;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.wanyangproject.R;
import com.wanyangproject.entity.HuoQuYuanQuLieBiaoEntity;
import com.wanyangproject.entity.LunTanEntity;
import com.wanyangproject.utils.ContractUtils;
import com.zhy.http.okhttp.OkHttpUtils;
import com.zhy.http.okhttp.callback.StringCallback;

import java.util.ArrayList;
import java.util.List;

import okhttp3.Call;

/**
 * Created by 甜甜 on 2018/8/10.
 */

public class XuanZeYuanQuPopupWindow  {
//public class XuanZeYuanQuPopupWindow extends PopupWindow implements View.OnClickListener {
//
//    private Button btn_quxiao,btn_queding;
//    private View mView;
//    private NumberPicker numberPicker;
////    private String[] number = {"超市1","超市2","超市3","超市4","超市5","超市6","超市7","超市8"};
//    private ArrayList<String> yqname = new ArrayList<>();
//    private ArrayList<String> yqid = new ArrayList<>();
//    private String[] str;
//
//
//
//
//    public XuanZeYuanQuPopupWindow(Context context, View.OnClickListener itemsOnClick){
//        super(context);
//        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
//        mView = inflater.inflate(R.layout.xuanze_yuanqu_popupwindow, null);
//
//        numberPicker = (NumberPicker) mView.findViewById(R.id.numberPicker);
////        Array转String
////        String[] str = {"超市1","超市2","超市3","超市4","超市5","超市6","超市7","超市8"};
//
//        for (int i = 0; i < HuoQuYuanQuLieBiaoEntity.mmm.size(); i++) {
//            HuoQuYuanQuLieBiaoEntity.MsgBean ll = HuoQuYuanQuLieBiaoEntity.mmm.get(i);
//            yqname.add(ll.getParkName());
//            yqid.add(ll.getParkId());
//            System.out.println(ll.getParkName()+"    namename");
//            System.out.println(ll.getParkId()+"     ididid");
//        }
//        str = new String[HuoQuYuanQuLieBiaoEntity.mmm.size()];
//        yqname.toArray(str);
//        System.out.println(str+"    str");
//
//        numberPicker.setDisplayedValues(str);
//        numberPicker.setMinValue(0);
//        numberPicker.setMaxValue(str.length -1);
//
//
//
//        btn_quxiao = (Button) mView.findViewById(R.id.btn_quxiao);
//        btn_queding = (Button) mView.findViewById(R.id.btn_queding);
//        numberPicker = (NumberPicker) mView.findViewById(R.id.numberPicker);
//
//        btn_quxiao.setOnClickListener(this);
//        btn_queding.setOnClickListener(this);
//        numberPicker.setOnClickListener(this);
//
//
////        设置PopupWindow的view
//        this.setContentView(mView);
////        设置PopupWindow弹出窗体的宽
//        this.setWidth(RelativeLayout.LayoutParams.MATCH_PARENT);
////        设置PopupWindow弹出窗体的高
//        this.setHeight(RelativeLayout.LayoutParams.WRAP_CONTENT);
////        设置PopupWindow弹出窗体可点击
//        this.setFocusable(true);
////        设置SelectPicPopupWindow弹出窗体动画效果
//        this.setAnimationStyle(R.style.Animation);
////        实例化一个ColorDrawable颜色为半透明
//        ColorDrawable colorDrawable = new ColorDrawable(0xb0000000);
////        设置SelectPicPopupWindow弹出窗体的背景
//        this.setBackgroundDrawable(colorDrawable);
////        mView添加OnTouchListener监听判断获取触屏位置如果在选择框外面则销毁弹出框
//        mView.setOnTouchListener(new View.OnTouchListener() {
//            @Override
//            public boolean onTouch(View v, MotionEvent event) {
//
//                int height = mView.findViewById(R.id.relative_xuanze_yuanqu).getTop();
//                int y = (int) event.getY();
//                if(event.getAction()==MotionEvent.ACTION_UP){
//                    if(y < height){
//                        dismiss();
//                    }
//                }
//                return true;
//            }
//        });
//    }
//
//
//    private XuanZeYuanQuClick xuanZeYuanQuClick;
//
//    public void setXuanZeYuanQuClick(XuanZeYuanQuClick xuanZeYuanQuClick) {
//        this.xuanZeYuanQuClick = xuanZeYuanQuClick;
//    }
//
//    public interface XuanZeYuanQuClick{
//        void xuanzeyuanquClick(String title,String yuanid);
//    }
//
//
//    @Override
//    public void onClick(View view) {
//        switch (view.getId()) {
////            点击取消
//            case R.id.btn_quxiao:
//                dismiss();
//                break;
////            点击确定
//            case R.id.btn_queding:
//                try {
//                    System.out.println(str[numberPicker.getValue()]+"  title");
//                    System.out.println(yqid.get(numberPicker.getValue())+"  id");
//                    xuanZeYuanQuClick.xuanzeyuanquClick(str[numberPicker.getValue()],yqid.get(numberPicker.getValue()));
//                    dismiss();
//                } catch (Exception e) {
//                    e.printStackTrace();
//                }
//                break;
//        }
//    }
}
