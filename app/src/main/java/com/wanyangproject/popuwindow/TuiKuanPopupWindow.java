package com.wanyangproject.popuwindow;

import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.NumberPicker;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.wanyangproject.R;
import com.wanyangproject.my.TuiKuanActivity;

/**
 * Created by 甜甜 on 2018/7/30.
 */

public class TuiKuanPopupWindow extends PopupWindow implements View.OnClickListener {
//    private Button btn_quxiao,btn_queding;
    private View mView;
//    private NumberPicker numberPicker;
//    private String[] number = {"质量问题","假冒品牌","未按约定时间发货","颜色/尺寸/参数不符"};
    private TextView tv_zhiliang,tv_jiamao,tv_fahuo,tv_bufu;


    private TuiKuanClick tuiKuanClick;

    public void setTuiKuanClick(TuiKuanClick tuiKuanClick) {
        this.tuiKuanClick = tuiKuanClick;
    }



    public interface TuiKuanClick{
        void tuikuanClick(String title);
    }


    public TuiKuanPopupWindow(Context context, View.OnClickListener itemsOnClick){
        super(context);
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        mView = inflater.inflate(R.layout.tuikuan_popupwindow, null);

        tv_zhiliang = mView.findViewById(R.id.tv_zhiliang);
        tv_jiamao = mView.findViewById(R.id.tv_jiamao);
        tv_fahuo = mView.findViewById(R.id.tv_fahuo);
        tv_bufu = mView.findViewById(R.id.tv_bufu);

        tv_zhiliang.setOnClickListener(this);
        tv_jiamao.setOnClickListener(this);
        tv_fahuo.setOnClickListener(this);
        tv_bufu.setOnClickListener(this);

//        numberPicker = (NumberPicker) mView.findViewById(R.id.numberPicker);
//        numberPicker.setDisplayedValues(number);
//        numberPicker.setMinValue(0);
//        numberPicker.setMaxValue(number.length-1);
//
//        btn_quxiao = (Button) mView.findViewById(R.id.btn_quxiao);
//        btn_queding = (Button) mView.findViewById(R.id.btn_queding);
//        numberPicker = (NumberPicker) mView.findViewById(R.id.numberPicker);
//
//        btn_quxiao.setOnClickListener(this);
//        btn_queding.setOnClickListener(this);
//        numberPicker.setOnClickListener(this);


//        设置PopupWindow的view
        this.setContentView(mView);
//        设置PopupWindow弹出窗体的宽
        this.setWidth(RelativeLayout.LayoutParams.MATCH_PARENT);
//        设置PopupWindow弹出窗体的高
        this.setHeight(RelativeLayout.LayoutParams.WRAP_CONTENT);
//        设置PopupWindow弹出窗体可点击
        this.setFocusable(true);
//        设置SelectPicPopupWindow弹出窗体动画效果
        this.setAnimationStyle(R.style.Animation);
//        实例化一个ColorDrawable颜色为半透明
        ColorDrawable colorDrawable = new ColorDrawable(0xb0000000);
//        设置SelectPicPopupWindow弹出窗体的背景
        this.setBackgroundDrawable(colorDrawable);
//        mView添加OnTouchListener监听判断获取触屏位置如果在选择框外面则销毁弹出框
        mView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {

                int height = mView.findViewById(R.id.relative_tuikuan).getTop();
                int y = (int) event.getY();
                if(event.getAction()==MotionEvent.ACTION_UP){
                    if(y < height){
                        dismiss();
                    }
                }
                return true;
            }
        });
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.tv_zhiliang:
                tuiKuanClick.tuikuanClick(tv_zhiliang.getText().toString());
                dismiss();
                break;
            case R.id.tv_jiamao:
                tuiKuanClick.tuikuanClick(tv_jiamao.getText().toString());
                dismiss();
                break;
            case R.id.tv_fahuo:
                tuiKuanClick.tuikuanClick(tv_fahuo.getText().toString());
                dismiss();
                break;
            case R.id.tv_bufu:
                tuiKuanClick.tuikuanClick(tv_bufu.getText().toString());
                dismiss();
                break;
        }
    }



//    public void onClick(View v) {
//        switch (v.getId()) {
////            点击取消
//            case R.id.btn_quxiao:
//                dismiss();
//                break;
////            点击确定
//            case R.id.btn_queding:
//                try {
//                    tuiKuanClick.tuikuanClick(String.valueOf(number[numberPicker.getValue()]));
//                    dismiss();
//                } catch (Exception e) {
//                    e.printStackTrace();
//                }
//                break;
//        }
//    }
}
