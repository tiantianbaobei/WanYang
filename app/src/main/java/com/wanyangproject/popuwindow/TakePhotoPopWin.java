package com.wanyangproject.popuwindow;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.provider.MediaStore;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.wanyangproject.R;

/**
 * Created by 甜甜 on 2018/7/17.
 */

public class TakePhotoPopWin extends PopupWindow implements View.OnClickListener{
    private TextView open_from_camera;
    private TextView open_album;
    private TextView tv_cancel_quxiao;
    private View mView;
    private Context context;
    private int num;
    private OnClickXiangji onClickXiangji;
    private OnClickXiangce onClickXiangce;

    public void setOnClickXiangce(OnClickXiangce onClickXiangce) {
        this.onClickXiangce = onClickXiangce;
    }

    public void setOnClickXiangji(OnClickXiangji onClickXiangji) {
        this.onClickXiangji = onClickXiangji;
    }

    public interface OnClickXiangji{
        void onClick(int id);
    }

    public interface OnClickXiangce{
        void onClick(int id);
    }




    public TakePhotoPopWin(Context context, View.OnClickListener itemsOnClick) {
        super(context);
        this.context=context;
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        mView = inflater.inflate(R.layout.layout_paishe_shangchuan_popupwindow, null);



        open_from_camera = (TextView) mView.findViewById(R.id.open_from_camera);
        open_album = (TextView) mView.findViewById(R.id.open_album);
        tv_cancel_quxiao = (TextView) mView.findViewById(R.id.tv_cancel_quxiao);
//        点击按钮的监听
        tv_cancel_quxiao.setOnClickListener(this);
        open_from_camera.setOnClickListener(this);
        open_album.setOnClickListener(this);

        initPopupWindow();
    }


    //    点击按钮的监听
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
//            点击拍照
            case R.id.open_from_camera:
                if (onClickXiangji!=null){
                    onClickXiangji.onClick(R.id.open_from_camera);
                }
                dismiss();
                break;
//            点击拍相册选择
            case R.id.open_album:
                if (onClickXiangce!=null){
                    onClickXiangce.onClick(R.id.open_album);
                }
                dismiss();
                break;
//            点击取消按钮
            case R.id.tv_cancel_quxiao:
                dismiss();
                break;
        }
    }



    private void initPopupWindow() {
//        设置PopupWindow的view
        this.setContentView(mView);
//        设置PopupWindow弹出窗体的宽
        this.setWidth(RelativeLayout.LayoutParams.MATCH_PARENT);
//        设置PopupWindow弹出窗体的宽
        this.setHeight(RelativeLayout.LayoutParams.WRAP_CONTENT);
//        设置PopupWindow弹出窗体可点击
        this.setFocusable(true);
//        设置SelectPicPopupWindow弹出窗体动画效果
        this.setAnimationStyle(R.style.Animation);
//        点击外部popueWindow消失
//        this.setOutsideTouchable(true);
//        实例化一个ColorDrawable颜色为半透明
        ColorDrawable colorDrawable = new ColorDrawable(0xb0000000);
//        设置SelectPicPopupWindow弹出窗体的背景
        this.setBackgroundDrawable(colorDrawable);
//        mView添加OnTouchListener监听判断获取触屏位置如果在选择框外面则销毁弹出框
        mView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {

                int height = mView.findViewById(R.id.linearLayout).getTop();
                int y = (int) event.getY();
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    if (y < height) {
                        dismiss();
                    }
                }
                return true;
            }
        });
    }
}
