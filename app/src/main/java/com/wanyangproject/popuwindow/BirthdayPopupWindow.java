package com.wanyangproject.popuwindow;

import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.NumberPicker;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;

import com.wanyangproject.R;

/**
 * Created by 甜甜 on 2018/8/3.
 */

public class BirthdayPopupWindow extends PopupWindow implements View.OnClickListener {

    private Button btn_quxiao,btn_queding;
    private NumberPicker numberPicker_year,numberPicker_month,numberPicker_day;
    private int year,month,day;
    private View mView;


    private BirthdayClick birthdayClick;

    public void setBirthdayClick(BirthdayClick birthdayClick) {
        this.birthdayClick = birthdayClick;
    }

    public interface BirthdayClick{
        void birthdayclick(int year,int month,int day);
    }


    public BirthdayPopupWindow(Context context, View.OnClickListener itemsOnClick){
        super(context);
        DataShow();
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        mView = inflater.inflate(R.layout.birthday_popupwindow, null);

        btn_quxiao = (Button) mView.findViewById(R.id.btn_quxiao);
        btn_queding = (Button) mView.findViewById(R.id.btn_queding);
        numberPicker_year = (NumberPicker) mView.findViewById(R.id.numberPicker_year);
        numberPicker_month = (NumberPicker) mView.findViewById(R.id.numberPicker_month);
        numberPicker_day = (NumberPicker) mView.findViewById(R.id.numberPicker_day);

        btn_queding.setOnClickListener(this);
        btn_quxiao.setOnClickListener(this);

        initPopupWindow();

        numberPicker_year.setValue(year);
        numberPicker_month.setValue(month);
        numberPicker_day.setValue(day);


        numberPicker_year.setMaxValue(2100);
        numberPicker_year.setMinValue(1900);
        numberPicker_year.setValue(year);
//        设置numberPicker不可编辑
        numberPicker_year.setDescendantFocusability(NumberPicker.FOCUS_BLOCK_DESCENDANTS);
        numberPicker_year.setOnValueChangedListener(OnYearChangedListener);


        numberPicker_month.setMaxValue(12);
        numberPicker_month.setMinValue(1);
        numberPicker_month.setValue(month);
//        格式化显示数字，个位数前面加个零
        numberPicker_month.setFormatter(formatter);
        numberPicker_month.setDescendantFocusability(NumberPicker.FOCUS_BLOCK_DESCENDANTS);
        numberPicker_month.setOnValueChangedListener(OnMonthChangedListener);


//        判断是否闰年，从而设置二月份的天数
        judgeMonth();
        numberPicker_day.setValue(day);
        numberPicker_day.setMaxValue(31);
        numberPicker_day.setMinValue(1);
        numberPicker_day.setFormatter(formatter);
        numberPicker_day.setDescendantFocusability(NumberPicker.FOCUS_BLOCK_DESCENDANTS);
        numberPicker_day.setOnValueChangedListener(OnDayChangedListener);


    }









    //            格式化显示数字，个位数前面加个零
    private NumberPicker.Formatter formatter = new NumberPicker.Formatter() {
        @Override
        public String format(int value) {
            String string = String.valueOf(value);
            if(value < 10){
                string = "0" + string;
            }
            return string;
        }
    };
    //    年
    private NumberPicker.OnValueChangeListener OnYearChangedListener = new NumberPicker.OnValueChangeListener() {
        @Override
        public void onValueChange(NumberPicker picker, int oldVal, int newVal) {
            year = numberPicker_year.getValue();
            judgeYear();

        }
    };

    //    月
    private NumberPicker.OnValueChangeListener OnMonthChangedListener = new NumberPicker.OnValueChangeListener() {
        @Override
        public void onValueChange(NumberPicker picker, int oldVal, int newVal) {
            month = numberPicker_month.getValue();
            judgeMonth();
        }
    };

    //    日
    private NumberPicker.OnValueChangeListener OnDayChangedListener = new NumberPicker.OnValueChangeListener() {
        @Override
        public void onValueChange(NumberPicker picker, int oldVal, int newVal) {
            day = numberPicker_day.getValue();
        }
    };



    //    年
    private void judgeYear() {
        if(month == 2){
            if(year % 4 == 0){
                if(numberPicker_day.getMaxValue() != 29){
                    numberPicker_day.setDisplayedValues(null);
                    numberPicker_day.setMinValue(1);
                    numberPicker_day.setMaxValue(29);
                }
            }else{
                if(numberPicker_day.getMaxValue() != 28){
                    numberPicker_day.setDisplayedValues(null);
                    numberPicker_day.setMinValue(1);
                    numberPicker_day.setMaxValue(28);
                }
            }
        }
        day = numberPicker_day.getValue();
    }

    //    月
    private void judgeMonth() {
        if(month == 2){
            if(year % 4 == 0){
                if(numberPicker_day.getMaxValue() != 29){
                    numberPicker_day.setDisplayedValues(null);
                    numberPicker_day.setMinValue(1);
                    numberPicker_day.setMaxValue(29);
                }
            }else{
                if(numberPicker_day.getMaxValue() != 28){
                    numberPicker_day.setDisplayedValues(null);
                    numberPicker_day.setMinValue(1);
                    numberPicker_day.setMaxValue(28);
                }
            }
        }else{
            switch (month){
                case 4:
                case 6:
                case 9:
                case 11:
                    if(numberPicker_day.getMaxValue() != 30){
                        numberPicker_day.setDisplayedValues(null);
                        numberPicker_day.setMinValue(1);
                        numberPicker_day.setMaxValue(30);
                    }
                    break;
                default:
                    if(numberPicker_day.getMaxValue() != 31){
                        numberPicker_day.setDisplayedValues(null);
                        numberPicker_day.setMinValue(1);
                        numberPicker_day.setMaxValue(31);
                    }
            }
        }
        day = numberPicker_day.getValue();
    }


    //    设置PopupWindow
    private void initPopupWindow() {
//        设置PopupWindow的view
        this.setContentView(mView);
//        设置PopupWindow弹出窗体的宽
        this.setWidth(RelativeLayout.LayoutParams.MATCH_PARENT);
//        设置PopupWindow弹出窗体的宽
        this.setHeight(RelativeLayout.LayoutParams.WRAP_CONTENT);
//        设置PopupWindow弹出窗体可点击
        this.setFocusable(true);
//        设置SelectPicPopupWindow弹出窗体动画效果
        this.setAnimationStyle(R.style.Animation);
//        实例化一个ColorDrawable颜色为半透明
        ColorDrawable colorDrawable = new ColorDrawable(0xb0000000);
//        设置SelectPicPopupWindow弹出窗体的背景
        this.setBackgroundDrawable(colorDrawable);

//        mView添加OnTouchListener监听判断获取触屏位置如果在选择框外面则销毁弹出框
        mView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                int height = mView.findViewById(R.id.birthday_PopupWindow).getTop();
                int y = (int) event.getY();
                if(event.getAction()==MotionEvent.ACTION_UP){
                    if(y < height){
                        dismiss();
                    }
                }
                return true;
            }
        });
    }


    private void DataShow() {
        java.util.Calendar calendar = java.util.Calendar.getInstance();
        year = calendar.get(java.util.Calendar.YEAR);
        month = calendar.get(java.util.Calendar.MONTH)+1;
        day = calendar.get(java.util.Calendar.DAY_OF_MONTH);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
//            点击取消
            case R.id.btn_quxiao:
                dismiss();
                break;
//            点击确定
            case R.id.btn_queding:
                try {
                    birthdayClick.birthdayclick(numberPicker_year.getValue(),numberPicker_month.getValue(),numberPicker_day.getValue());
                    dismiss();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;
        }
    }





}
