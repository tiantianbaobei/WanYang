package com.wanyangproject.popuwindow;

import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.NumberPicker;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;

import com.wanyangproject.R;
import com.wanyangproject.entity.HuoQuYuanQuLieBiaoEntity;
import com.wanyangproject.entity.LunTanEntity;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by 甜甜 on 2018/8/10.
 */

public class XuanZeFeiLeiPopupWindow extends PopupWindow implements View.OnClickListener {

    private Button btn_quxiao,btn_queding;
    private View mView;
    private NumberPicker numberPicker;
//    private String[] number = {"超市1","超市2","超市3","超市4","超市5","超市6","超市7","超市8"};

    private ArrayList<String> fenleititle = new ArrayList<>();
    private ArrayList<String> fenleiid = new ArrayList<>();
    private String[] title;




    public XuanZeFeiLeiPopupWindow(Context context, View.OnClickListener itemsOnClick){
        super(context);
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        mView = inflater.inflate(R.layout.xuanze_feilei_popupwindow, null);

        for (int i = 0; i < LunTanEntity.fenlei.size(); i++) {
            LunTanEntity.ResponseBean fenlei = LunTanEntity.fenlei.get(i);
            fenleititle.add(fenlei.getTitle());
            fenleiid.add(fenlei.getId());
        }
        title = new String[LunTanEntity.fenlei.size()];
        fenleititle.toArray(title);



        numberPicker = (NumberPicker) mView.findViewById(R.id.numberPicker);
        numberPicker.setDisplayedValues(title);
        numberPicker.setMinValue(0);
        numberPicker.setMaxValue(title.length-1);

        btn_quxiao = (Button) mView.findViewById(R.id.btn_quxiao);
        btn_queding = (Button) mView.findViewById(R.id.btn_queding);
        numberPicker = (NumberPicker) mView.findViewById(R.id.numberPicker);

        btn_quxiao.setOnClickListener(this);
        btn_queding.setOnClickListener(this);
        numberPicker.setOnClickListener(this);


//        设置PopupWindow的view
        this.setContentView(mView);
//        设置PopupWindow弹出窗体的宽
        this.setWidth(RelativeLayout.LayoutParams.MATCH_PARENT);
//        设置PopupWindow弹出窗体的高
        this.setHeight(RelativeLayout.LayoutParams.WRAP_CONTENT);
//        设置PopupWindow弹出窗体可点击
        this.setFocusable(true);
//        设置SelectPicPopupWindow弹出窗体动画效果
        this.setAnimationStyle(R.style.Animation);
//        实例化一个ColorDrawable颜色为半透明
        ColorDrawable colorDrawable = new ColorDrawable(0xb0000000);
//        设置SelectPicPopupWindow弹出窗体的背景
        this.setBackgroundDrawable(colorDrawable);
//        mView添加OnTouchListener监听判断获取触屏位置如果在选择框外面则销毁弹出框
        mView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {

                int height = mView.findViewById(R.id.relative_xuanze_fenlei).getTop();
                int y = (int) event.getY();
                if(event.getAction()==MotionEvent.ACTION_UP){
                    if(y < height){
                        dismiss();
                    }
                }
                return true;
            }
        });
    }


    private XuanZeFenLeiClick xuanZeFenLeiClick;

    public void setXuanZeFenLeiClick(XuanZeFenLeiClick xuanZeFenLeiClick) {
        this.xuanZeFenLeiClick = xuanZeFenLeiClick;
    }

    public interface XuanZeFenLeiClick{
        void xuanzefenleiClick(String title,String fenleiid);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
//            点击取消
            case R.id.btn_quxiao:
                dismiss();
                break;
//            点击确定
            case R.id.btn_queding:
                try {
                    xuanZeFenLeiClick.xuanzefenleiClick(title[numberPicker.getValue()],fenleiid.get(numberPicker.getValue()));
//                    dianPuLeiXingPopupWindowClick.dianpuleixingPopupWindowClick(String.valueOf(number[numberPicker.getValue()]));
                    dismiss();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;
        }
    }
}
