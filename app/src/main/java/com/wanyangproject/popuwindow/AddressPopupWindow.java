package com.wanyangproject.popuwindow;

import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.widget.NumberPicker;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.wanyangproject.R;
import com.wanyangproject.entity.ProvinceEntity;
import com.wanyangproject.entity.ShengShiQuEntity;
import com.wanyangproject.utils.ContractUtils;
import com.zhy.http.okhttp.OkHttpUtils;
import com.zhy.http.okhttp.callback.StringCallback;

import java.util.ArrayList;
import java.util.List;

import okhttp3.Call;

/**
 * Created by 甜甜 on 2017/11/27.
 */

public class AddressPopupWindow extends PopupWindow implements View.OnClickListener {

    private TextView address_dialog_tv_quxiao, address_dialog_tv_queding;
    private NumberPicker province_picker, city_picker, qu_picker;
    private View mView;
    private String[] provinceEntityList;
    private String[] provinceEntityList1;
    private List<ProvinceEntity> citylist = new ArrayList<>();
    private List<ProvinceEntity> countryList = new ArrayList<>();
    private String[] city;
    private String[] country;
    private int[] city1;
//    private String[] city1;
    private int i = 0;
    private ShengShiQuEntity shengShiQuEntity;


    private AddressPopupWindow.AddressPopupClick addressPopupClick;
    private ProvinceClick provinceClick;

    public void setProvinceClick(ProvinceClick provinceClick) {
        this.provinceClick = provinceClick;
    }

    public void setAddressPopupClick(AddressPopupClick addressPopupClick) {
        this.addressPopupClick = addressPopupClick;
    }

    public interface AddressPopupClick {
        void addressPopupclick(String Province, String City, String country);
    }

    public interface ProvinceClick {
        void click(int id);
    }

    public AddressPopupWindow(Context context, Object itemOnClick, String[] provinceEntityList) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        mView = inflater.inflate(R.layout.layout_address_popupwindow, null);
        this.provinceEntityList = provinceEntityList;
//        this.provinceEntityList1 = provinceEntityList1;


        address_dialog_tv_quxiao = (TextView) mView.findViewById(R.id.address_dialog_tv_quxiao);
        address_dialog_tv_queding = (TextView) mView.findViewById(R.id.address_dialog_tv_queding);
        province_picker = (NumberPicker) mView.findViewById(R.id.province_picker);
        city_picker = (NumberPicker) mView.findViewById(R.id.city_picker);
        qu_picker = (NumberPicker) mView.findViewById(R.id.qu_picker);
        province_picker.setDisplayedValues(provinceEntityList);
        province_picker.setMaxValue(provinceEntityList.length-1);
        province_picker.setMinValue(1);

        if (provinceClick != null) {
            provinceClick.click(province_picker.getValue());
        }
        initCityHttp();

        province_picker.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
            @Override
            public void onValueChange(NumberPicker picker, int oldVal, int newVal) {
                System.out.println(oldVal + "" + newVal);
                initCityHttp();
            }
        });


        address_dialog_tv_quxiao.setOnClickListener(this);
        address_dialog_tv_queding.setOnClickListener(this);
        province_picker.setOnClickListener(this);
        city_picker.setOnClickListener(this);
        qu_picker.setOnClickListener(this);


//        设置PopupWindow的view
        this.setContentView(mView);
//        设置PopupWindow弹出窗体的宽
        this.setWidth(RelativeLayout.LayoutParams.MATCH_PARENT);
//        设置PopupWindow弹出窗体的宽
        this.setHeight(RelativeLayout.LayoutParams.WRAP_CONTENT);
//        设置PopupWindow弹出窗体可点击
        this.setFocusable(true);
//        设置SelectPicPopupWindow弹出窗体动画效果
        this.setAnimationStyle(R.style.Animation);
//        实例化一个ColorDrawable颜色为半透明
        ColorDrawable colorDrawable = new ColorDrawable(0xb0000000);
//        设置SelectPicPopupWindow弹出窗体的背景
        this.setBackgroundDrawable(colorDrawable);

//        mView添加OnTouchListener监听判断获取触屏位置如果在选择框外面则销毁弹出框
        mView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {

                int height = mView.findViewById(R.id.address_relative).getTop();
                int y = (int) event.getY();
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    if (y < height) {
                        dismiss();
                    }
                }
                return true;
            }
        });
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
//            点击取消按钮
            case R.id.address_dialog_tv_quxiao:
                dismiss();
                break;
//            点击确定按钮
            case R.id.address_dialog_tv_queding:
                try {
                    addressPopupClick.addressPopupclick(provinceEntityList[province_picker.getValue() - 1], city[city_picker.getValue()], country[qu_picker.getValue()]);
                    dismiss();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;
//            点击省
            case R.id.province_picker:
                break;
//            点击市
            case R.id.city_picker:
                break;
//            点击区
            case R.id.qu_picker:
                break;
        }
    }



    //                        选择城市的网络请求
    private void initCityHttp() {
        OkHttpUtils.get()
                .url(ContractUtils.URL_CITY + province_picker.getValue())
                .build()
                .execute(new StringCallback() {
                    @Override
                    public void onError(Call call, Exception e, int id) {

                    }

                    @Override
                    public void onResponse(String response, int id) {
                        i = 0;
                        Gson gson = new Gson();
//                        ShengShiQuEntity1 shengShiQuEntity1=gson.fromJson(response,ShengShiQuEntity1.class);
//                        city = new String[shengShiQuEntity1.getExplain().size()];
//                        city1 = new String[shengShiQuEntity1.getExplain().size()];
//                        for (ShengShiQuEntity1.ExplainBean explainBean:shengShiQuEntity1.getExplain()){
//                            city[i]=explainBean.getREGION_NAME();
//                            city1[i]=explainBean.getID();
//                        }

                        citylist = gson.fromJson(response, new TypeToken<List<ProvinceEntity>>() {
                        }.getType());
                        city = new String[citylist.size()];
                        city1 = new int[citylist.size()];
                        for (ProvinceEntity provinceEntity : citylist) {
                            city[i] = provinceEntity.getName();
                            city1[i] = provinceEntity.getId();
                            i++;
                        }
                        if (city.length - 1 > city_picker.getMaxValue()) {

                            city_picker.setDisplayedValues(city);
                            city_picker.setMaxValue(city.length - 1);
                        } else {
                            city_picker.setMaxValue(city.length - 1);
                            city_picker.setDisplayedValues(city);
                        }
                        initCountyHttp(city1[0]);
                    }
                });
        city_picker.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
            @Override
            public void onValueChange(NumberPicker picker, int oldVal, int newVal) {
                int i = city1[newVal];
                initCountyHttp(i);
            }
        });

    }


//    区的网络请求
    private void initCountyHttp(int i1) {
        OkHttpUtils.get()
                .url(ContractUtils.URL_CITY +province_picker.getValue()+"/"+ i1)
                .build()
                .execute(new StringCallback() {
                    @Override
                    public void onError(Call call, Exception e, int id) {

                    }

                    @Override
                    public void onResponse(String response, int id) {
                        System.out.println(response);
                        i = 0;
                        Gson gson = new Gson();
                        countryList = gson.fromJson(response, new TypeToken<List<ProvinceEntity>>() {
                        }.getType());
                        if (countryList.get(0).getName().equals("济南")) {
                            countryList.add(0, new ProvinceEntity("历下"));
                            countryList.add(0, new ProvinceEntity("市中"));
                            countryList.add(0, new ProvinceEntity("天桥"));
                            countryList.add(0, new ProvinceEntity("槐荫"));
                            countryList.add(0, new ProvinceEntity("历城"));
                        }
                        if (countryList.get(0).getName().equals("泰安")) {
                            countryList.add(0, new ProvinceEntity("泰山"));
                            countryList.add(0, new ProvinceEntity("岱岳"));
                        }
                        if (countryList.get(0).getName().equals("青岛")) {
                            countryList.add(0, new ProvinceEntity("市南"));
                            countryList.add(0, new ProvinceEntity("市北"));
                            countryList.add(0, new ProvinceEntity("城阳"));
                            countryList.add(0, new ProvinceEntity("四方"));
                            countryList.add(0, new ProvinceEntity("李沧"));
                            countryList.add(0, new ProvinceEntity("胶南"));
                        }
                        if (countryList.get(0).getName().equals("淄博")) {
                            countryList.add(0, new ProvinceEntity("张店"));
                        }
                        if (countryList.get(0).getName().equals("枣庄")) {
                            countryList.add(0, new ProvinceEntity("市中"));
                            countryList.add(0, new ProvinceEntity("山亭"));
                        }
                        if (countryList.get(0).getName().equals("烟台")) {
                            countryList.add(0, new ProvinceEntity("芝罘"));
                            countryList.add(0, new ProvinceEntity("莱山"));
                            countryList.add(0, new ProvinceEntity("莱州"));
                        }
                        if (countryList.get(0).getName().equals("潍坊")) {
                            countryList.add(0, new ProvinceEntity("寒亭"));
                            countryList.add(0, new ProvinceEntity("坊子"));
                            countryList.add(0, new ProvinceEntity("奎文"));
                        }
                        if (countryList.get(0).getName().equals("济宁")) {
                            countryList.add(0, new ProvinceEntity("市中"));
                            countryList.add(0, new ProvinceEntity("任城"));
                        }
                        if (countryList.get(0).getName().equals("日照")) {
                            countryList.add(0, new ProvinceEntity("东港"));
                            countryList.add(0, new ProvinceEntity("岚山"));
                        }
                        if (countryList.get(0).getName().equals("莱芜")) {
                            countryList.add(0, new ProvinceEntity("钢城"));
                        }
                        if (countryList.get(0).getName().equals("临沂")) {
                            countryList.add(0, new ProvinceEntity("兰山"));
                            countryList.add(0, new ProvinceEntity("罗庄"));
                            countryList.add(0, new ProvinceEntity("河东"));
                            countryList.add(0, new ProvinceEntity("苍山"));
                        }
                        if (countryList.get(0).getName().equals("德州")) {
                            countryList.add(0, new ProvinceEntity("德城"));
                        }
                        if (countryList.get(0).getName().equals("聊城")) {
                            countryList.add(0, new ProvinceEntity("东昌府"));
                        }
                        if (countryList.get(0).getName().equals("滨州")) {
                            countryList.add(0, new ProvinceEntity("滨城"));
                        }
                        if (countryList.get(0).getName().equals("菏泽")) {
                            countryList.add(0, new ProvinceEntity("牡丹"));
                        }
                        country = new String[countryList.size()];
                        for (ProvinceEntity provinceEntity : countryList) {
                            country[i] = provinceEntity.getName();
                            i++;
                        }
                        if (country.length - 1 > qu_picker.getMaxValue()) {

                            qu_picker.setDisplayedValues(country);
                            qu_picker.setMaxValue(country.length - 1);
                        } else {
                            qu_picker.setMaxValue(country.length - 1);
                            qu_picker.setDisplayedValues(country);

                        }
                    }
                });
    }
}
