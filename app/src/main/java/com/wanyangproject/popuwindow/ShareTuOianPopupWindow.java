package com.wanyangproject.popuwindow;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.ColorDrawable;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.tencent.mm.opensdk.modelmsg.SendMessageToWX;
import com.tencent.mm.opensdk.modelmsg.WXMediaMessage;
import com.wanyangproject.R;
import com.wanyangproject.activity.LoginActivity;
import com.wanyangproject.utils.JieShuYeMian;
import com.wanyangproject.utils.ShareFangKeUtils;
import com.wanyangproject.utils.ShareUtils;
import com.wanyangproject.zxing.decoding.Intents;

import java.io.ByteArrayOutputStream;
import java.util.HashMap;
import java.util.List;

import cn.sharesdk.framework.Platform;
import cn.sharesdk.framework.PlatformActionListener;
import okhttp3.internal.Util;

/**
 * Created by 甜甜 on 2018/8/20.
 */

public class ShareTuOianPopupWindow extends PopupWindow implements View.OnClickListener {




    private String isOK;
    private String tupian;
    private Bitmap bitmap;


    public Bitmap getBitmap() {
        return bitmap;
    }

    public void setBitmap(Bitmap bitmap) {
        this.bitmap = bitmap;
    }

    public String getTupian() {
        return tupian;
    }

    public void setTupian(String tupian) {
        this.tupian = tupian;
    }

    public String getIsOK() {
        return isOK;
    }

    public void setIsOK(String isOK) {
        this.isOK = isOK;
    }





    private View mView;
    private Context context;
    private TextView tv_weixin_haoyou,tv_qq_haoyou,tv_weixin_pengyouquan,tv_xinlang_weibo,tv_quxiao;
    //分享标题
    private String share_title="访客二维码";
    //分享链接
//    private String share_url = "http://img.zcool.cn/community/0117e2571b8b246ac72538120dd8a4.jpg@1280w_1l_2o_100sh.jpg";
    //分享封面图片
//    private String share_img = "http://img1.imgtn.bdimg.com/it/u=3044191397,2911599132&fm=26&gp=0.jpg";
//    private String image = "http://park.hostop.net/static/wanyang/img/download-logo.png";
    private String image = "http://park.hostop.net/static/wanyang/img/download-logo.jpg";




    //分享描述
    private String share_desc="万洋众创城";
//    Bitmap bitmap = BitmapFactory.decodeResource(context.getResources(), R.mipmap.logo);
    private JieShuYeMian jieShuYeMian;

    public void setJieShuYeMian(JieShuYeMian jieShuYeMian) {
        this.jieShuYeMian = jieShuYeMian;
    }

    public ShareTuOianPopupWindow(Context context, View.OnClickListener itemsOnClick) {
        super(context);
        this.context=context;
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        mView = inflater.inflate(R.layout.share_erweima, null);;


        tv_weixin_haoyou = mView.findViewById(R.id.tv_weixin_haoyou);
        tv_qq_haoyou = mView.findViewById(R.id.tv_qq_haoyou);
//        tv_weixin_pengyouquan = mView.findViewById(R.id.tv_weixin_pengyouquan);
//        tv_xinlang_weibo = mView.findViewById(R.id.tv_xinlang_weibo);
        tv_quxiao = mView.findViewById(R.id.tv_quxiao);

        tv_weixin_haoyou.setOnClickListener(this);
        tv_qq_haoyou.setOnClickListener(this);
//        tv_weixin_pengyouquan.setOnClickListener(this);
//        tv_xinlang_weibo.setOnClickListener(this);
        tv_quxiao.setOnClickListener(this);

        initPopupWindow();
    }


    private void initPopupWindow() {
//        设置PopupWindow的view
        this.setContentView(mView);
//        设置PopupWindow弹出窗体的宽
        this.setWidth(RelativeLayout.LayoutParams.MATCH_PARENT);
//        设置PopupWindow弹出窗体的宽
        this.setHeight(RelativeLayout.LayoutParams.WRAP_CONTENT);
//        设置PopupWindow弹出窗体可点击
        this.setFocusable(true);
//        设置SelectPicPopupWindow弹出窗体动画效果
        this.setAnimationStyle(R.style.Animation);
//        点击外部popueWindow消失
//        this.setOutsideTouchable(true);
//        实例化一个ColorDrawable颜色为半透明
        ColorDrawable colorDrawable = new ColorDrawable(0xb0000000);
//        设置SelectPicPopupWindow弹出窗体的背景
        this.setBackgroundDrawable(colorDrawable);
//        mView添加OnTouchListener监听判断获取触屏位置如果在选择框外面则销毁弹出框
        mView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {

                int height = mView.findViewById(R.id.linearLayout_share).getTop();
                int y = (int) event.getY();
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    if (y < height) {
                        dismiss();
                    }
                }
                return true;
            }
        });
    }




    @Override
    public void onClick(View view) {
        switch (view.getId()){
//            微信好友
            case R.id.tv_weixin_haoyou:
                com.tencent.mm.opensdk.modelmsg.WXWebpageObject webpage = new com.tencent.mm.opensdk.modelmsg.WXWebpageObject();
                webpage.webpageUrl = isOK;

                WXMediaMessage msg = new WXMediaMessage(webpage);

                msg.title = share_title;
                msg.description = share_desc;

//                msg.thumbData = bitmap;



//                Bitmap bmp = BitmapFactory.decodeFile(tupian);
//                Bitmap thumbBmp = Bitmap.createScaledBitmap(bmp, 100, 100, true);
//                Bitmap bitmap = BitmapFactory.decodeResource(getResources(), R.mipmap.logo);
                msg.setThumbImage(bitmap);
//                bmp.recycle();


                SendMessageToWX.Req req = new SendMessageToWX.Req();
//             WXSceneTimeline朋友圈    WXSceneSession聊天界面
                req.scene = SendMessageToWX.Req.WXSceneSession;//聊天界面
                req.message = msg;
                req.transaction = String.valueOf(System.currentTimeMillis());
                LoginActivity.iwxapi.sendReq(req);
//                ShareUtils.shareWechat(share_title,share_url,share_desc,share_img,platformActionListener);
                dismiss();

//                ShareFangKeUtils.shareWechat(share_title,isOK,share_desc,tupian,platformActionListener);
//                dismiss();
                break;
//            QQ好友
            case R.id.tv_qq_haoyou:
                ShareFangKeUtils.shareQQ(share_title,isOK,share_desc,image,platformActionListener);
//                ShareUtils.shareQQ(share_title,share_url,share_desc,bitmap,platformActionListener);
                dismiss();
                break;
////            微信朋友圈
//            case R.id.tv_weixin_pengyouquan:
//                ShareUtils.sharepyq(share_title,isOK,share_desc,share_img,platformActionListener);
//                dismiss();
//                break;
////            新浪微博
//            case R.id.tv_xinlang_weibo:
//                ShareUtils.shareWeibo(share_title,isOK,share_desc,share_img,platformActionListener);
//                dismiss();
//                break;
//            取消按钮
            case R.id.tv_quxiao:
                dismiss();
                break;
        }
    }




    /**
     * 分享回调
     */
    PlatformActionListener platformActionListener=new PlatformActionListener() {
        @Override
        public void onComplete(Platform platform, int i, HashMap<String, Object> hashMap) {
            Toast.makeText(context, "分享成功", Toast.LENGTH_SHORT).show();
            jieShuYeMian.jieshu();
        }

        @Override
        public void onError(Platform platform, int i, Throwable throwable) {
            Toast.makeText(context, "分享失败", Toast.LENGTH_SHORT).show();
        }

        @Override
        public void onCancel(Platform platform, int i) {
            Toast.makeText(context, "分享取消", Toast.LENGTH_SHORT).show();
        }
    };
}
