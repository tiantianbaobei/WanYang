package com.wanyangproject.luntanAdapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.wanyangproject.R;
import com.wanyangproject.entity.TieZiXiangQingEntity;

import java.util.List;

/**
 * Created by 甜甜 on 2018/8/11.
 */

public class DianZanAdapter extends RecyclerView.Adapter<DianZanAdapter.ViewHolder>{

    private Context context;
    private  List<TieZiXiangQingEntity.ResponseBean.LikeArrBean> likeArr;


    public DianZanAdapter(Context context, List<TieZiXiangQingEntity.ResponseBean.LikeArrBean> likeArr) {
        this.context = context;
        this.likeArr = likeArr;
    }

    @Override
    public DianZanAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_dianzan, parent, false);
        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(DianZanAdapter.ViewHolder holder, int position) {
        if(likeArr.get(position).getUinfo().getAvatar() == null){

        }else{
            Glide.with(context).load(likeArr.get(position).getUinfo().getAvatar()).into(holder.image);
        }
    }

    @Override
    public int getItemCount() {
        return likeArr.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private ImageView image;
        private View view;
        public ViewHolder(View itemView) {
            super(itemView);
            view = itemView;
            image = itemView.findViewById(R.id.image);
        }
    }

//    private Context context;
//    private List<TieZiXiangQingEntity.ResponseBean.LikeArrBean> likeArr;
//
//    public DianZanAdapter(Context context, List<TieZiXiangQingEntity.ResponseBean.LikeArrBean> likeArr) {
//        this.context = context;
//        this.likeArr = likeArr;
//    }
//
//    @Override
//    public DianZanAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
//        View view = LayoutInflater.from(context).inflate(R.layout.item_dianzan, parent, false);
//        ViewHolder viewHolder = new ViewHolder(view);
//        return viewHolder;
//    }
//
//    @Override
//    public void onBindViewHolder(DianZanAdapter.ViewHolder holder, int position) {
//        if(likeArr.get(position).getUinfo().getAvatar() == null){
//
//        }else{
//            Glide.with(context).load(likeArr.get(position).getUinfo().getAvatar()).into(holder.image);
//            System.out.println("        /////////////");
//        }
//    }
//
//    @Override
//    public int getItemCount() {
//        return likeArr.size();
//    }
//
//    public class ViewHolder extends RecyclerView.ViewHolder {
//        private ImageView image;
//        private View view;
//        public ViewHolder(View itemView) {
//            super(itemView);
//            view = itemView;
//            image = itemView.findViewById(R.id.image);
//        }
//    }
}
