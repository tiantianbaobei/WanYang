package com.wanyangproject.luntanAdapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.style.BackgroundColorSpan;
import android.text.style.ForegroundColorSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.wanyangproject.R;
import com.wanyangproject.R2;
import com.wanyangproject.entity.TieZiXiangQingEntity;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by 甜甜 on 2018/8/11.
 */

public class PingLunAdapter extends RecyclerView.Adapter<PingLunAdapter.ViewHolder>{

    private Context context;
    private List<TieZiXiangQingEntity.ResponseBean.ReplyArrBean> replyArr;

    public PingLunAdapter(Context context, List<TieZiXiangQingEntity.ResponseBean.ReplyArrBean> replyArr) {
        this.context = context;
        this.replyArr = replyArr;
    }



//    点击行布局传回复的id
    private HuiFuidClick huifuidClick;

    public void setHuifuidClick(HuiFuidClick huifuidClick) {
        this.huifuidClick = huifuidClick;
    }

    public interface HuiFuidClick{
        void huifuidClick(int position,String id,String name);
}






    @Override
    public PingLunAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_pinglun, parent, false);
        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final PingLunAdapter.ViewHolder holder, final int position) {
        if(replyArr.get(position).getUinfo().getAvatar() == null){
//            头像
        }else{
            Glide.with(context).load(replyArr.get(position).getUinfo().getAvatar()).into(holder.image_touxiang);
        }


        if(replyArr.get(position).getUinfo().getNickname() == null){

        }else{ //名字
            holder.tv_title.setText(replyArr.get(position).getUinfo().getNickname());
        }


        if(replyArr.get(position).getAdd_time() == null){

        }else{//  时间
            holder.tv_time.setText(replyArr.get(position).getAdd_time());
        }

        if(replyArr.get(position).getContent() == null){

        }else{// 内容
            String string = replyArr.get(position).getContent();
            String pinglunname = "";

//            String pinglunneirong = "";
            ArrayList <HashMap<String,Integer>> list = new ArrayList<>();


            for (int i = 0; i < replyArr.get(position).getUpInfo().size(); i++) {
                HashMap map = new HashMap();

                TieZiXiangQingEntity.ResponseBean.ReplyArrBean.UpInfoBean upInfoBean = replyArr.get(position).getUpInfo().get(i);
//                string  = string + "\n"+ upInfoBean.getUinfo().getNickname()+"："+upInfoBean.getContent();

                map.put("kaishi",pinglunname.length());
                map.put("jieshu",pinglunname.length() + upInfoBean.getUinfo().getNickname().length()+1);
                list.add(map);

                pinglunname = pinglunname + upInfoBean.getUinfo().getNickname()+"："+upInfoBean.getContent();

                if(i != replyArr.get(position).getUpInfo().size()-1){
                    pinglunname = pinglunname+"\n";
                }


//                pinglunneirong = upInfoBean.getContent();

//                holder.tv_pinglun_name.setText(upInfoBean.getUinfo().getNickname()+"：");
//                holder.tv_pinglun_neirong.setText(upInfoBean.getContent());

//
////                String neirong = upInfoBean.getUinfo().getNickname()+"："+upInfoBean.getContent();
//
//
//                int fstart = pinglunname.length() - 1 - 1 - upInfoBean.getUinfo().getNickname().length() ;
//
//                int fend = fstart + upInfoBean.getUinfo().getNickname().length() +1;



                System.out.println("[[[[[[[[[[[[[[[[[[[[[[[[[[[[");
            }

            SpannableStringBuilder style = new SpannableStringBuilder(pinglunname);
            for (int i = 0; i < list.size(); i++) {
                HashMap<String, Integer> stringIntegerHashMap = list.get(i);
                style.setSpan(new ForegroundColorSpan(Color.parseColor("#9b9b9b")),stringIntegerHashMap.get("kaishi"),stringIntegerHashMap.get("jieshu"), Spannable.SPAN_EXCLUSIVE_INCLUSIVE);//文字颜色

            }
            holder.tv_pinglun_name.setText(style);

                holder.tv_neirong.setText(string);

            if(pinglunname.length() != 0){
                pinglunname = pinglunname.substring(0,pinglunname.length()-1);
            }

            if(pinglunname.equals("")){
             holder.relative_huifu.setVisibility(View.GONE);
            }else{
                holder.relative_huifu.setVisibility(View.VISIBLE);
//                holder.tv_pinglun_name.setText(pinglunname);
            }
        }



        holder.relative_one.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(huifuidClick != null){
                    huifuidClick.huifuidClick(holder.getAdapterPosition(),replyArr.get(position).getId(),replyArr.get(position).getUinfo().getNickname());
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return replyArr.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private ImageView image_touxiang;
        private TextView tv_title,tv_time,tv_neirong,tv_pinglun_name;
//                tv_pinglun_neirong;
        private RelativeLayout relative_one,relative_huifu;
        private View view;
        public ViewHolder(View itemView) {
            super(itemView);
            view = itemView;
            image_touxiang = itemView.findViewById(R.id.image_touxiang);
            tv_title = itemView.findViewById(R.id.tv_title);
            tv_time = itemView.findViewById(R.id.tv_time);
            tv_neirong = itemView.findViewById(R.id.tv_neirong);
            relative_one = itemView.findViewById(R.id.relative_one);
            tv_pinglun_name = itemView.findViewById(R.id.tv_pinglun_name);
//            tv_pinglun_neirong = itemView.findViewById(R.id.tv_pinglun_neirong);
            relative_huifu = itemView.findViewById(R.id.relative_huifu);
        }
    }
}
