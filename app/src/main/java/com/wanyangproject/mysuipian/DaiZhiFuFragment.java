package com.wanyangproject.mysuipian;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.google.gson.Gson;
import com.wanyangproject.R;
import com.wanyangproject.adapter.DingDanDaiZhiFuAdapter;
import com.wanyangproject.entity.DaiZhiFuEntity;
import com.wanyangproject.entity.NumberEntity;
import com.wanyangproject.my.DaiZhiFuXiangQingActivity;
import com.wanyangproject.myadapter.DaiZhiFuAdapter;
import com.wanyangproject.shouye.DingDanXiangQingActivity;
import com.wanyangproject.shouye.WaiMaiActivity;
import com.wanyangproject.shouye.ZhiFuActivity;
import com.wanyangproject.utils.ContractUtils;
import com.zhy.http.okhttp.OkHttpUtils;
import com.zhy.http.okhttp.callback.StringCallback;

import butterknife.BindView;
import butterknife.ButterKnife;
import okhttp3.Call;

/**
 * Created by 甜甜 on 2018/7/30.
 */

public class    DaiZhiFuFragment extends Fragment {
    @BindView(R.id.recycerView)
    RecyclerView recycerView;
    private DaiZhiFuAdapter daiZhiFuAdapter;
    private DingDanDaiZhiFuAdapter dingDanAdapter;
    private DaiZhiFuEntity daiZhiFuEntity;
    private String finish;
    private NetWork netWork;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.dingdan_daizifu_fragment, container, false);
        ButterKnife.bind(this, view);
        initView();

//        订单列表待支付的网络请求
//        initDaiZhiFuHttp();



        IntentFilter intentFilter4 = new IntentFilter();
        intentFilter4.addAction("finish");
        netWork = new NetWork();
        getContext().registerReceiver(netWork, intentFilter4);


        return view;
    }





//    @Override
//    protected void onDestroy() {
//        super.onDestroy();
//        unregisterReceiver(netWork);
//    }

    class NetWork extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {

            //            结束页面
            finish = intent.getStringExtra("finish");
            if (finish != null) {
                //        订单列表待支付的网络请求
                initDaiZhiFuHttp();
            }
        }
    }







//    @Override
//    public void onResume() {
//        super.onResume();
//        //        订单列表待支付的网络请求
//        initDaiZhiFuHttp();
//    }

    //    订单列表待支付的网络请求
    public void initDaiZhiFuHttp() {
        OkHttpUtils.post()
                .url(ContractUtils.LOGIN_URL+"Life/orderlist")
                .addHeader("token",ContractUtils.getTOKEN(getContext()))
                .addParams("parkId",ContractUtils.getParkId(getContext()))
                .addParams("type","1")//1：未支付2：已支付3：代发货4：已完成
                .build()
                .execute(new StringCallback() {
                    @Override
                    public void onError(Call call, Exception e, int id) {

                    }

                    @Override
                    public void onResponse(String response, int id) {
                        ContractUtils.Code500shibai(getContext(),response);
                        System.out.println(response+"     订单列表待支付的网络请求");

                        if(response.indexOf("200") != -1){
                            Gson gson = new Gson();
                            daiZhiFuEntity = gson.fromJson(response, DaiZhiFuEntity.class);
//                            上面和下面的
                            daiZhiFuAdapter = new DaiZhiFuAdapter(getContext(),daiZhiFuEntity.getResponse());
                            LinearLayoutManager manager = new LinearLayoutManager(getContext());
                            recycerView.setLayoutManager(manager);
                            recycerView.setAdapter(daiZhiFuAdapter);


//                            点击进入详情
                            daiZhiFuAdapter.setDaiZhiFuXiangQingClick(new DaiZhiFuAdapter.DaiZhiFuXiangQingClick() {
                                @Override
                                public void daizhifuxiangqiangClick(int position) {
                                    Intent intent = new Intent(getContext(), DaiZhiFuXiangQingActivity.class);
                                    intent.putExtra("dingdanhao",daiZhiFuEntity.getResponse().get(position).getOrder_sn());
                                    startActivity(intent);
                                }
                            });



//                            点击去支付
                            daiZhiFuAdapter.setQuZhiFuClick(new DaiZhiFuAdapter.QuZhiFuClick() {
                                @Override
                                public void quzhifuClick(int position) {
                                    Intent intent = new Intent(getContext(), ZhiFuActivity.class);
                                    intent.putExtra("money",daiZhiFuEntity.getResponse().get(position).getMoney());
                                    intent.putExtra("dingdanhao",daiZhiFuEntity.getResponse().get(position).getOrder_sn());
                                    getContext().startActivity(intent);
//
                                }
                            });





////                            点击去支付
//                            daiZhiFuAdapter.setQuZhiFuClick(new DaiZhiFuAdapter.QuZhiFuClick() {
//                                @Override
//                                public void quzhifuClick(int position) { //   peisongfei      zongjiage      shopid         dingdanhao
//
//                                    if(daiZhiFuEntity.getResponse().get(position).getShop_cost().equals("")){
//                                        daiZhiFuEntity.getResponse().get(position).setShop_cost("0");
//                                    }
//                                    float zongjiaone = 0;
//                                    float zongjiatwo = Float.parseFloat(daiZhiFuEntity.getResponse().get(position).getMoney()); //合计价格
//                                    float baohanyunfei = Float.parseFloat(daiZhiFuEntity.getResponse().get(position).getShop_cost());
//
//                                    for (int i = 0; i < daiZhiFuEntity.getResponse().get(position).getGoods().size(); i++) {
//                                        DaiZhiFuEntity.ResponseBean.GoodsBean goodsBean = daiZhiFuEntity.getResponse().get(position).getGoods().get(i);
//                                        float mon = Integer.parseInt(goodsBean.getNum()) * Float.parseFloat(goodsBean.getPic());
//                                        zongjiaone += mon;
//                                    }
//
//                                    String yunfei;
//                                    float jiagejiage;
//                                    if(zongjiaone < zongjiatwo){
////                                       说明包含运费
//                                        yunfei = "包含运费 ¥";
//                                        jiagejiage = zongjiaone;
//                                    }else{
////                                          不含运费
//                                        yunfei = "不包含运费 ¥";
//                                        jiagejiage = zongjiatwo;
//                                    }
//
//
//                                    String format = String.format("%.2f", jiagejiage);
//
//
//                                    Intent intent = new Intent(getContext(), DingDanXiangQingActivity.class);
//                                    intent.putExtra("peisongfei",daiZhiFuEntity.getResponse().get(position).getShop_cost());
//                                    intent.putExtra("shopid",daiZhiFuEntity.getResponse().get(position).getShop_id());
//                                    intent.putExtra("zongjiage",format);
//                                    intent.putExtra("dingdanhao",daiZhiFuEntity.getResponse().get(position).getOrder_sn());
//                                    startActivity(intent);
//                                }
//                            });
                        }else if(response.indexOf("400") != -1){
                            Toast.makeText(getContext(), "您还没有待支付订单!", Toast.LENGTH_SHORT).show();
                            if(daiZhiFuEntity != null){
                                daiZhiFuEntity.getResponse().clear();

                                daiZhiFuAdapter.notifyDataSetChanged();
                            }

                        }
                    }
                });
    }

    private void initView() {
//        daiZhiFuAdapter = new DaiZhiFuAdapter(getContext());
//        LinearLayoutManager manager = new LinearLayoutManager(getContext());
//        recycerView.setLayoutManager(manager);
//        recycerView.setAdapter(daiZhiFuAdapter);
//
//        daiZhiFuAdapter.setDaiZhiFuXiangQingClick(new DaiZhiFuAdapter.DaiZhiFuXiangQingClick() {
//            @Override
//            public void daizhifuxiangqiangClick(int position) {
//                Intent intent = new Intent(getContext(), DaiZhiFuXiangQingActivity.class);
//                startActivity(intent);
//            }
//        });
//
//
//        daiZhiFuAdapter.setQuZhiFuClick(new DaiZhiFuAdapter.QuZhiFuClick() {
//            @Override
//            public void quzhifuClick(int position) {
//                Intent intent = new Intent(getContext(), ZhiFuActivity.class);
//                startActivity(intent);
//            }
//        });
    }
}
