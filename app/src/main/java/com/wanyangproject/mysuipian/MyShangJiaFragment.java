package com.wanyangproject.mysuipian;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.google.gson.Gson;
import com.wanyangproject.R;
import com.wanyangproject.entity.ShouCangShangJiaXianShiEntity;
import com.wanyangproject.myadapter.MyShangJiaAdapter;
import com.wanyangproject.shouye.WaiMaiActivity;
import com.wanyangproject.utils.ContractUtils;
import com.zhy.http.okhttp.OkHttpUtils;
import com.zhy.http.okhttp.callback.StringCallback;

import butterknife.BindView;
import butterknife.ButterKnife;
import okhttp3.Call;

/**
 * Created by 甜甜 on 2018/7/31.
 */

public class MyShangJiaFragment extends Fragment {

    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    private MyShangJiaAdapter myShangJiaAdapter;
    private ShouCangShangJiaXianShiEntity shouCangShangJiaXianShiEntity;
    private int isok = 0;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.my_shangjia_fragment, container, false);
        ButterKnife.bind(this, view);
        initView();

//        收藏商家的显示的网络请求
        initShouCangShangJiaXianShiHttp();

        return view;
    }



//    @Override
//    public void onResume() {
//        super.onResume();
//        isok++;
//        if(isok == 1){
////        收藏商家的显示的网络请求
//            initShouCangShangJiaXianShiHttp();
//        }
//    }


//    收藏商家的显示的网络请求
    private void initShouCangShangJiaXianShiHttp() {
        final ProgressDialog progressDialog = new ProgressDialog(getContext());
        progressDialog.setTitle("提示");
        progressDialog.setMessage("请等待...");
        progressDialog.setCancelable(false);
        progressDialog.show();
        OkHttpUtils.post()
                .url(ContractUtils.LOGIN_URL+"user/collectionlist")
                .addHeader("token",ContractUtils.getTOKEN(getContext()))
                .addParams("parkId",ContractUtils.getParkId(getContext()))
                .addParams("type","1")//1：用户收藏商家
                .build()
                .execute(new StringCallback() {
                    @Override
                    public void onError(Call call, Exception e, int id) {
                        System.out.println(e+"      eeee用户收藏商家显示");
                        progressDialog.dismiss();
                    }

                    @Override
                    public void onResponse(String response, int id) {
                        ContractUtils.Code500(getContext(),response);
                        System.out.println(response+"          用户收藏商家显示网络请求");
                        progressDialog.dismiss();
                        if(response.indexOf("200") != -1){
                            Gson gson = new Gson();
                            shouCangShangJiaXianShiEntity = gson.fromJson(response, ShouCangShangJiaXianShiEntity.class);
                            myShangJiaAdapter = new MyShangJiaAdapter(getContext(),shouCangShangJiaXianShiEntity.getResponse());
                            LinearLayoutManager manager = new LinearLayoutManager(getContext());
                            recyclerView.setLayoutManager(manager);
                            recyclerView.setAdapter(myShangJiaAdapter);


//                            进入详情
                            myShangJiaAdapter.setXiangQIngClick(new MyShangJiaAdapter.XiangQIngClick() {
                                @Override
                                public void xiangqingClick(int position, String id) {
                                    Intent intent = new Intent(getContext(),WaiMaiActivity.class);
                                    intent.putExtra("id",id);
                                    startActivity(intent);
                                }
                            });



                            myShangJiaAdapter.setShanChuClick(new MyShangJiaAdapter.ShanChuClick() {
                                @Override
                                public void shanchuClick(int position, String id) {
//                                    商家删除的网络请求
                                    initShanChuHttp(position,id);
                                }
                            });
                          }else if(response.indexOf("400") != -1){
//                            ContractUtils.Code400(getContext(),response);
                            Toast.makeText(getContext(), "暂无数据", Toast.LENGTH_SHORT).show();
                            if(shouCangShangJiaXianShiEntity != null){
                                shouCangShangJiaXianShiEntity.getResponse().clear();
                                myShangJiaAdapter.notifyDataSetChanged();
                        }
                    }
                    }
                });
    }



//    商家删除的网络请求
    private void initShanChuHttp(int position, String id) {
        if(id == null){
            return;
        }

        OkHttpUtils.post()
                .url(ContractUtils.LOGIN_URL+"user/collection")
                .addHeader("token",ContractUtils.getTOKEN(getContext()))
                .addParams("id",id)
                .addParams("parkId", ContractUtils.getParkId(getContext()))
                .addParams("type", id)//1:收藏商家
                .build()
                .execute(new StringCallback() {
                    @Override
                    public void onError(Call call, Exception e, int id) {

                    }

                    @Override
                    public void onResponse(String response, int id) {
                        System.out.println(response+"            商家删除的网络请求 ");
                        ContractUtils.Code500(getContext(),response);
                        if(response.indexOf("200") != -1){
                            Toast.makeText(getContext(), "删除成功", Toast.LENGTH_SHORT).show();

//                               收藏商家的显示的网络请求
                            initShouCangShangJiaXianShiHttp();
                        }
                    }
                });
    }







    private void initView() {
//        myShangJiaAdapter = new MyShangJiaAdapter(getContext());
//        LinearLayoutManager manager = new LinearLayoutManager(getContext());
//        recyclerView.setLayoutManager(manager);
//        recyclerView.setAdapter(myShangJiaAdapter);
    }

}
