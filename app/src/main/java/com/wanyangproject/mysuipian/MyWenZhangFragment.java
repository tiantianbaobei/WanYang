package com.wanyangproject.mysuipian;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.google.gson.Gson;
import com.wanyangproject.R;
import com.wanyangproject.activity.WenZhangChanYeJinRongXiangQingActivity;
import com.wanyangproject.activity.YuanQuZhaoShangXiangQingActivity;
import com.wanyangproject.entity.MyShouCangWenZhangEntity;
import com.wanyangproject.entity.WenZhangChanYeJinRongEntity;
import com.wanyangproject.myadapter.MyShouCangWenZhangAdapter;
import com.wanyangproject.shouye.ChanYeJinRongXiangQingActivity;
import com.wanyangproject.shouye.YuanQuJieShaoActivity;
import com.wanyangproject.shouye.ZhiNengZhiZaoXiangQingActivity;
import com.wanyangproject.shouye.ZiXunXiangQingActivity;
import com.wanyangproject.utils.ContractUtils;
import com.zhy.http.okhttp.OkHttpUtils;
import com.zhy.http.okhttp.callback.StringCallback;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import okhttp3.Call;

/**
 * Created by 甜甜 on 2018/7/31.
 */

public class MyWenZhangFragment extends Fragment {

    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    private MyShouCangWenZhangAdapter myShouCangWenZhangAdapter;
    private MyShouCangWenZhangEntity myShouCangWenZhangEntity;
    private JSONArray response1;
    private int isok = 0;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.my_wenzhang_fagment, container, false);
        ButterKnife.bind(this, view);
        initView();

////        我的收藏文章收藏的网络请求
        initMyShouCangHttp();

        return view;
    }



//    @Override
//    public void onResume() {
//        super.onResume();
//        isok++;
//        if(isok == 1){
////        我的收藏文章收藏的网络请求
//            initMyShouCangHttp();
//        }
//    }


//    @Override
//    public void onResume() {
//        super.onResume();
//        //        我的收藏文章收藏的网络请求
//        initMyShouCangHttp();
//    }


    //    我的收藏文章收藏的网络请求
    private void initMyShouCangHttp() {
        final ProgressDialog progressDialog = new ProgressDialog(getContext());
        progressDialog.setTitle("提示");
        progressDialog.setMessage("请等待...");
        progressDialog.setCancelable(false);
        progressDialog.show();
        OkHttpUtils.post()
                .url(ContractUtils.LOGIN_URL+"user/myShoucang")
                .addHeader("token",ContractUtils.getTOKEN(getContext()))
                .build()
                .execute(new StringCallback() {
                    @Override
                    public void onError(Call call, Exception e, int id) {
                        progressDialog.dismiss();
                        System.out.println(e+"         我的收藏文章eeeeeee ");
                    }

                    @Override
                    public void onResponse(String response, int id) {
                        System.out.println(response+"           我的收藏文章收藏的网络请求 ");
                        ContractUtils.Code500(getContext(),response);
                        progressDialog.dismiss();
                        if(response.indexOf("200") != -1){
                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                response1 = jsonObject.getJSONArray("response");

                                myShouCangWenZhangAdapter = new MyShouCangWenZhangAdapter(getContext(),response1);
                                LinearLayoutManager manager = new LinearLayoutManager(getContext());
                                recyclerView.setLayoutManager(manager);
                                recyclerView.setAdapter(myShouCangWenZhangAdapter);


//                            点击进入详情
                            myShouCangWenZhangAdapter.setXiagQingClick(new MyShouCangWenZhangAdapter.XiagQingClick() {
                                @Override
                                public void xiangqingClick(int position, String id,String typeid) {
                                    if(typeid.equals("0")){///0 园区手册   1 智能制造   2 产业金融   3 园区资讯   4 政策法规    5企服中心    6 园区招商
                                        Intent intent = new Intent(getContext(), YuanQuJieShaoActivity.class);
                                        intent.putExtra("id",id);
                                        System.out.println(id+"         收藏收藏收藏园区手册传送的id");
                                        startActivity(intent);
                                    }else if(typeid.equals("1")){///0 园区手册   1 智能制造   2 产业金融   3 园区资讯   4 政策法规    5企服中心    6 园区招商
//                                        Intent intent = new Intent(getContext(), WenZhangChanYeJinRongXiangQingActivity.class);
                                        Intent intent = new Intent(getContext(), ZhiNengZhiZaoXiangQingActivity.class);
                                        intent.putExtra("wenzhangid",id);
                                        System.out.println(id+"         收藏收藏收藏智能制造传送的id");
                                        startActivity(intent);
                                    }else if(typeid.equals("2")){///0 园区手册   1 智能制造   2 产业金融   3 园区资讯   4 政策法规    5企服中心    6 园区招商
//                                        Intent intent = new Intent(getContext(), ChanYeJinRongXiangQingActivity.class);
                                        Intent intent = new Intent(getContext(), WenZhangChanYeJinRongXiangQingActivity.class);
                                        intent.putExtra("wenzhangid",id);
                                        System.out.println(id+"         收藏收藏收藏产业金融传送的id");
                                        startActivity(intent);
                                    }else if(typeid.equals("3")){///0 园区手册   1 智能制造   2 产业金融   3 园区资讯   4 政策法规    5企服中心    6 园区招商
                                        Intent intent = new Intent(getContext(), ZiXunXiangQingActivity.class);
                                        intent.putExtra("id",id);
                                        System.out.println(id+"         收藏收藏收园区资讯id");
                                        startActivity(intent);
                                    }else if(typeid.equals("4")){///0 园区手册   1 智能制造   2 产业金融   3 园区资讯   4 政策法规    5企服中心    6 园区招商
                                        Intent intent = new Intent(getContext(), YuanQuZhaoShangXiangQingActivity.class);
                                        intent.putExtra("id",id);
                                        intent.putExtra("typeId","4");
                                        System.out.println(id+"         收藏收藏收政策法规id");
                                        startActivity(intent);
                                    }else if(typeid.equals("5")){///0 园区手册   1 智能制造   2 产业金融   3 园区资讯   4 政策法规    5企服中心    6 园区招商
                                        Intent intent = new Intent(getContext(), YuanQuZhaoShangXiangQingActivity.class);
                                        intent.putExtra("id",id);
                                        intent.putExtra("typeId","5");
                                        System.out.println(id+"         收藏收藏5企服中心id");
                                        startActivity(intent);
                                    }else if(typeid.equals("6")){///0 园区手册   1 智能制造   2 产业金融   3 园区资讯   4 政策法规    5企服中心    6 园区招商
                                        Intent intent = new Intent(getContext(), YuanQuZhaoShangXiangQingActivity.class);
                                        intent.putExtra("id",id);
                                        intent.putExtra("typeId","6");
                                        System.out.println(id+"         收藏收藏收园区招商id");
                                        startActivity(intent);
                                    }
                                }
                            });



////                            删除
                            myShouCangWenZhangAdapter.setShanChuClick(new MyShouCangWenZhangAdapter.ShanChuClick() {
                                @Override
                                public void shanchuClick(int position, String idd) {
//                                    删除文章的网络请求
                                    initShanChuHttp(position,idd);
                                }
                            });
//
//                        }else if(response.indexOf("400") != -1){
//                            if(myShouCangWenZhangEntity != null){
//                                myShouCangWenZhangEntity.getResponse().clear();
//                                myShouCangWenZhangAdapter.notifyDataSetChanged();
//                            }
//                        }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }else if(response.indexOf("400") != -1){
                            response1 = null;
                            if(myShouCangWenZhangAdapter != null){
                                myShouCangWenZhangAdapter.notifyDataSetChanged();
                            }
                            ContractUtils.Code400(getContext(),response);
                        }
                    }
                });
    }



//    删除文章的网络请求
    private void initShanChuHttp(int position, String idd) {
        System.out.println(idd+"    iddddddshanchu");
        OkHttpUtils.post()
                .url(ContractUtils.LOGIN_URL+"user/delMyShoucang")
                .addHeader("token",ContractUtils.getTOKEN(getContext()))
                .addParams("id",idd)
                .build()
                .execute(new StringCallback() {
                    @Override
                    public void onError(Call call, Exception e, int id) {

                    }

                    @Override
                    public void onResponse(String response, int id) {
                        System.out.println(response+"        删除文章的网络请求");
                        ContractUtils.Code500(getContext(),response);
                        if(response.indexOf("200") != -1){
//                            Toast.makeText(getContext(), "删除成功", Toast.LENGTH_SHORT).show();
                            //        我的收藏文章收藏的网络请求
                            initMyShouCangHttp();
                        }
                    }
                });
    }



    private void initView() {
//        myShouCangWenZhangAdapter = new MyShouCangWenZhangAdapter(getContext());
//        LinearLayoutManager manager = new LinearLayoutManager(getContext());
//        recyclerView.setLayoutManager(manager);
//        recyclerView.setAdapter(myShouCangWenZhangAdapter);
    }

}
