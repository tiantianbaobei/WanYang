package com.wanyangproject.mysuipian;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.google.gson.Gson;
import com.wanyangproject.R;
import com.wanyangproject.entity.ZhanPinLieBiaoEntity;
import com.wanyangproject.my.ZhaoPinXiangQingActivity;
import com.wanyangproject.myadapter.MyZhaoPinAdapter;
import com.wanyangproject.utils.ContractUtils;
import com.zhy.http.okhttp.OkHttpUtils;
import com.zhy.http.okhttp.callback.StringCallback;

import butterknife.BindView;
import butterknife.ButterKnife;
import okhttp3.Call;

/**
 * Created by 甜甜 on 2018/7/31.
 */

public class MyZhaoPinFragment extends Fragment {

    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    private MyZhaoPinAdapter myZhaoPinAdapter;
    private ZhanPinLieBiaoEntity zhanPinLieBiaoEntity;
    private int isok = 0;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.my_zhaopin_fragment, container, false);
        ButterKnife.bind(this, view);
        initView();

////        招聘收藏列表的网络请求
        initZhaoPinShouCangHttp();
        return view;
    }


//
//    @Override
//    public void onResume() {
//        super.onResume();
//        isok++;
//        if(isok == 1){
////        招聘收藏列表的网络请求
//            initZhaoPinShouCangHttp();
//        }
//    }




    //    招聘收藏列表的网络请求
  private void initZhaoPinShouCangHttp() {
        final ProgressDialog progressDialog = new ProgressDialog(getContext());
        progressDialog.setTitle("提示");
        progressDialog.setMessage("请等待...");
        progressDialog.setCancelable(false);
        progressDialog.show();
        OkHttpUtils.post()
                .url(ContractUtils.LOGIN_URL+"zhaopin/myshoucang")
                .addHeader("token",ContractUtils.getTOKEN(getContext()))
                .build()
                .execute(new StringCallback() {
                    @Override
                    public void onError(Call call, Exception e, int id) {
                        progressDialog.dismiss();
                    }

                    @Override
                    public void onResponse(String response, int id) {
                        progressDialog.dismiss();
                        System.out.println(response+"           招聘收藏列表的网络请求");
                        ContractUtils.Code500(getContext(),response);
                        if(response.indexOf("200") != -1){
                            Gson gson = new Gson();
                            zhanPinLieBiaoEntity = gson.fromJson(response, ZhanPinLieBiaoEntity.class);
                            myZhaoPinAdapter = new MyZhaoPinAdapter(getContext(),zhanPinLieBiaoEntity.getResponse());
                            LinearLayoutManager manager = new LinearLayoutManager(getContext());
                            recyclerView.setLayoutManager(manager);
                            recyclerView.setAdapter(myZhaoPinAdapter);


//                            点击进入详请
                            myZhaoPinAdapter.setXiangQingClick(new MyZhaoPinAdapter.XiangQingClick() {
                                @Override
                                public void xiangqingClick(int position, String id) {
                                    Intent intent = new Intent(getContext(), ZhaoPinXiangQingActivity.class);
                                    intent.putExtra("zhaopinid",id);
                                    System.out.println(id+"    点击招聘列表行布局传送的招聘id");
                                    startActivity(intent);
                                }
                            });




                            myZhaoPinAdapter.setShanChuClick(new MyZhaoPinAdapter.ShanChuClick() {
                                @Override
                                public void shamchuClick(int position, String id) {
//                                    删除招聘的的网络请求
                                    initShanChuHttp(position,id);
                                }
                            });
                        }else if(response.indexOf("400") != -1){
//                            ContractUtils.Code400(getContext(),response);
                            Toast.makeText(getContext(), "暂无数据", Toast.LENGTH_SHORT).show();
                            if(zhanPinLieBiaoEntity != null){
                                zhanPinLieBiaoEntity.getResponse().clear();

                                myZhaoPinAdapter.notifyDataSetChanged();
                            }
                        }
                    }
                });
    }



    private void initShanChuHttp(int postion,String id) {
        OkHttpUtils.post()
                .url(ContractUtils.LOGIN_URL+"zhaopin/shoucang")
                .addHeader("token",ContractUtils.getTOKEN(getContext()))
                .addParams("id",id)
                .build()
                .execute(new StringCallback() {
                    @Override
                    public void onError(Call call, Exception e, int id) {

                    }

                    @Override
                    public void onResponse(String response, int id) {
                        ContractUtils.Code500(getContext(),response);
                        if(response.indexOf("200") != -1){
                            Toast.makeText(getContext(), "删除成功", Toast.LENGTH_SHORT).show();
//                          招聘收藏列表的网络请求
                            initZhaoPinShouCangHttp();

                        }
                    }
                });
    }




    private void initView() {
//        myZhaoPinAdapter = new MyZhaoPinAdapter(getContext());
//        LinearLayoutManager manager = new LinearLayoutManager(getContext());
//        recyclerView.setLayoutManager(manager);
//        recyclerView.setAdapter(myZhaoPinAdapter);
    }

}
