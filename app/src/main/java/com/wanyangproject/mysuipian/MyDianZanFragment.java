package com.wanyangproject.mysuipian;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.google.gson.Gson;
import com.wanyangproject.R;
import com.wanyangproject.activity.TieZiXiangQingActivity;
import com.wanyangproject.entity.LunTanShanChuEntity;
import com.wanyangproject.entity.WoDeDianZanEntity;
import com.wanyangproject.myadapter.MyDianZanAdapter;
import com.wanyangproject.utils.ContractUtils;
import com.zhy.http.okhttp.OkHttpUtils;
import com.zhy.http.okhttp.callback.StringCallback;

import butterknife.BindView;
import butterknife.ButterKnife;
import okhttp3.Call;

/**
 * Created by 甜甜 on 2018/7/31.
 */

public class MyDianZanFragment extends Fragment {
    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    private MyDianZanAdapter myDianZanAdapter;
    private WoDeDianZanEntity woDeDianZanEntity;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.my_dianzan_fragment, container, false);
        ButterKnife.bind(this, view);
        initView();

//        我的论坛我的点赞的网络请求
        initDianZanHttp();

        return view;
    }


    @Override
    public void onResume() {
        super.onResume();
//        我的论坛我的点赞的网络请求
        initDianZanHttp();

    }

    //    我的论坛我的点赞的网络请求
    private void initDianZanHttp() {
        OkHttpUtils.post()
                .url(ContractUtils.LOGIN_URL+"forum/myLikeForum")
                .addHeader("token",ContractUtils.getTOKEN(getContext()))
                .build()
                .execute(new StringCallback() {
                    @Override
                    public void onError(Call call, Exception e, int id) {

                    }

                    @Override
                    public void onResponse(String response, int id) {
                        ContractUtils.Code500(getContext(),response);
                        System.out.println(response+"          我的论坛的我的点赞的网络请求");
                        if(response.indexOf("200") != -1){
                            Gson gson = new Gson();
                            woDeDianZanEntity = gson.fromJson(response, WoDeDianZanEntity.class);
                            myDianZanAdapter = new MyDianZanAdapter(getContext(),woDeDianZanEntity.getResponse());
                            LinearLayoutManager manager = new LinearLayoutManager(getContext());
                            recyclerView.setLayoutManager(manager);
                            recyclerView.setAdapter(myDianZanAdapter);


                            myDianZanAdapter.setShanChuClick(new MyDianZanAdapter.ShanChuClick() {
                                @Override
                                public void shanchuClick(int position, String id) {
//                                    点赞删除的网络请求
                                    initDianZanShanChuHttp(position,id);
                                }
                            });



//                            详情
                            myDianZanAdapter.setXiangQingClick(new MyDianZanAdapter.XiangQingClick() {
                                @Override
                                public void xiangqingClick(int position, String id) {
                                    //                                    点击进入帖子详情
                                    Intent intent = new Intent(getContext(), TieZiXiangQingActivity.class);
                                    intent.putExtra("ispc",woDeDianZanEntity.getResponse().get(position).getIsPc());
                                    intent.putExtra("tieziid",id);
                                    System.out.println(id+"     发送帖子的id");
                                    startActivity(intent);
                                }
                            });
                        }else if(response.indexOf("400") != -1){
                            if(woDeDianZanEntity != null){
                                woDeDianZanEntity.getResponse().clear();

                                myDianZanAdapter.notifyDataSetChanged();
                            }
                        }
                    }
                });
    }






//    点赞删除的网络请求
    private void initDianZanShanChuHttp(int position, String id) {
        OkHttpUtils.post()
                .url(ContractUtils.LOGIN_URL+"forum/toLike")
                .addHeader("token",ContractUtils.getTOKEN(getContext()))
                .addParams("forumId",id)
                .build()
                .execute(new StringCallback() {
                    @Override
                    public void onError(Call call, Exception e, int id) {

                    }

                    @Override
                    public void onResponse(String response, int id) {
                        System.out.println(response+"        点赞删除的网络请求 ");
                        ContractUtils.Code500(getContext(),response);
                        if(response.indexOf("200") != -1){
                            Gson gson = new Gson();
                            LunTanShanChuEntity lunTanShanChuEntity = gson.fromJson(response, LunTanShanChuEntity.class);
                            Toast.makeText(getContext(), "删除成功", Toast.LENGTH_SHORT).show();
                            //        我的论坛我的点赞的网络请求
                            initDianZanHttp();
                        }
                    }
                });
    }






    private void initView() {
//        myDianZanAdapter = new MyDianZanAdapter(getContext());
//        LinearLayoutManager manager = new LinearLayoutManager(getContext());
//        recyclerView.setLayoutManager(manager);
//        recyclerView.setAdapter(myDianZanAdapter);
    }
}
