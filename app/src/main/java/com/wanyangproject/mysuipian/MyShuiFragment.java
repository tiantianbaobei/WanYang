package com.wanyangproject.mysuipian;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.gson.Gson;
import com.wanyangproject.R;
import com.wanyangproject.entity.MenWeiShuiDianXinXiEntity;
import com.wanyangproject.entity.QiYeShuiDianJiLuEntity;
import com.wanyangproject.myadapter.ShuiAdapter;
import com.wanyangproject.utils.ContractUtils;
import com.zhy.http.okhttp.OkHttpUtils;
import com.zhy.http.okhttp.callback.StringCallback;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import okhttp3.Call;

/**
 * Created by 甜甜 on 2018/7/31.
 */

public class MyShuiFragment extends Fragment {
    @BindView(R.id.reyclerView)
    RecyclerView reyclerView;
    private ShuiAdapter shuiAdapter;
    private String waterUserId;
    private QiYeShuiDianJiLuEntity qiYeShuiDianJiLuEntity;

    private ArrayList<QiYeShuiDianJiLuEntity.ResponseBean.DataBean.ListBean> list = new ArrayList<>();

    public MenWeiShuiDianXinXiEntity weiShuiDianXinXiEntity;


    public MenWeiShuiDianXinXiEntity getWeiShuiDianXinXiEntity() {
        return weiShuiDianXinXiEntity;
    }

    public void setWeiShuiDianXinXiEntity(MenWeiShuiDianXinXiEntity weiShuiDianXinXiEntity) {
        this.weiShuiDianXinXiEntity = weiShuiDianXinXiEntity;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.shui_fragment, container, false);
        ButterKnife.bind(this, view);
        initView();




//        企业用水记录的网络请求
        initYongShuiHttp();


        initYongShuiHttp222();


        return view;
    }



//    员工用水记录的网络请求
    private void initYongShuiHttp() {
        System.out.println();

        if(weiShuiDianXinXiEntity != null){
            if(weiShuiDianXinXiEntity.getResponse() != null){
                if(weiShuiDianXinXiEntity.getResponse().getData() != null){
                    if(weiShuiDianXinXiEntity.getResponse().getData().get(0).getWaterUserId() != null){
                        waterUserId = weiShuiDianXinXiEntity.getResponse().getData().get(0).getWaterUserId();
                    }
                }else {
                    return;
                }
            }else {
                return;
            }
        }else {
            return;
        }
        OkHttpUtils.post()
//                .url(ContractUtils.LOGIN_URL+"company/getWaterEle111")
                .url(ContractUtils.LOGIN_URL+"company/hydropower")
                .addHeader("token",ContractUtils.getTOKEN(getContext()))
                .addParams("type","1")//1代表水 2代表电
                .addParams("parkId",ContractUtils.getParkId(getContext()))
                .addParams("electricityUserId",waterUserId)
                .addParams("page","0")
                .build()
                .execute(new StringCallback() {
                    @Override
                    public void onError(Call call, Exception e, int id) {

                    }

                    @Override
                    public void onResponse(String response, int id) {
                        System.out.println(response+"        企业用水记录的网络请求 ");
                        ContractUtils.Code500(getContext(),response);
                        if(response.indexOf("list") != -1){
                            Gson gson = new Gson();
                            qiYeShuiDianJiLuEntity = gson.fromJson(response, QiYeShuiDianJiLuEntity.class);

                            for (int i = 0; i < qiYeShuiDianJiLuEntity.getResponse().getData().getList().size(); i++) {
                                QiYeShuiDianJiLuEntity.ResponseBean.DataBean.ListBean listBean = qiYeShuiDianJiLuEntity.getResponse().getData().getList().get(i);
                                listBean.setName("水表1");
                                list.add(listBean);
                            }

                            shuiAdapter = new ShuiAdapter(getContext(),list);
                            LinearLayoutManager manager = new LinearLayoutManager(getContext());
                            reyclerView.setLayoutManager(manager);
                            reyclerView.setAdapter(shuiAdapter);
                        }else if(response.indexOf("400") != -1){
                            ContractUtils.Code400(getContext(),response);
                        }
                    }
                });
    }



















    //    员工用水记录的网络请求
    public void initYongShuiHttp222() {
        System.out.println();

        if(weiShuiDianXinXiEntity != null){
            if(weiShuiDianXinXiEntity.getResponse() != null){
                if(weiShuiDianXinXiEntity.getResponse().getData() != null){
                    if(weiShuiDianXinXiEntity.getResponse().getData().size() < 2){
                        return;
                    }else{
                        if(weiShuiDianXinXiEntity.getResponse().getData().get(1).getWaterUserId() != null){
                            waterUserId = weiShuiDianXinXiEntity.getResponse().getData().get(1).getWaterUserId();
                        }
                    }
                }else {
                    return;
                }
            }else {
                return;
            }
        }else {
            return;
        }
        OkHttpUtils.post()
//                .url(ContractUtils.LOGIN_URL+"company/getWaterEle111")
                .url(ContractUtils.LOGIN_URL+"company/hydropower")
                .addHeader("token",ContractUtils.getTOKEN(getContext()))
                .addParams("type","1")//1代表水 2代表电
                .addParams("parkId",ContractUtils.getParkId(getContext()))
                .addParams("electricityUserId",waterUserId)
                .addParams("page","0")
                .build()
                .execute(new StringCallback() {
                    @Override
                    public void onError(Call call, Exception e, int id) {

                    }

                    @Override
                    public void onResponse(String response, int id) {
                        System.out.println(response+"        企业用水记录的网络请求 ");
                        ContractUtils.Code500(getContext(),response);
                        if(response.indexOf("list") != -1){
                            Gson gson = new Gson();
                            qiYeShuiDianJiLuEntity = gson.fromJson(response, QiYeShuiDianJiLuEntity.class);
                            for (int i = 0; i < qiYeShuiDianJiLuEntity.getResponse().getData().getList().size(); i++) {
                                QiYeShuiDianJiLuEntity.ResponseBean.DataBean.ListBean listBean = qiYeShuiDianJiLuEntity.getResponse().getData().getList().get(i);
                                listBean.setName("水表2");
                                list.add(listBean);
                            }

                            shuiAdapter = new ShuiAdapter(getContext(),list);
                            LinearLayoutManager manager = new LinearLayoutManager(getContext());
                            reyclerView.setLayoutManager(manager);
                            reyclerView.setAdapter(shuiAdapter);
                        }else if(response.indexOf("400") != -1){
                            ContractUtils.Code400(getContext(),response);
                        }
                    }
                });
    }


    private void initView() {
//        shuiAdapter = new ShuiAdapter(getContext());
//        LinearLayoutManager manager = new LinearLayoutManager(getContext());
//        reyclerView.setLayoutManager(manager);
//        reyclerView.setAdapter(shuiAdapter);
    }
}
