package com.wanyangproject.mysuipian;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.google.gson.Gson;
import com.wanyangproject.R;
import com.wanyangproject.entity.QuanBuEntity;
import com.wanyangproject.entity.QueRenShouHuoEntity;
import com.wanyangproject.entity.QueRenShouHuoShiBaiEntity;
import com.wanyangproject.my.QuanBuActivity;
import com.wanyangproject.myadapter.QuanBuAdapter;
import com.wanyangproject.utils.ContractUtils;
import com.zhy.http.okhttp.OkHttpUtils;
import com.zhy.http.okhttp.callback.StringCallback;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import okhttp3.Call;

/**
 * Created by 甜甜 on 2018/7/30.
 */

public class DingDanQuanBuFragment extends Fragment {
    @BindView(R.id.recycerView)
    RecyclerView recycerView;
    private QuanBuAdapter quanBuAdapter;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.dingdan_quanbu_fragment, container, false);
        ButterKnife.bind(this, view);
        initView();

        //        订单列表的网络请求
//        initDingDanLieBiaoHttp();

        return view;
    }


//    @Override
//    public void onResume() {
//        super.onResume();
//        //        订单列表的网络请求
//        initDingDanLieBiaoHttp();
//    }

    //    订单列表的网络请求
    public void initDingDanLieBiaoHttp() {
        OkHttpUtils.post()
                .url(ContractUtils.LOGIN_URL+"Life/orderlist")
                .addHeader("token",ContractUtils.getTOKEN(getContext()))
                .addParams("parkId",ContractUtils.getParkId(getContext()))
                .addParams("type","1,2,3,4,5,6,7,8,9,10,11,12")
                .build()
                .execute(new StringCallback() {
                    @Override
                    public void onError(Call call, Exception e, int id) {

                    }

                    @Override
                    public void onResponse(String response, int id) {
                        ContractUtils.Code500shibai(getContext(),response);
                        System.out.println(response+"      订单列表全部的网络请求");

                        if(response.indexOf("200") != -1){
                            Gson gson = new Gson();
                            QuanBuEntity quanBuEntity = gson.fromJson(response, QuanBuEntity.class);
                            quanBuAdapter = new QuanBuAdapter(getContext(),quanBuEntity.getResponse());
                            LinearLayoutManager manager = new LinearLayoutManager(getContext());
                            recycerView.setLayoutManager(manager);
                            recycerView.setAdapter(quanBuAdapter);


                            quanBuAdapter.setQueRenShouHuoClick(new QuanBuAdapter.QueRenShouHuoClick() {
                                @Override
                                public void querenshouhuoClick(int position, String dingdanhao) {
                                    //           确认收货的网络请求
                                    initQueRenShouHuoHttp(position,dingdanhao);
                                }
                            });
                        }else if(response.indexOf("400") != -1){
                            Toast.makeText(getContext(), "您还没有订单，到【园区生活】中采购一番哦！", Toast.LENGTH_SHORT).show();
                        }
                    }
                });
    }





    //    确认收货的网络请求
    private void initQueRenShouHuoHttp(int position, String dingdanhao) {
        OkHttpUtils.post()
                .url(ContractUtils.LOGIN_URL+"user/userrefuse")
                .addHeader("token",ContractUtils.getTOKEN(getContext()))
                .addParams("parkId",ContractUtils.getParkId(getContext()))
                .addParams("order_sn",dingdanhao)
                .build()
                .execute(new StringCallback() {
                    @Override
                    public void onError(Call call, Exception e, int id) {

                    }

                    @Override
                    public void onResponse(String response, int id) {
                        ContractUtils.Code500shibai(getContext(),response);
                        System.out.println(response+"          用户确认收货的网络请求");

                        if(response.indexOf("200") != -1){
                            Gson gson = new Gson();
                            QueRenShouHuoEntity queRenShouHuoEntity = gson.fromJson(response, QueRenShouHuoEntity.class);
                            //        订单列表的网络请求
                            initDingDanLieBiaoHttp();
                            Toast.makeText(getContext(), queRenShouHuoEntity.getResponse().getMes(), Toast.LENGTH_SHORT).show();

//                            Toast.makeText(getContext(), queRenShouHuoEntity.getMsg(), Toast.LENGTH_SHORT).show();
//                            Intent intent = new Intent(getContext(), FinishXiangQingActivity.class);
//                            startActivity(intent);

                        }else if(response.indexOf("400") != -1){
                            Gson gson1 = new Gson();
                            QueRenShouHuoShiBaiEntity queRenShouHuoShiBaiEntity = gson1.fromJson(response, QueRenShouHuoShiBaiEntity.class);
                            Toast.makeText(getContext(), queRenShouHuoShiBaiEntity.getMsg(), Toast.LENGTH_SHORT).show();

                        }
                    }
                });
    }

    private void initView() {
//        quanBuAdapter = new QuanBuAdapter(getContext());
//        LinearLayoutManager manager = new LinearLayoutManager(getContext());
//        recycerView.setLayoutManager(manager);
//        recycerView.setAdapter(quanBuAdapter);
    }
}
