package com.wanyangproject.mysuipian;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.wanyangproject.R;
import com.wanyangproject.activity.YiKaTongZhiFuActivity;
import com.wanyangproject.entity.MenWeiShuiDianXinXiEntity;
import com.wanyangproject.entity.MyQiYeChangFangXiangQingEntity;
import com.wanyangproject.entity.QiYeSuSheXiangQingEntity;
import com.wanyangproject.entity.YiKaTongYuEEntity;
import com.wanyangproject.shouye.ZhiFuActivity;
import com.wanyangproject.utils.ContractUtils;
import com.zhy.http.okhttp.OkHttpUtils;
import com.zhy.http.okhttp.callback.StringCallback;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.Call;

/**
 * Created by 甜甜 on 2018/8/1.
 */

public class DianFragment extends Fragment {
    @BindView(R.id.tv_dianfei_zhanghao)
    TextView tvDianfeiZhanghao;
    @BindView(R.id.tv_money)
    TextView tvMoney;
    @BindView(R.id.btn_wushi)
    Button btnWushi;
    @BindView(R.id.btn_yibai)
    Button btnYibai;
    @BindView(R.id.btn_erbai)
    Button btnErbai;
    @BindView(R.id.btn_queren)
    Button btnQueren;
    @BindView(R.id.et_jine)
    EditText etJine;
    private String money = "0";
    public MenWeiShuiDianXinXiEntity weiShuiDianXinXiEntity;
    private NetWork netWork;
    private String chongzhichenggong;
    private String electricityUserId;
    private String electricityUserName;
    private QiYeSuSheXiangQingEntity qiYeSuSheXiangQingEntity;
    private MyQiYeChangFangXiangQingEntity myQiYeChangFangXiangQingEntity;

    private String dianname;
    private String dianid;
    private String yuedian;

    public TextView getTvDianfeiZhanghao() {
        return tvDianfeiZhanghao;
    }

    public void setTvDianfeiZhanghao(TextView tvDianfeiZhanghao) {
        this.tvDianfeiZhanghao = tvDianfeiZhanghao;
    }

    public String getDianname() {
        return dianname;
    }

    public void setDianname(String dianname) {
        this.dianname = dianname;
    }

    public String getDianid() {
        return dianid;
    }

    public void setDianid(String dianid) {
        this.dianid = dianid;
    }

    public String getYuedian() {
        return yuedian;
    }

    public void setYuedian(String yuedian) {
        this.yuedian = yuedian;
    }

    //
//    public MyQiYeChangFangXiangQingEntity getMyQiYeChangFangXiangQingEntity() {
//        return myQiYeChangFangXiangQingEntity;
//    }
//
//    public void setMyQiYeChangFangXiangQingEntity(MyQiYeChangFangXiangQingEntity myQiYeChangFangXiangQingEntity) {
//        this.myQiYeChangFangXiangQingEntity = myQiYeChangFangXiangQingEntity;
//    }
//
//    public QiYeSuSheXiangQingEntity getQiYeSuSheXiangQingEntity() {
//        return qiYeSuSheXiangQingEntity;
//    }
//
//    public void setQiYeSuSheXiangQingEntity(QiYeSuSheXiangQingEntity qiYeSuSheXiangQingEntity) {
//        this.qiYeSuSheXiangQingEntity = qiYeSuSheXiangQingEntity;
//    }

    public MenWeiShuiDianXinXiEntity getWeiShuiDianXinXiEntity() {
        return weiShuiDianXinXiEntity;
    }

    public void setWeiShuiDianXinXiEntity(MenWeiShuiDianXinXiEntity weiShuiDianXinXiEntity) {
        this.weiShuiDianXinXiEntity = weiShuiDianXinXiEntity;
    }




    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.dianfei_fragment, container, false);
        ButterKnife.bind(this, view);




        if(dianname != null){
            tvDianfeiZhanghao.setText("电表用户:"+dianname);
        }

        if(yuedian != null){
            tvMoney.setText("¥"+yuedian);
        }
//

////        //        一卡通余额的网络请求
//        initYiKaTongYuEHttp();
//
//        员工的水电充值
        if(weiShuiDianXinXiEntity != null){
            if(weiShuiDianXinXiEntity.getResponse().getDian() != null){
                if(weiShuiDianXinXiEntity.getResponse().getDian().getElectricityUserName() != null){
                    tvDianfeiZhanghao.setText("电表用户:"+weiShuiDianXinXiEntity.getResponse().getDian().getElectricityUserName());
                }
            }
        }

        if(weiShuiDianXinXiEntity != null){
            if(weiShuiDianXinXiEntity.getResponse().getDian() != null){
                if(weiShuiDianXinXiEntity.getResponse().getDian().getBalance() != null){
                    tvMoney.setText("¥"+weiShuiDianXinXiEntity.getResponse().getDian().getBalance());
                }
            }
        }


        IntentFilter intentFilter4 = new IntentFilter();
        intentFilter4.addAction("chongzhichenggong");
        netWork = new NetWork();
        getContext().registerReceiver(netWork, intentFilter4);



        return view;
    }









    @Override
    public void onDestroy() {
        super.onDestroy();
        getActivity().unregisterReceiver(netWork);
    }


    class NetWork extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {

            //            结束页面
            chongzhichenggong = intent.getStringExtra("chongzhichenggong");
            if(chongzhichenggong != null){
                getActivity().finish();
            }
        }
    }





//    //    一卡通余额的网络请求
//    private void initYiKaTongYuEHttp() {
//        OkHttpUtils.post()
//                .url(ContractUtils.LOGIN_URL + "user/getCardInfo")
//                .addHeader("token", ContractUtils.getTOKEN(getContext()))
//                .build()
//                .execute(new StringCallback() {
//                    @Override
//                    public void onError(Call call, Exception e, int id) {
//                        System.out.println(e + "   eeee一卡通余额");
//                    }
//
//                    @Override
//                    public void onResponse(String response, int id) {
//                        ContractUtils.Code500(getContext(), response);
//                        System.out.println(response + "            一卡通余额的网络请求");
//                        if (response.indexOf("200") != -1) {
//                            Gson gson = new Gson();
//                            YiKaTongYuEEntity yiKaTongYuEEntity = gson.fromJson(response, YiKaTongYuEEntity.class);
//                            if (yiKaTongYuEEntity.getResponse().getBalance() == null) {
//
//                            } else {
//                                tvMoney.setText("¥" + yiKaTongYuEEntity.getResponse().getBalance());
//                            }
//
////                            if (yiKaTongYuEEntity.getResponse().getCardNumber() == null) {
////
////                            } else {
////                                tvDianfeiZhanghao.setText("电费账号: " + yiKaTongYuEEntity.getResponse().getCardNumber());
////                            }
//                        }
//                    }
//                });
//    }
//




    @OnClick({R.id.tv_dianfei_zhanghao, R.id.tv_money, R.id.btn_wushi, R.id.btn_yibai, R.id.btn_erbai, R.id.btn_queren})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.tv_dianfei_zhanghao:
                break;
            case R.id.tv_money:
                break;
            case R.id.btn_wushi:
                money = "50";
                setFastRecharge(btnWushi, true);
                setFastRecharge(btnYibai, false);
                setFastRecharge(btnErbai, false);
                etJine.setText(money);
                break;
            case R.id.btn_yibai:
                money = "100";
                setFastRecharge(btnWushi, false);
                setFastRecharge(btnYibai, true);
                setFastRecharge(btnErbai, false);
                etJine.setText(money);
                break;
            case R.id.btn_erbai:
                money = "200";
                setFastRecharge(btnWushi, false);
                setFastRecharge(btnYibai, false);
                setFastRecharge(btnErbai, true);
                etJine.setText(money);
                break;
            case R.id.btn_queren:
                if (etJine.getText().toString().trim().equals("") && money.equals("0")) {
                    Toast.makeText(getContext(), "请选择或输入充值金额", Toast.LENGTH_SHORT).show();
                    return;
                }
                if(etJine.getText().toString().trim().length() > 0){
                    money = etJine.getText().toString().trim();
                }

                if(weiShuiDianXinXiEntity != null){
                    if(weiShuiDianXinXiEntity.getResponse() != null){
                        if(weiShuiDianXinXiEntity.getResponse().getDian() != null){
                            electricityUserId = weiShuiDianXinXiEntity.getResponse().getDian().getElectricityUserId();
                        }
                    }
                }
                if(weiShuiDianXinXiEntity != null){
                    if(weiShuiDianXinXiEntity.getResponse() != null){
                        if(weiShuiDianXinXiEntity.getResponse().getDian() != null){
                            electricityUserName = weiShuiDianXinXiEntity.getResponse().getDian().getElectricityUserName();
                        }
                    }
                }


                if(dianid == null){
                    Toast.makeText(getContext(), "暂无电表信息，无法充值！", Toast.LENGTH_SHORT).show();
                    return;
                }


                Intent intent1 = new Intent(getContext(), YiKaTongZhiFuActivity.class);
                intent1.putExtra("money", money);
                intent1.putExtra("state","3");//1：一卡通2：水费3：电费
                intent1.putExtra("id",dianid);
                intent1.putExtra("name",dianname);
                startActivity(intent1);
//                getActivity().finish();
                break;
        }
    }

    private void setFastRecharge(Button button, boolean isChecked) {
        if (isChecked) {
            button.setTextColor(getResources().getColor(R.color.white));
            button.setBackgroundResource(R.drawable.shape_red_bg);
        } else {
            button.setTextColor(getResources().getColor(R.color.red));
            button.setBackgroundResource(R.drawable.shape_red_border);
        }
    }
}
