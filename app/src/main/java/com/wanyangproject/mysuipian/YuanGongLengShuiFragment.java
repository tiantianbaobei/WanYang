package com.wanyangproject.mysuipian;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.wanyangproject.R;
import com.wanyangproject.myadapter.YuanGongLengShuiAdapter;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by 甜甜 on 2018/8/1.
 */

public class YuanGongLengShuiFragment extends Fragment {
    @BindView(R.id.recycerView)
    RecyclerView recycerView;
    private YuanGongLengShuiAdapter yuanGongLengShuiAdapter;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.dingdan_daishouhuo_fragment, container, false);
        ButterKnife.bind(this, view);
        initView();
        return view;
    }

    private void initView() {
        yuanGongLengShuiAdapter = new YuanGongLengShuiAdapter(getContext());
        LinearLayoutManager manager = new LinearLayoutManager(getContext());
        recycerView.setLayoutManager(manager);
        recycerView.setAdapter(yuanGongLengShuiAdapter);
    }

}
