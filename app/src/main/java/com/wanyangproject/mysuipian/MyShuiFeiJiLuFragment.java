package com.wanyangproject.mysuipian;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.scwang.smartrefresh.layout.SmartRefreshLayout;
import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.scwang.smartrefresh.layout.listener.OnLoadmoreListener;
import com.scwang.smartrefresh.layout.listener.OnRefreshListener;
import com.wanyangproject.R;
import com.wanyangproject.activity.LoginActivity;
import com.wanyangproject.myadapter.ShuiFeiAdapter;
import com.wanyangproject.utils.ContractUtils;
import com.zhy.http.okhttp.OkHttpUtils;
import com.zhy.http.okhttp.callback.StringCallback;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import okhttp3.Call;

/**
 * Created by 甜甜 on 2018/8/1.
 */

public class MyShuiFeiJiLuFragment extends Fragment implements OnLoadmoreListener {
    @BindView(R.id.recycerView)
    RecyclerView recycerView;
    @BindView(R.id.smart)
    SmartRefreshLayout smart;
    private ShuiFeiAdapter shuiFeiAdapter;
//    private JSONArray list;
    private int page = 0;
    private ArrayList<JSONObject> list = new ArrayList();//总
    private ArrayList<JSONObject>  shanxuaihou = new ArrayList();//筛选之后的
    private String time;
    private NetWork netWork;
    private String timejilu;
    private String yuangongshuiid;
//    private String yuangongshuiid2;


    public String getYuangongshuiid() {
        return yuangongshuiid;
    }

    public void setYuangongshuiid(String yuangongshuiid) {
        this.yuangongshuiid = yuangongshuiid;
    }

//    public String getYuangongshuiid2() {
//        return yuangongshuiid2;
//    }
//
//    public void setYuangongshuiid2(String yuangongshuiid2) {
//        this.yuangongshuiid2 = yuangongshuiid2;
//    }

    private String shuiid;
//    private String shuiid2;

    public String getShuiid() {
        return shuiid;
    }

    public void setShuiid(String shuiid) {
        this.shuiid = shuiid;
    }

//    public String getShuiid2() {
//        return shuiid2;
//    }
//
//    public void setShuiid2(String shuiid2) {
//        this.shuiid2 = shuiid2;
//    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.shuidian_jilu_fragment, container, false);
        ButterKnife.bind(this, view);
        initView();


        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("timejilu");
        netWork = new NetWork();
        getContext().registerReceiver(netWork, intentFilter);

        //                下拉刷新
//        smart.setOnRefreshListener(this);
//        上拉加载
        smart.setOnLoadmoreListener(this);




        if(shuiid != null){
            //        充值记录水费的网络请求
            initShuiFeiHttp();
        }




        return view;
    }




    class NetWork extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            timejilu = intent.getStringExtra("timejilu");
            time = intent.getStringExtra("time");
//            Toast.makeText(context, wxopenid + wxnickname + wxheadimgurl, Toast.LENGTH_SHORT).show();
            System.out.println(timejilu + "        timejilu");

            shanxuaihou.clear();
            for (int i = 0; i < list.size(); i++) {
                JSONObject jsonObject2 = list.get(i);
                String time1 = null;
                try {
                    time1 = jsonObject2.getString("time");
                    if (time1.indexOf(time) != -1){
                        shanxuaihou.add(jsonObject2);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            shuiFeiAdapter.notifyDataSetChanged();
        }
    }








    //    充值记录水费的网络请求
    private void initShuiFeiHttp() {
        OkHttpUtils.post()
                .url(ContractUtils.LOGIN_URL + "user/record")
                .addHeader("token", ContractUtils.getTOKEN(getContext()))
                .addParams("parkId", ContractUtils.getParkId(getContext()))
                .addParams("id",shuiid)
                .addParams("page", page+"")
                .addParams("type", "1")//1：水费充值记录 2：电表充值记录 3：一卡通
                .build()
                .execute(new StringCallback() {
                    @Override
                    public void onError(Call call, Exception e, int id) {

                    }

                    @Override
                    public void onResponse(String response, int id) {
                        System.out.println(response + "          充值记录水费的网络请求");
                        ContractUtils.Code500(getContext(), response);
                        if (response.indexOf("200") != -1) {

                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                JSONObject jsonObject1 = jsonObject.getJSONObject("response");
                                JSONObject data = jsonObject1.getJSONObject("data");
                                if (data != null) {
                                    JSONArray jsonArray = data.getJSONArray("list");

                                    for (int i = 0; i < jsonArray.length(); i++) {
                                        JSONObject object = (JSONObject) jsonArray.get(i);
                                        if(object != null){
                                            list.add(object);
                                        }
                                    }
                                }


                                if(time != null){
                                    shanxuaihou.clear();
                                    for (int i = 0; i < list.size(); i++) {
                                        JSONObject jsonObject2 = list.get(i);
                                        String time1 = jsonObject2.getString("time");
                                        if (time1.indexOf(time) != -1){
                                            shanxuaihou.add(jsonObject2);
                                        }
                                    }

//                                    chongZhiAdapter.notifyDataSetChanged();
                                }else{
                                    shanxuaihou.clear();
                                    shanxuaihou.addAll(list);
                                }


                                if (list == null) {
                                    Toast.makeText(getContext(), "暂无数据", Toast.LENGTH_SHORT).show();
                                    return;
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

//
//                            Gson gson = new Gson();
//                            ShuiFeiJiLuEntity shuiFeiJiLuEntity = gson.fromJson(response, ShuiFeiJiLuEntity.class);
                            shuiFeiAdapter = new ShuiFeiAdapter(getContext(), shanxuaihou);
                            LinearLayoutManager manager = new LinearLayoutManager(getContext());
                            recycerView.setLayoutManager(manager);
                            recycerView.setAdapter(shuiFeiAdapter);

                        }
                    }
                });
    }


    private void initView() {
//        shuiFeiAdapter = new ShuiFeiAdapter(getContext());
//        LinearLayoutManager manager = new LinearLayoutManager(getContext());
//        recycerView.setLayoutManager(manager);
//        recycerView.setAdapter(shuiFeiAdapter);
    }


    @Override
    public void onPause() {
        super.onPause();
        page = 0;
    }



//    @Override
//    public void onRefresh(RefreshLayout refreshlayout) {
//        page = 0;
//
//        //        充值记录水费的网络请求
//        initShuiFeiHttp();
//        if (refreshlayout.isRefreshing()) {
//            refreshlayout.finishRefresh();
//        }
//    }

    @Override
    public void onLoadmore(RefreshLayout refreshlayout) {
        page++;

        //        充值记录水费的网络请求
        initShuiFeiHttp();
        refreshlayout.finishLoadmore();
    }
}
