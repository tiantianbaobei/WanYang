package com.wanyangproject.mysuipian;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.google.gson.Gson;
import com.wanyangproject.R;
import com.wanyangproject.entity.FinishEntity;
import com.wanyangproject.my.FinishXiangQingActivity;
import com.wanyangproject.myadapter.FinishAdapter;
import com.wanyangproject.utils.ContractUtils;
import com.zhy.http.okhttp.OkHttpUtils;
import com.zhy.http.okhttp.callback.StringCallback;

import butterknife.BindView;
import butterknife.ButterKnife;
import okhttp3.Call;

/**
 * Created by 甜甜 on 2018/7/30.
 */

public class FinishFragment extends Fragment {

    @BindView(R.id.recycerView)
    RecyclerView recycerView;
    private FinishAdapter finishAdapter;
    private FinishEntity finishEntity;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.dingdan_finish_fragment, container, false);
        ButterKnife.bind(this, view);
        initView();

//       订单列表已完成的网络请求
//        initFinishHttp();
        return view;
    }

//
//    @Override
//    public void onResume() {
//        super.onResume();
//        //       订单列表已完成的网络请求
//        initFinishHttp();
//    }

    //    订单列表已完成的网络请求
    public void initFinishHttp() {
        OkHttpUtils.post()
                .url(ContractUtils.LOGIN_URL+"Life/orderlist")
                .addHeader("token",ContractUtils.getTOKEN(getContext()))
                .addParams("parkId",ContractUtils.getParkId(getContext()))
                .addParams("type","9,12")//订单状态1：未支付2：已支付3：代发货4：已完成5：商家已接单6：商家准备完成7：商家确认送达8：用户确认送达9：已评价10：商家拒绝接单11:用户申请退款12：退款订单完成
                .build()
                .execute(new StringCallback() {
                    @Override
                    public void onError(Call call, Exception e, int id) {

                    }

                    @Override
                    public void onResponse(String response, int id) {
                        ContractUtils.Code500shibai(getContext(),response);
                        System.out.println(response+"     订单列表已完成的网络请求");

                        if(response.indexOf("200") != -1){
                            Gson gson = new Gson();
                            finishEntity = gson.fromJson(response, FinishEntity.class);
                            finishAdapter = new FinishAdapter(getContext(),finishEntity.getResponse());
                            LinearLayoutManager manager = new LinearLayoutManager(getContext());
                            recycerView.setLayoutManager(manager);
                            recycerView.setAdapter(finishAdapter);

//                            点击进入详情
                            finishAdapter.setFinishClick(new FinishAdapter.FinishClick() {
                                @Override
                                public void finishClick(int position) {
                                    Intent intent = new Intent(getContext(), FinishXiangQingActivity.class);
                                    intent.putExtra("dingdanhao",finishEntity.getResponse().get(position).getOrder_sn());
                                    startActivity(intent);
                                }
                            });
                        }else if(response.indexOf("400") != -1){
                            Toast.makeText(getContext(), "您还没有已完成订单！", Toast.LENGTH_SHORT).show();
                            if(finishEntity != null){
                                finishEntity.getResponse().clear();

                                finishAdapter.notifyDataSetChanged();
                            }
                        }
                    }
                });
    }

    private void initView() {
//        finishAdapter = new FinishAdapter(getContext());
//        LinearLayoutManager manager = new LinearLayoutManager(getContext());
//        recycerView.setLayoutManager(manager);
//        recycerView.setAdapter(finishAdapter);
//
//        finishAdapter.setFinishClick(new FinishAdapter.FinishClick() {
//            @Override
//            public void finishClick(int position) {
//                Intent intent = new Intent(getContext(), FinishXiangQingActivity.class);
//                startActivity(intent);
//            }
//        });
    }

}
