package com.wanyangproject.mysuipian;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.google.gson.Gson;
import com.wanyangproject.R;
import com.wanyangproject.activity.TieZiXiangQingActivity;
import com.wanyangproject.entity.FaBuTieZiEntity;
import com.wanyangproject.entity.LunTanShanChuEntity;
import com.wanyangproject.myadapter.MyFaBuAdapter;
import com.wanyangproject.utils.ContractUtils;
import com.zhy.http.okhttp.OkHttpUtils;
import com.zhy.http.okhttp.callback.StringCallback;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import okhttp3.Call;

/**
 * Created by 甜甜 on 2018/7/31.
 */

public class MyFaBuFragment extends Fragment {
    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    private MyFaBuAdapter myFaBuAdapter;
    private FaBuTieZiEntity faBuTieZiEntity;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.my_fabu_fragment, container, false);
        ButterKnife.bind(this, view);
        initView();


//        我发布帖子的网络请求
        initFaBuTieZiHttp();

        return view;
    }


    @Override
    public void onResume() {
        super.onResume();
        //        我发布帖子的网络请求
        initFaBuTieZiHttp();
    }

    //    我发布帖子的网络请求
    private void initFaBuTieZiHttp() {
        OkHttpUtils.post()
                .url(ContractUtils.LOGIN_URL+"forum/myForum")
                .addHeader("token",ContractUtils.getTOKEN(getContext()))
                .build()
                .execute(new StringCallback() {
                    @Override
                    public void onError(Call call, Exception e, int id) {

                    }

                    @Override
                    public void onResponse(String response, int id) {
                        ContractUtils.Code500(getContext(),response);
                        System.out.println(response+"      我发布帖子的网络请求");
                        if(response.indexOf("200") != -1){
                            Gson gson = new Gson();
                            faBuTieZiEntity = gson.fromJson(response, FaBuTieZiEntity.class);
                            myFaBuAdapter = new MyFaBuAdapter(getContext(),faBuTieZiEntity.getResponse().getList());
                            LinearLayoutManager manager = new LinearLayoutManager(getContext());
                            recyclerView.setLayoutManager(manager);
                            recyclerView.setAdapter(myFaBuAdapter);


                            myFaBuAdapter.setShanChuClick(new MyFaBuAdapter.ShanChuClick() {
                                @Override
                                public void shanchuClick(int position, String id) {
//                                    删除的网络请求
                                    initDeleteHttp(position,id);
                                }
                            });


                            myFaBuAdapter.setXiangQingClick(new MyFaBuAdapter.XiangQingClick() {
                                @Override
                                public void xiangqingClick(int position, String id) {
//                                    点击进入帖子详情
                                    Intent intent = new Intent(getContext(), TieZiXiangQingActivity.class);
                                    intent.putExtra("ispc",faBuTieZiEntity.getResponse().getList().get(position).getIsPc());
                                    intent.putExtra("tieziid",id);
                                    System.out.println(id+"     发送帖子的id");
                                    startActivity(intent);
                                }
                            });


                        }else if(response.indexOf("400") != -1){
                            if(faBuTieZiEntity != null){
                                faBuTieZiEntity.getResponse().getList().clear();

                                myFaBuAdapter.notifyDataSetChanged();
                            }
                        }
                    }
                });
    }




//    删除的网络请求
    private void initDeleteHttp(int position,String id) {
        OkHttpUtils.post()
                .url(ContractUtils.LOGIN_URL+"forum/delMyForum")
                .addHeader("token",ContractUtils.getTOKEN(getContext()))
                .addParams("id",id)
                .build()
                .execute(new StringCallback() {
                    @Override
                    public void onError(Call call, Exception e, int id) {

                    }

                    @Override
                    public void onResponse(String response, int id) {
                        System.out.println(response+"            删除的网络请求");
                        ContractUtils.Code500(getContext(),response);
                        if(response.indexOf("200") != -1){
                            Gson gson = new Gson();
                            LunTanShanChuEntity lunTanShanChuEntity = gson.fromJson(response, LunTanShanChuEntity.class);
                            Toast.makeText(getContext(), "删除成功", Toast.LENGTH_SHORT).show();

//                          我发布帖子的网络请求
                            initFaBuTieZiHttp();
                        }

                    }
                });
    }




    private void initView() {
//        myFaBuAdapter = new MyFaBuAdapter(getContext());
//        LinearLayoutManager manager = new LinearLayoutManager(getContext());
//        recyclerView.setLayoutManager(manager);
//        recyclerView.setAdapter(myFaBuAdapter);
    }
}
