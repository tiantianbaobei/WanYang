package com.wanyangproject.mysuipian;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.google.gson.Gson;
import com.wanyangproject.R;
import com.wanyangproject.entity.ChuanZhiEntity;
import com.wanyangproject.entity.DaiShouHuoEntity;
import com.wanyangproject.entity.QueRenShouHuoEntity;
import com.wanyangproject.entity.QueRenShouHuoShiBaiEntity;
import com.wanyangproject.fragment.ShouYeFragment;
import com.wanyangproject.my.DaiShouHuoXiangQingActivity;
import com.wanyangproject.my.DaiZhiFuXiangQingActivity;
import com.wanyangproject.my.FinishXiangQingActivity;
import com.wanyangproject.my.TuiKuanActivity;
import com.wanyangproject.myadapter.DaiShouHuoAdapter;
import com.wanyangproject.myadapter.DaiZhiFuAdapter;
import com.wanyangproject.utils.ContractUtils;
import com.zhy.http.okhttp.OkHttpUtils;
import com.zhy.http.okhttp.callback.StringCallback;

import butterknife.BindView;
import butterknife.ButterKnife;
import okhttp3.Call;

/**
 * Created by 甜甜 on 2018/7/30.
 */

public class DaiShouHuoFragment extends Fragment {
    @BindView(R.id.recycerView)
    RecyclerView recycerView;
    private DaiShouHuoAdapter daiShouHuoAdapter;
    private DaiShouHuoEntity daiShouHuoEntity;
    private NetWork netWork;
    private String tuikuan;
    private String shouhuo;
    private QueRenShouHuoEntity queRenShouHuoEntity;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.dingdan_daishouhuo_fragment, container, false);
        ButterKnife.bind(this, view);
        initView();
//        订单列表待收货的网络请求
//        initDaiShouHuoHttp();

        //        获取园区id接收广播
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("tuikuan");
        netWork = new NetWork();
        getActivity().registerReceiver(netWork, intentFilter);


        IntentFilter intentFilter5 = new IntentFilter();
        intentFilter5.addAction("shouhuo");
        netWork = new NetWork();
        getContext().registerReceiver(netWork, intentFilter5);

        return view;
    }



    class NetWork extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            tuikuan = intent.getStringExtra("tuikuan");

            shouhuo = intent.getStringExtra("shouhuo");


            if(shouhuo != null){
                //                订单列表待收货的网络请求
                initDaiShouHuoHttp();
            }


            if(tuikuan != null){
//                订单列表待收货的网络请求
                initDaiShouHuoHttp();
            }
            System.out.println(tuikuan + "    接收的tuikuan");
        }
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        getContext().unregisterReceiver(netWork);
    }


//
//    @Override
//    public void onResume() {
//        super.onResume();
//        //        订单列表待收货的网络请求
//        initDaiShouHuoHttp();
//    }

    //    订单列表待收货的网络请求
    public void initDaiShouHuoHttp() {
        OkHttpUtils.post()
                .url(ContractUtils.LOGIN_URL+"Life/orderlist")
                .addHeader("token",ContractUtils.getTOKEN(getContext()))
                .addParams("parkId",ContractUtils.getParkId(getContext()))
                .addParams("type","2,3,4,5,6,10,11")//订单状态1：未支付2：已支付3：代发货4：已完成5：商家已接单6：商家准备完成7：商家确认送达8：用户确认送达9：已评价10：商家拒绝接单11:用户申请退款12：退款订单完成
                .build()
                .execute(new StringCallback() {
                    @Override
                    public void onError(Call call, Exception e, int id) {

                    }

                    @Override
                    public void onResponse(String response, final int id) {
                        ContractUtils.Code500shibai(getContext(),response);
                        System.out.println(response+"     订单列表待收货的网络请求");

                        if(response.indexOf("200") != -1){
                            Gson gson = new Gson();
                            daiShouHuoEntity = gson.fromJson(response, DaiShouHuoEntity.class);
//                            上面和下面的信息
                            daiShouHuoAdapter = new DaiShouHuoAdapter(getContext(),daiShouHuoEntity.getResponse());
                            LinearLayoutManager manager = new LinearLayoutManager(getContext());
                            recycerView.setLayoutManager(manager);
                            recycerView.setAdapter(daiShouHuoAdapter);

//                           申请退款
                            daiShouHuoAdapter.setDaiShouHuoClick(new DaiShouHuoAdapter.DaiShouHuoClick() {
                                @Override
                                public void daishouhuoClick(int position) {
                                    System.out.println(position+"         xiabaiao");
                                    Intent intent = new Intent(getContext(), TuiKuanActivity.class);
                                    intent.putExtra("dingdanhao",daiShouHuoEntity.getResponse().get(position).getOrder_sn());// 订单号
//                                    ChuanZhiEntity.obj = daiShouHuoEntity.getResponse().get(position);// 传值
                                    startActivity(intent);

                                }
                            });

//                           确认收货
                            daiShouHuoAdapter.setQueRenShouHuoClick(new DaiShouHuoAdapter.QueRenShouHuoClick() {
                                @Override
                                public void querenshouhuoClick(int position, String dingdanhao) {
                                    //           确认收货的网络请求
                                    initQueRenShouHuoHttp(position,dingdanhao);
                                }
                            });


//                            daiShouHuoAdapter.setQueRenShouHuoClick(new DaiShouHuoAdapter.QueRenShouHuoClick() {
//                                @Override
//                                public void querenshouhuoClick(int position) {
////                                    Intent intent = new Intent(getContext(), FinishXiangQingActivity.class);
////                                    startActivity(intent);
//                                    //                                    确认收货的网络请求
////                                    initQueRenShouHuoHttp();
//                                }
//                            });



//                          待收货详情
                            daiShouHuoAdapter.setDaiShouHuoXiangQingClick(new DaiShouHuoAdapter.DaiShouHuoXiangQingClick() {
                                @Override
                                public void daishouhuoXiangQingClick(int position) {
                                    Intent intent = new Intent(getContext(), DaiShouHuoXiangQingActivity.class);
                                    intent.putExtra("dingdanhao",daiShouHuoEntity.getResponse().get(position).getOrder_sn());
                                    startActivity(intent);
                                }
                            });
                        } else if(response.indexOf("400") != -1){
                            Toast.makeText(getContext(), "您还没有待收货订单！", Toast.LENGTH_SHORT).show();
                            if(daiShouHuoEntity != null){
                                daiShouHuoEntity.getResponse().clear();

                                daiShouHuoAdapter.notifyDataSetChanged();
                            }
                        }
                    }
                });
    }



//    确认收货的网络请求
    private void initQueRenShouHuoHttp(int position, String dingdanhao) {
        OkHttpUtils.post()
                .url(ContractUtils.LOGIN_URL+"user/userrefuse")
                .addHeader("token",ContractUtils.getTOKEN(getContext()))
                .addParams("parkId",ContractUtils.getParkId(getContext()))
                .addParams("order_sn",dingdanhao)
                .build()
                .execute(new StringCallback() {
                    @Override
                    public void onError(Call call, Exception e, int id) {

                    }

                    @Override
                    public void onResponse(String response, int id) {
                        ContractUtils.Code500shibai(getContext(),response);
                        System.out.println(response+"          用户确认收货的网络请求");
                        if(response.indexOf("200") != -1){
                            Gson gson = new Gson();
                            queRenShouHuoEntity = gson.fromJson(response, QueRenShouHuoEntity.class);
                            Toast.makeText(getContext(), queRenShouHuoEntity.getResponse().getMes(), Toast.LENGTH_SHORT).show();
//                            Intent intent = new Intent(getContext(), FinishXiangQingActivity.class);
//                            startActivity(intent);
                            //        订单列表待收货的网络请求
                            initDaiShouHuoHttp();
                        }else if(response.indexOf("400") != -1){
                            Gson gson = new Gson();
                            QueRenShouHuoShiBaiEntity queRenShouHuoShiBaiEntity = gson.fromJson(response, QueRenShouHuoShiBaiEntity.class);
                            Toast.makeText(getContext(), queRenShouHuoShiBaiEntity.getMsg(), Toast.LENGTH_SHORT).show();
                        }
                    }
                });
    }

    private void initView() {
//        daiShouHuoAdapter = new DaiShouHuoAdapter(getContext());
//        LinearLayoutManager manager = new LinearLayoutManager(getContext());
//        recycerView.setLayoutManager(manager);
//        recycerView.setAdapter(daiShouHuoAdapter);
//
////        申请退款
//        daiShouHuoAdapter.setDaiShouHuoClick(new DaiShouHuoAdapter.DaiShouHuoClick() {
//            @Override
//            public void daishouhuoClick(int position) {
//                Intent intent = new Intent(getContext(), TuiKuanActivity.class);
//                startActivity(intent);
//            }
//        });
//
////        确认收货
//        daiShouHuoAdapter.setQueRenShouHuoClick(new DaiShouHuoAdapter.QueRenShouHuoClick() {
//            @Override
//            public void querenshouhuoClick(int position) {
//                Intent intent = new Intent(getContext(), FinishXiangQingActivity.class);
//                startActivity(intent);
//            }
//        });
//
//
//
////        待收货详情
//        daiShouHuoAdapter.setDaiShouHuoXiangQingClick(new DaiShouHuoAdapter.DaiShouHuoXiangQingClick() {
//            @Override
//            public void daishouhuoXiangQingClick(int position) {
//                Intent intent = new Intent(getContext(), DaiShouHuoXiangQingActivity.class);
//                startActivity(intent);
//            }
//        });


    }

}
