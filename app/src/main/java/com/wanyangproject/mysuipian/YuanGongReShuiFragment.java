package com.wanyangproject.mysuipian;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.wanyangproject.R;
import com.wanyangproject.myadapter.YuanGongReShuiAdapter;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by 甜甜 on 2018/8/1.
 */

public class YuanGongReShuiFragment extends Fragment {
    @BindView(R.id.recycerView)
    RecyclerView recycerView;
    private YuanGongReShuiAdapter yuanGongReShuiAdapter;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.dingdan_daishouhuo_fragment, container, false);
        ButterKnife.bind(this, view);
        initView();
        return view;
    }

    private void initView() {
        yuanGongReShuiAdapter = new YuanGongReShuiAdapter(getContext());
        LinearLayoutManager manager = new LinearLayoutManager(getContext());
        recycerView.setLayoutManager(manager);
        recycerView.setAdapter(yuanGongReShuiAdapter);
    }

}
