package com.wanyangproject.mysuipian;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.google.gson.Gson;
import com.wanyangproject.R;
import com.wanyangproject.activity.TieZiXiangQingActivity;
import com.wanyangproject.entity.LunTanShanChuEntity;
import com.wanyangproject.entity.MyLunTanPingLunEntity;
import com.wanyangproject.myadapter.MyPingLunAdapter;
import com.wanyangproject.utils.ContractUtils;
import com.zhy.http.okhttp.OkHttpUtils;
import com.zhy.http.okhttp.callback.StringCallback;

import butterknife.BindView;
import butterknife.ButterKnife;
import okhttp3.Call;

/**
 * Created by 甜甜 on 2018/7/31.
 */

public class MyPingLunFragment extends Fragment {

    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    private MyPingLunAdapter myPingLunAdapter;
    private MyLunTanPingLunEntity myLunTanPingLunEntity;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.my_pinglun_fragment, container, false);
        ButterKnife.bind(this, view);
        initView();

//        我的论坛我的评论的网络请求
        initPingLunHttp();


        return view;
    }


    @Override
    public void onResume() {
        super.onResume();
        //        我的论坛我的评论的网络请求
        initPingLunHttp();

    }

    //    我的论坛评论的网络请求
    private void initPingLunHttp() {
        OkHttpUtils.post()
                .url(ContractUtils.LOGIN_URL+"forum/myReplyForum")
                .addHeader("token",ContractUtils.getTOKEN(getContext()))
                .build()
                .execute(new StringCallback() {
                    @Override
                    public void onError(Call call, Exception e, int id) {

                    }

                    @Override
                    public void onResponse(String response, int id) {
                        ContractUtils.Code500(getContext(),response);
                        System.out.println(response+"           我的论坛我的评论的网络请求");
                        if(response.indexOf("200") != -1){
                            Gson gson = new Gson();
                            myLunTanPingLunEntity = gson.fromJson(response, MyLunTanPingLunEntity.class);
                            myPingLunAdapter = new MyPingLunAdapter(getContext(),myLunTanPingLunEntity.getResponse());
                            LinearLayoutManager manager = new LinearLayoutManager(getContext());
                            recyclerView.setLayoutManager(manager);
                            recyclerView.setAdapter(myPingLunAdapter);


                            myPingLunAdapter.setShanChuClick(new MyPingLunAdapter.ShanChuClick() {
                                @Override
                                public void shanchuClick(int position, String id) {
//                                    我的论坛删除评论的网络请求
                                    initShanChuClickHttp(position,id);
                                }
                            });


//                            详情
                            myPingLunAdapter.setXiangQingClick(new MyPingLunAdapter.XiangQingClick() {
                                @Override
                                public void xiangqingClick(int position, String id) {
                                    //                                    点击进入帖子详情
                                    Intent intent = new Intent(getContext(), TieZiXiangQingActivity.class);
                                    intent.putExtra("ispc",myLunTanPingLunEntity.getResponse().get(position).getIsPc());
                                    intent.putExtra("tieziid",id);
                                    System.out.println(id+"     发送帖子的id");
                                    startActivity(intent);
                                }
                            });


                        }else if(response.indexOf("400") != -1){
                            if(myLunTanPingLunEntity != null){
                                myLunTanPingLunEntity.getResponse().clear();

                                myPingLunAdapter.notifyDataSetChanged();
                            }
                        }
                    }
                });
    }




//    我的论坛删除评论的网络请求
    private void initShanChuClickHttp(int position, String id) {
        OkHttpUtils.post()
                .url(ContractUtils.LOGIN_URL+"forum/delMyForumReply")
                .addHeader("token",ContractUtils.getTOKEN(getContext()))
                .addParams("id",id)
                .build()
                .execute(new StringCallback() {
                    @Override
                    public void onError(Call call, Exception e, int id) {

                    }

                    @Override
                    public void onResponse(String response, int id) {
                        System.out.println(response+"           我的论坛删除评论的网络请求 ");
                        ContractUtils.Code500(getContext(),response);
                        if(response.indexOf("200") != -1){
                            Gson gson = new Gson();
                            LunTanShanChuEntity lunTanShanChuEntity = gson.fromJson(response, LunTanShanChuEntity.class);
                            Toast.makeText(getContext(), "删除成功", Toast.LENGTH_SHORT).show();
                            //        我的论坛我的评论的网络请求
                            initPingLunHttp();

                        }
                    }
                });
    }




    private void initView() {
//        myPingLunAdapter = new MyPingLunAdapter(getContext());
//        LinearLayoutManager manager = new LinearLayoutManager(getContext());
//        recyclerView.setLayoutManager(manager);
//        recyclerView.setAdapter(myPingLunAdapter);
    }

}
