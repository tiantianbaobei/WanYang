package com.wanyangproject.mysuipian;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.gson.Gson;
import com.wanyangproject.R;
import com.wanyangproject.entity.QiYeShuiDianEntity;
import com.wanyangproject.entity.QiYeShuiDianJiLuEntity;
import com.wanyangproject.myadapter.QiYeShuiAdapter;
import com.wanyangproject.myadapter.ShuiAdapter;
import com.wanyangproject.utils.ContractUtils;
import com.zhy.http.okhttp.OkHttpUtils;
import com.zhy.http.okhttp.callback.StringCallback;

import org.apache.http.params.HttpConnectionParams;

import java.util.concurrent.TimeUnit;

import butterknife.BindView;
import butterknife.ButterKnife;
import okhttp3.Call;
import okhttp3.OkHttpClient;

/**
 * Created by 甜甜 on 2018/7/31.
 */

public class MyQiYeShuiJiLuFragment extends Fragment {
    @BindView(R.id.reyclerView)
    RecyclerView reyclerView;
    private QiYeShuiAdapter qiYeShuiAdapter;



    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.shui_fragment, container, false);
        ButterKnife.bind(this, view);
        initView();

////        企业用水记录的网络请求
        initYongShuiHttp();

        return view;
    }



//    企业用水记录的网络请求
    private void initYongShuiHttp() {
        final ProgressDialog progressDialog = new ProgressDialog(getContext());
        progressDialog.setTitle("提示");
        progressDialog.setMessage("请等待...");
        progressDialog.setCancelable(false);
        progressDialog.show();



        OkHttpUtils.post()
                .url(ContractUtils.LOGIN_URL+"company/Enterprisewd")
                .addHeader("token",ContractUtils.getTOKEN(getContext()))
                .addParams("type","1")//1代表水 2代表电
                .addParams("parkId",ContractUtils.getParkId(getContext()))
//                .addParams("page","0")
                .build()
                .connTimeOut(20000)
                .readTimeOut(20000)
                .writeTimeOut(20000)
                .execute(new StringCallback() {
                    @Override
                    public void onError(Call call, Exception e, int id) {
                        System.out.println(e+"   eee11111111企业用水记录");
                        progressDialog.dismiss();
                    }

                    @Override
                    public void onResponse(String response, int id) {
                        System.out.println(response+"        111111111企业用水记录的网络请求 ");
                        ContractUtils.Code500(getContext(),response);
                        progressDialog.dismiss();
                        if(response.indexOf("200") != -1){
                            Gson gson = new Gson();
                            QiYeShuiDianEntity qiYeShuiDianEntity = gson.fromJson(response, QiYeShuiDianEntity.class);
                            if(qiYeShuiDianEntity.getResponse()!= null) {
                                qiYeShuiAdapter = new QiYeShuiAdapter(getContext(), qiYeShuiDianEntity.getResponse());
                                LinearLayoutManager manager = new LinearLayoutManager(getContext());
                                reyclerView.setLayoutManager(manager);
                                reyclerView.setAdapter(qiYeShuiAdapter);
                            }
                        }else if(response.indexOf("400")  != -1){
//                            ContractUtils.Code400(getContext(),response);
                        }
                    }
                });
    }




    private void initView() {
//        shuiAdapter = new ShuiAdapter(getContext());
//        LinearLayoutManager manager = new LinearLayoutManager(getContext());
//        reyclerView.setLayoutManager(manager);
//        reyclerView.setAdapter(shuiAdapter);
    }
}
