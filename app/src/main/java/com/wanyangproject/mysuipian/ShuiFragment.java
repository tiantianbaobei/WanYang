package com.wanyangproject.mysuipian;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bigkoo.pickerview.OptionsPickerView;
import com.google.gson.Gson;
import com.wanyangproject.R;
import com.wanyangproject.activity.YiKaTongZhiFuActivity;
import com.wanyangproject.entity.MenWeiShuiDianXinXiEntity;
import com.wanyangproject.entity.MyQiYeChangFangXiangQingEntity;
import com.wanyangproject.entity.QiYeSuSheXiangQingEntity;
import com.wanyangproject.entity.ShuiFeiJiLuEntity;
import com.wanyangproject.entity.YiKaTongYuEEntity;
import com.wanyangproject.my.MySuSheActivity;
import com.wanyangproject.my.YiKaTongActivity;
import com.wanyangproject.popuwindow.ReShuiLengShuiPopupWindow;
import com.wanyangproject.utils.ContractUtils;
import com.zhy.http.okhttp.OkHttpUtils;
import com.zhy.http.okhttp.callback.StringCallback;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.Call;

/**
 * Created by 甜甜 on 2018/8/1.
 */

public class ShuiFragment extends Fragment {
    @BindView(R.id.tv_shuifei_zhanghao)
    TextView tvShuifeiZhanghao;
    @BindView(R.id.tv_money)
    TextView tvMoney;
    @BindView(R.id.btn_wushi)
    Button btnWushi;
    @BindView(R.id.btn_yibai)
    Button btnYibai;
    @BindView(R.id.btn_erbai)
    Button btnErbai;
    @BindView(R.id.btn_queren)
    Button btnQueren;
    @BindView(R.id.et_jine)
    EditText etJine;


    @BindView(R.id.linear_shui)
    LinearLayout linearShui;
    @BindView(R.id.relative_chongzhi_leixing)
    RelativeLayout relativeChongzhiLeixing;
    @BindView(R.id.tv_xuanze)
    TextView tvXuanze;
    private ReShuiLengShuiPopupWindow reShuiLengShuiPopupWindow;
    private ArrayList<String> listtitle = new ArrayList<>();
    private ArrayList<String> listid= new ArrayList<>();
    private ArrayList<String> listname= new ArrayList<>();
    private String money = "0";
    private String water = "2";//1：冷水2：热水
    private String lxid;
    private String lxname;
    private NetWork netWork;
    private String chongzhichenggong;
    private QiYeSuSheXiangQingEntity qiYeSuSheXiangQingEntity;
    private MyQiYeChangFangXiangQingEntity myQiYeChangFangXiangQingEntity;
    private String yuesushe;


    private String yue;
    private String waterUserName;
    private String shuiid;
    private String yue2;
    private String waterUserName2;
    private String shuiid2;




    public String getShuiid() {
        return shuiid;
    }

    public void setShuiid(String shuiid) {
        this.shuiid = shuiid;
    }

    public String getShuiid2() {
        return shuiid2;
    }

    public void setShuiid2(String shuiid2) {
        this.shuiid2 = shuiid2;
    }

    public TextView getTvShuifeiZhanghao() {
        return tvShuifeiZhanghao;
    }

    public void setTvShuifeiZhanghao(TextView tvShuifeiZhanghao) {
        this.tvShuifeiZhanghao = tvShuifeiZhanghao;
    }

    public String getYue() {
        return yue;
    }

    public void setYue(String yue) {
        this.yue = yue;
    }

    public String getWaterUserName() {
        return waterUserName;
    }

    public void setWaterUserName(String waterUserName) {
        this.waterUserName = waterUserName;
    }


    public String getYue2() {
        return yue2;
    }

    public void setYue2(String yue2) {
        this.yue2 = yue2;
    }

    public String getWaterUserName2() {
        return waterUserName2;
    }

    public void setWaterUserName2(String waterUserName2) {
        this.waterUserName2 = waterUserName2;
    }



    //    public MyQiYeChangFangXiangQingEntity getMyQiYeChangFangXiangQingEntity() {
//        return myQiYeChangFangXiangQingEntity;
//    }
//
//    public void setMyQiYeChangFangXiangQingEntity(MyQiYeChangFangXiangQingEntity myQiYeChangFangXiangQingEntity) {
//        this.myQiYeChangFangXiangQingEntity = myQiYeChangFangXiangQingEntity;
//    }
//
//    public QiYeSuSheXiangQingEntity getQiYeSuSheXiangQingEntity() {
//        return qiYeSuSheXiangQingEntity;
//    }
//
//    public void setQiYeSuSheXiangQingEntity(QiYeSuSheXiangQingEntity qiYeSuSheXiangQingEntity) {
//        this.qiYeSuSheXiangQingEntity = qiYeSuSheXiangQingEntity;
//    }

    public MenWeiShuiDianXinXiEntity weiShuiDianXinXiEntity;

    public MenWeiShuiDianXinXiEntity getWeiShuiDianXinXiEntity() {
        return weiShuiDianXinXiEntity;
    }

    public void setWeiShuiDianXinXiEntity(MenWeiShuiDianXinXiEntity weiShuiDianXinXiEntity) {
        this.weiShuiDianXinXiEntity = weiShuiDianXinXiEntity;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.shuifei_fragment, container, false);
        ButterKnife.bind(this, view);



            if(waterUserName != null){
                tvShuifeiZhanghao.setText("水表用户:"+waterUserName);
            }


             if(waterUserName != null){
                tvXuanze.setText(waterUserName);
             }



            if(yue != null){
                tvMoney.setText("¥"+yue);
            }


            if(shuiid != null){
                lxid = shuiid;
            }

            if(weiShuiDianXinXiEntity != null){
                lxid = weiShuiDianXinXiEntity.getResponse().getData().get(0).getWaterUserId();
            }


//        if(waterUserName2 != null){
//            tvShuifeiZhanghao.setText("水表用户:"+waterUserName2);
//        }
//
//        if(yue2 != null){
//            tvMoney.setText("¥"+yue2);
//        }
//
//


        Intent intent = new Intent();
        intent.putExtra("qiyeshuigid", lxid);
        intent.setAction("qiyeshuigid");
        getActivity().sendBroadcast(intent);

        Intent intent1 = new Intent();
        intent1.putExtra("shuileixingid", lxid);
        intent1.setAction("shuileixingid");
        getActivity().sendBroadcast(intent1);

        if(waterUserName != null){
            listtitle.add(waterUserName);
        }
        if(shuiid != null){
            listid.add(shuiid);
        }

        if(waterUserName2 != null){
            listtitle.add(waterUserName2);
        }

        if(shuiid2 != null){
            listid.add(shuiid2);
        }





       //        一卡通余额的网络请求
//        initYiKaTongYuEHttp();

        System.out.println(weiShuiDianXinXiEntity+"       weiShuiDianXinXiEntity");

//       员工的水电充值
        if(weiShuiDianXinXiEntity != null){
            if (weiShuiDianXinXiEntity.getResponse().getData() != null) {
                if (weiShuiDianXinXiEntity.getResponse().getData().size() >= 1) {
//                    listtitle.add("水表1");
                    listtitle.add(weiShuiDianXinXiEntity.getResponse().getData().get(0).getWaterUserName());
                    listid.add(weiShuiDianXinXiEntity.getResponse().getData().get(0).getWaterUserId());
                    tvShuifeiZhanghao.setText("水表用户:"+weiShuiDianXinXiEntity.getResponse().getData().get(0).getWaterUserName());

                    tvMoney.setText("¥"+weiShuiDianXinXiEntity.getResponse().getData().get(0).getBalance());
                }
            }
        }



        if(weiShuiDianXinXiEntity != null){
            if (weiShuiDianXinXiEntity.getResponse().getData() != null) {
                if (weiShuiDianXinXiEntity.getResponse().getData().size() >= 2) {
//                    listtitle.add("水表2");
                    listtitle.add(weiShuiDianXinXiEntity.getResponse().getData().get(1).getWaterUserName());
                    listid.add(weiShuiDianXinXiEntity.getResponse().getData().get(1).getWaterUserId());
//                    tvShuifeiZhanghao.setText("水表用户:"+listtitle.get(1));
//                    tvMoney.setText("¥"+weiShuiDianXinXiEntity.getResponse().getData().get(1).getBalance());
                }
            }
        }

//        选择类型
        if(weiShuiDianXinXiEntity != null){
            if (weiShuiDianXinXiEntity.getResponse().getData() != null) {
                if (weiShuiDianXinXiEntity.getResponse().getData().size() >= 1) {
                    tvXuanze.setText(weiShuiDianXinXiEntity.getResponse().getData().get(0).getWaterUserName());
                }
            }
        }



        IntentFilter intentFilter4 = new IntentFilter();
        intentFilter4.addAction("chongzhichenggong");
        netWork = new NetWork();
        getContext().registerReceiver(netWork, intentFilter4);

//
//        IntentFilter intentFilter5 = new IntentFilter();
//        intentFilter5.addAction("yue");
//        netWork = new NetWork();
//        getContext().registerReceiver(netWork, intentFilter4);
//

        IntentFilter intentFilter6 = new IntentFilter();
        intentFilter6.addAction("yuesushe");
        netWork = new NetWork();
        getContext().registerReceiver(netWork, intentFilter4);


        return view;
    }






    @Override
    public void onDestroy() {
        super.onDestroy();
        getActivity().unregisterReceiver(netWork);
    }


    class NetWork extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {


            //            结束页面
            chongzhichenggong = intent.getStringExtra("chongzhichenggong");
            if(chongzhichenggong != null){
                getActivity().finish();
            }
        }
    }






//    //    一卡通余额的网络请求
//    private void initYiKaTongYuEHttp() {
//        OkHttpUtils.post()
//                .url(ContractUtils.LOGIN_URL + "user/getCardInfo")
//                .addHeader("token", ContractUtils.getTOKEN(getContext()))
//                .build()
//                .execute(new StringCallback() {
//                    @Override
//                    public void onError(Call call, Exception e, int id) {
//                        System.out.println(e + "   eeee一卡通余额");
//                    }
//
//                    @Override
//                    public void onResponse(String response, int id) {
//                        ContractUtils.Code500(getContext(), response);
//                        System.out.println(response + "            一卡通余额的网络请求");
//                        if (response.indexOf("200") != -1) {
//                            Gson gson = new Gson();
//                            YiKaTongYuEEntity yiKaTongYuEEntity = gson.fromJson(response, YiKaTongYuEEntity.class);
//                            if (yiKaTongYuEEntity.getResponse().getBalance() == null) {
//
//                            } else {
//                                tvMoney.setText("¥" + yiKaTongYuEEntity.getResponse().getBalance());
//                            }
//
////                            if (yiKaTongYuEEntity.getResponse().getCardNumber() == null) {
////
////                            } else {
////                                tvShuifeiZhanghao.setText("水费账号: " + yiKaTongYuEEntity.getResponse().getCardNumber());
////                            }
//                        }
//                    }
//                });
//    }





    @OnClick({R.id.tv_shuifei_zhanghao, R.id.tv_money, R.id.btn_wushi, R.id.btn_yibai, R.id.btn_erbai, R.id.btn_queren, R.id.tv_xuanze, R.id.relative_chongzhi_leixing})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.tv_shuifei_zhanghao:
                break;
            case R.id.tv_money:
                break;
//            五十
            case R.id.btn_wushi:
                money = "50";
                setFastRecharge(btnWushi, true);
                setFastRecharge(btnYibai, false);
                setFastRecharge(btnErbai, false);
                etJine.setText(money);
                break;
//            一百
            case R.id.btn_yibai:
                money = "100";
                setFastRecharge(btnWushi, false);
                setFastRecharge(btnYibai, true);
                setFastRecharge(btnErbai, false);
                etJine.setText(money);
                break;
//            二百
            case R.id.btn_erbai:
                money = "200";
                setFastRecharge(btnWushi, false);
                setFastRecharge(btnYibai, false);
                setFastRecharge(btnErbai, true);
                etJine.setText(money);
                break;
//            确认
            case R.id.btn_queren:
                if(tvXuanze.getText().toString().trim().equals("")){
                    Toast.makeText(getContext(), "请选择充值类型", Toast.LENGTH_SHORT).show();
                    return;
                }

                if(tvXuanze.getText().toString().equals("热水")){//1：冷水2：热水
                    water = "2";
                }else if(tvXuanze.getText().toString().equals("冷水")){//1：冷水2：热水
                    water = "1";
                }

                if (etJine.getText().toString().trim().equals("") && money.equals("0")) {
                    Toast.makeText(getContext(), "请选择或输入充值金额", Toast.LENGTH_SHORT).show();
                    return;
                }
                if(etJine.getText().toString().trim().length() > 0){
                    money = etJine.getText().toString().trim();
                }


                if(lxid == null){
                    Toast.makeText(getContext(), "暂无水表信息，无法充值！", Toast.LENGTH_SHORT).show();
                    return;
                }

                System.out.println(lxid+"   lxidlxidlxid ");

                Intent intent1 = new Intent(getContext(), YiKaTongZhiFuActivity.class);
                intent1.putExtra("money", money);
                intent1.putExtra("state","2");//1：一卡通2：水费3：电费
                intent1.putExtra("water",water);
                intent1.putExtra("id",lxid);
                intent1.putExtra("name",tvXuanze.getText().toString().trim());
                startActivity(intent1);
//                getActivity().finish();
                break;
//            选择
            case R.id.tv_xuanze:
            case R.id.relative_chongzhi_leixing:
//                if(listid == null || lxname == null){
//                    return;
//                }

                if(listtitle.size() == 0){
                    Toast.makeText(getContext(), "暂无数据！", Toast.LENGTH_SHORT).show();
                    return;
                }

//
//                if(listtitle.size() == 0 || listid.size() == 0){
//                    Toast.makeText(getContext(), "暂无数据！", Toast.LENGTH_SHORT).show();
//                    return;
//                }


                OptionsPickerView pvOptions = new  OptionsPickerView.Builder(getContext(), new OptionsPickerView.OnOptionsSelectListener() {
                    @Override
                    public void onOptionsSelect(int options1, int option2, int options3 ,View v) {
                        //返回的分别是三个级别的选中位置
                        String tx = listtitle.get(options1);
                        lxid = listid.get(options1);

                        tvXuanze.setText(tx);


                        Intent intent = new Intent();
                        intent.putExtra("qiyeshuigid", lxid);
                        intent.setAction("qiyeshuigid");
                        getActivity().sendBroadcast(intent);

                        Intent intent1 = new Intent();
                        intent1.putExtra("shuileixingid", lxid);
                        intent1.setAction("shuileixingid");
                        getActivity().sendBroadcast(intent1);

                        if(options1 == 0){
                            tvShuifeiZhanghao.setText("水表用户:"+waterUserName);
                            tvMoney.setText("¥"+yue);

                        }else if(options1 == 1){
                            tvShuifeiZhanghao.setText("水表用户:"+waterUserName2);
                            tvMoney.setText("¥"+yue2);

                        }

//                        员工
                        if(weiShuiDianXinXiEntity != null){
                            if(options1 == 0){
                                if(weiShuiDianXinXiEntity != null){
                                    if (weiShuiDianXinXiEntity.getResponse().getData() != null) {
                                        if (weiShuiDianXinXiEntity.getResponse().getData().size() >= 1) {
                                            listid.add(weiShuiDianXinXiEntity.getResponse().getData().get(0).getWaterUserId());
                                            tvShuifeiZhanghao.setText("水表用户:"+listtitle.get(0));
                                            tvMoney.setText("¥"+weiShuiDianXinXiEntity.getResponse().getData().get(0).getBalance());
                                        }
                                    }
                                }
                            }else if(options1 == 1){
                                if(weiShuiDianXinXiEntity != null){
                                    if (weiShuiDianXinXiEntity.getResponse().getData() != null) {
                                        if (weiShuiDianXinXiEntity.getResponse().getData().size() >= 2) {
                                            listid.add(weiShuiDianXinXiEntity.getResponse().getData().get(1).getWaterUserId());
                                            tvShuifeiZhanghao.setText("水表用户:"+listtitle.get(1));
                                            tvMoney.setText("¥"+weiShuiDianXinXiEntity.getResponse().getData().get(1).getBalance());
                                        }
                                    }
                                }
                            }
                        }


                    }
                })
                        .setSubmitText("确定")//确定按钮文字
                        .setCancelText("取消")//取消按钮文字
                        .setTitleText("申请类型")//标题
                        .setSubCalSize(14)//确定和取消文字大小
                        .setTitleSize(16)//标题文字大小
                        .setTitleColor(Color.BLACK)//标题文字颜色
                        .setSubmitColor(Color.RED)//确定按钮文字颜色
                        .setLineSpacingMultiplier(3.0f)//设置两横线之间的间隔倍数
                        .setCancelColor(Color.parseColor("#AEAAAA"))//取消按钮文字颜色
                        .setTitleBgColor(Color.parseColor("#f5f5f5"))//标题背景颜色 Night mode
                        .setBgColor(Color.parseColor("#ffffff"))//滚轮背景颜色 Night mode
                        .setContentTextSize(16)//滚轮文字大小
                        .setLinkage(false)//设置是否联动，默认true
                        .setLabels("", "", "")//设置选择的三级单位
                        .isCenterLabel(false) //是否只显示中间选中项的label文字，false则每项item全部都带有label。
                        .setCyclic(false, false, false)//循环与否
                        .setSelectOptions(1, 1, 1)  //设置默认选中项
                        .setOutSideCancelable(false)//点击外部dismiss default true
                        .isDialog(false)//是否显示为对话框样式
                        .build();

                pvOptions.setPicker(listtitle);//添加数据源
                pvOptions.show();


//                Object itemsOnClick = null;
//                reShuiLengShuiPopupWindow = new ReShuiLengShuiPopupWindow(getContext(), (View.OnClickListener) itemsOnClick);
//                reShuiLengShuiPopupWindow.showAtLocation(linearShui, Gravity.BOTTOM | Gravity.CENTER_HORIZONTAL, 0, 0);
//
//                reShuiLengShuiPopupWindow.setShuiClick(new ReShuiLengShuiPopupWindow.ShuiClick() {
//                    @Override
//                    public void shuiclick(String state) {
//                        tvXuanze.setText(state);
//                    }
//                });
                break;
        }
    }


    private void setFastRecharge(Button button, boolean isChecked) {
        if (isChecked) {
            button.setTextColor(getResources().getColor(R.color.white));
            button.setBackgroundResource(R.drawable.shape_red_bg);
        } else {
            button.setTextColor(getResources().getColor(R.color.red));
            button.setBackgroundResource(R.drawable.shape_red_border);
        }
    }
}
