package com.wanyangproject.mysuipian;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.gson.Gson;
import com.wanyangproject.R;
import com.wanyangproject.entity.MenWeiShuiDianXinXiEntity;
import com.wanyangproject.entity.QiYeShuiDianJiLuEntity;
import com.wanyangproject.myadapter.DianAdapter;
import com.wanyangproject.utils.ContractUtils;
import com.zhy.http.okhttp.OkHttpUtils;
import com.zhy.http.okhttp.callback.StringCallback;

import butterknife.BindView;
import butterknife.ButterKnife;
import okhttp3.Call;

/**
 * Created by 甜甜 on 2018/7/31.
 */

public class MyDianFragment extends Fragment {
    @BindView(R.id.reyclerView)
    RecyclerView reyclerView;
    private DianAdapter dianAdapter;
    public MenWeiShuiDianXinXiEntity weiShuiDianXinXiEntity;
    private String electricityUserId;


    public MenWeiShuiDianXinXiEntity getWeiShuiDianXinXiEntity() {
        return weiShuiDianXinXiEntity;
    }

    public void setWeiShuiDianXinXiEntity(MenWeiShuiDianXinXiEntity weiShuiDianXinXiEntity) {
        this.weiShuiDianXinXiEntity = weiShuiDianXinXiEntity;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.dian_fragment, container, false);
        ButterKnife.bind(this, view);
        initView();
//
//
////        企业管理用电记录的网络请求
        initDianJiLuHttp();

        return view;
    }



//    yuqngng用电记录的网络请求
    private void initDianJiLuHttp() {
        if(weiShuiDianXinXiEntity != null){
            if(weiShuiDianXinXiEntity.getResponse() != null){
                if(weiShuiDianXinXiEntity.getResponse().getDian() != null){
                    if(weiShuiDianXinXiEntity.getResponse().getDian().getElectricityUserId() != null){
                        electricityUserId = weiShuiDianXinXiEntity.getResponse().getDian().getElectricityUserId();
                    }
                }else{
                    return;
                }
            }else{
                return;
            }
        }else {
            return;
        }
        OkHttpUtils.post()
//                .url(ContractUtils.LOGIN_URL+"company/getWaterEle111")
                .url(ContractUtils.LOGIN_URL+"company/hydropower")
                .addHeader("token",ContractUtils.getTOKEN(getContext()))
                .addParams("type","2")//1代表水 2代表电
                .addParams("parkId",ContractUtils.getParkId(getContext()))
                .addParams("electricityUserId",electricityUserId)
                .addParams("page","0")
                .build()
                .execute(new StringCallback() {
                    @Override
                    public void onError(Call call, Exception e, int id) {

                    }

                    @Override
                    public void onResponse(String response, int id) {
                        System.out.println(response+"              企业管理用电记录的网络请求");
                        ContractUtils.Code500(getContext(),response);
                        if(response.indexOf("list") != -1){
                            Gson gson = new Gson();
                            QiYeShuiDianJiLuEntity qiYeShuiDianJiLuEntity = gson.fromJson(response, QiYeShuiDianJiLuEntity.class);
                            if(qiYeShuiDianJiLuEntity.getResponse().getData().getList() != null){
                                dianAdapter = new DianAdapter(getContext(),qiYeShuiDianJiLuEntity.getResponse().getData().getList());
                                LinearLayoutManager manager = new LinearLayoutManager(getContext());
                                reyclerView.setLayoutManager(manager);
                                reyclerView.setAdapter(dianAdapter);
                            }

                        }else if(response.indexOf("400") != -1){
                            ContractUtils.Code400(getContext(),response);
                        }
                    }
                });
    }








    private void initView() {
//        dianAdapter = new DianAdapter(getContext());
//        LinearLayoutManager manager = new LinearLayoutManager(getContext());
//        reyclerView.setLayoutManager(manager);
//        reyclerView.setAdapter(dianAdapter);
    }

}
