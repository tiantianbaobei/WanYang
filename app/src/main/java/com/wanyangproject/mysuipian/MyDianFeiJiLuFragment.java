package com.wanyangproject.mysuipian;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.google.gson.Gson;
import com.scwang.smartrefresh.layout.SmartRefreshLayout;
import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.scwang.smartrefresh.layout.listener.OnLoadmoreListener;
import com.scwang.smartrefresh.layout.listener.OnRefreshListener;
import com.wanyangproject.R;
import com.wanyangproject.entity.ShuiFeiJiLuEntity;
import com.wanyangproject.my.ChongZhJiLuActivity;
import com.wanyangproject.myadapter.DianFeiAdapter;
import com.wanyangproject.utils.ContractUtils;
import com.zhy.http.okhttp.OkHttpUtils;
import com.zhy.http.okhttp.callback.StringCallback;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import okhttp3.Call;

/**
 * Created by 甜甜 on 2018/8/1.
 */

public class MyDianFeiJiLuFragment extends Fragment implements OnLoadmoreListener {
    @BindView(R.id.recycerView)
    RecyclerView recycerView;
    private DianFeiAdapter dianFeiAdapter;
//    private JSONArray list;
    @BindView(R.id.smart)
    SmartRefreshLayout smart;
    private int page = 0;
    private ArrayList<JSONObject> list = new ArrayList();//总
    private ArrayList<JSONObject>  shanxuaihou = new ArrayList();//筛选之后的
    private String time;
    private NetWork netWork;
    private String timejilu;
    private String yudian;
    private String yuangongdian;

    public String getYuangongdian() {
        return yuangongdian;
    }

    public void setYuangongdian(String yuangongdian) {
        this.yuangongdian = yuangongdian;
    }

    public String getYudian() {
        return yudian;
    }

    public void setYudian(String yudian) {
        this.yudian = yudian;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.shuidian_jilu_fragment, container, false);
        ButterKnife.bind(this, view);
        initView();

        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("timejilu");
        netWork = new NetWork();
        getContext().registerReceiver(netWork, intentFilter);

        //                下拉刷新
//        smart.setOnRefreshListener(this);
//        上拉加载
        smart.setOnLoadmoreListener(this);



        if(yudian != null){
            //        电费充值记录的网络请求
            initDianFeiHttp();
        }


        if(yuangongdian != null){
            //        电费充值记录的网络请求
            initDianFeiHttpYuanGong2();
        }



        return view;
    }



    class NetWork extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            timejilu = intent.getStringExtra("timejilu");
            time = intent.getStringExtra("time");
//            Toast.makeText(context, wxopenid + wxnickname + wxheadimgurl, Toast.LENGTH_SHORT).show();
            System.out.println(timejilu + "        timejilu");

            shanxuaihou.clear();
            for (int i = 0; i < list.size(); i++) {
                JSONObject jsonObject2 = list.get(i);
                String time1 = null;
                try {
                    time1 = jsonObject2.getString("time");
                    if (time1.indexOf(time) != -1){
                        shanxuaihou.add(jsonObject2);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            dianFeiAdapter.notifyDataSetChanged();
        }
    }


    //    电费充值记录的网络请求
    private void initDianFeiHttpYuanGong2() {
        OkHttpUtils.post()
                .url(ContractUtils.LOGIN_URL+"user/record")
                .addHeader("token",ContractUtils.getTOKEN(getContext()))
                .addParams("parkId",ContractUtils.getParkId(getContext()))
                .addParams("page",page+"")
                .addParams("id",yuangongdian)
                .addParams("type","2")//1：水费充值记录 2：电表充值记录 3：一卡通
                .build()
                .execute(new StringCallback() {
                    @Override
                    public void onError(Call call, Exception e, int id) {

                    }

                    @Override
                    public void onResponse(String response, int id) {
                        System.out.println(response+"          dianfei充值记录水费的网络请求");
                        ContractUtils.Code500(getContext(),response);
                        if(response.indexOf("200") != -1){
                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                JSONObject jsonObject1 = jsonObject.getJSONObject("response");
                                JSONObject data = jsonObject1.getJSONObject("data");
                                if(data != null){
                                    JSONArray jsonArray = data.getJSONArray("list");

                                    for (int i = 0; i < jsonArray.length(); i++) {
                                        JSONObject object = (JSONObject) jsonArray.get(i);
                                        if(object != null){
                                            list.add(object);
                                        }
                                    }
                                }

                                if(time != null){
                                    shanxuaihou.clear();
                                    for (int i = 0; i < list.size(); i++) {
                                        JSONObject jsonObject2 = list.get(i);
                                        String time1 = jsonObject2.getString("time");
                                        if (time1.indexOf(time) != -1){
                                            shanxuaihou.add(jsonObject2);
                                        }
                                    }

//                                    chongZhiAdapter.notifyDataSetChanged();
                                }else{
                                    shanxuaihou.clear();
                                    shanxuaihou.addAll(list);
                                }



                                if(list == null){
                                    Toast.makeText(getContext(), "暂无数据", Toast.LENGTH_SHORT).show();
                                    return;
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

//                            Gson gson = new Gson();
//                            ShuiFeiJiLuEntity shuiFeiJiLuEntity = gson.fromJson(response, ShuiFeiJiLuEntity.class);
                            dianFeiAdapter = new DianFeiAdapter(getContext(),shanxuaihou);
                            LinearLayoutManager manager = new LinearLayoutManager(getContext());
                            recycerView.setLayoutManager(manager);
                            recycerView.setAdapter(dianFeiAdapter);
                        }
                    }
                });
    }




    //    电费充值记录的网络请求
    private void initDianFeiHttp() {
        OkHttpUtils.post()
                .url(ContractUtils.LOGIN_URL+"user/record")
                .addHeader("token",ContractUtils.getTOKEN(getContext()))
                .addParams("parkId",ContractUtils.getParkId(getContext()))
                .addParams("page",page+"")
                .addParams("id",yudian)
                .addParams("type","2")//1：水费充值记录 2：电表充值记录 3：一卡通
                .build()
                .execute(new StringCallback() {
                    @Override
                    public void onError(Call call, Exception e, int id) {

                    }

                    @Override
                    public void onResponse(String response, int id) {
                        System.out.println(response+"          dianfei充值记录水费的网络请求");
                        ContractUtils.Code500(getContext(),response);
                        if(response.indexOf("200") != -1){
                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                JSONObject jsonObject1 = jsonObject.getJSONObject("response");
                                JSONObject data = jsonObject1.getJSONObject("data");
                                if(data != null){
                                    JSONArray jsonArray = data.getJSONArray("list");

                                    for (int i = 0; i < jsonArray.length(); i++) {
                                        JSONObject object = (JSONObject) jsonArray.get(i);
                                        if(object != null){
                                            list.add(object);
                                        }
                                    }
                                }

                                if(time != null){
                                    shanxuaihou.clear();
                                    for (int i = 0; i < list.size(); i++) {
                                        JSONObject jsonObject2 = list.get(i);
                                        String time1 = jsonObject2.getString("time");
                                        if (time1.indexOf(time) != -1){
                                            shanxuaihou.add(jsonObject2);
                                        }
                                    }

//                                    chongZhiAdapter.notifyDataSetChanged();
                                }else{
                                    shanxuaihou.clear();
                                    shanxuaihou.addAll(list);
                                }



                                if(list == null){
                                    Toast.makeText(getContext(), "暂无数据", Toast.LENGTH_SHORT).show();
                                    return;
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

//                            Gson gson = new Gson();
//                            ShuiFeiJiLuEntity shuiFeiJiLuEntity = gson.fromJson(response, ShuiFeiJiLuEntity.class);
                            dianFeiAdapter = new DianFeiAdapter(getContext(),shanxuaihou);
                            LinearLayoutManager manager = new LinearLayoutManager(getContext());
                            recycerView.setLayoutManager(manager);
                            recycerView.setAdapter(dianFeiAdapter);
                        }
                    }
                });
    }


    private void initView() {
//        dianFeiAdapter = new DianFeiAdapter(getContext());
//        LinearLayoutManager manager = new LinearLayoutManager(getContext());
//        recycerView.setLayoutManager(manager);
//        recycerView.setAdapter(dianFeiAdapter);
    }


    @Override
    public void onPause() {
        super.onPause();
        page = 0;
    }



//    @Override
//    public void onRefresh(RefreshLayout refreshlayout) {
//        page = 0;
//
//        //        电费充值记录的网络请求
//        initDianFeiHttp();
//        if (refreshlayout.isRefreshing()) {
//            refreshlayout.finishRefresh();
//        }
//    }

    @Override
    public void onLoadmore(RefreshLayout refreshlayout) {
        page++;

        //        电费充值记录的网络请求
        initDianFeiHttp();
        refreshlayout.finishLoadmore();
    }
}
