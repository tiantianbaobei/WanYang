package com.wanyangproject.mysuipian;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.google.gson.Gson;
import com.wanyangproject.R;
import com.wanyangproject.entity.ShangJiaDingDanEntity;
import com.wanyangproject.entity.ShangJiaJieDanEntity;
import com.wanyangproject.entity.YiJieDanShiBaiEntity;
import com.wanyangproject.my.ZhaoPinXiangQingActivity;
import com.wanyangproject.myadapter.DaiPeiSongAdapter;
import com.wanyangproject.myadapter.YiJieDanAdapter;
import com.wanyangproject.utils.ContractUtils;
import com.zhy.http.okhttp.OkHttpUtils;
import com.zhy.http.okhttp.callback.StringCallback;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import okhttp3.Call;
import retrofit2.http.GET;

/**
 * Created by 甜甜 on 2018/8/4.
 */

public class DaiPeiSongFragment extends Fragment {
    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    private DaiPeiSongAdapter daiPeiSongAdapter;
    ArrayList arrayList = new ArrayList();
    private ShangJiaDingDanEntity shangJiaDingDanEntity;
    public String phone;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.my_fabu_fragment, container, false);
        ButterKnife.bind(this, view);
        initView();

        //        商家订单显示的网络请求
//        initShangJiaDingDanXianShiHttp();
        return view;
    }



    //    商家订单显示的网络请求
    public void initShangJiaDingDanXianShiHttp() {
        OkHttpUtils.post()
                .url(ContractUtils.LOGIN_URL+"Shop/shoporder")
                .addHeader("token",ContractUtils.getTOKEN(getContext()))
                .addParams("parkId",ContractUtils.getParkId(getContext()))
                .addParams("phone",ContractUtils.getPhone(getContext())) // 暂时  tiantian//订单状态1：未支付2：已支付3：代发货4：已完成5：商家已接单6：商家准备完成7：商家确认送达8：用户确认送达9：已评价10：商家拒绝接单11:用户申请退款12：退款订单完成
                .build()
                .execute(new StringCallback() {
                    @Override
                    public void onError(Call call, Exception e, int id) {

                    }

                    @Override
                    public void onResponse(String response, int id) {
                        ContractUtils.Code500(getContext(),response);
                        System.out.println(response+"        待配送商家订单显示的网络请求");

                        if(response.indexOf("200") != -1){
                            Gson gson = new Gson();
                            shangJiaDingDanEntity = gson.fromJson(response, ShangJiaDingDanEntity.class);
                            arrayList.clear();
                            for (int i = 0; i < shangJiaDingDanEntity.getResponse().size(); i++) {
                                ShangJiaDingDanEntity.ResponseBean responseBean = shangJiaDingDanEntity.getResponse().get(i);
                                if(responseBean.getType().equals("6")){
                                    arrayList.add(responseBean);
                                }
                            }
                            daiPeiSongAdapter = new DaiPeiSongAdapter(getContext(),arrayList);
                            LinearLayoutManager manager = new LinearLayoutManager(getContext());
                            recyclerView.setLayoutManager(manager);
                            recyclerView.setAdapter(daiPeiSongAdapter);

                            daiPeiSongAdapter.notifyDataSetChanged();

                            daiPeiSongAdapter.setShangJiaQueRenSongDaClick(new DaiPeiSongAdapter.ShangJiaQueRenSongDaClick() {
                                @Override
                                public void shangjiaquerensongdaClick(int position, String Order_sn) {
//                                    商家确认送达的网络请求
                                    initShangJiaQueRenSongDaHttp(position,Order_sn);
                                }
                            });


                            daiPeiSongAdapter.setLianXiMaiJiaClick(new DaiPeiSongAdapter.LianXiMaiJiaClick() {
                                @Override
                                public void lianximaijiaClick(int position,String phone) {
                                    phone = phone;
                                    //                判断当前SDK版本号
                                    if (Build.VERSION.SDK_INT >= 23) {
//                                         当前拨打电话权限是没有给的
                                        if (ContextCompat.checkSelfPermission(getContext(), Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
//                                           我要请求权限
                                            ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.CALL_PHONE}, 1);
                                        } else {
                                            call(phone);
                                        }
                                    } else {
                                        call(phone);
                                    }
                                }
                            });

                        }else if(response.indexOf("400") != -1){
                            Gson gson = new Gson();
                            YiJieDanShiBaiEntity yiJieDanShiBaiEntity = gson.fromJson(response, YiJieDanShiBaiEntity.class);
//                            Toast.makeText(getContext(), yiJieDanShiBaiEntity.getMsg(), Toast.LENGTH_SHORT).show();
                        }
                    }
                });
    }


    public void call(String phone) {
        try {
            Intent intent = new Intent(Intent.ACTION_CALL);
            intent.setData(Uri.parse("tel:" + phone));
            startActivity(intent);
        } catch (SecurityException e) {
            e.printStackTrace();
        }
    }






    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case 1:
                if (permissions.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    call(phone);
                } else {
                    Toast.makeText(getContext(), "您拒绝拨打电话", Toast.LENGTH_SHORT).show();
                }
                break;
        }
    }




    //    商家确认送达的网络请求
    private void initShangJiaQueRenSongDaHttp(int position, String Order_sn) {
        OkHttpUtils.post()
                .url(ContractUtils.LOGIN_URL+"Shop/orderconfirm")
                .addHeader("token",ContractUtils.getTOKEN(getContext()))
                .addParams("parkId",ContractUtils.getParkId(getContext()))
                .addParams("order_sn",Order_sn)
                .build()
                .execute(new StringCallback() {
                    @Override
                    public void onError(Call call, Exception e, int id) {

                    }

                    @Override
                    public void onResponse(String response, int id) {
                        ContractUtils.Code500(getContext(),response);
                        System.out.println(response+"     商家确认送达的网络请求");
                        if(response.indexOf("200") != -1){
                            Gson gson = new Gson();
                            ShangJiaJieDanEntity shangJiaJieDanEntity = gson.fromJson(response, ShangJiaJieDanEntity.class);
                            Toast.makeText(getContext(), shangJiaJieDanEntity.getResponse().getMes(), Toast.LENGTH_SHORT).show();
                                //        商家订单显示的网络请求
                                initShangJiaDingDanXianShiHttp();
                        }else if(response.indexOf("400") !=-1){
                            Gson gson = new Gson();
                            YiJieDanShiBaiEntity yiJieDanShiBaiEntity = gson.fromJson(response, YiJieDanShiBaiEntity.class);
                            Toast.makeText(getContext(), yiJieDanShiBaiEntity.getMsg(), Toast.LENGTH_SHORT).show();
                        }
//                        if(shangJiaJieDanEntity.getMsg() != null){
//                            Toast.makeText(getContext(), shangJiaJieDanEntity.getMsg(), Toast.LENGTH_SHORT).show();
//                        }
                    }
                });
    }



    private void initView() {
//        daiPeiSongAdapter = new DaiPeiSongAdapter(getContext());
//        LinearLayoutManager manager = new LinearLayoutManager(getContext());
//        recyclerView.setLayoutManager(manager);
//        recyclerView.setAdapter(daiPeiSongAdapter);
    }
}
