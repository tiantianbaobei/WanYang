package com.wanyangproject.mysuipian;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.google.gson.Gson;
import com.wanyangproject.R;
import com.wanyangproject.activity.TieZiXiangQingActivity;
import com.wanyangproject.entity.MyTieZiLieBiaoEntity;
import com.wanyangproject.myadapter.MyTieZiAdapter;
import com.wanyangproject.utils.ContractUtils;
import com.zhy.http.okhttp.OkHttpUtils;
import com.zhy.http.okhttp.callback.StringCallback;

import butterknife.BindView;
import butterknife.ButterKnife;
import okhttp3.Call;

/**
 * Created by 甜甜 on 2018/7/31.
 */

public class MyTieZiFragment extends Fragment {


    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    private MyTieZiAdapter myTieZiAdapter;
    private MyTieZiLieBiaoEntity myTieZiLieBiaoEntity;
    private int isok = 0;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.my_tiezi_fragment, container, false);
        ButterKnife.bind(this, view);
        initView();

////        我的收藏帖子列表的网络请求
        initShouCangTieZiHttp();

        return view;
    }


//
//    @Override
//    public void onResume() {
//        super.onResume();
//        isok++;
//        if(isok == 1){
/////        我的收藏帖子列表的网络请求
//            initShouCangTieZiHttp();
//        }
//    }



//    @Override
//    public void onResume() {
//        super.onResume();
//        //        我的收藏帖子列表的网络请求
//        initShouCangTieZiHttp();
//    }

    //    我的收藏帖子列表的网络请求
    private void initShouCangTieZiHttp() {
        final ProgressDialog progressDialog = new ProgressDialog(getContext());
        progressDialog.setTitle("提示");
        progressDialog.setMessage("请等待...");
        progressDialog.setCancelable(false);
        progressDialog.show();
        OkHttpUtils.post()
                .url(ContractUtils.LOGIN_URL + "forum/myShoucangForum")
                .addHeader("token", ContractUtils.getTOKEN(getContext()))
                .build()
                .execute(new StringCallback() {
                    @Override
                    public void onError(Call call, Exception e, int id) {
                        progressDialog.dismiss();
                    }

                    @Override
                    public void onResponse(String response, int id) {
                        progressDialog.dismiss();
                        System.out.println(response + "           我的收藏帖子列表的网络请求");
                        ContractUtils.Code500(getContext(), response);
                        if (response.indexOf("200") != -1) {
                            Gson gson = new Gson();
                            myTieZiLieBiaoEntity = gson.fromJson(response, MyTieZiLieBiaoEntity.class);
                            myTieZiAdapter = new MyTieZiAdapter(getContext(), myTieZiLieBiaoEntity.getResponse().getList());
                            LinearLayoutManager manager = new LinearLayoutManager(getContext());
                            recyclerView.setLayoutManager(manager);
                            recyclerView.setAdapter(myTieZiAdapter);


//                            点击进入详请
                            myTieZiAdapter.setXiangQingClick(new MyTieZiAdapter.XiangQingClick() {
                                @Override
                                public void xiangqingClick(int position, String id) {
                                    //                                点击进入帖子详情
                                    Intent intent = new Intent(getContext(), TieZiXiangQingActivity.class);
                                    intent.putExtra("ispc",myTieZiLieBiaoEntity.getResponse().getList().get(position).getIsPc());
                                    intent.putExtra("tieziid",id);
                                    System.out.println(id+"     发送帖子的id");
                                    startActivity(intent);
                                }
                            });




                            myTieZiAdapter.setShanChuClick(new MyTieZiAdapter.ShanChuClick() {
                                @Override
                                public void shanchuClick(int position, String id) {
//                                    删除帖子的网络请求
                                    initShanChuTieZiHttp(position, id);
                                }
                            });
                        }else if(response.indexOf("400") != -1){
//                            ContractUtils.Code400(getContext(),response);
                            Toast.makeText(getContext(), "暂无数据", Toast.LENGTH_SHORT).show();
                            if(myTieZiLieBiaoEntity != null){
                                myTieZiLieBiaoEntity.getResponse().getList().clear();
                                myTieZiAdapter.notifyDataSetChanged();
                            }
                        }
                    }
                });
    }


    //    删除帖子的网络请求
    private void initShanChuTieZiHttp(int position, String id) {
        OkHttpUtils.post()
                .url(ContractUtils.LOGIN_URL + "forum/ShoucangForum")
                .addHeader("token", ContractUtils.getTOKEN(getContext()))
                .addParams("id", id)
                .build()
                .execute(new StringCallback() {
                    @Override
                    public void onError(Call call, Exception e, int id) {

                    }

                    @Override
                    public void onResponse(String response, int id) {
                        System.out.println(response + "            删除帖子的网络请求");
                        if(response.indexOf("200") != -1){
                            Toast.makeText(getContext(), "删除成功", Toast.LENGTH_SHORT).show();

//                          我的收藏帖子列表的网络请求
                            initShouCangTieZiHttp();
                        }
                    }
                });
    }


    private void initView() {
//        myTieZiAdapter = new MyTieZiAdapter(getContext());
//        LinearLayoutManager manager = new LinearLayoutManager(getContext());
//        recyclerView.setLayoutManager(manager);
//        recyclerView.setAdapter(myTieZiAdapter);
    }
}
