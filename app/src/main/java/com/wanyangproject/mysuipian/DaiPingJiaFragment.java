package com.wanyangproject.mysuipian;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.google.gson.Gson;
import com.wanyangproject.R;
import com.wanyangproject.entity.ChuanZhiEntity;
import com.wanyangproject.entity.DaiPingJiaEntity;
import com.wanyangproject.my.DaiPingJiaXiangQingActivity;
import com.wanyangproject.my.PingJiaActivity;
import com.wanyangproject.myadapter.DaiPingJiaAdapter;
import com.wanyangproject.utils.ContractUtils;
import com.zhy.http.okhttp.OkHttpUtils;
import com.zhy.http.okhttp.callback.StringCallback;

import butterknife.BindView;
import butterknife.ButterKnife;
import okhttp3.Call;

/**
 * Created by 甜甜 on 2018/7/30.
 */

public class DaiPingJiaFragment extends Fragment {

    @BindView(R.id.recycerView)
    RecyclerView recycerView;
    private DaiPingJiaAdapter daiPingJiaAdapter;
    private DaiPingJiaEntity daiPingJiaEntity;
    private NetWork netWork;
    private String pingjia;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.dingdan_daipingjia_fragment, container, false);
        ButterKnife.bind(this, view);
        initView();
//        订单列表待评价的网络请求
//        initDaiPingJiaHttp();

        IntentFilter intentFilter4 = new IntentFilter();
        intentFilter4.addAction("pingjia");
        netWork = new NetWork();
        getContext().registerReceiver(netWork, intentFilter4);

        return view;
    }



    class NetWork extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {

            //            结束页面
            pingjia = intent.getStringExtra("pingjia");
            if (pingjia != null) {
                //        //        订单列表待评价的网络请求
                initDaiPingJiaHttp();
            }
        }
    }









//    @Override
//    public void onResume() {
//        super.onResume();
//        //        订单列表待评价的网络请求
//        initDaiPingJiaHttp();
//    }

    //    订单列表待评价的网络请求
    public void initDaiPingJiaHttp() {
        OkHttpUtils.post()
                .url(ContractUtils.LOGIN_URL+"Life/orderlist")
                .addHeader("token",ContractUtils.getTOKEN(getContext()))
                .addParams("parkId",ContractUtils.getParkId(getContext()))
                .addParams("type","7,8")//订单状态1：未支付2：已支付3：代发货4：已完成5：商家已接单6：商家准备完成7：商家确认送达8：用户确认送达9：已评价10：商家拒绝接单11:用户申请退款12：退款订单完成
                .build()
                .execute(new StringCallback() {
                    @Override
                    public void onError(Call call, Exception e, int id) {

                    }

                    @Override
                    public void onResponse(String response, int id) {
                        ContractUtils.Code500shibai(getContext(),response);
                        System.out.println(response+"       订单列表待评价的网络请求");

                        if(response.indexOf("200") != -1){
                            Gson gson = new Gson();
                            daiPingJiaEntity = gson.fromJson(response, DaiPingJiaEntity.class);
                            daiPingJiaAdapter = new DaiPingJiaAdapter(getContext(),daiPingJiaEntity.getResponse());
                            LinearLayoutManager manager = new LinearLayoutManager(getContext());
                            recycerView.setLayoutManager(manager);
                            recycerView.setAdapter(daiPingJiaAdapter);

//                            点击进入详情
                            daiPingJiaAdapter.setDaiPingJiaClick(new DaiPingJiaAdapter.DaiPingJiaClick() {
                                @Override
                                public void daipingjiaclick(int position) {
                                    Intent intent = new Intent(getContext(), DaiPingJiaXiangQingActivity.class);
                                    intent.putExtra("dingdanhao",daiPingJiaEntity.getResponse().get(position).getOrder_sn());
                                    startActivity(intent);
                                }
                            });


//                            点击去评价
                            daiPingJiaAdapter.setQuPingJiaClick(new DaiPingJiaAdapter.QuPingJiaClick() {
                                @Override
                                public void qupungjiaClick(int position, String dingdanhao) {
                                    Intent intent = new Intent(getContext(), PingJiaActivity.class);
                                    ChuanZhiEntity.obj = daiPingJiaEntity.getResponse().get(position);// 传值
//                                    intent.putExtra("dingdanhao",dingdanhao);
//                                    intent.putExtra("name",daiPingJiaEntity.getResponse().get(position).getShop_name());
                                    startActivity(intent);
                                }
                            });

//                            daiPingJiaAdapter.setQuPingJiaClick(new DaiPingJiaAdapter.QuPingJiaClick() {
//                                @Override
//                                public void qupungjiaClick(int position) {
//                                    Intent intent = new Intent(getContext(), PingJiaActivity.class);
//                                    startActivity(intent);
//                                }
//                            });
                        }else if(response.indexOf("400") != -1){
                            Toast.makeText(getContext(), "您还没有待评价订单！", Toast.LENGTH_SHORT).show();
                            if(daiPingJiaEntity != null){
                                daiPingJiaEntity.getResponse().clear();

                                daiPingJiaAdapter.notifyDataSetChanged();
                            }

                        }
                    }
                });
    }

    private void initView() {
//        daiPingJiaAdapter = new DaiPingJiaAdapter(getContext());
//        LinearLayoutManager manager = new LinearLayoutManager(getContext());
//        recycerView.setLayoutManager(manager);
//        recycerView.setAdapter(daiPingJiaAdapter);
//
//        daiPingJiaAdapter.setDaiPingJiaClick(new DaiPingJiaAdapter.DaiPingJiaClick() {
//            @Override
//            public void daipingjiaclick(int position) {
//                Intent intent = new Intent(getContext(), DaiPingJiaXiangQingActivity.class);
//                startActivity(intent);
//            }
//        });
//
//
//
//        daiPingJiaAdapter.setQuPingJiaClick(new DaiPingJiaAdapter.QuPingJiaClick() {
//            @Override
//            public void qupungjiaClick(int position) {
//                Intent intent = new Intent(getContext(), PingJiaActivity.class);
//                startActivity(intent);
//            }
//        });
    }
}
