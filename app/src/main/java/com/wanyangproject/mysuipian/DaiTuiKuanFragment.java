package com.wanyangproject.mysuipian;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.google.gson.Gson;
import com.wanyangproject.R;
import com.wanyangproject.entity.DaiTuiKuanEntity;
import com.wanyangproject.entity.ShangJiaDingDanEntity;
import com.wanyangproject.entity.YiJieDanShiBaiEntity;
import com.wanyangproject.myadapter.DaiTuiKuanAdapter;
import com.wanyangproject.myadapter.YiJieDanAdapter;
import com.wanyangproject.utils.ContractUtils;
import com.zhy.http.okhttp.OkHttpUtils;
import com.zhy.http.okhttp.callback.StringCallback;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import okhttp3.Call;

/**
 * Created by 甜甜 on 2018/8/4.
 */

public class DaiTuiKuanFragment extends Fragment {
    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    private DaiTuiKuanAdapter daiTuiKuanAdapter;
    ArrayList arrayList = new ArrayList();

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.my_fabu_fragment, container, false);
        ButterKnife.bind(this, view);
        initView();


        //        商家订单显示的网络请求
//        initShangJiaDingDanXianShiHttp();

        return view;
    }






    //    商家订单显示的网络请求
    public void initShangJiaDingDanXianShiHttp() {
        OkHttpUtils.post()
                .url(ContractUtils.LOGIN_URL+"Shop/shoporder")
                .addHeader("token",ContractUtils.getTOKEN(getContext()))
                .addParams("parkId",ContractUtils.getParkId(getContext()))
                .addParams("phone",ContractUtils.getPhone(getContext())) // 暂时  tiantian//订单状态1：未支付2：已支付3：代发货4：已完成5：商家已接单6：商家准备完成7：商家确认送达8：用户确认送达9：已评价10：商家拒绝接单11:用户申请退款12：退款订单完成
                .build()
                .execute(new StringCallback() {
                    @Override
                    public void onError(Call call, Exception e, int id) {

                    }

                    @Override
                    public void onResponse(String response, int id) {
                        ContractUtils.Code500(getContext(),response);
                        System.out.println(response+"        待退款商家订单显示的网络请求");

                        if(response.indexOf("200") != -1){
                            Gson gson = new Gson();
                            ShangJiaDingDanEntity shangJiaDingDanEntity = gson.fromJson(response, ShangJiaDingDanEntity.class);
                            arrayList.clear();
                            for (int i = 0; i < shangJiaDingDanEntity.getResponse().size(); i++) {
                                ShangJiaDingDanEntity.ResponseBean responseBean = shangJiaDingDanEntity.getResponse().get(i);
                                if(responseBean.getType().equals("11") || responseBean.getType().equals("12")){ // 退款的type  tiantian 暂时
                                    arrayList.add(responseBean);
                                }
                            }

                            daiTuiKuanAdapter = new DaiTuiKuanAdapter(getContext(),arrayList);
                            LinearLayoutManager manager = new LinearLayoutManager(getContext());
                            recyclerView.setLayoutManager(manager);
                            recyclerView.setAdapter(daiTuiKuanAdapter);

                            daiTuiKuanAdapter.notifyDataSetChanged();

                            daiTuiKuanAdapter.setQueRenTuiKuanClick(new DaiTuiKuanAdapter.QueRenTuiKuanClick() {
                                @Override
                                public void querentuikuanClick(int position, String Order_sn) {
//                                    确认退款按钮的网络请求
                                    initQueRenTuiKuanHttp(position,Order_sn);
                                }
                            });
                        }else if(response.indexOf("400") != -1){
                            Gson gson = new Gson();
                            YiJieDanShiBaiEntity yiJieDanShiBaiEntity = gson.fromJson(response, YiJieDanShiBaiEntity.class);
//                            Toast.makeText(getContext(), yiJieDanShiBaiEntity.getMsg(), Toast.LENGTH_SHORT).show();
                        }
                    }
                });
    }



//    确认退款按钮的网络请求
    private void initQueRenTuiKuanHttp(int position, String Order_sn) {
        OkHttpUtils.post()
                .url(ContractUtils.LOGIN_URL+"Shop/orderrefusetui")
                .addHeader("token",ContractUtils.getTOKEN(getContext()))
                .addParams("parkId",ContractUtils.getParkId(getContext()))
                .addParams("order_sn",Order_sn)
                .build()
                .execute(new StringCallback() {
                    @Override
                    public void onError(Call call, Exception e, int id) {
                        System.out.println(e+"          eee确认退款按钮");
                    }

                    @Override
                    public void onResponse(String response, int id) {
                        ContractUtils.Code500(getContext(),response);
                        System.out.println(response+"      点击确认退款按钮的网络请求");
                        if(response.indexOf("200") != -1){
                            Toast.makeText(getContext(), "退款成功！", Toast.LENGTH_SHORT).show();
                            //        商家订单显示的网络请求
                            initShangJiaDingDanXianShiHttp();

                        }else if(response.indexOf("400") != -1){
                            Gson gson = new Gson();
                            DaiTuiKuanEntity daiTuiKuanEntity = gson.fromJson(response, DaiTuiKuanEntity.class);

                            Toast.makeText(getContext(), daiTuiKuanEntity.getMsg(), Toast.LENGTH_SHORT).show();
                        }
                    }
                });
    }



    private void initView() {
//        daiTuiKuanAdapter = new DaiTuiKuanAdapter(getContext());
//        LinearLayoutManager manager = new LinearLayoutManager(getContext());
//        recyclerView.setLayoutManager(manager);
//        recyclerView.setAdapter(daiTuiKuanAdapter);
    }
}
