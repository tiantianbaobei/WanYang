package com.wanyangproject.wxapi;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.widget.Toast;

import com.tencent.mm.opensdk.constants.ConstantsAPI;
import com.tencent.mm.opensdk.modelbase.BaseReq;
import com.tencent.mm.opensdk.modelbase.BaseResp;
import com.tencent.mm.opensdk.openapi.IWXAPI;
import com.tencent.mm.opensdk.openapi.IWXAPIEventHandler;
import com.tencent.mm.opensdk.openapi.WXAPIFactory;

/**
 * Created by 甜甜 on 2018/8/30.
 */

public class WXPayEntryActivity extends Activity implements IWXAPIEventHandler {



    private IWXAPI api;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        api = WXAPIFactory.createWXAPI(this, "wx363ae6178db5e958");
        api.handleIntent(getIntent(), this);
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        setIntent(intent);
        api.handleIntent(intent, this);

    }

    @Override
    public void onReq(BaseReq baseReq) {
        
    }

    @Override
    public void onResp(BaseResp baseResp) {

        if (baseResp.errCode==0) {
            //支付成功
            Toast.makeText(this, "支付成功", Toast.LENGTH_SHORT).show();
            Intent intent=new Intent();
            intent.putExtra("cheng",baseResp.errCode+"");
            intent.setAction("wxpay");
            sendBroadcast(intent);



            Intent intent1=new Intent();
            intent1.putExtra("finish","finish");
            intent1.setAction("finish");
            sendBroadcast(intent1);

            Intent intent2=new Intent();
            intent2.putExtra("chongzhichenggong","chongzhichenggong");
            intent2.setAction("chongzhichenggong");
            sendBroadcast(intent2);


            finish();
        }else {
            //支付失败
            Toast.makeText(this, "支付失败", Toast.LENGTH_SHORT).show();
            finish();
        }




        }
}
