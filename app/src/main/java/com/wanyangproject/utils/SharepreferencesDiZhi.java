package com.wanyangproject.utils;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by 甜甜 on 2018/8/30.
 */

public class SharepreferencesDiZhi {
    public static void saveDiZhi(Context context, String dizhi){
        SharedPreferences sharedPreferences = context.getSharedPreferences("dz",Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString("dizhi",dizhi);
        editor.apply();
    }
}
