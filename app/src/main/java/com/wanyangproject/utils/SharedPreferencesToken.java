package com.wanyangproject.utils;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by 甜甜 on 2018/8/7.
 */

//   登录成功之后保存的Token
public class SharedPreferencesToken {


    public static void saveToken(Context context, String token) {
        SharedPreferences sharedPreferences = context.getSharedPreferences("token", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString("token", token);
        editor.apply();
    }


    public static void saveRegisterToken(Context context, String token) {
        SharedPreferences sharedPreferences = context.getSharedPreferences("registertoken", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString("registertoken", token);
        editor.apply();
    }

    public static void saveParkID(Context context, String parkid) {
        SharedPreferences sharedPreferences = context.getSharedPreferences("parkid", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString("parkid", parkid);
        editor.apply();
    }



    public static void savePhone(Context context, String phone) {
        SharedPreferences sharedPreferences = context.getSharedPreferences("phone", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString("phone", phone);
        editor.apply();
    }


    public static void saveYuanquName(Context context, String yuanquname) {
        SharedPreferences sharedPreferences = context.getSharedPreferences("yuanquname", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString("yuanquname", yuanquname);
        editor.apply();
    }



    public static void savetypeId(Context context, String typeid) {
        SharedPreferences sharedPreferences = context.getSharedPreferences("typeIdshenfen", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString("typeid", typeid);
        editor.apply();
}



    public static void savetypeId22(Context context, String typeid22) {
        SharedPreferences sharedPreferences = context.getSharedPreferences("typeIdshenfen22", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString("typeIdshenfen22", typeid22);
        editor.apply();
    }






    public static void savehuancun(Context context, String huancun) {
        SharedPreferences sharedPreferences = context.getSharedPreferences("huancun", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString("huancun", huancun);
        editor.apply();
    }





    public static void saveZiTiDaXiao(Context context, float zitidaiao) {
        SharedPreferences sharedPreferences = context.getSharedPreferences("zitidaxiao", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putFloat("zitidaxiao", zitidaiao);
        editor.apply();
    }





    public static void saveziti(Context context, String ziti) {
        SharedPreferences sharedPreferences = context.getSharedPreferences("ziti", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString("ziti", ziti);
        editor.apply();
    }





}
