package com.wanyangproject.utils;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by 甜甜 on 2018/8/6.
 */

public class SharedPreferencesXuanZeYuanQu {

    public static void saveYuanQu(Context context,String id, String yuanqu){
        SharedPreferences sharedPreferences = context.getSharedPreferences("yuanqu",Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString("yuanqu",yuanqu);
        editor.apply();
    }

}
