package com.wanyangproject.utils;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by 甜甜 on 2017/12/13.
 */

public class SharedPreferencesPhone {

    public static void savePhone(Context context, String response) {
        SharedPreferences sharedPreferences = context.getSharedPreferences("user", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString("response", response);
        editor.apply();
    }



    public static void clearPhone(Context context) {
        SharedPreferences sharedPreferences = context.getSharedPreferences("user", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.clear();
        editor.commit();
    }

}