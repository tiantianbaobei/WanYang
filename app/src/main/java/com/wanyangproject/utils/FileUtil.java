package com.wanyangproject.utils;

public class FileUtil {

    /**
     * 自定义文件名称
     */
    public static String convertFileName(String url) {
        return Md5Util.getEncryption(url) + url.substring(url.lastIndexOf("."));
    }


    // 获取文件扩展名
    public static String getExtensionName(String filename) {
        if ((filename != null) && (filename.length() > 0)) {
            int dot = filename.lastIndexOf('.');
            if ((dot > -1) && (dot < (filename.length() - 1))) {
                return filename.substring(dot + 1);
            }
        }
        return "";
    }

}
