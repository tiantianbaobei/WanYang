package com.wanyangproject.utils;

import android.content.Context;
import android.content.Intent;
import android.webkit.WebResourceRequest;
import android.webkit.WebView;

import com.wanyangproject.activity.LoginActivity;
import com.wanyangproject.fuwuactivity.FangKeDengJiActivity;
import com.wanyangproject.fuwuactivity.QiYeFuWuActivity;
import com.wanyangproject.fuwuactivity.WuYeFuWuLieBiaoActivity;
import com.wanyangproject.fuwuactivity.YuanQuTongZhiActivity;
import com.wanyangproject.my.MyBangDingActivity;
import com.wanyangproject.my.MyLunTanActivity;
import com.wanyangproject.my.MyShouCangActivity;
import com.wanyangproject.my.QuanBuActivity;
import com.wanyangproject.my.RuZhuShenQingActivity;
import com.wanyangproject.my.SettingActivity;
import com.wanyangproject.shouye.ChanYeJinRongActivity;
import com.wanyangproject.shouye.QiuZhiZhaoPinActivity;
import com.wanyangproject.shouye.ShouHuoXinXiActivity;
import com.wanyangproject.shouye.YuanQuQiYeActivity;
import com.wanyangproject.shouye.YuanQuShengHuoActivity;
import com.wanyangproject.shouye.YuanQuShouCeActivity;
import com.wanyangproject.shouye.YuanQuZhaoShangActivity;
import com.wanyangproject.shouye.ZhengCeFaGuiActivity;
import com.wanyangproject.shouye.ZhiNengZhiZaoActivity;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;

/**
 * Created by 甜甜 on 2018/8/31.
 */

public class WebViewUtils {

    public static Boolean shouldOverrideUrlLoading(WebView view, String url, Context context)  {

        System.out.println(url+"                qqqqqqqqqqqqqqqqqqqqqq");
        String hh = "http://www.baidu.com/wanyang=";
        if(url.indexOf("wanyang=") > -1){
            String string = url;
            String substring = string.substring(hh.length(),string.length());
            System.out.println(substring+"           subsubsub");
            try {
                String code = URLDecoder.decode(substring,"UTF-8");
                System.out.println(code+"      ...............");

//                园区手册、园区企业、园区生活、园区招商、求职招聘、政策法规、金融/制造、企服中心、
//          物业服务、企业服务、园区通知、访客登记、我的订单、我的收藏、我的论坛、我的地址、商家入驻、绑定园区账号、设置

                if(code.equals("园区生活")){
                    Intent intent = new Intent(context,YuanQuShengHuoActivity.class);
                    context.startActivity(intent);
                }else if(code.equals("园区手册")){
                    Intent intent = new Intent(context, YuanQuShouCeActivity.class);
                    context.startActivity(intent);
                }else if(code.equals("园区企业")){
                    Intent intent = new Intent(context, YuanQuQiYeActivity.class);
                    context.startActivity(intent);
                }else if(code.equals("园区招商")){
                    Intent intent = new Intent(context, YuanQuZhaoShangActivity.class);
                    context.startActivity(intent);
                }else if(code.equals("求职招聘")){
                    Intent intent = new Intent(context, QiuZhiZhaoPinActivity.class);
                    context.startActivity(intent);
                }else if (code.equals("政策法规")){
                    Intent intent = new Intent(context, ZhengCeFaGuiActivity.class);
                    context.startActivity(intent);
                }else if(code.equals("金融/制造")){
                    Intent intent = new Intent(context,ChanYeJinRongActivity.class);
                    context.startActivity(intent);
                }else if(code.equals("企服中心")){
                    Intent intent = new Intent(context, ZhiNengZhiZaoActivity.class);
                    context.startActivity(intent);
                }else if(code.equals("物业服务")){
                    Intent intent = new Intent(context,WuYeFuWuLieBiaoActivity.class);
                    context.startActivity(intent);
                }else if(code.equals("企业服务")){
                    Intent intent = new Intent(context,QiYeFuWuActivity.class);
                    context.startActivity(intent);
                }else if(code.equals("园区通知")){
                    Intent intent = new Intent(context,YuanQuTongZhiActivity.class);
                    context.startActivity(intent);
                }else if(code.equals("访客登记")){
                    Intent intent = new Intent(context, FangKeDengJiActivity.class);
                    context.startActivity(intent);
                }else if(code.equals("我的订单")){
                    Intent intent = new Intent(context,QuanBuActivity.class);
                    context.startActivity(intent);
                }else if(code.equals("我的收藏")){
                    Intent intent = new Intent(context,MyShouCangActivity.class);
                    context.startActivity(intent);
                }else if(code.equals("我的论坛")){
                    Intent intent = new Intent(context,MyLunTanActivity.class);
                    context.startActivity(intent);
                }else if(code.equals("我的地址")){
                    Intent intent = new Intent(context,ShouHuoXinXiActivity.class);
                    context.startActivity(intent);
                }else if(code.equals("商家入驻")){
                    Intent intent = new Intent(context,RuZhuShenQingActivity.class);
                    context.startActivity(intent);
                }else if(code.equals("绑定园区账号")){
                    Intent intent = new Intent(context,MyBangDingActivity.class);
                    context.startActivity(intent);
                }else if(code.equals("设置")){
                    Intent intent = new Intent(context,SettingActivity.class);
                    context.startActivity(intent);
                }


            } catch (UnsupportedEncodingException e) {
                System.out.println("/////////////");
                e.printStackTrace();
            }

            return true;
        }


        if(url.indexOf("http://www.baidu.com") != -1){
            return true;
        }

        view.loadUrl(url);
        System.out.println(url+"1111110000000000");

        return false;
    }


































    public static Boolean shouldOverrideUrlLoading(WebView view, WebResourceRequest request, Context context)  {

        System.out.println(request.getUrl().toString()+"                qqqqqqqqqqqqqqqqqqqqqq");
        String hh = "http://www.baidu.com/wanyang=";
        if(request.getUrl().toString().indexOf("wanyang=") > -1){
            String string = request.getUrl().toString();
            String substring = string.substring(hh.length(),string.length());
            System.out.println(substring+"           subsubsub");

            try {
                String code = URLDecoder.decode(substring,"UTF-8");
                System.out.println(code+"      ...............");

//                园区手册、园区企业、园区生活、园区招商、求职招聘、政策法规、金融/制造、企服中心、
//          物业服务、企业服务、园区通知、访客登记、我的订单、我的收藏、我的论坛、我的地址、商家入驻、绑定园区账号、设置

                if(code.equals("园区生活")){
                    Intent intent = new Intent(context,YuanQuShengHuoActivity.class);
                    context.startActivity(intent);
                }else if(code.equals("园区手册")){
                    Intent intent = new Intent(context, YuanQuShouCeActivity.class);
                    context.startActivity(intent);
                }else if(code.equals("园区企业")){
                    Intent intent = new Intent(context, YuanQuQiYeActivity.class);
                    context.startActivity(intent);
                }else if(code.equals("园区招商")){
                    Intent intent = new Intent(context, YuanQuZhaoShangActivity.class);
                    context.startActivity(intent);
                }else if(code.equals("求职招聘")){
                    Intent intent = new Intent(context, QiuZhiZhaoPinActivity.class);
                    context.startActivity(intent);
                }else if (code.equals("政策法规")){
                    Intent intent = new Intent(context, ZhengCeFaGuiActivity.class);
                    context.startActivity(intent);
                }else if(code.equals("金融/制造")){
                    Intent intent = new Intent(context,ChanYeJinRongActivity.class);
                    context.startActivity(intent);
                }else if(code.equals("企服中心")){
                    Intent intent = new Intent(context, ZhiNengZhiZaoActivity.class);
                    context.startActivity(intent);
                }else if(code.equals("物业服务")){
                    Intent intent = new Intent(context,WuYeFuWuLieBiaoActivity.class);
                    context.startActivity(intent);
                }else if(code.equals("企业服务")){
                    Intent intent = new Intent(context,QiYeFuWuActivity.class);
                    context.startActivity(intent);
                }else if(code.equals("园区通知")){
                    Intent intent = new Intent(context,YuanQuTongZhiActivity.class);
                    context.startActivity(intent);
                }else if(code.equals("访客登记")){
                    Intent intent = new Intent(context, FangKeDengJiActivity.class);
                    context.startActivity(intent);
                }else if(code.equals("我的订单")){
                    Intent intent = new Intent(context,QuanBuActivity.class);
                    context.startActivity(intent);
                }else if(code.equals("我的收藏")){
                    Intent intent = new Intent(context,MyShouCangActivity.class);
                    context.startActivity(intent);
                }else if(code.equals("我的论坛")){
                    Intent intent = new Intent(context,MyLunTanActivity.class);
                    context.startActivity(intent);
                }else if(code.equals("我的地址")){
                    Intent intent = new Intent(context,ShouHuoXinXiActivity.class);
                    context.startActivity(intent);
                }else if(code.equals("商家入驻")){
                    Intent intent = new Intent(context,RuZhuShenQingActivity.class);
                    context.startActivity(intent);
                }else if(code.equals("绑定园区账号")){
                    Intent intent = new Intent(context,MyBangDingActivity.class);
                    context.startActivity(intent);
                }else if(code.equals("设置")){
                    Intent intent = new Intent(context,SettingActivity.class);
                    context.startActivity(intent);
                }


            } catch (UnsupportedEncodingException e) {
                System.out.println("/////////////");
                e.printStackTrace();
            }

            return true;
        }


        if(request.getUrl().toString().indexOf("http://www.baidu.com") != -1){
            return true;
        }

        view.loadUrl(request.getUrl().toString());
        System.out.println(request.getUrl().toString()+"1111110000000000");

        return false;
    }
}
