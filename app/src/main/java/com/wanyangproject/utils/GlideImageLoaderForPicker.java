package com.wanyangproject.utils;

import android.app.Activity;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.lzy.imagepicker.loader.ImageLoader;

/**
 * Created by 甜甜 on 2018/1/12.
 */


public class GlideImageLoaderForPicker implements ImageLoader {

    @Override
    public void displayImage(Activity activity, String path, ImageView imageView, int width, int height) {

        Glide.with(activity)
                .load(path)
                .into(imageView);
    }

    @Override
    public void clearMemoryCache() {

    }
}
