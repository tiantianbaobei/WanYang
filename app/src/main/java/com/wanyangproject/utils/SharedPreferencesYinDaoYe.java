package com.wanyangproject.utils;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by 甜甜 on 2018/1/31.
 */

public class SharedPreferencesYinDaoYe {

    public static void saveyindaoye(Context context, String yindaoye){
        SharedPreferences sharedPreferences = context.getSharedPreferences("yindaoye", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString("yindaoye",yindaoye);
        editor.apply();
    }
}
