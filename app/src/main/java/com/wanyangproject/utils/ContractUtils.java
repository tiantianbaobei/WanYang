package com.wanyangproject.utils;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.text.TextUtils;
import android.widget.Toast;

import com.umeng.message.PushAgent;
import com.umeng.message.UTrack;
import com.wanyangproject.activity.LoginActivity;
import com.wanyangproject.base.MyApp;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;

/**
 * Created by 甜甜 on 2017/9/10.
 */

public class ContractUtils {


//    所有接口网址
//    public static final String LOGIN_URL = "http://park.hostop.net/api/";
    public static final String LOGIN_URL = "http://park.vyzcc.com/api/";

//    园区招商的图片地址
//    public static final String YUANQUZHAOSHANG_PHOTO_URL = "http://park.hostop.net";
    public static final String YUANQUZHAOSHANG_PHOTO_URL = "http://park.vyzcc.com";
//    图片地址
    public static final String PHOTO_URL = "http://";











    //    郭林省市区的网址
    public static final String URL_PROVINCE = "http://guolin.tech/api/china";

//    市的网址
    public static final String URL_CITY = URL_PROVINCE+"/";
//    区的网址
    public static final String URL_COUNTY = URL_CITY+"/";

    private static Context context;


    public static String TOKEN;
    public static String parkId;
    public static String phone;
    public static String yuanquname;
    public static String typeId;
    public static String typeId22;



    //    typeId 切换身份角色
    public static String getTypeId(Context context) {
        SharedPreferences sharedPreferences = context.getSharedPreferences("typeIdshenfen",Context.MODE_APPEND);
        String typeid = sharedPreferences.getString("typeid", "0");
        System.out.println(typeid+"            typeId 切换身份角色");
        return typeid;
    }

    public static void setTypeId(String typeId) {
        ContractUtils.typeId = typeId;
        SharedPreferencesToken.savetypeId(MyApp.getContext(),typeId);
    }


    //    typeId 切换身份角色第二个身份

    public static String getTypeId22(Context context) {
        SharedPreferences sharedPreferences = context.getSharedPreferences("typeIdshenfen22",Context.MODE_APPEND);
        String typeid2 = sharedPreferences.getString("typeIdshenfen22", "1");
        System.out.println(typeid2+"            typeId222222222222 切换身份角色");
        return typeid2;
    }

    public static void setTypeId22(String typeId22) {
        ContractUtils.typeId22 = typeId22;
        SharedPreferencesToken.savetypeId22(MyApp.getContext(),typeId22);
    }


//    public static String getTypeId22(Context context) {
//        SharedPreferences sharedPreferences = context.getSharedPreferences("typeIdshenfen22",Context.MODE_APPEND);
//        String typeid2 = sharedPreferences.getString("typeid22", "1");
//        System.out.println(typeid2+"            typeId 切换身份角色");
//        return typeid2;
//    }
////
//    public static String getTypeId22() {
//        return typeId22;
//    }
//
//    public static void setTypeId22(String typeId2) {
//        ContractUtils.typeId2 = typeId2;
//        SharedPreferencesToken.savetypeId(MyApp.getContext(),typeId2);
//    }





//    园区名称
    public static String getYuanquname(Context context) {
        SharedPreferences sharedPreferences = context.getSharedPreferences("yuanquname",Context.MODE_APPEND);
        String yuanquname = sharedPreferences.getString("yuanquname", "yuanquname");
        System.out.println(yuanquname+"         yuanqunamenamename");
        return yuanquname;
    }

    public static void setYuanquname(String yuanquname) {
        ContractUtils.yuanquname = yuanquname;
        SharedPreferencesToken.saveYuanquName(MyApp.getContext(),yuanquname);
    }






//    登录手机号
    public static String getPhone(Context context) {
        SharedPreferences sharedPreferences = MyApp.getContext().getSharedPreferences("phone",Context.MODE_APPEND);
        String phone = sharedPreferences.getString("phone", "phone");
        System.out.println(phone+"         手机号");
        return phone;
    }

    public static void setPhone(String phone) {
        ContractUtils.phone = phone;
        SharedPreferencesToken.savePhone(MyApp.getContext(),phone);
    }





//    园区id
    public static String getParkId(Context context) {
        SharedPreferences sharedPreferences = MyApp.getContext().getSharedPreferences("parkid", Context.MODE_PRIVATE);
        String parkid = sharedPreferences.getString("parkid", "parkid");
        System.out.println(parkid+"           parkidparkidparkidparkidparkid");
        return parkid;
    }

    public static void setParkId(String parkId) {
        ContractUtils.parkId = parkId;
        SharedPreferencesToken.saveParkID(MyApp.getContext(),parkId);
    }




    //    //    Token
    public static String getTOKEN(Context context) {
        SharedPreferences sharedPreferences = MyApp.getContext().getSharedPreferences("token", Context.MODE_PRIVATE);
        String token = sharedPreferences.getString("token", "token");
        System.out.println(token + "     ContractUtilstoken");
        return token;
    }
    //
    public static void setTOKEN(String TOKEN) {
        ContractUtils.TOKEN = TOKEN;
        SharedPreferencesToken.saveToken(MyApp.getContext(),TOKEN);
        System.out.println(TOKEN+"        tokentokentoken");
    }










    public static void  Code500(Context context, String response){
        //
        try {
            JSONObject jsonObject = new JSONObject(response);
            String code = jsonObject.getString("code");
            if(code != null){
                if(code.equals("500")){
                    Intent intent = new Intent(context, LoginActivity.class);
                    context.startActivity(intent);
                }
//                else if(code.equals("400")){
//                    Toast.makeText(context, jsonObject.getString("msg"), Toast.LENGTH_SHORT).show();
//                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }





    public static void  Code500shibai(Context context, String response){
        //
        try {
            JSONObject jsonObject = new JSONObject(response);
            String code = jsonObject.getString("code");
            if(code != null){
                if(code.equals("500")){
                    Intent intent = new Intent(context, LoginActivity.class);
                    context.startActivity(intent);
                }
//                else if(code.equals("400")){
//                    Toast.makeText(context, jsonObject.getString("msg"), Toast.LENGTH_SHORT).show();
//                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }










    public static void  Code400(Context context, String response){
        //
        try {
            JSONObject jsonObject = new JSONObject(response);
            String code = jsonObject.getString("code");
            if(code != null){
//                if(code.equals("500")){
//                    Intent intent = new Intent(context, LoginActivity.class);
//                    context.startActivity(intent);
//                }else

                    if(code.equals("400")){
                    Toast.makeText(context, jsonObject.getString("msg"), Toast.LENGTH_SHORT).show();
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }












    //    手机号
    public static boolean isChinaPhoneLegal(String str)
            throws PatternSyntaxException {
        String regExp = "^((13[0-9])|(15[^4])|(166)|(17[0-8])|(18[0-9])|(19[8-9])|(147,145))\\d{8}$";
        Pattern p = Pattern.compile(regExp);
        Matcher m = p.matcher(str);
        return m.matches();
    }




//    身份验证
    public static boolean verForm(String num) {
        String reg = "^\\d{15}$|^\\d{17}[0-9Xx]$";
        if (!num.matches(reg)) {
            System.out.println("Format Error!");
            return false;
        }
        return true;
    }



}
