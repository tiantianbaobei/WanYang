package com.wanyangproject.shouyefragment;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.google.gson.Gson;
import com.wanyangproject.R;
import com.wanyangproject.adapter.YuanQuShouCeAdapter;
import com.wanyangproject.entity.YuanQuShouCeEntity;
import com.wanyangproject.my.ZhaoPinXiangQingActivity;
import com.wanyangproject.shouye.YuanQuJieShaoActivity;
import com.wanyangproject.shouye.YuanQuShouCeActivity;
import com.wanyangproject.shouye.ZiXunXiangQingActivity;
import com.wanyangproject.utils.ContractUtils;
import com.zhy.http.okhttp.OkHttpUtils;
import com.zhy.http.okhttp.callback.StringCallback;

import butterknife.BindView;
import butterknife.ButterKnife;
import okhttp3.Call;

/**
 * Created by 甜甜 on 2018/7/24.
 */

public class YuanGongFragment extends Fragment {
    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
//    @BindView(R.id.tv_yuanqu_jieshao)
//    TextView tvYuanquJieshao;
//    @BindView(R.id.tv_ruyuan_xuzhi)
//    TextView tvRuyuanXuzhi;
//    @BindView(R.id.tv_shenghuo_zhinan)
//    TextView tvShenghuoZhinan;
//    @BindView(R.id.tv_fuwu_shenqing)
//    TextView tvFuwuShenqing;
    private YuanQuShouCeAdapter yuanQuShouCeAdapter;
    private YuanQuShouCeEntity yuanQuShouCeEntity;
    private String isOK;

    public String getIsOK() {
        return isOK;
    }

    public void setIsOK(String isOK) {
        this.isOK = isOK;
    }



    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.yuangong_fragment, container, false);
        ButterKnife.bind(this, view);


        //                           1 游客 2员工  3 企业  4 商家
//        typeId: 0 游客 1 普通员工 2企业用户 3商家 4 门卫 5 物业
        if(isOK.equals("2")){
            if(ContractUtils.getTypeId(getContext()).equals("1") || ContractUtils.getTypeId22(getContext()).equals("1")
                    || ContractUtils.getTypeId(getContext()).equals("2") || ContractUtils.getTypeId22(getContext()).equals("2")
                    ||  ContractUtils.getTypeId(getContext()).equals("4") || ContractUtils.getTypeId22(getContext()).equals("4")){
                //        园区手册的网络请求
                initYuanQuShouCeHttp();
            }
        }else if(isOK.equals("3")){
            if(ContractUtils.getTypeId(getContext()).equals("2") || ContractUtils.getTypeId22(getContext()).equals("2")){
                //        园区手册的网络请求
                initYuanQuShouCeHttp();
            }
        }else if(isOK.equals("4")){
            if(ContractUtils.getTypeId(getContext()).equals("3") || ContractUtils.getTypeId22(getContext()).equals("3")){
                //        园区手册的网络请求
                initYuanQuShouCeHttp();
            }
        }else if(isOK.equals("1")){
            if(ContractUtils.getTypeId(getContext()).equals("0") || ContractUtils.getTypeId22(getContext()).equals("0")){
                //        园区手册的网络请求
                initYuanQuShouCeHttp();
            }
        }


//        园区手册的网络请求
//        initYuanQuShouCeHttp();
        return view;
    }

//    园区手册的网络请求
    private void initYuanQuShouCeHttp() {
        final ProgressDialog progressDialog = new ProgressDialog(getContext());
        progressDialog.setTitle("提示");
        progressDialog.setMessage("请等待...");
        progressDialog.setCancelable(false);
        progressDialog.show();
        System.out.println(isOK+"   oooooooooooooo");
        OkHttpUtils.post()
                .url(ContractUtils.LOGIN_URL+"park/getmanual")
                .addHeader("token",ContractUtils.getTOKEN(getContext()))
                .addParams("parkId",ContractUtils.getParkId(getContext()))
                .addParams("bqId",isOK)
                .build()
                .execute(new StringCallback() {
                    @Override
                    public void onError(Call call, Exception e, int id) {
                        progressDialog.dismiss();
                    }

                    @Override
                    public void onResponse(String response, int id) {
                        progressDialog.dismiss();
                        ContractUtils.Code500(getContext(),response);
                        System.out.println(response+"    园区手册的网络请求");

                        if(response.indexOf("200") != -1){
                            Gson gson = new Gson();
                            yuanQuShouCeEntity = gson.fromJson(response, YuanQuShouCeEntity.class);
                            yuanQuShouCeAdapter = new YuanQuShouCeAdapter(getContext(),yuanQuShouCeEntity.getResponse().getList());
                            LinearLayoutManager manager = new LinearLayoutManager(getContext());
                            recyclerView.setLayoutManager(manager);
                            recyclerView.setAdapter(yuanQuShouCeAdapter);

                            yuanQuShouCeAdapter.setYuanQuShouCeXiangQingClick(new YuanQuShouCeAdapter.YuanQuShouCeXiangQingClick() {
                                @Override
                                public void yuanqushoucexiangqingClick(int position) {
                                    if(isOK.equals("2")){//员工
                                        if(ContractUtils.getTypeId(getContext()).equals("1") || ContractUtils.getTypeId22(getContext()).equals("1")){//typeId: 0 游客 1 普通员工 2企业用户 3商家 4 门卫 5 物业
                                            Intent intent = new Intent(getContext(), YuanQuJieShaoActivity.class);
                                            intent.putExtra("id",yuanQuShouCeEntity.getResponse().getList().get(position).getId());
                                            startActivity(intent);
                                        }else{
                                            Toast.makeText(getContext(), "您当前不是员工身份！", Toast.LENGTH_SHORT).show();
                                        }
                                    }



                                    if(isOK.equals("3")){//企业
                                        if(ContractUtils.getTypeId(getContext()).equals("2") || ContractUtils.getTypeId22(getContext()).equals("2")){//typeId: 0 游客 1 普通员工 2企业用户 3商家 4 门卫 5 物业
                                            Intent intent = new Intent(getContext(), YuanQuJieShaoActivity.class);
                                            intent.putExtra("id",yuanQuShouCeEntity.getResponse().getList().get(position).getId());
                                            startActivity(intent);
                                        }else{
                                            Toast.makeText(getContext(), "您当前不是企业身份！", Toast.LENGTH_SHORT).show();
                                        }
                                    }



                                    if(isOK.equals("4")){//商家
                                        if(ContractUtils.getTypeId(getContext()).equals("3") || ContractUtils.getTypeId22(getContext()).equals("3")){//typeId: 0 游客 1 普通员工 2企业用户 3商家 4 门卫 5 物业
                                            Intent intent = new Intent(getContext(), YuanQuJieShaoActivity.class);
                                            intent.putExtra("id",yuanQuShouCeEntity.getResponse().getList().get(position).getId());
                                            startActivity(intent);
                                        }else{
                                            Toast.makeText(getContext(), "您当前不是商家身份！", Toast.LENGTH_SHORT).show();
                                        }
                                    }


                                    if(isOK.equals("1")){//游客
                                        if(ContractUtils.getTypeId(getContext()).equals("0") || ContractUtils.getTypeId22(getContext()).equals("0")){//typeId: 0 游客 1 普通员工 2企业用户 3商家 4 门卫 5 物业
                                            Intent intent = new Intent(getContext(), YuanQuJieShaoActivity.class);
                                            intent.putExtra("id",yuanQuShouCeEntity.getResponse().getList().get(position).getId());
                                            startActivity(intent);
                                        }else{
                                            Toast.makeText(getContext(), "您当前不是游客身份！", Toast.LENGTH_SHORT).show();
                                        }
                                    }
                                }
                            });
                        }else if(response.indexOf("400") != -1){
                            ContractUtils.Code400(getContext(),response);
                        }
                    }
                });
    }




//    @OnClick({R.id.tv_yuanqu_jieshao, R.id.tv_ruyuan_xuzhi, R.id.tv_shenghuo_zhinan, R.id.tv_fuwu_shenqing})
//    public void onViewClicked(View view) {
//        switch (view.getId()) {
////            园区介绍
//            case R.id.tv_yuanqu_jieshao:
//                Intent intent = new Intent(getContext(), YuanQuJieShaoActivity.class);
//                startActivity(intent);
//                break;
////            入园须知
//            case R.id.tv_ruyuan_xuzhi:
//                Intent intent1 = new Intent(getContext(), RuYuanXuZhiActivity.class);
//                startActivity(intent1);
//                break;
////            生活指南
//            case R.id.tv_shenghuo_zhinan:
//                Intent intent2 = new Intent(getContext(), ShengHuoZhiNanActivity.class);
//                startActivity(intent2);
//                break;
////            服务申请流程
//            case R.id.tv_fuwu_shenqing:
//                Intent intent3 = new Intent(getContext(), FuWuLiuChengActivity.class);
//                startActivity(intent3);
//                break;
//        }
//    }
}
