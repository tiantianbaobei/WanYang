package com.wanyangproject.shouyefragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.wanyangproject.R;
import com.wanyangproject.adapter.ShouYePingJiaAdapter;
import com.wanyangproject.shouye.WaiMaiActivity;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by 甜甜 on 2018/8/8.
 */

public class ShouYePingJiaFragment extends Fragment {
    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    private ShouYePingJiaAdapter shouYePingJiaAdapter;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.shouye_waimai_fragment, container, false);
        ButterKnife.bind(this, view);
        initView();
        return view;
    }

    private void initView() {
        shouYePingJiaAdapter = new ShouYePingJiaAdapter(getContext());
        LinearLayoutManager manager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(manager);
        recyclerView.setAdapter(shouYePingJiaAdapter);

        shouYePingJiaAdapter.setPingJiaClick(new ShouYePingJiaAdapter.PingJiaClick() {
            @Override
            public void pingjiaClick(int position) {
                Intent intent = new Intent(getContext(),WaiMaiActivity.class);
                startActivity(intent);
            }
        });

    }
}
