package com.wanyangproject.shouyefragment;

import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.gson.Gson;
import com.wanyangproject.R;
import com.wanyangproject.activity.FaTieActivity;
import com.wanyangproject.adapter.ShangDianWaiMaiAdapter;
import com.wanyangproject.entity.PaiXuEntity;
import com.wanyangproject.shouye.DianPuActivity;
import com.wanyangproject.shouye.ShenFenBangDingActivity;
import com.wanyangproject.shouye.WaiMaiActivity;
import com.wanyangproject.utils.ContractUtils;
import com.zhy.http.okhttp.OkHttpUtils;
import com.zhy.http.okhttp.callback.StringCallback;

import butterknife.BindView;
import butterknife.ButterKnife;
import okhttp3.Call;

/**
 * Created by 甜甜 on 2018/7/25.
 */

public class ShouYePaiXuFragment extends Fragment {

    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    private ShangDianWaiMaiAdapter shangDianWaiMaiAdapter;
    private String str;//分类的id
    private int paixu;
    private PaiXuEntity paiXuEntity;

    public int getPaixu() {
        return paixu;
    }

    public void setPaixu(int paixu) {
        this.paixu = paixu;
    }

    public String getStr() {
        return str;
    }

    public void setStr(String str) {
        this.str = str;

    }



    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.shouye_waimai_fragment, container, false);
        ButterKnife.bind(this, view);

///        排序的网络请求
        initPaiXuHttp();

        return view;
    }

//    排序的网络请求
    private void initPaiXuHttp() {
        final ProgressDialog progressDialog = new ProgressDialog(getContext());
        progressDialog.setTitle("提示");
        progressDialog.setMessage("请等待...");
        progressDialog.setCancelable(false);
        progressDialog.show();
        System.out.println(str+"    接收分类的id");
//        tiantian
//          .addParams("parkId",ContractUtils.getParkId(getContext()))
//                .addParams("classId",str)
        String isOK = "";
        if(paixu == 0){

        }else if(paixu == 1){
            isOK = "sales";
        }else if(paixu == 2){
            isOK = "evaluate";
        }

        System.out.println(str+"     str ");
        System.out.println(isOK+"     isok");
        OkHttpUtils.post()
                .url(ContractUtils.LOGIN_URL+"Life/classlist")
                .addHeader("token",ContractUtils.getTOKEN(getContext()))
                .addParams("parkId",ContractUtils.getParkId(getContext()))
                .addParams("classId",str)
                .addParams(isOK,"1")
                .build()
                .execute(new StringCallback() {
                    @Override
                    public void onError(Call call, Exception e, int id) {
                        progressDialog.dismiss();
                    }

                    @Override
                    public void onResponse(String response, int id) {
                        ContractUtils.Code500(getContext(),response);
                        System.out.println(paixu+"     paixu");
                        progressDialog.dismiss();
                        System.out.println(response+"    排序的网络请求");
                        ContractUtils.Code500(getContext(),response);
                        if(response.indexOf("200") != -1){
                            Gson gson = new Gson();
                            paiXuEntity = gson.fromJson(response, PaiXuEntity.class);
                            shangDianWaiMaiAdapter = new ShangDianWaiMaiAdapter(getContext(),paiXuEntity.getResponse());
                            LinearLayoutManager manager = new LinearLayoutManager(getContext());
                            recyclerView.setLayoutManager(manager);
                            recyclerView.setAdapter(shangDianWaiMaiAdapter);



////                      1 营业    2 休息
//                            if(response.get(position).getBusiness() != null){
//                                if(response.get(position).getBusiness().equals("1")){
//                                    holder.tv_bidian.setVisibility(View.GONE);
//                                }else if(response.get(position).getBusiness().equals("2")){
//                                    holder.tv_bidian.setVisibility(View.VISIBLE);
//                                }
//                            }
//

                            shangDianWaiMaiAdapter.setShangDianWaiMaiClick(new ShangDianWaiMaiAdapter.ShangDianWaiMaiClick() {
                                @Override
                                public void shangdianwaimaiclick(int position) {
                                    Intent intent = new Intent(getContext(),WaiMaiActivity.class);
                                    intent.putExtra("id",paiXuEntity.getResponse().get(position).getId());
                                    intent.putExtra("bidian",paiXuEntity.getResponse().get(position).getBusiness());
                                    startActivity(intent);
                                }
                            });
                        }
                    }
                });
    }
}
