package com.wanyangproject.myfragment;

import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;


import com.wanyangproject.R;
import com.wanyangproject.adapter.OrderAdapter;
import com.wanyangproject.adapter.ShuiAdapter;
import com.wanyangproject.base.BaseFragment;
import com.wanyangproject.model.TestModel;
import com.wanyangproject.net.util.Constant;
import com.wanyangproject.net.util.ProgressSubscriber;
import com.wanyangproject.net.util.SwipeRefreshUtil;
import com.wanyangproject.widget.recyclerview.IRecyclerSwipe;
import com.wanyangproject.widget.recyclerview.refresh.OnLoadMoreListener;
import com.wanyangproject.widget.recyclerview.refresh.RecyclerViewWithFooter;

import java.util.List;
//   用水记录
public class ShuiFragment extends BaseFragment implements IRecyclerSwipe, SwipeRefreshLayout.OnRefreshListener, OnLoadMoreListener {

    private SwipeRefreshLayout swipeRefresh;
    private RecyclerViewWithFooter swipeRecycler;
    private int page = 0;
    private final int COUNT = 20;
    private boolean isRefresh = false;
    private ShuiAdapter shuiAdapter;

    public static ShuiFragment newInstance() {
        return new ShuiFragment();
    }

    @Override
    public int getContentResId() {
        return R.layout.swipe_recycler;
    }

    @Override
    public void init() {

        swipeRefresh = findViewById(R.id.swipe_refresh);
        swipeRecycler = findViewById(R.id.swipe_recycler);

        SwipeRefreshUtil.setColors(swipeRefresh);

        swipeRecycler.setLayoutManager(new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false));
        shuiAdapter = new ShuiAdapter(mContext, null);
        swipeRecycler.setAdapter(shuiAdapter);

        swipeRefresh.setOnRefreshListener(this);
//        swipeRecycler.setOnLoadMoreListener(this);

        getData(0);
    }

    private void getData(int page) {
//        mHttpUtil.getData(new ProgressSubscriber<TestModel>(mActivity, getContext()) {
//            @Override
//            public void next(TestModel testModel) {
//                List<TestModel.ResultsBean> results = testModel.getResults();
//                if (results != null) {
//                    int size = results.size();
//                    if (size > 0) {
//                        if (shuiAdapter != null) {
//                            if (isRefresh) {
//                                shuiAdapter.setData(results);
//                            } else {
//                                shuiAdapter.addData(results);
//                            }
//                        }
//                        if (size < COUNT) {
//                            setNoMore();
//                        }
//                    } else {
//                        setEnd();
//                    }
//                } else {
//                    setEnd();
//                }
//
//            }
//        }, COUNT, page);
    }

    private void setEnd() {
        if (shuiAdapter != null) {
            shuiAdapter.clearData();
            shuiAdapter.notifyDataSetChanged();
//            swipeRecycler.setEnd("获取数据为空");
        }
    }

    @Override
    public void showItemFail(String msg) {

    }

    @Override
    public void hideSwipeLoading() {
        swipeRefresh.setRefreshing(false);
    }

    @Override
    public void setNoMore() {
//        swipeRecycler.setEnd("没有更多数据");
    }

    @Override
    public void onRefresh() {
        isRefresh = true;
        page = 0;
        getData(page);
    }

    @Override
    public void onLoadMore() {
        isRefresh = false;
        page++;
        getData(page);
    }
}