package com.wanyangproject.myfragment;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.drawable.Drawable;
import android.media.Image;
import android.support.annotation.DrawableRes;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.gson.Gson;
import com.wanyangproject.MenWeiQiYeXinXiActivity;
import com.wanyangproject.R;
import com.wanyangproject.activity.ChangeParkActivity;
import com.wanyangproject.activity.YouHuiQuanFuWuActivity;
import com.wanyangproject.base.BaseFragment;
import com.wanyangproject.entity.SheZhiYuanQuNameEntity;
import com.wanyangproject.entity.UserEntity;
import com.wanyangproject.entity.XiTongXiaoXiWeiDuGeShuEntity;
import com.wanyangproject.fuwuactivity.FuWuMessageActivity;
import com.wanyangproject.fuwuactivity.FuWuShenQingJiLuActivity;
import com.wanyangproject.model.EventModel;
import com.wanyangproject.my.CheLiangGuanLiActivity;
import com.wanyangproject.my.DingDanTongJiActivity;
import com.wanyangproject.my.MyBangDingActivity;
import com.wanyangproject.my.MyFangKeGuanLiActivity;
import com.wanyangproject.my.MyLunTanActivity;
import com.wanyangproject.my.MyMenWeiFangKeGuanLiActivity;
import com.wanyangproject.my.MyQiYeGuanLiActivity;
import com.wanyangproject.my.MyShouCangActivity;
import com.wanyangproject.my.MySuSheActivity;
import com.wanyangproject.my.MySuSheGuanLiActivity;
import com.wanyangproject.my.MyYuanGongXinXiActivity;
import com.wanyangproject.my.QuanBuActivity;
import com.wanyangproject.my.RuZhuShenQingActivity;
import com.wanyangproject.my.ShangJiaDingDanActivity;
import com.wanyangproject.my.ShangJiaGuanLiActivity;
import com.wanyangproject.my.ShangJiaShangPinGuanLiActivity;
import com.wanyangproject.my.ShangJiaYouHuiQuanActivity;
import com.wanyangproject.my.ShuiDianFeiJiLuActivity;
import com.wanyangproject.my.WoDeZiLiaoActivity;
import com.wanyangproject.my.WuYeGuanLiActivity;
import com.wanyangproject.my.WuYeYuanGongXinXiActivity;
import com.wanyangproject.my.YiKaTongActivity;
import com.wanyangproject.net.util.Constant;
import com.wanyangproject.net.util.DensityUtil;
import com.wanyangproject.shouye.QieHuanYuanQuActivity;
import com.wanyangproject.shouye.ShenFenBangDingActivity;
import com.wanyangproject.shouye.ShouHuoXinXiActivity;
import com.wanyangproject.shouye.XuanZeYuanQuActivity;
import com.wanyangproject.shouye.YuanQuShengHuoActivity;
import com.wanyangproject.ui.BoxDialog;
import com.wanyangproject.ui.CheLiangActivity;
import com.wanyangproject.ui.CompanyDetailActivity;
import com.wanyangproject.ui.DormMgrActivity;
import com.wanyangproject.ui.EmployMgrActivity;
import com.wanyangproject.ui.FangKeActivity;
import com.wanyangproject.ui.OneCardActivity;
import com.wanyangproject.my.SettingActivity;
import com.wanyangproject.utils.ContractUtils;
import com.zhy.http.okhttp.OkHttpUtils;
import com.zhy.http.okhttp.callback.StringCallback;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import okhttp3.Call;

import static com.wanyangproject.net.util.Constant.BTN_SHANGJIA;
import static com.wanyangproject.net.util.Constant.ORDER_INDEX;
import static com.wanyangproject.net.util.Constant.ROLE_TOURIST;

//    我的界面
public class MineFragment extends BaseFragment implements View.OnClickListener {

    private ImageView mineHead;
    private TextView mineMgr1;
    private TextView mineMgr2;
    private TextView mineMgr3;
    private LinearLayout mineMgr;
    private TextView mineOrderAll;
    private TextView mineOrderPay;
    private TextView mineOrderReceive;
    private TextView mineOrderAppraise;
    private TextView mineOrderFinish;
    private RelativeLayout mineDot1;
    private RelativeLayout mineDot2;
    private RelativeLayout mineDot3;
    private RelativeLayout mineDot4;
    private LinearLayout mineOrder;
    private TextView mineCollect;
    private TextView mineForum;
    private TextView mineAddress;
    private TextView mineOther1;
    private TextView mineOther2;
    private TextView mineSet;
    private TextView tv_dingdan;
    private int ROLE_INDEX = ROLE_TOURIST;
    private TextView mine_dingwei;
    private TextView tv_shoucang;
    private View view;
    private TextView tv_yonghu;
    private String yuanqu;
    private ImageView image_qiehuan;
    private ImageView image_xiaoxi;
    private String touxiang;
    private String name;
    private String sex;
    private String shengri;
    private TextView tv_sex;
    private ImageView image_sex;
    private TextView tv_age;
    private String roleIndex;
    private XiTongXiaoXiWeiDuGeShuEntity xiTongXiaoXiWeiDuGeShuEntity;
    private TextView tv_xiaoxi_number;


    @Override
    public int getContentResId() {
        return R.layout.fragment_mine;
    }



    @Override
    public void init() {
        EventBus.getDefault().register(this);

//        roleIndex = mCache.getAsString(Constant.ROLE_INDEX);
//        if(ContractUtils.getTypeId(getContext()).equals("0")){
//            ROLE_INDEX = Integer.parseInt(roleIndex);
//        }

//        暂时 哈哈jiangtiantian 放开
        String string = "0";
        if(ContractUtils.getTypeId(getContext()).equals("0")){

        }else{
            string = ContractUtils.getTypeId(getContext());
        }


        if(ContractUtils.getTypeId22(getContext()).equals("0")){

        }else{
            string = ContractUtils.getTypeId22(getContext());
        }

//        typeId: 0 游客 1 普通员工 2企业用户 3商家 4 门卫 5 物业
        if(string.equals("0")){
            ROLE_INDEX = Constant.ROLE_TOURIST;
        }else if(string.equals("1")){
            ROLE_INDEX = Constant.ROLE_EMPLOY;
        }else if(string.equals("2")){
            ROLE_INDEX = Constant.ROLE_COMPANY;
        }else if(string.equals("3")){
            ROLE_INDEX = Constant.BTN_SHANGJIA;
        }else if(string.equals("4")){
            ROLE_INDEX = Constant.ROLE_DOOR;
        }else if(string.equals("5")){
            ROLE_INDEX = Constant.BTN_WUYE;
        }

//        System.out.println("1111111111111111111111111111111111111111111111111111111111");


//        String roleIndex = mCache.getAsString(Constant.ROLE_INDEX);
//        if (roleIndex != null) {
//            ROLE_INDEX = Integer.parseInt(roleIndex);
//        }

        initView();
        initListener();
    }



    private void initView() {
        mineHead = findViewById(R.id.mine_head);

        mineMgr1 = findViewById(R.id.mine_mgr_1);
        mineMgr2 = findViewById(R.id.mine_mgr_2);
        mineMgr3 = findViewById(R.id.mine_mgr_3);
        mineMgr = findViewById(R.id.mine_mgr);

        mineOrderAll = findViewById(R.id.mine_order_all);
        mineOrderPay = findViewById(R.id.mine_order_pay);
        mineOrderReceive = findViewById(R.id.mine_order_receive);
        mineOrderAppraise = findViewById(R.id.mine_order_appraise);
        mineOrderFinish = findViewById(R.id.mine_order_finish);
        mineDot1 = findViewById(R.id.mine_dot_1);
        mineDot2 = findViewById(R.id.mine_dot_2);
        mineDot3 = findViewById(R.id.mine_dot_3);
        mineDot4 = findViewById(R.id.mine_dot_4);
        mineOrder = findViewById(R.id.mine_order);

        mineCollect = findViewById(R.id.mine_collect);
        mineForum = findViewById(R.id.mine_forum);
        mineAddress = findViewById(R.id.mine_address);
        mineOther1 = findViewById(R.id.mine_other_1);
        mineOther2 = findViewById(R.id.mine_other_2);
        mineSet = findViewById(R.id.mine_set);
        tv_dingdan = findViewById(R.id.tv_dingdan);
        tv_shoucang = findViewById(R.id.tv_shoucang);
        view = findViewById(R.id.view);
        tv_sex = findViewById(R.id.tv_sex);
        image_sex = findViewById(R.id.image_sex);
        tv_age = findViewById(R.id.tv_age);
        tv_xiaoxi_number = findViewById(R.id.tv_xiaoxi_number);

        image_qiehuan = findViewById(R.id.image_qiehuan);
        image_xiaoxi = findViewById(R.id.image_xiaoxi);
        image_xiaoxi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent11 = new Intent(getContext(), FuWuMessageActivity.class);
                startActivity(intent11);
            }
        });



//        切换身份
        image_qiehuan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                暂时 哈哈jiangtiantian 放开
                if(ContractUtils.getTypeId(getContext()).equals("0") && ContractUtils.getTypeId22(getContext()).equals("0")){
                    Toast.makeText(getContext(), "当前您只有游客身份！", Toast.LENGTH_SHORT).show();
                    return;
                }



//                if(ContractUtils.getTypeId(getContext()).equals("0") || ContractUtils.getTypeId22(getContext()).equals("0")){
//                    Toast.makeText(getContext(), "您不能切换身份！", Toast.LENGTH_SHORT).show();
//                    return;
//                }
                BoxDialog.start(mContext);
            }
        });




//       编辑资料
        tv_yonghu = findViewById(R.id.tv_yonghu);
//        tv_yonghu.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Intent intent = new Intent(getContext(), WoDeZiLiaoActivity.class);
//                startActivity(intent);
//            }
//        });

        mine_dingwei = (TextView) findViewById(R.id.mine_dingwei);

//        默认园区
        mine_dingwei.setText(ContractUtils.getYuanquname(getContext()));

        setView();
    }


    private void setView() {
        switch (ROLE_INDEX) {
//            、游客
            case ROLE_TOURIST:
                mineMgr.setVisibility(View.GONE);
                mineOrder.setPadding(0, DensityUtil.dp2px(mContext, 30), 0, DensityUtil.dp2px(mContext, 10));
                setLeftDrawableAndText(mineOther1, R.mipmap.my_shj, "商家入驻");
                setLeftDrawableAndText(mineOther2, R.mipmap.my_bd, "绑定园区账号");

                tv_dingdan.setText("我的订单");
                setTopDrawableAndText(mineOrderPay,R.mipmap.daizhifu,"待支付");
                setTopDrawableAndText(mineOrderReceive,R.mipmap.daishouhuo,"待收货");
                setTopDrawableAndText(mineOrderAppraise,R.mipmap.daipingjia,"待评价");
                setTopDrawableAndText(mineOrderFinish,R.mipmap.yiwancheng,"已完成");

                tv_shoucang.setVisibility(View.GONE);
                view.setVisibility(View.GONE);
                setLeftDrawableAndText(mineCollect,R.mipmap.my_shoucang,"我的收藏");

                break;
//            企业
            case Constant.ROLE_COMPANY:
                mineMgr.setVisibility(View.VISIBLE);
                mineOrder.setPadding(0, DensityUtil.dp2px(mContext, 10), 0, DensityUtil.dp2px(mContext, 10));
                setLeftDrawableAndText(mineOther1, R.mipmap.my_fuwu, "服务记录");
                setLeftDrawableAndText(mineOther2, R.mipmap.my_shj, "商家入驻");
                setTopDrawableAndText(mineMgr1, R.mipmap.my_yuangongxinxi, "员工信息");
                setTopDrawableAndText(mineMgr2, R.mipmap.my_qiyeguanli, "企业管理");
                setTopDrawableAndText(mineMgr3, R.mipmap.my_yikatong, "一卡通");

                tv_dingdan.setText("我的订单");
                setTopDrawableAndText(mineOrderPay,R.mipmap.daizhifu,"待支付");
                setTopDrawableAndText(mineOrderReceive,R.mipmap.daishouhuo,"待收货");
                setTopDrawableAndText(mineOrderAppraise,R.mipmap.daipingjia,"待评价");
                setTopDrawableAndText(mineOrderFinish,R.mipmap.yiwancheng,"已完成");

                tv_shoucang.setVisibility(View.GONE);
                view.setVisibility(View.GONE);
                setLeftDrawableAndText(mineCollect,R.mipmap.my_shoucang,"我的收藏");

                break;
//            员工
            case Constant.ROLE_EMPLOY:
                mineMgr.setVisibility(View.VISIBLE);
                mineOrder.setPadding(0, DensityUtil.dp2px(mContext, 10), 0, DensityUtil.dp2px(mContext, 10));
                setLeftDrawableAndText(mineOther1, R.mipmap.my_fuwu, "服务记录");
                setLeftDrawableAndText(mineOther2, R.mipmap.my_shj, "商家入驻");
                setTopDrawableAndText(mineMgr1, R.mipmap.my_qiyeguanli, "我的宿舍");
                setTopDrawableAndText(mineMgr2, R.mipmap.my_yikatong, "一卡通");
                setTopDrawableAndText(mineMgr3, R.mipmap.my_qiyexinix, "企业信息");

                tv_dingdan.setText("我的订单");
                setTopDrawableAndText(mineOrderPay,R.mipmap.daizhifu,"待支付");
                setTopDrawableAndText(mineOrderReceive,R.mipmap.daishouhuo,"待收货");
                setTopDrawableAndText(mineOrderAppraise,R.mipmap.daipingjia,"待评价");
                setTopDrawableAndText(mineOrderFinish,R.mipmap.yiwancheng,"已完成");

                tv_shoucang.setVisibility(View.GONE);
                view.setVisibility(View.GONE);
                setLeftDrawableAndText(mineCollect,R.mipmap.my_shoucang,"我的收藏");

                break;
//            门卫
            case Constant.ROLE_DOOR:
                mineMgr.setVisibility(View.VISIBLE);
                mineOrder.setPadding(0, DensityUtil.dp2px(mContext, 10), 0, DensityUtil.dp2px(mContext, 10));
                setLeftDrawableAndText(mineOther1, R.mipmap.my_fuwu, "服务记录");
                setLeftDrawableAndText(mineOther2, R.mipmap.my_shj, "商家入驻");
                setTopDrawableAndText(mineMgr1, R.mipmap.my_qiyeguanli, "宿舍管理");
                setTopDrawableAndText(mineMgr2, R.mipmap.my_wodecheliang, "我的车辆");
                setTopDrawableAndText(mineMgr3, R.mipmap.my_fangkeguanli, "查看访客");

                tv_dingdan.setText("我的订单");
                setTopDrawableAndText(mineOrderPay,R.mipmap.daizhifu,"待支付");
                setTopDrawableAndText(mineOrderReceive,R.mipmap.daishouhuo,"待收货");
                setTopDrawableAndText(mineOrderAppraise,R.mipmap.daipingjia,"待评价");
                setTopDrawableAndText(mineOrderFinish,R.mipmap.yiwancheng,"已完成");

                tv_shoucang.setVisibility(View.GONE);
                view.setVisibility(View.GONE);
                setLeftDrawableAndText(mineCollect,R.mipmap.my_shoucang,"我的收藏");

                break;
//            物业
            case Constant.BTN_WUYE:
                mineMgr.setVisibility(View.VISIBLE);
                mineOrder.setPadding(0, DensityUtil.dp2px(mContext, 10), 0, DensityUtil.dp2px(mContext, 10));
                setLeftDrawableAndText(mineOther1, R.mipmap.my_fuwu, "服务记录");
                setLeftDrawableAndText(mineOther2, R.mipmap.my_shj, "商家入驻");
                setTopDrawableAndText(mineMgr1, R.mipmap.my_yuangongxinxi, "员工信息");
                setTopDrawableAndText(mineMgr2, R.mipmap.my_wuyeguanli, "物业管理");
                setTopDrawableAndText(mineMgr3, R.mipmap.my_yikatong, "一卡通");

                tv_dingdan.setText("我的订单");
                setTopDrawableAndText(mineOrderPay,R.mipmap.daizhifu,"待支付");
                setTopDrawableAndText(mineOrderReceive,R.mipmap.daishouhuo,"待收货");
                setTopDrawableAndText(mineOrderAppraise,R.mipmap.daipingjia,"待评价");
                setTopDrawableAndText(mineOrderFinish,R.mipmap.yiwancheng,"已完成");

                tv_shoucang.setVisibility(View.GONE);
                view.setVisibility(View.GONE);
                setLeftDrawableAndText(mineCollect,R.mipmap.my_shoucang,"我的收藏");
                break;
            //            商家
            case BTN_SHANGJIA:
                mineMgr.setVisibility(View.VISIBLE);
                mineOrder.setPadding(0, DensityUtil.dp2px(mContext, 10), 0, DensityUtil.dp2px(mContext, 10));
                setLeftDrawableAndText(mineOther1, R.mipmap.my_fuwu, "服务记录");
                setLeftDrawableAndText(mineOther2, R.mipmap.youhuiquan, "优惠券服务");
                setTopDrawableAndText(mineMgr1, R.mipmap.my_shangpinguanli, "商品管理");
                setTopDrawableAndText(mineMgr2, R.mipmap.my_youhuiquan, "优惠券");
                setTopDrawableAndText(mineMgr3, R.mipmap.my_shangjiaguanli, "商家管理");

                tv_dingdan.setText("商家订单");
                setTopDrawableAndText(mineOrderPay,R.mipmap.myshj_ddtj,"订单统计");
                setTopDrawableAndText(mineOrderReceive,R.mipmap.myshj_yjd,"已接单");
                setTopDrawableAndText(mineOrderAppraise,R.mipmap.myshj_dps,"待配送");
                setTopDrawableAndText(mineOrderFinish,R.mipmap.myshj_ywch,"已完成");

                tv_shoucang.setVisibility(View.VISIBLE);
                view.setVisibility(View.VISIBLE);
                setLeftDrawableAndText(mineCollect,R.mipmap.daizhifu,"我的订单");

                tv_shoucang.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent intentsc = new Intent(getContext(), MyShouCangActivity.class);
                        startActivity(intentsc);
                    }
                });
                break;
        }
    }

    private void initListener() {
        mineHead.setOnClickListener(this);
        mineMgr1.setOnClickListener(this);
        mineMgr2.setOnClickListener(this);
        mineMgr3.setOnClickListener(this);
        mineOrderAll.setOnClickListener(this);
        mineOrderPay.setOnClickListener(this);
        mineOrderReceive.setOnClickListener(this);
        mineOrderAppraise.setOnClickListener(this);
        mineOrderFinish.setOnClickListener(this);
        mineCollect.setOnClickListener(this);
        mineForum.setOnClickListener(this);
        mineAddress.setOnClickListener(this);
        mineOther1.setOnClickListener(this);
        mineOther2.setOnClickListener(this);
        mineSet.setOnClickListener(this);
        mine_dingwei.setOnClickListener(this);
    }

    private void setLeftDrawableAndText(TextView textView, @DrawableRes int leftResId, String title) {
        Drawable leftDrawable = getResources().getDrawable(leftResId);
        leftDrawable.setBounds(0, 0, leftDrawable.getMinimumWidth(), leftDrawable.getMinimumHeight());
        Drawable rightDrawable = getResources().getDrawable(R.mipmap.to);
        rightDrawable.setBounds(0, 0, rightDrawable.getMinimumWidth(), rightDrawable.getMinimumHeight());
        textView.setCompoundDrawables(leftDrawable, null, rightDrawable, null);
        textView.setText(title);
    }



    private void setLeftDrawableAnd6666666666(TextView textView, @DrawableRes int leftResId, String title) {
        Drawable leftDrawable = getResources().getDrawable(leftResId);
        leftDrawable.setBounds(0, 0, leftDrawable.getMinimumWidth(), leftDrawable.getMinimumHeight());
//        Drawable rightDrawable = getResources().getDrawable(R.mipmap.to);
//        rightDrawable.setBounds(0, 0, rightDrawable.getMinimumWidth(), rightDrawable.getMinimumHeight());
//        textView.setCompoundDrawables(leftDrawable, null, rightDrawable, null);
        textView.setText(title);
    }





    private void setTopDrawableAndText(TextView textView, @DrawableRes int resId, String title) {
        Drawable drawable = getResources().getDrawable(resId);
        drawable.setBounds(0, 0, drawable.getMinimumWidth(), drawable.getMinimumHeight());
        textView.setCompoundDrawables(null, drawable, null, null);
        textView.setText(title);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
//            头像
            case R.id.mine_head:
                Intent intentbianji = new Intent(getActivity(), WoDeZiLiaoActivity.class);
                startActivity(intentbianji);
                break;
            case R.id.mine_mgr_1:
                switch (ROLE_INDEX) {
//                    企业
                    case Constant.ROLE_COMPANY:
                        // 员工信息
                        Intent intent = new Intent(getContext(), MyYuanGongXinXiActivity.class);
                        startActivity(intent);
//                        EmployMgrActivity.start(mContext);
                        break;
//                    员工
                    case Constant.ROLE_EMPLOY:
                        // 我的宿舍
                        Intent intent1 = new Intent(getContext(), MySuSheActivity.class);
                        intent1.putExtra("title","yuangong");
                        startActivity(intent1);
                        break;
//                    门卫
                    case Constant.ROLE_DOOR:
                        // 宿舍管理
                        Intent intent8 = new Intent(getContext(), MySuSheActivity.class);
                        intent8.putExtra("title","menwei");
                        startActivity(intent8);

//                        Intent intent2 = new Intent(getContext(), MySuSheGuanLiActivity.class);
//                        startActivity(intent2);
//                        DormMgrActivity.start(mContext);
                        break;
//                    物业   员工信息
                    case Constant.BTN_WUYE:
                        Intent intent3 = new Intent(getContext(), WuYeYuanGongXinXiActivity.class);
                        startActivity(intent3);
                        break;
//                    商家    商品管理
                    case BTN_SHANGJIA:
                        Intent intent4 = new Intent(getContext(), ShangJiaShangPinGuanLiActivity.class);
                        startActivity(intent4);
                        break;
                }
                break;
            case R.id.mine_mgr_2:
                switch (ROLE_INDEX) {
                    case Constant.ROLE_COMPANY:
                        // 企业管理
                        Intent intent = new Intent(getContext(), MyQiYeGuanLiActivity.class);
                        startActivity(intent);
//                        CompanyMgrActivity.start(mContext);
                        break;
                    case Constant.ROLE_EMPLOY:
                        // 一卡通
                        Intent intent2 = new Intent(getContext(), YiKaTongActivity.class);
                        startActivity(intent2);
//                        OneCardActivity.start(mContext);
                        break;
//                    门卫
                    case Constant.ROLE_DOOR:
                        // 我的车辆
//                        CheLiangActivity.start(mContext);
                        Intent intent1 = new Intent(getContext(),CheLiangGuanLiActivity.class);
                        startActivity(intent1);
                        break;
                    //                    物业  物业管理
                    case Constant.BTN_WUYE:
                        Intent intent3 = new Intent(getContext(), WuYeGuanLiActivity.class);
                        startActivity(intent3);
                        break;
                    //    商家    优惠券
                    case BTN_SHANGJIA:
                        Intent intent4 = new Intent(getContext(), ShangJiaYouHuiQuanActivity.class);
                        startActivity(intent4);
                        break;
                }
                break;
            case R.id.mine_mgr_3:
                switch (ROLE_INDEX) {
//                    企业一卡通
                    case Constant.ROLE_COMPANY:
                        // 一卡通
                        Intent intent = new Intent(getContext(), YiKaTongActivity.class);
                        startActivity(intent);
//                        Intent intent = new Intent(getContext(), YiKaTongActivity.class);
//                        startActivity(intent);
//                        OneCardActivity.start(mContext);
                        break;
                    case Constant.ROLE_EMPLOY:
                        // 企业信息
                        Intent intentxinxi = new Intent(getContext(), MenWeiQiYeXinXiActivity.class);
                        startActivity(intentxinxi);
//                        CompanyDetailActivity.start(mContext);
                        break;
                    case Constant.ROLE_DOOR:
                        // 访客管理
                        Intent intent1 = new Intent(getContext(), MyMenWeiFangKeGuanLiActivity.class);
//                        Intent intent1 = new Intent(getContext(), MyFangKeGuanLiActivity.class);
                        startActivity(intent1);
//                        FangKeActivity.start(mContext);
                        break;
                    //      物业  一卡通
                    case Constant.BTN_WUYE:
                        Intent intent2 = new Intent(getContext(), YiKaTongActivity.class);
                        startActivity(intent2);
                        break;
                    //   商家  商家管理
                    case BTN_SHANGJIA:
                        Intent intent3 = new Intent(getContext(), ShangJiaGuanLiActivity.class);
                        startActivity(intent3);
                        break;
                }
                break;
//            查看全部订单
            case R.id.mine_order_all:
                if(ROLE_INDEX == BTN_SHANGJIA){
                    Intent intent = new Intent(getContext(), ShangJiaDingDanActivity.class);
                    intent.putExtra("shangjiadingdan","0");
                    startActivity(intent);
                }else{
                    Intent intent = new Intent(getContext(),QuanBuActivity.class);
                    intent.putExtra("daizhifu","0");
                    startActivity(intent);
                }

//                QuanBuActivity.start(mContext, Constant.ORDER_ALL);
                break;
//            待支付
            case R.id.mine_order_pay:
                switch (ROLE_INDEX) {
                    //                    游客
                    case Constant.ROLE_TOURIST:
                        Intent intenty = new Intent(getContext(),QuanBuActivity.class);
                        intenty.putExtra("daizhifu","1");
                        startActivity(intenty);
//                        QuanBuActivity.start(mContext, Constant.ORDER_RECEIVE);
                        break;
//                    企业
                    case Constant.ROLE_COMPANY:
                        Intent intentq = new Intent(getContext(),QuanBuActivity.class);
                        intentq.putExtra("daizhifu","1");
                        startActivity(intentq);
//                        mineDot1.setVisibility(View.GONE);
//                        QuanBuActivity.start(mContext, Constant.ORDER_PAY);
                        break;
//                    员工
                    case Constant.ROLE_EMPLOY:
                        Intent intentyg = new Intent(getContext(),QuanBuActivity.class);
                        intentyg.putExtra("daizhifu","1");
                        startActivity(intentyg);
//                        mineDot1.setVisibility(View.GONE);
//                        QuanBuActivity.start(mContext, Constant.ORDER_PAY);
                        break;
//                    门卫
                    case Constant.ROLE_DOOR:
                        Intent intentm = new Intent(getContext(),QuanBuActivity.class);
                        intentm.putExtra("daizhifu","1");
                        startActivity(intentm);
//                        mineDot1.setVisibility(View.GONE);
//                        QuanBuActivity.start(mContext, Constant.ORDER_PAY);
                        break;
//                    物业   员工信息
                    case Constant.BTN_WUYE:
                        Intent intentmwy = new Intent(getContext(),QuanBuActivity.class);
                        intentmwy.putExtra("daizhifu","1");
                        startActivity(intentmwy);
//                        mineDot1.setVisibility(View.GONE);
//                        QuanBuActivity.start(mContext, Constant.ORDER_PAY);
                        break;
//                    商家
                    case BTN_SHANGJIA:// 跳转订单统计
                        Intent intent1 = new Intent(getContext(), DingDanTongJiActivity.class);
                        startActivity(intent1);
                        break;
                }
//                mineDot1.setVisibility(View.GONE);
//                QuanBuActivity.start(mContext, Constant.ORDER_PAY);
                break;
//            待收货
            case R.id.mine_order_receive:
                switch (ROLE_INDEX) {
                    //                    游客
                    case Constant.ROLE_TOURIST:
                        Intent intent2 = new Intent(getContext(),QuanBuActivity.class);
                        intent2.putExtra("daizhifu","2");
                        startActivity(intent2);
//                        QuanBuActivity.start(mContext, Constant.ORDER_RECEIVE);
                        break;
//                    企业
                    case Constant.ROLE_COMPANY:
                        Intent intentq = new Intent(getContext(),QuanBuActivity.class);
                        intentq.putExtra("daizhifu","2");
                        startActivity(intentq);
//                        QuanBuActivity.start(mContext, Constant.ORDER_RECEIVE);
                        break;
//                    员工
                    case Constant.ROLE_EMPLOY:
                        Intent intenty = new Intent(getContext(),QuanBuActivity.class);
                        intenty.putExtra("daizhifu","2");
                        startActivity(intenty);
//                        QuanBuActivity.start(mContext, Constant.ORDER_RECEIVE);
                        break;
//                    门卫
                    case Constant.ROLE_DOOR:
                        Intent intentm = new Intent(getContext(),QuanBuActivity.class);
                        intentm.putExtra("daizhifu","2");
                        startActivity(intentm);
//                        QuanBuActivity.start(mContext, Constant.ORDER_RECEIVE);
                        break;
//                    物业   员工信息
                    case Constant.BTN_WUYE:
                        Intent intentw = new Intent(getContext(),QuanBuActivity.class);
                        intentw.putExtra("daizhifu","2");
                        startActivity(intentw);
//                        QuanBuActivity.start(mContext, Constant.ORDER_RECEIVE);
                        break;
//                    商家
                    case BTN_SHANGJIA:
                        Intent intent = new Intent(getContext(), ShangJiaDingDanActivity.class);
                        intent.putExtra("shangjiadingdan","0");
                        startActivity(intent);
                        break;
                }
//                QuanBuActivity.start(mContext, Constant.ORDER_RECEIVE);
                break;
//            待评价
            case R.id.mine_order_appraise:
                switch (ROLE_INDEX) {
                    //                    游客
                    case Constant.ROLE_TOURIST:
                        Intent intent3 = new Intent(getContext(),QuanBuActivity.class);
                        intent3.putExtra("daizhifu","3");
                        startActivity(intent3);
//                        QuanBuActivity.start(mContext, Constant.ORDER_RECEIVE);
                        break;
//                    企业
                    case Constant.ROLE_COMPANY:
                        Intent intentq = new Intent(getContext(),QuanBuActivity.class);
                        intentq.putExtra("daizhifu","3");
                        startActivity(intentq);
//                        QuanBuActivity.start(mContext, Constant.ORDER_RECEIVE);
                        break;
//                    员工
                    case Constant.ROLE_EMPLOY:
                        Intent intenty = new Intent(getContext(),QuanBuActivity.class);
                        intenty.putExtra("daizhifu","3");
                        startActivity(intenty);
//                        QuanBuActivity.start(mContext, Constant.ORDER_RECEIVE);
                        break;
//                    门卫
                    case Constant.ROLE_DOOR:
                        Intent intentm = new Intent(getContext(),QuanBuActivity.class);
                        intentm.putExtra("daizhifu","3");
                        startActivity(intentm);
//                        QuanBuActivity.start(mContext, Constant.ORDER_APPRAISE);
                        break;
//                    物业   员工信息
                    case Constant.BTN_WUYE:
                        Intent intentw = new Intent(getContext(),QuanBuActivity.class);
                        intentw.putExtra("daizhifu","3");
                        startActivity(intentw);
//                        QuanBuActivity.start(mContext, Constant.ORDER_APPRAISE);
                        break;
//                    商家
                    case BTN_SHANGJIA:
                        Intent intent = new Intent(getContext(), ShangJiaDingDanActivity.class);
                        intent.putExtra("shangjiadingdan","1");
                        startActivity(intent);
                        break;
                }
//                QuanBuActivity.start(mContext, Constant.ORDER_APPRAISE);
                break;
//            已完成
            case R.id.mine_order_finish:
                switch (ROLE_INDEX) {
//                    游客
                    case Constant.ROLE_TOURIST:
                        Intent intent4 = new Intent(getContext(),QuanBuActivity.class);
                        intent4.putExtra("daizhifu","4");
                        startActivity(intent4);
//                        QuanBuActivity.start(mContext, Constant.ORDER_RECEIVE);
                        break;
//                    企业
                    case Constant.ROLE_COMPANY:
                        Intent intentq = new Intent(getContext(),QuanBuActivity.class);
                        intentq.putExtra("daizhifu","4");
                        startActivity(intentq);
//                        QuanBuActivity.start(mContext, Constant.ORDER_RECEIVE);
                        break;
//                    员工
                    case Constant.ROLE_EMPLOY:
                        Intent intenty = new Intent(getContext(),QuanBuActivity.class);
                        intenty.putExtra("daizhifu","4");
                        startActivity(intenty);
//                        QuanBuActivity.start(mContext, Constant.ORDER_RECEIVE);
                        break;
//                    门卫
                    case Constant.ROLE_DOOR:
                        Intent intentm = new Intent(getContext(),QuanBuActivity.class);
                        intentm.putExtra("daizhifu","4");
                        startActivity(intentm);
//                        QuanBuActivity.start(mContext, Constant.ORDER_APPRAISE);
                        break;
//                    物业   员工信息
                    case Constant.BTN_WUYE:
                        Intent intentw = new Intent(getContext(),QuanBuActivity.class);
                        intentw.putExtra("daizhifu","4");
                        startActivity(intentw);
//                        QuanBuActivity.start(mContext, Constant.ORDER_APPRAISE);
                        break;
//                    商家
                    case BTN_SHANGJIA:
                        Intent intent = new Intent(getContext(), ShangJiaDingDanActivity.class);
                        intent.putExtra("shangjiadingdan","2");
                        startActivity(intent);
                        break;
                }
//                QuanBuActivity.start(mContext, Constant.ORDER_FINISH);
                break;
//            我的收藏
            case R.id.mine_collect:
                if(ROLE_INDEX == BTN_SHANGJIA){
                    Intent intentdingdan = new Intent(getContext(), QuanBuActivity.class);
                    intentdingdan.putExtra("daizhifu","0");
                    startActivity(intentdingdan);
                }else{
                    Intent intents = new Intent(getContext(), MyShouCangActivity.class);
                    intents.putExtra("shoucang","shoucang");
                    startActivity(intents);
                }
//                CollectActivity.start(mContext);
                break;
//            我的论坛
            case R.id.mine_forum:
                Intent intent2 = new Intent(getContext(), MyLunTanActivity.class);
                startActivity(intent2);
//                ForumActivity.start(mContext);
                break;
//            我的地址
            case R.id.mine_address:
                Intent intent1 = new Intent(getContext(), ShouHuoXinXiActivity.class);
                startActivity(intent1);
//                Intent intent = new Intent(getContext(),AddressActivity.class);
//                startActivity(intent);
//                AddressActivity.start(mContext);
                break;
//            商家入驻
            case R.id.mine_other_1:
                if (ROLE_INDEX == ROLE_TOURIST) {
//                    商家入驻
                    Intent intent3 = new Intent(getContext(), RuZhuShenQingActivity.class);
                    startActivity(intent3);
//                    CompanyJoinActivity.start(mContext);
                } else {
//                  服务记录
                    Intent intent4 = new Intent(getContext(), FuWuShenQingJiLuActivity.class);
                    intent4.putExtra("wode","wode");
                    startActivity(intent4);
                }
                break;
//            绑定园区账号
            case R.id.mine_other_2:
                if (ROLE_INDEX == ROLE_TOURIST) {
                    // 绑定园区账号
                    Intent intent3 = new Intent(getContext(), MyBangDingActivity.class);
//                    Intent intent3 = new Intent(getContext(), QieHuanYuanQuActivity.class);
                    startActivity(intent3);
                }else if( ROLE_INDEX == BTN_SHANGJIA){
//                    优惠券服务
                    Intent intent = new Intent(getContext(), YouHuiQuanFuWuActivity.class);
                    startActivity(intent);
                } else {
//                    商家入驻
                    Intent intent3 = new Intent(getContext(), RuZhuShenQingActivity.class);
                    startActivity(intent3);
//                    CompanyJoinActivity.start(mContext);
                }
                break;
//            设置
            case R.id.mine_set:
                Intent intent = new Intent(getContext(),SettingActivity.class);
                startActivity(intent);
//                SettingActivity.start(mContext);
                break;
//            定位
            case R.id.mine_dingwei:
                Intent intent11 = new Intent(getContext(), QieHuanYuanQuActivity.class);
                intent11.putExtra("biaoti","2");
                startActivity(intent11);
//                startActivity(new Intent(getActivity(), ChangeParkActivity.class));


//                Intent intentq = new Intent(getContext(), XuanZeYuanQuActivity.class);
//                startActivity(intentq);
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN, priority = 100)
    public void onEvent(EventModel eventModel) {
        if (eventModel != null && (Constant.ROLE_INDEX).equals(eventModel.getString())) {
            ROLE_INDEX = eventModel.getInteger();


//            暂时 哈哈jiangtiantian 放开
//              typeId: 0 游客 1 普通员工 2企业用户 3商家 4 门卫 5 物业
//            if(ROLE_INDEX == 0){
//                ContractUtils.setTypeId("0");
//            }else if(ROLE_INDEX == 1){
//                ContractUtils.setTypeId("2");
//            }else if(ROLE_INDEX == 2){
//                ContractUtils.setTypeId("1");
//            }else if(ROLE_INDEX == 3){
//                ContractUtils.setTypeId("4");
//            }else if(ROLE_INDEX == 4){
//                ContractUtils.setTypeId("3");
//            }else if(ROLE_INDEX == 5){
//                ContractUtils.setTypeId("5");
//            }
            setView();

        }
    }


//    @Override
//    public void onHiddenChanged(boolean hidden) {
//        super.onHiddenChanged(hidden);
//
//
//        System.out.println(ContractUtils.getTypeId(getContext())+"            id1111111111111");
//        System.out.println(ContractUtils.getTypeId22(getContext())+"            id222222222");
//
//
//
//
//
//
//
//    }
//
//
//

    @Override
    public void onResume() {
        super.onResume();
        onEvent(new EventModel());


        System.out.println(ContractUtils.getTypeId(getContext())+"            id13333");
        System.out.println(ContractUtils.getTypeId22(getContext())+"            id44444444");







//        暂时 哈哈jiangtiantian 放开
        String string = "0";
        String string22 = "0";
        if(ContractUtils.getTypeId(getContext()).equals("0")){

        }else{
            string22 = ContractUtils.getTypeId(getContext());
        }


        if(ContractUtils.getTypeId22(getContext()).equals("0")){

        }else{
            string22 = ContractUtils.getTypeId22(getContext());
        }

//        typeId: 0 游客 1 普通员工 2企业用户 3商家 4 门卫 5 物业
        if( ROLE_INDEX == Constant.ROLE_TOURIST){
            string = "0";
        }else if(ROLE_INDEX == Constant.ROLE_EMPLOY){
            string = "1";
        }else if(ROLE_INDEX == Constant.ROLE_COMPANY){
            string = "2";
        }else if(ROLE_INDEX == Constant.BTN_SHANGJIA){
            string = "3";
        }else if( ROLE_INDEX == Constant.ROLE_DOOR){
            string = "4";
        }else if( ROLE_INDEX == Constant.BTN_WUYE){
            string = "5";
        }

        if(string.equals(ContractUtils.getTypeId(getContext())) || string.equals(ContractUtils.getTypeId22(getContext()))){

        }else{
            // typeId: 0 游客 1 普通员工 2企业用户 3商家 4 门卫 5 物业
            if(string22.equals("0")){
                ROLE_INDEX = Constant.ROLE_TOURIST;
            }else if(string22.equals("1")){
                ROLE_INDEX = Constant.ROLE_EMPLOY;
            }else if(string22.equals("2")){
                ROLE_INDEX = Constant.ROLE_COMPANY;
            }else if(string22.equals("3")){
                ROLE_INDEX = Constant.BTN_SHANGJIA;
            }else if(string22.equals("4")){
                ROLE_INDEX = Constant.ROLE_DOOR;
            }else if(string22.equals("5")){
                ROLE_INDEX = Constant.BTN_WUYE;
            }


        }





//        获取用户信息的网络请求
        initYongHuiXinXiHttp();

//        设置园区接口
        initSheZhiYuanQuHttp();

        //   用户未读消息的个数的网络请求
        initWeiDuXiaoXiHttp();

//        IntentFilter intentFilter = new IntentFilter();
//        intentFilter.addAction("xiugai");
//        netWork = new NetWork();
//        getContext().registerReceiver(netWork, intentFilter);


        SharedPreferences sharedPreferences = getContext().getSharedPreferences("yuanqu", Context.MODE_PRIVATE);
        yuanqu = sharedPreferences.getString("yuanqu", "");
        if (yuanqu.equals("")) {

        } else {
            mine_dingwei.setText(yuanqu);
            System.out.println(yuanqu + "    选择的园区名称");
        }


//            暂时 哈哈jiangtiantian 放开
//        typeId: 0 游客 1 普通员工 2企业用户 3商家 4 门卫 5 物业

//        if (ContractUtils.getTypeId(getContext()).equals("")) {
//            ROLE_INDEX = 0;
//        } else {
//            if (ContractUtils.getTypeId(getContext()).equals("0")) {
////                restart(Constant.ROLE_TOURIST);
//                ROLE_INDEX = 0;
//            } else if (ContractUtils.getTypeId(getContext()).equals("1")) {
//                ROLE_INDEX = 2;
////                restart(Constant.ROLE_EMPLOY);
//            } else if (ContractUtils.getTypeId(getContext()).equals("2")) {
//                ROLE_INDEX = 1;
////                restart(Constant.ROLE_COMPANY);
//            } else if (ContractUtils.getTypeId(getContext()).equals("3")) {
//                ROLE_INDEX = 4;
////                restart(Constant.BTN_SHANGJIA);
//            } else if (ContractUtils.getTypeId(getContext()).equals("4")) {
//                ROLE_INDEX = 3;
////                restart(Constant.ROLE_DOOR);
//            } else if (ContractUtils.getTypeId(getContext()).equals("5")) {
//                ROLE_INDEX = 5;
////                restart(Constant.BTN_WUYE);
//            }
            setView();
//        }
    }




    //    设置园区接口
    private void initSheZhiYuanQuHttp() {
        OkHttpUtils.post()
                .url(ContractUtils.LOGIN_URL+"user/setDefaultPark")
                .addHeader("token",ContractUtils.getTOKEN(getContext()))
                .addParams("parkId",ContractUtils.getParkId(getContext()))
                .build()
                .execute(new StringCallback() {
                    @Override
                    public void onError(Call call, Exception e, int id) {

                    }

                    @Override
                    public void onResponse(String response, int id) {
                        System.out.println(response+"               设置园区接口");
                        if(response.indexOf("200") != -1){
                            Gson gson = new Gson();
                            SheZhiYuanQuNameEntity sheZhiYuanQuNameEntity = gson.fromJson(response, SheZhiYuanQuNameEntity.class);
                            ContractUtils.setParkId(sheZhiYuanQuNameEntity.getResponse().getParkInfo().getParkId());
                            ContractUtils.setPhone(sheZhiYuanQuNameEntity.getResponse().getUsername());
                            ContractUtils.setYuanquname(sheZhiYuanQuNameEntity.getResponse().getParkInfo().getParkName());
                            ContractUtils.setTypeId(sheZhiYuanQuNameEntity.getResponse().getTypeId());
                            ContractUtils.setTypeId22(sheZhiYuanQuNameEntity.getResponse().getTypeId2());
                        }
                    }
                });
    }







//        switch (view.getId()) {
////            游客
//            case R.id.role_tourist:
//                restart(Constant.ROLE_TOURIST);
//                break;
////            企业
//            case R.id.role_company:
//                restart(Constant.ROLE_COMPANY);
//                break;
////            员工
//            case R.id.role_employ:
//                restart(Constant.ROLE_EMPLOY);
//                break;
////            门卫
//            case R.id.role_door:
//                restart(Constant.ROLE_DOOR);
//                break;
////            商家
//            case R.id.btn_shangjia:
//                restart(Constant.BTN_SHANGJIA);
//                break;
////            物业
//            case R.id.btn_wuye:
//                restart(Constant.BTN_WUYE);
//                break;
//        }
//        }







    //    用户未读消息的个数的网络请求
    private void initWeiDuXiaoXiHttp() {
        OkHttpUtils.post()
                .url(ContractUtils.LOGIN_URL + "life/messagege")
                .addHeader("token", ContractUtils.getTOKEN(getContext()))
                .addParams("parkId", ContractUtils.getParkId(getContext()))
                .build()
                .execute(new StringCallback() {
                    @Override
                    public void onError(Call call, Exception e, int id) {

                    }

                    @Override
                    public void onResponse(String response, int id) {
                        ContractUtils.Code500(getContext(),response);
                        System.out.println(response + "         未读消息个数的网络请求");

                        Integer number = 0;
                        if (response.indexOf("200") != -1) {
                            Gson gson = new Gson();
                            xiTongXiaoXiWeiDuGeShuEntity = gson.fromJson(response, XiTongXiaoXiWeiDuGeShuEntity.class);
                            number = Integer.parseInt(xiTongXiaoXiWeiDuGeShuEntity.getResponse().getGe());
                            if (number > 0) {
                                tv_xiaoxi_number.setVisibility(View.VISIBLE);
                                tv_xiaoxi_number.setText(xiTongXiaoXiWeiDuGeShuEntity.getResponse().getGe());
                            } else if (number <= 0) {
                                tv_xiaoxi_number.setVisibility(View.GONE);
                            }
                        }
                    }
                });
    }






    private void restart(int roleIndex) {
        EventModel eventModel = new EventModel();
        eventModel.setInteger(roleIndex);
        eventModel.setString(Constant.ROLE_INDEX);
        EventBus.getDefault().post(eventModel);
        mCache.put(Constant.ROLE_INDEX, String.valueOf(roleIndex));
        getActivity().finish();
    }


//        获取用户信息的网络请求
    private void initYongHuiXinXiHttp() {
        OkHttpUtils.post()
                .url(ContractUtils.LOGIN_URL+"Life/user")
                .addHeader("token",ContractUtils.getTOKEN(getContext()))
                .build()
                .execute(new StringCallback() {
                    @Override
                    public void onError(Call call, Exception e, int id) {

                    }

                    @Override
                    public void onResponse(String response, int id) {
                        System.out.println(response+"          获取用户信息的网络请求我的页面 ");
                        ContractUtils.Code500(getContext(),response);
                        if(response.indexOf("200") != -1){
                            Gson gson = new Gson();
                            UserEntity userEntity = gson.fromJson(response, UserEntity.class);
                            if(userEntity.getResponse().getNickname() == null){

                            }else{
                                tv_yonghu.setText(userEntity.getResponse().getNickname());
                            }



                            if(userEntity.getResponse().getAvatar() == null){

                            }else{
                                Glide.with(getContext()).load(userEntity.getResponse().getAvatar()).into(mineHead);
                            }


                            if(userEntity.getResponse().getAge() == null){

                            }else{
                                tv_age.setText(userEntity.getResponse().getAge());
                            }



                            if(userEntity.getResponse().getSex() == null){

                            }else{
                                if(userEntity.getResponse().getSex().equals("0")){
                                    image_sex.setImageResource(R.mipmap.nan);
//                                    setLeftDrawableAnd6666666666(tv_sex,R.mipmap.nan,userEntity.getResponse().getAge());
                                }else if(userEntity.getResponse().getSex().equals("1")){
                                    image_sex.setImageResource(R.mipmap.nv);
//                                    setLeftDrawableAnd6666666666(tv_sex,R.mipmap.nv,userEntity.getResponse().getAge());
                                }
                            }
                        }
                    }
                });
    }


//    class NetWork extends BroadcastReceiver {
//        @Override
//        public void onReceive(Context context, Intent intent) {
//            touxiang = intent.getStringExtra("touxiang");
//            name = intent.getStringExtra("name");
//            sex = intent.getStringExtra("sex");
//            shengri = intent.getStringExtra("shengri");
////            Toast.makeText(context, wxopenid + wxnickname + wxheadimgurl, Toast.LENGTH_SHORT).show();
//
//            if(touxiang!= null){
//                Glide.with(context).load(touxiang).into(mineHead);
//            }
//
//            if(name != null){
//                tv_yonghu.setText(name);
//            }
//
//
//            if(sex != null){
//                if(sex.equals("0")){
//                    setLeftDrawableAndText(tv_sex,R.mipmap.nan,"男");
//                }else if(sex.equals("1")){
//                    setLeftDrawableAndText(tv_sex,R.mipmap.nv,"女");
//                }
//            }
//
//
//        }
//    }



    @Override
    public void onDestroyView() {
        super.onDestroyView();
        EventBus.getDefault().unregister(this);
    }
}