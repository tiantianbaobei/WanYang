package com.wanyangproject.myadapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.wanyangproject.R;
import com.wanyangproject.entity.DingDanXiangQingEntity;
import com.wanyangproject.my.DaiZhiFuXiangQingActivity;
import com.wanyangproject.utils.ContractUtils;

import java.util.List;

/**
 * Created by 甜甜 on 2018/7/30.
 */

public class DaiZhiFuXiangQingAdapter extends RecyclerView.Adapter<DaiZhiFuXiangQingAdapter.ViewHolder>{

    private Context context;
    private List<DingDanXiangQingEntity.ResponseBean.GoodsBean> goods;

    public DaiZhiFuXiangQingAdapter(Context context, List<DingDanXiangQingEntity.ResponseBean.GoodsBean> goods) {
        this.context = context;
        this.goods = goods;
    }

    @Override
    public DaiZhiFuXiangQingAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_daizhifu_xiangqing,parent,false);
        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(DaiZhiFuXiangQingAdapter.ViewHolder holder, int position) {
        if(ContractUtils.PHOTO_URL+goods.get(position).getMaster() == null){

        }else{
            Glide.with(context).load(ContractUtils.PHOTO_URL+goods.get(position).getMaster()).into(holder.image_touxiang);
        }

        if(goods.get(position).getNum() == null){

        }else{
            holder.tv_number.setText("x"+goods.get(position).getNum());
        }


        if(goods.get(position).getGoods_name() == null){

        }else{
            holder.tv_name.setText(goods.get(position).getGoods_name());
        }


        if(goods.get(position).getPic() == null){

        }else{
            holder.tv_money.setText("¥"+goods.get(position).getPic());
        }

    }

    @Override
    public int getItemCount() {
        return goods.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private ImageView image_touxiang;
        private TextView tv_name,tv_money,tv_number;
        private View view;
        public ViewHolder(View itemView) {
            super(itemView);
            view = itemView;
            image_touxiang = itemView.findViewById(R.id.image_touxiang);
            tv_name = itemView.findViewById(R.id.tv_name);
            tv_money = itemView.findViewById(R.id.tv_money);
            tv_number = itemView.findViewById(R.id.tv_number);
        }
    }
}
