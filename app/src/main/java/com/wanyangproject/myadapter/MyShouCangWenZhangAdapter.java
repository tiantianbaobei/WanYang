package com.wanyangproject.myadapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.wanyangproject.R;
import com.wanyangproject.entity.MyShouCangWenZhangEntity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * Created by 甜甜 on 2018/7/31.
 */

public class MyShouCangWenZhangAdapter extends RecyclerView.Adapter<MyShouCangWenZhangAdapter.ViewHolder>{

    private Context context;
    private JSONArray response;
    private String id;
    private String typeId;

    public MyShouCangWenZhangAdapter(Context context, JSONArray response) {
        this.context = context;
        this.response = response;
    }


//    收藏文章删除
    private ShanChuClick shanChuClick;

    public void setShanChuClick(ShanChuClick shanChuClick) {
        this.shanChuClick = shanChuClick;
    }

    public interface ShanChuClick{
        void shanchuClick(int position,String id);
}





//        点击进入详情
    private XiagQingClick xiagQingClick;

    public void setXiagQingClick(XiagQingClick xiagQingClick) {
        this.xiagQingClick = xiagQingClick;
    }

    public interface XiagQingClick{
        void xiangqingClick(int position,String id,String typeId);
    }





    @Override
    public MyShouCangWenZhangAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_wenzhang,parent,false);
        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final MyShouCangWenZhangAdapter.ViewHolder holder, final int position) {


        try {
            JSONObject jsonObject = (JSONObject) response.get(position);
            String title = jsonObject.getString("title");
            String add_time = jsonObject.getString("add_time");
            String desc = jsonObject.getString("desc");
            id = jsonObject.getString("0");
            typeId = jsonObject.getString("typeId");




            if(desc != null){
                String replace1 = desc.replace("&nbsp;", " ");
                holder.tv_neirong.setText(replace1);
            }


            if(add_time != null){
                holder.tv_time.setText(stampToDate(add_time));
            }

            if(title != null){
                if(title.indexOf("手册:")  != -1){
                    String substring = title.substring(3);

                    holder.tv_title.setText(substring);
                    System.out.println(substring+"           substring");
                }else{
                    holder.tv_title.setText(title);
                }
            }




        } catch (JSONException e) {
            e.printStackTrace();
        }




        //        删除
        holder.tv_delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                try {
                    JSONObject jsonObject = (JSONObject) response.get(position);
//                    String idd = jsonObject.getString("0");
                    String idd = jsonObject.getString("id");
                    System.out.println(idd+"          iddiddiddiddiddiddiddiddiddv");
                    if(shanChuClick != null){
                        shanChuClick.shanchuClick(holder.getAdapterPosition(),idd);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        });



        holder.linear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
//                    JSONObject jsonObject = (JSONObject) response.get(position);
//                    String idd1 = jsonObject.getString("0");
//                    String title = jsonObject.getString("title");
//                    System.out.println(idd1+"          111111111iddiddiddiddiddiddiddiddiddv");
//                    if(title.equals("没有标题")){
//                        typeId = "0";
//                    }
                    if(xiagQingClick != null){
                        JSONObject jsonObject = (JSONObject) response.get(position);
                        String idd1 = jsonObject.getString("0");
                        String title = jsonObject.getString("title");
                        System.out.println(idd1+"          111111111iddiddiddiddiddiddiddiddiddv");
                        if(title.indexOf("手册:") != -1){
                            typeId = "0";
                        }
                        xiagQingClick.xiangqingClick(holder.getAdapterPosition(),idd1,typeId);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        });





//        if(response.get(position).getTitle() == null){
//
//        }else{
//            holder.tv_title.setText(response.get(position).getTitle());
//        }


//        if(response.get(position).getAdd_time() == null){
//
//        }else{
//            holder.tv_time.setText(stampToDate(response.get(position).getAdd_time()));
//        }
//
//
//        if(response.get(position).getDesc() == null){
//
//        }else{
//            holder.tv_neirong.setText(response.get(position).getDesc());
//        }


    }


    /*
* 将时间戳转换为时间
 */
    public String stampToDate(String s) {
        if(s == null){
            return "";
        }

        if(s.length() == 0){
            return "";
        }

        if(s.indexOf("-") != -1){
            return "";
        }
        String res;
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm");
        long lt = new Long(s);
        Date date = new Date(lt*1000);
        res = simpleDateFormat.format(date);
        return res;
    }


    @Override
    public int getItemCount() {
        if(response == null){
            return 0;
        }
        return response.length();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView tv_title,tv_time,tv_neirong,tv_delete;
        private LinearLayout linear;
        private View view;
        public ViewHolder(View itemView) {
            super(itemView);
            view = itemView;
            tv_title = itemView.findViewById(R.id.tv_title);
            tv_time = itemView.findViewById(R.id.tv_time);
            tv_neirong = itemView.findViewById(R.id.tv_neirong);
            tv_delete = itemView.findViewById(R.id.tv_delete);
            linear = itemView.findViewById(R.id.linear);
        }
    }
}
