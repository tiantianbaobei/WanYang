package com.wanyangproject.myadapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.wanyangproject.R;
import com.wanyangproject.entity.QiYeChangFangGuanLiEntity;
import com.wanyangproject.my.MyChangFangGuanLiActivity;

import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by 甜甜 on 2018/8/1.
 */

public class ChangFangGuanLiAdapter extends RecyclerView.Adapter<ChangFangGuanLiAdapter.ViewHolder>{

    private Context context;
    private List<QiYeChangFangGuanLiEntity.ResponseBean.DataBean> data;
    private String dikuai = "";
    private String fangjian = "";
    private String louhao = "";


    public ChangFangGuanLiAdapter(Context context, List<QiYeChangFangGuanLiEntity.ResponseBean.DataBean> data) {
        this.context = context;
        this.data = data;
    }


//    点击查看详情
    private XiangQingClick xiangQingClick;

    public void setXiangQingClick(XiangQingClick xiangQingClick) {
        this.xiangQingClick = xiangQingClick;
    }

    public interface XiangQingClick{
        void xiangqingClick(int position,String id,String dikuai,String louhao,String fangjianhao);
}

    @Override
    public ChangFangGuanLiAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_changfang,parent,false);
        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }





//    **
//            * 判断字符串是否全部为中文字符组成
//	 * @param str	检测的文字
//	 * @return	true：为中文字符串，false:含有非中文字符
//	 */
    public static boolean isChineseStr(String str){
        Pattern pattern = Pattern.compile("[\u4e00-\u9fa5]");
        char c[] = str.toCharArray();
        for(int i=0;i<c.length;i++){
            Matcher matcher = pattern.matcher(String.valueOf(c[i]));
            if(!matcher.matches()){
                return false;
            }
        }
        return true;
    }




    @Override
    public void onBindViewHolder(final ChangFangGuanLiAdapter.ViewHolder holder, final int position) {


        String title = data.get(position).getWorkshopName();

        String[] split = title.split("#");
        if(split.length >= 1){
            String sring = split[0];
            String[] split22 = sring.split("-");

            if(split22.length >= 1){
                String s = split22[0];
                dikuai = s;
//                for (int i = 0; i < s.length(); i++) {
//                    String substring = s.substring(i, i + 1);
////                    isChineseStr(substring);
//                    if(isChineseStr(substring) == false){
//                        dikuai = dikuai +substring;
//
//                    }
//                }
            }

        }




        if(split.length >= 2){
            String sss = split[1];
            String[] split1 = sss.split("-");
            System.out.println(sss+"      sssssssssss");
            System.out.println(split1+"       111111111111");
            if(split1.length >= 1){
                louhao = split1[0];
            }

            if(split1.length>=2){
                fangjian = split1[1];
            }
        }

        System.out.println(dikuai+"     地块");
        System.out.println(fangjian+"       房间111111");
        System.out.println(louhao+"       楼号");





















        if(data.get(position).getWorkshopName() == null){

        }else{
//            holder.tv_name.setText(dikuai+"地块"+louhao+"号楼"+fangjian+"室");
            holder.tv_name.setText(data.get(position).getWorkshopName());
        }


        holder.relative.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(xiangQingClick != null){
                    String title = data.get(position).getWorkshopName();
                    String[] split = title.split("#");
                    if(split.length >= 1){
                        String sring = split[0];
                        String[] split22 = sring.split("-");

                        if(split22.length >= 1){
                            String s = split22[0];
                            dikuai = s;
//                for (int i = 0; i < s.length(); i++) {
//                    String substring = s.substring(i, i + 1);
////                    isChineseStr(substring);
//                    if(isChineseStr(substring) == false){
//                        dikuai = dikuai +substring;
//
//                    }
//                }
                        }

                    }




                    if(split.length >= 2){
                        String sss = split[1];
                        String[] split1 = sss.split("-");
                        System.out.println(sss+"      sssssssssss");
                        System.out.println(split1+"       111111111111");
                        if(split1.length >= 1){
                            louhao = split1[0];
                        }

                        if(split1.length>=2){
                            fangjian = split1[1];
                        }
                    }

                    System.out.println(dikuai+"     地块");
                    System.out.println(fangjian+"       房间111111");
                    System.out.println(louhao+"       楼号");


                    System.out.println(fangjian+"      点击房间号");
                    System.out.println(data.get(position).getWorkshopId()+"      厂房下标");
                    xiangQingClick.xiangqingClick(holder.getAdapterPosition(),data.get(position).getWorkshopId(),dikuai,louhao,fangjian);
                }
            }
        });


//        holder.relative.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                if(xiangQingClick != null){
//                    xiangQingClick.xiangqingClick(holder.getAdapterPosition(),data.get(position).getWorkshopId(),dikuai,louhao,fangjian);
//                    System.out.println(fangjian+"      点击房间号");
//                }
//            }
//        });
//


        if(data.get(position).getParcelName() != null){
            holder.tv_dikuai.setText("地块："+data.get(position).getParcelName()+"地块");
        }


        if(data.get(position).getParkName() != null){
            holder.tv_yuanqu_name.setText("园区："+data.get(position).getParkName());
        }

    }

    @Override
    public int getItemCount() {
        if(data == null){
            return 0;
        }
        return data.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView tv_name,tv_yuanqu_name,tv_dikuai;
        private RelativeLayout relative;
        private View view;
        public ViewHolder(View itemView) {
            super(itemView);
            view = itemView;
            tv_name = itemView.findViewById(R.id.tv_name);
            tv_yuanqu_name = itemView.findViewById(R.id.tv_yuanqu_name);
            tv_dikuai = itemView.findViewById(R.id.tv_dikuai);
            relative = itemView.findViewById(R.id.relative);
        }
    }
}
