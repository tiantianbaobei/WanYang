package com.wanyangproject.myadapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.wanyangproject.R;
import com.wanyangproject.adapter.DingDanDaiShouHuoAdapter;
import com.wanyangproject.adapter.DingDanFinishAdapter;
import com.wanyangproject.entity.DaiPingJiaEntity;
import com.wanyangproject.entity.FinishEntity;
import com.wanyangproject.my.FinishXiangQingActivity;

import java.util.List;

/**
 * Created by 甜甜 on 2018/7/30.
 */

public class FinishAdapter extends RecyclerView.Adapter<FinishAdapter.ViewHolder>{

    private Context context;
    private List<FinishEntity.ResponseBean> response;

    public FinishAdapter(Context context, List<FinishEntity.ResponseBean> response) {
        this.context = context;
        this.response = response;
    }




//    进入详情
    private FinishClick finishClick;

    public void setFinishClick(FinishClick finishClick) {
        this.finishClick = finishClick;
    }

    public interface FinishClick{
        void finishClick(int position);
    }







    @Override
    public FinishAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.dingdan_item_finish,parent,false);
        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final FinishAdapter.ViewHolder holder, final int position) {
//        点击进入详情
        holder.linear_finish.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(finishClick != null){
                    finishClick.finishClick(holder.getAdapterPosition());
                }
            }
        });


        if(response.get(position).getType().equals("12")){
            holder.Linear_tuikuan.setVisibility(View.VISIBLE);
            holder.btn_quanbu.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(context, FinishXiangQingActivity.class);
                    intent.putExtra("dingdanhao",response.get(position).getOrder_sn());
                    context.startActivity(intent);
                }
            });
            holder.tv_zhuangtai.setText("退款完成");
        }



//        已完成中间的列表
        DingDanFinishAdapter dingDanFinishAdapter = new DingDanFinishAdapter(context,response.get(position).getGoods());
        LinearLayoutManager manager = new LinearLayoutManager(context);
        holder.recycerView.setLayoutManager(manager);
        holder.recycerView.setAdapter(dingDanFinishAdapter);


        dingDanFinishAdapter.setDingDanFinishClick(new DingDanFinishAdapter.DingDanFinishClick() {
            @Override
            public void dingdanFinishClick(String id) {
                Intent intent = new Intent(context, FinishXiangQingActivity.class);
                intent.putExtra("dingdanhao",response.get(position).getOrder_sn());
                System.out.println(response.get(position).getOrder_sn()  +"       .....");
                context.startActivity(intent);
            }
        });


        if(response.get(position).getShop_name() == null){

        }else{ // 商家店名
            holder.tv_name.setText(response.get(position).getShop_name());
        }



        int sum = 0;
        float quanbu = 0;
        for (int i = 0; i < response.get(position).getGoods().size(); i++) {
            FinishEntity.ResponseBean.GoodsBean goodsBean = response.get(position).getGoods().get(i);
            sum += Integer.parseInt(goodsBean.getNum());
            quanbu +=  Integer.parseInt(goodsBean.getNum())*Float.parseFloat(goodsBean.getPic());
        }
        if(sum+"" == null){

        }else{ //合计的商品的数量
            holder.tv_heji.setText("共计"+sum+"件商品  合计：");
        }



        if(response.get(position).getMoney() == null){

        }else{ // 合计的总钱数
            holder.tv_heji_money.setText(" ¥"+response.get(position).getMoney()+"  ");
        }


//        if(response.get(position).getShop_cost() == null){
//
//        }else{ //配送费
//            holder.tv_yunfei.setText(("含运费：¥"+response.get(position).getShop_cost()));
//        }


        float vmon = Float.parseFloat(response.get(position).getMoney());
        if (quanbu < vmon){
            holder.tv_yunfei.setText(("(含运费：¥"+response.get(position).getShop_cost()+")"));
        }else{
            holder.tv_yunfei.setText(("(不含运费：¥"+response.get(position).getShop_cost()+")"));
        }


    }

    @Override
    public int getItemCount() {
        return response.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
//        private TextView tv_name,tv_zhuangtai,tv_neirong,tv_monry,tv_number,tv_heji_money,tv_yunfei;
        private TextView tv_heji,tv_name,tv_heji_money,tv_yunfei,tv_zhuangtai;
//        private ImageView image_tupian;
        private Button btn_quanbu;
        private LinearLayout linear_finish,Linear_tuikuan;
        private RecyclerView recycerView;
        private View view;
        public ViewHolder(View itemView) {
            super(itemView);
            view = itemView;
            tv_name = itemView.findViewById(R.id.tv_name);
//            tv_zhuangtai = itemView.findViewById(R.id.tv_zhuangtai);
//            tv_neirong = itemView.findViewById(R.id.tv_neirong);
//            tv_monry = itemView.findViewById(R.id.tv_monry);
//            tv_number = itemView.findViewById(R.id.tv_number);
            tv_heji = itemView.findViewById(R.id.tv_heji);
            tv_heji_money = itemView.findViewById(R.id.tv_heji_money);
            tv_yunfei = itemView.findViewById(R.id.tv_yunfei);
//            image_tupian = itemView.findViewById(R.id.image_tupian);
            btn_quanbu = itemView.findViewById(R.id.btn_quanbu);
            linear_finish = itemView.findViewById(R.id.linear_finish);
            recycerView = itemView.findViewById(R.id.recycerView);
            Linear_tuikuan = itemView.findViewById(R.id.Linear_tuikuan);
            tv_zhuangtai = itemView.findViewById(R.id.tv_zhuangtai);
        }
    }
}
