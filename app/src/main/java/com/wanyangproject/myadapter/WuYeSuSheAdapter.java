package com.wanyangproject.myadapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.wanyangproject.R;
import com.wanyangproject.adapter.SuSheGuanLiAdapter;
import com.wanyangproject.entity.MyQiYeSuSheGuanLiEntity;
import com.wanyangproject.my.WuYeSuSheGuanLiActivity;

import java.util.List;

/**
 * Created by 甜甜 on 2018/8/2.
 */

public class WuYeSuSheAdapter extends RecyclerView.Adapter<WuYeSuSheAdapter.ViewHolder>{

    private Context context;
    private List<MyQiYeSuSheGuanLiEntity.ResponseBean.DataBean> data;

    public WuYeSuSheAdapter(Context context, List<MyQiYeSuSheGuanLiEntity.ResponseBean.DataBean> data) {
        this.context = context;
        this.data = data;
    }





    //    点击查看详情
    private XiangQingClick xiangQingClick;

    public void setXiangQingClick(XiangQingClick xiangQingClick) {
        this.xiangQingClick = xiangQingClick;
    }

    public interface XiangQingClick{
        void xiangqingClick(int position,String id);
    }





    @Override
    public WuYeSuSheAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_changfang,parent,false);
        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final WuYeSuSheAdapter.ViewHolder holder, final int position) {
        holder.relative.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(xiangQingClick != null){
                    xiangQingClick.xiangqingClick(holder.getAdapterPosition(),data.get(position).getDormitoryId());
                }
            }
        });







        if(data.get(position).getDormitoryName() == null){

        }else{
            holder.tv_name.setText(data.get(position).getDormitoryName());
        }

    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView tv_name,tv_yuanqu_name,tv_dikuai;
        private RelativeLayout relative;
        private View view;
        public ViewHolder(View itemView) {
            super(itemView);
            view = itemView;
            tv_name = itemView.findViewById(R.id.tv_name);
            tv_yuanqu_name = itemView.findViewById(R.id.tv_yuanqu_name);
            tv_dikuai = itemView.findViewById(R.id.tv_dikuai);
            relative = itemView.findViewById(R.id.relative);
        }
    }
}
