package com.wanyangproject.myadapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.wanyangproject.R;
import com.wanyangproject.adapter.DingDanDaiZhiFuAdapter;
import com.wanyangproject.entity.DaiZhiFuEntity;
import com.wanyangproject.my.DaiZhiFuXiangQingActivity;

import java.util.List;

/**
 * Created by 甜甜 on 2018/7/30.
 */

public class DaiZhiFuAdapter extends RecyclerView.Adapter<DaiZhiFuAdapter.ViewHolder>{

    private Context context;
    private List<DaiZhiFuEntity.ResponseBean> response;

    public DaiZhiFuAdapter(Context context, List<DaiZhiFuEntity.ResponseBean> response) {
        this.context = context;
        this.response = response;
    }


//    点击待支付跳转详情
    private DaiZhiFuXiangQingClick daiZhiFuXiangQingClick;

    public void setDaiZhiFuXiangQingClick(DaiZhiFuXiangQingClick daiZhiFuXiangQingClick) {
        this.daiZhiFuXiangQingClick = daiZhiFuXiangQingClick;
    }

    public interface DaiZhiFuXiangQingClick{
        void daizhifuxiangqiangClick(int position);
    }




//    点击去支付
    private QuZhiFuClick quZhiFuClick;

    public void setQuZhiFuClick(QuZhiFuClick quZhiFuClick) {
        this.quZhiFuClick = quZhiFuClick;
    }

    public interface QuZhiFuClick{
        void quzhifuClick(int position);
    }



    @Override
    public DaiZhiFuAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.dingdan_item_daizhifu,parent,false);
        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final DaiZhiFuAdapter.ViewHolder holder, final int position) {
        holder.linear_daizhifu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(daiZhiFuXiangQingClick!= null){
                    daiZhiFuXiangQingClick.daizhifuxiangqiangClick(holder.getAdapterPosition());
                }
            }
        });

//        点击支付按钮
        holder.btn_quzhifu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(quZhiFuClick!= null){
                    quZhiFuClick.quzhifuClick(holder.getAdapterPosition());
                }
            }
        });



//        待支付中间的列表
        DingDanDaiZhiFuAdapter dingDanAdapter = new DingDanDaiZhiFuAdapter(context,response.get(position).getGoods());
        LinearLayoutManager manager = new LinearLayoutManager(context);
        holder.recyclerView.setLayoutManager(manager);
        holder.recyclerView.setAdapter(dingDanAdapter);

//        点击中间的列表进入详情
        dingDanAdapter.setDingdandaizhifuxiangqiangClick(new DingDanDaiZhiFuAdapter.DingDanDaiZhiFuXiangQingClick() {
            @Override
            public void dingdandaizhifuxiangqiangClick(String id) {
                Intent intent = new Intent(context, DaiZhiFuXiangQingActivity.class);
                intent.putExtra("dingdanhao",response.get(position).getOrder_sn());
                context.startActivity(intent);
            }
        });




        float zongjiaone = 0;
        float zongjiatwo = Float.parseFloat(response.get(position).getMoney()); //合计价格

        for (int i = 0; i < response.get(position).getGoods().size(); i++) {
            DaiZhiFuEntity.ResponseBean.GoodsBean goodsBean = response.get(position).getGoods().get(i);
            float mon = Integer.parseInt(goodsBean.getNum()) * Float.parseFloat(goodsBean.getPic());
            zongjiaone += mon;
        }


        String yunfei;
        if(zongjiaone < zongjiatwo){
//            说明包含运费
            yunfei = "包含运费 ¥";
        }else{
//            不含运费
            yunfei = "不包含运费 ¥";
        }






        if(response.get(position).getShop_name() == null){

        }else{ // 商家店名
            holder.tv_name.setText(response.get(position).getShop_name());
        }


        int sum = 0;
        float quanbu = 0;
        for (int i = 0; i < response.get(position).getGoods().size(); i++) {
            DaiZhiFuEntity.ResponseBean.GoodsBean goodsBean = response.get(position).getGoods().get(i);
            sum += Integer.parseInt(goodsBean.getNum());
            quanbu +=  Integer.parseInt(goodsBean.getNum())*Float.parseFloat(goodsBean.getPic());
        }
        if(sum+"" == null){

        }else{ //合计的商品的数量
            holder.tv_heji.setText("共计"+sum+"件商品  合计：");
        }


        if(response.get(position).getMoney() == null){

        }else{ // 合计的总钱数
            holder.tv_heji_money.setText(" ¥"+response.get(position).getMoney()+"  ");
        }

//
//        if(response.get(position).getShop_cost() == null){
//
//        }else{ //配送费
//            holder.tv_yunfei.setText((yunfei + response.get(position).getShop_cost()));
//        }

        float vmon = Float.parseFloat(response.get(position).getMoney());
        if (quanbu < vmon){
            holder.tv_yunfei.setText(("(含运费：¥"+response.get(position).getShop_cost()+")"));
        }else{
            holder.tv_yunfei.setText(("(不含运费：¥"+response.get(position).getShop_cost()+")"));
        }



    }

    @Override
    public int getItemCount() {
        return response.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
//        private TextView tv_name,tv_zhuangtai,tv_neirong,tv_monry,tv_number,tv_heji,tv_heji_money,tv_yunfei;
        private TextView tv_name,tv_heji_money,tv_heji,tv_yunfei;
        private LinearLayout linear_daizhifu;
        private ImageView image_tupian;
        private Button btn_quzhifu;
        private RecyclerView recyclerView;
        private View view;
        public ViewHolder(View itemView) {
            super(itemView);
            view = itemView;
            tv_name = itemView.findViewById(R.id.tv_name);
//            tv_zhuangtai = itemView.findViewById(R.id.tv_zhuangtai);
//            tv_neirong = itemView.findViewById(R.id.tv_neirong);
//            tv_monry = itemView.findViewById(R.id.tv_monry);
//            tv_number = itemView.findViewById(R.id.tv_number);
            tv_heji = itemView.findViewById(R.id.tv_heji);
            tv_heji_money = itemView.findViewById(R.id.tv_heji_money);
            tv_yunfei = itemView.findViewById(R.id.tv_yunfei);
            image_tupian = itemView.findViewById(R.id.image_tupian);
            btn_quzhifu = itemView.findViewById(R.id.btn_quzhifu);
            linear_daizhifu = itemView.findViewById(R.id.linear_daizhifu);
            recyclerView = itemView.findViewById(R.id.recyclerView);
        }
    }
}
