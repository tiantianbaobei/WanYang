package com.wanyangproject.myadapter;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.wanyangproject.R;
import com.wanyangproject.entity.ShangJiaXianShiJineEntity;
import com.wanyangproject.my.WoDeYuEActivity;

import java.util.List;

/**
 * Created by 甜甜 on 2018/8/4.
 */

public class ShouRuMingXiAdapter extends RecyclerView.Adapter<ShouRuMingXiAdapter.ViewHolder>{

    private Context context;
    private List<ShangJiaXianShiJineEntity.ResponseBean.CapitalBean> capital;

    public ShouRuMingXiAdapter(Context context, List<ShangJiaXianShiJineEntity.ResponseBean.CapitalBean> capital) {
        this.context = context;
        this.capital = capital;
    }

    @Override
    public ShouRuMingXiAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_shouru_mingxi,parent,false);
        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ShouRuMingXiAdapter.ViewHolder holder, int position) {
        if(capital.get(position).getTitle() == null){

        }else{  // 标题
            holder.tv_title.setText(capital.get(position).getTitle());
        }


        if(capital.get(position).getTime() == null){

        }else{ // 时间
            holder.tv_time.setText(capital.get(position).getTime());
        }


        if(capital.get(position).getMoney() == null){

        }else{ // 金额
            if(capital.get(position).getTitle().equals("收入")){
                holder.tv_number.setTextColor(Color.parseColor("#e32d30"));
                holder.tv_number.setText("+"+capital.get(position).getMoney());
            }else if(capital.get(position).getTitle().equals("提现")){
                holder.tv_number.setTextColor(Color.parseColor("#2b94fe"));
                holder.tv_number.setText("-"+capital.get(position).getMoney());

            }
//            holder.tv_number.setText(capital.get(position).getMoney());
        }







    }

    @Override
    public int getItemCount() {
        return capital.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView tv_title,tv_time,tv_number;
        private View view;
        public ViewHolder(View itemView) {
            super(itemView);
            view = itemView;
            tv_title = itemView.findViewById(R.id.tv_title);
            tv_time = itemView.findViewById(R.id.tv_time);
            tv_number = itemView.findViewById(R.id.tv_number);
        }
    }
}
