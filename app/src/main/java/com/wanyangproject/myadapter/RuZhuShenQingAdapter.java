package com.wanyangproject.myadapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.wanyangproject.R;
import com.wanyangproject.activity.ShangJiaRuZhuXiangQingActivity;
import com.wanyangproject.activity.ShangJiaRuZhuXiuGaiActivity;
import com.wanyangproject.entity.ShangJiaShenQingLieBiaoEntity;
import com.wanyangproject.my.RuZhuShenQingActivity;
import com.wanyangproject.my.ShangJiaRuZhuActivity;

import java.util.List;

/**
 * Created by 甜甜 on 2018/7/31.
 */

public class RuZhuShenQingAdapter extends RecyclerView.Adapter<RuZhuShenQingAdapter.ViewHolder>{

    private Context context;
    private List<ShangJiaShenQingLieBiaoEntity.ResponseBean> response;

    public RuZhuShenQingAdapter(Context context, List<ShangJiaShenQingLieBiaoEntity.ResponseBean> response) {
        this.context = context;
        this.response = response;
    }







//    审核拒绝的编辑





    @Override
    public RuZhuShenQingAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_ruzhu_shenqing,parent,false);
        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(RuZhuShenQingAdapter.ViewHolder holder, final int position) {
        if(response.get(position).getApplicant_name() == null){

        }else{
            holder.tv_name.setText(response.get(position).getApplicant_name());
        }


        if(response.get(position).getApplicant_phone() == null){

        }else{
            holder.tv_danpu_name.setText(response.get(position).getName());
        }


        if(response.get(position).getType() == null){

        }else{
            if(response.get(position).getType().equals("1")){
                holder.tv_shenhezhong.setText("审核中");
                holder.linear.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent intent = new Intent(context, ShangJiaRuZhuXiangQingActivity.class);
                        intent.putExtra("id",response.get(position).getId());
                        context.startActivity(intent);
                    }
                });
            }else if(response.get(position).getType().equals("2")){
                holder.tv_shenhezhong.setText("申请通过");
                holder.tv_shenhezhong.setTextColor(Color.parseColor("#1fad1e"));
                holder.linear.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent intent = new Intent(context, ShangJiaRuZhuXiangQingActivity.class);
                        intent.putExtra("id",response.get(position).getId());
                        context.startActivity(intent);
                    }
                });
            }else if(response.get(position).getType().equals("3")){
                holder.tv_shenhezhong.setText("拒绝");
                holder.tv_shibai.setVisibility(View.VISIBLE);
                holder.tv_shibai.setText("审核失败原因："+response.get(position).getRefusal());
                holder.linear.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent intent = new Intent(context, ShangJiaRuZhuXiuGaiActivity.class);
                        intent.putExtra("id",response.get(position).getId());
//                        intent.putExtra("type","3");
                        context.startActivity(intent);
                    }
                });
            }
        }
    }

    @Override
    public int getItemCount() {
        return response.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView tv_shenqingren,tv_name,tv_dianpu,tv_danpu_name,tv_shenhezhong,tv_shibai;
        private LinearLayout linear;
        private View view;
        public ViewHolder(View itemView) {
            super(itemView);
            view = itemView;
            tv_shenqingren = itemView.findViewById(R.id.tv_shenqingren);
            tv_name = itemView.findViewById(R.id.tv_name);
            tv_dianpu = itemView.findViewById(R.id.tv_dianpu);
            tv_danpu_name = itemView.findViewById(R.id.tv_danpu_name);
            tv_shenhezhong = itemView.findViewById(R.id.tv_shenhezhong);
            tv_shibai = itemView.findViewById(R.id.tv_shibai);
            linear = itemView.findViewById(R.id.linear);
        }
    }
}
