package com.wanyangproject.myadapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.umeng.commonsdk.debug.E;
import com.wanyangproject.R;
import com.wanyangproject.adapter.TuiKuanYuanYinShangMianAdapter;
import com.wanyangproject.entity.DaiShouHuoEntity;
import com.wanyangproject.entity.DingDanXiangQingEntity;
import com.wanyangproject.my.TuiKuanActivity;
import com.wanyangproject.popuwindow.TuiKuanPopupWindow;

/**
 * Created by 甜甜 on 2018/7/30.
 */

public class TuiKuanAdapter extends RecyclerView.Adapter<TuiKuanAdapter.ViewHolder>{

    private Context context;
    private TuiKuanPopupWindow tuiKuanPopupWindow;
//    private DaiShouHuoEntity.ResponseBean responseBean;
    private DingDanXiangQingEntity.ResponseBean response;

    public TuiKuanAdapter(Context context, DingDanXiangQingEntity.ResponseBean response) {
        this.context = context;
        this.response = response;
//        this.responseBean = responseBean;
    }


//    public TuiKuanAdapter(Context context, DaiShouHuoEntity.ResponseBean responseBean) {
//        this.context = context;
//        this.responseBean = responseBean;
//    }


//    退款
    private TuiKuanClick tuiKuanClick;



    public void setTuiKuanClick(TuiKuanClick tuiKuanClick) {
        this.tuiKuanClick = tuiKuanClick;
    }

    public interface TuiKuanClick{
        void tuikuanClick(int position,String number);
    }



    @Override
    public TuiKuanAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_tuikuan,parent,false);
        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final TuiKuanAdapter.ViewHolder holder, int position) {
//        holder.tv_xuanze.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                if(tuiKuanClick != null){
//                    tuiKuanClick.tuikuanClick(holder.getAdapterPosition());
//                }
//            }
//        });


////        申请退款界面上面的列表
//        TuiKuanYuanYinShangMianAdapter tuiKuanYuanYinShangMianAdapter = new TuiKuanYuanYinShangMianAdapter(context,response);
//        LinearLayoutManager manager = new LinearLayoutManager(context);
//        holder.recyclerView.setLayoutManager(manager);
//        holder.recyclerView.setAdapter(tuiKuanYuanYinShangMianAdapter);




        holder.tv_xuanze.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Object itemsOnClick = null;
                tuiKuanPopupWindow = new TuiKuanPopupWindow(context, (View.OnClickListener) itemsOnClick);
                tuiKuanPopupWindow.showAtLocation(holder.relative, Gravity.BOTTOM | Gravity.CENTER_HORIZONTAL, 0, 0);

                tuiKuanPopupWindow.setTuiKuanClick(new TuiKuanPopupWindow.TuiKuanClick() {
                    @Override
                    public void tuikuanClick(String number) {
                        if(number == null){

                        }else{
                            holder.tv_xuanze.setText(number);
                            Intent intent=new Intent();
                            intent.putExtra("tuikuanyuanyin",number);
                            intent.setAction("tuikuan");
                            context.sendBroadcast(intent);
                        }

                    }
                });

            }
        });



        if(response.getMoney() == null){

        }else{
            holder.tv_tuikuan_jine.setText("退款金额："+response.getMoney());
        }






    }

    @Override
    public int getItemCount() {
        return 1;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView tv_xuanze,tv_tuikuan_jine;
        private RelativeLayout relative;
        private RecyclerView recyclerView;
        private EditText et_zhifubao_zhanghao,et_zhifubao_mingcheng;
        private View view;
        public ViewHolder(View itemView) {
            super(itemView);
            view = itemView;
            tv_xuanze = itemView.findViewById(R.id.tv_xuanze);
            tv_tuikuan_jine = itemView.findViewById(R.id.tv_tuikuan_jine);
            relative = itemView.findViewById(R.id.relative);
            recyclerView = itemView.findViewById(R.id.recyclerView);
            et_zhifubao_zhanghao = itemView.findViewById(R.id.et_zhifubao_zhanghao);
            et_zhifubao_mingcheng = itemView.findViewById(R.id.et_zhifubao_mingcheng);
        }
    }
}
