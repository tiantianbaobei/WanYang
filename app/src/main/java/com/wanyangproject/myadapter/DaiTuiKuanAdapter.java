package com.wanyangproject.myadapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.wanyangproject.R;
import com.wanyangproject.entity.ShangJiaDingDanEntity;

import java.util.List;

/**
 * Created by 甜甜 on 2018/8/4.
 */

public class DaiTuiKuanAdapter extends RecyclerView.Adapter<DaiTuiKuanAdapter.ViewHolder>{

    private Context context;
    private List<ShangJiaDingDanEntity.ResponseBean> response;

    public DaiTuiKuanAdapter(Context context,List<ShangJiaDingDanEntity.ResponseBean> response) {
        this.context = context;
        this.response = response;
    }


//    确认退款的按钮
    private QueRenTuiKuanClick queRenTuiKuanClick;

    public void setQueRenTuiKuanClick(QueRenTuiKuanClick queRenTuiKuanClick) {
        this.queRenTuiKuanClick = queRenTuiKuanClick;
    }

    public interface QueRenTuiKuanClick{
        void querentuikuanClick(int position,String Order_sn);
    }



    @Override
    public DaiTuiKuanAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_dai_tuikuan,parent,false);
        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final DaiTuiKuanAdapter.ViewHolder holder, final int position) {
        holder.tv_queren.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(queRenTuiKuanClick != null){
                    queRenTuiKuanClick.querentuikuanClick(holder.getAdapterPosition(),response.get(position).getOrder_sn());
                }
            }
        });




        if(response.get(position).getType().equals("12")){
            holder.tv_yiwancheng.setVisibility(View.VISIBLE);
            holder.relative_tuikuan.setVisibility(View.GONE);
        }




        if(response.get(position).getOrder_sn() == null){

        }else{
            holder.tv_dingdanbianhao.setText("订单编号："+response.get(position).getOrder_sn());
        }


        if(response.get(position).getOrder_goods() == null){

        }else{
            String goods_name = "";
            for (int i = 0; i < response.get(position).getOrder_goods().size(); i++) {
                ShangJiaDingDanEntity.ResponseBean.OrderGoodsBean orderGoodsBean = response.get(position).getOrder_goods().get(i);
                goods_name = orderGoodsBean.getGoods_name();
            }
            holder.tv_tuikuan.setText(goods_name);
        }




        if(response.get(position).getOrder_goods() == null){

        }else{
            float pic = 0;
            for (int i = 0; i < response.get(position).getOrder_goods().size(); i++) {
                ShangJiaDingDanEntity.ResponseBean.OrderGoodsBean orderGoodsBean = response.get(position).getOrder_goods().get(i);

                pic = Float.parseFloat(orderGoodsBean.getPic())*Integer.parseInt(response.get(position).getOrder_goods().get(i).getNum());
            }
            holder.tv_jine.setText("¥"+ pic);
        }




        if(response.get(position).getAccount() == null){

        }else{
            holder.tv_zhifubao_zhanghao.setText(response.get(position).getAccount());
        }



        if(response.get(position).getRefund() == null){

        }else{
            holder.tv_zhifubao_name.setText(response.get(position).getRefund());
        }



//        退款原因
        if(response.get(position).getReason() == null){

        }else{
            holder.tv_yuanyin.setText(response.get(position).getReason());
        }



        if(response.get(position).getType().equals("12")){
            holder.tv_queren.setText("退款完成");
            holder.tv_queren.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    return;
                }
            });
        }


    }

    @Override
    public int getItemCount() {
        return response.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView tv_dingdanbianhao,tv_tuikuan,tv_jine,tv_yuanyin,tv_queren,tv_zhifubao_zhanghao,tv_zhifubao_name,tv_yiwancheng;
        private RelativeLayout relative_tuikuan;
        private View view;
        public ViewHolder(View itemView) {
            super(itemView);
            view = itemView;
            tv_dingdanbianhao = itemView.findViewById(R.id.tv_dingdanbianhao);
            tv_tuikuan = itemView.findViewById(R.id.tv_tuikuan);
            tv_jine = itemView.findViewById(R.id.tv_jine);
            tv_yuanyin = itemView.findViewById(R.id.tv_yuanyin);
            tv_queren = itemView.findViewById(R.id.tv_queren);
            tv_zhifubao_name = itemView.findViewById(R.id.tv_zhifubao_name);
            tv_zhifubao_zhanghao = itemView.findViewById(R.id.tv_zhifubao_zhanghao);
            relative_tuikuan = itemView.findViewById(R.id.relative_tuikuan);
            tv_yiwancheng = itemView.findViewById(R.id.tv_yiwancheng);
        }
    }
}
