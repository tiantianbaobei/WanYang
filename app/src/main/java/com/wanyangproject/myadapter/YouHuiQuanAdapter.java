package com.wanyangproject.myadapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.wanyangproject.R;
import com.wanyangproject.entity.ShangJiaYouHuiQuanEntity;
import com.wanyangproject.my.ShangJiaYouHuiQuanActivity;

import java.util.List;

/**
 * Created by 甜甜 on 2018/8/3.
 */

public class YouHuiQuanAdapter extends RecyclerView.Adapter<YouHuiQuanAdapter.ViewHolder>{

    private Context context;
    private List<ShangJiaYouHuiQuanEntity.ResponseBean> response;

    public YouHuiQuanAdapter(Context context, List<ShangJiaYouHuiQuanEntity.ResponseBean> response) {
     this.context = context;
        this.response = response;
    }

    @Override
    public YouHuiQuanAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_youhuiquan,parent,false);
        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(YouHuiQuanAdapter.ViewHolder holder, int position) {
        if(response.get(position).getService() == null){

        }else{ // 名称
            holder.tv_title.setText(response.get(position).getService());
        }


        if(response.get(position).getMoney() == null){

        }else{  // 金额
            holder.tv_money.setText("¥ "+response.get(position).getMoney());
        }


        if(response.get(position).getUse_time() == null){

        }else{
            if(response.get(position).getPermanent().equals("1")){
                holder.tv_time.setText("有效期："+response.get(position).getUse_time());
            }

        }



        if(response.get(position).getPermanent() == null){

        }else{
            if(response.get(position).getPermanent().equals("2")){
                holder.tv_time.setText("有效期：永久");
            }
        }





        if(response.get(position).getYi() == null){

        }else{
            holder.tv_shiyong.setText("已使用 ("+response.get(position).getYi()+"张)");
        }


        if(response.get(position).getNember() == null){

        }else{
            holder.tv_zonggong.setText("共计 ("+response.get(position).getNember()+"张)");
        }


        if(response.get(position).getStatus() == null){

        }else{
            if(response.get(position).getStatus().equals("2")){
                holder.relativeLayout.setBackgroundResource(R.drawable.youhuiquan_huoqi);
                holder.image_guoqi.setVisibility(View.VISIBLE);
            }
        }

    }

    @Override
    public int getItemCount() {
        return response.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView tv_title,tv_money,tv_time,tv_shiyong,tv_zonggong;
        private RelativeLayout relativeLayout;
        private ImageView image_guoqi;
        private View view;
        public ViewHolder(View itemView) {
            super(itemView);
            tv_title = itemView.findViewById(R.id.tv_title);
            tv_money = itemView.findViewById(R.id.tv_money);
            tv_time = itemView.findViewById(R.id.tv_time);
            tv_shiyong = itemView.findViewById(R.id.tv_shiyong);
            tv_zonggong = itemView.findViewById(R.id.tv_zonggong);
            relativeLayout = itemView.findViewById(R.id.relativeLayout);
            image_guoqi = itemView.findViewById(R.id.image_guoqi);
        }
    }
}
