package com.wanyangproject.myadapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.wanyangproject.R;
import com.wanyangproject.entity.QiYeShuiDianEntity;

import java.util.List;

/**
 * Created by 甜甜 on 2018/7/31.
 */

public class QiYeDianAdapter extends RecyclerView.Adapter<QiYeDianAdapter.ViewHolder>{

    private Context context;
    private  List<QiYeShuiDianEntity.ResponseBean> response;

    public QiYeDianAdapter(Context context, List<QiYeShuiDianEntity.ResponseBean> response) {
        this.context = context;
        this.response = response;
    }


    @Override
    public QiYeDianAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_dian,parent,false);
        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(QiYeDianAdapter.ViewHolder holder, int position) {
        if(response.get(position).getMonth() == null){

        }else{
            holder.tv_time.setText(response.get(position).getMonth());
        }


        if(response.get(position).getCost() == null){

        }else{
            holder.tv_number.setText(response.get(position).getCost()+"度");
        }


    }

    @Override
    public int getItemCount() {
        if(response == null){
            return 0;
        }
        return response.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView tv_time,tv_number;
        private View view;
        public ViewHolder(View itemView) {
            super(itemView);
            view = itemView;
            tv_time = itemView.findViewById(R.id.tv_time);
            tv_number = itemView.findViewById(R.id.tv_number);
        }
    }
}
