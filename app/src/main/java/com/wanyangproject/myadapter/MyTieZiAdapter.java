package com.wanyangproject.myadapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.wanyangproject.R;
import com.wanyangproject.entity.MyTieZiLieBiaoEntity;

import java.util.List;

/**
 * Created by 甜甜 on 2018/7/31.
 */

public class MyTieZiAdapter extends RecyclerView.Adapter<MyTieZiAdapter.ViewHolder>{

    private Context context;
    private List<MyTieZiLieBiaoEntity.ResponseBean.ListBean> list;

    public MyTieZiAdapter(Context context, List<MyTieZiLieBiaoEntity.ResponseBean.ListBean> list) {
        this.context = context;
        this.list = list;
    }


//    删除
    private ShanChuClick shanChuClick;

    public void setShanChuClick(ShanChuClick shanChuClick) {
        this.shanChuClick = shanChuClick;
    }

    public interface ShanChuClick{
        void shanchuClick(int position,String id);
}



//    点击进入详情
    private XiangQingClick xiangQingClick;

    public void setXiangQingClick(XiangQingClick xiangQingClick) {
        this.xiangQingClick = xiangQingClick;
    }

    public interface XiangQingClick{
        void xiangqingClick(int position,String id);
    }


    @Override
    public MyTieZiAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_tiezi,parent,false);
        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final MyTieZiAdapter.ViewHolder holder, final int position) {

//        点击进入详请
        holder.linear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(xiangQingClick != null){
                    xiangQingClick.xiangqingClick(holder.getAdapterPosition(),list.get(position).getId());
                }
            }
        });








        if(list.get(position).getUinfo().getAvatar().equals("")){

        }else{
            Glide.with(context).load(list.get(position).getUinfo().getAvatar()).into(holder.image_touxiang);
        }


        if(list.get(position).getUinfo().getNickname().equals("")){

        }else{
            holder.tv_name.setText(list.get(position).getUinfo().getNickname());
        }



        if(list.get(position).getAdd_time().equals("")){

        }else{
            holder.tv_shijian.setText(list.get(position).getAdd_time());
        }


        if(list.get(position).getTitle().equals("")){

        }else{
            holder.tv_neirong.setText(list.get(position).getTitle());
        }


        if(list.get(position).getTitle().equals("")){

        }else{
            holder.tv_title.setText(list.get(position).getTitle());
        }





//        删除
        holder.tv_delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(shanChuClick != null){
                    shanChuClick.shanchuClick(holder.getAdapterPosition(),list.get(position).getId());
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        System.out.println(list.size()+"                  list.size()");

        return list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView tv_name,tv_shijian,tv_neirong,tv_delete,tv_title;
        private ImageView image_touxiang;
        private LinearLayout linear;
        private View view;
        public ViewHolder(View itemView) {
            super(itemView);
            view = itemView;
            tv_name = itemView.findViewById(R.id.tv_name);
            tv_shijian = itemView.findViewById(R.id.tv_shijian);
            tv_neirong = itemView.findViewById(R.id.tv_neirong);
            image_touxiang = itemView.findViewById(R.id.image_touxiang);
            tv_delete = itemView.findViewById(R.id.tv_delete);
            linear = itemView.findViewById(R.id.linear);
            tv_title = itemView.findViewById(R.id.tv_title);
        }
    }
}
