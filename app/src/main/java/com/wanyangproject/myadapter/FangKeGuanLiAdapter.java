package com.wanyangproject.myadapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.wanyangproject.R;
import com.wanyangproject.entity.FangKeGuanLiEntity;
import com.wanyangproject.my.MyFangKeGuanLiActivity;

import java.util.List;

/**
 * Created by 甜甜 on 2018/7/31.
 */

public class FangKeGuanLiAdapter extends RecyclerView.Adapter<FangKeGuanLiAdapter.ViewHolder>{

    private Context context;
    private  List<FangKeGuanLiEntity.ResponseBean> response;

    public FangKeGuanLiAdapter(Context context, List<FangKeGuanLiEntity.ResponseBean> response) {
        this.context = context;
        this.response = response;
    }



//    编辑
    private BianJiClick bianJiClick;

    public void setBianJiClick(BianJiClick bianJiClick) {
        this.bianJiClick = bianJiClick;
    }

    public interface BianJiClick{
        void banjiclick(int position);
}



//    删除
    private ShanChuClick shanChuClick;

    public void setShanChuClick(ShanChuClick shanChuClick) {
        this.shanChuClick = shanChuClick;
    }

    public interface ShanChuClick{
        void shanchuClick(int position,String id);
    }


    @Override
    public FangKeGuanLiAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_fangkeguanli,parent,false);
        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final FangKeGuanLiAdapter.ViewHolder holder, final int position) {

        holder.tv_delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(shanChuClick != null){
                    shanChuClick.shanchuClick(holder.getAdapterPosition(),response.get(position).getId());
                }
            }
        });


        if(response.get(position).getName() == null){

        }else{
            holder.tv_name.setText(response.get(position).getName());
        }



        if(response.get(position).getNums() == null){

        }else{
            holder.tv_number.setText("到访人数："+response.get(position).getNums()+"人");
        }


        if(response.get(position).getPhone() == null){

        }else{
            holder.tv_phone.setText(response.get(position).getPhone());
        }


        if(response.get(position).getDaodaTime() == null){

        }else{
            holder.tv_time.setText(response.get(position).getDaodaTime());
        }




        if(response.get(position).getType().equals("1")){
            holder.image_yidaofang.setImageResource(R.mipmap.weidaofang);//  1 未到访
            holder.tv_delete.setVisibility(View.GONE);
            holder.tv_bianji.setVisibility(View.VISIBLE);
        }else if(response.get(position).getType().equals("2")){
            holder.image_yidaofang.setImageResource(R.mipmap.yidaofang);// 2 已到访
            holder.tv_delete.setVisibility(View.VISIBLE);
            holder.tv_bianji.setVisibility(View.GONE);
        }else if(response.get(position).getType().equals("3")){//已失效
            holder.image_yidaofang.setImageResource(R.drawable.yishixiao);// 3 已失效
            holder.tv_delete.setVisibility(View.VISIBLE);
            holder.tv_bianji.setVisibility(View.GONE);
        }



        holder.tv_bianji.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(bianJiClick != null){
                    bianJiClick.banjiclick(holder.getAdapterPosition());
                }
            }
        });
    }


    @Override
    public int getItemCount() {
        return response.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView tv_name,tv_phone,tv_number,tv_time,tv_delete,tv_bianji;
        private ImageView image_yidaofang;
        private View view;
        public ViewHolder(View itemView) {
            super(itemView);
            view = itemView;
            image_yidaofang = itemView.findViewById(R.id.image_yidaofang);
            tv_name = itemView.findViewById(R.id.tv_name);
            tv_phone = itemView.findViewById(R.id.tv_phone);
            tv_number = itemView.findViewById(R.id.tv_number);
            tv_time = itemView.findViewById(R.id.tv_time);
            tv_delete = itemView.findViewById(R.id.tv_delete);
            tv_bianji = itemView.findViewById(R.id.tv_bianji);
        }
    }
}
