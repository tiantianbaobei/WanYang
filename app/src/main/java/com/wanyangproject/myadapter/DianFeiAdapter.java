package com.wanyangproject.myadapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.wanyangproject.R;
import com.wanyangproject.entity.ShuiFeiJiLuEntity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by 甜甜 on 2018/8/1.
 */

public class DianFeiAdapter extends RecyclerView.Adapter<DianFeiAdapter.ViewHolder>{

    private Context context;
    private ArrayList<JSONObject> list;



    public DianFeiAdapter(Context context, ArrayList<JSONObject> list) {
        this.context = context;
        this.list = list;
    }


    @Override
    public DianFeiAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_shuifei_jilu,parent,false);
        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(DianFeiAdapter.ViewHolder holder, int position) {
        try {
            JSONObject jsonObject = (JSONObject) list.get(position);
            String amount = jsonObject.getString("amount");
            holder.tv_money.setText(amount);

        } catch (JSONException e) {
            e.printStackTrace();
        }




        try {
            JSONObject jsonObject = (JSONObject) list.get(position);
            String time = jsonObject.getString("time");
            holder.tv_time.setText(time);

        } catch (JSONException e) {
            e.printStackTrace();
        }





        try {
            JSONObject jsonObject = (JSONObject) list.get(position);
            String time = jsonObject.getString("chargeType");
            if(time.equals("0")){
                holder.tv_zhifu.setText("服务中心充值");
            }else if(time.equals("1")){
                holder.tv_zhifu.setText("微信充值");
            }else if(time.equals("2")){
                holder.tv_zhifu.setText("支付宝充值");
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }








//        if(list.get(position).getTime() == null){
//
//        }else{
//            holder.tv_time.setText(list.get(position).getTime());
//        }
//
//
//        if(list.get(position).getAmount() == null){
//
//        }else{
//            holder.tv_money.setText(list.get(position).getAmount());
//        }



    }

    @Override
    public int getItemCount() {
        if(list == null){
            return 0;
        }
        return list.size();
    }
    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView tv_zhifu,tv_time,tv_money;
        private View view;
        public ViewHolder(View itemView) {
            super(itemView);
            view = itemView;
            tv_time = itemView.findViewById(R.id.tv_time);
            tv_zhifu = itemView.findViewById(R.id.tv_zhifu);
            tv_money = itemView.findViewById(R.id.tv_money);
        }
    }
}
