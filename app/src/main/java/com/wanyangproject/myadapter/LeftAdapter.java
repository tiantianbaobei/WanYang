package com.wanyangproject.myadapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.wanyangproject.R;
import com.wanyangproject.entity.ShangJiaShangPinEntity;
import com.wanyangproject.entity.ShangJiaShangPinGuanLiEntity;
import com.wanyangproject.my.ShangJiaShangPinGuanLiActivity;

import java.util.List;

/**
 * Created by 甜甜 on 2018/8/3.
 */

public class LeftAdapter extends RecyclerView.Adapter<LeftAdapter.ViewHolder>{

    private Context context;
    private List<ShangJiaShangPinGuanLiEntity.ResponseBean.GoodsBeanX> goods;

    public LeftAdapter(Context context, List<ShangJiaShangPinGuanLiEntity.ResponseBean.GoodsBeanX> goods) {
        this.context = context;
        this.goods = goods;
    }

//    商家商品管理左边的列表
    private LeftClick leftClick;

    public void setLeftClick(LeftClick leftClick) {
        this.leftClick = leftClick;
    }

    public interface LeftClick{
        void leftClick(int position);
}

    @Override
    public LeftAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_left,parent,false);
        final ViewHolder viewHolder = new ViewHolder(view);
        viewHolder.view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(leftClick != null){
                    leftClick.leftClick(viewHolder.getAdapterPosition());
                }

                for (ShangJiaShangPinGuanLiEntity.ResponseBean.GoodsBeanX shangpinshangjiaguanli:goods){
                    shangpinshangjiaguanli.setOk(false);
                }
                goods.get(viewHolder.getAdapterPosition()).setOk(true);
                notifyDataSetChanged();


            }
        });
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(LeftAdapter.ViewHolder holder, int position) {
        if(goods.get(position).getName() == null){

        }else{
            holder.tv_yinpin.setText(goods.get(position).getName());
            if (goods.get(position).isOk()){
                holder.view_shu.setVisibility(View.VISIBLE);
                holder.tv_yinpin.setBackgroundColor(Color.WHITE);
                holder.tv_yinpin.setTextColor(Color.RED);

                Intent intent=new Intent();
                intent.putExtra("shangjianame",goods.get(position).getName());
                intent.setAction("shangjianame");
                context.sendBroadcast(intent);

            }else {
                holder.view_shu.setVisibility(View.GONE);
                holder.tv_yinpin.setBackgroundResource(R.drawable.shape_item_check);
                holder.tv_yinpin.setTextColor(Color.parseColor("#858585"));
            }
        }
    }






//           if(goods.get(position).getName() == null){
//
//    }else{
//        holder.tv_yinpin.setText(goods.get(position).getName());
//        if (goods.get(position).isOk()){
//            holder.view_shu.setVisibility(View.VISIBLE);
//            holder.tv_yinpin.setBackgroundColor(Color.WHITE);
//            holder.tv_yinpin.setTextColor(Color.RED);
//
//            Intent intent=new Intent();
//            intent.putExtra("name",goods.get(position).getName());
//            intent.setAction("name");
//            context.sendBroadcast(intent);
//
//        }else {
//            holder.view_shu.setVisibility(View.GONE);
//            holder.tv_yinpin.setBackgroundResource(R.drawable.shape_item_check);
//            holder.tv_yinpin.setTextColor(Color.parseColor("#1A1A1A"));
//        }
//    }
//






    @Override
    public int getItemCount() {
        return goods.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView tv_yinpin;
        private View view,view_shu;
        public ViewHolder(View itemView) {
            super(itemView);
            view = itemView;
            tv_yinpin = itemView.findViewById(R.id.tv_yinpin);
            view_shu = itemView.findViewById(R.id.view_shu);
        }
    }
}
