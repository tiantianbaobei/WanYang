package com.wanyangproject.myadapter;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.wanyangproject.R;
import com.wanyangproject.entity.QiYeYuanGongXinXiEntity;
import com.wanyangproject.entity.XiTongXiaoXiEntity;
import com.wanyangproject.my.WuYeYuanGongXinXiActivity;
import com.wanyangproject.utils.ContractUtils;
import com.wanyangproject.widget.suspensionindexbar.IndexBar;
import com.zhy.http.okhttp.OkHttpUtils;
import com.zhy.http.okhttp.callback.StringCallback;

import java.util.List;

import okhttp3.Call;

/**
 * Created by 甜甜 on 2018/8/2.
 */

public class WuYeYuanGongXinXiAdapter extends RecyclerView.Adapter<WuYeYuanGongXinXiAdapter.ViewHolder>{

    private Context context;
    private List<QiYeYuanGongXinXiEntity.ResponseBean> response;
    private ImageView imageBianji;
    private TextView tvSheweiMenwei;
    private ImageView imageSousuo;
    private IndexBar zimu;

    public WuYeYuanGongXinXiAdapter(Context context, List<QiYeYuanGongXinXiEntity.ResponseBean> response, ImageView imageBianji, TextView tvSheweiMenwei, ImageView imageSousuo, IndexBar zimu) {
        this.context = context;
        this.response = response;
        this.imageBianji = imageBianji;
        this.tvSheweiMenwei = tvSheweiMenwei;
        this.imageSousuo = imageSousuo;
        this.zimu = zimu;
    }




    @Override
    public WuYeYuanGongXinXiAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_wuye_yuangong_xinxi,parent,false);
        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }





    @Override
    public void onBindViewHolder(final WuYeYuanGongXinXiAdapter.ViewHolder holder, final int position) {

        if(response.get(position).getAvatar() == null){

        }else{
            Glide.with(context).load(response.get(position).getAvatar()).into(holder.image_touxiang);
        }



        if(response.get(position).getPersonName() == null){

        }else{
            holder.tv_name.setText(response.get(position).getPersonName());
        }


        if(response.get(position).getSex() == null){

        }else{
            holder.tv_sex.setText(response.get(position).getSex());
        }



        if(response.get(position).getPhone() == null){

        }else{
            holder.tv_phone.setText("电话："+response.get(position).getPhone());
        }



        if(response.get(position).getEntryTime() == null){

        }else{
            holder.tv_time.setText(response.get(position).getEntryTime());
        }


        if(response.get(position).getDormitoryName() == null){

        }else{
            holder.tv_sushe.setText("宿舍："+response.get(position).getDormitoryName());
        }








        if (response.get(position).isZhuangtai()){
            holder.image_weixuanzhong.setVisibility(View.VISIBLE);
        }else{
            holder.image_weixuanzhong.setVisibility(View.GONE);
        }




        if (response.get(position).isZhuangtai1()){
            holder.image_weixuanzhong.setButtonDrawable(R.mipmap.checked);
        }else {
            holder.image_weixuanzhong.setButtonDrawable(R.mipmap.unchecked);
        }


        holder.image_weixuanzhong.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (holder.image_weixuanzhong.isChecked()){
                    holder.image_weixuanzhong.setButtonDrawable(R.mipmap.checked);
                    response.get(position).setZhuangtai1(true);
                }else {
                    holder.image_weixuanzhong.setButtonDrawable(R.mipmap.unchecked);
                    response.get(position).setZhuangtai1(false);
                }
            }
        });



//        编辑按钮
        imageBianji.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                imageBianji.setVisibility(View.GONE);
                imageSousuo.setVisibility(View.GONE);
                zimu.setVisibility(View.GONE);
                tvSheweiMenwei.setVisibility(View.VISIBLE);


                for (QiYeYuanGongXinXiEntity.ResponseBean responseBean:response){
                    responseBean.setZhuangtai(true);
                }
                notifyDataSetChanged();
            }
        });


        tvSheweiMenwei.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String id = "";
                String phone = "";
                for (QiYeYuanGongXinXiEntity.ResponseBean responseBean:response){
//                    responseBean.setZhuangtai(true);
                    if(responseBean.isZhuangtai1() == true){
                        id = id + responseBean.getParkPersonId()+",";
                        phone = phone + responseBean.getPhone()+",";
                    }

                }
                notifyDataSetChanged();

//                设为门卫的网络请求
                initSheWeiMenWeiHttp(id,phone);
            }
        });
    }





//    设为门卫的网络请求
    private void initSheWeiMenWeiHttp(String id,String phone) {
        OkHttpUtils.post()
                .url(ContractUtils.LOGIN_URL+"user/setMenwei")
                .addHeader("token",ContractUtils.getTOKEN(context))
                .addParams("parkPersonId",id)
                .addParams("phone",phone)
                .build()
                .execute(new StringCallback() {
                    @Override
                    public void onError(Call call, Exception e, int id) {

                    }

                    @Override
                    public void onResponse(String response, int id) {
                        System.out.println(response+"          设为门卫的网络请求 ");
                        ContractUtils.Code500(context,response);
//                        if(response.indexOf("200") != -1{
//
//                        }
                    }
                });
    }



    @Override
    public int getItemCount() {
        return response.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView tv_name,tv_menwei,tv_time,tv_phone,tv_sushe,tv_sex;
        private ImageView image_touxiang;
        private CheckBox  image_weixuanzhong;
        private View view;
        public ViewHolder(View itemView) {
            super(itemView);
            view = itemView;
            tv_name = itemView.findViewById(R.id.tv_name);
            tv_menwei = itemView.findViewById(R.id.tv_menwei);
            tv_time = itemView.findViewById(R.id.tv_time);
            tv_phone = itemView.findViewById(R.id.tv_phone);
            tv_sushe = itemView.findViewById(R.id.tv_sushe);
            image_weixuanzhong = itemView.findViewById(R.id.image_weixuanzhong);
            image_touxiang = itemView.findViewById(R.id.image_touxiang);
            tv_sex = itemView.findViewById(R.id.tv_sex);
        }
    }
}
