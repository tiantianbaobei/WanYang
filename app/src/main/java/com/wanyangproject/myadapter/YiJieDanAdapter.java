package com.wanyangproject.myadapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.wanyangproject.R;
import com.wanyangproject.entity.ShangJiaDingDanEntity;

import java.util.List;

/**
 * Created by 甜甜 on 2018/8/4.
 */

public class YiJieDanAdapter extends RecyclerView.Adapter<YiJieDanAdapter.ViewHolder>{

    private Context context;
    private List<ShangJiaDingDanEntity.ResponseBean> response;

    public YiJieDanAdapter(Context context, List<ShangJiaDingDanEntity.ResponseBean> response) {
        this.context = context;
        this.response = response;
    }




//    点击准备完成的按钮
    private ZhunBeiWanChengClick zhunBeiWanChengClick;

    public ZhunBeiWanChengClick getZhunBeiWanChengClick() {
        return zhunBeiWanChengClick;
    }

    public void setZhunBeiWanChengClick(ZhunBeiWanChengClick zhunBeiWanChengClick) {
        this.zhunBeiWanChengClick = zhunBeiWanChengClick;
    }

    public interface ZhunBeiWanChengClick{
        void zhunbeiwanchengClick(int position,String Order_sn);
    }





    @Override
    public YiJieDanAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_yijiedan,parent,false);
        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final YiJieDanAdapter.ViewHolder holder, final int position) {
        holder.tv_wancheng.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                holder.tv_wancheng.setText("已完成");
                if(zhunBeiWanChengClick != null){
                    zhunBeiWanChengClick.zhunbeiwanchengClick(holder.getAdapterPosition(),response.get(position).getOrder_sn());
                }
            }
        });




        if(response.get(position).getAddres_type().equals("1")){
            holder.tv_shnagjia_peisong.setText("商家配送");
        }else if(response.get(position).getAddres_type().equals("2")){
            holder.tv_shnagjia_peisong.setText("到店自取");
        }



        if(response.get(position).getOrder_sn() == null){

        }else{
            holder.tv_dingdanbianhao.setText("订单编号："+response.get(position).getOrder_sn());
        }

        if(response.get(position).getAddres_add() == null){

        }else{
            if(response.get(position).getAddres_type().equals("1")){
                holder.tv_dizhi.setText(response.get(position).getAddres_add());
            }else if(response.get(position).getAddres_type().equals("2")){
                holder.tv_dizhi.setText("自取");
            }

        }






        if(response.get(position).getAddres_type().equals("2")){
            holder.tv_shijian.setText(response.get(position).getExtraction());
        }else{
            holder.tv_shijian.setText(response.get(position).getYuji());
        }



        if(response.get(position).getOrder_goods() == null){

        }else{
            String aaa = "";
            for (int i = 0; i < response.get(position).getOrder_goods().size(); i++) {
                ShangJiaDingDanEntity.ResponseBean.OrderGoodsBean orderGoodsBean = response.get(position).getOrder_goods().get(i);
                aaa = aaa+orderGoodsBean.getGoods_name();
                if(i != response.get(position).getOrder_goods().size() -1){
                    aaa = aaa+"\n";
                }
            }
            holder.tv_shangpin.setText(aaa);
        }


        if(response.get(position).getOrder_goods() == null){

        }else{
            String bbb = "";
            for (int i = 0; i < response.get(position).getOrder_goods().size(); i++) {
                ShangJiaDingDanEntity.ResponseBean.OrderGoodsBean orderGoodsBean1 = response.get(position).getOrder_goods().get(i);
                bbb = bbb + "x"+orderGoodsBean1.getNum();
                if(i != response.get(position).getOrder_goods().size() -1){
                    bbb = bbb+"\n";
                }
            }
            holder.tv_number.setText(bbb);
        }
    }

    @Override
    public int getItemCount() {
        return response.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView tv_dingdanbianhao,tv_shnagjia_peisong,tv_shijian,tv_shangpin_xinxi,tv_shangpin,tv_dizhi,tv_wancheng,tv_number;
        private View view;
        public ViewHolder(View itemView) {
            super(itemView);
            view = itemView;
            tv_dingdanbianhao = itemView.findViewById(R.id.tv_dingdanbianhao);
            tv_shnagjia_peisong = itemView.findViewById(R.id.tv_shnagjia_peisong);
            tv_shijian = itemView.findViewById(R.id.tv_shijian);
            tv_shangpin_xinxi = itemView.findViewById(R.id.tv_shangpin_xinxi);
            tv_shangpin = itemView.findViewById(R.id.tv_shangpin);
            tv_dizhi = itemView.findViewById(R.id.tv_dizhi);
            tv_wancheng = itemView.findViewById(R.id.tv_wancheng);
            tv_number = itemView.findViewById(R.id.tv_number);
        }
    }
}
