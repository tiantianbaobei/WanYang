package com.wanyangproject.myadapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.wanyangproject.R;
import com.wanyangproject.entity.QiYeShuiDianEntity;
import com.wanyangproject.entity.QiYeShuiDianJiLuEntity;

import java.util.List;

/**
 * Created by 甜甜 on 2018/7/31.
 */

public class ShuiAdapter extends RecyclerView.Adapter<ShuiAdapter.ViewHolder>{

    private Context context;
    private  List<QiYeShuiDianJiLuEntity.ResponseBean.DataBean.ListBean> list;



    public ShuiAdapter(Context context, List<QiYeShuiDianJiLuEntity.ResponseBean.DataBean.ListBean> list) {
        this.context = context;
        this.list = list;
    }



    @Override
    public ShuiAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_shui,parent,false);
        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ShuiAdapter.ViewHolder holder, int position) {
        if(list.get(position).getMonth() == null){

        }else{
            holder.tv_time.setText("("+list.get(position).getName()+")  "+list.get(position).getMonth());
        }


        if(list.get(position).getCost() == null){

        }else{
            holder.tv_number.setText(list.get(position).getCost()+"吨");
        }



    }

    @Override
    public int getItemCount() {
        if(list == null){
            return 0;
        }
        return list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView tv_time,tv_number;
        private View view;
        public ViewHolder(View itemView) {
            super(itemView);
            view = itemView;
            tv_time = itemView.findViewById(R.id.tv_time);
            tv_number = itemView.findViewById(R.id.tv_number);
        }
    }
}
