package com.wanyangproject.myadapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.wanyangproject.R;
import com.wanyangproject.my.WuYeQiYeGuanLiActivity;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by 甜甜 on 2018/8/2.
 */

public class WuYeQiYeGuanLiAdapter extends RecyclerView.Adapter<WuYeQiYeGuanLiAdapter.ViewHolder>{

    private Context context;
    private ArrayList<JSONObject> jsonArrayFenZu;
    private ArrayList<String> arrayList = new ArrayList<>();
    private HashMap<Integer,String> map = new HashMap<>();

    public WuYeQiYeGuanLiAdapter(Context context, ArrayList<JSONObject> jsonArrayFenZu) {
        this.context = context;
        this.jsonArrayFenZu = jsonArrayFenZu;
    }



    public QiYGuanLiClick qiYeGuanLiClick;

    public void setQiYeGuanLiClick(QiYGuanLiClick qiYeGuanLiClick) {
        this.qiYeGuanLiClick = qiYeGuanLiClick;
    }

    public interface QiYGuanLiClick {
        void qiyeguanliclick(int position,String id);
    }



    @Override
    public WuYeQiYeGuanLiAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_wuye_qiye_guanli, parent, false);
        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final WuYeQiYeGuanLiAdapter.ViewHolder holder, final int position) {



        holder.parkenterprise_ll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    String enterId = jsonArrayFenZu.get(position).getString("enterId");
                    if(enterId == null){

                    }else{
                        if(qiYeGuanLiClick != null){
                            qiYeGuanLiClick.qiyeguanliclick(holder.getAdapterPosition(),enterId);
                        }

                    }
                } catch (JSONException e) {
                }
            }
        });







        //        字母索引上面
        try {
            if(arrayList.contains(jsonArrayFenZu.get(position).getString("initials"))){
//                holder.relative_zimu.setVisibility(View.GONE);

                if(map.containsKey(position)){
                    holder.relative_zimu.setVisibility(View.VISIBLE);
                    String s = map.get(position);
                    holder.tv_zimu.setText(s);
                }else{
                    holder.relative_zimu.setVisibility(View.GONE);
                }



            }else{
                holder.relative_zimu.setVisibility(View.VISIBLE);
                arrayList.add(jsonArrayFenZu.get(position).getString("initials"));
                map.put(position, jsonArrayFenZu.get(position).getString("initials"));
                holder.tv_zimu.setText(jsonArrayFenZu.get(position).getString("initials"));
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }









        try {
            String touxiang = jsonArrayFenZu.get(position).getString("logoUrl");
            if(touxiang == null){

            }else{ // 头像
                Glide.with(context).load(touxiang).into(holder.image_touxiang);
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }


        try {
            String title = jsonArrayFenZu.get(position).getString("enterName");
            if(title == null){

            }else{ // 标题
                holder.tv_title.setText(title);
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }

        try {
            String address = jsonArrayFenZu.get(position).getString("address");
            if(address == null){

            }else{  //  企业位置
                holder.tv_address.setText("企业位置："+address);
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }


    }

    @Override
    public int getItemCount() {
        return jsonArrayFenZu.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView tv_title,tv_address,tv_zimu;
        private ImageView image_touxiang;
        private RelativeLayout parkenterprise_ll,relative_zimu;
        private View view;
        public ViewHolder(View itemView) {
            super(itemView);
            view = itemView;
            image_touxiang = itemView.findViewById(R.id.image_touxiang);
            tv_title = itemView.findViewById(R.id.tv_title);
            tv_address = itemView.findViewById(R.id.tv_address);
            parkenterprise_ll = itemView.findViewById(R.id.parkenterprise_ll);
            relative_zimu = itemView.findViewById(R.id.relative_zimu);
            tv_zimu = itemView.findViewById(R.id.tv_zimu);
        }
    }
}
