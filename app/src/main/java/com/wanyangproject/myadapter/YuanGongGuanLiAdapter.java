package com.wanyangproject.myadapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.wanyangproject.R;
import com.wanyangproject.entity.QiYeYuanGongXinXiEntity;
import com.wanyangproject.my.MyYuanGongXinXiActivity;
import com.wanyangproject.utils.ContractUtils;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by 甜甜 on 2018/7/31.
 */
//   企业员工信息
public class YuanGongGuanLiAdapter extends RecyclerView.Adapter<YuanGongGuanLiAdapter.ViewHolder>{

    private Context context;
    private ArrayList<JSONObject> response;

    public YuanGongGuanLiAdapter(Context context, ArrayList<JSONObject> response) {
        this.context = context;
        this.response = response;
    }

    @Override
    public YuanGongGuanLiAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_yuangongguanli,parent,false);
        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(YuanGongGuanLiAdapter.ViewHolder holder, int position) {


        try {
            String title = response.get(position).getString("personName");
            if(title == null){

            }else{ // 标题
                holder.tv_name.setText(title);
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }





        try {
            String sex = response.get(position).getString("sex");
            if(sex == null){

            }else{ // 性别
                holder.tv_sex.setText(sex);
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }





        try {
            String phone = response.get(position).getString("phone");
            if(phone == null){

            }else{ // 电话
                holder.tv_phone.setText("电话："+phone);
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }





        try {
            String entryTime = response.get(position).getString("entryTime");
            if(entryTime == null){

            }else{ // 时间
                holder.tv_time.setText(entryTime+"入园");
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }



        try {
            String dormitoryName = response.get(position).getString("dormitoryName");
            if(dormitoryName == null){

            }else{ // 宿舍
                holder.tv_sushe.setText(dormitoryName);
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }



        try {
            String avatar = response.get(position).getString("avatar");
            if(avatar == null){

            }else{ // 头像
                Glide.with(context).load(ContractUtils.PHOTO_URL+avatar).into(holder.image_touxiang);
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }

//
//
//        if(response.get(position).getAvatar() == null){
//
//        }else{
//            Glide.with(context).load(ContractUtils.PHOTO_URL+response.get(position).getAvatar()).into(holder.image_touxiang);
//        }



//
//        if(response.get(position).getPersonName() == null){
//
//        }else{
//            holder.tv_name.setText(response.get(position).getPersonName());
//        }

//
//        if(response.get(position).getSex() == null){
//
//        }else{
//            holder.tv_sex.setText(response.get(position).getSex());
//        }
//


//        if(response.get(position).getPhone() == null){
//
//        }else{
//            holder.tv_phone.setText("电话："+response.get(position).getPhone());
//        }
//

//
//        if(response.get(position).getEntryTime() == null){
//
//        }else{
//            holder.tv_time.setText(response.get(position).getEntryTime()+"入园");
//        }

//
//        if(response.get(position).getDormitoryName() == null){
//
//        }else{
//            holder.tv_sushe.setText("宿舍："+response.get(position).getDormitoryName());
//        }
//
//

    }

    @Override
    public int getItemCount() {
        if(response == null){
            return 0;
        }
        return response.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private ImageView image_touxiang;
        private TextView tv_name,tv_time,tv_phone,tv_sushe,tv_sex;
        private View view;
        public ViewHolder(View itemView) {
            super(itemView);
            view = itemView;
            image_touxiang = itemView.findViewById(R.id.image_touxiang);
            tv_name = itemView.findViewById(R.id.tv_name);
            tv_time = itemView.findViewById(R.id.tv_time);
            tv_phone = itemView.findViewById(R.id.tv_phone);
            tv_sushe = itemView.findViewById(R.id.tv_sushe);
            tv_sex = itemView.findViewById(R.id.tv_sex);
        }
    }
}
