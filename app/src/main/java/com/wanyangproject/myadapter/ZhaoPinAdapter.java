package com.wanyangproject.myadapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.wanyangproject.R;
import com.wanyangproject.entity.QiYeZhaoPinGuanLiEntity;
import com.wanyangproject.my.ZhaoPinGuanLiActivity;

import java.util.List;

/**
 * Created by 甜甜 on 2018/8/1.
 */

public class ZhaoPinAdapter extends RecyclerView.Adapter<ZhaoPinAdapter.ViewHolder>{

    private Context context;
    private List<QiYeZhaoPinGuanLiEntity.ResponseBean> response;

    public ZhaoPinAdapter(Context context, List<QiYeZhaoPinGuanLiEntity.ResponseBean> response) {
        this.context = context;
        this.response = response;
    }


//    招聘详情
    private ZhaoPinClick zhaoPinClick;

    public void setZhaoPinClick(ZhaoPinClick zhaoPinClick) {
        this.zhaoPinClick = zhaoPinClick;
    }

    public interface ZhaoPinClick{
        void zhaopinClick(int position);
}

    @Override
    public ZhaoPinAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_zhaopin,parent,false);
        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final ZhaoPinAdapter.ViewHolder holder, int position) {
        holder.linear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(zhaoPinClick != null){
                    zhaoPinClick.zhaopinClick(holder.getAdapterPosition());
                }
            }
        });




        if(response.get(position).getTitle() == null){

        }else{
            holder.tv_title.setText(response.get(position).getTitle());
        }



        if(response.get(position).getMoney().equals("")){
            holder.tv_money.setText("薪资面议");
        }else{
            if(response.get(position).getMoney() != null){
                holder.tv_money.setText(response.get(position).getMoney());
            }
        }


        if(response.get(position).getAddress() != null){//地址
            holder.tv_address.setText("地址："+response.get(position).getAddress());
        }

        if(response.get(position).getEnterName() != null){//公司
            holder.tv_gongsi.setText("公司："+response.get(position).getEnterName());
        }


    }

    @Override
    public int getItemCount() {
        return response.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView tv_title,tv_money,tv_delete,tv_gongsi,tv_address;
//        tv_miaoshu,
        private LinearLayout linear;
        private View view;
        public ViewHolder(View itemView) {
            super(itemView);
            view = itemView;
            tv_title = itemView.findViewById(R.id.tv_title);
            tv_money = itemView.findViewById(R.id.tv_money);
            tv_gongsi = itemView.findViewById(R.id.tv_gongsi);
            tv_address = itemView.findViewById(R.id.tv_address);
            tv_delete = itemView.findViewById(R.id.tv_delete);
            linear = itemView.findViewById(R.id.linear);
//            tv_miaoshu = itemView.findViewById(R.id.tv_miaoshu);
        }
    }
}
