package com.wanyangproject.myadapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.wanyangproject.R;
import com.wanyangproject.entity.TiXianLieBiaoEntity;
import com.wanyangproject.my.TiXianActivity;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * Created by 甜甜 on 2018/8/4.
 */

public class TiXianAdapter extends RecyclerView.Adapter<TiXianAdapter.ViewHolder>{

    private Context context;
    private List<TiXianLieBiaoEntity.ResponseBean> response;

    public TiXianAdapter(Context context, List<TiXianLieBiaoEntity.ResponseBean> response) {
        this.context = context;
        this.response = response;
    }




        /*
        * 将时间戳转换为时间
         */
    public String stampToDate(String s) {
        if(s == null){
            return "";
        }

        if(s.length() == 0){
            return "";
        }

        if(s.indexOf("-") != -1){
            return "";
        }
        String res;
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm");
        long lt = new Long(s);
        Date date = new Date(lt*1000);
        res = simpleDateFormat.format(date);
        return res;
    }




    @Override
    public TiXianAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_tixian_jilu,parent,false);
        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(TiXianAdapter.ViewHolder holder, int position) {
        if(response.get(position).getAlipay() == null){

        }else{ // 提现账号
            holder.tv_title.setText(response.get(position).getAlipay());
        }


        if(response.get(position).getMoney() == null){

        }else{  // 提现的金额
            holder.tv_number.setText(response.get(position).getMoney());
        }


        if(response.get(position).getType() == null){

        }else{
            if(response.get(position).getType().equals("1")){
                holder.tv_shenhe.setText("审核中");
            }
            if(response.get(position).getType().equals("2")){
                holder.tv_shenhe.setText("审核通过");
            }
            if(response.get(position).getType().equals("3")){
                holder.tv_shenhe.setText("拒绝提现");
            }
        }






        if(response.get(position).getTime() == null){

        }else{
            holder.tv_time.setText(stampToDate(String.valueOf(response.get(position).getTime())));
        }

    }

    @Override
    public int getItemCount() {
        return response.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView tv_title,tv_time,tv_number,tv_shenhe;
        private View view;
        public ViewHolder(View itemView) {
            super(itemView);
            view = itemView;
            tv_title = itemView.findViewById(R.id.tv_title);
            tv_time = itemView.findViewById(R.id.tv_time);
            tv_number = itemView.findViewById(R.id.tv_number);
            tv_shenhe = itemView.findViewById(R.id.tv_shenhe);
        }
    }
}
