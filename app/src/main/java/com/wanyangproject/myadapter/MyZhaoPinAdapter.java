package com.wanyangproject.myadapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.wanyangproject.R;
import com.wanyangproject.entity.ZhanPinLieBiaoEntity;

import java.util.List;

/**
 * Created by 甜甜 on 2018/7/31.
 */

public class MyZhaoPinAdapter extends RecyclerView.Adapter<MyZhaoPinAdapter.ViewHolder>{

    private Context context;
    private List<ZhanPinLieBiaoEntity.ResponseBean> response;

    public MyZhaoPinAdapter(Context context, List<ZhanPinLieBiaoEntity.ResponseBean> response) {
        this.context = context;
        this.response = response;
    }



//    删除
    private ShanChuClick shanChuClick;

    public void setShanChuClick(ShanChuClick shanChuClick) {
        this.shanChuClick = shanChuClick;
    }

    public interface ShanChuClick{
        void shamchuClick(int position,String id);
}


//     详情
    private XiangQingClick xiangQingClick;

    public void setXiangQingClick(XiangQingClick xiangQingClick) {
        this.xiangQingClick = xiangQingClick;
    }

    public interface XiangQingClick{
        void xiangqingClick(int position,String id);
}



    @Override
    public MyZhaoPinAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_zhaopin,parent,false);
        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final MyZhaoPinAdapter.ViewHolder holder, final int position) {

        holder.linear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(xiangQingClick != null){
                    xiangQingClick.xiangqingClick(holder.getAdapterPosition(),response.get(position).getId());
                }
            }
        });







        if(response.get(position).getTitle() == null){

        }else{
            holder.tv_title.setText(response.get(position).getTitle());
        }


        if(response.get(position).getMoney() == null){

        }else{
            holder.tv_money.setText(response.get(position).getMoney());
        }



        if(response.get(position).getAddress() == null){

        }else{
            holder.tv_address.setText("地址："+response.get(position).getAddress());
        }


        if(response.get(position).getEnterName() == null){

        }else{
            holder.tv_gongsi.setText("公司："+response.get(position).getEnterName());
        }





        holder.tv_delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(shanChuClick != null){
                    shanChuClick.shamchuClick(holder.getAdapterPosition(),response.get(position).getId());
                }
            }
        });






//        删除
//        holder.tv_delete.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                if(shanChuClick != null){
//                    shanChuClick.shamchuClick(holder.getAdapterPosition(),);
//                }
//            }
//        });
    }

    @Override
    public int getItemCount() {
        return response.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView tv_title,tv_money,tv_gongsi,tv_address,tv_delete;
        private LinearLayout linear;
        private View view;
        public ViewHolder(View itemView) {
            super(itemView);
            view = itemView;
            tv_title = itemView.findViewById(R.id.tv_title);
            tv_money = itemView.findViewById(R.id.tv_money);
            tv_gongsi = itemView.findViewById(R.id.tv_gongsi);
            tv_address = itemView.findViewById(R.id.tv_address);
            tv_delete = itemView.findViewById(R.id.tv_delete);
            linear = itemView.findViewById(R.id.linear);
        }
    }
}
