package com.wanyangproject.myadapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.wanyangproject.R;
import com.wanyangproject.adapter.DingDanDaiPingJiaAdapter;
import com.wanyangproject.adapter.DingDanDaiShouHuoAdapter;
import com.wanyangproject.entity.DaiPingJiaEntity;
import com.wanyangproject.entity.DaiShouHuoEntity;
import com.wanyangproject.my.DaiPingJiaXiangQingActivity;

import java.util.List;

/**
 * Created by 甜甜 on 2018/7/30.
 */

public class DaiPingJiaAdapter extends RecyclerView.Adapter<DaiPingJiaAdapter.ViewHolder>{

    private Context context;
    private List<DaiPingJiaEntity.ResponseBean> response;

    public DaiPingJiaAdapter(Context context, List<DaiPingJiaEntity.ResponseBean> response) {
        this.context = context;
        this.response = response;
    }


//    点击详情
    private DaiPingJiaClick daiPingJiaClick;

    public void setDaiPingJiaClick(DaiPingJiaClick daiPingJiaClick) {
        this.daiPingJiaClick = daiPingJiaClick;
    }

    public interface DaiPingJiaClick{
        void daipingjiaclick(int position);
    }



//    点击去评价
    private QuPingJiaClick quPingJiaClick;

    public void setQuPingJiaClick(QuPingJiaClick quPingJiaClick) {
        this.quPingJiaClick = quPingJiaClick;
    }

    public interface QuPingJiaClick{
        void qupungjiaClick(int position,String dingdanhao);
    }





    @Override
    public DaiPingJiaAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.dingdan_item_daipingjia,parent,false);
        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final DaiPingJiaAdapter.ViewHolder holder, final int position) {
//        点击进入详情
        holder.linear_pingjia.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(daiPingJiaClick != null){
                    daiPingJiaClick.daipingjiaclick(holder.getAdapterPosition());
                }
            }
        });


//        点击去评价
        holder.btn_pingjia.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(quPingJiaClick != null){
                    quPingJiaClick.qupungjiaClick(holder.getAdapterPosition(),response.get(position).getOrder_sn());
                }
            }
        });



        //        待评价中间的列表
        DingDanDaiPingJiaAdapter dingDanDaiPingJiaAdapter = new DingDanDaiPingJiaAdapter(context,response.get(position).getGoods());
        LinearLayoutManager manager = new LinearLayoutManager(context);
        holder.recyclerView.setLayoutManager(manager);
        holder.recyclerView.setAdapter(dingDanDaiPingJiaAdapter);

        dingDanDaiPingJiaAdapter.setDingDanDaiPingJiaClick(new DingDanDaiPingJiaAdapter.DingDanDaiPingJiaClick() {
            @Override
            public void daingdanDaiPingJiaClick(String id) {
                Intent intent = new Intent(context, DaiPingJiaXiangQingActivity.class);
                intent.putExtra("dingdanhao",response.get(position).getOrder_sn());
                context.startActivity(intent);
            }
        });




        if(response.get(position).getShop_name() == null){

        }else{ // 商家店名
            holder.tv_name.setText(response.get(position).getShop_name());
        }



        int sum = 0;
        float quanbu = 0;
        for (int i = 0; i < response.get(position).getGoods().size(); i++) {
            DaiPingJiaEntity.ResponseBean.GoodsBean goodsBean = response.get(position).getGoods().get(i);
            sum += Integer.parseInt(goodsBean.getNum());
            quanbu +=  Integer.parseInt(goodsBean.getNum())*Float.parseFloat(goodsBean.getPic());
        }
        if(sum+"" == null){

        }else{ //合计的商品的数量
            holder.tv_heji.setText("共计"+sum+"件商品  合计：");
        }



        if(response.get(position).getMoney() == null){

        }else{ // 合计的总钱数
            holder.tv_heji_money.setText(" ¥"+response.get(position).getMoney()+"  ");
        }


//        if(response.get(position).getShop_cost() == null){
//
//        }else{ //配送费
//            holder.tv_yunfei.setText(("含运费：¥"+response.get(position).getShop_cost()));
//        }



        float vmon = Float.parseFloat(response.get(position).getMoney());
        if (quanbu < vmon){
            holder.tv_yunfei.setText(("(含运费：¥"+response.get(position).getShop_cost()+")"));
        }else{
            holder.tv_yunfei.setText(("(不含运费：¥"+response.get(position).getShop_cost()+")"));
        }


    }

    @Override
    public int getItemCount() {
        return response.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView tv_name,tv_zhuangtai,tv_neirong,tv_money,tv_number,tv_heji,tv_heji_money,tv_yunfei;
        private ImageView image_tupian;
        private Button btn_pingjia;
        private LinearLayout linear_pingjia;
        private RecyclerView recyclerView;
        private View view;
        public ViewHolder(View itemView) {
            super(itemView);
            view = itemView;
            tv_name = itemView.findViewById(R.id.tv_name);
            tv_zhuangtai = itemView.findViewById(R.id.tv_zhuangtai);
            tv_neirong = itemView.findViewById(R.id.tv_neirong);
            tv_money = itemView.findViewById(R.id.tv_money);
            tv_number = itemView.findViewById(R.id.tv_number);
            tv_heji = itemView.findViewById(R.id.tv_heji);
            tv_heji_money = itemView.findViewById(R.id.tv_heji_money);
            tv_yunfei = itemView.findViewById(R.id.tv_yunfei);
            image_tupian = itemView.findViewById(R.id.image_tupian);
            btn_pingjia = itemView.findViewById(R.id.btn_pingjia);
            linear_pingjia = itemView.findViewById(R.id.linear_pingjia);
            recyclerView = itemView.findViewById(R.id.recyclerView);
        }
    }
}
