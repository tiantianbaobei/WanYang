package com.wanyangproject.myadapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.wanyangproject.R;
import com.wanyangproject.entity.ShouCangShangJiaXianShiEntity;
import com.wanyangproject.utils.ContractUtils;

import java.util.List;

/**
 * Created by 甜甜 on 2018/7/31.
 */

public class MyShangJiaAdapter extends RecyclerView.Adapter<MyShangJiaAdapter.ViewHolder>{

    private Context context;
    private List<ShouCangShangJiaXianShiEntity.ResponseBean> response;

    public MyShangJiaAdapter(Context context, List<ShouCangShangJiaXianShiEntity.ResponseBean> response) {
        this.context = context;
        this.response =  response;
    }



//    删除
    private ShanChuClick shanChuClick;

    public void setShanChuClick(ShanChuClick shanChuClick) {
        this.shanChuClick = shanChuClick;
    }

    public interface ShanChuClick{
        void shanchuClick(int position,String id);
}


//    详情
    private XiangQIngClick xiangQIngClick;

    public void setXiangQIngClick(XiangQIngClick xiangQIngClick) {
        this.xiangQIngClick = xiangQIngClick;
    }

    public interface XiangQIngClick{
        void xiangqingClick(int position,String id);
}



    @Override
    public MyShangJiaAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_shangjia,parent,false);
        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final MyShangJiaAdapter.ViewHolder holder, final int position) {

//        删除
        holder.tv_delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(shanChuClick != null){
                    shanChuClick.shanchuClick(holder.getAdapterPosition(),response.get(position).getId());
                }
            }
        });



//        进入详请
        holder.linear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(xiangQIngClick != null){
                    xiangQIngClick.xiangqingClick(holder.getAdapterPosition(),response.get(position).getId());
                }
            }
        });




        if(ContractUtils.PHOTO_URL+response.get(position).getPhoto() == null){

        }else{
            Glide.with(context).load(ContractUtils.PHOTO_URL+response.get(position).getPhoto()).into(holder.image_shangdian);
        }

        if(response.get(position).getNickname() == null){

        }else{
            holder.tv_title.setText(response.get(position).getNickname());
        }

        if(response.get(position).getSales() ==null){

        }else{
            holder.tv_yuexiao.setText("月销："+response.get(position).getSales());
        }

        if(response.get(position).getStarting() == null && response.get(position).getCost() == null){

        }else{
            holder.tv_peisong.setText("起送价¥"+response.get(position).getStarting()+" | 配送费¥"+response.get(position).getCost());
        }
    }

    @Override
    public int getItemCount() {
        return response.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private ImageView image_shangdian;
        private TextView tv_title,tv_yuexiao,tv_peisong,tv_delete;
        private LinearLayout linear;
        private View view;
        public ViewHolder(View itemView) {
            super(itemView);
            view = itemView;
            image_shangdian = itemView.findViewById(R.id.image_shangdian);
            tv_title = itemView.findViewById(R.id.tv_title);
            tv_yuexiao = itemView.findViewById(R.id.tv_yuexiao);
            tv_peisong = itemView.findViewById(R.id.tv_peisong);
            tv_delete = itemView.findViewById(R.id.tv_delete);
            linear = itemView.findViewById(R.id.linear);
        }
    }
}
