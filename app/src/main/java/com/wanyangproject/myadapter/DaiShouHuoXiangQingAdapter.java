package com.wanyangproject.myadapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.wanyangproject.R;
import com.wanyangproject.entity.DingDanXiangQingEntity;
import com.wanyangproject.my.DaiShouHuoXiangQingActivity;
import com.wanyangproject.utils.ContractUtils;

import java.util.List;

/**
 * Created by 甜甜 on 2018/7/30.
 */

public class DaiShouHuoXiangQingAdapter extends RecyclerView.Adapter<DaiShouHuoXiangQingAdapter.ViewHolder>{

    private Context context;
    private List<DingDanXiangQingEntity.ResponseBean.GoodsBean> goods;

    public DaiShouHuoXiangQingAdapter(Context context, List<DingDanXiangQingEntity.ResponseBean.GoodsBean> goods) {
        this.context = context;
        this.goods = goods;
    }




    private TuiKuanClick tuiKuanClick;

    public void setTuiKuanClick(TuiKuanClick tuiKuanClick) {
        this.tuiKuanClick = tuiKuanClick;
    }

    public interface TuiKuanClick{
        void TuiKuanclick(int position);
    }




    @Override
    public DaiShouHuoXiangQingAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_shouhuo_xiangqing,parent,false);
        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final DaiShouHuoXiangQingAdapter.ViewHolder holder, int position) {
//        holder.tv_tuikuan.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                if(tuiKuanClick != null){
//                    tuiKuanClick.TuiKuanclick(holder.getAdapterPosition());
//                }
//            }
//        });



        if(ContractUtils.PHOTO_URL+goods.get(position).getMaster() == null){

        }else{
            Glide.with(context).load(ContractUtils.PHOTO_URL+goods.get(position).getMaster()).into(holder.image_touxiang);
        }

        if(goods.get(position).getNum() == null){

        }else{
            holder.tv_number.setText("x"+goods.get(position).getNum());
        }


        if(goods.get(position).getGoods_name() == null){

        }else{
            holder.tv_name.setText(goods.get(position).getGoods_name());
        }


        if(goods.get(position).getPic() == null){

        }else{
            holder.tv_money.setText("¥"+goods.get(position).getPic());
        }



    }

    @Override
    public int getItemCount() {
        return goods.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private ImageView image_touxiang,image_dingwei;
        private TextView tv_title,tv_name,tv_money,tv_number,tv_peisongfei,tv_beizhu,tv_dingdan_bianhao,tv_chuanjianshijian,
                tv_shouhuoren,tv_shoujihao,tv_address,tv_tuikuan;
        private View view;
        public ViewHolder(View itemView) {
            super(itemView);
            view = itemView;
            image_touxiang = itemView.findViewById(R.id.image_touxiang);
            image_dingwei = itemView.findViewById(R.id.image_dingwei);
            tv_title = itemView.findViewById(R.id.tv_title);
            tv_name = itemView.findViewById(R.id.tv_name);
            tv_money = itemView.findViewById(R.id.tv_money);
            tv_number = itemView.findViewById(R.id.tv_number);
            tv_peisongfei = itemView.findViewById(R.id.tv_peisongfei);
            tv_beizhu = itemView.findViewById(R.id.tv_beizhu);
            tv_dingdan_bianhao = itemView.findViewById(R.id.tv_dingdan_bianhao);
            tv_chuanjianshijian = itemView.findViewById(R.id.tv_chuanjianshijian);
            tv_shouhuoren = itemView.findViewById(R.id.tv_shouhuoren);
            tv_shoujihao = itemView.findViewById(R.id.tv_shoujihao);
            tv_address = itemView.findViewById(R.id.tv_address);
            tv_tuikuan = itemView.findViewById(R.id.tv_tuikuan);
        }
    }
}
