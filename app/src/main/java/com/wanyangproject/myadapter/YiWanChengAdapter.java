package com.wanyangproject.myadapter;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.wanyangproject.R;
import com.wanyangproject.entity.ShangJiaDingDanEntity;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * Created by 甜甜 on 2018/8/4.
 */

public class YiWanChengAdapter extends RecyclerView.Adapter<YiWanChengAdapter.ViewHolder>{

    private Context context;
    private List<ShangJiaDingDanEntity.ResponseBean> response;

    public YiWanChengAdapter(Context context,List<ShangJiaDingDanEntity.ResponseBean> response) {
        this.context = context;
        this.response = response;
    }

    @Override
    public YiWanChengAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_yiwancheng,parent,false);
        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }



    @Override
    public void onBindViewHolder(YiWanChengAdapter.ViewHolder holder, int position) {
        if(response.get(position).getOrder_sn() == null){

        }else{
            holder.tv_dingdanbianhao.setText("订单编号："+response.get(position).getOrder_sn());
        }

        if(response.get(position).getAddres_add() == null){

        }else{
            if(response.get(position).getAddres_type().equals("1")){
                holder.tv_dizhi.setText(response.get(position).getAddres_add());
            }else if(response.get(position).getAddres_type().equals("2")){
                holder.tv_dizhi.setText("自取");
            }
        }




        if(response.get(position).getOrder_goods() == null){

        }else{
            String aaa = "";
            for (int i = 0; i < response.get(position).getOrder_goods().size(); i++) {
                ShangJiaDingDanEntity.ResponseBean.OrderGoodsBean orderGoodsBean = response.get(position).getOrder_goods().get(i);
                aaa = aaa+orderGoodsBean.getGoods_name();
                if(i != response.get(position).getOrder_goods().size()-1){
                    aaa = aaa+"\n";
                }
            }
            holder.tv_shangpin.setText(aaa);
        }





        if(response.get(position).getOrder_goods() == null){

        }else{
            String bbb = "";
            for (int i = 0; i < response.get(position).getOrder_goods().size(); i++) {
                ShangJiaDingDanEntity.ResponseBean.OrderGoodsBean orderGoodsBean1 = response.get(position).getOrder_goods().get(i);
                bbb = bbb + "x"+orderGoodsBean1.getNum()+"";
                if(i != response.get(position).getOrder_goods().size() -1){
                    bbb = bbb +"\n";
                }
            }
            holder.tv_number.setText(bbb);
        }


        if(response.get(position).getType() == null){

        }else{
//            if(response.get(position).getType().equals("7")){
//                holder.tv_dengdai_shouhuo.setText("等待确认收货");
//                holder.tv_dengdai_shouhuo.setTextColor(Color.parseColor("#ff0000"));
//            }else
                if(response.get(position).getType().equals("7") || response.get(position).getType().equals("8") || response.get(position).getType().equals("9")){
                holder.tv_dengdai_shouhuo.setText("已完成");
                holder.tv_dengdai_shouhuo.setTextColor(Color.parseColor("#a8aeb6"));
            }
        }




//        if(response.get(position).getType().equals("7") || response.get(position).getType().equals("8")){
//            holder.tv_time.setText("送达时间：");
//        }







//
//        if(response.get(position).getType() == null){
//
//        }else{
//            if(response.get(position).getType().equals("8") || response.get(position).getType().equals("8")){
//                holder.tv_dengdai_shouhuo.setText("已完成");
//                holder.tv_dengdai_shouhuo.setTextColor(Color.parseColor("#a8aeb6"));
//            }
//        }




//        if(response.get(position).getAddres_type().equals("2")){
//            holder.tv_shijian.setText(response.get(position).getExtraction());
//        }else{
//            holder.tv_shijian.setText(response.get(position).getYuji());
//        }



//
//        if(response.get(position).getQueren_time().equals("")){
//            holder.tv_shijian.setText("暂未送达");
//        }else{
//            holder.tv_shijian.setText(stampToDate(response.get(position).getQueren_time()));
//        }

        if(response.get(position).getYuji() == null){

        }else{
            holder.tv_shijian.setText(response.get(position).getYuji());
        }

    }








    /*
  * 将时间戳转换为时间
   */
    public String stampToDate(String s) {
        if (s == null) {
            return "";
        }

        if (s.length() == 0) {
            return "";
        }

        if (s.indexOf("-") != -1) {
            return "";
        }
        String res;
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm");
        long lt = new Long(s);
        Date date = new Date(lt*1000);
        res = simpleDateFormat.format(date);
        return res;
    }


    @Override
    public int getItemCount() {
        return response.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView tv_dingdanbianhao,tv_dengdai_shouhuo,tv_shijian,tv_shangpin,tv_dizhi,tv_number,tv_time;
        private View view;
        public ViewHolder(View itemView) {
            super(itemView);
            view = itemView;
            tv_dingdanbianhao = itemView.findViewById(R.id.tv_dingdanbianhao);
            tv_dengdai_shouhuo = itemView.findViewById(R.id.tv_dengdai_shouhuo);
            tv_shijian = itemView.findViewById(R.id.tv_shijian);
            tv_shangpin = itemView.findViewById(R.id.tv_shangpin);
            tv_dizhi = itemView.findViewById(R.id.tv_dizhi);
            tv_number = itemView.findViewById(R.id.tv_number);
            tv_time = itemView.findViewById(R.id.tv_time);
        }
    }
}
