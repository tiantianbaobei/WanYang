package com.wanyangproject.myadapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.wanyangproject.R;
import com.wanyangproject.activity.HomeActivity;
import com.wanyangproject.activity.RegisterActivity;
import com.wanyangproject.adapter.TuiKuanYuanYinShangMianAdapter;
import com.wanyangproject.entity.DaiShouHuoEntity;
import com.wanyangproject.entity.DingDanXiangQingEntity;
import com.wanyangproject.my.TuiKuanXiangQingActivity;

/**
 * Created by 甜甜 on 2018/7/30.
 */

public class TuiKuanXiangQingAdapter extends RecyclerView.Adapter<TuiKuanXiangQingAdapter.ViewHolder>{

    private Context context;
    private DingDanXiangQingEntity.ResponseBean responseBean;
    private String tuikuanyuanyin;

    public TuiKuanXiangQingAdapter(Context context, DingDanXiangQingEntity.ResponseBean responseBean, String tuikuanyuanyin) {
        this.context = context;
        this.responseBean = responseBean;
        this.tuikuanyuanyin = tuikuanyuanyin;
    }





//    撤销申请
    private TuiKuanCheXiaoClick tuiKuanCheXiaoClick;

    public void setTuiKuanCheXiaoClick(TuiKuanCheXiaoClick tuiKuanCheXiaoClick) {
        this.tuiKuanCheXiaoClick = tuiKuanCheXiaoClick;
    }

    public interface TuiKuanCheXiaoClick{
        void tuikuanchexiaoClick(int position);
    }

    @Override
    public TuiKuanXiangQingAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_tuikuan_xiangqing,parent,false);
        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final TuiKuanXiangQingAdapter.ViewHolder holder, int position) {
        holder.tv_chexiaoshenqing.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final AlertDialog alertDialog = new AlertDialog.Builder(context).create();
                alertDialog.show();
                alertDialog.getWindow().setContentView(R.layout.chexiao_tuikuan_alertdialog);
                alertDialog.getWindow().findViewById(R.id.tv_quxiao).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        alertDialog.dismiss();
                    }
                });

                alertDialog.getWindow().findViewById(R.id.tv_queding).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        alertDialog.dismiss();
                    }
                });
            }
        });



        //        申请退款界面上面的列表
        TuiKuanYuanYinShangMianAdapter tuiKuanYuanYinShangMianAdapter = new TuiKuanYuanYinShangMianAdapter(context,responseBean);
        LinearLayoutManager manager = new LinearLayoutManager(context);
        holder.recyclerView.setLayoutManager(manager);
        holder.recyclerView.setAdapter(tuiKuanYuanYinShangMianAdapter);




        if(tuikuanyuanyin ==null){

        }else{ //退款原因
            holder.tv_tuikun_yuanyin.setText("退款原因："+tuikuanyuanyin);
        }



        if(responseBean.getMoney() != null){
            holder.tv_tukuanjine.setText("退款金额："+responseBean.getMoney());
        }



    }

    @Override
    public int getItemCount() {
        return 1;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView tv_lianxi_maijia,tv_chexiaoshenqing,tv_title,tv_number,tv_tuikun_yuanyin,tv_tukuanjine;
        private ImageView image_touxiang;
        private RecyclerView recyclerView;
        private View view;
        public ViewHolder(View itemView) {
            super(itemView);
            view = itemView;
            tv_lianxi_maijia = itemView.findViewById(R.id.tv_lianxi_maijia);
            tv_chexiaoshenqing = itemView.findViewById(R.id.tv_chexiaoshenqing);
            tv_title = itemView.findViewById(R.id.tv_title);
            tv_number = itemView.findViewById(R.id.tv_number);
            tv_tuikun_yuanyin = itemView.findViewById(R.id.tv_tuikun_yuanyin);
            tv_tukuanjine = itemView.findViewById(R.id.tv_tukuanjine);
//            tv_shenqing_shijian = itemView.findViewById(R.id.tv_shenqing_shijian);
            image_touxiang = itemView.findViewById(R.id.image_touxiang);
            recyclerView = itemView.findViewById(R.id.recyclerView);
        }
    }
}
