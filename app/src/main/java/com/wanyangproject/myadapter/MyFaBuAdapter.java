package com.wanyangproject.myadapter;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.wanyangproject.R;
import com.wanyangproject.entity.FaBuTieZiEntity;

import java.util.List;

/**
 * Created by 甜甜 on 2018/7/31.
 */

public class MyFaBuAdapter extends RecyclerView.Adapter<MyFaBuAdapter.ViewHolder>{

    private Context context;
    private List<FaBuTieZiEntity.ResponseBean.ListBean> list;

    public MyFaBuAdapter(Context context, List<FaBuTieZiEntity.ResponseBean.ListBean> list) {
        this.context = context;
        this.list = list;
    }

//    删除
    private ShanChuClick shanChuClick;

    public ShanChuClick getShanChuClick() {
        return shanChuClick;
    }

    public void setShanChuClick(ShanChuClick shanChuClick) {
        this.shanChuClick = shanChuClick;
    }

    public interface ShanChuClick{
        void shanchuClick(int position,String id);
}



//       进入详情
    private XiangQingClick xiangQingClick;

    public void setXiangQingClick(XiangQingClick xiangQingClick) {
        this.xiangQingClick = xiangQingClick;
    }

    public interface XiangQingClick{
        void xiangqingClick(int position,String id);
}







    @Override
    public MyFaBuAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_fabu,parent,false);
        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final MyFaBuAdapter.ViewHolder holder, final int position) {
        holder.linear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(xiangQingClick != null){
                    xiangQingClick.xiangqingClick(holder.getAdapterPosition(),list.get(position).getId());
                }
            }
        });







        if(list.get(position).getTitle() == null){

        }else{
            holder.tv_title.setText(list.get(position).getTitle());
        }




        if(list.get(position).getContent() == null){

        }else{
            holder.tv_neirong.setText(list.get(position).getContent());
        }




        if(list.get(position).getAdd_time() == null){

        }else{
            holder.tv_time.setText(list.get(position).getAdd_time());
        }


        if(list.get(position).getStatus() == null){

        }else{
            if(list.get(position).getStatus().equals("1")){
                holder.tv_shenhe.setText("审核通过");
                holder.tv_shenhe.setVisibility(View.VISIBLE);
            }else if(list.get(position).getStatus().equals("0")){
                holder.tv_shenhe.setText("待审核");
                holder.tv_shenhe.setVisibility(View.VISIBLE);
                holder.tv_shenhe.setTextColor(Color.parseColor("#56a3fe"));
            }else if(list.get(position).getStatus().equals("2")){
                holder.tv_shenhe.setText("审核未通过");
                holder.tv_shenhe.setVisibility(View.VISIBLE);
                holder.tv_shenhe.setTextColor(Color.parseColor("#e75052"));
            }
        }


//        删除
        holder.tv_delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(shanChuClick != null){
                    shanChuClick.shanchuClick(holder.getAdapterPosition(),list.get(position).getId());
                }
            }
        });



    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView tv_neirong,tv_time,tv_shenhe,tv_delete,tv_title;
        private LinearLayout linear;
        private View view;
        public ViewHolder(View itemView) {
            super(itemView);
            view = itemView;
            tv_neirong = itemView.findViewById(R.id.tv_neirong);
            tv_time = itemView.findViewById(R.id.tv_time);
            tv_shenhe = itemView.findViewById(R.id.tv_shenhe);
            tv_delete = itemView.findViewById(R.id.tv_delete);
            linear = itemView.findViewById(R.id.linear);
            tv_title = itemView.findViewById(R.id.tv_title);
        }
    }
}
