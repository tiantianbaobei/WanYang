package com.wanyangproject.myadapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.wanyangproject.R;
import com.wanyangproject.entity.WoDeDianZanEntity;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * Created by 甜甜 on 2018/7/31.
 */

public class MyDianZanAdapter extends RecyclerView.Adapter<MyDianZanAdapter.ViewHolder>{

    private Context context;
    private List<WoDeDianZanEntity.ResponseBean> list;

    public MyDianZanAdapter(Context context, List<WoDeDianZanEntity.ResponseBean> list) {
        this.context = context;
        this.list = list;
    }




//    删除的网络请求
    private ShanChuClick shanChuClick;

    public ShanChuClick getShanChuClick() {
        return shanChuClick;
    }

    public void setShanChuClick(ShanChuClick shanChuClick) {
        this.shanChuClick = shanChuClick;
    }

    public interface ShanChuClick{
        void shanchuClick(int position,String id);
}



//              详情
    private XiangQingClick xiangQingClick;

    public void setXiangQingClick(XiangQingClick xiangQingClick) {
        this.xiangQingClick = xiangQingClick;
    }

    public interface XiangQingClick{
        void xiangqingClick(int position,String id);
}







    @Override
    public MyDianZanAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_my_dianzan,parent,false);
        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final MyDianZanAdapter.ViewHolder holder, final int position) {





        holder.linear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(xiangQingClick != null){
                    xiangQingClick.xiangqingClick(holder.getAdapterPosition(),list.get(position).getId());
                }
            }
        });









        if(list.get(position).getUinfo().getAvatar() == null){

        }else{
            Glide.with(context).load(list.get(position).getUinfo().getAvatar()).into(holder.image_touxiang);
        }



        if(list.get(position).getUinfo().getNickname() == null){

        }else{
            holder.tv_name.setText(list.get(position).getUinfo().getNickname());
        }


        if(list.get(position).getAdd_time() == null){

        }else{
            holder.tv_shijian.setText(stampToDate(list.get(position).getAdd_time()));
        }



        if(list.get(position).getContent() == null){

        }else{
            holder.tv_neirong.setText(list.get(position).getContent());
        }



        if(list.get(position).getTitle() == null){

        }else{
            holder.tv_title.setText(list.get(position).getTitle());
        }

//        删除
        holder.tv_delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(shanChuClick != null){
                    shanChuClick.shanchuClick(holder.getAdapterPosition(),list.get(position).getId());
                }
            }
        });



    }




    /*
  * 将时间戳转换为时间
   */
    public String stampToDate(String s) {
        String res;
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm");
        long lt = new Long(s);
        Date date = new Date(lt*1000);
        res = simpleDateFormat.format(date);
        return res;
    }






    @Override
    public int getItemCount() {
        return list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private ImageView image_touxiang;
        private TextView tv_name,tv_shijian,tv_neirong,tv_delete,tv_title;
        private LinearLayout linear;
        private View view;
        public ViewHolder(View itemView) {
            super(itemView);
            view = itemView;
            image_touxiang = itemView.findViewById(R.id.image_touxiang);
            tv_name = itemView.findViewById(R.id.tv_name);
            tv_shijian = itemView.findViewById(R.id.tv_shijian);
            tv_neirong = itemView.findViewById(R.id.tv_neirong);
            tv_delete = itemView.findViewById(R.id.tv_delete);
            linear = itemView.findViewById(R.id.linear);
            tv_title = itemView.findViewById(R.id.tv_title);
        }
    }
}
