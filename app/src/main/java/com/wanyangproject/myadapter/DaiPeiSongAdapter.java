package com.wanyangproject.myadapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.wanyangproject.R;
import com.wanyangproject.entity.ShangJiaDingDanEntity;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by 甜甜 on 2018/8/4.
 */

public class DaiPeiSongAdapter extends RecyclerView.Adapter<DaiPeiSongAdapter.ViewHolder>{

    private Context context;
    private List<ShangJiaDingDanEntity.ResponseBean> response;

    public DaiPeiSongAdapter(Context context, List<ShangJiaDingDanEntity.ResponseBean> response) {
        this.context = context;
        this.response = response;
    }

//    点击商家确认送达
    private ShangJiaQueRenSongDaClick shangJiaQueRenSongDaClick;

    public void setShangJiaQueRenSongDaClick(ShangJiaQueRenSongDaClick shangJiaQueRenSongDaClick) {
        this.shangJiaQueRenSongDaClick = shangJiaQueRenSongDaClick;
    }

    public interface ShangJiaQueRenSongDaClick{
        void shangjiaquerensongdaClick(int position,String Order_sn);
    }



//    联系买家
    private LianXiMaiJiaClick lianXiMaiJiaClick;

    public void setLianXiMaiJiaClick(LianXiMaiJiaClick lianXiMaiJiaClick) {
        this.lianXiMaiJiaClick = lianXiMaiJiaClick;
    }

    public interface LianXiMaiJiaClick{
        void lianximaijiaClick(int position,String phone);
}



    @Override
    public DaiPeiSongAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_dai_peisong,parent,false);
        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final DaiPeiSongAdapter.ViewHolder holder, final int position) {
        holder.tv_queren.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(shangJiaQueRenSongDaClick != null){
                    shangJiaQueRenSongDaClick.shangjiaquerensongdaClick(holder.getAdapterPosition(),response.get(position).getOrder_sn());
                }
            }
        });


        holder.tv_lianxi_maijia.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(lianXiMaiJiaClick != null){
                    lianXiMaiJiaClick.lianximaijiaClick(holder.getAdapterPosition(),response.get(position).getUser_phone());
                }
            }
        });




        if(response.get(position).getOrder_sn() == null){

        }else{
            holder.tv_dingdanbianhao.setText("订单编号："+response.get(position).getOrder_sn());
        }


        if(response.get(position).getAddres_type().equals("2")){
            holder.tv_shijian.setText(response.get(position).getExtraction());
        }else{
            holder.tv_shijian.setText(response.get(position).getYuji());
        }


        if(response.get(position).getOrder_goods() == null){

        }else{
            String aaa = "";
            for (int i = 0; i < response.get(position).getOrder_goods().size(); i++) {
                ShangJiaDingDanEntity.ResponseBean.OrderGoodsBean orderGoodsBean = response.get(position).getOrder_goods().get(i);
                aaa = aaa+orderGoodsBean.getGoods_name();
                if(i != response.get(position).getOrder_goods().size() -1){
                    aaa = aaa +"\n";
                }
            }
            holder.tv_shangpin.setText(aaa);
        }


        if(response.get(position).getOrder_goods() == null){

        }else{
            String bbb = "";
            for (int i = 0; i < response.get(position).getOrder_goods().size(); i++) {
                ShangJiaDingDanEntity.ResponseBean.OrderGoodsBean orderGoodsBean1 = response.get(position).getOrder_goods().get(i);
                bbb = bbb + "x"+orderGoodsBean1.getNum();
                if(i != response.get(position).getOrder_goods().size() -1){
                    bbb = bbb +"\n";
                }
            }
            holder.tv_number.setText(bbb);
        }


        if(response.get(position).getAddres_add() == null){

        }else{
            if(response.get(position).getAddres_type().equals("1")){
                holder.tv_dizhi.setText(response.get(position).getAddres_add());
            }else if(response.get(position).getAddres_type().equals("2")){
                holder.tv_dizhi.setText("自取");
            }
        }


        if(response.get(position).getChaoshi() == null){

        }else{
            holder.tv_daojishi.setText(response.get(position).getChaoshi());
        }
    }


    @Override
    public int getItemCount() {
        return response.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView tv_dingdanbianhao,tv_daojishi,tv_shijian,tv_shangpin_xinxi,tv_shangpin,tv_dizhi,tv_queren,tv_lianxi_maijia,tv_number;
        private ImageView image_daojishi;
        private View view;
        public ViewHolder(View itemView) {
            super(itemView);
            view =  itemView;
            tv_dingdanbianhao = itemView.findViewById(R.id.tv_dingdanbianhao);
            tv_daojishi = itemView.findViewById(R.id.tv_daojishi);
            tv_shijian = itemView.findViewById(R.id.tv_shijian);
            tv_shangpin_xinxi = itemView.findViewById(R.id.tv_shangpin_xinxi);
            tv_shangpin = itemView.findViewById(R.id.tv_shangpin);
            tv_dizhi = itemView.findViewById(R.id.tv_dizhi);
            tv_queren = itemView.findViewById(R.id.tv_queren);
            tv_lianxi_maijia = itemView.findViewById(R.id.tv_lianxi_maijia);
            image_daojishi = itemView.findViewById(R.id.image_daojishi);
            tv_number = itemView.findViewById(R.id.tv_number);
        }
    }
}
