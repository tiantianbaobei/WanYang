package com.wanyangproject.myadapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.wanyangproject.R;
import com.wanyangproject.adapter.DingDanDaiShouHuoAdapter;
import com.wanyangproject.adapter.DingDanQuanBuAdapter;
import com.wanyangproject.entity.ChuanZhiEntity;
import com.wanyangproject.entity.DaiPingJiaEntity;
import com.wanyangproject.entity.DaiZhiFuEntity;
import com.wanyangproject.entity.QuanBuEntity;
import com.wanyangproject.my.DaiPingJiaXiangQingActivity;
import com.wanyangproject.my.DaiShouHuoXiangQingActivity;
import com.wanyangproject.my.DaiZhiFuXiangQingActivity;
import com.wanyangproject.my.FinishXiangQingActivity;
import com.wanyangproject.my.PingJiaActivity;
import com.wanyangproject.my.TuiKuanActivity;
import com.wanyangproject.shouye.DingDanXiangQingActivity;
import com.wanyangproject.shouye.ZhiFuActivity;

import java.util.List;

import butterknife.ButterKnife;

/**
 * Created by 甜甜 on 2018/7/30.
 */

public class QuanBuAdapter extends RecyclerView.Adapter<QuanBuAdapter.ViewHolder>{

    private Context context;
    private List<QuanBuEntity.ResponseBean> response;
    private DingDanQuanBuAdapter dingDanQuanBuAdapter;


    public QuanBuAdapter(Context context, List<QuanBuEntity.ResponseBean> response) {
        this.context = context;
        this.response = response;
    }


//    确认收货
    private QueRenShouHuoClick queRenShouHuoClick;

    public void setQueRenShouHuoClick(QueRenShouHuoClick queRenShouHuoClick) {
        this.queRenShouHuoClick = queRenShouHuoClick;
    }

    public interface QueRenShouHuoClick{
        void querenshouhuoClick(int position,String dingdanhao);
}




//      去支付
    private QuZhiFuClick quZhiFuClick;

    public void setQuZhiFuClick(QuZhiFuClick quZhiFuClick) {
        this.quZhiFuClick = quZhiFuClick;
    }

    public interface QuZhiFuClick{
        void quzhifuClick(int position);
    }



//    去支付
    private void QuZhiFu(int position){
        float zongjiaone = 0;
        float zongjiatwo = Float.parseFloat(response.get(position).getMoney()); //合计价格
//        float baohanyunfei = Float.parseFloat(response.get(position).getShop_cost());

        for (int i = 0; i < response.get(position).getGoods().size(); i++) {
            QuanBuEntity.ResponseBean.GoodsBean goodsBean = response.get(position).getGoods().get(i);
            float mon = Integer.parseInt(goodsBean.getNum()) * Float.parseFloat(goodsBean.getPic());
            zongjiaone += mon;
        }

        String yunfei;
        float jiagejiage;
        if(zongjiaone < zongjiatwo){
//                                       说明包含运费
            yunfei = "包含运费 ¥";
            jiagejiage = zongjiaone;

        }else{
//                                          不含运费
            yunfei = "不包含运费 ¥";
            jiagejiage = zongjiatwo;
        }


        String format = String.format("%.2f", jiagejiage);

        if(response.get(position).getShop_cost().equals("")){
            response.get(position).setShop_cost("0");
        }



//        Intent intent = new Intent(context, DingDanXiangQingActivity.class);
        Intent intent = new Intent(context, ZhiFuActivity.class);
//        intent.putExtra("peisongfei",response.get(position).getShop_cost());
//        intent.putExtra("shopid",response.get(position).getShop_id());
        intent.putExtra("money",response.get(position).getMoney());
        intent.putExtra("dingdanhao",response.get(position).getOrder_sn());
        context.startActivity(intent);
    }



//    去评价
    private void QuPingJia(int position){
        Intent intent = new Intent(context, PingJiaActivity.class);
        ChuanZhiEntity.obj = response.get(position);// 传值
        context.startActivity(intent);
    }






//        if(response.get(position).getType().equals("11")){
//        holder.btn_tuikuan.setText("已申请退款");
//    }else if(response.get(position).getType().equals("12")){
//        holder.btn_tuikuan.setText("退款完成");
//    }else{
//        //        申请退款
//        holder.btn_tuikuan.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                if(daiShouHuoClick != null){
//                    daiShouHuoClick.daishouhuoClick(holder.getAdapterPosition());
//                }
//            }
//        });
//    }





    @Override
    public QuanBuAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.dingdan_item_quanbu,parent,false);
        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final QuanBuAdapter.ViewHolder holder, final int position) {


        //        全部中间的列表
        dingDanQuanBuAdapter = new DingDanQuanBuAdapter(context,response.get(position).getGoods());
        LinearLayoutManager manager = new LinearLayoutManager(context);
        holder.recyclerView.setLayoutManager(manager);
        holder.recyclerView.setAdapter(dingDanQuanBuAdapter);




        if(response.get(position).getType().equals("")){

        }else{
//            待支付
            if(response.get(position).getType().equals("1")){
                holder.btn_quanbu.setVisibility(View.VISIBLE);
                holder.btn_tuikuan.setVisibility(View.GONE);
                holder.tv_zhuangtai.setText("待支付");
                holder.btn_quanbu.setText("去支付");
////                点击去支付
//                holder.btn_quanbu.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View view) {
//
//                        float zongjiaone = 0;
//                        float zongjiatwo = Float.parseFloat(response.get(position).getMoney()); //合计价格
//                        float baohanyunfei = Float.parseFloat(response.get(position).getShop_cost());
//
//                        for (int i = 0; i < response.get(position).getGoods().size(); i++) {
//                            QuanBuEntity.ResponseBean.GoodsBean goodsBean = response.get(position).getGoods().get(i);
//                            float mon = Integer.parseInt(goodsBean.getNum()) * Float.parseFloat(goodsBean.getPic());
//                            zongjiaone += mon;
//                        }
//
//                        String yunfei;
//                        float jiagejiage;
//                        if(zongjiaone < zongjiatwo){
////                                       说明包含运费
//                            yunfei = "包含运费 ¥";
//                            jiagejiage = zongjiaone;
//
//                        }else{
////                                          不含运费
//                            yunfei = "不包含运费 ¥";
//                            jiagejiage = zongjiatwo;
//                        }
//
//
//                        String format = String.format("%.2f", jiagejiage);
//
//
////                        Intent intent = new Intent(context, ZhiFuActivity.class);
//                        Intent intent = new Intent(context, DingDanXiangQingActivity.class);
//                        intent.putExtra("peisongfei",response.get(position).getShop_cost());
//                        intent.putExtra("shopid",response.get(position).getShop_id());
//                        intent.putExtra("zongjiage",format);
//                        intent.putExtra("dingdanhao",response.get(position).getOrder_sn());
//                        context.startActivity(intent);
//                    }
//                });
//                点击进入详情
                holder.linear.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        Intent intent = new Intent(context, DaiZhiFuXiangQingActivity.class);
                        intent.putExtra("dingdanhao",response.get(position).getOrder_sn());
                        context.startActivity(intent);
                    }
                });




//                点击进入详情
                dingDanQuanBuAdapter.setXiangQingClick(new DingDanQuanBuAdapter.XiangQingClick() {
                    @Override
                    public void xiangqingClick(String id) {
                        Intent intent = new Intent(context, DaiZhiFuXiangQingActivity.class);
                        intent.putExtra("dingdanhao",response.get(position).getOrder_sn());
                        context.startActivity(intent);
                    }
                });
            }



//            待收货
            if(response.get(position).getType().equals("2") || response.get(position).getType().equals("3") || response.get(position).getType().equals("4") ||
                    response.get(position).getType().equals("5") ||response.get(position).getType().equals("6") || response.get(position).getType().equals("7") ||
                    response.get(position).getType().equals("10") || response.get(position).getType().equals("11") || response.get(position).getType().equals("12")){
                holder.btn_quanbu.setVisibility(View.VISIBLE);
                holder.btn_tuikuan.setVisibility(View.VISIBLE);
                holder.btn_quanbu.setText("确认收货");
                holder.btn_tuikuan.setText("申请退款");
                holder.tv_zhuangtai.setText("待收货");
//                点击申请退款
//                holder.btn_tuikuan.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View view) {
//                        Intent intent = new Intent(context, TuiKuanActivity.class);
////                        ChuanZhiEntity.obj = response.get(position);// 传值
//                        intent.putExtra("dingdanhao",response.get(position).getOrder_sn());// 订单号
//                        context.startActivity(intent);
//                    }
//                });
//                点击确认收货
//                holder.btn_quanbu.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View view) {
//                        if(queRenShouHuoClick != null){
//                            queRenShouHuoClick.querenshouhuoClick(holder.getAdapterPosition(),response.get(position).getOrder_sn());
//                        }
//                    }
//                });
//                点击进入详情
                holder.linear.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent intent = new Intent(context, DaiShouHuoXiangQingActivity.class);
                        intent.putExtra("dingdanhao",response.get(position).getOrder_sn());
                        context.startActivity(intent);
                    }
                });
//                点击进入详情
                dingDanQuanBuAdapter.setXiangQingClick(new DingDanQuanBuAdapter.XiangQingClick() {
                    @Override
                    public void xiangqingClick(String id) {
                        Intent intent = new Intent(context, DaiShouHuoXiangQingActivity.class);
                        intent.putExtra("dingdanhao",response.get(position).getOrder_sn());
                        context.startActivity(intent);
                    }
                });
            }


            if(response.get(position).getType().equals("11")){
                holder.tv_zhuangtai.setText("已申请退款");

            }



            if(response.get(position).getType().equals("12")){
                holder.tv_zhuangtai.setText("退款完成");
                holder.btn_tuikuan.setVisibility(View.GONE);
                holder.btn_quanbu.setVisibility(View.VISIBLE);
                holder.btn_quanbu.setText("退款完成");
//                holder.btn_quanbu.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View view) {
//                        Intent intent = new Intent(context, FinishXiangQingActivity.class);
//                        intent.putExtra("dingdanhao",response.get(position).getOrder_sn());
//                        context.startActivity(intent);
//                    }
//                });


                //                点击进入详情
                dingDanQuanBuAdapter.setXiangQingClick(new DingDanQuanBuAdapter.XiangQingClick() {
                    @Override
                    public void xiangqingClick(String id) {
                        Intent intent = new Intent(context, FinishXiangQingActivity.class);
                        intent.putExtra("dingdanhao",response.get(position).getOrder_sn());
                        context.startActivity(intent);
                    }
                });

            }


//                待评价
            if(response.get(position).getType().equals("7") || response.get(position).getType().equals("8")){
                holder.btn_quanbu.setVisibility(View.VISIBLE);
                holder.btn_tuikuan.setVisibility(View.GONE);
                holder.btn_quanbu.setText("评价");
                holder.tv_zhuangtai.setText("待评价");
//                holder.btn_quanbu.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View view) {
//                        Intent intent = new Intent(context, PingJiaActivity.class);
//                        ChuanZhiEntity.obj = response.get(position);// 传值
//                        context.startActivity(intent);
//                    }
//                });
                //           点击进入详情
                holder.linear.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent intent = new Intent(context, DaiPingJiaXiangQingActivity.class);
                        intent.putExtra("dingdanhao",response.get(position).getOrder_sn());
                        context.startActivity(intent);
                    }
                });
//                点击进入详情
                dingDanQuanBuAdapter.setXiangQingClick(new DingDanQuanBuAdapter.XiangQingClick() {
                    @Override
                    public void xiangqingClick(String id) {
                        Intent intent = new Intent(context, DaiPingJiaXiangQingActivity.class);
                        intent.putExtra("dingdanhao",response.get(position).getOrder_sn());
                        context.startActivity(intent);
                    }
                });
            }

//                已完成
            if(response.get(position).getType().equals("9")){
                holder.btn_quanbu.setVisibility(View.GONE);
                holder.btn_tuikuan.setVisibility(View.GONE);
                holder.tv_zhuangtai.setText("已完成");
                //                点击进入详情
                holder.linear.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent intent = new Intent(context, FinishXiangQingActivity.class);
                        intent.putExtra("dingdanhao",response.get(position).getOrder_sn());
                        context.startActivity(intent);
                    }
                });
//                点击进入详情
                dingDanQuanBuAdapter.setXiangQingClick(new DingDanQuanBuAdapter.XiangQingClick() {
                    @Override
                    public void xiangqingClick(String id) {
                        Intent intent = new Intent(context, FinishXiangQingActivity.class);
                        intent.putExtra("dingdanhao",response.get(position).getOrder_sn());
                        context.startActivity(intent);
                    }
                });
            }






        }







        holder.btn_quanbu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String type = response.get(position).getType();
                if(type.equals("1")){
                    QuZhiFu(position);
                }else if(type.equals("2") || type.equals("3") || type.equals("4") || type.equals("5") || type.equals("6") || type.equals("10") || type.equals("11")){
                    if(response.get(position).getType().equals("11")){
                        Toast.makeText(context, "请耐心等待，商家待退款", Toast.LENGTH_SHORT).show();
                        return;
                    }
                    if(queRenShouHuoClick != null){
                        queRenShouHuoClick.querenshouhuoClick(holder.getAdapterPosition(),response.get(position).getOrder_sn());
                    }
                }else if(type.equals("7") || type.equals("8")){
                    QuPingJia(position);
                }else if(type.equals("12")){
                  Intent intent = new Intent(context, FinishXiangQingActivity.class);
                  intent.putExtra("dingdanhao",response.get(position).getOrder_sn());
                  context.startActivity(intent);


                }
            }
        });



        if(response.get(position).getType().equals("11")){
//            holder.btn_tuikuan.setText("已申请退款");
            holder.btn_tuikuan.setVisibility(View.GONE);
            holder.btn_quanbu.setVisibility(View.VISIBLE);
            holder.btn_quanbu.setText("已申请退款");
            holder.btn_quanbu.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(context, DaiShouHuoXiangQingActivity.class);
                    intent.putExtra("dingdanhao",response.get(position).getOrder_sn());
                    context.startActivity(intent);
                }
            });
//        }else if(response.get(position).getType().equals("12")){
////            holder.btn_tuikuan.setText("退款完成");
//            holder.btn_tuikuan.setVisibility(View.GONE);
//            holder.btn_quanbu.setVisibility(View.VISIBLE);
//            holder.btn_quanbu.setText("退款完成");
        }else{

            //        申请退款
            holder.btn_tuikuan.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    //        申请退款
                    Intent intent = new Intent(context, TuiKuanActivity.class);
//                        ChuanZhiEntity.obj = response.get(position);// 传值
                    intent.putExtra("dingdanhao",response.get(position).getOrder_sn());// 订单号
                    context.startActivity(intent);
                }
            });
        }


//
//        holder.btn_tuikuan.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                if(response.get(position).getType().equals("11")){
//                    holder.btn_tuikuan.setText("已申请退款");
//                }else if(response.get(position).getType().equals("12")){
//                    holder.btn_tuikuan.setText("退款完成");
//                }else{
//                    //        申请退款
//                    Intent intent = new Intent(context, TuiKuanActivity.class);
////                        ChuanZhiEntity.obj = response.get(position);// 传值
//                    intent.putExtra("dingdanhao",response.get(position).getOrder_sn());// 订单号
//                    context.startActivity(intent);
//                }
//
//            }
//        });
//




        if(response.get(position).getShop_name() == null){

        }else{ // 商家店名
            holder.tv_name.setText(response.get(position).getShop_name());
        }

        int sum = 0;
        float quanbu = 0;
        for (int i = 0; i < response.get(position).getGoods().size(); i++) {
            QuanBuEntity.ResponseBean.GoodsBean goodsBean = response.get(position).getGoods().get(i);
            sum += Integer.parseInt(goodsBean.getNum());
            quanbu +=  Integer.parseInt(goodsBean.getNum())*Float.parseFloat(goodsBean.getPic());


        }



        float vmon = Float.parseFloat(response.get(position).getMoney());
        if (quanbu < vmon){
            holder.tv_yunfei.setText(("(含运费：¥"+response.get(position).getShop_cost()+")"));
        }else{
            holder.tv_yunfei.setText(("(不含运费：¥"+response.get(position).getShop_cost()+")"));
        }

        if(sum+"" == null){

        }else{ //合计的商品的数量
            holder.tv_heji.setText("共计"+sum+"件商品  合计：");
        }



        if(response.get(position).getMoney() == null){

        }else{ // 合计的总钱数
            holder.tv_heji_money.setText(" ¥"+response.get(position).getMoney()+"  ");
        }

//        if(response.get(position).getShop_cost() == null){
//
//        }else{ //配送费
//
//            holder.tv_yunfei.setText(("(含运费：¥"+response.get(position).getShop_cost()+")"));
//        }
    }


    @Override
    public int getItemCount() {
        return response.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView tv_name,tv_neirong,tv_money,tv_number,tv_heji,tv_heji_money,tv_yunfei,tv_zhuangtai;
        private ImageView image_tupian;
        private Button btn_quanbu,btn_tuikuan;
        private RecyclerView recyclerView;
        private LinearLayout linear;
        private View view;
        public ViewHolder(View itemView) {
            super(itemView);
            view = itemView;
            tv_name = itemView.findViewById(R.id.tv_name);
            tv_neirong = itemView.findViewById(R.id.tv_neirong);
            tv_money = itemView.findViewById(R.id.tv_money);
            tv_number = itemView.findViewById(R.id.tv_number);
            tv_heji = itemView.findViewById(R.id.tv_heji);
            tv_heji_money = itemView.findViewById(R.id.tv_heji_money);
            tv_yunfei = itemView.findViewById(R.id.tv_yunfei);
            image_tupian = itemView.findViewById(R.id.image_tupian);
            btn_quanbu = itemView.findViewById(R.id.btn_quanbu);
            recyclerView = itemView.findViewById(R.id.recyclerView);
            btn_tuikuan = itemView.findViewById(R.id.btn_tuikuan);
            linear = itemView.findViewById(R.id.linear);
            tv_zhuangtai = itemView.findViewById(R.id.tv_zhuangtai);
        }
    }
}
