package com.wanyangproject.myadapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.wanyangproject.R;
import com.wanyangproject.entity.ShangJiaShangPinEntity;
import com.wanyangproject.entity.ShangJiaShangPinGuanLiEntity;
import com.wanyangproject.my.ShangJiaShangPinGuanLiActivity;
import com.wanyangproject.utils.ContractUtils;

import java.util.List;

/**
 * Created by 甜甜 on 2018/8/3.
 */

public class RightAdapter extends RecyclerView.Adapter<RightAdapter.ViewHolder>{

    private Context context;
    private List<ShangJiaShangPinGuanLiEntity.ResponseBean.GoodsBeanX> goods;
    private List<ShangJiaShangPinGuanLiEntity.ResponseBean.GoodsBeanX.GoodsBean> list;

    public RightAdapter(Context context, List<ShangJiaShangPinGuanLiEntity.ResponseBean.GoodsBeanX> goods) {
        this.context = context;
        this.goods = goods;
    }



//    点击上架
    private ShangJiaClick shangJiaClick;

    public void setShangJiaClick(ShangJiaClick shangJiaClick) {
        this.shangJiaClick = shangJiaClick;
    }

    public interface ShangJiaClick{
        void shangjiaclick(int position,String type,String id);
}


//      点击下架
    private XiaJiaClick xiaJiaClick;

    public void setXiaJiaClick(XiaJiaClick xiaJiaClick) {
        this.xiaJiaClick = xiaJiaClick;
    }

    public interface XiaJiaClick{
        void xiajiaClick(int position,String type,String id);
    }


    @Override
    public RightAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_right,parent,false);
        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final RightAdapter.ViewHolder holder, final int position) {
//        点击上架
        holder.tv_shangjia.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(shangJiaClick != null){
                    shangJiaClick.shangjiaclick(holder.getAdapterPosition(),list.get(position).getType(),list.get(position).getId());
                    ShangJiaShangPinGuanLiEntity.ResponseBean.GoodsBeanX.GoodsBean goodsBean = list.get(position);
                    goodsBean.setType("1");
                    holder.tv_xiajia.setVisibility(View.VISIBLE);
                    holder.tv_shangjia.setVisibility(View.GONE);
                }
            }
        });

//        点击下架
        holder.tv_xiajia.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(xiaJiaClick != null){
                    xiaJiaClick.xiajiaClick(holder.getAdapterPosition(),list.get(position).getType(),list.get(position).getId());
                    ShangJiaShangPinGuanLiEntity.ResponseBean.GoodsBeanX.GoodsBean goodsBean = list.get(position);
                    goodsBean.setType("2");
                    holder.tv_xiajia.setVisibility(View.GONE);
                    holder.tv_shangjia.setVisibility(View.VISIBLE);
                }
            }
        });



        if(list != null){
            if(list.get(position).getGoods_name() == null){

            }else{
                holder.tv_title.setText(list.get(position).getGoods_name());
            }


            if(ContractUtils.PHOTO_URL+list.get(position).getMaster() == null){

            }else{
                Glide.with(context).load(ContractUtils.PHOTO_URL+list.get(position).getMaster()).into(holder.image_tupian);
            }


            if(list.get(position).getStock() == null && list.get(position).getVolume() == null){

            }else{
                holder.tv_kucun.setText("库存:"+list.get(position).getStock()+"  月销:"+list.get(position).getVolume());
            }


            if(list.get(position).getPic() == null){

            }else{
                holder.tv_money.setText("¥ "+list.get(position).getPic());
            }


            if(list.get(position).getMarket() == null){

            }else{
                holder.tv_shichang_money.setText("¥ "+list.get(position).getMarket());
            }


            if(list.get(position).getType() == null){

            }else{
                if(list.get(position).getType().equals("1")){
                    holder.tv_shangjia.setVisibility(View.GONE);
                    holder.tv_xiajia.setVisibility(View.VISIBLE);
                }else if(list.get(position).getType().equals("2")){
                    holder.tv_shangjia.setVisibility(View.VISIBLE);
                    holder.tv_xiajia.setVisibility(View.GONE);
                }

            }
        }



    }

    @Override
    public int getItemCount() {
        for(ShangJiaShangPinGuanLiEntity.ResponseBean.GoodsBeanX shangJiaShangPinEntity :goods){
            if(shangJiaShangPinEntity.isOk()){
                list = shangJiaShangPinEntity.getGoods();
            }
        }
        if(list == null){
            return 0;
        }
        return list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private ImageView image_tupian;
        private TextView tv_title,tv_kucun,tv_money,tv_shangjia,tv_shichang_money,tv_xiajia;
        private View view;
        public ViewHolder(View itemView) {
            super(itemView);
            view = itemView;
            image_tupian = itemView.findViewById(R.id.image_tupian);
            tv_title = itemView.findViewById(R.id.tv_title);
            tv_kucun = itemView.findViewById(R.id.tv_kucun);
            tv_money = itemView.findViewById(R.id.tv_money);
            tv_shangjia = itemView.findViewById(R.id.tv_shangjia);
            tv_shichang_money = itemView.findViewById(R.id.tv_shichang_money);
            tv_xiajia = itemView.findViewById(R.id.tv_xiajia);
        }
    }
}
