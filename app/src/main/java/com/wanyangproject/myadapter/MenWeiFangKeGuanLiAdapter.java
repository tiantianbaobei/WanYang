package com.wanyangproject.myadapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.wanyangproject.R;
import com.wanyangproject.entity.FangKeGuanLiEntity;
import com.wanyangproject.my.MyMenWeiFangKeGuanLiActivity;

import java.util.List;

/**
 * Created by 甜甜 on 2018/8/1.
 */

public class MenWeiFangKeGuanLiAdapter extends RecyclerView.Adapter<MenWeiFangKeGuanLiAdapter.ViewHolder>{

    private Context context;
    private List<FangKeGuanLiEntity.ResponseBean> response;

    public MenWeiFangKeGuanLiAdapter(Context context, List<FangKeGuanLiEntity.ResponseBean> response) {
        this.context = context;
        this.response = response;
    }



    //    编辑
    private BianJiClick bianJiClick;

    public void setBianJiClick(BianJiClick bianJiClick) {
        this.bianJiClick = bianJiClick;
    }

    public interface BianJiClick{
        void banjiclick(int position);
    }

    @Override
    public MenWeiFangKeGuanLiAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_menwei_fangke,parent,false);
        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final MenWeiFangKeGuanLiAdapter.ViewHolder holder, int position) {
//        holder.tv_bianji.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                if(bianJiClick != null){
//                    bianJiClick.banjiclick(holder.getAdapterPosition());
//                }
//            }
//        });




        if(response.get(position).getName() == null){

        }else{
            holder.tv_name.setText(response.get(position).getName());
        }



        if(response.get(position).getNums() == null){

        }else{
            holder.tv_number.setText("到访人数："+response.get(position).getNums()+"人");
        }


        if(response.get(position).getPhone() == null){

        }else{
            holder.tv_phone.setText(response.get(position).getPhone());
        }


        if(response.get(position).getDaodaTime() == null){

        }else{
            holder.tv_time.setText(response.get(position).getDaodaTime());
        }




        if(response.get(position).getType().equals("1")){
            holder.image_yidaofang.setImageResource(R.mipmap.weidaofang);//未到访
        }else if(response.get(position).getType().equals("2")){
            holder.image_yidaofang.setImageResource(R.mipmap.yidaofang);//已到访
        }else if(response.get(position).getType().equals("3")){//已失效
            holder.image_yidaofang.setImageResource(R.drawable.yishixiao);//已失效
        }







    }

    @Override
    public int getItemCount() {
        return response.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView tv_name,tv_phone,tv_number,tv_time,tv_bianji,tv_delete;
        private ImageView image_yidaofang;
        private View view;
        public ViewHolder(View itemView) {
            super(itemView);
            view = itemView;
            image_yidaofang = itemView.findViewById(R.id.image_yidaofang);
            tv_name = itemView.findViewById(R.id.tv_name);
            tv_phone = itemView.findViewById(R.id.tv_phone);
            tv_number = itemView.findViewById(R.id.tv_number);
            tv_time = itemView.findViewById(R.id.tv_time);
            tv_bianji = itemView.findViewById(R.id.tv_bianji);
            tv_delete = itemView.findViewById(R.id.tv_delete);
        }
    }
}
