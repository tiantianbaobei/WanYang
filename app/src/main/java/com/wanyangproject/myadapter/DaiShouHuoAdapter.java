package com.wanyangproject.myadapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.wanyangproject.R;
import com.wanyangproject.adapter.DingDanDaiShouHuoAdapter;
import com.wanyangproject.adapter.DingDanDaiZhiFuAdapter;
import com.wanyangproject.entity.DaiShouHuoEntity;
import com.wanyangproject.entity.DaiZhiFuEntity;
import com.wanyangproject.my.DaiShouHuoXiangQingActivity;

import java.util.List;

/**
 * Created by 甜甜 on 2018/7/30.
 */

public class DaiShouHuoAdapter extends RecyclerView.Adapter<DaiShouHuoAdapter.ViewHolder>{

    private Context context;
    private List<DaiShouHuoEntity.ResponseBean> response;

    public DaiShouHuoAdapter(Context context, List<DaiShouHuoEntity.ResponseBean> response) {
        this.context = context;
        this.response = response;
    }


//    申请退款
    private DaiShouHuoClick daiShouHuoClick;

    public void setDaiShouHuoClick(DaiShouHuoClick daiShouHuoClick) {
        this.daiShouHuoClick = daiShouHuoClick;
    }

    public interface DaiShouHuoClick {
        void daishouhuoClick(int position);
    }



//    确认收货
    private QueRenShouHuoClick queRenShouHuoClick;

    public void setQueRenShouHuoClick(QueRenShouHuoClick queRenShouHuoClick) {
        this.queRenShouHuoClick = queRenShouHuoClick;
    }

    public interface QueRenShouHuoClick{
        void querenshouhuoClick(int position,String dingdanhao);
    }


//    待收货详情
    private DaiShouHuoXiangQingClick daiShouHuoXiangQingClick;

    public void setDaiShouHuoXiangQingClick(DaiShouHuoXiangQingClick daiShouHuoXiangQingClick) {
        this.daiShouHuoXiangQingClick = daiShouHuoXiangQingClick;
    }

    public interface DaiShouHuoXiangQingClick{
        void daishouhuoXiangQingClick(int position);
    }


    @Override
    public DaiShouHuoAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.dingdan_item_daishouhuo,parent,false);
        final ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final DaiShouHuoAdapter.ViewHolder holder, final int position) {


        if(response.get(position).getType().equals("11")){
//            holder.btn_tuikuan.setText("已申请退款");
            holder.btn_tuikuan.setVisibility(View.GONE);
            holder.btn_queren_shouuo.setText("已申请退款");
            holder.tv_zhuangtai.setText("已申请退款");
            holder.btn_queren_shouuo.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(context, DaiShouHuoXiangQingActivity.class);
                    intent.putExtra("dingdanhao",response.get(position).getOrder_sn());
                    context.startActivity(intent);
                }
            });

//        }else if(response.get(position).getType().equals("12")){
////            holder.btn_tuikuan.setText("退款完成");
//            holder.btn_tuikuan.setVisibility(View.GONE);
//            holder.btn_queren_shouuo.setText("退款完成");
        }else{
            //        申请退款
            holder.btn_tuikuan.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if(response.get(position).getType().equals("11")){
                        Toast.makeText(context, "请耐心等待，商家待退款", Toast.LENGTH_SHORT).show();
                        Intent intent = new Intent(context, DaiShouHuoXiangQingActivity.class);
                        intent.putExtra("dingdanhao",response.get(position).getOrder_sn());
                        context.startActivity(intent);
//                        return;
                    }
                    if(daiShouHuoClick != null){
                        daiShouHuoClick.daishouhuoClick(holder.getAdapterPosition());
                    }
                }
            });


            //        确认收货
            holder.btn_queren_shouuo.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if(response.get(position).getType().equals("11")){
                        return;
                    }
                    if(queRenShouHuoClick != null){
                        queRenShouHuoClick.querenshouhuoClick(holder.getAdapterPosition(),response.get(position).getOrder_sn());
                    }
                }
            });
        }





//            holder.btn_tuikuan.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View view) {
//                    if(response.get(position).getType().equals("11")){
//                        return;
//                    }
//                }
//            });













//        点击进入详情
        holder.linear_shouhuo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(daiShouHuoXiangQingClick != null){
                    daiShouHuoXiangQingClick.daishouhuoXiangQingClick(holder.getAdapterPosition());

                }
            }
        });

//        待收货中间的列表
        DingDanDaiShouHuoAdapter dingDanDaiShouHuoAdapter = new DingDanDaiShouHuoAdapter(context,response.get(position).getGoods());
        LinearLayoutManager manager = new LinearLayoutManager(context);
        holder.recyclerView.setLayoutManager(manager);
        holder.recyclerView.setAdapter(dingDanDaiShouHuoAdapter);

//        点击待收货进入详情
        dingDanDaiShouHuoAdapter.setDingDanDaiShouHuoClick(new DingDanDaiShouHuoAdapter.DingDanDaiShouHuoClick() {
            @Override
            public void dingdanDaiShouHuoClick(String id) {
                Intent intent = new Intent(context, DaiShouHuoXiangQingActivity.class);
                intent.putExtra("dingdanhao",response.get(position).getOrder_sn());
                context.startActivity(intent);
            }
        });



        if(response.get(position).getShop_name() == null){

        }else{ // 商家店名
            holder.tv_name.setText(response.get(position).getShop_name());
        }



        int sum = 0;
        float quanbu = 0;
        for (int i = 0; i < response.get(position).getGoods().size(); i++) {
            DaiShouHuoEntity.ResponseBean.GoodsBean goodsBean = response.get(position).getGoods().get(i);
            sum += Integer.parseInt(goodsBean.getNum());
            quanbu +=  Integer.parseInt(goodsBean.getNum())*Float.parseFloat(goodsBean.getPic());
        }
        if(sum+"" == null){

        }else{ //合计的商品的数量
            holder.tv_heji.setText("共计"+sum+"件商品  合计：");
        }



        if(response.get(position).getMoney() == null){

        }else{ // 合计的总钱数
            holder.tv_heji_money.setText(" ¥"+response.get(position).getMoney()+"  ");
        }


//        if(response.get(position).getShop_cost() == null){
//
//        }else{ //配送费
//            holder.tv_yunfei.setText(("含运费：¥"+response.get(position).getShop_cost()));
//        }


        float vmon = Float.parseFloat(response.get(position).getMoney());
        if (quanbu < vmon){
            holder.tv_yunfei.setText(("(含运费：¥"+response.get(position).getShop_cost()+")"));
        }else{
            holder.tv_yunfei.setText(("(不含运费：¥"+response.get(position).getShop_cost()+")"));
        }




    }

    @Override
    public int getItemCount() {
        return response.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
//        private TextView tv_name,tv_zhuangtai,tv_neirong,tv_monry,tv_number,tv_heji,tv_heji_money,tv_yunfei;
        private TextView tv_heji,tv_name,tv_heji_money,tv_yunfei,tv_zhuangtai;//tv_zhuangtai,tv_neirong,tv_monry,tv_number,
//        private ImageView image_tupian;
        private Button btn_queren_shouuo,btn_tuikuan;
        private LinearLayout linear_shouhuo;
        private RecyclerView recyclerView;
        private View view;
        public ViewHolder(View itemView) {
            super(itemView);
            view = itemView;
            tv_name = itemView.findViewById(R.id.tv_name);
//            tv_zhuangtai = itemView.findViewById(R.id.tv_zhuangtai);
//            tv_neirong = itemView.findViewById(R.id.tv_neirong);
//            tv_monry = itemView.findViewById(R.id.tv_monry);
//            tv_number = itemView.findViewById(R.id.tv_number);
            tv_heji = itemView.findViewById(R.id.tv_heji);
            tv_heji_money = itemView.findViewById(R.id.tv_heji_money);
            tv_yunfei = itemView.findViewById(R.id.tv_yunfei);
//            image_tupian = itemView.findViewById(R.id.image_tupian);
            btn_queren_shouuo = itemView.findViewById(R.id.btn_queren_shouuo);
            btn_tuikuan = itemView.findViewById(R.id.btn_tuikuan);
            linear_shouhuo = itemView.findViewById(R.id.linear_shouhuo);
            recyclerView = itemView.findViewById(R.id.recyclerView);
            tv_zhuangtai = itemView.findViewById(R.id.tv_zhuangtai);
        }
    }
}
