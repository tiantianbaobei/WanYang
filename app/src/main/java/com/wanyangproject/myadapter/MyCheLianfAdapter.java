package com.wanyangproject.myadapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.wanyangproject.R;
import com.wanyangproject.entity.QiYeCheLiangGuanLiEntity;
import com.wanyangproject.my.CheLiangGuanLiActivity;

import java.util.List;

/**
 * Created by 甜甜 on 2018/8/1.
 */

public class MyCheLianfAdapter extends RecyclerView.Adapter<MyCheLianfAdapter.ViewHolder>{

    private Context context;
    private List<QiYeCheLiangGuanLiEntity.ResponseBean> response;

    public MyCheLianfAdapter(Context context, List<QiYeCheLiangGuanLiEntity.ResponseBean> response) {
        this.context = context;
        this.response = response;
    }

    @Override
    public MyCheLianfAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_cheliang,parent,false);
        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(MyCheLianfAdapter.ViewHolder holder, int position) {
        if(response.get(position).getPhone() == null){

        }else{
            holder.tv_phone.setText("联系电话："+response.get(position).getPhone());
        }


        if(response.get(position).getPersonName() == null){

        }else{
            holder.tv_name.setText(response.get(position).getPersonName());
        }


        if(response.get(position).getPlateNumber() == null){

        }else{
            holder.tv_chepaihao.setText(response.get(position).getPlateNumber());
        }

    }

    @Override
    public int getItemCount() {
        return response.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView tv_name,tv_phone,tv_chepaihao;
        private View view;
        public ViewHolder(View itemView) {
            super(itemView);
            view = itemView;
            tv_name = itemView.findViewById(R.id.tv_name);
            tv_phone = itemView.findViewById(R.id.tv_phone);
            tv_chepaihao = itemView.findViewById(R.id.tv_chepaihao);
        }
    }
}
