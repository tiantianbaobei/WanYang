package com.wanyangproject.myadapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.wanyangproject.R;
import com.wanyangproject.entity.MyLunTanPingLunEntity;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * Created by 甜甜 on 2018/7/31.
 */

public class MyPingLunAdapter extends RecyclerView.Adapter<MyPingLunAdapter.ViewHolder>{

    private Context context;
    private  List<MyLunTanPingLunEntity.ResponseBean> response;


    public MyPingLunAdapter(Context context, List<MyLunTanPingLunEntity.ResponseBean> response) {
        this.context = context;
        this.response = response;
    }




//    删除
    private ShanChuClick shanChuClick;

    public void setShanChuClick(ShanChuClick shanChuClick) {
        this.shanChuClick = shanChuClick;
    }

    public interface ShanChuClick{
        void shanchuClick(int position,String id);
    }


//    详情
    private XiangQingClick xiangQingClick;

    public void setXiangQingClick(XiangQingClick xiangQingClick) {
        this.xiangQingClick = xiangQingClick;
    }

    public interface XiangQingClick{
        void xiangqingClick(int position,String id);
    }



    @Override
    public MyPingLunAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.my_item_pinglun,parent,false);
        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final MyPingLunAdapter.ViewHolder holder, final int position) {

//        删除
        holder.tv_delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(shanChuClick != null){
                    shanChuClick.shanchuClick(holder.getAdapterPosition(),response.get(position).getReplyId());
                }
            }
        });


//        详情
        holder.linear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(xiangQingClick != null){
                    xiangQingClick.xiangqingClick(holder.getAdapterPosition(),response.get(position).getId());
                }
            }
        });


        if(response.get(position).getAdd_time() == null){

        }else{
            holder.tv_time.setText(stampToDate(response.get(position).getAdd_time()));
        }


        if(response.get(position).getTitle() == null){

        }else{
            holder.tv_neirong.setText("原贴："+response.get(position).getTitle());
        }



        if(response.get(position).getReplyContent() == null){

        }else{
            holder.tv_title.setText(response.get(position).getReplyContent());
        }



        if(response.get(position).getReplystatusName() == null){

        }else{
            holder.tv_zhuangtai.setText(response.get(position).getReplystatusName());
        }
    }





    /*
  * 将时间戳转换为时间
   */
    public String stampToDate(String s) {
        String res;
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm");
        long lt = new Long(s);
        Date date = new Date(lt*1000);
        res = simpleDateFormat.format(date);
        return res;
    }







    @Override
    public int getItemCount() {
        return response.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView tv_title,tv_neirong,tv_time,tv_delete,tv_zhuangtai;
        private LinearLayout linear;
        private View view;
        public ViewHolder(View itemView) {
            super(itemView);
            view = itemView;
            tv_title = itemView.findViewById(R.id.tv_title);
            tv_time = itemView.findViewById(R.id.tv_time);
            tv_neirong = itemView.findViewById(R.id.tv_neirong);
            tv_delete = itemView.findViewById(R.id.tv_delete);
            linear = itemView.findViewById(R.id.linear);
            tv_zhuangtai = itemView.findViewById(R.id.tv_zhuangtai);
        }
    }
}
