package com.wanyangproject.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.wanyangproject.R;
import com.wanyangproject.entity.FuWuJiLuShenQingXiangQingEntity;
import com.wanyangproject.fuwuactivity.BuChongWuYeFuWuActivity;
import com.wanyangproject.fuwuactivity.FuWuShenQingJiLuJinXingActivity;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * Created by 甜甜 on 2018/8/29.
 */

public class FuWuJiLuShenHeZhuangTaiAdapter extends RecyclerView.Adapter<FuWuJiLuShenHeZhuangTaiAdapter.ViewHolder>{

    private Context context;
    private  FuWuJiLuShenQingXiangQingEntity.ResponseBean response;
    private List<FuWuJiLuShenQingXiangQingEntity.ResponseBean.BucBean> buc;
    private String entype;



    public FuWuJiLuShenHeZhuangTaiAdapter(Context context, FuWuJiLuShenQingXiangQingEntity.ResponseBean response, List<FuWuJiLuShenQingXiangQingEntity.ResponseBean.BucBean> buc, String entype) {
        this.context = context;
        this.response = response;
        this.buc = buc;
        this.entype = entype;
    }




    @Override
    public FuWuJiLuShenHeZhuangTaiAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_fuwujilu_jinxing,parent,false);
        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }




    @Override
    public void onBindViewHolder(FuWuJiLuShenHeZhuangTaiAdapter.ViewHolder holder, final int position) {
        if(buc.get(position).getEntype() == null){

        }else{
//            if(position == 0){
//                0:待处理 1:处理中 2:完成 3:退回 4:关闭
            if(buc.get(position).getEntype().equals("3") && position != 0){
                holder.tv_buchong.setVisibility(View.GONE);
                holder.tv_title.setText("待补充");
//                holder.tv_neirong.setText("请您重新提交申请。");
                holder.tv_neirong.setText(buc.get(position).getRefusal());
            }else if(buc.get(position).getEntype().equals("0")){
                    holder.tv_buchong.setVisibility(View.GONE);
                    holder.tv_title.setText("待处理");
//                    holder.tv_neirong.setText("您的申请待处理。");
                    holder.tv_neirong.setText(buc.get(position).getRefusal());
                }else if(buc.get(position).getEntype().equals("1")){
                    holder.tv_buchong.setVisibility(View.GONE);
                    holder.tv_title.setText("处理中");
//                    holder.tv_neirong.setText("您的申请处理中。");
                    holder.tv_neirong.setText(buc.get(position).getRefusal());
                }else if(buc.get(position).getEntype().equals("2")){
                    holder.tv_buchong.setVisibility(View.GONE);
                    holder.tv_title.setText("已完成");
//                    holder.tv_neirong.setText("您的申请已完成。");
                holder.tv_neirong.setText(buc.get(position).getRefusal());
                }else if(buc.get(position).getEntype().equals("3")){
                    holder.tv_title.setText("待补充");
//                    holder.tv_neirong.setText("请您重新提交申请。");
                holder.tv_neirong.setText(buc.get(position).getRefusal());
                    holder.tv_buchong.setVisibility(View.VISIBLE);
                    holder.tv_buchong.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            Intent intent = new Intent(context,  BuChongWuYeFuWuActivity.class);
                            intent.putExtra("id",response.getId());
                            intent.putExtra("entype",entype);
                            intent.putExtra("jieshao",response.getContent());
                            intent.putExtra("jujueliyou",buc.get(0).getRefusal());
                            System.out.println(response.getContent()+"          介绍");
                            System.out.println(buc.get(0).getRefusal()  +"         拒绝理由 ");
                            context.startActivity(intent);
                        }
                    });
//                }else if(buc.get(position).getEntype().equals("2")){
//                    holder.tv_buchong.setVisibility(View.GONE);
//                    holder.tv_title.setText("已完成");
//                    holder.tv_neirong.setText("您的申请已完成。");
//                }else if(buc.get(position).getEntype().equals("5")){
//                    holder.tv_title.setText("已关闭");
//                    holder.tv_buchong.setVisibility(View.GONE);
//                    holder.tv_neirong.setText("您的申请已被管理员关闭。");
                }else if(buc.get(position).getEntype().equals("4")){
                    holder.tv_title.setText("已关闭");
                    holder.tv_buchong.setVisibility(View.GONE);
//                    holder.tv_neirong.setText("您的申请已关闭。");
                holder.tv_neirong.setText(buc.get(position).getRefusal());
                }

//                holder.tv_time.setText(stampToDate(response.getBuchong_time()));
                if(response.getAdd_time() != null){
                    String add_time = response.getAdd_time();
                    String substring = add_time.substring(0, 10);
                    String substring1 = add_time.substring(10, add_time.length());
                    System.out.println(substring+"      substring");
                    System.out.println(substring1+"     substring1substring1substring1");
                    holder.tv_time.setText(substring+" "+substring1);
//                    holder.tv_time.setText(response.getAdd_time());
                }
//                else{
//                    holder.tv_time.setText(stampToDate(response.getOperation_time()));
//                }

//                if(response.getAdd_time().equals("")){
//
//                }else{
//                    holder.tv_time.setText(response.getAdd_time());
//                }
//            }
//            else if(position == 1){
//                    holder.tv_buchong.setVisibility(View.GONE);
//                    holder.tv_title.setText("已处理");
//                    if(response.getBuchong_time().equals("")){
//
//                    }else{
//                        holder.tv_time.setText(stampToDate(response.getBuchong_time()));
//                    }
//                    holder.tv_neirong.setText("您的申请已处理。");
//            }
            else{
                holder.tv_buchong.setVisibility(View.GONE);
                holder.tv_title.setText("已处理");
                    String add_time = buc.get(position-1).getTime();
                    String substring = add_time.substring(0, 10);
                    String substring1 = add_time.substring(10, add_time.length());
                    System.out.println(substring+"      substring");
                    System.out.println(substring1+"     substring1substring1substring1");
                    holder.tv_time.setText(substring+" "+substring1);
//                holder.tv_time.setText(response.getAdd_time());
//                holder.tv_neirong.setText("您的申请已处理。");
                    holder.tv_neirong.setText(buc.get(position).getRefusal());
            }
        }




//        暂时测试补充
//        holder.tv_buchong.setVisibility(View.VISIBLE);
//        holder.tv_buchong.setText("补充");
//        holder.tv_buchong.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Intent intent = new Intent(context,  BuChongWuYeFuWuActivity.class);
//                intent.putExtra("id",response.getId());
//                intent.putExtra("entype",entype);
//                intent.putExtra("jieshao",response.getContent());
//                intent.putExtra("jujueliyou",response.getRefusal());
//                System.out.println(response.getContent()+"          介绍");
//                System.out.println(response.getRefusal()  +"               拒绝理由 ");
//                context.startActivity(intent);
//            }
//        });
//        、暂时测试补充

        if(position == 0){
            holder.view_shang.setVisibility(View.GONE);
        }

        if(buc.size() == 0){
//            if(response.getAdd_time() != null){
//                holder.tv_time.setText(response.getAdd_time());
//            }else
               if(buc.get(position).getTime() != null){
                   String add_time = buc.get(position).getTime();
                   String substring = add_time.substring(0, 10);
                   String substring1 = add_time.substring(10, add_time.length());
                   System.out.println(substring+"      substring");
                   System.out.println(substring1+"     substring1substring1substring1");
                   holder.tv_time.setText(substring+" "+substring1);
//                holder.tv_time.setText(buc.get(position).getTime());
            }
            holder.tv_buchong.setVisibility(View.GONE);
            holder.tv_title.setText("待处理");
//            holder.tv_neirong.setText("您的申请待处理。");
            holder.tv_neirong.setText(buc.get(position).getRefusal());
            holder.view_xia.setVisibility(View.GONE);
            return;
        }




        if(buc.size()-1  == position){
            holder.view_xia.setVisibility(View.GONE);
        }























//        原来
//        if(response.getType() == null){
//
//        }else{
//            if(position == 0){
//                if(response.getType().equals("1")){
//                    holder.tv_buchong.setVisibility(View.GONE);
//                    holder.tv_title.setText("待处理");
//                    holder.tv_neirong.setText("您的申请待处理。");
//                }else if(response.getType().equals("2")){
//                    holder.tv_buchong.setVisibility(View.GONE);
//                    holder.tv_title.setText("已完成");
//                    holder.tv_neirong.setText("您的申请已完成。");
//                }else if(response.getType().equals("3")){
//                    holder.tv_title.setText("待补充");
//                    holder.tv_neirong.setText("请您重新提交申请。");
//                    holder.tv_buchong.setVisibility(View.VISIBLE);
//                    holder.tv_buchong.setOnClickListener(new View.OnClickListener() {
//                        @Override
//                        public void onClick(View view) {
//                            Intent intent = new Intent(context,  BuChongWuYeFuWuActivity.class);
//                            intent.putExtra("id",response.getId());
//                            intent.putExtra("entype",entype);
//                            intent.putExtra("jieshao",response.getContent());
//                            intent.putExtra("jujueliyou",response.getRefusal());
//                            System.out.println(response.getContent()+"          介绍");
//                            System.out.println(response.getRefusal()  +"         拒绝理由 ");
//                            context.startActivity(intent);
//                        }
//                    });
//                }else if(response.getType().equals("4")){
//                    holder.tv_buchong.setVisibility(View.GONE);
//                    holder.tv_title.setText("已完成");
//                    holder.tv_neirong.setText("您的申请已完成。");
//                }else if(response.getType().equals("5")){
//                    holder.tv_title.setText("已关闭");
//                    holder.tv_buchong.setVisibility(View.GONE);
//                    holder.tv_neirong.setText("您的申请已被管理员关闭。");
//                }else if(response.getType().equals("6")){
//                    holder.tv_title.setText("已关闭");
//                    holder.tv_buchong.setVisibility(View.GONE);
//                    holder.tv_neirong.setText("您的申请已关闭。");
//
//                }
//
////                holder.tv_time.setText(stampToDate(response.getBuchong_time()));
//                if(response.getOperation_time().equals("")){
//                    holder.tv_time.setText(response.getAdd_time());
//                }else{
//                    holder.tv_time.setText(stampToDate(response.getOperation_time()));
//                }
//
////                if(response.getAdd_time().equals("")){
////
////                }else{
////                    holder.tv_time.setText(response.getAdd_time());
////                }
//            }
////            else if(position == 1){
////                    holder.tv_buchong.setVisibility(View.GONE);
////                    holder.tv_title.setText("已处理");
////                    if(response.getBuchong_time().equals("")){
////
////                    }else{
////                        holder.tv_time.setText(stampToDate(response.getBuchong_time()));
////                    }
////                    holder.tv_neirong.setText("您的申请已处理。");
////            }
//            else{
//                holder.tv_buchong.setVisibility(View.GONE);
//                holder.tv_title.setText("已处理");
//                holder.tv_time.setText(stampToDate(buc.get(position-1).getTime()));
//                holder.tv_neirong.setText("您的申请已处理。");
//            }
//        }
//
//
//
//
////        暂时测试补充
////        holder.tv_buchong.setVisibility(View.VISIBLE);
////        holder.tv_buchong.setText("补充");
////        holder.tv_buchong.setOnClickListener(new View.OnClickListener() {
////            @Override
////            public void onClick(View view) {
////                Intent intent = new Intent(context,  BuChongWuYeFuWuActivity.class);
////                intent.putExtra("id",response.getId());
////                intent.putExtra("entype",entype);
////                intent.putExtra("jieshao",response.getContent());
////                intent.putExtra("jujueliyou",response.getRefusal());
////                System.out.println(response.getContent()+"          介绍");
////                System.out.println(response.getRefusal()  +"               拒绝理由 ");
////                context.startActivity(intent);
////            }
////        });
////        、暂时测试补充
//
//
//
//
//        if(position == 0){
//            holder.view_shang.setVisibility(View.GONE);
//        }
//
//        if(buc == null){
//            if(response.getAdd_time() != null){
//                holder.tv_time.setText(response.getAdd_time());
//            }
//            holder.tv_buchong.setVisibility(View.GONE);
//            holder.tv_title.setText("待处理");
//            holder.tv_neirong.setText("您的申请待处理。");
//            holder.view_xia.setVisibility(View.GONE);
//            return;
//        }
//
//        if(buc.size()  == position){
//            holder.view_xia.setVisibility(View.GONE);
//        }
    }










    /*
* 将时间戳转换为时间
 */
    public String stampToDate(String s) {
        System.out.println(s+"              sssssssssss");

        if(s == null){
            return "";
        }

        if(s.length() == 0){
            return "";
        }

        if(s.indexOf("-") != -1){
            return "";
        }
        String res;
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        long lt = new Long(s);
        Date date = new Date(lt*1000);
        res = simpleDateFormat.format(date);
        return res;
    }


    @Override
    public int getItemCount() {
        return buc.size();
    }



//    原来
//    @Override
//    public int getItemCount() {
//        if(buc == null){
//            return 1;
//        }
//        return buc.size()+1;
//    }



    public class ViewHolder extends RecyclerView.ViewHolder {
        private ImageView image;
        private View view,view_shang,view_xia;
        private TextView tv_title,tv_neirong,tv_buchong,tv_time;
        public ViewHolder(View itemView) {
            super(itemView);
            view = itemView;
            image = itemView.findViewById(R.id.image);
            view_shang = itemView.findViewById(R.id.view_shang);
            tv_title = itemView.findViewById(R.id.tv_title);
            tv_neirong = itemView.findViewById(R.id.tv_neirong);
            tv_buchong = itemView.findViewById(R.id.tv_buchong);
            view_xia = itemView.findViewById(R.id.view_xia);
            tv_time = itemView.findViewById(R.id.tv_time);
        }

    }
}
