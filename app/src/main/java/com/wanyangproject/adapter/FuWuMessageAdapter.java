package com.wanyangproject.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.gson.Gson;
import com.wanyangproject.R;
import com.wanyangproject.entity.ShanChuXiTongXiaoXiEntity;
import com.wanyangproject.entity.XiTongXiaoXiEntity;
import com.wanyangproject.fuwuactivity.FuWuMessageActivity;
import com.wanyangproject.utils.ContractUtils;
import com.zhy.http.okhttp.OkHttpUtils;
import com.zhy.http.okhttp.callback.StringCallback;

import java.util.List;

import okhttp3.Call;

/**
 * Created by 甜甜 on 2018/7/24.
 */

public class FuWuMessageAdapter extends RecyclerView.Adapter<FuWuMessageAdapter.ViewHolder>{

    private Context context;
    private List<XiTongXiaoXiEntity.ResponseBean> response1;
    private TextView tvDelete;
    private ImageView imageDelete;
    private CheckBox imageQuanxuan;
    private RelativeLayout relative;

    public FuWuMessageAdapter(Context context, List<XiTongXiaoXiEntity.ResponseBean> response, TextView tvDelete, ImageView imageDelete,CheckBox imageQuanxuan,RelativeLayout relative) {
        this.context = context;
        this.response1 = response;
        this.tvDelete = tvDelete;
        this.imageDelete = imageDelete;
        this.imageQuanxuan = imageQuanxuan;
        this.relative = relative;
    }


    private FuWuMessageOnelick FuWuMessageOnelick;

    public void setFuWuMessageOnelick(FuWuMessageOnelick FuWuMessageOnelick) {
        this.FuWuMessageOnelick = FuWuMessageOnelick;
    }

    public interface FuWuMessageOnelick{
        void FuWuMessageOneClick();
    }


    @Override
    public FuWuMessageAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, final int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.fuwu_message_item, parent, false);
        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final FuWuMessageAdapter.ViewHolder holder, final int position) {
        if(response1.get(position).getAdd_time() == null){

        }else{
            holder.tv_time.setText(response1.get(position).getAdd_time());
        }

        if(response1.get(position).getTitle() == null){

        }else{
            holder.tv_dingdan_tixing.setText(response1.get(position).getTitle());
        }

        if(response1.get(position).getContes() == null){

        }else{
            holder.tv_neirong.setText(response1.get(position).getContes());
        }



        if(response1.get(position).getVisitor() == null){

        }else{
            if(response1.get(position).getVisitor().equals("1")){//	1：不是访客登记2：访客登记
                holder.image.setVisibility(View.GONE);
            }else if(response1.get(position).getVisitor().equals("2")){
                holder.image.setVisibility(View.VISIBLE);
                Glide.with(context).load(ContractUtils.YUANQUZHAOSHANG_PHOTO_URL+"/"+response1.get(position).getPhoto()).into(holder.image);
            }
        }


        if (response1.get(position).isZhuangtai()){
            holder.image_delete.setVisibility(View.VISIBLE);
        }else {
            holder.image_delete.setVisibility(View.GONE);
        }



        if (response1.get(position).isZhuangtai1()){
            holder.image_delete.setButtonDrawable(R.mipmap.checked);
        }else {
            holder.image_delete.setButtonDrawable(R.mipmap.unchecked);
        }




        holder.image_delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (holder.image_delete.isChecked()){
                    holder.image_delete.setButtonDrawable(R.mipmap.checked);
                    response1.get(position).setZhuangtai1(true);
                }else {
                    holder.image_delete.setButtonDrawable(R.mipmap.unchecked);
                    response1.get(position).setZhuangtai1(false);
                }

            }
        });








        imageQuanxuan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (imageQuanxuan.isChecked()){
                    imageQuanxuan.setButtonDrawable(R.mipmap.checked);
                    for (XiTongXiaoXiEntity.ResponseBean responseBean:response1){
                        responseBean.setZhuangtai1(true);
                    }
                    notifyDataSetChanged();
                }else {
                    imageQuanxuan.setButtonDrawable(R.mipmap.unchecked);
                    for (XiTongXiaoXiEntity.ResponseBean responseBean:response1){
                        responseBean.setZhuangtai1(false);
                    }
                    notifyDataSetChanged();
                }
            }
        });

        imageDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                imageDelete.setVisibility(View.GONE);
                relative.setVisibility(View.VISIBLE);
                tvDelete.setVisibility(View.VISIBLE);

                for (XiTongXiaoXiEntity.ResponseBean responseBean:response1){
                    responseBean.setZhuangtai(true);
                }
                notifyDataSetChanged();
            }
        });



        tvDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                imageDelete.setVisibility(View.VISIBLE);
//                relative.setVisibility(View.GONE);
//                tvDelete.setVisibility(View.GONE);
                String id = "";
//                for (XiTongXiaoXiEntity.ResponseBean responseBean:response1){
//                    responseBean.setZhuangtai(false);
//                }
//                notifyDataSetChanged();


                Boolean isOK = false;
                for (XiTongXiaoXiEntity.ResponseBean responseBean:response1){
//                    responseBean.setZhuangtai(false);
                    if(responseBean.isZhuangtai1()){
                        isOK = true;
                    }
                }

                if(isOK){
                    //                系统消息删除的网络请求
                    for (XiTongXiaoXiEntity.ResponseBean responseBean:response1) {
//                        responseBean.setZhuangtai(false);
                        if(responseBean.isZhuangtai1()) {
                            id = id + responseBean.getId()+",";
                        }
                    }
                } else{
                    Toast.makeText(context, "请先选择", Toast.LENGTH_SHORT).show();
                    return;
                }
//                imageDelete.setVisibility(View.VISIBLE);
//                relative.setVisibility(View.GONE);
//                tvDelete.setVisibility(View.GONE);
                System.out.println(id+"            iiiiiiiiiiddddddddddd");
                initXiTongXiaoXiHttp(id);
            }
        });
    }




//    系统消息删除的网络请求
    private void initXiTongXiaoXiHttp(String id) {
        OkHttpUtils.post()
                .url(ContractUtils.LOGIN_URL+"life/messagedelect")
                .addHeader("token",ContractUtils.getTOKEN(context))
                .addParams("parkId",ContractUtils.getParkId(context))
                .addParams("id",id)
                .build()
                .execute(new StringCallback() {
                    @Override
                    public void onError(Call call, Exception e, int id) {
                        System.out.println(e+"           eeeeee系统消息删除");
                        imageDelete.setVisibility(View.VISIBLE);
                        relative.setVisibility(View.GONE);
                        tvDelete.setVisibility(View.GONE);
                    }

                    @Override
                    public void onResponse(String response, int id) {
                        System.out.println(response+"           系统消息删除的网络请求");
                        ContractUtils.Code500(context,response);
                        if(response.indexOf("200") != -1){
                            Gson gson = new Gson();
                            ShanChuXiTongXiaoXiEntity shanChuXiTongXiaoXiEntity = gson.fromJson(response, ShanChuXiTongXiaoXiEntity.class);
                            Toast.makeText(context, shanChuXiTongXiaoXiEntity.getResponse().getMes(), Toast.LENGTH_SHORT).show();
                            for (XiTongXiaoXiEntity.ResponseBean responseBean:response1){
                                responseBean.setZhuangtai(false);
                            }
                            relative.setVisibility(View.GONE);
                            tvDelete.setVisibility(View.GONE);
                            imageDelete.setVisibility(View.VISIBLE);
                            if(FuWuMessageOnelick != null){
                                FuWuMessageOnelick.FuWuMessageOneClick();
                            }

                        }else{
                            imageDelete.setVisibility(View.VISIBLE);
                            relative.setVisibility(View.GONE);
                            tvDelete.setVisibility(View.GONE);
                        }
                    }
                });
    }


    
    @Override
    public int getItemCount() {
        return response1.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView tv_time,tv_dingdan_tixing,tv_neirong;
        private ImageView image;
        private CheckBox image_delete;
        private LinearLayout relative_message;
        private View view;
        public ViewHolder(View itemView) {
            super(itemView);
            view = itemView;
            tv_time = itemView.findViewById(R.id.tv_time);
            tv_dingdan_tixing = itemView.findViewById(R.id.tv_dingdan_tixing);
            tv_neirong = itemView.findViewById(R.id.tv_neirong);
            image = itemView.findViewById(R.id.image);
            image_delete = itemView.findViewById(R.id.image_delete);
            relative_message = itemView.findViewById(R.id.relative_message);
        }
    }
}
