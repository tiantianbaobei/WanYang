package com.wanyangproject.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.wanyangproject.R;
import com.wanyangproject.entity.DaiShouHuoEntity;
import com.wanyangproject.entity.DingDanXiangQingEntity;
import com.wanyangproject.my.TuiKuanActivity;
import com.wanyangproject.utils.ContractUtils;

/**
 * Created by 甜甜 on 2018/8/24.
 */

public class TuiKuanYuanYinShangMianAdapter extends RecyclerView.Adapter<TuiKuanYuanYinShangMianAdapter.ViewHolder>{

    private Context context;
    private DingDanXiangQingEntity.ResponseBean response;

    public TuiKuanYuanYinShangMianAdapter(Context context, DingDanXiangQingEntity.ResponseBean response) {
        this.context = context;
        this.response = response;

    }


    @Override
    public TuiKuanYuanYinShangMianAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_tuikuan_yuanyin,parent,false);
        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(TuiKuanYuanYinShangMianAdapter.ViewHolder holder, int position) {
        if(ContractUtils.PHOTO_URL+response.getGoods().get(position).getMaster() == null){

        }else{ // 商品头像
            Glide.with(context).load(ContractUtils.PHOTO_URL+response.getGoods().get(position).getMaster()).into(holder.image_touxiang);
        }


        if(response.getGoods().get(position).getGoods_name() == null){

        }else{ // 商品名称
            holder.tv_name.setText(response.getGoods().get(position).getGoods_name());
        }


        if(response.getGoods().get(position).getNum() == null){

        }else{ // 商品数量
            holder.tv_number.setText("x"+response.getGoods().get(position).getNum());
        }

    }

    @Override
    public int getItemCount() {
        return response.getGoods().size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private ImageView image_touxiang;
        private TextView tv_name,tv_number;
        private View view;
        public ViewHolder(View itemView) {
            super(itemView);
            view = itemView;
            image_touxiang = itemView.findViewById(R.id.image_touxiang);
            tv_name = itemView.findViewById(R.id.tv_name);
            tv_number = itemView.findViewById(R.id.tv_number);
        }
    }
}
