package com.wanyangproject.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.wanyangproject.R;
import com.wanyangproject.activity.YuanQuZhaoShangFenLeiActivity;
import com.wanyangproject.entity.FenLeititleEntity;

import java.util.List;

/**
 * Created by 甜甜 on 2018/9/10.
 */

public class FenLeiZiXunAdapter extends RecyclerView.Adapter<FenLeiZiXunAdapter.ViewHolder>{


    private Context context;
    private List<FenLeititleEntity.ResponseBean.ListBean> list;

    public FenLeiZiXunAdapter(Context context, List<FenLeititleEntity.ResponseBean.ListBean> list) {
        this.context = context;
        this.list = list;
    }




//    点击进入详情
    private XiangQingClick xiangQingClick;

    public XiangQingClick getXiangQingClick() {
        return xiangQingClick;
    }

    public void setXiangQingClick(XiangQingClick xiangQingClick) {
        this.xiangQingClick = xiangQingClick;
    }

    public interface XiangQingClick{
        void xiangqingClick(int position,String id);
}





    @Override
    public FenLeiZiXunAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_fenlei, parent, false);
        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final FenLeiZiXunAdapter.ViewHolder holder, final int position) {

//        点击进入详情
        holder.relative.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(xiangQingClick != null){
                    xiangQingClick.xiangqingClick(holder.getAdapterPosition(),list.get(position).getId());
                }
            }
        });




        if(list.get(position).getTitle() == null){

        }else{  // 标题
            holder.tv_title.setText(list.get(position).getTitle());
        }

        if(list.get(position).getDesc() == null){

        }else{  // 内容
            holder.tv_neirong.setText(list.get(position).getDesc());
        }

        if(list.get(position).getAdd_time() == null){

        }else{  // 时间
            holder.tv_time.setText(list.get(position).getAdd_time());
        }

//        if(list.get(position).getLogo() == null){
//
//        }else{  // 头像
//            Glide.with(context).load(list.get(position).getLogo().replace("\"","")).into(holder.image_touxiang);
//        }

    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
//        private ImageView image_touxiang;
        private TextView tv_title,tv_time,tv_neirong;
        private RelativeLayout relative;
        private View view;
        public ViewHolder(View itemView) {
            super(itemView);
            view = itemView;
//            image_touxiang = itemView.findViewById(R.id.image_touxiang);
            tv_title = itemView.findViewById(R.id.tv_title);
            tv_time = itemView.findViewById(R.id.tv_time);
            tv_neirong = itemView.findViewById(R.id.tv_neirong);
            relative = itemView.findViewById(R.id.relative);
        }
    }
}
