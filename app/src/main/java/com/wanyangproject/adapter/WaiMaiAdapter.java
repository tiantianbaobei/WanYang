package com.wanyangproject.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import java.util.List;

/**
 * Created by 甜甜 on 2018/7/21.
 */

public class WaiMaiAdapter extends FragmentPagerAdapter{
    private List<Fragment> FragmentList;

    public void setFragments(List<Fragment> fragments) {
        FragmentList = fragments;
    }

    public WaiMaiAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        Fragment fragment = FragmentList.get(position);
        return fragment;
    }

    @Override
    public int getCount() {
        return FragmentList.size();
    }
}
