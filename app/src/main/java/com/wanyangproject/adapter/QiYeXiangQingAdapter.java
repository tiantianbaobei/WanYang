package com.wanyangproject.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.wanyangproject.R;
import com.wanyangproject.entity.QiYeXiangQingEntity;
import com.wanyangproject.my.ZhaoPinXiangQingActivity;
import com.wanyangproject.shouye.QiYeXiangQingActivity;
import com.wanyangproject.shouye.YuanQuQiYeActivity;

import java.util.List;

/**
 * Created by 甜甜 on 2018/7/25.
 */

public class QiYeXiangQingAdapter extends RecyclerView.Adapter<QiYeXiangQingAdapter.ViewHolder>{

    private Context context;
    private List<QiYeXiangQingEntity.ResponseBean.RecruitBean> recruit;

    public QiYeXiangQingAdapter(Context context, List<QiYeXiangQingEntity.ResponseBean.RecruitBean> recruit) {
        this.context = context;
        this.recruit = recruit;
    }




//    点击招聘信息进入详情页
    private XiangQingClick xiangQingClick;

    public void setXiangQingClick(XiangQingClick xiangQingClick) {
        this.xiangQingClick = xiangQingClick;
    }

    public interface XiangQingClick{
        void xiangqingClick(int position,String id);
}






    @Override
    public QiYeXiangQingAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.zhaopin_xinxi_item, parent, false);
        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final QiYeXiangQingAdapter.ViewHolder holder, final int position) {



        holder.zhaopin_fuwuyuan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(xiangQingClick != null){
                    xiangQingClick.xiangqingClick(holder.getAdapterPosition(),recruit.get(position).getId());
                }
            }
        });









        if(recruit.get(position).getTitle() == null){

        }else{ // 标题
            holder.tv_name.setText(recruit.get(position).getTitle());
        }


        if(recruit.get(position).getMoney() == null){

        }else{ // 薪资
            holder.tv_xinzi.setText(recruit.get(position).getMoney());
        }

        if(recruit.get(position).getDesc() == null){

        }else{ // 内容
            String content = recruit.get(position).getDesc();
            String replace1 = content.replace("&nbsp;", " ");
            holder.tv_neirong.setText("职位要求："+replace1);
        }
    }





    @Override
    public int getItemCount() {
        if(recruit == null){
            return 0;
        }
        return recruit.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView tv_name,tv_xinzi,tv_neirong;
        private LinearLayout zhaopin_fuwuyuan;
        private View view;
        public ViewHolder(View itemView) {
            super(itemView);
            view = itemView;
            tv_neirong = itemView.findViewById(R.id.tv_neirong);
            tv_name = itemView.findViewById(R.id.tv_name);
            tv_xinzi = itemView.findViewById(R.id.tv_xinzi);
            zhaopin_fuwuyuan = itemView.findViewById(R.id.zhaopin_fuwuyuan);
        }
    }
}
