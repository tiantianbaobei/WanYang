package com.wanyangproject.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import java.util.List;

/**
 * Created by 甜甜 on 2018/8/10.
 */

public class TabAdapter extends FragmentPagerAdapter {
    private List<Fragment> FragmentList;
    private List<String> stringList;

    public TabAdapter(FragmentManager supportFragmentManager, List<Fragment> fragments, List<String> stringList) {
        super(supportFragmentManager);
        this.FragmentList = fragments;
        this.stringList = stringList;
    }

    public void setFragments(List<Fragment> fragments) {
        FragmentList = fragments;
    }


    @Override
    public Fragment getItem(int position) {
        Fragment fragment = FragmentList.get(position);
        return fragment;
    }

    @Override
    public int getCount() {
        return FragmentList.size();
    }


    @Override
    public CharSequence getPageTitle(int position) {
        return stringList.get(position);
    }
}
