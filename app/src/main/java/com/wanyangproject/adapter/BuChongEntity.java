package com.wanyangproject.adapter;

import java.util.List;

/**
 * Created by 甜甜 on 2018/8/29.
 */

public class BuChongEntity {

    /**
     * code : 200
     * msg :
     * response : {"mes":"操作成功"}
     * request : {"id":"37","entype":"1","Remarks":"测试"}
     * other : []
     */

    private int code;
    private String msg;
    private ResponseBean response;
    private RequestBean request;
    private List<?> other;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public ResponseBean getResponse() {
        return response;
    }

    public void setResponse(ResponseBean response) {
        this.response = response;
    }

    public RequestBean getRequest() {
        return request;
    }

    public void setRequest(RequestBean request) {
        this.request = request;
    }

    public List<?> getOther() {
        return other;
    }

    public void setOther(List<?> other) {
        this.other = other;
    }

    public static class ResponseBean {
        /**
         * mes : 操作成功
         */

        private String mes;

        public String getMes() {
            return mes;
        }

        public void setMes(String mes) {
            this.mes = mes;
        }
    }

    public static class RequestBean {
        /**
         * id : 37
         * entype : 1
         * Remarks : 测试
         */

        private String id;
        private String entype;
        private String Remarks;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getEntype() {
            return entype;
        }

        public void setEntype(String entype) {
            this.entype = entype;
        }

        public String getRemarks() {
            return Remarks;
        }

        public void setRemarks(String Remarks) {
            this.Remarks = Remarks;
        }
    }
}
