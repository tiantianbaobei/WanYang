package com.wanyangproject.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.wanyangproject.R;
import com.wanyangproject.entity.YuanQuShouCeEntity;

import java.util.List;

/**
 * Created by 甜甜 on 2018/8/16.
 */

public class YuanQuShouCeAdapter extends RecyclerView.Adapter<YuanQuShouCeAdapter.ViewHolder>{

    private Context context;
    private List<YuanQuShouCeEntity.ResponseBean.ListBean> list;


    public YuanQuShouCeAdapter(Context context, List<YuanQuShouCeEntity.ResponseBean.ListBean> list) {
        this.context = context;
        this.list = list;
    }


//    点击跳转详情页面
    private YuanQuShouCeXiangQingClick yuanQuShouCeXiangQingClick;

    public void setYuanQuShouCeXiangQingClick(YuanQuShouCeXiangQingClick yuanQuShouCeXiangQingClick) {
        this.yuanQuShouCeXiangQingClick = yuanQuShouCeXiangQingClick;
    }

    public interface YuanQuShouCeXiangQingClick{
        void yuanqushoucexiangqingClick(int position);
    }



    @Override
    public YuanQuShouCeAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_yuanqu_shouce, parent, false);
        final ViewHolder viewHolder = new ViewHolder(view);
        viewHolder.view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(yuanQuShouCeXiangQingClick != null){
                    yuanQuShouCeXiangQingClick.yuanqushoucexiangqingClick(viewHolder.getAdapterPosition());
                }
            }
        });
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(YuanQuShouCeAdapter.ViewHolder holder, int position) {
        if(list.get(position).getTitle() == null){

        }else{ // 标题
            holder.tv_yuanqu_jieshao.setText(list.get(position).getTitle());
        }
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView tv_yuanqu_jieshao;
        private View view;
        public ViewHolder(View itemView) {
            super(itemView);
            view = itemView;
            tv_yuanqu_jieshao = itemView.findViewById(R.id.tv_yuanqu_jieshao);
        }
    }
}
