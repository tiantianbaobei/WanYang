package com.wanyangproject.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.wanyangproject.R;
import com.wanyangproject.entity.ShangJiaPingFenEntity;

import java.util.List;

/**
 * Created by 甜甜 on 2018/8/25.
 */

public class ShangJiaPingLunAdapter extends RecyclerView.Adapter<ShangJiaPingLunAdapter.ViewHolder>{

    private Context context;
    private List<ShangJiaPingFenEntity.ResponseBean.ListBean> shangJiaPingFenEntity;


    public ShangJiaPingLunAdapter(Context context, List<ShangJiaPingFenEntity.ResponseBean.ListBean> shangJiaPingFenEntity) {
        this.context = context;
        this.shangJiaPingFenEntity = shangJiaPingFenEntity;
    }

    @Override
    public ShangJiaPingLunAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_shangjia_pinglun, parent, false);
        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ShangJiaPingLunAdapter.ViewHolder holder, int position) {
        if(shangJiaPingFenEntity.get(position).getAdd_time() == null){

        }else{ // 时间
            holder.tv_time.setText(shangJiaPingFenEntity.get(position).getAdd_time());
        }


        if(shangJiaPingFenEntity.get(position).getContent() == null){

        }else{ // 评论内容
            holder.tv_pinglun.setText(shangJiaPingFenEntity.get(position).getContent());
        }


        if(shangJiaPingFenEntity.get(position).getAvatar() == null){

        }else{
            Glide.with(context).load(shangJiaPingFenEntity.get(position).getAvatar()).into(holder.image_touxiang);
        }


        if(shangJiaPingFenEntity.get(position).getNickname() == null){

        }else{
            holder.tv_name.setText(shangJiaPingFenEntity.get(position).getNickname());
        }




    }

    @Override
    public int getItemCount() {
        return shangJiaPingFenEntity.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private ImageView image_touxiang;
        private TextView tv_name,tv_time,tv_pinglun;
        private View view;
        public ViewHolder(View itemView) {
            super(itemView);
            view = itemView;
            image_touxiang = itemView.findViewById(R.id.image_touxiang);
            tv_name = itemView.findViewById(R.id.tv_name);
            tv_time = itemView.findViewById(R.id.tv_time);
            tv_pinglun = itemView.findViewById(R.id.tv_pinglun);
        }
    }
}
