package com.wanyangproject.adapter;

import android.content.Context;
import android.view.View;
import android.widget.TextView;


import com.wanyangproject.R;
import com.wanyangproject.model.TestModel;
import com.wanyangproject.net.util.Constant;
import com.wanyangproject.widget.recyclerview.RecyclerAdapter;
import com.wanyangproject.widget.recyclerview.RecyclerHolder;
import com.wanyangproject.widget.recyclerview.RecyclerListener;

import java.util.List;

public class ForumAdapter extends RecyclerAdapter<TestModel.ResultsBean> implements RecyclerListener.OnClickListener<RecyclerHolder> {

    private int forum;
    private TextView forumState;

    public ForumAdapter(Context context, List<TestModel.ResultsBean> data, int forum) {
        super(context, data, R.layout.item_forum);
        this.forum = forum;
    }

    @Override
    public void convert(RecyclerHolder holder, TestModel.ResultsBean model) {
        forumState = holder.findViewById(R.id.forum_state);
        switch (forum) {
            case Constant.FORUM_ISSUE:
                setView(holder.position);
                break;
            case Constant.FORUM_COMMENT:
                setView(0);
                break;
            case Constant.FORUM_LIKE:
                setView(0);
                break;
        }
        holder.setOnClickListener(this, R.id.forum_ll, R.id.forum_delete);
    }

    private void setView(int position) {
        if (position % 3 == 0) {
            forumState.setText("");
        } else if (position % 3 == 1) {
            forumState.setText("待审核");
            forumState.setTextColor(mContext.getResources().getColor(R.color.blue));
        } else if (position % 3 == 2) {
            forumState.setText("审核未通过");
            forumState.setTextColor(mContext.getResources().getColor(R.color.red));
        }
    }

    @Override
    public void onClick(View v, int position, RecyclerHolder holder) {
        switch (v.getId()) {
            case R.id.forum_ll:
                com.wanyangproject.ui.ForumDetailActivity.start(mContext);
                break;
            case R.id.forum_delete:
                removeItem(position);
                break;
        }
    }
}