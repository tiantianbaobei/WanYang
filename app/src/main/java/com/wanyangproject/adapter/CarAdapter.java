package com.wanyangproject.adapter;

import android.content.Context;
import android.widget.TextView;


import com.wanyangproject.R;
import com.wanyangproject.model.CarModel;
import com.wanyangproject.widget.recyclerview.RecyclerAdapter;
import com.wanyangproject.widget.recyclerview.RecyclerHolder;

import java.util.List;

public class CarAdapter extends RecyclerAdapter<CarModel> {

    public CarAdapter(Context context, List<CarModel> data) {
        super(context, data, R.layout.item_car);
    }

    @Override
    public void convert(RecyclerHolder holder, CarModel model) {
        TextView carName = holder.findViewById(R.id.car_name);
        TextView carTel = holder.findViewById(R.id.car_tel);
        TextView carNum = holder.findViewById(R.id.car_num);
        carNum.setText(model.getNum());
    }


}