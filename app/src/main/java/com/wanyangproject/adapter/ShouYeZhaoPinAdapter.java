package com.wanyangproject.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.wanyangproject.R;
import com.wanyangproject.entity.QiuZhiZhaoPinLieBiaoEntity;

import java.util.List;

/**
 * Created by 甜甜 on 2018/8/5.
 */

public class ShouYeZhaoPinAdapter extends RecyclerView.Adapter<ShouYeZhaoPinAdapter.ViewHolder>{

    private Context context;
    private List<QiuZhiZhaoPinLieBiaoEntity.ResponseBean> response;

    public ShouYeZhaoPinAdapter(Context context, List<QiuZhiZhaoPinLieBiaoEntity.ResponseBean> response) {
        this.context = context;
        this.response = response;
    }


    //   点击行布局进入详情
    private ShouYeZhaoPinClick shouYeZhaoPinClick;

    public void setShouYeZhaoPinClick(ShouYeZhaoPinClick shouYeZhaoPinClick) {
        this.shouYeZhaoPinClick = shouYeZhaoPinClick;
    }

    public interface ShouYeZhaoPinClick{
        void shouyezhaopinClick(int position);
    }


    @Override
    public ShouYeZhaoPinAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_shouye_zhaopin, parent, false);
        final ViewHolder viewHolder = new ViewHolder(view);
        viewHolder.view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(shouYeZhaoPinClick != null){
                    shouYeZhaoPinClick.shouyezhaopinClick(viewHolder.getAdapterPosition());
                }
            }
        });
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ShouYeZhaoPinAdapter.ViewHolder holder, int position) {
        if(response.get(position).getTitle() == null){

        }else{ //标题
            holder.tv_title.setText(response.get(position).getTitle());
        }

        if(response.get(position).getMoney() == null){

        }else{  //薪资
            holder.tv_money.setText(response.get(position).getMoney());
        }

        if(response.get(position).getEntername() == null){

        }else{
            holder.tv_gongsi.setText("公司："+response.get(position).getEntername());
        }



        if(response.get(position).getAddress() == null){

        }else{
            holder.tv_address.setText("地址："+response.get(position).getAddress());
        }


    }

    @Override
    public int getItemCount() {
        return response.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView tv_title,tv_money,tv_gongsi,tv_address;
        private RelativeLayout relative;
        private View view;
        public ViewHolder(View itemView) {
            super(itemView);
            view = itemView;
            tv_title = itemView.findViewById(R.id.tv_title);
            tv_money = itemView.findViewById(R.id.tv_money);
            tv_gongsi = itemView.findViewById(R.id.tv_gongsi);
            tv_address = itemView.findViewById(R.id.tv_address);
            relative = itemView.findViewById(R.id.relative);
        }
    }
}
