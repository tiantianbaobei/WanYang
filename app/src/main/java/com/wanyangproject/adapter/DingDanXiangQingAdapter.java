package com.wanyangproject.adapter;

import com.bumptech.glide.Glide;
import com.wanyangproject.R;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.wanyangproject.entity.NumberEntity;
import com.wanyangproject.entity.TiJiaoDingDanEntity;
import com.wanyangproject.utils.ContractUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by 甜甜 on 2018/8/8.
 */

public class DingDanXiangQingAdapter extends RecyclerView.Adapter<DingDanXiangQingAdapter.ViewHolder>{

    private Context context;
    private ArrayList<NumberEntity> goods;


    public DingDanXiangQingAdapter(Context context, ArrayList<NumberEntity> goods) {
        this.context = context;
        this.goods = goods;

    }

    @Override
    public DingDanXiangQingAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.iten_dingdanxiangqing, parent, false);
        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(DingDanXiangQingAdapter.ViewHolder holder, int position) {
        if(goods.get(position).getName() == null){

        }else{  // 名称
            holder.tv_name.setText(goods.get(position).getName());
        }

        if(goods.get(position).getNumber() == null){

        }else{ // 数量
            holder.tv_number.setText("x"+goods.get(position).getNumber());
        }

        if(goods.get(position).getPic() == null){

        }else{ // 单价
            holder.tv_money.setText("¥ "+goods.get(position).getPic());
        }




//        if(ContractUtils.PHOTO_URL+goods.get(position).getMaster() == null){
//
//        }else{
//            Glide.with(context).load(ContractUtils.PHOTO_URL+goods.get(position).getMaster()).into(holder.image_tupian);
//        }



    }





    @Override
    public int getItemCount() {
        return goods.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView tv_name,tv_money,tv_number;
        private ImageView image_tupian;
        private View view;
        public ViewHolder(View itemView) {
            super(itemView);
            view = itemView;
            tv_name = itemView.findViewById(R.id.tv_name);
            tv_money = itemView.findViewById(R.id.tv_money);
            tv_number = itemView.findViewById(R.id.tv_number);
            image_tupian = itemView.findViewById(R.id.image_tupian);
        }
    }
}
