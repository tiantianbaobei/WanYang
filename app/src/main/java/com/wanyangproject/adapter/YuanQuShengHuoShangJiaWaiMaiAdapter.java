package com.wanyangproject.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.wanyangproject.R;
import com.wanyangproject.entity.YuanQuShengHuoShangJiaYouHUiHuoDongEntity;
import com.wanyangproject.entity.YuanQuShengHuoTuiJianShangJiaEntity;
import com.wanyangproject.shouye.YuanQuShengHuoActivity;

import java.util.List;

/**
 * Created by 甜甜 on 2018/7/24.
 */

public class YuanQuShengHuoShangJiaWaiMaiAdapter extends RecyclerView.Adapter<YuanQuShengHuoShangJiaWaiMaiAdapter.ViewHolder>{

    private Context context;
    private List<YuanQuShengHuoShangJiaYouHUiHuoDongEntity.ResponseBean> shopPreferences;

    public YuanQuShengHuoShangJiaWaiMaiAdapter(Context context, List<YuanQuShengHuoShangJiaYouHUiHuoDongEntity.ResponseBean> shopPreferences) {
        this.context = context;
        this.shopPreferences = shopPreferences;
    }



    private XiangQingClick xiangQingClick;

    public void setXiangQingClick(XiangQingClick xiangQingClick) {
        this.xiangQingClick = xiangQingClick;
    }

    public interface XiangQingClick{
        void xiangqingClick(int position);
    }

    @Override
    public YuanQuShengHuoShangJiaWaiMaiAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.yuanqushenghuo_shangjiahuodong_item, parent, false);
        final ViewHolder viewHolder = new ViewHolder(view);
        viewHolder.view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(viewHolder != null){
                    xiangQingClick.xiangqingClick(viewHolder.getAdapterPosition());
                }
            }
        });
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(YuanQuShengHuoShangJiaWaiMaiAdapter.ViewHolder holder, int position) {
        if(shopPreferences.get(position).getTitle() == null){

        }else{
            holder.tv_title.setText(shopPreferences.get(position).getTitle());
        }


        if(shopPreferences.get(position).getContent() == null){

        }else{
            holder.tv_neirong.setText(shopPreferences.get(position).getContent());
        }
    }

    @Override
    public int getItemCount() {
        return shopPreferences.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView tv_title,tv_neirong;
        private View view;
        public ViewHolder(View itemView) {
            super(itemView);
            view = itemView;
            tv_title = itemView.findViewById(R.id.tv_title);
            tv_neirong = itemView.findViewById(R.id.tv_neirong);

        }
    }
}
