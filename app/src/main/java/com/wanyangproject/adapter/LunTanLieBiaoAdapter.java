package com.wanyangproject.adapter;

import android.content.Context;
import android.widget.CompoundButton;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.chad.library.adapter.base.BaseMultiItemQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.wanyangproject.R;
import com.wanyangproject.base.MyApp;
import com.wanyangproject.entity.LunTanLieBiaoEntity;
import com.wanyangproject.luntanfragment.QuanBuFragment;
import com.wanyangproject.utils.ContractUtils;

import java.util.List;

/**
 * Created by 甜甜 on 2018/8/9.
 */

public class LunTanLieBiaoAdapter extends BaseMultiItemQuickAdapter<LunTanLieBiaoEntity.ResponseBean.ListBean,BaseViewHolder> {
    /**
     * Same as QuickAdapter#QuickAdapter(Context,int) but with
     * some initialization data.
     *
     * @param list A new list is created out of this one to avoid mutable list
     */

    public LunTanLieBiaoAdapter(List<LunTanLieBiaoEntity.ResponseBean.ListBean> list) {
        super(list);
        addItemType(LunTanLieBiaoEntity.ResponseBean.ListBean.LING,R.layout.item_luntan_liebiao_ling);
        addItemType(LunTanLieBiaoEntity.ResponseBean.ListBean.ONE,R.layout.item_luntan_liebiao_one);
        addItemType(LunTanLieBiaoEntity.ResponseBean.ListBean.TWO, R.layout.item_luntan_liebiao_two);
        addItemType(LunTanLieBiaoEntity.ResponseBean.ListBean.THREE,R.layout.item_luntan_liebiao_three);

    }


    private DianZanClick dianZanClick;

    public void setDianZanClick(DianZanClick dianZanClick) {
        this.dianZanClick = dianZanClick;
    }

    public interface DianZanClick{
        void dianzanClick(int position);
    }



    public void ss(List<LunTanLieBiaoEntity.ResponseBean.ListBean> list){
        addData(list);
        notifyDataSetChanged();
    }



    @Override
    protected void convert(final BaseViewHolder helper, LunTanLieBiaoEntity.ResponseBean.ListBean list) {
        switch (helper.getItemViewType()){
            case LunTanLieBiaoEntity.ResponseBean.ListBean.LING:
                helper.setText(R.id.tv_name,list.getUinfo().getNickname())
                        .setText(R.id.tv_title,list.getTitle())
                        .setText(R.id.tv_time,list.getAdd_time())
                        .setText(R.id.tv_neirong,list.getContent())
                        .setText(R.id.tv_zan_number,list.getLike())
                        .setText(R.id.tv_pinglun_number,list.getReply())
                        .addOnClickListener(R.id.image_zan);
                if(list.getIsLike().equals("1")){
                    helper.setImageResource(R.id.image_zan,R.mipmap.dianzan_true);
                }else{
                    helper.setImageResource(R.id.image_zan,R.mipmap.dianzan_false);
                }
//                头像
                Glide.with(MyApp.getContext()).load(list.getUinfo().getAvatar()).into((ImageView) helper.getView(R.id.image_touxiang));



                break;
            case LunTanLieBiaoEntity.ResponseBean.ListBean.ONE:
                helper.setText(R.id.tv_name,list.getUinfo().getNickname())
                        .setText(R.id.tv_title,list.getTitle())
                        .setText(R.id.tv_time,list.getAdd_time())
                        .setText(R.id.tv_neirong,list.getContent())
                        .setText(R.id.tv_zan_number,list.getLike())
                        .setText(R.id.tv_pinglun_number,list.getReply())
                        .addOnClickListener(R.id.image_zan);
                if(list.getIsLike().equals("1")){
                    helper.setImageResource(R.id.image_zan,R.mipmap.dianzan_true);
                }else{
                    helper.setImageResource(R.id.image_zan,R.mipmap.dianzan_false);
                }

//                头像
                Glide.with(MyApp.getContext()).load(list.getUinfo().getAvatar()).into((ImageView) helper.getView(R.id.image_touxiang));
//                内容图片
                Glide.with(MyApp.getContext()).load(list.getImgArr().get(0).replace(" ","")).into((ImageView)helper.getView(R.id.image));
                break;
            case LunTanLieBiaoEntity.ResponseBean.ListBean.TWO:
                helper.setText(R.id.tv_name,list.getUinfo().getNickname())
                        .setText(R.id.tv_title,list.getTitle())
                        .setText(R.id.tv_time,list.getAdd_time())
                        .setText(R.id.tv_neirong,list.getContent())
                        .setText(R.id.tv_zan_number,list.getLike())
                        .setText(R.id.tv_pinglun_number,list.getReply())
                        .addOnClickListener(R.id.image_zan);
                if(list.getIsLike().equals("1")){
                    helper.setImageResource(R.id.image_zan,R.mipmap.dianzan_true);
//
                }else{
                    helper.setImageResource(R.id.image_zan,R.mipmap.dianzan_false);
                }
//                        头像
                Glide.with(MyApp.getContext()).load(list.getUinfo().getAvatar()).into((ImageView) helper.getView(R.id.image_touxiang));
//               内容图片
                Glide.with(MyApp.getContext()).load(list.getImgArr().get(0).replace(" ","")).into((ImageView)helper.getView(R.id.image_one));
                Glide.with(MyApp.getContext()).load(list.getImgArr().get(1).replace(" ","")).into((ImageView)helper.getView(R.id.image_two));
                break;
            case LunTanLieBiaoEntity.ResponseBean.ListBean.THREE:
                helper.setText(R.id.tv_name,list.getUinfo().getNickname())
                        .setText(R.id.tv_title,list.getTitle())
                        .setText(R.id.tv_time,list.getAdd_time())
                        .setText(R.id.tv_neirong,list.getContent())
                        .setText(R.id.tv_zan_number,list.getLike())
                        .setText(R.id.tv_pinglun_number,list.getReply())
                        .addOnClickListener(R.id.image_zan);
                if(list.getIsLike().equals("1")){
                    helper.setImageResource(R.id.image_zan,R.mipmap.dianzan_true);
//
                }else{
                    helper.setImageResource(R.id.image_zan,R.mipmap.dianzan_false);
                }
//                头像
                Glide.with(MyApp.getContext()).load(list.getUinfo().getAvatar()).into((ImageView) helper.getView(R.id.image_touxiang));
//                内容图片
                Glide.with(MyApp.getContext()).load(list.getImgArr().get(0).replace(" ","")).into((ImageView)helper.getView(R.id.image_three_one));
                Glide.with(MyApp.getContext()).load(list.getImgArr().get(1).replace(" ","")).into((ImageView)helper.getView(R.id.image_three_two));
                Glide.with(MyApp.getContext()).load(list.getImgArr().get(2).replace(" ","")).into((ImageView)helper.getView(R.id.image_three_three));
                break;
        }
    }
}
