package com.wanyangproject.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.wanyangproject.luntanfragment.LunTanLieBiaoFragment;
import com.wanyangproject.luntanfragment.QuanBuFragment;

import java.util.List;

/**
 * Created by 甜甜 on 2018/7/19.
 */

public class LunTanAdapter extends FragmentPagerAdapter {

//    private List<QuanBuFragment> FragmentList;
    private List<QuanBuFragment> FragmentList;
    private List<String> title;


    public void setFragments(List<QuanBuFragment> fragments) {
        FragmentList = fragments;
    }


    public LunTanAdapter(FragmentManager fm,List<QuanBuFragment> fragments,List<String> title) {
        super(fm);
        FragmentList = fragments;
        this.title=title;
    }

    @Override
    public Fragment getItem(int position) {
        Fragment fragment = FragmentList.get(position);
        return fragment;
    }

    @Override
    public int getCount() {
        return FragmentList.size();
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return title.get(position);
    }
}
