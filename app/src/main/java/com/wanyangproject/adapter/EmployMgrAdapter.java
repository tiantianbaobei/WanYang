package com.wanyangproject.adapter;

import android.content.Context;
import android.widget.ImageView;
import android.widget.TextView;


import com.wanyangproject.R;
import com.wanyangproject.model.EmployModel;
import com.wanyangproject.widget.recyclerview.RecyclerAdapter;
import com.wanyangproject.widget.recyclerview.RecyclerHolder;

import java.util.List;

public class EmployMgrAdapter extends RecyclerAdapter<EmployModel> {

    public EmployMgrAdapter(Context context, List<EmployModel> data) {
        super(context, data, R.layout.item_employ_mgr);
    }

    @Override
    public void convert(RecyclerHolder holder, EmployModel model) {
        ImageView employHead = holder.findViewById(R.id.employ_head);
        TextView employName = holder.findViewById(R.id.employ_name);
        TextView employSex = holder.findViewById(R.id.employ_sex);
        employName.setText(model.getName());
        employSex.setText(model.getSix());

        if ("男".equals(model.getSix())) {
            employHead.setImageResource(R.mipmap.touxiang2);
        } else {
            employHead.setImageResource(R.mipmap.touxiang3);
        }
    }
}