package com.wanyangproject.adapter;

import android.content.Context;
import android.widget.TextView;


import com.wanyangproject.R;
import com.wanyangproject.model.HouseModel;
import com.wanyangproject.widget.recyclerview.RecyclerAdapter;
import com.wanyangproject.widget.recyclerview.RecyclerHolder;

import java.util.List;

public class HouseAdapter extends RecyclerAdapter<HouseModel> {

    public HouseAdapter(Context context, List<HouseModel> data) {
        super(context, data, R.layout.item_house);
    }

    @Override
    public void convert(RecyclerHolder holder, HouseModel model) {
        TextView houseTitle = holder.findViewById(R.id.house_title);
        TextView houseAddress = holder.findViewById(R.id.house_address);
        TextView houseSite = holder.findViewById(R.id.house_site);
        houseTitle.setText(model.getName());
    }
}