package com.wanyangproject.adapter;

import android.content.Context;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;


import com.wanyangproject.R;
import com.wanyangproject.model.TestModel;
import com.wanyangproject.myfragment.OrderAllFragment;
import com.wanyangproject.net.util.Constant;
import com.wanyangproject.ui.OrderDetailActivity;
import com.wanyangproject.widget.recyclerview.RecyclerAdapter;
import com.wanyangproject.widget.recyclerview.RecyclerHolder;
import com.wanyangproject.widget.recyclerview.RecyclerListener;

import java.util.List;

public class OrderAdapter extends RecyclerAdapter<TestModel.ResultsBean> implements RecyclerListener.OnClickListener<RecyclerHolder> {

    private int order;
    private TextView orderState;
    private LinearLayout orderBtnLl;
    private Button orderPay;

    public OrderAdapter(Context context, List<TestModel.ResultsBean> data, int order) {
        super(context, data, R.layout.item_order);
        this.order = order;
    }

    @Override
    public void convert(RecyclerHolder holder, TestModel.ResultsBean model) {
        orderState = holder.findViewById(R.id.order_state);
        orderBtnLl = holder.findViewById(R.id.order_btn_ll);
        orderPay = holder.findViewById(R.id.order_pay);

        switch (order) {
//            全部
            case Constant.ORDER_ALL:
                setView(holder.position);
                break;
//            待支付
            case Constant.ORDER_PAY:
                setView(0);
                break;
//            待收货
            case Constant.ORDER_RECEIVE:
                setView(1);
                break;
//            待评价
            case Constant.ORDER_APPRAISE:
                setView(2);
                break;
//            已完成
            case Constant.ORDER_FINISH:
                setView(3);
                break;
        }
        holder.setOnClickListener(this, R.id.order_ll);
    }

    private void setView(int position) {
        if (position % 4 == 0) {
            orderState.setText("待支付");
            orderBtnLl.setVisibility(View.VISIBLE);
            orderPay.setText("去支付");
        } else if (position % 4 == 1) {
            orderState.setText("待收货");
            orderBtnLl.setVisibility(View.VISIBLE);
            orderPay.setText("去收货");
        } else if (position % 4 == 2) {
            orderState.setText("待评价");
            orderBtnLl.setVisibility(View.VISIBLE);
            orderPay.setText("去评价");
        } else if (position % 4 == 3) {
            orderState.setText("已完成");
            orderBtnLl.setVisibility(View.GONE);
        }
    }

    @Override
    public void onClick(View v, int position, RecyclerHolder holder) {
        TextView orderState = v.findViewById(R.id.order_state);
        String state = orderState.getText().toString();
        if (state.equals("待支付")) {
            state = "去支付";
        } else if (state.equals("待收货")) {
            state = "去收货";
        } else if (state.equals("待评价")) {
            state = "去评价";
        } else {
            state = null;
        }
        OrderDetailActivity.start(mContext, state);
    }
}