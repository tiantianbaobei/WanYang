package com.wanyangproject.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.wanyangproject.R;
import com.wanyangproject.entity.MyBangDingLieBiaoEntity;
import com.wanyangproject.my.MyBangDingActivity;

import java.util.List;

/**
 * Created by 甜甜 on 2018/8/30.
 */

public class MyBangDingAdapter extends RecyclerView.Adapter<MyBangDingAdapter.ViewHolder>{

    private Context context;
    private List<MyBangDingLieBiaoEntity.ResponseBean.ListBean> response;

    public MyBangDingAdapter(Context context, List<MyBangDingLieBiaoEntity.ResponseBean.ListBean> response) {
        this.context = context;
        this.response = response;
    }

    @Override
    public MyBangDingAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_bangding_yuanqu, parent, false);
        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(MyBangDingAdapter.ViewHolder holder, int position) {
        if(response.get(position).getParkName() == null){

        }else{
            holder.tv_yuanqu.setText(response.get(position).getParkName());
        }
    }

    @Override
    public int getItemCount() {
        return response.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView tv_yuanqu;
        private View view;
        public ViewHolder(View itemView) {
            super(itemView);
            view = itemView;
            tv_yuanqu = itemView.findViewById(R.id.tv_yuanqu);
        }
    }
}
