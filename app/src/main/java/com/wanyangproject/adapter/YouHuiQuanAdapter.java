package com.wanyangproject.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.wanyangproject.R;
import com.wanyangproject.activity.YouHuiQuanFuWuActivity;
import com.wanyangproject.entity.YouHuiQuanLieBiaoEntity;

import java.util.List;

/**
 * Created by 甜甜 on 2018/9/2.
 */

public class YouHuiQuanAdapter extends RecyclerView.Adapter<YouHuiQuanAdapter.ViewHolder>{

    private Context context;
    private List<YouHuiQuanLieBiaoEntity.ResponseBean.ShopPreferencesBean> shopPreferences;


    public YouHuiQuanAdapter(Context context, List<YouHuiQuanLieBiaoEntity.ResponseBean.ShopPreferencesBean> shopPreferences) {
        this.context = context;
        this.shopPreferences = shopPreferences;
    }




//    点击列表进入详情
    private XiangQingClick xiangQingClick;

    public void setXiangQingClick(XiangQingClick xiangQingClick) {
        this.xiangQingClick = xiangQingClick;
    }

    public interface XiangQingClick{
        void xiangqingClick(int position);
}




    @Override
    public YouHuiQuanAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_qiyefuwu_youhuihuodong, parent, false);
        final ViewHolder viewHolder = new ViewHolder(view);
        viewHolder.view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(xiangQingClick != null){
                    xiangQingClick.xiangqingClick(viewHolder.getAdapterPosition());
                }
            }
        });
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(YouHuiQuanAdapter.ViewHolder holder, int position) {
        if(shopPreferences.get(position).getTitle() == null){

        }else{//  标题
            holder.tv_title.setText(shopPreferences.get(position).getTitle());
        }

        if(shopPreferences.get(position).getIntroduction() == null){

        }else{//   内容
            holder.tv_neirong.setText(shopPreferences.get(position).getIntroduction());
        }
    }

    @Override
    public int getItemCount() {
        return shopPreferences.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView tv_title,tv_neirong;
        private View view;
        public ViewHolder(View itemView) {
            super(itemView);
            view = itemView;
            tv_title = itemView.findViewById(R.id.tv_title);
            tv_neirong = itemView.findViewById(R.id.tv_neirong);
        }
    }
}
