package com.wanyangproject.adapter;

import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.chad.library.adapter.base.BaseMultiItemQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.wanyangproject.R;
import com.wanyangproject.base.MyApp;
import com.wanyangproject.entity.LunTanLieBiaoEntity;

import java.util.List;

/**
 * Created by 甜甜 on 2018/10/23.
 */

public class LunTanLieBiaoXianShiXiuGaiAdapter extends BaseMultiItemQuickAdapter<LunTanLieBiaoEntity.ResponseBean.ListBean,BaseViewHolder> {
    /**
     * Same as QuickAdapter#QuickAdapter(Context,int) but with
     * some initialization data.
     *
     * @param data A new list is created out of this one to avoid mutable list
     */
    public LunTanLieBiaoXianShiXiuGaiAdapter(List<LunTanLieBiaoEntity.ResponseBean.ListBean> data) {
        super(data);

        addItemType(LunTanLieBiaoEntity.ResponseBean.ListBean.LING, R.layout.luntan_image_ling_item);
        addItemType(LunTanLieBiaoEntity.ResponseBean.ListBean.ONE,R.layout.luntan_image_one_item);
        addItemType(LunTanLieBiaoEntity.ResponseBean.ListBean.TWO, R.layout.luntan_image_two_item);
        addItemType(LunTanLieBiaoEntity.ResponseBean.ListBean.THREE,R.layout.luntan_image_three_item);
        addItemType(LunTanLieBiaoEntity.ResponseBean.ListBean.SEX,R.layout.luntan_image_sex_item);
        addItemType(LunTanLieBiaoEntity.ResponseBean.ListBean.NINE,R.layout.luntan_image_nine_item);
    }
    @Override
    protected void convert(final BaseViewHolder helper, LunTanLieBiaoEntity.ResponseBean.ListBean list) {
        switch (helper.getItemViewType()){
            case LunTanLieBiaoEntity.ResponseBean.ListBean.LING:
                helper.setText(R.id.tv_name,list.getUinfo().getNickname())
                        .setText(R.id.tv_time,list.getAdd_time())
                        .setText(R.id.tv_neirong,list.getTitle())
                        .setText(R.id.tv_zan_number,list.getLike())
                        .setText(R.id.tv_pinglun_number,list.getReply())
                        .addOnClickListener(R.id.image_zan);
                if(list.getIsLike().equals("1")){
                    helper.setImageResource(R.id.image_zan,R.mipmap.dianzan_true);
                }else{
                    helper.setImageResource(R.id.image_zan,R.mipmap.dianzan_false);
                }
//                头像
                Glide.with(MyApp.getContext()).load(list.getUinfo().getAvatar()).into((ImageView) helper.getView(R.id.image_touxiang));



                break;
            case LunTanLieBiaoEntity.ResponseBean.ListBean.ONE:
                helper.setText(R.id.tv_name,list.getUinfo().getNickname())
                        .setText(R.id.tv_time,list.getAdd_time())
                        .setText(R.id.tv_neirong,list.getTitle())
                        .setText(R.id.tv_zan_number,list.getLike())
                        .setText(R.id.tv_pinglun_number,list.getReply())
                        .addOnClickListener(R.id.image_zan);
                if(list.getIsLike().equals("1")){
                    helper.setImageResource(R.id.image_zan,R.mipmap.dianzan_true);
                }else{
                    helper.setImageResource(R.id.image_zan,R.mipmap.dianzan_false);
                }

//                头像
                Glide.with(MyApp.getContext()).load(list.getUinfo().getAvatar()).into((ImageView) helper.getView(R.id.image_touxiang));
//                内容图片
                Glide.with(MyApp.getContext()).load(list.getImgArr().get(0).replace(" ","")).into((ImageView)helper.getView(R.id.image));
                break;
            case LunTanLieBiaoEntity.ResponseBean.ListBean.TWO:
                helper.setText(R.id.tv_name,list.getUinfo().getNickname())
                        .setText(R.id.tv_time,list.getAdd_time())
                        .setText(R.id.tv_neirong,list.getTitle())
                        .setText(R.id.tv_zan_number,list.getLike())
                        .setText(R.id.tv_pinglun_number,list.getReply())
                        .addOnClickListener(R.id.image_zan);
                if(list.getIsLike().equals("1")){
                    helper.setImageResource(R.id.image_zan,R.mipmap.dianzan_true);
//
                }else{
                    helper.setImageResource(R.id.image_zan,R.mipmap.dianzan_false);
                }
//                        头像
                Glide.with(MyApp.getContext()).load(list.getUinfo().getAvatar()).into((ImageView) helper.getView(R.id.image_touxiang));
//               内容图片
                Glide.with(MyApp.getContext()).load(list.getImgArr().get(0).replace(" ","")).into((ImageView)helper.getView(R.id.image_one));
                Glide.with(MyApp.getContext()).load(list.getImgArr().get(1).replace(" ","")).into((ImageView)helper.getView(R.id.image_two));
                break;
            case LunTanLieBiaoEntity.ResponseBean.ListBean.THREE:
                helper.setText(R.id.tv_name,list.getUinfo().getNickname())
                        .setText(R.id.tv_time,list.getAdd_time())
                        .setText(R.id.tv_neirong,list.getTitle())
                        .setText(R.id.tv_zan_number,list.getLike())
                        .setText(R.id.tv_pinglun_number,list.getReply())
                        .addOnClickListener(R.id.image_zan);
                if(list.getIsLike().equals("1")){
                    helper.setImageResource(R.id.image_zan,R.mipmap.dianzan_true);
//
                }else{
                    helper.setImageResource(R.id.image_zan,R.mipmap.dianzan_false);
                }
//                头像
                Glide.with(MyApp.getContext()).load(list.getUinfo().getAvatar()).into((ImageView) helper.getView(R.id.image_touxiang));
//                内容图片
                Glide.with(MyApp.getContext()).load(list.getImgArr().get(0).replace(" ","")).into((ImageView)helper.getView(R.id.image_three_one));
                Glide.with(MyApp.getContext()).load(list.getImgArr().get(1).replace(" ","")).into((ImageView)helper.getView(R.id.image_three_two));
                Glide.with(MyApp.getContext()).load(list.getImgArr().get(2).replace(" ","")).into((ImageView)helper.getView(R.id.image_three_three));
                break;
        }
    }
}
