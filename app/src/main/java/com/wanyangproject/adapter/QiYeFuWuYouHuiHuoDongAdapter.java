package com.wanyangproject.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.wanyangproject.R;
import com.wanyangproject.entity.QiYeFuWuEntity;
import com.wanyangproject.entity.QiYeFuWuYouHuiHuoDongEntity;
import com.wanyangproject.fuwuactivity.QiYeFuWuActivity;
import com.zhy.http.okhttp.callback.StringCallback;

import java.util.List;

/**
 * Created by 甜甜 on 2018/8/14.
 */

public class QiYeFuWuYouHuiHuoDongAdapter extends RecyclerView.Adapter<QiYeFuWuYouHuiHuoDongAdapter.ViewHolder>{

    private Context context;
    private List<QiYeFuWuEntity.ResponseBean.DataBean> shopPreferences;

    public QiYeFuWuYouHuiHuoDongAdapter(Context context, List<QiYeFuWuEntity.ResponseBean.DataBean> shopPreferences) {
        this.context = context;
        this.shopPreferences = shopPreferences;
    }



//    点击进入详情
    private XiangQingClick xiangQingClick;

    public void setXiangQingClick(XiangQingClick xiangQingClick) {
        this.xiangQingClick = xiangQingClick;
    }

    public interface XiangQingClick{
        void xiangqingClick(int position);
}

    @Override
    public QiYeFuWuYouHuiHuoDongAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_qiyefuwu_youhuihuodong, parent, false);
        final ViewHolder viewHolder = new ViewHolder(view);
        viewHolder.view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(xiangQingClick!= null){
                    xiangQingClick.xiangqingClick(viewHolder.getAdapterPosition());
                }
            }
        });
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(QiYeFuWuYouHuiHuoDongAdapter.ViewHolder holder, int position) {
        if(shopPreferences.get(position).getTitle() == null){

        }else{//  标题
            holder.tv_title.setText(shopPreferences.get(position).getTitle());
        }

        if(shopPreferences.get(position).getDesc() == null || shopPreferences.get(position).getDesc().equals("")){
            holder.tv_neirong.setVisibility(View.GONE);
        }else{//   内容
            holder.tv_neirong.setVisibility(View.VISIBLE);
            holder.tv_neirong.setText(shopPreferences.get(position).getDesc());
        }
    }

    @Override
    public int getItemCount() {
        return shopPreferences.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView tv_title,tv_neirong;
        private View view;
        public ViewHolder(View itemView) {
            super(itemView);
            view = itemView;
            tv_title = itemView.findViewById(R.id.tv_title);
            tv_neirong = itemView.findViewById(R.id.tv_neirong);

        }
    }
}
