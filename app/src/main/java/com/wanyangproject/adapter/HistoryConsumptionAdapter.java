package com.wanyangproject.adapter;

import android.content.Context;
import android.widget.TextView;

import com.wanyangproject.R;
import com.wanyangproject.entity.HistoryConsumptionEntity;
import com.wanyangproject.widget.recyclerview.RecyclerAdapter;
import com.wanyangproject.widget.recyclerview.RecyclerHolder;

import java.util.List;

/**
 * 历史用量adapter
 */
public class HistoryConsumptionAdapter  extends RecyclerAdapter<HistoryConsumptionEntity.Response.Data.Consumption> {

    public HistoryConsumptionAdapter(Context context, List<HistoryConsumptionEntity.Response.Data.Consumption>  data) {
        super(context, data, R.layout.item_shuidian);
    }

    @Override
    public void convert(RecyclerHolder holder,HistoryConsumptionEntity.Response.Data.Consumption model) {
        TextView shuidianTime = holder.findViewById(R.id.shuidian_time);
        TextView shuidianNumber = holder.findViewById(R.id.shuidian_number);
        shuidianTime.setText(""+model.getMonth());
        switch (mType){
            case "1"://水
                shuidianNumber.setText(""+model.getCost()+"吨");
                break;
            case "2"://电
                shuidianNumber.setText(""+model.getCost()+"度");
                break;
        }

    }
}
