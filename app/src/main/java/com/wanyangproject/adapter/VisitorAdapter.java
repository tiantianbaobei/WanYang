package com.wanyangproject.adapter;

import android.content.Context;
import android.view.View;
import android.widget.ImageView;


import com.wanyangproject.R;
import com.wanyangproject.model.TestModel;
import com.wanyangproject.widget.recyclerview.RecyclerAdapter;
import com.wanyangproject.widget.recyclerview.RecyclerHolder;
import com.wanyangproject.widget.recyclerview.RecyclerListener;

import java.util.List;

public class VisitorAdapter extends RecyclerAdapter<TestModel.ResultsBean> implements RecyclerListener.OnClickListener<RecyclerHolder> {

    public VisitorAdapter(Context context, List<TestModel.ResultsBean> data) {
        super(context, data, R.layout.item_visitor);
    }

    @Override
    public void convert(RecyclerHolder holder, TestModel.ResultsBean model) {
        ImageView visitorState = holder.findViewById(R.id.visitor_state);
        if (holder.position % 2 == 0) {
            visitorState.setImageResource(R.mipmap.yidaofang);
        } else if (holder.position % 2 == 1) {
            visitorState.setImageResource(R.mipmap.weidaofang);
        }

        holder.setOnClickListener(this, R.id.visitor_root, R.id.visitor_update);
    }

    @Override
    public void onClick(View v, int position, RecyclerHolder holder) {
        switch (v.getId()) {
            case R.id.visitor_root:
                com.wanyangproject.ui.VisitorActivity.start(mContext);
                break;
            case R.id.visitor_update:
                com.wanyangproject.ui.VisitorActivity.start(mContext);
                break;
        }
    }
}