package com.wanyangproject.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.wanyangproject.R;
import com.wanyangproject.shouye.QieHuanYuanQuActivity;

/**
 * Created by 甜甜 on 2018/9/4.
 */

public class MoRenYuanQuAdapter extends RecyclerView.Adapter<MoRenYuanQuAdapter.ViewHolder>{


    private Context context;
    private String yuanquname;

    public MoRenYuanQuAdapter(Context context, String yuanquname) {
        this.context = context;
        this.yuanquname = yuanquname;
    }

    @Override
    public MoRenYuanQuAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_moren, parent, false);
        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(MoRenYuanQuAdapter.ViewHolder holder, int position) {
        if(yuanquname == null){

        }else{
            holder.tv_name.setText(yuanquname);
        }

    }

    @Override
    public int getItemCount() {
        return 1;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView tv_name;
        private View view;
        public ViewHolder(View itemView) {
            super(itemView);
            view = itemView;
            tv_name = itemView.findViewById(R.id.tv_name);
        }
    }
}
