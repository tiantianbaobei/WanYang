package com.wanyangproject.adapter;

import android.content.Context;
import android.widget.TextView;


import com.wanyangproject.R;
import com.wanyangproject.model.ForumModel;
import com.wanyangproject.widget.recyclerview.RecyclerAdapter;
import com.wanyangproject.widget.recyclerview.RecyclerHolder;

import java.util.List;

public class ReplyAdapter extends RecyclerAdapter<ForumModel.Data> {

    public ReplyAdapter(Context context, List<ForumModel.Data> data) {
        super(context, data, R.layout.item_reply);
    }

    @Override
    public void convert(RecyclerHolder holder, ForumModel.Data model) {

        TextView replyNick = holder.findViewById(R.id.reply_nick);
        TextView replyContent = holder.findViewById(R.id.reply_content);

        replyNick.setText(model.getNick() + ": ");
        replyContent.setText(model.getContent());
    }

}