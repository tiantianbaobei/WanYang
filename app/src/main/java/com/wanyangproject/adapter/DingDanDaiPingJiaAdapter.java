package com.wanyangproject.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.wanyangproject.R;
import com.wanyangproject.entity.DaiPingJiaEntity;
import com.wanyangproject.entity.DaiZhiFuEntity;
import com.wanyangproject.my.DaiPingJiaXiangQingActivity;
import com.wanyangproject.utils.ContractUtils;

import java.util.List;

/**
 * Created by 甜甜 on 2018/8/21.
 */

public class DingDanDaiPingJiaAdapter extends RecyclerView.Adapter<DingDanDaiPingJiaAdapter.ViewHolder>{

    private Context context;
    private List<DaiPingJiaEntity.ResponseBean.GoodsBean> response;

    public DingDanDaiPingJiaAdapter(Context context, List<DaiPingJiaEntity.ResponseBean.GoodsBean> response) {
        this.context = context;
        this.response = response;
    }



//    点击待评价中间的列表
    private DingDanDaiPingJiaClick dingDanDaiPingJiaClick;


    public void setDingDanDaiPingJiaClick(DingDanDaiPingJiaClick dingDanDaiPingJiaClick) {
        this.dingDanDaiPingJiaClick = dingDanDaiPingJiaClick;
    }

    public interface DingDanDaiPingJiaClick{
        void daingdanDaiPingJiaClick(String id);
}



    @Override
    public DingDanDaiPingJiaAdapter.ViewHolder onCreateViewHolder(final ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_dingdan,parent,false);
        final ViewHolder viewHolder = new ViewHolder(view);


        return viewHolder;
    }

    @Override
    public void onBindViewHolder(DingDanDaiPingJiaAdapter.ViewHolder holder, final int position) {
//        //        点击中间的待评价列表进入详情
        holder.relative.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(dingDanDaiPingJiaClick != null){
                    dingDanDaiPingJiaClick.daingdanDaiPingJiaClick(response.get(position).getId());
                }
            }
        });











        if(ContractUtils.PHOTO_URL+response.get(position).getMaster() == null){

        }else{ //商品的图片
            Glide.with(context).load(ContractUtils.PHOTO_URL+response.get(position).getMaster()).into(holder.image_tupian);
        }


        if(response.get(position).getGoods_name() == null){

        }else{  // 商品的名称
            holder.tv_name.setText(response.get(position).getGoods_name());
        }


        if(response.get(position).getPic() == null){

        }else{  //商品的单价
            holder.tv_money.setText("¥"+response.get(position).getPic());
        }


        if(response.get(position).getNum() == null){

        }else{
            holder.tv_number.setText("x"+response.get(position).getNum());
        }
    }

    @Override
    public int getItemCount() {
        return response.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView tv_name,tv_money,tv_number;
        private ImageView image_tupian;
        private RelativeLayout relative;
        private View view;
        public ViewHolder(View itemView) {
            super(itemView);
            view = itemView;
            tv_name = itemView.findViewById(R.id.tv_name);
            tv_money = itemView.findViewById(R.id.tv_money);
            tv_number = itemView.findViewById(R.id.tv_number);
            image_tupian = itemView.findViewById(R.id.image_tupian);
            relative = itemView.findViewById(R.id.relative);
        }
    }
}
