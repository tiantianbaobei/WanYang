package com.wanyangproject.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.scwang.smartrefresh.header.material.CircleImageView;
import com.wanyangproject.R;
import com.wanyangproject.entity.QiFuZhongXinEntity;
import com.wanyangproject.entity.YuanQuZhaoShangEntity;
import com.wanyangproject.shouye.ZhiNengZhiZaoActivity;
import com.wanyangproject.utils.ContractUtils;

import java.util.List;

/**
 * Created by 甜甜 on 2018/8/13.
 */

public class QiFuZhongXinAdapter extends RecyclerView.Adapter<QiFuZhongXinAdapter.ViewHolder>{

    private Context context;
    private List<QiFuZhongXinEntity.ResponseBean.UpTjArrBean> upTjArr;

    public QiFuZhongXinAdapter(Context context, List<QiFuZhongXinEntity.ResponseBean.UpTjArrBean> upTjArr) {
        this.context = context;
        this.upTjArr = upTjArr;
    }


    //    点击进入详情
    private XingQingClick xingQingClick;



    public void setXingQingClick(XingQingClick xingQingClick) {
        this.xingQingClick = xingQingClick;
    }

    public interface XingQingClick{
        void xiangqingClick(int position);
    }

    @Override
    public QiFuZhongXinAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_heng_qifuzhongxin, parent, false);
        final ViewHolder viewHolder = new ViewHolder(view);
        viewHolder.view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(xingQingClick != null){
                    xingQingClick.xiangqingClick(viewHolder.getAdapterPosition());
                }
            }
        });
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(QiFuZhongXinAdapter.ViewHolder holder, int position) {
        if(upTjArr.get(position).getTitle() == null){

        }else{ // 标题
            holder.tv_name.setText(upTjArr.get(position).getTitle());
        }

        if(upTjArr.get(position).getImage() == null){

        }else{
//            Glide.with(context).load(ContractUtils.YUANQUZHAOSHANG_PHOTO_URL+upTjArr.get(position).getLogo()).into(holder.image);
            Glide.with(context).load(upTjArr.get(position).getImage()).into(holder.image);
        }
    }


    @Override
    public int getItemCount() {
        if(upTjArr.size()>4){
            return 4;
        }
        return upTjArr.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        private ImageView image;
        private TextView tv_name;
        private View view;
        public ViewHolder(View itemView) {
            super(itemView);
            view = itemView;
            image = itemView.findViewById(R.id.image);
            tv_name = itemView.findViewById(R.id.tv_name);
        }
    }
}
