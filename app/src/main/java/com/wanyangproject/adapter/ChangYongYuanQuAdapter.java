package com.wanyangproject.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.wanyangproject.R;
import com.wanyangproject.entity.SheZhiChangYongYuanQuEntity;
import com.wanyangproject.shouye.QieHuanYuanQuActivity;
import com.wanyangproject.utils.SharedPreferencesXuanZeYuanQu;

import java.util.List;

/**
 * Created by 甜甜 on 2018/9/4.
 */

public class ChangYongYuanQuAdapter extends RecyclerView.Adapter<ChangYongYuanQuAdapter.ViewHolder>{

    private Context context;
    private List<SheZhiChangYongYuanQuEntity.ResponseBean> response;

    public ChangYongYuanQuAdapter(Context context, List<SheZhiChangYongYuanQuEntity.ResponseBean> response) {
        this.context = context;
        this.response = response;
    }





    private ChangYongYuanQuClick changYongYuanQuClick;

    public ChangYongYuanQuClick getChangYongYuanQuClick() {
        return changYongYuanQuClick;
    }

    public void setChangYongYuanQuClick(ChangYongYuanQuClick changYongYuanQuClick) {
        this.changYongYuanQuClick = changYongYuanQuClick;
    }

    public interface ChangYongYuanQuClick{
        void changyongyuanquClick(int position, String name,String id);

    }







    @Override
    public ChangYongYuanQuAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_moren, parent, false);
        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final ChangYongYuanQuAdapter.ViewHolder holder, final int position) {
        if(response.get(position).getParkName() == null){

        }else{
            holder.tv_name.setText(response.get(position).getParkName());
        }




        holder.tv_name.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(changYongYuanQuClick != null){
                    changYongYuanQuClick.changyongyuanquClick(holder.getAdapterPosition(),response.get(position).getParkName(),response.get(position).getParkId());
                    SharedPreferencesXuanZeYuanQu.saveYuanQu(context,response.get(position).getParkId(),response.get(position).getParkName());
                }
            }
        });
    }





    @Override
    public int getItemCount() {
        return response.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView tv_name;
        private View view;
        public ViewHolder(View itemView) {
            super(itemView);
            view = itemView;
            tv_name = itemView.findViewById(R.id.tv_name);
        }
    }
}
