package com.wanyangproject.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.PagerAdapter;
import android.view.View;

import com.wanyangproject.luntanfragment.LunTanLieBiaoFragment;
import com.wanyangproject.luntanfragment.QuanBuFragment;

import java.util.List;

/**
 * Created by 甜甜 on 2018/10/23.
 */

public class LunTanLieBiaoXiuGaiAdapter extends FragmentPagerAdapter {

    private List<LunTanLieBiaoFragment> FragmentList;
    private List<String> title;

    public void setFragments(List<LunTanLieBiaoFragment> fragments) {
        FragmentList = fragments;
    }





    public LunTanLieBiaoXiuGaiAdapter(FragmentManager fm,List<LunTanLieBiaoFragment> fragments,List<String> title) {
        super(fm);
        FragmentList = fragments;
        this.title=title;
    }




    @Override
    public CharSequence getPageTitle(int position) {
        return title.get(position);
    }

    @Override
    public int getCount() {
        return FragmentList.size();
    }


    @Override
    public Fragment getItem(int position) {
        return null;
    }

}
