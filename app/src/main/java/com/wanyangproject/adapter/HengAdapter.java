package com.wanyangproject.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.wanyangproject.R;
import com.wanyangproject.entity.YuanQuShengHuoTuiJianShangJiaEntity;
import com.wanyangproject.shouye.YuanQuShengHuoActivity;
import com.wanyangproject.utils.ContractUtils;

import java.util.List;

/**
 * Created by 甜甜 on 2018/8/8.
 */

public class HengAdapter extends RecyclerView.Adapter<HengAdapter.ViewHolder>{

    private Context context;
    private List<YuanQuShengHuoTuiJianShangJiaEntity.ResponseBean.MerchantClassBean> merchantClass;

    public HengAdapter(Context context, List<YuanQuShengHuoTuiJianShangJiaEntity.ResponseBean.MerchantClassBean> merchantClass) {
        this.context = context;
        this.merchantClass = merchantClass;
    }


//    点击跳转外卖页面
    private XiangQingClick xiangQingClick;

    public void setXiangQingClick(XiangQingClick xiangQingClick) {
        this.xiangQingClick = xiangQingClick;
    }

    public interface XiangQingClick{
        void xiangqingClick(int position);
}

    @Override
    public HengAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.yuanqu_shanghuo_item_heng, parent, false);
        final ViewHolder viewHolder = new ViewHolder(view);
        viewHolder.view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(xiangQingClick != null){
                    xiangQingClick.xiangqingClick(viewHolder.getAdapterPosition());
                }
            }
        });
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(HengAdapter.ViewHolder holder, int position) {
        if(merchantClass.get(position).getName() == null || ContractUtils.PHOTO_URL + merchantClass.get(position).getPhoto() == null){

        }else{
            holder.tv_name.setText(merchantClass.get(position).getName());
            Glide.with(context).load(ContractUtils.PHOTO_URL + merchantClass.get(position).getPhoto()).into(holder.image);
        }
    }

    @Override
    public int getItemCount() {
        return merchantClass.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private ImageView image;
        private TextView tv_name;
        private View view;
        public ViewHolder(View itemView) {
            super(itemView);
            view = itemView;
            image = itemView.findViewById(R.id.image);
            tv_name = itemView.findViewById(R.id.tv_name);
        }
    }
}
