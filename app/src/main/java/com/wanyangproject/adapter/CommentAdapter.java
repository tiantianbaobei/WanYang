package com.wanyangproject.adapter;

import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.wanyangproject.R;
import com.wanyangproject.model.ForumModel;
import com.wanyangproject.widget.recyclerview.RecyclerAdapter;
import com.wanyangproject.widget.recyclerview.RecyclerHolder;

import java.util.List;

public class CommentAdapter extends RecyclerAdapter<ForumModel> {

    public CommentAdapter(Context context, List<ForumModel> data) {
        super(context, data, R.layout.item_comment);
    }

    @Override
    public void convert(RecyclerHolder holder, ForumModel model) {
        ImageView commentHead = holder.findViewById(R.id.comment_head);
        TextView commentNick = holder.findViewById(R.id.comment_nick);
        TextView commentTime = holder.findViewById(R.id.comment_time);
        TextView commentContent = holder.findViewById(R.id.comment_content);
        RecyclerView commentReply = holder.findViewById(R.id.comment_reply);

        Glide.with(mContext).load(model.getResId()).into(commentHead);
        commentNick.setText(model.getNick());
        commentTime.setText(model.getTime());
        commentContent.setText(model.getContent());

        List<ForumModel.Data> data = model.getData();
        if (data != null && data.size() > 0) {
            commentReply.setVisibility(View.VISIBLE);
            commentReply.setLayoutManager(new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false));
            commentReply.setAdapter(new ReplyAdapter(mContext, data));
        } else {
            commentReply.setVisibility(View.GONE);
        }
    }
}