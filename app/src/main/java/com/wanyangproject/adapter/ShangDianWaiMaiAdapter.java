package com.wanyangproject.adapter;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.wanyangproject.R;
import com.wanyangproject.entity.PaiXuEntity;
import com.wanyangproject.shouye.DianPuActivity;
import com.wanyangproject.utils.ContractUtils;

import java.util.List;

/**
 * Created by 甜甜 on 2018/7/25.
 */

public class ShangDianWaiMaiAdapter extends RecyclerView.Adapter<ShangDianWaiMaiAdapter.ViewHolder>{

    private Context context;
    private List<PaiXuEntity.ResponseBean> response;

    public ShangDianWaiMaiAdapter(Context context, List<PaiXuEntity.ResponseBean> response) {
        this.context = context;
        this.response = response;
    }

    private ShangDianWaiMaiClick shangDianWaiMaiClick;

    public void setShangDianWaiMaiClick(ShangDianWaiMaiClick shangDianWaiMaiClick) {
        this.shangDianWaiMaiClick = shangDianWaiMaiClick;
    }

    public interface ShangDianWaiMaiClick{
        void shangdianwaimaiclick(int position);
    }

    @Override
    public ShangDianWaiMaiAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.waimai_item, parent, false);
        final ViewHolder viewHolder = new ViewHolder(view);
        viewHolder.view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(shangDianWaiMaiClick != null){
                    shangDianWaiMaiClick.shangdianwaimaiclick(viewHolder.getAdapterPosition());
                }
            }
        });

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ShangDianWaiMaiAdapter.ViewHolder holder, int position) {
//        holder.tv_bidian.setBackgroundColor(Color.argb(255, 0, 255, 0)); //背景透明
//        holder.tv_bidian.getBackground().setAlpha(100);//0~255透明度值;

        if(response.get(position).getNickname() == null){

        }else{
            holder.tv_title.setText(response.get(position).getNickname());
        }


        if(response.get(position).getSales() == null){

        }else{
            if(response.get(position).getSales().equals("")){
                response.get(position).setSales("0");
            }
            holder.tv_yuexiao.setText("月销 "+response.get(position).getSales());
        }


        if(response.get(position).getStarting() == null && response.get(position).getCost() == null){

        }else{
            if(response.get(position).getCost().equals("")){
                response.get(position).setCost("0");
            }
            holder.tv_peisong.setText("起送价¥"+response.get(position).getStarting()+" | 配送费¥"+response.get(position).getCost());
        }


        if(ContractUtils.PHOTO_URL + response.get(position).getPhoto() != null){
            if(response.get(position).getPhoto().equals("park.hostop.net/")){
                Glide.with(context).load(R.drawable.shangdian).into(holder.image_shangdian);
            }else{
                Glide.with(context).load(ContractUtils.PHOTO_URL + response.get(position).getPhoto()).into(holder.image_shangdian);
            }
        }




//        1 营业    2 休息
        if(response.get(position).getBusiness() != null){
            if(response.get(position).getBusiness().equals("1")){
                holder.tv_bidian.setVisibility(View.GONE);
            }else if(response.get(position).getBusiness().equals("2")){
                holder.tv_bidian.setVisibility(View.VISIBLE);
            }
        }

    }

    @Override
    public int getItemCount() {
        return response.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private ImageView image_shangdian;
        private TextView tv_title,tv_yuexiao,tv_peisong,tv_bidian;
        private Button btn_chaoshi;
        private View view;
        public ViewHolder(View itemView) {
            super(itemView);
            view = itemView;
            image_shangdian = itemView.findViewById(R.id.image_shangdian);
            tv_title = itemView.findViewById(R.id.tv_title);
            tv_yuexiao = itemView.findViewById(R.id.tv_yuexiao);
            tv_peisong = itemView.findViewById(R.id.tv_peisong);
            btn_chaoshi = itemView.findViewById(R.id.btn_chaoshi);
            tv_bidian = itemView.findViewById(R.id.tv_bidian);
        }
    }
}
