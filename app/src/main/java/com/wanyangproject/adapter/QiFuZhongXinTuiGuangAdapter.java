package com.wanyangproject.adapter;

import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.chad.library.adapter.base.BaseMultiItemQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.wanyangproject.R;
import com.wanyangproject.base.MyApp;
import com.wanyangproject.entity.ChanYeJinRongEntity;
import com.wanyangproject.entity.QiFuZhongXinEntity;
import com.wanyangproject.entity.YuanQuZhaoShangEntity;

import java.util.List;

import static android.R.id.list;

/**
 * Created by 甜甜 on 2018/8/24.
 */

public class QiFuZhongXinTuiGuangAdapter extends BaseMultiItemQuickAdapter<QiFuZhongXinEntity.ResponseBean.TjIndexBean,BaseViewHolder> {

    /**
     * Same as QuickAdapter#QuickAdapter(Context,int) but with
     * some initialization data.
     *
     * @param data A new list is created out of this one to avoid mutable list
     */
    public QiFuZhongXinTuiGuangAdapter(List<QiFuZhongXinEntity.ResponseBean.TjIndexBean> list) {
        super(list);
        addItemType(QiFuZhongXinEntity.ResponseBean.TjIndexBean.LING,R.layout.image_ling_item);
        addItemType(QiFuZhongXinEntity.ResponseBean.TjIndexBean.ONE,R.layout.image_one_item);
        addItemType(QiFuZhongXinEntity.ResponseBean.TjIndexBean.TWO,R.layout.image_two_item);
        addItemType(QiFuZhongXinEntity.ResponseBean.TjIndexBean.THREE,R.layout.image_three_item);
        addItemType(QiFuZhongXinEntity.ResponseBean.TjIndexBean.SEX,R.layout.image_sex_item);
        addItemType(QiFuZhongXinEntity.ResponseBean.TjIndexBean.NINE,R.layout.image_nine_item);
    }

    @Override
    protected void convert(BaseViewHolder helper, QiFuZhongXinEntity.ResponseBean.TjIndexBean list) {
        switch (helper.getItemViewType()){
//            没有图片的
            case ChanYeJinRongEntity.ResponseBean.LING:
                helper.setText(R.id.tv_title,list.getTitle())
                        .setText(R.id.tv_shijian,list.getAdd_time())
                        .setText(R.id.tv_neirong,list.getDesc())
                        .addOnClickListener(R.id.relative_two);
                break;
//            一张图片的
            case ChanYeJinRongEntity.ResponseBean.ONE:
                helper.setText(R.id.tv_title,list.getTitle())
                        .setText(R.id.tv_shijian,list.getAdd_time())
                        .setText(R.id.tv_neirong,list.getDesc())
                        .addOnClickListener(R.id.relative_two);
                Glide.with(MyApp.getContext()).load(list.getImgArr().get(0)).into((ImageView) helper.getView(R.id.image));
                break;
//            两张图片的
            case ChanYeJinRongEntity.ResponseBean.TWO:
                helper.setText(R.id.tv_title,list.getTitle())
                        .setText(R.id.tv_shijian,list.getAdd_time())
                        .setText(R.id.tv_neirong,list.getDesc())
                        .addOnClickListener(R.id.relative_two);
                Glide.with(MyApp.getContext()).load(list.getImgArr().get(0)).into((ImageView) helper.getView(R.id.image_one));
                Glide.with(MyApp.getContext()).load(list.getImgArr().get(1)).into((ImageView) helper.getView(R.id.image_two));
                break;
//            三张图片的
            case ChanYeJinRongEntity.ResponseBean.THREE:
                helper.setText(R.id.tv_title,list.getTitle())
                        .setText(R.id.tv_shijian,list.getAdd_time())
                        .setText(R.id.tv_neirong,list.getDesc())
                        .addOnClickListener(R.id.relative_two);
                Glide.with(MyApp.getContext()).load(list.getImgArr().get(0)).into((ImageView) helper.getView(R.id.image_one));
                Glide.with(MyApp.getContext()).load(list.getImgArr().get(1)).into((ImageView) helper.getView(R.id.image_two));
                Glide.with(MyApp.getContext()).load(list.getImgArr().get(2)).into((ImageView) helper.getView(R.id.image_three));
                break;
//            六张图片的
            case ChanYeJinRongEntity.ResponseBean.SEX:
                helper.setText(R.id.tv_title,list.getTitle())
                        .setText(R.id.tv_shijian,list.getAdd_time())
                        .setText(R.id.tv_neirong,list.getDesc())
                        .addOnClickListener(R.id.relative_two);
                Glide.with(MyApp.getContext()).load(list.getImgArr().get(0)).into((ImageView) helper.getView(R.id.image_one));
                Glide.with(MyApp.getContext()).load(list.getImgArr().get(1)).into((ImageView) helper.getView(R.id.image_two));
                Glide.with(MyApp.getContext()).load(list.getImgArr().get(2)).into((ImageView) helper.getView(R.id.image_three));
                Glide.with(MyApp.getContext()).load(list.getImgArr().get(3)).into((ImageView) helper.getView(R.id.image_four));
                Glide.with(MyApp.getContext()).load(list.getImgArr().get(4)).into((ImageView) helper.getView(R.id.image_five));
                Glide.with(MyApp.getContext()).load(list.getImgArr().get(5)).into((ImageView) helper.getView(R.id.image_sex));
                break;
//            九张图片的
            case ChanYeJinRongEntity.ResponseBean.NINE:
                helper.setText(R.id.tv_title,list.getTitle())
                        .setText(R.id.tv_shijian,list.getAdd_time())
                        .setText(R.id.tv_neirong,list.getDesc())
                        .addOnClickListener(R.id.relative_two);
                Glide.with(MyApp.getContext()).load(list.getImgArr().get(0)).into((ImageView) helper.getView(R.id.image_one));
                Glide.with(MyApp.getContext()).load(list.getImgArr().get(1)).into((ImageView) helper.getView(R.id.image_two));
                Glide.with(MyApp.getContext()).load(list.getImgArr().get(2)).into((ImageView) helper.getView(R.id.image_three));
                Glide.with(MyApp.getContext()).load(list.getImgArr().get(3)).into((ImageView) helper.getView(R.id.image_four));
                Glide.with(MyApp.getContext()).load(list.getImgArr().get(4)).into((ImageView) helper.getView(R.id.image_five));
                Glide.with(MyApp.getContext()).load(list.getImgArr().get(5)).into((ImageView) helper.getView(R.id.image_sex));
                Glide.with(MyApp.getContext()).load(list.getImgArr().get(6)).into((ImageView) helper.getView(R.id.image_seven));
                Glide.with(MyApp.getContext()).load(list.getImgArr().get(7)).into((ImageView) helper.getView(R.id.image_eight));
                Glide.with(MyApp.getContext()).load(list.getImgArr().get(8)).into((ImageView) helper.getView(R.id.image_nine));
                break;
        }
    }
}
