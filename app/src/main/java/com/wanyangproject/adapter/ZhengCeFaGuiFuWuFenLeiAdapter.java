package com.wanyangproject.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.wanyangproject.R;
import com.wanyangproject.entity.YuanQuZhaoShangEntity;
import com.wanyangproject.shouye.ZhengCeFaGuiActivity;
import com.wanyangproject.utils.ContractUtils;

import java.util.List;

/**
 * Created by 甜甜 on 2018/8/13.
 */

public class ZhengCeFaGuiFuWuFenLeiAdapter extends RecyclerView.Adapter<ZhengCeFaGuiFuWuFenLeiAdapter.ViewHolder>{

    private Context context;
    private List<YuanQuZhaoShangEntity.ResponseBean.CategoryBean> category;

    public ZhengCeFaGuiFuWuFenLeiAdapter(Context context, List<YuanQuZhaoShangEntity.ResponseBean.CategoryBean> category) {
        this.context = context;
        this.category = category;
    }


    //    点击进入详情
    private FenLeiXiangQingClick fenLeiXiangQingClick;

    public void setFenLeiXiangQingClick(FenLeiXiangQingClick fenLeiXiangQingClick) {
        this.fenLeiXiangQingClick = fenLeiXiangQingClick;
    }

    public interface FenLeiXiangQingClick{
        void fenleixiangqingClick(String id);
    }


    @Override
    public ZhengCeFaGuiFuWuFenLeiAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_zhengcefagui_fuwufenlei, parent, false);
        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ZhengCeFaGuiFuWuFenLeiAdapter.ViewHolder holder, final int position) {
        holder.relative_left.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(fenLeiXiangQingClick != null){
                    fenLeiXiangQingClick.fenleixiangqingClick(category.get(position*2).getId());
                }
            }
        });


        holder.relative_right.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(fenLeiXiangQingClick != null){
                    if(position*2+1 == category.size()){
                        return;
                    }
                    fenLeiXiangQingClick.fenleixiangqingClick(category.get(position*2+1).getId());
                }
            }
        });



        if(category.get(position*2).getTitle() == null){

        }else{
            holder.tv_fenlei_one.setText(category.get(position*2).getTitle());
        }


        if(category.get(position*2).getLogo() == null){

        }else{
//            Glide.with(context).load(ContractUtils.YUANQUZHAOSHANG_PHOTO_URL+category.get(position*2).getLogo()).into(holder.image_fenlei_one);
            Glide.with(context).load(category.get(position*2).getLogo()).into(holder.image_fenlei_one);
        }



        if(position*2+1 == category.size()){
            holder.relative_right.setVisibility(View.GONE);
            return;
        }

        if(category.get(position*2+1).getTitle() == null){

        }else{
            holder.tv_fenlei_two.setText(category.get(position*2+1).getTitle());
        }


        if(category.get(position*2+1).getLogo() == null){

        }else{
//            Glide.with(context).load(ContractUtils.YUANQUZHAOSHANG_PHOTO_URL+category.get(position*2+1).getLogo()).into(holder.image_fenlei_two);
            Glide.with(context).load(category.get(position*2+1).getLogo()).into(holder.image_fenlei_two);
        }
    }

    @Override
    public int getItemCount() {
        return category.size()/2+category.size()%2;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private ImageView image_fenlei_one,image_fenlei_two;
        private TextView tv_fenlei_one,tv_fenlei_two;//tv_xiangqing_one   tv_xiangqing_two
        private RelativeLayout relative_right,relative_left;
        private View view;
        public ViewHolder(View itemView) {
            super(itemView);
            view = itemView;
            image_fenlei_one = itemView.findViewById(R.id.image_fenlei_one);
            image_fenlei_two = itemView.findViewById(R.id.image_fenlei_two);
            tv_fenlei_one = itemView.findViewById(R.id.tv_fenlei_one);
//            tv_xiangqing_one = itemView.findViewById(R.id.tv_xiangqing_one);
            tv_fenlei_two = itemView.findViewById(R.id.tv_fenlei_two);
            relative_right = itemView.findViewById(R.id.relative_right);
            relative_left = itemView.findViewById(R.id.relative_left);
//            tv_xiangqing_two = itemView.findViewById(R.id.tv_xiangqing_two);
        }
    }
}
