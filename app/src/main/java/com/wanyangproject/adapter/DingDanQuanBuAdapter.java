package com.wanyangproject.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.wanyangproject.R;
import com.wanyangproject.entity.QuanBuEntity;
import com.wanyangproject.utils.ContractUtils;

import java.util.List;

/**
 * Created by 甜甜 on 2018/8/21.
 */

public class DingDanQuanBuAdapter extends RecyclerView.Adapter<DingDanQuanBuAdapter.ViewHolder>{

    private Context context;
    private List<QuanBuEntity.ResponseBean.GoodsBean> goods;

    public DingDanQuanBuAdapter(Context context, List<QuanBuEntity.ResponseBean.GoodsBean> goods) {
        this.context = context;
        this.goods = goods;
    }


//    点击进入详情
    private XiangQingClick xiangQingClick;

    public void setXiangQingClick(XiangQingClick xiangQingClick) {
        this.xiangQingClick = xiangQingClick;
    }

    public interface XiangQingClick{
        void xiangqingClick(String id);
    }


    @Override
    public DingDanQuanBuAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_dingdan,parent,false);
        final ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final DingDanQuanBuAdapter.ViewHolder holder, final int position) {
        holder.relative.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(xiangQingClick != null){
                    xiangQingClick.xiangqingClick(goods.get(position).getId());
                }
            }
        });












        if(ContractUtils.PHOTO_URL+goods.get(position).getMaster() == null){

        }else{ //商品的图片
            Glide.with(context).load(ContractUtils.PHOTO_URL+goods.get(position).getMaster()).into(holder.image_tupian);
        }


        if(goods.get(position).getGoods_name() == null){

        }else{  // 商品的名称
            holder.tv_name.setText(goods.get(position).getGoods_name());
        }


        if(goods.get(position).getPic() == null){

        }else{  //商品的单价
            holder.tv_money.setText("¥"+goods.get(position).getPic());
        }


        if(goods.get(position).getNum() == null){

        }else{
            holder.tv_number.setText("x"+goods.get(position).getNum());
        }
    }

    @Override
    public int getItemCount() {
        return goods.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView tv_name,tv_money,tv_number;
        private ImageView image_tupian;
        private RelativeLayout relative;
        private View view;
        public ViewHolder(View itemView) {
            super(itemView);
            view = itemView;
            tv_name = itemView.findViewById(R.id.tv_name);
            tv_money = itemView.findViewById(R.id.tv_money);
            tv_number = itemView.findViewById(R.id.tv_number);
            image_tupian = itemView.findViewById(R.id.image_tupian);
            relative = itemView.findViewById(R.id.relative);

        }
    }
}
