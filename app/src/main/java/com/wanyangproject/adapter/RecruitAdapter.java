package com.wanyangproject.adapter;

import android.content.Context;
import android.widget.TextView;


import com.wanyangproject.R;
import com.wanyangproject.widget.recyclerview.RecyclerAdapter;
import com.wanyangproject.widget.recyclerview.RecyclerHolder;

import java.util.List;

public class RecruitAdapter extends RecyclerAdapter<String> {

    public RecruitAdapter(Context context, List<String> data) {
        super(context, data, R.layout.item_recruit);
    }

    @Override
    public void convert(RecyclerHolder holder, String model) {
        TextView recruitJob = holder.findViewById(R.id.recruit_job);
        recruitJob.setText(model);
    }
}