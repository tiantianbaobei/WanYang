package com.wanyangproject.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.wanyangproject.R;
import com.wanyangproject.entity.YuanQuTongZhiEntity;

import java.util.List;

/**
 * Created by 甜甜 on 2018/8/16.
 */

public class YuanQuTongZhiAdapter extends RecyclerView.Adapter<YuanQuTongZhiAdapter.ViewHolder>{

    private Context context;
    private List<YuanQuTongZhiEntity.ResponseBean> response;

    public YuanQuTongZhiAdapter(Context context, List<YuanQuTongZhiEntity.ResponseBean> response) {
        this.context = context;
        this.response = response;
    }

    @Override
    public YuanQuTongZhiAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_yuanqu_tongzhi, parent, false);
        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(YuanQuTongZhiAdapter.ViewHolder holder, int position) {
        if(response.get(position).getAdd_time() == null){

        }else{ // 时间
            holder.tv_time.setText(response.get(position).getAdd_time());
        }


        if(response.get(position).getTitle() == null){

        }else{ // 标题
            holder.tv_neirong.setText(response.get(position).getTitle());
        }

    }

    @Override
    public int getItemCount() {
        return response.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView tv_time,tv_neirong;
        private View view;
        public ViewHolder(View itemView) {
            super(itemView);
            view = itemView;
            tv_time = itemView.findViewById(R.id.tv_time);
            tv_neirong = itemView.findViewById(R.id.tv_neirong);
        }
    }
}
