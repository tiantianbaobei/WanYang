package com.wanyangproject.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.wanyangproject.R;
import com.wanyangproject.entity.YuanQuShengHuoTuiJianShangJiaEntity;
import com.wanyangproject.shouye.YuanQuShengHuoActivity;
import com.wanyangproject.utils.ContractUtils;

import java.util.List;

/**
 * Created by 甜甜 on 2018/7/24.
 */

public class YuanQuShengHuoShangJiaTuiJianAdapter extends RecyclerView.Adapter<YuanQuShengHuoShangJiaTuiJianAdapter.ViewHolder>{

    private Context context;
    private List<YuanQuShengHuoTuiJianShangJiaEntity.ResponseBean.ShopBean> shop;


    public YuanQuShengHuoShangJiaTuiJianAdapter(Context context, List<YuanQuShengHuoTuiJianShangJiaEntity.ResponseBean.ShopBean> shop) {
        this.context = context;
        this.shop = shop;

    }

    private ShangJiaTuiJianClick shangJiaTuiJianClick;

    public void setShangJiaTuiJianClick(ShangJiaTuiJianClick shangJiaTuiJianClick) {
        this.shangJiaTuiJianClick = shangJiaTuiJianClick;
    }

    public interface ShangJiaTuiJianClick{
        void shangjiatuijianClick(int position);
    }

    @Override
    public YuanQuShengHuoShangJiaTuiJianAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.yuanqushenghuo_shangjiatuijian_item, parent, false);
        final ViewHolder viewHolder = new ViewHolder(view);
        viewHolder.view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(shangJiaTuiJianClick != null){
                    shangJiaTuiJianClick.shangjiatuijianClick(viewHolder.getAdapterPosition());
                }
            }
        });
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(YuanQuShengHuoShangJiaTuiJianAdapter.ViewHolder holder, int position) {
        if(shop.get(position).getNickname() == null){

        }else{
            holder.tv_title.setText(shop.get(position).getNickname());
        }


        if(shop.get(position).getSales() == null){

        }else{
            holder.tv_yuexiao.setText("月销 "+shop.get(position).getSales());
        }


        if(shop.get(position).getStarting() == null && shop.get(position).getCost() == null){

        }else{
            holder.tv_peisong.setText("起送价¥"+shop.get(position).getStarting()+" | 配送费¥"+shop.get(position).getCost());
        }


        if(shop.get(position).getMer_class() .equals("")){
            holder.tv_waimai.setVisibility(View.GONE);
        }else{

            if(shop.get(position).getMer_class().equals("外卖")){
//                holder.tv_waimai.setVisibility(View.VISIBLE);
            }else if(shop.get(position).getMer_class().equals("超市")){
//                holder.tv_waimai.setVisibility(View.VISIBLE);
                holder.tv_waimai.setBackgroundResource(R.drawable.shape_chaoshi4);
            }

            holder.tv_waimai.setText(shop.get(position).getMer_class());

        }

        if(ContractUtils.PHOTO_URL +shop.get(position).getPhoto() == null){

        }else{
            Glide.with(context).load(ContractUtils.PHOTO_URL + shop.get(position).getPhoto()).into(holder.image_shangdian);
//            Glide.with(PeopleInfoActivity.this).load(ContractUtils.PHOTO_URL + userEntity.getPhoto()).into(peopleInfoImage);
        }



        //        1 营业    2 休息
        if(shop.get(position).getBusiness() != null){
            if(shop.get(position).getBusiness().equals("1")){
                holder.tv_bidian.setVisibility(View.GONE);
            }else if(shop.get(position).getBusiness().equals("2")){
                holder.tv_bidian.setVisibility(View.VISIBLE);
            }
        }



    }

    @Override
    public int getItemCount() {
        return shop.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private ImageView image_shangdian;
        private TextView tv_title,tv_yuexiao,tv_peisong,tv_waimai,tv_bidian;
        private View view;
        public ViewHolder(View itemView) {
            super(itemView);
            view = itemView;
            image_shangdian = itemView.findViewById(R.id.image_shangdian);
            tv_title = itemView.findViewById(R.id.tv_title);
            tv_yuexiao = itemView.findViewById(R.id.tv_yuexiao);
            tv_peisong = itemView.findViewById(R.id.tv_peisong);
            tv_waimai = itemView.findViewById(R.id.tv_waimai);
            tv_bidian = itemView.findViewById(R.id.tv_bidian);
        }
    }
}
