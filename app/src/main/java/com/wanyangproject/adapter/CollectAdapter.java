package com.wanyangproject.adapter;

import android.content.Context;


import com.wanyangproject.R;
import com.wanyangproject.model.TestModel;
import com.wanyangproject.widget.recyclerview.RecyclerAdapter;
import com.wanyangproject.widget.recyclerview.RecyclerHolder;

import java.util.List;

public class CollectAdapter extends RecyclerAdapter<TestModel.ResultsBean> {

    public CollectAdapter(Context context, List<TestModel.ResultsBean> data) {
        super(context, data, R.layout.item_collect);
    }

    @Override
    public void convert(RecyclerHolder holder, TestModel.ResultsBean model) {

    }
}