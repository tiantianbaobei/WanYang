package com.wanyangproject.adapter;

import android.content.Context;
import android.widget.TextView;


import com.wanyangproject.R;
import com.wanyangproject.model.TestModel;
import com.wanyangproject.widget.recyclerview.RecyclerAdapter;
import com.wanyangproject.widget.recyclerview.RecyclerHolder;

import java.util.List;

public class ShuiAdapter extends RecyclerAdapter<TestModel.ResultsBean> {

    public ShuiAdapter(Context context, List<TestModel.ResultsBean> data) {
        super(context, data, R.layout.item_shuidian);
    }

    @Override
    public void convert(RecyclerHolder holder, TestModel.ResultsBean model) {
        TextView shuidianTime = holder.findViewById(R.id.shuidian_time);
        TextView shuidianNumber = holder.findViewById(R.id.shuidian_number);
        shuidianTime.setText("2018年1月");
        shuidianNumber.setText("523.5吨");
    }
}