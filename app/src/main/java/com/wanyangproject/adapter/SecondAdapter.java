package com.wanyangproject.adapter;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.wanyangproject.R;
import com.wanyangproject.entity.NumberEntity;
import com.wanyangproject.entity.ShangJiaShangPinEntity;
import com.wanyangproject.fragment.ShangPinFragment;
import com.wanyangproject.shouye.WaiMaiActivity;
import com.wanyangproject.utils.ContractUtils;

import java.util.List;

/**
 * Created by 甜甜 on 2018/7/21.
 */

public class SecondAdapter extends RecyclerView.Adapter<SecondAdapter.ViewHolder>{

    private Context context;
    private List<ShangJiaShangPinEntity.ResponseBean.GoodsBeanX> goods;
    private List<ShangJiaShangPinEntity.ResponseBean.GoodsBeanX.GoodsBean> list;
    private ShangJiaShangPinEntity.ResponseBean.ShopBeanX shop;
    private WaiMaiActivity waimai ;
    private int iii;
    private NetWork netWork;
    private int kucun;
    private int position;





    public SecondAdapter(Context context, List<ShangJiaShangPinEntity.ResponseBean.GoodsBeanX> goods, ShangJiaShangPinEntity.ResponseBean.ShopBeanX shop, WaiMaiActivity waimai) {
        this.context = context;
        this.goods = goods;
        this.shop = shop;
        this.waimai = waimai;

    }


//    跳转商品详请
    private ShangPinXiangQingClick shangPinXiangQingClick;

    public void setShangPinXiangQingClick(ShangPinXiangQingClick shangPinXiangQingClick) {
        this.shangPinXiangQingClick = shangPinXiangQingClick;
    }

    public interface ShangPinXiangQingClick{
        void shangpinxiangqingClick(int position,String peisongfei,String qisongjia,String shangpinid,String kucun);
    }






    @Override
    public SecondAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_shangpin_second, parent, false);
        final ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final SecondAdapter.ViewHolder holder, final int position) {


        IntentFilter intentFilter5 = new IntentFilter();
        intentFilter5.addAction("kucun");
        netWork = new NetWork();
        context.registerReceiver(netWork, intentFilter5);



        holder.relative1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                System.out.println(position+"        0000000000000");
                if(shangPinXiangQingClick != null){
                    shangPinXiangQingClick.shangpinxiangqingClick(holder.getAdapterPosition(),shop.getShop().getCost(),shop.getShop().getStarting(),list.get(position).getId(),list.get(position).getStock());
                }
            }
        });






        if(list != null){

            if(list.get(position).getStock() == null && list.get(position).getVolume() == null){

            }else{
                holder.tv_kucun.setText("库存:"+list.get(position).getStock()+"  月销:"+list.get(position).getVolume());
            }






            if(list.get(position).getGoods_name() == null){

            }else{
                holder.tv_name.setText(list.get(position).getGoods_name());
            }


            if(list.get(position).getStock() == null && list.get(position).getVolume() == null){

            }else{
                holder.tv_kucun.setText("库存:"+list.get(position).getStock()+"  月销:"+list.get(position).getVolume());
            }


            if(list.get(position).getMarket() == null){

            }else{
                holder.tv_shichangjia.setText("¥ "+list.get(position).getMarket());
            }


            if(list.get(position).getPic() == null){

            }else{
                holder.tv_money.setText("¥ "+list.get(position).getPic());
            }




            if(list.get(position).getMaster() != null){
                String master = list.get(position).getMaster();
                if(master.indexOf("http") != -1){
                    String http = master.substring(master.indexOf("http"));
                    System.out.println(http+"     http");
                    Glide.with(context).load(http).into(holder.image_shangpin);
                }else {
                    Glide.with(context).load(ContractUtils.PHOTO_URL+list.get(position).getMaster()).into(holder.image_shangpin);
                }
            }


            String num = "0";
            for (int i = 0; i < waimai.getList().size(); i++) {
                NumberEntity numberEntity =  waimai.getList().get(i);
                if(numberEntity.getId().equals(list.get(position).getId())){
                    num = numberEntity.getNumber();
                }
            }
            holder.tv_number.setText(num);




            holder.image_add.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if(list.get(position).getStock().equals("0")){
                        Toast.makeText(context, "暂无库存", Toast.LENGTH_SHORT).show();
                    }else{
                        int jia = Integer.parseInt(holder.tv_number.getText().toString());
                        jia++;
                        iii = Integer.parseInt(list.get(position).getStock());

                        if(iii == 0){
                            Toast.makeText(context, "库存不足", Toast.LENGTH_SHORT).show();
                            return;
                        }
                        iii--;
                        if(list.get(position).getStock() == null && list.get(position).getVolume() == null){

                        }else{
                            holder.tv_kucun.setText("库存:"+iii+"  月销:"+list.get(position).getVolume());
                        }
                        list.get(position).setStock(iii+"");

                        holder.tv_number.setText(jia + "");
                        System.out.println(jia+"   加加加");
                        Intent intent = new Intent();
                        intent.putExtra("number",jia+"");
                        intent.putExtra("id",list.get(position).getId());
                        intent.putExtra("money",list.get(position).getPic());
                        intent.putExtra("name",list.get(position).getGoods_name());
                        intent.setAction("11");
                        context.sendBroadcast(intent);

                        System.out.println(waimai.getList().size()+"   1111");
                    }
                }
            });


            holder.image_jian.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    int jian = Integer.parseInt(holder.tv_number.getText().toString());
                    jian--;
                    iii = Integer.parseInt(list.get(position).getStock());
                    if (jian < 0) {
                        jian = 0;
                    }else {
                        iii++;
                    }

                    if(list.get(position).getStock() == null && list.get(position).getVolume() == null){

                    }else{
                        holder.tv_kucun.setText("库存:"+iii+"  月销:"+list.get(position).getVolume());
                    }
                    list.get(position).setStock(iii+"");

                    holder.tv_number.setText(jian + "");
                    System.out.println(jian+"    减减减");
                    Intent intent = new Intent();
                    intent.putExtra("number",jian+"");
                    intent.putExtra("id",list.get(position).getId());
                    intent.putExtra("money",list.get(position).getPic());
                    intent.setAction("11");
                    context.sendBroadcast(intent);
                }
            });
        }
    }



    class NetWork extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {

            kucun = intent.getIntExtra("kucun",0);
            position = intent.getIntExtra("position", 0);

            System.out.println(kucun+"           接受广播库存");
            System.out.println(position+"        下标接受广播库存");

           list.get(position).setStock(kucun+"");
            notifyDataSetChanged();

        }
    }



    @Override
    public int getItemCount() {
        for (ShangJiaShangPinEntity.ResponseBean.GoodsBeanX shangJiaShangPinEntity:goods){
            if(shangJiaShangPinEntity.isOk()){
                list = shangJiaShangPinEntity.getGoods();
            }
        }
        if(list == null){
            return 0;
        }
        return list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private ImageView image_shangpin,image_add,image_jian;
        private TextView tv_name,tv_kucun,tv_money,tv_number;
        private RelativeLayout relative1;
        private TextView tv_shichangjia;
        private View view;
        public ViewHolder(View itemView) {
            super(itemView);
            view = itemView;
            image_shangpin = itemView.findViewById(R.id.image_shangpin);
            tv_name = itemView.findViewById(R.id.tv_name);
            tv_kucun = itemView.findViewById(R.id.tv_kucun);
            tv_money = itemView.findViewById(R.id.tv_money);
            image_add = itemView.findViewById(R.id.image_add);
            image_jian = itemView.findViewById(R.id.image_jian);
            tv_number = itemView.findViewById(R.id.tv_number);
            relative1 = itemView.findViewById(R.id.relative1);
            tv_shichangjia = itemView.findViewById(R.id.tv_shichangjia);
        }
    }
}
