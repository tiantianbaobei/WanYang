package com.wanyangproject.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.wanyangproject.R;

/**
 * Created by 甜甜 on 2018/8/8.
 */

public class ShouYePingJiaAdapter extends RecyclerView.Adapter<ShouYePingJiaAdapter.ViewHolder>{

    private Context context;

    public ShouYePingJiaAdapter(Context context) {
        this.context = context;
    }



    private PingJiaClick pingJiaClick;

    public void setPingJiaClick(PingJiaClick pingJiaClick) {
        this.pingJiaClick = pingJiaClick;
    }

    public interface PingJiaClick{
        void pingjiaClick(int position);
    }

    @Override
    public ShouYePingJiaAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.waimai_item, parent, false);
        final ViewHolder viewHolder = new ViewHolder(view);
        viewHolder.view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(pingJiaClick != null){
                    pingJiaClick.pingjiaClick(viewHolder.getAdapterPosition());
                }
            }
        });
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ShouYePingJiaAdapter.ViewHolder holder, int position) {

    }

    @Override
    public int getItemCount() {
        return 6;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private ImageView image_shangdian;
        private TextView tv_title,tv_yuexiao,tv_peisong;
        private Button btn_chaoshi;
        private View view;
        public ViewHolder(View itemView) {
            super(itemView);
            view = itemView;
            image_shangdian = itemView.findViewById(R.id.image_shangdian);
            tv_title = itemView.findViewById(R.id.tv_title);
            tv_yuexiao = itemView.findViewById(R.id.tv_yuexiao);
            tv_peisong = itemView.findViewById(R.id.tv_peisong);
            btn_chaoshi = itemView.findViewById(R.id.btn_chaoshi);
        }
    }
}
