package com.wanyangproject.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.wanyangproject.R;
import com.wanyangproject.entity.FuWuJiLuJinXingEntity;

import java.util.List;

/**
 * Created by 甜甜 on 2018/8/28.
 */

public class FuWuJiLuJinXingAdapter extends RecyclerView.Adapter<FuWuJiLuJinXingAdapter.ViewHolder>{

    private Context context ;
    private List<FuWuJiLuJinXingEntity.ResponseBean> response;

    public FuWuJiLuJinXingAdapter(Context context, List<FuWuJiLuJinXingEntity.ResponseBean> response) {
        this.context = context;
        this.response = response;
    }



//    点击进入详情
    private JinXingXiangQingClick jinXingXiangQingClick;

    public void setJinXingXiangQingClick(JinXingXiangQingClick jinXingXiangQingClick) {
        this.jinXingXiangQingClick = jinXingXiangQingClick;
    }

    public interface JinXingXiangQingClick{
        void jinxingXiangQingClick(int position,String id);
}


    @Override
    public FuWuJiLuJinXingAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_fuwu_jinxing,parent,false);
        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final FuWuJiLuJinXingAdapter.ViewHolder holder, final int position) {
        holder.relative_one.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(jinXingXiangQingClick != null){
                    jinXingXiangQingClick.jinxingXiangQingClick(holder.getAdapterPosition(),response.get(position).getId());
                }
            }
        });




       if(response.get(position).getEntype().equals("")){

       }else{
           if(response.get(position).getEntype().equals("1")){
               holder.tv_title.setText("企业服务");
           }else if(response.get(position).getEntype().equals("2")){
               holder.tv_title.setText("商家优惠券");
           }else if(response.get(position).getEntype().equals("3")){
               holder.tv_title.setText("物业服务");
           }
       }



//        0:待处理 1:处理中 2:完成 3:退回 4:关闭
        if(response.get(position).getState() != null){
            if(response.get(position).getState().equals("0")){
                holder.tv_zhuagtai.setText("待处理");
            }else if(response.get(position).getState().equals("1")){
                holder.tv_zhuagtai.setText("处理中");
            }else if(response.get(position).getState().equals("2")){
                holder.tv_zhuagtai.setText("完成");
            }else if(response.get(position).getState().equals("3")){
                holder.tv_zhuagtai.setText("退回");
            }else if(response.get(position).getState().equals("4")){
                holder.tv_zhuagtai.setText("关闭");
            }
        }





        if(response.get(position).getAdd_time() == null){

        }else{
            holder.tv_time.setText(response.get(position).getAdd_time());
        }


        if(response.get(position).getServicename() == null){

        }else{
            holder.tv_neirong.setText(response.get(position).getServicename());
        }
    }


    @Override
    public int getItemCount() {
        return response.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView tv_title,tv_time,tv_neirong,tv_zhuagtai;
        private RelativeLayout relative_one;
        private View view;
        public ViewHolder(View itemView) {
            super(itemView);
            view = itemView;
            tv_title = itemView.findViewById(R.id.tv_title);
            tv_time = itemView.findViewById(R.id.tv_time);
            tv_neirong = itemView.findViewById(R.id.tv_neirong);
            relative_one = itemView.findViewById(R.id.relative_one);
            tv_zhuagtai = itemView.findViewById(R.id.tv_zhuagtai);
        }
    }
}
