package com.wanyangproject.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.wanyangproject.R;
import com.wanyangproject.entity.ShouYeYuanQuZiXunEntity;

import java.util.List;

/**
 * Created by 甜甜 on 2018/7/25.
 */

public class ShouYeZiXunAdapter extends RecyclerView.Adapter<ShouYeZiXunAdapter.ViewHolder>{

    private Context context;
    private List<ShouYeYuanQuZiXunEntity.ResponseBean> response;

    public ShouYeZiXunAdapter(Context context, List<ShouYeYuanQuZiXunEntity.ResponseBean> response) {
        this.context = context;
        this.response = response;
    }


//    点击园区资讯进入详情
    private ShouYeZiXunClick shouYeZiXunClick;

    public void setShouYeZiXunClick(ShouYeZiXunClick shouYeZiXunClick) {
        this.shouYeZiXunClick = shouYeZiXunClick;
    }

    public interface ShouYeZiXunClick{
        void shouyezixunClick(int position,String id);
}



    @Override
    public ShouYeZiXunAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.shouye_item, parent, false);
        final ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final ShouYeZiXunAdapter.ViewHolder holder, final int position) {
        holder.relative.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(shouYeZiXunClick != null){
                    shouYeZiXunClick.shouyezixunClick(holder.getAdapterPosition(),response.get(position).getId());
                }
            }
        });



        if(response.get(position).getTitle() == null){

        }else{  // 标题
            holder.tv_title.setText(response.get(position).getTitle());
        }

        if(response.get(position).getDesc() == null){

        }else{  // 内容
            String replace1 = response.get(position).getDesc().replace("&nbsp;", " ");
            holder.tv_neirong.setText(replace1);
        }

        if(response.get(position).getAdd_time() == null){

        }else{  // 时间
            holder.tv_time.setText(response.get(position).getAdd_time());
        }

        if(response.get(position).getLogo() == null){

        }else{  // 头像
            Glide.with(context).load(response.get(position).getLogo().replace("\"","")).into(holder.image_touxiang);
        }


    }

    @Override
    public int getItemCount() {
        return response.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private ImageView image_touxiang;
        private TextView tv_title,tv_time,tv_neirong;
        private RelativeLayout relative;
        private View view;
        public ViewHolder(View itemView) {
            super(itemView);
            view = itemView;
            image_touxiang = itemView.findViewById(R.id.image_touxiang);
            tv_title = itemView.findViewById(R.id.tv_title);
            tv_time = itemView.findViewById(R.id.tv_time);
            tv_neirong = itemView.findViewById(R.id.tv_neirong);
            relative = itemView.findViewById(R.id.relative);
        }
    }
}
