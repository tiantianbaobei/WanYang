package com.wanyangproject.adapter;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.v4.app.NavUtils;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.wanyangproject.R;
import com.wanyangproject.entity.ShouHuoXinXiLieBiaoEntity;
import com.wanyangproject.shouye.ShouHuoXinXiActivity;

import java.util.List;

/**
 * Created by 甜甜 on 2018/7/31.
 */

public class ShouHuoXinXiAdapter extends RecyclerView.Adapter<ShouHuoXinXiAdapter.ViewHolder>{

    private Context context;
    private Boolean isOK = false;
    private ViewHolder vv;
    private List<ShouHuoXinXiLieBiaoEntity.ResponseBean> response;

    public ShouHuoXinXiAdapter(Context context, List<ShouHuoXinXiLieBiaoEntity.ResponseBean> response) {
        this.context = context;
        this.response = response;
    }


//    点击行布局想上个界面传地址信息
    private DianJiHangBuJuClick dianJiHangBuJuClick;

    public void setDianJiHangBuJuClick(DianJiHangBuJuClick dianJiHangBuJuClick) {
        this.dianJiHangBuJuClick = dianJiHangBuJuClick;
    }

    public interface DianJiHangBuJuClick{
        void dianjihangbujuClick(int position,String phone,String name,String address,String addressid,String sheng);
    }


//    点击删除
    private ShanChuClick shanChuClick;

    public void setShanChuClick(ShanChuClick shanChuClick) {
        this.shanChuClick = shanChuClick;
    }

    public interface ShanChuClick{
        void shanchuclick(int position);
    }

//        点击默认
    private MoRenClick moRenClick;

    public void setMoRenClick(MoRenClick moRenClick) {
        this.moRenClick = moRenClick;
    }

    public interface MoRenClick{
    void morenClick(int position,String id);
    }


//    点击编辑
    private BianJiClick bianJiClick;

    public void setBianJiClick(BianJiClick bianJiClick) {
        this.bianJiClick = bianJiClick;
    }

    public interface BianJiClick{
        void bianjiClick(int position);
}






    @Override
    public ShouHuoXinXiAdapter.ViewHolder onCreateViewHolder(final ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_shouhuo_xinxi,parent,false);
        final ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }


    @Override
    public void onBindViewHolder(final ShouHuoXinXiAdapter.ViewHolder holder, final int position) {
        System.out.println(position+"     position");




        holder.relative.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(dianJiHangBuJuClick != null){
                    dianJiHangBuJuClick.dianjihangbujuClick(holder.getAdapterPosition(),response.get(position).getPhone(),response.get(position).getName(),response.get(position).getAddres(),response.get(position).getId(),response.get(position).getRegion());
                }
            }
        });





        if(response.get(position).getName() == null){

        }else{
            holder.tv_name.setText(response.get(position).getName());
        }

        if(response.get(position).getPhone() == null){

        }else{
            holder.tv_phone.setText(response.get(position).getPhone());
        }

        if(response.get(position).getRegion()+""+response.get(position).getAddres() == null){

        }else{
            holder.tv_address.setText(response.get(position).getRegion()+""+response.get(position).getAddres());
        }


//        if(response.get(position).getType() == null){
//
//        }else{
//            if(response.get(position).getType().equals("1")){
//                holder.image_false.setImageResource(R.drawable.checked);
//
//            }else{
//                holder.image_false.setImageResource(R.drawable.unchecked);
//            }
//        }


        SharedPreferences sharedPreferences = context.getSharedPreferences("dz",Context.MODE_APPEND);
        String string = sharedPreferences.getString("dizhi", "dizhi");
        if(response.get(position).getId().equals(string)){
            holder.image_false.setImageResource(R.drawable.checked);
        }else{
            holder.image_false.setImageResource(R.drawable.unchecked);
        }


//        删除
        holder.image_delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(shanChuClick != null){
                    shanChuClick.shanchuclick(holder.getAdapterPosition());
                }
            }
        });


//        编辑
        holder.image_bianji.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(bianJiClick != null){
                    bianJiClick.bianjiClick(holder.getAdapterPosition());
                }
            }
        });


        holder.image_false.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(moRenClick != null){
                    moRenClick.morenClick(holder.getAdapterPosition(),response.get(position).getId());
                }

                if(response.get(position).getType().equals("1")){
                    holder.image_false.setImageResource(R.drawable.unchecked);
                }else{
                    holder.image_false.setImageResource(R.drawable.checked);
                }









//                holder.image_false.setImageResource(position);
//                System.out.println(position+"     qqqqqqqqqqqqqqqqqqqqqq");
////                默认地址
//                if( vv != holder){
//                    if(vv != null){
//                        vv.image_false.setImageResource(R.drawable.unchecked);
//                    }
//                    holder.image_false.setImageResource(R.drawable.checked);
//                }else{
//                    if(isOK){
//                        holder.image_false.setImageResource(R.drawable.unchecked);
//                    }else {
//                        holder.image_false.setImageResource(R.drawable.checked);
//                    }
//                   isOK = !isOK;
//
//                }
//                vv = holder;
            }
        });
    }

    @Override
    public int getItemCount() {
        return response.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView tv_name,tv_phone,tv_address;
        private ImageView image_false,image_bianji,image_delete;
        private RelativeLayout relative;
        private View view;
        public ViewHolder(View itemView) {
            super(itemView);
            view = itemView;
            tv_name = itemView.findViewById(R.id.tv_name);
            tv_phone = itemView.findViewById(R.id.tv_phone);
            tv_address = itemView.findViewById(R.id.tv_address);
            image_false = itemView.findViewById(R.id.image_false);
            image_bianji = itemView.findViewById(R.id.image_bianji);
            image_delete = itemView.findViewById(R.id.image_delete);
            relative = itemView.findViewById(R.id.relative);
        }
    }
}
