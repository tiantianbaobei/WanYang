package com.wanyangproject.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.google.gson.Gson;
import com.wanyangproject.R;
import com.wanyangproject.entity.ShouYeYuanQuQiYeEntity;
import com.wanyangproject.shouye.YuanQuQiYeActivity;
import com.wanyangproject.utils.ContractUtils;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by 甜甜 on 2018/7/24.
 */

public class YuanQuQiYeAdapter extends RecyclerView.Adapter<YuanQuQiYeAdapter.ViewHolder>{
    private Context context;
    private ArrayList<JSONObject> list;
    private ArrayList<String> arrayList = new ArrayList<>();
    private HashMap<Integer,String> map = new HashMap<>();


    public YuanQuQiYeAdapter(Context context, ArrayList<ArrayList<JSONObject>> list) {
        this.context = context;
        this.list = new ArrayList<JSONObject>();
        for (int i = 0; i < list.size(); i++) {
            ArrayList<JSONObject> jsonObjects = list.get(i);
            for (int j = 0; j < jsonObjects.size(); j++) {
                this.list.add(jsonObjects.get(j));
            }

        }
//        arrayList.clear();
    }

    private YuanQuQiYeClick yuanQuQiYeClick;

    public void setYuanQuQiYeClick(YuanQuQiYeClick yuanQuQiYeClick) {
        this.yuanQuQiYeClick = yuanQuQiYeClick;
    }

    public interface YuanQuQiYeClick{
        void yuanquqiyeClick(int position,String id);
    }



//    招聘信息简介点击进入详情
    private XingQingClick xingQingClick;

    public void setXingQingClick(XingQingClick xingQingClick) {
        this.xingQingClick = xingQingClick;
    }

    public interface XingQingClick{
        void xiangqingClivk(int position,String id);
}




    @Override
    public YuanQuQiYeAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.yuanqu_qiye_item, parent, false);
        final ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final YuanQuQiYeAdapter.ViewHolder holder, final int position) {

        holder.parkenterprise_ll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    String enterId = list.get(position).getString("enterId");
                    if(enterId == null){

                    }else{
                        if(yuanQuQiYeClick != null){
                            yuanQuQiYeClick.yuanquqiyeClick(holder.getAdapterPosition(),enterId);
                        }

                    }
                } catch (JSONException e) {
                }
            }
        });



//        holder.relative.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                try {
//                    String enterId = list.get(position).getString("enterId");
//                    if(enterId == null){
//
//                    }else{
//                        if(xingQingClick != null){
//                            xingQingClick.xiangqingClivk(holder.getAdapterPosition(),enterId);
//                        }
//
//                    }
//                } catch (JSONException e) {
//                }
//
//
//            }
//        });




//        字母索引上面
        try {
            if(arrayList.contains(list.get(position).getString("initials"))){
//                holder.relative_zimu.setVisibility(View.GONE);

                if(map.containsKey(position)){
                    holder.relative_zimu.setVisibility(View.VISIBLE);
                    String s = map.get(position);
                    holder.tv_zimu.setText(s);
                }else{
                    holder.relative_zimu.setVisibility(View.GONE);
                }



            }else{
                holder.relative_zimu.setVisibility(View.VISIBLE);
                arrayList.add(list.get(position).getString("initials"));
                map.put(position, list.get(position).getString("initials"));
                holder.tv_zimu.setText(list.get(position).getString("initials"));
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }


        try {
            String touxiang = list.get(position).getString("logoUrl");
            if(touxiang == null){
            }else{ // 头像
                if(touxiang.contains(ContractUtils.PHOTO_URL)){
                    Glide.with(context).load(touxiang).into(holder.image_touxiang);
                }else{
                    Glide.with(context).load(ContractUtils.PHOTO_URL+touxiang).into(holder.image_touxiang);
                }
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }


        try {
            String title = list.get(position).getString("enterName");
            if(title == null){

            }else{ // 标题
                holder.tv_title.setText(title);
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }

        try {
            String address = list.get(position).getString("address");
            if(address == null){

            }else{  //  企业位置
                holder.tv_address.setText("企业位置："+address);
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }


//        if(list.get(position).get == null){
//
//        }else{//  头像
//            Glide.with(context).load(list.get(position).getLogoUrl()).into(holder.image_touxiang);
//        }
//
//        if(list.get(position).getEnterName() == null){
//
//        }else{ // 标题
//            holder.tv_title.setText(list.get(position).getEnterName());
//        }
//
//        if(list.get(position).getAddress() == null){
//
//        }else{ // 地址
//            holder.tv_address.setText("企业位置："+list.get(position).getAddress());
//        }
    }

    @Override
    public int getItemCount() {
        if(list == null){
            return 0;
        }
        return list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView tv_title,tv_address,tv_zimu;
        private ImageView image_touxiang;
        private RelativeLayout parkenterprise_ll,relative_zimu;
        private View view;
        public ViewHolder(View itemView) {
            super(itemView);
            view = itemView;
            image_touxiang = itemView.findViewById(R.id.image_touxiang);
            tv_title = itemView.findViewById(R.id.tv_title);
            tv_address = itemView.findViewById(R.id.tv_address);
            parkenterprise_ll = itemView.findViewById(R.id.parkenterprise_ll);
            relative_zimu = itemView.findViewById(R.id.relative_zimu);
            tv_zimu = itemView.findViewById(R.id.tv_zimu);

        }
    }
}
