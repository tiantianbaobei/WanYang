package com.wanyangproject.adapter;

import android.content.Context;
import android.media.Image;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.wanyangproject.R;
import com.wanyangproject.entity.HuoQuYuanQuLieBiaoEntity;
import com.wanyangproject.shouye.XuanZeYuanQuActivity;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by 甜甜 on 2018/8/6.
 */

public class XuanZeYuanQuAdapter extends RecyclerView.Adapter<XuanZeYuanQuAdapter.ViewHolder>{

    private Context context;
//    private List<HuoQuYuanQuLieBiaoEntity.ResponseBean> msg;
    private ArrayList<JSONObject> jsonArrayFenZu;
    public ArrayList<JSONObject> list = new ArrayList();
    private ArrayList<String> arrayList = new ArrayList<>();
    private HashMap<Integer,String> map = new HashMap<>();


//    public XuanZeYuanQuAdapter(Context context, List<HuoQuYuanQuLieBiaoEntity.ResponseBean> msg) {
//        this.context = context;
//        this.msg = msg;
//    }


    public XuanZeYuanQuAdapter(Context context, ArrayList<JSONObject> jsonArrayFenZu) {
        this.context = context;
        this.jsonArrayFenZu = jsonArrayFenZu;
    }



//    选择园区
    private XuanZeYuanQuClick xuanZeYuanQuClick;



    public void setXuanZeYuanQuClick(XuanZeYuanQuClick xuanZeYuanQuClick) {
        this.xuanZeYuanQuClick = xuanZeYuanQuClick;
    }

    public interface XuanZeYuanQuClick{
        void xuanzeyuanquClick(int position);
    }



    @Override
    public XuanZeYuanQuAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_xuanzeuanqu, parent, false);
        final ViewHolder viewHolder = new ViewHolder(view);
        viewHolder.view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(xuanZeYuanQuClick != null){
                    xuanZeYuanQuClick.xuanzeyuanquClick(viewHolder.getAdapterPosition());
                }
            }
        });
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final XuanZeYuanQuAdapter.ViewHolder holder, final int position) {
//        if(msg.get(position).getParkName() == null){
//
//        }else{
//            holder.tv_yuanqu.setText(msg.get(position).getParkName());
//        }


        if(list.contains(jsonArrayFenZu.get(position))){
            holder.image.setImageResource(R.drawable.checked);
        }else{
            holder.image.setImageResource(R.drawable.unchecked);
        }




        try {

            String parkName = jsonArrayFenZu.get(position).getString("parkName");
            if(parkName != null){
                holder.tv_yuanqu.setText(parkName);
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }


        //        字母索引上面
        try {
            if(arrayList.contains(jsonArrayFenZu.get(position).getString("initials"))){
//                holder.relative_zimu.setVisibility(View.GONE);

                if(map.containsKey(position)){
                    holder.relative_zimu.setVisibility(View.VISIBLE);
                    String s = map.get(position);
                    holder.tv_zimu.setText(s);
                }else{
                    holder.relative_zimu.setVisibility(View.GONE);
                }

            }else{
                holder.relative_zimu.setVisibility(View.VISIBLE);
                arrayList.add(jsonArrayFenZu.get(position).getString("initials"));
                map.put(position, jsonArrayFenZu.get(position).getString("initials"));
                holder.tv_zimu.setText(jsonArrayFenZu.get(position).getString("initials"));
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }






        holder.image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                HuoQuYuanQuLieBiaoEntity.ResponseBean msgBean = msg.get(position);

                list.clear();
                JSONObject jsonObject = jsonArrayFenZu.get(position);
                list.add(jsonObject);
                notifyDataSetChanged();





//                if(list.contains(jsonObject)){
//                    list.remove(jsonObject);
//                    holder.image.setImageResource(R.drawable.unchecked);
//                    System.out.println(list.size()+"      1111111111");
//                }else{
//                    list.add(jsonObject);
//                    holder.image.setImageResource(R.drawable.checked);
//                    System.out.println(list.size()+"         2222222222222222");
//                }
            }
        });
    }


    @Override
    public int getItemCount() {
        return jsonArrayFenZu.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView tv_yuanqu,tv_zimu;
        private ImageView image;
        private RelativeLayout relative_zimu;
        private View view;
        public ViewHolder(View itemView) {
            super(itemView);
            view = itemView;
            tv_yuanqu = itemView.findViewById(R.id.tv_yuanqu);
            image = itemView.findViewById(R.id.image);
            relative_zimu = itemView.findViewById(R.id.relative_zimu);
            tv_zimu = itemView.findViewById(R.id.tv_zimu);
        }
    }
}
