package com.wanyangproject.adapter;

import android.content.Context;
import android.view.View;
import android.widget.TextView;


import com.wanyangproject.R;
import com.wanyangproject.model.TestModel;
import com.wanyangproject.widget.recyclerview.RecyclerAdapter;
import com.wanyangproject.widget.recyclerview.RecyclerHolder;

import java.util.List;

public class CompanyJoinAdapter extends RecyclerAdapter<TestModel.ResultsBean> {

    public CompanyJoinAdapter(Context context, List<TestModel.ResultsBean> data) {
        super(context, data, R.layout.item_company_join);
    }

    @Override
    public void convert(RecyclerHolder holder, TestModel.ResultsBean model) {
        TextView companyJoinState = holder.findViewById(R.id.company_join_state);
        TextView companyJoinReason = holder.findViewById(R.id.company_join_reason);

        if (holder.position % 3 == 0) {
            companyJoinState.setText("审核中");
            companyJoinState.setTextColor(mContext.getResources().getColor(R.color.blue));
            companyJoinReason.setVisibility(View.GONE);
        } else if (holder.position % 3 == 1) {
            companyJoinState.setText("通过审核");
            companyJoinState.setTextColor(mContext.getResources().getColor(R.color.green));
            companyJoinReason.setVisibility(View.GONE);
        } else if (holder.position % 3 == 2) {
            companyJoinState.setText("审核失败");
            companyJoinState.setTextColor(mContext.getResources().getColor(R.color.red));
            companyJoinReason.setVisibility(View.VISIBLE);
        }
    }
}