package com.wanyangproject.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.wanyangproject.R;
import com.wanyangproject.entity.WuYeFuWuLieBiaoEntity;
import com.wanyangproject.fuwuactivity.WuYeFuWuLieBiaoActivity;

import java.util.List;

/**
 * Created by 甜甜 on 2018/9/1.
 */

public class WuYeLieBiaoAdapter extends RecyclerView.Adapter<WuYeLieBiaoAdapter.ViewHolder>{

    private Context context;
    private List<WuYeFuWuLieBiaoEntity.ResponseBean.DataBean> data;

    public WuYeLieBiaoAdapter(Context context, List<WuYeFuWuLieBiaoEntity.ResponseBean.DataBean> data) {
        this.context = context;
        this.data = data;
    }



//    点击物业服务的列表
    private WuYeLieBiaoClick wuYeLieBiaoClick;

    public void setWuYeLieBiaoClick(WuYeLieBiaoClick wuYeLieBiaoClick) {
        this.wuYeLieBiaoClick = wuYeLieBiaoClick;
    }

    public interface WuYeLieBiaoClick{
        void wueLieViaoClick(int position,String id);
    }







    @Override
    public WuYeLieBiaoAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_wuyefuwu, parent, false);
        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final WuYeLieBiaoAdapter.ViewHolder holder, final int position) {
        holder.relative_huodong.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(wuYeLieBiaoClick != null){
                    wuYeLieBiaoClick.wueLieViaoClick(holder.getAdapterPosition(),data.get(position).getId());
                }
            }
        });








        if(data.get(position).getTitle() == null){

        }else{
            holder.tv_title.setText(data.get(position).getTitle());
        }


        if(data.get(position).getDesc() == null){

        }else{
            holder.tv_neirong.setText(data.get(position).getDesc());
        }
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView tv_title,tv_neirong;
        private RelativeLayout relative_huodong;
        private View view;
        public ViewHolder(View itemView) {
            super(itemView);
            view = itemView;
            tv_title = itemView.findViewById(R.id.tv_title);
            tv_neirong = itemView.findViewById(R.id.tv_neirong);
            relative_huodong = itemView.findViewById(R.id.relative_huodong);
        }
    }
}
