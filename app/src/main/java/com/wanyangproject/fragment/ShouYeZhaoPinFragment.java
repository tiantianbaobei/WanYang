package com.wanyangproject.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.gson.Gson;
import com.wanyangproject.R;
import com.wanyangproject.adapter.ShouYeZhaoPinAdapter;
import com.wanyangproject.adapter.TabAdapter;
import com.wanyangproject.entity.QiuZhiZhaoPinLieBiaoEntity;
import com.wanyangproject.entity.QiuZhiZhaoPinTitleEntity;
import com.wanyangproject.my.ZhaoPinXiangQingActivity;
import com.wanyangproject.shouye.QiuZhiZhaoPinActivity;
import com.wanyangproject.utils.ContractUtils;
import com.zhy.http.okhttp.OkHttpUtils;
import com.zhy.http.okhttp.callback.StringCallback;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import okhttp3.Call;

/**
 * Created by 甜甜 on 2018/8/5.
 */

public class ShouYeZhaoPinFragment extends Fragment {
    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    private ShouYeZhaoPinAdapter shouYeZhaoPinAdapter;
    private QiuZhiZhaoPinTitleEntity qiuZhiZhaoPinTitleEntity;
    private String typeid;
    private QiuZhiZhaoPinLieBiaoEntity qiuZhiZhaoPinLieBiaoEntity;

    public String getTypeid() {
        return typeid;
    }

    public void setTypeid(String typeid) {
        this.typeid = typeid;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.my_zhaopin_fragment, container, false);
        ButterKnife.bind(this, view);
        initView();

        //        求职招聘列表的网络请求
        initZhaoPinHttp();


        return view;
    }



//    求职招聘列表的网络请求
    private void initZhaoPinHttp() {
        System.out.println(typeid+"      typeid");
        OkHttpUtils.post()
                .url(ContractUtils.LOGIN_URL+"zhaopin/index")
                .addHeader("token",ContractUtils.getTOKEN(getContext()))
                .addParams("parkId",ContractUtils.getParkId(getContext()))
                .addParams("typeId",typeid)
                .build()
                .execute(new StringCallback() {
                    @Override
                    public void onError(Call call, Exception e, int id) {

                    }

                    @Override
                    public void onResponse(String response, int id) {
                        ContractUtils.Code500(getContext(),response);
                        System.out.println(response+"       求职招聘列表的网络请求");
                        if(response.indexOf("200") != -1){
                            Gson gson = new Gson();
                            qiuZhiZhaoPinLieBiaoEntity = gson.fromJson(response, QiuZhiZhaoPinLieBiaoEntity.class);
                            shouYeZhaoPinAdapter = new ShouYeZhaoPinAdapter(getContext(),qiuZhiZhaoPinLieBiaoEntity.getResponse());
                            LinearLayoutManager manager = new LinearLayoutManager(getContext());
                            recyclerView.setLayoutManager(manager);
                            recyclerView.setAdapter(shouYeZhaoPinAdapter);

                            shouYeZhaoPinAdapter.setShouYeZhaoPinClick(new ShouYeZhaoPinAdapter.ShouYeZhaoPinClick() {
                                @Override
                                public void shouyezhaopinClick(int position) {
                                    Intent intent = new Intent(getContext(), ZhaoPinXiangQingActivity.class);
                                    intent.putExtra("zhaopinid",qiuZhiZhaoPinLieBiaoEntity.getResponse().get(position).getId());
                                    System.out.println(qiuZhiZhaoPinLieBiaoEntity.getResponse().get(position).getId()+"    点击招聘列表行布局传送的招聘id");
                                    startActivity(intent);
                                }
                            });
                        }
                    }
                });
    }

    private void initView() {
//        shouYeZhaoPinAdapter = new ShouYeZhaoPinAdapter(getContext());
//        LinearLayoutManager manager = new LinearLayoutManager(getContext());
//        recyclerView.setLayoutManager(manager);
//        recyclerView.setAdapter(shouYeZhaoPinAdapter);
//
//        shouYeZhaoPinAdapter.setShouYeZhaoPinClick(new ShouYeZhaoPinAdapter.ShouYeZhaoPinClick() {
//            @Override
//            public void shouyezhaopinClick(int position) {
//                Intent intent = new Intent(getContext(), ZhaoPinXiangQingActivity.class);
//                startActivity(intent);
//            }
//        });

    }

}
