package com.wanyangproject.fragment;

import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.wanyangproject.R;
import com.wanyangproject.adapter.FirstAdapter;
import com.wanyangproject.adapter.SecondAdapter;
import com.wanyangproject.entity.NumberEntity;
import com.wanyangproject.entity.ShangJiaShangPinEntity;
import com.wanyangproject.shouye.ShangPinXiangQingActivity;
import com.wanyangproject.shouye.WaiMaiActivity;
import com.wanyangproject.utils.ContractUtils;
import com.zhy.http.okhttp.OkHttpUtils;
import com.zhy.http.okhttp.callback.StringCallback;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.Call;

/**
 * Created by 甜甜 on 2018/7/20.
 */
//  外卖商品
public class ShangPinFragment extends Fragment {

    @BindView(R.id.first_recyclerView)
    RecyclerView firstRecyclerView;
    @BindView(R.id.second_recyclerView)
    RecyclerView secondRecyclerView;
    @BindView(R.id.tv_name)
    TextView tvName;
    //    @BindView(R.id.first_recyclerView)
//    RecyclerView firstRecyclerView;
//    @BindView(R.id.second_recyclerView)
//    RecyclerView secondRecyclerView;
//    @BindView(R.id.image_add)
//    ImageView imageAdd;
//    @BindView(R.id.tv_number)
//    TextView tvNumber;
//    @BindView(R.id.image_jian)
//    ImageView imageJian;
//    @BindView(R.id.image_add1)
//    ImageView imageAdd1;
//    @BindView(R.id.tv_number1)
//    TextView tvNumber1;
//    @BindView(R.id.image_jian1)
//    ImageView imageJian1;
//    @BindView(R.id.image_add2)
//    ImageView imageAdd2;
//    @BindView(R.id.tv_number2)
//    TextView tvNumber2;
//    @BindView(R.id.image_jian2)
//    ImageView imageJian2;
//    @BindView(R.id.tv_chufang)
//    TextView tvChufang;
//    @BindView(R.id.tv_furit)
//    TextView tvFurit;
//    @BindView(R.id.tv_kangshifu)
//    TextView tvKangshifu;
//    @BindView(R.id.tv_cake)
//    TextView tvCake;
//    @BindView(R.id.tv_sushi)
//    TextView tvSushi;
//    @BindView(R.id.image_add3)
//    ImageView imageAdd3;
//    @BindView(R.id.tv_number3)
//    TextView tvNumber3;
//    @BindView(R.id.image_jian3)
//    ImageView imageJian3;
//    @BindView(R.id.image_add33)
//    ImageView imageAdd33;
//    @BindView(R.id.tv_number33)
//    TextView tvNumber33;
//    @BindView(R.id.image_jian33)
//    ImageView imageJian33;
//    @BindView(R.id.image_add333)
//    ImageView imageAdd333;
//    @BindView(R.id.tv_number333)
//    TextView tvNumber333;
//    @BindView(R.id.image_jian333)
//    ImageView imageJian333;
//    @BindView(R.id.image_add4)
//    ImageView imageAdd4;
//    @BindView(R.id.tv_number4)
//    TextView tvNumber4;
//    @BindView(R.id.image_jian4)
//    ImageView imageJian4;
//    @BindView(R.id.image_add44)
//    ImageView imageAdd44;
//    @BindView(R.id.tv_number44)
//    TextView tvNumber44;
//    @BindView(R.id.image_jian44)
//    ImageView imageJian44;
//    @BindView(R.id.image_add444)
//    ImageView imageAdd444;
//    @BindView(R.id.tv_number444)
//    TextView tvNumber444;
//    @BindView(R.id.image_jian444)
//    ImageView imageJian444;
//    @BindView(R.id.image_add5)
//    ImageView imageAdd5;
//    @BindView(R.id.tv_number5)
//    TextView tvNumber5;
//    @BindView(R.id.image_jian5)
//    ImageView imageJian5;
//    @BindView(R.id.image_add55)
//    ImageView imageAdd55;
//    @BindView(R.id.tv_number55)
//    TextView tvNumber55;
//    @BindView(R.id.image_jian55)
//    ImageView imageJian55;
//    @BindView(R.id.image_add555)
//    ImageView imageAdd555;
//    @BindView(R.id.tv_number555)
//    TextView tvNumber555;
//    @BindView(R.id.image_jian555)
//    ImageView imageJian555;
//    @BindView(R.id.relative1)
//    RelativeLayout relative1;
//    @BindView(R.id.relative11)
//    RelativeLayout relative11;
//    @BindView(R.id.relative111)
//    RelativeLayout relative111;
//    @BindView(R.id.relative2)
//    RelativeLayout relative2;
//    @BindView(R.id.relative22)
//    RelativeLayout relative22;
//    @BindView(R.id.relative222)
//    RelativeLayout relative222;
//    @BindView(R.id.relative3)
//    RelativeLayout relative3;
//    @BindView(R.id.relative33)
//    RelativeLayout relative33;
//    @BindView(R.id.relative333)
//    RelativeLayout relative333;
//    @BindView(R.id.relative4)
//    RelativeLayout relative4;
//    @BindView(R.id.relative44)
//    RelativeLayout relative44;
//    @BindView(R.id.relative444)
//    RelativeLayout relative444;
//    @BindView(R.id.relative5)
//    RelativeLayout relative5;
//    @BindView(R.id.relative55)
//    RelativeLayout relative55;
//    @BindView(R.id.relative555)
//    RelativeLayout relative555;
//    @BindView(R.id.image_add22)
//    ImageView imageAdd22;
//    @BindView(R.id.tv_number22)
//    TextView tvNumber22;
//    @BindView(R.id.image_jian22)
//    ImageView imageJian22;
//    @BindView(R.id.image_add222)
//    ImageView imageAdd222;
//    @BindView(R.id.tv_number222)
//    TextView tvNumber222;
//    @BindView(R.id.image_jian222)
//    ImageView imageJian222;
//    @BindView(R.id.image_add2222)
//    ImageView imageAdd2222;
//    @BindView(R.id.tv_number2222)
//    TextView tvNumber2222;
//    @BindView(R.id.image_jian2222)
//    ImageView imageJian2222;
    private FirstAdapter firstAdapter;
    private SecondAdapter secondAdapter;
    private LinearLayout shangpin1, shangpin2, shangpin3, shangpin4, shangpin5;
    private ShangJiaShangPinEntity shangJiaShangPinEntity;
    //    private ImageView image_add22, image_jian22, image_add222, image_jian222, image_add2222, image_jian2222;
//    private TextView tv_number22, tv_number222, tv_number2222;
    private String token;
    private String shopid;
    private WaiMaiActivity waimai;
    private NetWork netWork;
    private String name;
    private String finish;
    private int kucun;
    private  int position;
    private String bidian;


    public String getBidian() {
        return bidian;
    }

    public void setBidian(String bidian) {
        this.bidian = bidian;
    }

    public WaiMaiActivity getWaimai() {
        return waimai;
    }

    public void setWaimai(WaiMaiActivity waimai) {
        this.waimai = waimai;
    }


    public String getShopid() {
        return shopid;
    }

    public void setShopid(String shopid) {
        this.shopid = shopid;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.shangpin_fragment, container, false);
        ButterKnife.bind(this, view);

        SharedPreferences sharedPreferences = getContext().getSharedPreferences("token", Context.MODE_PRIVATE);
        token = sharedPreferences.getString("token", "token");
        System.out.println(token + "     token");



        //        商家商品的网络请求
        initShangJiaShangPinHttp();






        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("name");
        netWork = new NetWork();
        getContext().registerReceiver(netWork, intentFilter);


        IntentFilter intentFilter4 = new IntentFilter();
        intentFilter4.addAction("finish");
        netWork = new NetWork();
        getContext().registerReceiver(netWork, intentFilter4);




//        IntentFilter intentFilter5 = new IntentFilter();
//        intentFilter5.addAction("kucun");
//        netWork = new NetWork();
//        getContext().registerReceiver(netWork, intentFilter5);
//
//
//




//        shangpin1 = view.findViewById(R.id.shangpin1);
//        shangpin2 = view.findViewById(R.id.shangpin2);
//        shangpin3 = view.findViewById(R.id.shangpin3);
//        shangpin4 = view.findViewById(R.id.shangpin4);
//        shangpin5 = view.findViewById(R.id.shangpin5);

        return view;
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        getContext().unregisterReceiver(netWork);
    }

    class NetWork extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {

            //            结束页面
            finish = intent.getStringExtra("finish");
            if(finish != null) {
                NumberEntity.lists.clear();
                secondAdapter.notifyDataSetChanged();
                return;
            }



//            kucun = intent.getIntExtra("kucun",0);
//            position = intent.getIntExtra("position", 0);
//
//            System.out.println(kucun+"           接受广播库存");
//            System.out.println(position+"        下标接受广播库存");
//





            name = intent.getStringExtra("name");
            if(name != null){
                tvName.setText(name);
            }
//            Toast.makeText(context, wxopenid + wxnickname + wxheadimgurl, Toast.LENGTH_SHORT).show();

        }
    }



    @OnClick({R.id.first_recyclerView, R.id.second_recyclerView})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.first_recyclerView:
                break;
            case R.id.second_recyclerView:
                break;
        }
    }


    @Override
    public void onResume() {
        super.onResume();
        if(NumberEntity.lists.size() > 0){
            secondAdapter.notifyDataSetChanged();
        }
    }

    //    商家商品的网络请求
//    tiantian
    private void initShangJiaShangPinHttp() {
        System.out.println(shopid + "        shaopid");
        final ProgressDialog progressDialog = new ProgressDialog(getContext());
        progressDialog.setTitle("提示");
        progressDialog.setMessage("请等待...");
        progressDialog.setCancelable(false);
        progressDialog.show();
        OkHttpUtils.post()
                .url(ContractUtils.LOGIN_URL + "Life/shop")
                .addHeader("token", ContractUtils.getTOKEN(getContext()))
                .addParams("parkId", ContractUtils.getParkId(getContext()))
                .addParams("shopId", shopid)
                .build()
                .execute(new StringCallback() {
                    @Override
                    public void onError(Call call, Exception e, int id) {
                        progressDialog.dismiss();

                    }

                    @Override
                    public void onResponse(String response, final int id) {
                        progressDialog.dismiss();
                        ContractUtils.Code500(getContext(), response);
                        System.out.println(response + "        商家商品的网络请求");
                        if(response.indexOf("200") != -1){
                            Gson gson = new Gson();
                            shangJiaShangPinEntity = gson.fromJson(response, ShangJiaShangPinEntity.class);
                            if (shangJiaShangPinEntity.getResponse().getGoods().size() > 0) {
                                shangJiaShangPinEntity.getResponse().getGoods().get(0).setOk(true);
                            }


                            firstAdapter = new FirstAdapter(getContext(), shangJiaShangPinEntity.getResponse().getGoods());
                            System.out.println(shangJiaShangPinEntity.getResponse().getGoods() + "                aaaaaaaaaaa");
                            LinearLayoutManager manager = new LinearLayoutManager(getContext());
                            firstRecyclerView.setLayoutManager(manager);
                            firstRecyclerView.setAdapter(firstAdapter);
                            firstAdapter.setFirstClick(new FirstAdapter.FirstClick() {
                                @Override
                                public void firstClick(int position) {
                                    secondAdapter.notifyDataSetChanged();
                                }
                            });


                            secondAdapter = new SecondAdapter(getContext(), shangJiaShangPinEntity.getResponse().getGoods(), shangJiaShangPinEntity.getResponse().getShop(), waimai);
                            System.out.println(shangJiaShangPinEntity.getResponse().getGoods() + "                bbbbbbbbbbbbbbbbbbb");
                            LinearLayoutManager manager1 = new LinearLayoutManager(getContext());
                            secondRecyclerView.setLayoutManager(manager1);
                            secondRecyclerView.setAdapter(secondAdapter);

                            secondAdapter.setShangPinXiangQingClick(new SecondAdapter.ShangPinXiangQingClick() {
                                @Override
                                public void shangpinxiangqingClick(int position, String peisongfei, String qisongjia, String shangpinid,String iii) {
                                    Intent intent = new Intent(getContext(), ShangPinXiangQingActivity.class);
                                    intent.putExtra("peisongfei", peisongfei);
                                    intent.putExtra("qisongjia", qisongjia);
                                    intent.putExtra("shangjiaid", shopid);
                                    intent.putExtra("shangpinid", shangpinid);
                                    intent.putExtra("kucun",iii);
                                    intent.putExtra("bidian",bidian);
                                    intent.putExtra("name",shangJiaShangPinEntity.getResponse().getShop().getShop().getNickname());
//                                intent.putExtra("kucun",iii);
                                    System.out.println(iii+"         库存");
                                    System.out.println(shangpinid + "             商品id");
                                    System.out.println(shopid + "   shangjiaid");
                                    System.out.println(peisongfei + "    peipei");
                                    System.out.println(qisongjia + "      qiqiqi");
                                    startActivity(intent);
                                }
                            });

                        }


                    }
                });
    }



//
//    @OnClick({R.id.tv_chufang, R.id.tv_furit, R.id.tv_kangshifu, R.id.tv_cake, R.id.tv_sushi, R.id.image_add, R.id.tv_number, R.id.image_jian,
//            R.id.image_add1, R.id.tv_number1, R.id.image_jian1, R.id.image_add2, R.id.tv_number2, R.id.image_jian2,
//            R.id.image_add22, R.id.tv_number22, R.id.image_jian22, R.id.image_add222, R.id.tv_number222, R.id.image_jian222, R.id.image_add2222, R.id.tv_number2222, R.id.image_jian2222,
//            R.id.image_add3, R.id.tv_number3, R.id.image_jian3, R.id.image_add33, R.id.tv_number33, R.id.image_jian33, R.id.image_add333, R.id.tv_number333, R.id.image_jian333,
//            R.id.image_add4, R.id.tv_number4, R.id.image_jian4, R.id.image_add44, R.id.tv_number44, R.id.image_jian44, R.id.image_add444, R.id.tv_number444, R.id.image_jian444,
//            R.id.image_add5, R.id.tv_number5, R.id.image_jian5, R.id.image_add55, R.id.tv_number55, R.id.image_jian55, R.id.image_add555, R.id.tv_number555, R.id.image_jian555,
//            R.id.relative1, R.id.relative11, R.id.relative111, R.id.relative2, R.id.relative22, R.id.relative222, R.id.relative3, R.id.relative33, R.id.relative333,
//            R.id.relative4, R.id.relative44, R.id.relative444, R.id.relative5, R.id.relative55, R.id.relative555})
//    public void onViewClicked(View view) {
//        switch (view.getId()) {
//            case R.id.tv_chufang:
//                tvChufang.setBackgroundResource(R.drawable.shape_beijing_white);
//                tvFurit.setBackgroundResource(R.drawable.shape_beijing);
//                tvCake.setBackgroundResource(R.drawable.shape_beijing);
//                tvKangshifu.setBackgroundResource(R.drawable.shape_beijing);
//                tvSushi.setBackgroundResource(R.drawable.shape_beijing);
//                shangpin1.setVisibility(View.VISIBLE);
//                shangpin2.setVisibility(View.GONE);
//                shangpin3.setVisibility(View.GONE);
//                shangpin4.setVisibility(View.GONE);
//                shangpin5.setVisibility(View.GONE);
//
//                break;
//            case R.id.tv_furit:
//                tvFurit.setBackgroundResource(R.drawable.shape_beijing_white);
//                tvChufang.setBackgroundResource(R.drawable.shape_beijing);
//                tvCake.setBackgroundResource(R.drawable.shape_beijing);
//                tvKangshifu.setBackgroundResource(R.drawable.shape_beijing);
//                tvSushi.setBackgroundResource(R.drawable.shape_beijing);
//                shangpin1.setVisibility(View.GONE);
//                shangpin2.setVisibility(View.VISIBLE);
//                shangpin3.setVisibility(View.GONE);
//                shangpin4.setVisibility(View.GONE);
//                shangpin5.setVisibility(View.GONE);
//                break;
//            case R.id.tv_kangshifu:
//                tvKangshifu.setBackgroundResource(R.drawable.shape_beijing_white);
//                tvChufang.setBackgroundResource(R.drawable.shape_beijing);
//                tvCake.setBackgroundResource(R.drawable.shape_beijing);
//                tvSushi.setBackgroundResource(R.drawable.shape_beijing);
//                tvFurit.setBackgroundResource(R.drawable.shape_beijing);
//                shangpin1.setVisibility(View.GONE);
//                shangpin2.setVisibility(View.GONE);
//                shangpin3.setVisibility(View.VISIBLE);
//                shangpin4.setVisibility(View.GONE);
//                shangpin5.setVisibility(View.GONE);
//                break;
//            case R.id.tv_cake:
//                tvCake.setBackgroundResource(R.drawable.shape_beijing_white);
//                tvChufang.setBackgroundResource(R.drawable.shape_beijing);
//                tvFurit.setBackgroundResource(R.drawable.shape_beijing);
//                tvKangshifu.setBackgroundResource(R.drawable.shape_beijing);
//                tvSushi.setBackgroundResource(R.drawable.shape_beijing);
//                shangpin1.setVisibility(View.GONE);
//                shangpin2.setVisibility(View.GONE);
//                shangpin3.setVisibility(View.GONE);
//                shangpin4.setVisibility(View.VISIBLE);
//                shangpin5.setVisibility(View.GONE);
//                break;
//            case R.id.tv_sushi:
//                tvSushi.setBackgroundResource(R.drawable.shape_beijing_white);
//                tvFurit.setBackgroundResource(R.drawable.shape_beijing);
//                tvChufang.setBackgroundResource(R.drawable.shape_beijing);
//                tvKangshifu.setBackgroundResource(R.drawable.shape_beijing);
//                tvCake.setBackgroundResource(R.drawable.shape_beijing);
//                shangpin1.setVisibility(View.GONE);
//                shangpin2.setVisibility(View.GONE);
//                shangpin3.setVisibility(View.GONE);
//                shangpin4.setVisibility(View.GONE);
//                shangpin5.setVisibility(View.VISIBLE);
//                break;
//            case R.id.image_add:
//                int i = Integer.parseInt(tvNumber.getText().toString());
//                i++;
//                tvNumber.setText(i + "");
//                break;
//            case R.id.tv_number:
//                break;
//            case R.id.image_jian:
//                int ii = Integer.parseInt(tvNumber.getText().toString());
//                ii--;
//                if (ii <= 0) {
//                    ii = 0;
//                }
//                tvNumber.setText(ii + "");
//                break;
//            case R.id.image_add1:
//                int onedd = Integer.parseInt(tvNumber1.getText().toString());
//                onedd++;
//                tvNumber1.setText(onedd + "");
//                break;
//            case R.id.tv_number1:
//                break;
//            case R.id.image_jian1:
//                int onejian = Integer.parseInt(tvNumber1.getText().toString());
//                onejian--;
//                if (onejian <= 0) {
//                    onejian = 0;
//                }
//                tvNumber1.setText(onejian + "");
//                break;
//            case R.id.image_add2:
//                int twoadd = Integer.parseInt(tvNumber2.getText().toString());
//                twoadd++;
//                tvNumber2.setText(twoadd + "");
//                break;
//            case R.id.tv_number2:
//                break;
//            case R.id.image_jian2:
//                int twojian = Integer.parseInt(tvNumber2.getText().toString());
//                twojian--;
//                if (twojian <= 0) {
//                    twojian = 0;
//                }
//                tvNumber2.setText(twojian + "");
//                break;
//            case R.id.image_add22:
//                int add22 = Integer.parseInt(tvNumber22.getText().toString());
//                add22++;
//                tvNumber22.setText(add22 + "");
//                break;
//            case R.id.image_jian22:
//                int jian22 = Integer.parseInt(tvNumber22.getText().toString());
//                jian22--;
//                if (jian22 <= 0) {
//                    jian22 = 0;
//                }
//                tvNumber22.setText(jian22 + "");
//                break;
//            case R.id.image_add222:
//                int add222 = Integer.parseInt(tvNumber222.getText().toString());
//                add222++;
//                tvNumber222.setText(add222 + "");
//                break;
//            case R.id.image_jian222:
//                int jian222 = Integer.parseInt(tvNumber222.getText().toString());
//                jian222--;
//                if (jian222 <= 0) {
//                    jian222 = 0;
//                }
//                tvNumber222.setText(jian222 + "");
//                break;
//            case R.id.image_add2222:
//                int add2222 = Integer.parseInt(tvNumber2222.getText().toString());
//                add2222++;
//                tvNumber2222.setText(add2222 + "");
//                break;
//            case R.id.image_jian2222:
//                int jian2222 = Integer.parseInt(tvNumber2222.getText().toString());
//                jian2222--;
//                if (jian2222 <= 0) {
//                    jian2222 = 0;
//                }
//                tvNumber2222.setText(jian2222 + "");
//                break;
//            case R.id.image_add3:
//                int add3 = Integer.parseInt(tvNumber3.getText().toString());
//                add3++;
//                tvNumber3.setText(add3 + "");
//                break;
//            case R.id.tv_number3:
//                break;
//            case R.id.image_jian3:
//                int jian3 = Integer.parseInt(tvNumber3.getText().toString());
//                jian3--;
//                if (jian3 <= 0) {
//                    jian3 = 0;
//                }
//                tvNumber3.setText(jian3 + "");
//                break;
//            case R.id.image_add33:
//                int add33 = Integer.parseInt(tvNumber33.getText().toString());
//                add33++;
//                tvNumber33.setText(add33 + "");
//                break;
//            case R.id.tv_number33:
//                break;
//            case R.id.image_jian33:
//                int jian33 = Integer.parseInt(tvNumber33.getText().toString());
//                jian33--;
//                if (jian33 <= 0) {
//                    jian33 = 0;
//                }
//                tvNumber33.setText(jian33 + "");
//                break;
//            case R.id.image_add333:
//                int add333 = Integer.parseInt(tvNumber333.getText().toString());
//                add333++;
//                tvNumber333.setText(add333 + "");
//                break;
//            case R.id.tv_number333:
//                break;
//            case R.id.image_jian333:
//                int jian333 = Integer.parseInt(tvNumber333.getText().toString());
//                jian333--;
//                if (jian333 <= 0) {
//                    jian333 = 0;
//                }
//                tvNumber333.setText(jian333 + "");
//                break;
//            case R.id.image_add4:
//                int add4 = Integer.parseInt(tvNumber4.getText().toString());
//                add4++;
//                tvNumber4.setText(add4 + "");
//                break;
//            case R.id.tv_number4:
//                break;
//            case R.id.image_jian4:
//                int jian4 = Integer.parseInt(tvNumber4.getText().toString());
//                jian4--;
//                if (jian4 <= 0) {
//                    jian4 = 0;
//                }
//                tvNumber4.setText(jian4 + "");
//                break;
//            case R.id.image_add44:
//                int add44 = Integer.parseInt(tvNumber44.getText().toString());
//                add44++;
//                tvNumber44.setText(add44 + "");
//                break;
//            case R.id.tv_number44:
//                break;
//            case R.id.image_jian44:
//                int jian44 = Integer.parseInt(tvNumber44.getText().toString());
//                jian44--;
//                if (jian44 <= 0) {
//                    jian44 = 0;
//                }
//                tvNumber44.setText(jian44 + "");
//                break;
//            case R.id.image_add444:
//                int add444 = Integer.parseInt(tvNumber444.getText().toString());
//                add444++;
//                tvNumber444.setText(add444 + "");
//                break;
//            case R.id.tv_number444:
//                break;
//            case R.id.image_jian444:
//                int jian444 = Integer.parseInt(tvNumber444.getText().toString());
//                jian444--;
//                if (jian444 <= 0) {
//                    jian444 = 0;
//                }
//                tvNumber444.setText(jian444 + "");
//                break;
//            case R.id.image_add5:
//                int add5 = Integer.parseInt(tvNumber5.getText().toString());
//                add5++;
//                tvNumber5.setText(add5 + "");
//                break;
//            case R.id.tv_number5:
//                break;
//            case R.id.image_jian5:
//                int jian5 = Integer.parseInt(tvNumber5.getText().toString());
//                jian5--;
//                if (jian5 <= 0) {
//                    jian5 = 0;
//                }
//                tvNumber5.setText(jian5 + "");
//                break;
//            case R.id.image_add55:
//                int add55 = Integer.parseInt(tvNumber55.getText().toString());
//                add55++;
//                tvNumber55.setText(add55 + "");
//                break;
//            case R.id.tv_number55:
//                break;
//            case R.id.image_jian55:
//                int jian55 = Integer.parseInt(tvNumber55.getText().toString());
//                jian55--;
//                if (jian55 <= 0) {
//                    jian55 = 0;
//                }
//                tvNumber55.setText(jian55 + "");
//                break;
//            case R.id.image_add555:
//                int add555 = Integer.parseInt(tvNumber555.getText().toString());
//                add555++;
//                tvNumber555.setText(add555 + "");
//                break;
//            case R.id.tv_number555:
//                break;
//            case R.id.image_jian555:
//                int jian555 = Integer.parseInt(tvNumber555.getText().toString());
//                jian555--;
//                if (jian555 <= 0) {
//                    jian555 = 0;
//                }
//                tvNumber555.setText(jian555 + "");
//                break;
//            case R.id.relative1:
//                Intent intent = new Intent(getContext(), ShangPinXiangQingActivity.class);
//                startActivity(intent);
//                break;
//            case R.id.relative11:
//                Intent intent11 = new Intent(getContext(), ShangPinXiangQingActivity.class);
//                startActivity(intent11);
//                break;
//            case R.id.relative111:
//                Intent intent111 = new Intent(getContext(), ShangPinXiangQingActivity.class);
//                startActivity(intent111);
//                break;
//            case R.id.relative2:
//                Intent intent2 = new Intent(getContext(), ShangPinXiangQingActivity.class);
//                startActivity(intent2);
//                break;
//            case R.id.relative22:
//                Intent intent22 = new Intent(getContext(), ShangPinXiangQingActivity.class);
//                startActivity(intent22);
//                break;
//            case R.id.relative222:
//                Intent intent222 = new Intent(getContext(), ShangPinXiangQingActivity.class);
//                startActivity(intent222);
//                break;
//            case R.id.relative3:
//                Intent intent3 = new Intent(getContext(), ShangPinXiangQingActivity.class);
//                startActivity(intent3);
//                break;
//            case R.id.relative33:
//                Intent intent33 = new Intent(getContext(), ShangPinXiangQingActivity.class);
//                startActivity(intent33);
//                break;
//            case R.id.relative333:
//                Intent intent333 = new Intent(getContext(), ShangPinXiangQingActivity.class);
//                startActivity(intent333);
//                break;
//            case R.id.relative4:
//                Intent intent4 = new Intent(getContext(), ShangPinXiangQingActivity.class);
//                startActivity(intent4);
//                break;
//            case R.id.relative44:
//                Intent intent44 = new Intent(getContext(), ShangPinXiangQingActivity.class);
//                startActivity(intent44);
//                break;
//            case R.id.relative444:
//                Intent intent444 = new Intent(getContext(), ShangPinXiangQingActivity.class);
//                startActivity(intent444);
//                break;
//            case R.id.relative5:
//                Intent intent5 = new Intent(getContext(), ShangPinXiangQingActivity.class);
//                startActivity(intent5);
//                break;
//            case R.id.relative55:
//                Intent intent55 = new Intent(getContext(), ShangPinXiangQingActivity.class);
//                startActivity(intent55);
//                break;
//            case R.id.relative555:
//                Intent intent555 = new Intent(getContext(), ShangPinXiangQingActivity.class);
//                startActivity(intent555);
//                break;
//
//        }
//    }


}
