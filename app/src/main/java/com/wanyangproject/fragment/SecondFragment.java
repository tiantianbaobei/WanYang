package com.wanyangproject.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.gson.Gson;
import com.wanyangproject.R;
import com.wanyangproject.adapter.ShouYeZiXunAdapter;
import com.wanyangproject.entity.LunTanLieBiaoEntity;
import com.wanyangproject.entity.ShouYeYuanQuZiXunEntity;
import com.wanyangproject.shouye.ZiXunXiangQingActivity;
import com.wanyangproject.utils.ContractUtils;
import com.zhy.http.okhttp.OkHttpUtils;
import com.zhy.http.okhttp.callback.StringCallback;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import okhttp3.Call;

/**
 * Created by 甜甜 on 2018/7/18.
 */

public class SecondFragment extends Fragment {


    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    private ShouYeYuanQuZiXunEntity shouYeYuanQuZiXunEntity;
    private ShouYeZiXunAdapter shouYeZiXunAdapter;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_second, container, false);
        ButterKnife.bind(this, view);

//        园区资讯的网络请求
        initYuanQuZiXunHttp();

        return view;
    }

//    园区资讯的网络请求
    private void initYuanQuZiXunHttp() {
        OkHttpUtils.post()
                .url(ContractUtils.LOGIN_URL+"news/getIndexNews")
                .addHeader("token",ContractUtils.getTOKEN(getContext()))
                .addParams("parkId",ContractUtils.getParkId(getContext()))
                .addParams("isIndex","0")
                .build()
                .execute(new StringCallback() {
                    @Override
                    public void onError(Call call, Exception e, int id) {

                    }

                    @Override
                    public void onResponse(String response, int id) {
                        ContractUtils.Code500(getContext(),response);
                        System.out.println(response+"           园区资讯的网络请求");
                        Gson gson = new Gson();
                        shouYeYuanQuZiXunEntity = gson.fromJson(response, ShouYeYuanQuZiXunEntity.class);
                        if(response.indexOf("200") != -1){
                            shouYeZiXunAdapter = new ShouYeZiXunAdapter(getContext(), shouYeYuanQuZiXunEntity.getResponse());
                            LinearLayoutManager manager = new LinearLayoutManager(getContext());
                            recyclerView.setLayoutManager(manager);
                            recyclerView.setAdapter(shouYeZiXunAdapter);

                            shouYeZiXunAdapter.setShouYeZiXunClick(new ShouYeZiXunAdapter.ShouYeZiXunClick() {
                                @Override
                                public void shouyezixunClick(int position, String id) {
                                    Intent intent = new Intent(getContext(), ZiXunXiangQingActivity.class);
                                    intent.putExtra("id",id);
                                    startActivity(intent);
                                }
                            });

                        }
                    }
                });
    }

}
