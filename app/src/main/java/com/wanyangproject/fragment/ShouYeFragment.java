package com.wanyangproject.fragment;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ViewFlipper;

import com.bumptech.glide.Glide;
import com.google.gson.Gson;
import com.umeng.message.PushAgent;
import com.umeng.message.UTrack;
import com.wanyangproject.R;
import com.wanyangproject.activity.HomeActivity;
import com.wanyangproject.activity.RegisterActivity;
import com.wanyangproject.activity.WenZhangXiangQingActivity;
import com.wanyangproject.adapter.BannerAdapter;
import com.wanyangproject.adapter.ShouYeAdapter;
import com.wanyangproject.adapter.ShouYeZiXunAdapter;
import com.wanyangproject.entity.SheZhiYuanQuNameEntity;
import com.wanyangproject.entity.ShouYeBannerEntity;
import com.wanyangproject.entity.ShouYeTuiJianQiYeEntity;
import com.wanyangproject.entity.ShouYeYuanQuZiXunEntity;
import com.wanyangproject.entity.UpDataEntity;
import com.wanyangproject.entity.XiTongXiaoXiEntity;
import com.wanyangproject.entity.XiTongXiaoXiWeiDuGeShuEntity;
import com.wanyangproject.fuwuactivity.FangKeDengJiActivity;
import com.wanyangproject.fuwuactivity.FuWuMessageActivity;
import com.wanyangproject.fuwuactivity.YuanQuTongZhiActivity;
import com.wanyangproject.my.MyBangDingActivity;
import com.wanyangproject.my.MyShouCangActivity;
import com.wanyangproject.my.QuanBuActivity;
import com.wanyangproject.my.RuZhuShenQingActivity;
import com.wanyangproject.my.SettingActivity;
import com.wanyangproject.shouye.ChanYeJinRongActivity;
import com.wanyangproject.shouye.QiYeXiangQingActivity;
import com.wanyangproject.shouye.QieHuanYuanQuActivity;
import com.wanyangproject.shouye.QiuZhiZhaoPinActivity;
import com.wanyangproject.shouye.ShenFenBangDingActivity;
import com.wanyangproject.shouye.ShouHuoXinXiActivity;
import com.wanyangproject.shouye.XuanZeYuanQuActivity;
import com.wanyangproject.shouye.YuanQuQiYeActivity;
import com.wanyangproject.shouye.YuanQuShengHuoActivity;
import com.wanyangproject.shouye.YuanQuShouCeActivity;
import com.wanyangproject.shouye.YuanQuZhaoShangActivity;
import com.wanyangproject.shouye.ZhengCeFaGuiActivity;
import com.wanyangproject.shouye.ZhiNengZhiZaoActivity;
import com.wanyangproject.shouye.ZiXunXiangQingActivity;
import com.wanyangproject.utils.ContractUtils;
import com.wanyangproject.utils.FuWuTiaoZhuFragment;
import com.wanyangproject.utils.NotificationsUtils;
import com.wanyangproject.utils.TiaoZhuanFragment;
import com.wanyangproject.utils.UIUtil;
import com.wanyangproject.widget.suspensionindexbar.IndexBar;
import com.wanyangproject.zxing.activity.CaptureActivity;
import com.youth.banner.loader.ImageLoader;
import com.zhy.http.okhttp.OkHttpUtils;
import com.zhy.http.okhttp.callback.StringCallback;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.Call;

import static android.R.attr.padding;
import static com.wanyangproject.zxing.activity.CaptureActivity.REQ_PERM_CAMERA;
import static com.wanyangproject.zxing.activity.CaptureActivity.REQ_QR_CODE;


/**
 * Created by 甜甜 on 2018/7/11.
 */

//   首页
public class ShouYeFragment extends Fragment implements ViewPager.OnPageChangeListener,android.view.GestureDetector.OnGestureListener {

    //    @BindView(R.id.banner)
//    Banner banner;
    @BindView(R.id.tv_yuanquqiye)
    TextView tvYuanquqiye;
    @BindView(R.id.tv_yuanqushenghuo)
    TextView tvYuanqushenghuo;
    @BindView(R.id.tv_yuanquzhaoshang)
    TextView tvYuanquzhaoshang;
    @BindView(R.id.tv_qiuzhizhaopin)
    TextView tvQiuzhizhaopin;
    @BindView(R.id.tv_zhengcefagui)
    TextView tvZhengcefagui;
    @BindView(R.id.tv_chanyejinrong)
    TextView tvChanyejinrong;
    @BindView(R.id.tv_zhinengzhizao)
    TextView tvZhinengzhizao;
    @BindView(R.id.image_xiaoxi)
    ImageView imageXiaoxi;
    @BindView(R.id.image_saoyisao)
    ImageView imageSaoyisao;
    //    @BindView(R.id.img_one)
//    ImageView imgOne;
//    @BindView(R.id.img_two)
//    ImageView imgTwo;
//    @BindView(R.id.img_three)
//    ImageView imgThree;
    @BindView(R.id.viewflipper)
    ViewFlipper viewflipper;
    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    //    @BindView(R.id.viewPager)
//    ViewPager viewPager;
    @BindView(R.id.relative_banner)
    RelativeLayout relativeBanner;
    @BindView(R.id.tv_yuanqushouce)
    TextView tvYuanqushouce;
    @BindView(R.id.linear_shouye_one)
    LinearLayout linearShouyeOne;
    @BindView(R.id.linear_shouye_two)
    LinearLayout linearShouyeTwo;
    @BindView(R.id.relative_tubiao)
    RelativeLayout relativeTubiao;
    @BindView(R.id.tv_tuijianqiye)
    ImageView tvTuijianqiye;
    @BindView(R.id.view)
    View view;
    @BindView(R.id.tuijian_qiye)
    RelativeLayout tuijianQiye;
    @BindView(R.id.tv_yuanquzixun)
    TextView tvYuanquzixun;
    @BindView(R.id.relative_zixun)
    RelativeLayout relativeZixun;
    @BindView(R.id.relative)
    RelativeLayout relative;
    @BindView(R.id.scorllView)
    ScrollView scorllView;
    @BindView(R.id.shouye)
    RelativeLayout shouye;
    @BindView(R.id.zimu)
    IndexBar zimu;
    @BindView(R.id.recyclerView_banner)
    RecyclerView recyclerViewBanner;
    @BindView(R.id.btn_qiehuan_yuanqu)
    TextView btnQiehuanYuanqu;
    @BindView(R.id.tv_xiaoxi_number)
    TextView tvXiaoxiNumber;
    //    @BindView(R.id.banner)
//    Banner banner;
    private ShouYeAdapter shouyeAdapter;
    private ViewPager viewPager;
    private ShouYeZiXunAdapter shouYeZiXunAdapter;
    private String token;
    private ShouYeTuiJianQiYeEntity shouYeTuiJianQiYeEntity;
    private float indication_self_margin_percent = 1f;
    private float indication_self_percent;
    private BannerAdapter1 adapter1;
    private NetWork netWork;
    private String parkId;
    private String yuanqu;
    private XiTongXiaoXiEntity xiTongXiaoXiEntity;
    private XiTongXiaoXiWeiDuGeShuEntity xiTongXiaoXiWeiDuGeShuEntity;
    private  ShouYeBannerEntity shouYeBannerEntity;
    private View viewById;
    private int pos;//下标
    private int posgundong;
    private String yuanquname;
    private Boolean tiaozhuan = false;
    private TiaoZhuanFragment tiaoZhuanFragment;
    private FuWuTiaoZhuFragment fuWuTiaoZhuFragment;
    private LinearLayoutManager manager;
    private int xiabiao;
    private PackageInfo info;
    private  String lujing;



    private List<View> list = new ArrayList<>();
    private List<String> contentList = new ArrayList<>();

    private Boolean isOk = true;

    public void setFuWuTiaoZhuFragment(FuWuTiaoZhuFragment fuWuTiaoZhuFragment) {
        this.fuWuTiaoZhuFragment = fuWuTiaoZhuFragment;
    }

    public void setTiaoZhuanFragment(TiaoZhuanFragment tiaoZhuanFragment) {
        this.tiaoZhuanFragment = tiaoZhuanFragment;
    }

    private Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            switch (msg.what) {
                case 1:
                    viewPager.setCurrentItem(msg.arg1);
                    break;
                case 2:
                    viewPager.setCurrentItem(msg.arg1);
                    break;
            }
            if (adapter1 != null) {
                adapter1.notifyDataSetChanged();
            }
        }
    };


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_shouye, container, false);
        ButterKnife.bind(this, view);


        if(NotificationsUtils.isNotificationEnabled(getContext())){

        }else{
            final AlertDialog dialog = new AlertDialog.Builder(getContext()).create();
            dialog.show();
            dialog.getWindow().setContentView(R.layout.alertdialog_tonfzhi);
            dialog.getWindow().setBackgroundDrawableResource(R.drawable.shape_zhuce);
            WindowManager windowManager = getActivity().getWindowManager();
            Display defaultDisplay = windowManager.getDefaultDisplay();
            WindowManager.LayoutParams attributes = dialog.getWindow().getAttributes();
            attributes.width= (int) (defaultDisplay.getWidth()*0.8);
            dialog.getWindow().setAttributes(attributes);
            dialog.getWindow().findViewById(R.id.tv_zanbuno).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {//暂不更新
                    dialog.dismiss();
                }
            });
            dialog.getWindow().findViewById(R.id.tv_setting).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {//更新
                    // TODO Auto-generated method stub
                    // 6.0以上系统才可以判断权限
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.BASE) {
                        // 进入设置系统应用权限界面
                        Intent intent = new Intent(Settings.ACTION_SETTINGS);
                        startActivity(intent);
                        dialog.dismiss();
                        return;
                    } else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {// 运行系统在5.x环境使用
                        // 进入设置系统应用权限界面
                        Intent intent = new Intent(Settings.ACTION_SETTINGS);
                        startActivity(intent);
                        dialog.dismiss();
                        return;
                    }
                    return;
                }
            });
        }


        //获取包管理者对象
        PackageManager pm = getActivity().getPackageManager();
        //获取包的详细信息
        try {
//            PackageInfo info = pm.getPackageInfo(getActivity().getPackageName(), 0);
            info = pm.getPackageInfo(getActivity().getPackageName(), 0);
            //获取版本号和版本名称
            System.out.println("版本号："+info.versionCode);
            System.out.println("版本名称："+info.versionName);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }


//        App更新
        initUpdata();



        SharedPreferences sharedPreferences = getContext().getSharedPreferences("token", Context.MODE_PRIVATE);
        token = sharedPreferences.getString("token", "token");
        System.out.println(token + "     token");

        viewPager = view.findViewById(R.id.viewPager);






        PushAgent mPushAgent = PushAgent.getInstance(getContext());
        mPushAgent.setAlias(ContractUtils.getPhone(getContext()), "00", new UTrack.ICallBack() {
            @Override
            public void onMessage(boolean isSuccess, String message) {
                System.out.println(isSuccess+"           isSuccess");
                System.out.println(message+"            message");
            }
        });



//        View viewOne = LayoutInflater.from(getContext()).inflate(R.layout.item_one, null);
//        View viewTwo = LayoutInflater.from(getContext()).inflate(R.layout.item_two, null);
//        View viewThree = LayoutInflater.from(getContext()).inflate(R.layout.item_three, null);
//
//        list.add(viewOne);
//        list.add(viewTwo);
//        list.add(viewThree);

//        MyAdapter adapter = new MyAdapter(list);
//        viewPager.setAdapter(adapter);


//        默认园区
        btnQiehuanYuanqu.setText(ContractUtils.getYuanquname(getContext()));

        //        获取园区id接收广播
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("yuanquid");
        netWork = new NetWork();
        getActivity().registerReceiver(netWork, intentFilter);


        initThread();

        viewPager.addOnPageChangeListener(this);
        initView();
//        首页获取推荐企业的网络请求
        initTuiJianQuYeHttp();
//        首页园区资讯的网络请求
        initYuanQuZiXunHttp();
//        首页banner图的网络请求
        initBannerHttp();
//        首页系统消息的网络请求
        initXiTongXiaoXiHttp();

//        用户未读消息的个数的网络请求
        initWeiDuXiaoXiHttp();

//        首页总banner网络请求
        initShouYeBannerHttp();

        return view;
    }














    //    设置园区接口
    private void initSheZhiYuanQuHttp() {
        OkHttpUtils.post()
                .url(ContractUtils.LOGIN_URL+"user/setDefaultPark")
                .addHeader("token",ContractUtils.getTOKEN(getContext()))
                .addParams("parkId",ContractUtils.getParkId(getContext()))
                .build()
                .execute(new StringCallback() {
                    @Override
                    public void onError(Call call, Exception e, int id) {

                    }

                    @Override
                    public void onResponse(String response, int id) {
                        System.out.println(response+"               设置园区接口");
                        if(response.indexOf("200") != -1){
                            Gson gson = new Gson();
                            SheZhiYuanQuNameEntity sheZhiYuanQuNameEntity = gson.fromJson(response, SheZhiYuanQuNameEntity.class);
                            ContractUtils.setParkId(sheZhiYuanQuNameEntity.getResponse().getParkInfo().getParkId());
                            ContractUtils.setPhone(sheZhiYuanQuNameEntity.getResponse().getUsername());
                            ContractUtils.setYuanquname(sheZhiYuanQuNameEntity.getResponse().getParkInfo().getParkName());
                            ContractUtils.setTypeId(sheZhiYuanQuNameEntity.getResponse().getTypeId());
                            ContractUtils.setTypeId22(sheZhiYuanQuNameEntity.getResponse().getTypeId2());
                        }
                    }
                });
    }







//    protected void requestPermission() {
//        // TODO Auto-generated method stub
//        // 6.0以上系统才可以判断权限
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.BASE) {
//            // 进入设置系统应用权限界面
//            Intent intent = new Intent(Settings.ACTION_SETTINGS);
//            startActivity(intent);
//            return;
//        } else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {// 运行系统在5.x环境使用
//            // 进入设置系统应用权限界面
//            Intent intent = new Intent(Settings.ACTION_SETTINGS);
//            startActivity(intent);
//            return;
//        }
//        return;
//    }






    //        App更新
    private void initUpdata() {
        OkHttpUtils.post()
                .url(ContractUtils.LOGIN_URL+"user/appup")
                .addHeader("token",ContractUtils.getTOKEN(getContext()))
                .build()
                .execute(new StringCallback() {
                    @Override
                    public void onError(Call call, Exception e, int id) {

                    }

                    @Override
                    public void onResponse(String response, int id) {
                        System.out.println(response+"          App更新");
                        if(response.indexOf("200") != -1){
                            Gson gson = new Gson();
                            UpDataEntity upDataEntity = gson.fromJson(response, UpDataEntity.class);
                            lujing = upDataEntity.getResponse().getSrc();
                            System.out.println(lujing+"             lujing");
                            if(upDataEntity.getResponse().getV().equals("")){
                                return;
                            }
                            int banbenhao = Integer.parseInt(upDataEntity.getResponse().getV());
                            if(info.versionCode < banbenhao){
                                //        App更新的弹窗
                                initAlertDialog();
                            }
                        }
                    }
                });
    }


//    App更新的弹窗
    private void initAlertDialog() {
        final AlertDialog dialog = new AlertDialog.Builder(getContext()).create();
        dialog.show();
        dialog.getWindow().setContentView(R.layout.alertdialog_updata);
        dialog.getWindow().setBackgroundDrawableResource(R.drawable.shape_zhuce);
        WindowManager windowManager = getActivity().getWindowManager();
        Display defaultDisplay = windowManager.getDefaultDisplay();
        WindowManager.LayoutParams attributes = dialog.getWindow().getAttributes();
        attributes.width= (int) (defaultDisplay.getWidth()*0.8);
        dialog.getWindow().setAttributes(attributes);
        dialog.getWindow().findViewById(R.id.tv_zanbu).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {//暂不更新
                dialog.dismiss();
            }
        });
        dialog.getWindow().findViewById(R.id.tv_gengxin).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {//更新
                System.out.println(lujing+"    路径00");
                Intent intent = new Intent();
                intent.setAction("android.intent.action.VIEW");
                Uri content_url = Uri.parse(lujing);//splitflowurl为分流地址
                intent.setData(content_url);
                startActivity(intent);
                dialog.dismiss();
            }
        });
    }


    //    首页总banner网络请求
    private void initShouYeBannerHttp() {
        OkHttpUtils.post()
                .url(ContractUtils.LOGIN_URL+"news/getBanner")
                .addHeader("token",ContractUtils.getTOKEN(getContext()))
                .addParams("parkId",ContractUtils.getParkId(getContext()))
                .addParams("typeId","0")
                .build()
                .execute(new StringCallback() {
                    @Override
                    public void onError(Call call, Exception e, int id) {
                        System.out.println(e+"     eeeeeeeeeeee首页总banner网络请求");
                    }

                    @Override
                    public void onResponse(String response, int id) {
                        System.out.println(response+"          首页总banner网络请求");
                    }
                });
    }



    //    首页系统消息的网络请求
    private void initXiTongXiaoXiHttp() {
        OkHttpUtils.post()
                .url(ContractUtils.LOGIN_URL + "life/message")
                .addHeader("token", ContractUtils.getTOKEN(getContext()))
                .addParams("parkId", ContractUtils.getParkId(getContext()))
                .addParams("page", "1")
                .build()
                .execute(new StringCallback() {
                    @Override
                    public void onError(Call call, Exception e, int id) {

                    }

                    @Override
                    public void onResponse(String response, int id) {
                        ContractUtils.Code500(getContext(),response);
                        System.out.println(response + "       首页系统消息的网络请求");
                        Gson gson = new Gson();
                        xiTongXiaoXiEntity = gson.fromJson(response, XiTongXiaoXiEntity.class);

                    }
                });
    }


//    用户未读消息的个数的网络请求
    private void initWeiDuXiaoXiHttp() {
        OkHttpUtils.post()
                .url(ContractUtils.LOGIN_URL+"life/messagege")
                .addHeader("token",ContractUtils.getTOKEN(getContext()))
                .addParams("parkId",ContractUtils.getParkId(getContext()))
                .build()
                .execute(new StringCallback() {
                    @Override
                    public void onError(Call call, Exception e, int id) {

                    }

                    @Override
                    public void onResponse(String response, int id) {
                        ContractUtils.Code500(getContext(),response);
                        System.out.println(response+"         未读消息个数的网络请求");
                        Integer number = 0;
                        if(response.indexOf("200") != -1){
                            Gson gson = new Gson();
                            xiTongXiaoXiWeiDuGeShuEntity = gson.fromJson(response, XiTongXiaoXiWeiDuGeShuEntity.class);
                            number = Integer.parseInt(xiTongXiaoXiWeiDuGeShuEntity.getResponse().getGe());
                            if(number > 0){
//                                tvXiaoxiNumber.setVisibility(View.VISIBLE);
                                tvXiaoxiNumber.setVisibility(View.VISIBLE);
                                tvXiaoxiNumber.setText(xiTongXiaoXiWeiDuGeShuEntity.getResponse().getGe());
                            }else if(number <= 0){
                                tvXiaoxiNumber.setVisibility(View.GONE);
                            }
                        }
                    }
                });
    }


    @Override
    public boolean onDown(MotionEvent motionEvent) {
        System.out.println("aaaaaaaaaaaa");
        return false;
    }

    @Override
    public void onShowPress(MotionEvent motionEvent) {
        System.out.println("bbbbbbbbbbb");
    }

    @Override
    public boolean onSingleTapUp(MotionEvent motionEvent) {
        System.out.println("ccccccccccccc");
        return false;
    }

    @Override
    public boolean onScroll(MotionEvent motionEvent, MotionEvent motionEvent1, float v, float v1) {
        System.out.println("dddddddddddddddd");
        return false;
    }

    @Override
    public void onLongPress(MotionEvent motionEvent) {
        System.out.println("eeeeeeeeeeeeeeeeee");
    }

    @Override
    public boolean onFling(MotionEvent motionEvent, MotionEvent motionEvent1, float v, float v1) {
        System.out.println("fffffffffffffffff");
        return false;
    }


    class NetWork extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            parkId = intent.getStringExtra("parkId");
            System.out.println(parkId + "    接收的园区id");
        }
    }



    @Override
    public void onResume() {
        super.onResume();
        tiaozhuan = false;


        initView();
//        首页获取推荐企业的网络请求
        initTuiJianQuYeHttp();
//        首页园区资讯的网络请求
        initYuanQuZiXunHttp();
//        首页banner图的网络请求
        initBannerHttp();
//        首页系统消息的网络请求
        initXiTongXiaoXiHttp();


//        用户未读消息的个数的网络请求
        initWeiDuXiaoXiHttp();



        //        设置园区接口
        initSheZhiYuanQuHttp();


////        首页总banner网络请求
//        initShouYeBannerHttp();

        SharedPreferences sharedPreferences = getContext().getSharedPreferences("yuanqu", Context.MODE_PRIVATE);
        yuanqu = sharedPreferences.getString("yuanqu", "");
        if (yuanqu.equals("")) {

        } else {
            btnQiehuanYuanqu.setText(yuanqu);
            System.out.println(yuanqu + "    选择的园区名称");
        }

        System.out.println("11111111111111111");

    }

    //    首页banner图的网络请求
    private void initBannerHttp() {
        OkHttpUtils.post()
                .url(ContractUtils.LOGIN_URL + "news/getBanner")
                .addHeader("token", ContractUtils.getTOKEN(getContext()))
                .addParams("parkId", ContractUtils.getParkId(getContext()))
                .addParams("typeId","0")
                .build()
                .execute(new StringCallback() {
                    @Override
                    public void onError(Call call, Exception e, int id) {

                    }

                    @Override
                    public void onResponse(String response, int id) {
                        ContractUtils.Code500(getContext(),response);
                        System.out.println(response + "      首页banner图的网络请求");

                        if (response.indexOf("200") != -1) {
                            Gson gson = new Gson();
                            shouYeBannerEntity = gson.fromJson(response, ShouYeBannerEntity.class);
                            list.clear();

                            for (int i = 0; i < shouYeBannerEntity.getResponse().size(); i++) {
                                View view = LayoutInflater.from(getContext()).inflate(R.layout.item_one, null);
                                TextView textView = view.findViewById(R.id.tv_wenzi);
                                textView.setText(shouYeBannerEntity.getResponse().get(i).getId());
                                Glide.with(getContext()).load(shouYeBannerEntity.getResponse().get(i).getImage()).into((ImageView) view.findViewById(R.id.viewpager_image));
                                view.setTag(shouYeBannerEntity.getResponse().get(i).getId());
                                list.add(view);


                                view.findViewById(R.id.viewpager_image).setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
////                                        TextView textView = view.findViewById(R.id.tv_wenzi);
////                                        String trim = textView.getText().toString().trim();
////                                        System.out.println(textView.getId()+"         text");

//                                        System.out.println(pos+"           pos");
//                                        Intent intent = new Intent(getContext(), ZiXunXiangQingActivity.class);
//                                        intent.putExtra("id",shouYeBannerEntity.getResponse().get(pos).getId());
//                                        startActivity(intent);

                                        if (shouYeBannerEntity.getResponse().get(viewPager.getCurrentItem()).getHrefType().equals("0")){
                                            Intent intenttongzhi = new Intent(getContext(), YuanQuTongZhiActivity.class);
                                            intenttongzhi.putExtra("zhi","zhi");
                                            startActivity(intenttongzhi);
//                                            Intent intent1 = new Intent(getContext(),ChanYeJinRongActivity.class);
//                                            startActivity(intent1);
                                        }else if(shouYeBannerEntity.getResponse().get(viewPager.getCurrentItem()).getHrefType().equals("1")){
                                            Intent intent1 = new Intent(getContext(),YuanQuShouCeActivity.class);
                                            startActivity(intent1);
                                        }else if(shouYeBannerEntity.getResponse().get(viewPager.getCurrentItem()).getHrefType().equals("2")){
                                            Intent intent1 = new Intent(getContext(),YuanQuQiYeActivity.class);
                                            startActivity(intent1);
                                        }else if(shouYeBannerEntity.getResponse().get(viewPager.getCurrentItem()).getHrefType().equals("3")){
                                            Intent intent1 = new Intent(getContext(),ZhengCeFaGuiActivity.class);
                                            startActivity(intent1);
                                        }else if(shouYeBannerEntity.getResponse().get(viewPager.getCurrentItem()).getHrefType().equals("4")){
                                            Intent intent1 = new Intent(getContext(),ZhiNengZhiZaoActivity.class);
                                            startActivity(intent1);
                                        }else if(shouYeBannerEntity.getResponse().get(viewPager.getCurrentItem()).getHrefType().equals("5")){
                                            Intent intent1 = new Intent(getContext(),YuanQuZhaoShangActivity.class);
                                            startActivity(intent1);
                                        }else if(shouYeBannerEntity.getResponse().get(viewPager.getCurrentItem()).getHrefType().equals("6")){
                                        if (tiaoZhuanFragment!=null){
                                            tiaoZhuanFragment.onClick();
                                        }
                                        }else if(shouYeBannerEntity.getResponse().get(viewPager.getCurrentItem()).getHrefType().equals("7")){
                                            if(fuWuTiaoZhuFragment != null){
                                                fuWuTiaoZhuFragment.onClick1();
                                            }
                                        }else if(shouYeBannerEntity.getResponse().get(viewPager.getCurrentItem()).getHrefType().equals("8")){
                                            Intent intent = new Intent(getContext(), HomeActivity.class);
                                            startActivity(intent);
                                        }else if(shouYeBannerEntity.getResponse().get(viewPager.getCurrentItem()).getHrefType().equals("9")){
                                            if(shouYeBannerEntity.getResponse().get(viewPager.getCurrentItem()).getContent() != null){
                                                Intent intent = new Intent(getContext(), WenZhangXiangQingActivity.class);
                                                intent.putExtra("content",shouYeBannerEntity.getResponse().get(viewPager.getCurrentItem()).getContent());
                                                startActivity(intent);
                                            }
                                        }else if(shouYeBannerEntity.getResponse().get(viewPager.getCurrentItem()).getHrefType().equals("10")){
                                            if(shouYeBannerEntity.getResponse().get(viewPager.getCurrentItem()).getContent() != null){
                                                Intent intent = new Intent(getContext(), YuanQuShengHuoActivity.class);
                                                startActivity(intent);
                                            }
                                        }else if(shouYeBannerEntity.getResponse().get(viewPager.getCurrentItem()).getHrefType().equals("11")){
                                            if(shouYeBannerEntity.getResponse().get(viewPager.getCurrentItem()).getContent() != null){
                                                return;
                                            }
                                        }else if(shouYeBannerEntity.getResponse().get(viewPager.getCurrentItem()).getHrefType().equals("12")){
                                            if(shouYeBannerEntity.getResponse().get(viewPager.getCurrentItem()).getContent() != null){
                                                Intent intent = new Intent(getContext(),SettingActivity.class);
                                                startActivity(intent);
                                            }
                                        }else if(shouYeBannerEntity.getResponse().get(viewPager.getCurrentItem()).getHrefType().equals("13")){
                                            if(shouYeBannerEntity.getResponse().get(viewPager.getCurrentItem()).getContent() != null){
                                                Intent intent = new Intent(getContext(),QiuZhiZhaoPinActivity.class);
                                                startActivity(intent);
                                            }
                                        }else if(shouYeBannerEntity.getResponse().get(viewPager.getCurrentItem()).getHrefType().equals("14")){
                                            if(shouYeBannerEntity.getResponse().get(viewPager.getCurrentItem()).getContent() != null){
                                                Intent intent = new Intent(getContext(),ChanYeJinRongActivity.class);
                                                startActivity(intent);
                                            }
                                        }else if(shouYeBannerEntity.getResponse().get(viewPager.getCurrentItem()).getHrefType().equals("15")){
                                            if(shouYeBannerEntity.getResponse().get(viewPager.getCurrentItem()).getContent() != null){
                                                Intent intent = new Intent(getContext(),YuanQuTongZhiActivity.class);
                                                startActivity(intent);
                                            }
                                        }else if(shouYeBannerEntity.getResponse().get(viewPager.getCurrentItem()).getHrefType().equals("16")){
                                            if(shouYeBannerEntity.getResponse().get(viewPager.getCurrentItem()).getContent() != null){
                                                Intent intent = new Intent(getContext(),FangKeDengJiActivity.class);
                                                startActivity(intent);
                                            }
                                        }else if(shouYeBannerEntity.getResponse().get(viewPager.getCurrentItem()).getHrefType().equals("17")){
                                            if(shouYeBannerEntity.getResponse().get(viewPager.getCurrentItem()).getContent() != null){
                                                Intent intent = new Intent(getContext(),QuanBuActivity.class);
                                                intent.putExtra("daizhifu","0");
                                                startActivity(intent);
                                            }
                                        }else if(shouYeBannerEntity.getResponse().get(viewPager.getCurrentItem()).getHrefType().equals("18")){
                                            if(shouYeBannerEntity.getResponse().get(viewPager.getCurrentItem()).getContent() != null){
                                                Intent intent = new Intent(getContext(),MyShouCangActivity.class);
                                                startActivity(intent);
                                            }
                                        }else if(shouYeBannerEntity.getResponse().get(viewPager.getCurrentItem()).getHrefType().equals("19")){
                                            if(shouYeBannerEntity.getResponse().get(viewPager.getCurrentItem()).getContent() != null){
                                                Intent intent = new Intent(getContext(),ShouHuoXinXiActivity.class);
                                                startActivity(intent);
                                            }
                                        }else if(shouYeBannerEntity.getResponse().get(viewPager.getCurrentItem()).getHrefType().equals("20")){
                                            if(shouYeBannerEntity.getResponse().get(viewPager.getCurrentItem()).getContent() != null){
                                                Intent intent = new Intent(getContext(),RuZhuShenQingActivity.class);
                                                startActivity(intent);
                                            }
                                        }else if(shouYeBannerEntity.getResponse().get(viewPager.getCurrentItem()).getHrefType().equals("21")){
                                            if(shouYeBannerEntity.getResponse().get(viewPager.getCurrentItem()).getContent() != null){
                                                Intent intent = new Intent(getContext(),MyBangDingActivity.class);
                                                startActivity(intent);
                                            }
                                        }
                                    }
                                });
                            }

                            BannerAdapter adapter = new BannerAdapter(getContext(), list);
                            viewPager.setAdapter(adapter);
                            adapter1 = new BannerAdapter1();
                            manager = new LinearLayoutManager(getContext());
                            manager.setOrientation(LinearLayoutManager.HORIZONTAL);
                            recyclerViewBanner.setLayoutManager(manager);
                            recyclerViewBanner.setAdapter(adapter1);
                        }
                    }
                });
    }


    class BannerAdapter1 extends RecyclerView.Adapter<BannerAdapter1.ViewHolder> {

        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(getContext()).inflate(R.layout.item_banner_dian, parent, false);
            ViewHolder viewHolder = new ViewHolder(view);
            return viewHolder;
        }

        @Override
        public void onBindViewHolder(ViewHolder holder, int position) {
            if (viewPager.getCurrentItem() == position) {
                holder.img_two.setImageResource(R.drawable.yuanbai);
            } else {
                holder.img_two.setImageResource(R.drawable.yuanhui);
            }
        }

        @Override
        public int getItemCount() {
            return list.size();
        }

        public class ViewHolder extends RecyclerView.ViewHolder {
            private ImageView img_two;

            public ViewHolder(View itemView) {
                super(itemView);
                img_two = itemView.findViewById(R.id.img_two);
            }
        }
    }


    public class MyImageLoader extends ImageLoader {
        @Override
        public void displayImage(Context context, Object path, ImageView imageView) {
            Glide.with(context).load(path).into(imageView);
        }
    }


    //    首页园区资讯的网络请求
    private void initYuanQuZiXunHttp() {
        final ProgressDialog progressDialog = new ProgressDialog(getContext());
        progressDialog.setTitle("提示");
        progressDialog.setMessage("请等待...");
        progressDialog.setCancelable(false);
        progressDialog.show();
        OkHttpUtils.post()
                .url(ContractUtils.LOGIN_URL + "news/getIndexNews")
                .addHeader("token", ContractUtils.getTOKEN(getContext()))
                .addParams("parkId", ContractUtils.getParkId(getContext()))
                .addParams("isIndex","1")
                .build()
                .execute(new StringCallback() {
                    @Override
                    public void onError(Call call, Exception e, int id) {
                        progressDialog.dismiss();
                    }

                    @Override
                    public void onResponse(String response, int id) {
                        progressDialog.dismiss();
                        ContractUtils.Code500(getContext(),response);
                        System.out.println(response + "        首页园区资讯的网络请求");

                        if (response.indexOf("200") != -1) {
                            Gson gson = new Gson();
                            ShouYeYuanQuZiXunEntity shouYeYuanQuZiXunEntity = gson.fromJson(response, ShouYeYuanQuZiXunEntity.class);
                            shouYeZiXunAdapter = new ShouYeZiXunAdapter(getContext(), shouYeYuanQuZiXunEntity.getResponse());
                            LinearLayoutManager manager = new LinearLayoutManager(getContext());
                            recyclerView.setLayoutManager(manager);
                            recyclerView.setAdapter(shouYeZiXunAdapter);

                            shouYeZiXunAdapter.setShouYeZiXunClick(new ShouYeZiXunAdapter.ShouYeZiXunClick() {
                                @Override
                                public void shouyezixunClick(int position, String id) {
                                    Intent intent = new Intent(getContext(), ZiXunXiangQingActivity.class);
                                    intent.putExtra("id",id);
                                    startActivity(intent);
                                }
                            });
                        }
                    }
                });
    }


    //    首页获取推荐企业的网络请求
    private void initTuiJianQuYeHttp() {
        OkHttpUtils.post()
                .url(ContractUtils.LOGIN_URL + "company/getTj")
                .addHeader("token", ContractUtils.getTOKEN(getContext()))
                .addParams("parkId", ContractUtils.getParkId(getContext()))
                .build()
                .execute(new StringCallback() {
                    @Override
                    public void onError(Call call, Exception e, int id) {

                    }

                    @Override
                    public void onResponse(String response, int id) {
                        ContractUtils.Code500(getContext(),response);
                        System.out.println(response + "   首页获取企业推荐的网络请求");

                        if (response.indexOf("200") != -1) {
                            Gson gson = new Gson();
                            shouYeTuiJianQiYeEntity = gson.fromJson(response, ShouYeTuiJianQiYeEntity.class);

                            viewflipper.removeAllViews();

                            for (int i = 0; i < shouYeTuiJianQiYeEntity.getResponse().size(); i++) {
                                View view = View.inflate(getActivity(), R.layout.scroll_tv, null);
                                TextView tv_scroll_one = (TextView) view.findViewById(R.id.tv_scroll_one);
                                tv_scroll_one.setText(shouYeTuiJianQiYeEntity.getResponse().get(i).getEnterName());
                                viewflipper.addView(view);
                            }
                            viewflipper.startFlipping();
                            viewflipper.setInAnimation(AnimationUtils.loadAnimation(getActivity(), R.anim.push_down_in));
                            viewflipper.setOutAnimation(AnimationUtils.loadAnimation(getActivity(), R.anim.push_down_out));
                            viewflipper.setDisplayedChild(xiabiao);

////                            if(viewflipper.getDisplayedChild()==0) {
                                viewflipper.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View arg0) {
                                        xiabiao = viewflipper.getDisplayedChild();

                                        Intent intent = new Intent(getContext(), QiYeXiangQingActivity.class);
//                                    应该传企业的id   tiantian
                                        System.out.println(viewflipper.getDisplayedChild()+"              zzzzzzzzzzzzzzzzz");
//                                        if(viewflipper.getDisplayedChild() >shouYeTuiJianQiYeEntity.getResponse().size()-1){
//                                            return;
//                                        }
                                        intent.putExtra("id",shouYeTuiJianQiYeEntity.getResponse().get(viewflipper.getDisplayedChild()).getEnterId());
                                        System.out.println(shouYeTuiJianQiYeEntity.getResponse().get(viewflipper.getDisplayedChild()).getEnterId()+"             园区企业id");
                                        startActivity(intent);
                                    }
                                });
                            }
                    }
                });
    }





    private void initView() {

        zimu.setmOnIndexPressedListener(new IndexBar.onIndexPressedListener() {
            @Override
            public void onIndexPressed(int index, String text) {
                if(tiaozhuan == false){
                    System.out.println(index + "    index");
                    System.out.println(text + "     text");
                    Intent intent0 = new Intent(getContext(), YuanQuQiYeActivity.class);
                    intent0.putExtra("title", text);// 点击单个字母传的标题
                    startActivity(intent0);
                }
                tiaozhuan = true;

            }

            @Override
            public void onMotionEventEnd() {
                System.out.println("111111111111");
            }
        });


//        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
//        employIndexBar.setmPressedShowTextView(employHintBar)
//                .setNeedRealIndex(true)
//                .setmLayoutManager(layoutManager);
//        employIndexBar.getDataHelper().sortSourceDatas(results);
//        employIndexBar.setmSourceDatas(results).invalidate();
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
//        initData();
    }

    private void initThread() {
        new Thread() {
            @Override
            public void run() {
                super.run();
                try {
                    while (isOk) {
                        Thread.sleep(3000);
                        Message message = Message.obtain();
                        int currentItem = viewPager.getCurrentItem();
                        if (currentItem == list.size() - 1) {
                            currentItem = 0;
                            message.what = 1;
                            message.arg1 = currentItem;
                            handler.sendMessage(message);
                        } else {
                            message.what = 2;
                            message.arg1 = currentItem + 1;
                            handler.sendMessage(message);
                        }
                    }
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }.start();
    }


//    private void initData() {
//        contentList.add("中软国际有限公司");
//        contentList.add("软通动力信息技术（集团）有限公司");
//        contentList.add("中软国际有限公司");
//        contentList.add("软通动力信息技术（集团）有限公司");
//
//        for (int a = 0; a < contentList.size(); a++) {
//            View view = View.inflate(getActivity(), R.layout.scroll_tv, null);
//            TextView tv_scroll_one = (TextView) view.findViewById(R.id.tv_scroll_one);
//            TextView tv_scroll_two = (TextView) view.findViewById(R.id.tv_scroll_two);
//            tv_scroll_one.setText(contentList.get(a));
////            tv_scroll_two.setText(contentList.get(a+1));
//            viewflipper.addView(view);
//        }
//        viewflipper.startFlipping();
//        viewflipper.setInAnimation(AnimationUtils.loadAnimation(getActivity(),
//                R.anim.push_down_in));
//        viewflipper.setOutAnimation(AnimationUtils.loadAnimation(getActivity(),
//                R.anim.push_down_out));
//    }

    @OnClick({R.id.zimu, R.id.btn_qiehuan_yuanqu, R.id.tv_tuijianqiye,R.id.viewflipper, R.id.image_saoyisao, R.id.image_xiaoxi, R.id.tv_yuanqushouce, R.id.tv_yuanquqiye, R.id.tv_yuanqushenghuo, R.id.tv_yuanquzhaoshang, R.id.tv_qiuzhizhaopin, R.id.tv_zhengcefagui, R.id.tv_chanyejinrong, R.id.tv_zhinengzhizao})
    public void onViewClicked(View view) {
        switch (view.getId()) {
//            推荐企业
            case R.id.tv_tuijianqiye:
                break;
//            右侧的子母索引
            case R.id.zimu:
                break;
//            滚动的推荐企业
            case R.id.viewflipper:
                break;
//            左上角切换园区
            case R.id.btn_qiehuan_yuanqu:
//                Intent intentq = new Intent(getContext(), XuanZeYuanQuActivity.class);
//                startActivity(intentq);
                Intent intent11 = new Intent(getContext(), QieHuanYuanQuActivity.class);
                intent11.putExtra("biaoti","2");
                startActivity(intent11);
                break;
//            扫一扫
            case R.id.image_saoyisao:
                startQrCode();
                break;
//            系统消息
            case R.id.image_xiaoxi:
                Intent intent00 = new Intent(getContext(), FuWuMessageActivity.class);
                startActivity(intent00);
                break;
//            园区手册
            case R.id.tv_yuanqushouce:
                Intent intent = new Intent(getContext(), YuanQuShouCeActivity.class);
                intent.putExtra("shouce","0");
                startActivity(intent);
                break;
//            园区企业
            case R.id.tv_yuanquqiye:
                Intent intent1 = new Intent(getContext(), YuanQuQiYeActivity.class);
                startActivity(intent1);
                break;
//            园区生活
            case R.id.tv_yuanqushenghuo:
                Intent intent2 = new Intent(getContext(), YuanQuShengHuoActivity.class);
                startActivity(intent2);
                break;
//            园区招商
            case R.id.tv_yuanquzhaoshang:
                Intent intent3 = new Intent(getContext(), YuanQuZhaoShangActivity.class);
                startActivity(intent3);
                break;
//            求职招聘
            case R.id.tv_qiuzhizhaopin:
                Intent intent4 = new Intent(getContext(), QiuZhiZhaoPinActivity.class);
                startActivity(intent4);
                break;
//            政策法规
            case R.id.tv_zhengcefagui:
                Intent intent5 = new Intent(getContext(), ZhengCeFaGuiActivity.class);
                startActivity(intent5);
                break;
//            金融/制造
            case R.id.tv_chanyejinrong:
                Intent intent6 = new Intent(getContext(), ChanYeJinRongActivity.class);
                startActivity(intent6);
                break;
//              企服中心
            case R.id.tv_zhinengzhizao:
                Intent intent7 = new Intent(getContext(), ZhiNengZhiZaoActivity.class);
                startActivity(intent7);
                break;
        }
    }


    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {
//        System.out.println(position+"       position");
        pos = position;

        if (adapter1!=null){
            adapter1.notifyDataSetChanged();
        }






//        clearState();

//        Intent intent = new Intent(getContext(), ZiXunXiangQingActivity.class);
//        intent.putExtra("id",shouYeBannerEntity.getResponse().get(position).getId());
//        System.out.println(shouYeBannerEntity.getResponse().get(position).getId()+"       chuanid");
////                                        intent.putExtra("id",shouYeBannerEntity.getResponse().get().getId());
//        startActivity(intent);
//        switch (position) {
//            case 0:
//                imgOne.setImageResource(R.drawable.radio_true);
//                break;
//            case 1:
//                imgTwo.setImageResource(R.drawable.radio_true);
//                break;
//            case 2:
//                imgThree.setImageResource(R.drawable.radio_true);
//                break;
//        }
    }

    @Override
    public void onPageScrollStateChanged(int state) {


    }




    public void clearState() {
//        imgOne.setImageResource(R.drawable.radio_false);
//        imgTwo.setImageResource(R.drawable.radio_false);
//        imgThree.setImageResource(R.drawable.radio_false);
    }




    /**
     * 开始扫码
     */
    private void startQrCode() {
        if(Build.VERSION.SDK_INT >= 23){
            if (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                // 申请权限
                requestPermissions(new String[]{Manifest.permission.CAMERA}, REQ_PERM_CAMERA);
                return;
            }else{
                // 二维码扫码
                Intent intent = new Intent(getActivity(), CaptureActivity.class);
//        startActivity(intent);
                startActivityForResult(intent, REQ_QR_CODE);
            }
        }else{
            // 二维码扫码
            Intent intent = new Intent(getActivity(), CaptureActivity.class);
//        startActivity(intent);
            startActivityForResult(intent, REQ_QR_CODE);
        }
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case REQ_PERM_CAMERA:
                // 摄像头权限申请
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // 获得授权
                    startQrCode();
                } else {
                    // 被禁止授权
                    Toast.makeText(getActivity(), "请至权限中心打开本应用的相机访问权限", Toast.LENGTH_LONG).show();
                }
                break;
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        getContext().unregisterReceiver(netWork);
    }


//    public class MyImageLoader extends ImageLoader {
//
//        @Override
//        public void displayImage(Context context, Object path, ImageView imageView) {
//            Glide.with(context).load(path).into(imageView);
//        }
//    }
}
