package com.wanyangproject.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.gson.Gson;
import com.wanyangproject.R;
import com.wanyangproject.adapter.YuanQuTongZhiAdapter;
import com.wanyangproject.entity.YuanQuTongZhiEntity;
import com.wanyangproject.utils.ContractUtils;
import com.zhy.http.okhttp.OkHttpUtils;
import com.zhy.http.okhttp.callback.StringCallback;

import butterknife.BindView;
import butterknife.ButterKnife;
import okhttp3.Call;

/**
 * Created by 甜甜 on 2018/7/18.
 */

public class FirstFragment extends Fragment {
    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    private YuanQuTongZhiAdapter yuanQuTongZhiAdapter;
    private YuanQuTongZhiEntity yuanQuTongZhiEntity;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_first, container, false);
        ButterKnife.bind(this, view);
        initView();

//        园区通知的网络请求
        initYuanQuTongZhiHttp();

        return view;
    }

//    园区通知的网络请求
    private void initYuanQuTongZhiHttp() {
        OkHttpUtils.post()
                .url(ContractUtils.LOGIN_URL+"news/getTongzhi")
                .addHeader("token",ContractUtils.getTOKEN(getContext()))
                .addParams("parkId",ContractUtils.getParkId(getContext()))
                .build()
                .execute(new StringCallback() {
                    @Override
                    public void onError(Call call, Exception e, int id) {

                    }

                    @Override
                    public void onResponse(String response, int id) {
                        ContractUtils.Code500(getContext(),response);
                        System.out.println(response+"        园区通知的网络请求");
                        Gson gson = new Gson();
                        yuanQuTongZhiEntity = gson.fromJson(response, YuanQuTongZhiEntity.class);
                        if(response.indexOf("200") != -1){
                            yuanQuTongZhiAdapter = new YuanQuTongZhiAdapter(getContext(),yuanQuTongZhiEntity.getResponse());
                            LinearLayoutManager manager = new LinearLayoutManager(getContext());
                            recyclerView.setLayoutManager(manager);
                            recyclerView.setAdapter(yuanQuTongZhiAdapter);
                        }
                    }
                });
    }



    private void initView() {
//        yuanQuTongZhiAdapter = new YuanQuTongZhiAdapter(getContext());
//        LinearLayoutManager manager = new LinearLayoutManager(getContext());
//        recyclerView.setLayoutManager(manager);
//        recyclerView.setAdapter(yuanQuTongZhiAdapter);
    }

}
