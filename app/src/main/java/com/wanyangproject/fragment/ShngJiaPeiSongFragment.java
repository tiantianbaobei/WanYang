package com.wanyangproject.fragment;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.wanyangproject.R;
import com.wanyangproject.entity.ChuanZhiEntity;
import com.wanyangproject.entity.ShouHuoXinXiLieBiaoEntity;
import com.wanyangproject.shouye.ShouHuoXinXiActivity;
import com.wanyangproject.utils.ContractUtils;
import com.zhy.http.okhttp.OkHttpUtils;
import com.zhy.http.okhttp.callback.StringCallback;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.Call;

/**
 * Created by 甜甜 on 2018/7/19.
 */
//   外卖配送地址
public class ShngJiaPeiSongFragment extends Fragment {

    @BindView(R.id.relative_shouhuo)
    RelativeLayout relativeShouhuo;
    @BindView(R.id.tv_shouhuo_xinxi)
    TextView tvShouhuoXinxi;
    @BindView(R.id.tv_xuanzeshouhuo)
    TextView tvXuanzeshouhuo;
    @BindView(R.id.tv_name)
    TextView tvName;
    @BindView(R.id.tv_phone)
    TextView tvPhone;
    @BindView(R.id.tv_address)
    TextView tvAddress;
    private NetWork netWork;
    private String phone;
    private String name;
    private String address;
    private String addressid;
    private ShouHuoXinXiLieBiaoEntity shouHuoXinXiLieBiaoEntity;
    private String sheng;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.shangjia_peisong_fragment, container, false);
        ButterKnife.bind(this, view);


        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("shouhuoxinxi");
        netWork = new NetWork();
        getContext().registerReceiver(netWork, intentFilter);



//        地址列表的网络请求
        initDiZhiLieBiaoHttp();

        return view;
    }

//    地址列表的网络请求
    private void initDiZhiLieBiaoHttp() {
        OkHttpUtils.post()
                .url(ContractUtils.LOGIN_URL+"Life/reslist")
                .addHeader("token",ContractUtils.getTOKEN(getContext()))
                .addParams("parkId",ContractUtils.getParkId(getContext()))
                .build()
                .execute(new StringCallback() {
                    @Override
                    public void onError(Call call, Exception e, int id) {

                    }

                    @Override
                    public void onResponse(String response, int id) {
                        ContractUtils.Code500(getContext(),response);
                        System.out.println(response+"        收货信息列表的网络请求  ");

                        if (response.indexOf("200") != -1){
                            Gson gson = new Gson();
                            shouHuoXinXiLieBiaoEntity = gson.fromJson(response, ShouHuoXinXiLieBiaoEntity.class);
                            for (int i = 0; i < shouHuoXinXiLieBiaoEntity.getResponse().size(); i++) {
                                String type = shouHuoXinXiLieBiaoEntity.getResponse().get(i).getType();
                                if(type.equals("1")){
                                    tvXuanzeshouhuo.setVisibility(View.GONE);
                                    tvShouhuoXinxi.setVisibility(View.GONE);
                                    tvName.setVisibility(View.VISIBLE);
                                    tvName.setText(shouHuoXinXiLieBiaoEntity.getResponse().get(i).getName());
                                    tvPhone.setVisibility(View.VISIBLE);
                                    tvPhone.setText(shouHuoXinXiLieBiaoEntity.getResponse().get(i).getPhone());
                                    tvAddress.setVisibility(View.VISIBLE);
                                    tvAddress.setText(shouHuoXinXiLieBiaoEntity.getResponse().get(i).getRegion()+shouHuoXinXiLieBiaoEntity.getResponse().get(i).getAddres());

                                    Intent intent=new Intent();
                                    intent.putExtra("phone",shouHuoXinXiLieBiaoEntity.getResponse().get(i).getPhone());
                                    intent.putExtra("name",shouHuoXinXiLieBiaoEntity.getResponse().get(i).getName());
                                    intent.putExtra("address",shouHuoXinXiLieBiaoEntity.getResponse().get(i).getAddres());
                                    intent.putExtra("addrid",shouHuoXinXiLieBiaoEntity.getResponse().get(i).getId());
                                    intent.setAction("dizhixxx");
                                    getContext().sendBroadcast(intent);

                                }
                            }
                        }
                    }
                });
    }


    class NetWork extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            phone = intent.getStringExtra("phone");
            name = intent.getStringExtra("name");
            address = intent.getStringExtra("address");
            addressid = intent.getStringExtra("addressid");
            sheng = intent.getStringExtra("sheng");
//            ChuanZhiEntity.obj = "id"+addressid;
//            System.out.println(ChuanZhiEntity.obj+"      ChuanZhiEntity.obj");
//            System.out.println(addressid+"    addressid接收");

            Intent intent1=new Intent();
            intent1.putExtra("addressid",addressid);
            System.out.println(addressid+"         idididid");
            intent1.setAction("dizhiid");
            getContext().sendBroadcast(intent1);



            if (phone == null) {

            } else {
                tvShouhuoXinxi.setVisibility(View.GONE);
                tvXuanzeshouhuo.setVisibility(View.GONE);
                tvPhone.setVisibility(View.VISIBLE);
                tvPhone.setText(phone);
            }

            if(name == null){

            }else{
                tvShouhuoXinxi.setVisibility(View.GONE);
                tvXuanzeshouhuo.setVisibility(View.GONE);
                tvName.setVisibility(View.VISIBLE);
                tvName.setText(name);
            }

            if(address == null){

            }else{
                tvShouhuoXinxi.setVisibility(View.GONE);
                tvXuanzeshouhuo.setVisibility(View.GONE);
                tvAddress.setVisibility(View.VISIBLE);
                tvAddress.setText(sheng + address);
            }


        }
    }


    @OnClick({R.id.relative_shouhuo})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.relative_shouhuo:
                Intent intent = new Intent(getContext(), ShouHuoXinXiActivity.class);
                startActivity(intent);
                break;
        }
    }
}
