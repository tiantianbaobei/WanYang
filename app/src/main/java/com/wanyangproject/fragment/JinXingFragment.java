package com.wanyangproject.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.google.gson.Gson;

import com.wanyangproject.R;
import com.wanyangproject.adapter.FuWuJiLuJinXingAdapter;
import com.wanyangproject.entity.FuWuJiLuJinXingEntity;
import com.wanyangproject.fuwuactivity.FuWuShenQingJiLuJinXingActivity;
import com.wanyangproject.utils.ContractUtils;
import com.zhy.http.okhttp.OkHttpUtils;
import com.zhy.http.okhttp.callback.StringCallback;


import butterknife.BindView;
import butterknife.ButterKnife;
import okhttp3.Call;

/**
 * Created by 甜甜 on 2018/7/18.
 */

public class JinXingFragment extends Fragment {
    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
//    @BindView(R.id.relative_one)
//    RelativeLayout relativeOne;
//    @BindView(R.id.relative_two)
//    RelativeLayout relativeTwo;
    private FuWuJiLuJinXingAdapter fuWuJiLuJinXingAdapter;
    private FuWuJiLuJinXingEntity fuWuJiLuJinXingEntity;
    public String isOK;


    public String getIsOK() {
        return isOK;
    }

    public void setIsOK(String isOK) {
        this.isOK = isOK;
        System.out.println(isOK+"           isisisisiokookokok");

    }




    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.jinxing_fragment, container, false);
        ButterKnife.bind(this, view);
        //        服务申请记录进行中的网络请求
        initJinXingHttp();
        return view;
    }


    @Override
    public void onResume() {
        super.onResume();
        //        服务申请记录进行中的网络请求
        initJinXingHttp();
    }

    //    服务申请记录进行中的网络请求
    private void initJinXingHttp() {
        System.out.println(isOK+"      jinxingisOK");

        System.out.println(ContractUtils.getTOKEN(getContext())+"     token");
        System.out.println(ContractUtils.getParkId(getContext())+"         pk_id ");
        System.out.println(isOK+"          quan ");

        if(isOK != null){
            if(isOK.equals("wode")){
                isOK = "1";
            }else if(isOK.equals("fuwu")){
                isOK = "0";
            }
        }


        OkHttpUtils.post()
                .url(ContractUtils.LOGIN_URL+"fuwu/myFuwu")
                .addHeader("token",ContractUtils.getTOKEN(getContext()))
                .addParams("typeId","0")//0：进行中，1：已完成
                .addParams("pk_id",ContractUtils.getParkId(getContext()))
                .addParams("quan",isOK)//0：只有企业服务，1：企业加优惠券
                .build()
                .execute(new StringCallback() {
                    @Override
                    public void onError(Call call, Exception e, int id) {

                    }

                    @Override
                    public void onResponse(String response, int id) {
                        ContractUtils.Code500(getContext(),response);
                        System.out.println(response+"       服务申请记录进行中的网络请求 ");

                        if(response.indexOf("200") != -1){
                            Gson gson = new Gson();
                            fuWuJiLuJinXingEntity = gson.fromJson(response, FuWuJiLuJinXingEntity.class);
                            fuWuJiLuJinXingAdapter = new FuWuJiLuJinXingAdapter(getContext(),fuWuJiLuJinXingEntity.getResponse());
                            LinearLayoutManager manager = new LinearLayoutManager(getContext());
                            recyclerView.setLayoutManager(manager);
                            recyclerView.setAdapter(fuWuJiLuJinXingAdapter);

                            fuWuJiLuJinXingAdapter.setJinXingXiangQingClick(new FuWuJiLuJinXingAdapter.JinXingXiangQingClick() {
                                @Override
                                public void jinxingXiangQingClick(int position, String id) {
                                    Intent intent = new Intent(getContext(), FuWuShenQingJiLuJinXingActivity.class);
                                    intent.putExtra("id",fuWuJiLuJinXingEntity.getResponse().get(position).getId());
                                    intent.putExtra("entype",fuWuJiLuJinXingEntity.getResponse().get(position).getEntype());
                                    startActivity(intent);
                                }
                            });
                        }else if(response.indexOf("400") != -1){
                           ContractUtils.Code400(getContext(),response);
                        }
                    }
                });
    }



//    @OnClick({R.id.relative_one, R.id.relative_two})
//    public void onViewClicked(View view) {
//        switch (view.getId()) {
//            case R.id.relative_one:
//                Intent intent = new Intent(getContext(), FuWuShenQingJiLuJinXingActivity.class);
//                startActivity(intent);
//                break;
//            case R.id.relative_two:
//                Intent intent1 = new Intent(getContext(), FuWuShenQingJiLuJinXingActivity.class);
//                startActivity(intent1);
//                break;
//        }
//    }
}
