package com.wanyangproject.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.wanyangproject.R;
import com.wanyangproject.activity.RegisterActivity;
import com.wanyangproject.entity.DaoDianXieYiEntity;
import com.wanyangproject.entity.ShangJiaShangPinEntity;
import com.wanyangproject.utils.ContractUtils;
import com.zhy.http.okhttp.OkHttpUtils;
import com.zhy.http.okhttp.callback.StringCallback;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.Call;

/**
 * Created by 甜甜 on 2018/7/19.
 */

public class DaoDianZiQuFragment extends Fragment {

    @BindView(R.id.relative_shouhuo)
    RelativeLayout relativeShouhuo;
    @BindView(R.id.image_tongyi)
    ImageView imageTongyi;
    @BindView(R.id.tv_name)
    TextView tvName;
    //    @BindView(R.id.tv_phone)
//    TextView tvPhone;
    @BindView(R.id.tv_address)
    TextView tvAddress;
    @BindView(R.id.tv_xieyi)
    TextView tvXieyi;
    private Boolean isOK = false;
    private String shopid;
    private WebView webView;
    private ShangJiaShangPinEntity shangJiaShangPinEntity;

    public String getShopid() {
        return shopid;
    }

    public void setShopid(String shopid) {
        this.shopid = shopid;
    }
    private  DaoDianXieYiEntity daoDianXieYiEntity;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.daodian_ziqu, container, false);
        ButterKnife.bind(this, view);

//        商家地址的网络请求
        initShangJiaDiZhiHttp();

        Intent intent1 = new Intent();
        intent1.putExtra("ziqu", "2");
        intent1.setAction("111");
        getContext().sendBroadcast(intent1);



//        到店自取服务协议的网络请求
        initDaoDianZiQuXieYiHttp();



        return view;
    }





//    到店自取服务协议的网络请求
    private void initDaoDianZiQuXieYiHttp() {
        OkHttpUtils.post()
                .url(ContractUtils.LOGIN_URL+"Shop/shopxieyi")
                .addHeader("token",ContractUtils.getTOKEN(getContext()))
                .build()
                .execute(new StringCallback() {
                    @Override
                    public void onError(Call call, Exception e, int id) {

                    }

                    @Override
                    public void onResponse(String response, int id) {
                        System.out.println(response+"         到店自取服务协议的网络请求 ");
                        ContractUtils.Code500(getContext(),response);
                        if(response.indexOf("200") != -1){
                            Gson gson = new Gson();
                            daoDianXieYiEntity = gson.fromJson(response, DaoDianXieYiEntity.class);

                        }
                    }
                });
    }


    //    商家地址的网络请求
    private void initShangJiaDiZhiHttp() {
        OkHttpUtils.post()
                .url(ContractUtils.LOGIN_URL + "Life/shop")
                .addHeader("token", ContractUtils.getTOKEN(getContext()))
                .addParams("parkId", ContractUtils.getParkId(getContext()))
                .addParams("shopId", shopid)
                .build()
                .execute(new StringCallback() {
                    @Override
                    public void onError(Call call, Exception e, int id) {

                    }

                    @Override
                    public void onResponse(String response, int id) {
                        ContractUtils.Code500(getContext(), response);
                        System.out.println(response + "      商家地址的网络请求");
                        Gson gson = new Gson();
                        shangJiaShangPinEntity = gson.fromJson(response, ShangJiaShangPinEntity.class);
                        if (shangJiaShangPinEntity.getResponse().getShop().getShop().getNickname() == null) {

                        } else {
//                            tvName.setText(shangJiaShangPinEntity.getResponse().getShop().getShop().getNickname());
                            tvName.setText("自取地址");
                        }

//                        if(shangJiaShangPinEntity.getResponse().getShop().getShop().getPhone() == null){
//
//                        }else{
//                            tvPhone.setText(shangJiaShangPinEntity.getResponse().getShop().getShop().getPhone());
//                        }

                        if (shangJiaShangPinEntity.getResponse().getShop().getShop().getBaidu_address() == null) {

                        } else {
                            tvAddress.setText(shangJiaShangPinEntity.getResponse().getShop().getShop().getBaidu_address());
                        }
                    }
                });
    }




    @OnClick({R.id.relative_shouhuo, R.id.image_tongyi,R.id.tv_xieyi})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.relative_shouhuo:
//                Intent intent = new Intent(getContext(), ShouHuoXinXiActivity.class);
//                startActivity(intent);
                break;
            case R.id.image_tongyi:
                if (isOK) {
//                    如果选中显示密码
                    imageTongyi.setImageResource(R.drawable.unchecked);
                    isOK = false;
                } else {
//                    否则隐藏密码
                    imageTongyi.setImageResource(R.drawable.checked);
                    isOK = true;
                }
                Intent intent = new Intent();
                intent.putExtra("ziqupanduan", isOK);
                intent.setAction("daodianqiziqi");
                getContext().sendBroadcast(intent);

//                imageTongyi.setImageResource(R.drawable.checked);
                break;
//            到点自取服务协议
            case R.id.tv_xieyi:
                final AlertDialog alertDialog = new AlertDialog.Builder(getContext()).create();
                alertDialog.show();
                alertDialog.getWindow().setContentView(R.layout.daodianziqu_xieyi_alertdialog);
                alertDialog.getWindow().setBackgroundDrawableResource(R.drawable.shape_zhuce);
                alertDialog.setCancelable(false);
                WindowManager windowManager = getActivity().getWindowManager();
                Display defaultDisplay = windowManager.getDefaultDisplay();
                WindowManager.LayoutParams attributes = alertDialog.getWindow().getAttributes();
                attributes.width= (int) (defaultDisplay.getWidth()*0.8);
                alertDialog.getWindow().setAttributes(attributes);

//                网络请求到店自取服务协议的内容
                TextView tv_xieyi_neirong = alertDialog.getWindow().findViewById(R.id.tv_xieyi_neirong);
                if(daoDianXieYiEntity.getResponse().getContent() != null){
                    tv_xieyi_neirong.setText(daoDianXieYiEntity.getResponse().getContent());
                }

//                webView = alertDialog.getWindow().findViewById(R.id.webView);
//                if(xieYiEntity.getResponse().getContent() != null){
//                    //   内容
//                    content = xieYiEntity.getResponse().getContent();
//                    webView.loadDataWithBaseURL("http://www.baidu.com", content,"text/html", "UTF-8", null);
//                }

                alertDialog.getWindow().findViewById(R.id.tv_yuedu).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        alertDialog.dismiss();
                    }
                });
                alertDialog.getWindow().findViewById(R.id.image_quxiao).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        alertDialog.dismiss();
                    }
                });
                break;
        }
    }
}
