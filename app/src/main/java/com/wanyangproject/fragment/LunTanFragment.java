package com.wanyangproject.fragment;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;

import com.androidkun.xtablayout.XTabLayout;
import com.bigkoo.pickerview.OptionsPickerView;
import com.google.gson.Gson;
import com.wanyangproject.R;
import com.wanyangproject.activity.FaTieActivity;
import com.wanyangproject.adapter.LunTanAdapter;
import com.wanyangproject.adapter.LunTanLieBiaoXiuGaiAdapter;
import com.wanyangproject.entity.HuoQuYuanQuLieBiaoEntity;
import com.wanyangproject.entity.LunTanEntity;
import com.wanyangproject.luntanfragment.LunTanLieBiaoFragment;
import com.wanyangproject.luntanfragment.QuanBuFragment;
import com.wanyangproject.utils.ContractUtils;
import com.zhy.http.okhttp.OkHttpUtils;
import com.zhy.http.okhttp.callback.StringCallback;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.Call;

/**
 * Created by 甜甜 on 2018/7/11.
 */
//   论坛界面
public class LunTanFragment extends Fragment {

    @BindView(R.id.spinner)
    Spinner spinner;
    @BindView(R.id.viewPager)
    ViewPager viewPager;
    @BindView(R.id.tv_fatie)
    TextView tvFatie;
    @BindView(R.id.tabLayout)
    XTabLayout tabLayout;
    @BindView(R.id.tv_xuanze_yuanqu)
    TextView tvXuanzeYuanqu;
    private String token;
    private HuoQuYuanQuLieBiaoEntity huoQuYuanQuLieBiaoEntity;
    private LunTanEntity lunTanEntity;
    private List<String> stringList = new ArrayList<>();
    List<QuanBuFragment> fragments = new ArrayList<>();
//    List<LunTanLieBiaoFragment> fragments = new ArrayList<>();
    private String yuanquid = "";
    private boolean isok = false;
    private ArrayList<String> list11 = new ArrayList<>();
    private ArrayList<String> list22 = new ArrayList<>();
    private String lxid;




    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_luntan, container, false);
        ButterKnife.bind(this, view);

//        initViewPager();

        SharedPreferences sharedPreferences = getContext().getSharedPreferences("token", Context.MODE_PRIVATE);
        token = sharedPreferences.getString("token", "token");
        System.out.println(token + "     token");

//        论坛标题的网络请求
        initLunTanTitleHttp();

        initView();


        tvXuanzeYuanqu.setText(ContractUtils.getYuanquname(getContext()));

        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                fragments.get(position).initLunTanLieBiaoHttp(yuanquid, lunTanEntity.getResponse().get(position).getId());
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
        return view;
    }

    private void initView() {
//        //        推荐
//        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
//            @Override
//            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
////                TextView tv = (TextView) view;
////                tv.setTextColor(getResources().getColor(R.color.colorLunTanSpinner));    //设置颜色
////                tv.setTextSize(14.0f);    //设置大小
////                tv.setGravity(Gravity.CENTER_HORIZONTAL);   //设置居中
//            }
//
//            @Override
//            public void onNothingSelected(AdapterView<?> parent) {
//            }
//        });
    }


    //    获取园区列表的网络请求
    private void initYuanQuLieBiaoHttp() {
        final ProgressDialog progressDialog = new ProgressDialog(getContext());
        progressDialog.setTitle("提示");
        progressDialog.setMessage("请等待...");
        progressDialog.setCancelable(false);
        progressDialog.show();
        OkHttpUtils.post()
                .url(ContractUtils.LOGIN_URL + "park/getPark")
                .build()
                .execute(new StringCallback() {
                    @Override
                    public void onError(Call call, Exception e, int id) {
                        progressDialog.dismiss();
                    }
                    @Override

                    public void onResponse(String response, int id) {
                        progressDialog.dismiss();
                        ContractUtils.Code500(getContext(), response);
                        System.out.println(response + "     获取园区列表的网络请求");
                        if (response.indexOf("200") != -1) {
                            Gson gson = new Gson();
                            huoQuYuanQuLieBiaoEntity = gson.fromJson(response, HuoQuYuanQuLieBiaoEntity.class);


                            for (int i = 0; i < huoQuYuanQuLieBiaoEntity.getResponse().size(); i++) {
                                list11.add(huoQuYuanQuLieBiaoEntity.getResponse().get(i).getParkId());
                                list22.add(huoQuYuanQuLieBiaoEntity.getResponse().get(i).getParkName());
                            }


//                            ArrayList<String> spinnerList = new ArrayList<>();
//                            for (int i = 0; i < huoQuYuanQuLieBiaoEntity.getResponse().size(); i++) {
//                                spinnerList.add(huoQuYuanQuLieBiaoEntity.getResponse().get(i).getParkName());
//                                ArrayAdapter adapter = new ArrayAdapter(getContext(), R.layout.spinner, R.id.tv_spinner, spinnerList);
//                                spinner.setAdapter(adapter);
//                            }



                            yuanquid = huoQuYuanQuLieBiaoEntity.getResponse().get(0).getParkId();
//                            if (response.indexOf("200") != -1) {
                                for (int i = 0; i < lunTanEntity.getResponse().size(); i++) {
                                    stringList.add(lunTanEntity.getResponse().get(i).getTitle());
                                    QuanBuFragment quanBuFragment = new QuanBuFragment();
                                    System.out.println(lunTanEntity.getResponse().get(i).getId() + "       bbbbbbbbbbbbbbbbb");
                                    fragments.add(quanBuFragment);
//                                    LunTanLieBiaoFragment quanBuFragment = new LunTanLieBiaoFragment();
//                                    System.out.println(lunTanEntity.getResponse().get(i).getId() + "       bbbbbbbbbbbbbbbbb");
//                                    fragments.add(quanBuFragment);
                                }

                                // 创建ViewPager适配器
                                LunTanAdapter luntanAdapter = new LunTanAdapter(getChildFragmentManager(), fragments, stringList);
//                               LunTanLieBiaoXiuGaiAdapter lunTanLieBiaoXiuGaiAdapter = new LunTanLieBiaoXiuGaiAdapter(getChildFragmentManager(), fragments, stringList);
//                             luntanAdapter.setFragments(fragments);
                                // 给ViewPager设置适配器
//                                viewPager.setAdapter(lunTanLieBiaoXiuGaiAdapter);
                                viewPager.setAdapter(luntanAdapter);
                                // 使用 TabLayout 和 ViewPager 相关联
                                tabLayout.setupWithViewPager(viewPager);

                                fragments.get(0).initLunTanLieBiaoHttp(huoQuYuanQuLieBiaoEntity.getResponse().get(0).getParkId(), lunTanEntity.getResponse().get(0).getId());
//                            }


                            //        推荐
//                            spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
//                                @Override
//                                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
//                                    TextView tv = (TextView) view;
//                                    tv.setTextColor(getResources().getColor(R.color.colorLunTanSpinner));    //设置颜色
//                                    tv.setTextSize(14.0f);    //设置大小
//                                    tv.setGravity(Gravity.CENTER_HORIZONTAL);   //设置居中
//
//                                    if (isok) {
//                                        yuanquid = huoQuYuanQuLieBiaoEntity.getResponse().get(position).getParkId();
//                                        //                获取园区id发送广播
//                                        //Intent intent = new Intent();
//                                        //intent.putExtra("luntanparkId",huoQuYuanQuLieBiaoEntity.getMsg().get(position).getParkId());
//                                        //intent.setAction("luntanyuanquid");
//                                        //getContext().sendBroadcast(intent);
//                                        //System.out.println(huoQuYuanQuLieBiaoEntity.getMsg().get(position).getParkId()+"   论坛发送园区的id");
//                                        fragments.get(viewPager.getCurrentItem()).initLunTanLieBiaoHttp(yuanquid, lunTanEntity.getResponse().get(viewPager.getCurrentItem()).getId());
//                                    }
//                                    isok = true;
//                                }
//
//                                @Override
//                                public void onNothingSelected(AdapterView<?> parent) {
//                                }
//                            });
                        }
                    }
                });
    }



    //    论坛标题的网络请求
    private void initLunTanTitleHttp() {
        final ProgressDialog progressDialog = new ProgressDialog(getContext());
        progressDialog.setTitle("提示");
        progressDialog.setMessage("请等待...");
        progressDialog.setCancelable(false);
        progressDialog.show();
        OkHttpUtils.post()
                .url(ContractUtils.LOGIN_URL + "forum/typeInfo")
                .addHeader("token", ContractUtils.getTOKEN(getContext()))
                .build()
                .execute(new StringCallback() {
                    @Override
                    public void onError(Call call, Exception e, int id) {
                        progressDialog.dismiss();
                        System.out.println(e + "    论坛eeeeeeeeee");
                    }

                    @Override
                    public void onResponse(String response, int id) {
                        progressDialog.dismiss();
                        ContractUtils.Code500(getContext(), response);
                        System.out.println(response + "   论坛标题的网络请求");

                        if (response.indexOf("200") != -1) {
                            Gson gson = new Gson();
                            lunTanEntity = gson.fromJson(response, LunTanEntity.class);
                            //                园区列表的网络请求
                            initYuanQuLieBiaoHttp();
                        }
                    }
                });
    }



    private void initViewPager() {
//        List<Fragment> fragments = new ArrayList<>();
//        for (int i = 0; i < 30; i++) {
//            stringList.add("科技"+i);
//            fragments.add(new QuanBuFragment());
//        }

//        // 创建ViewPager适配器
//        LunTanAdapter luntanAdapter = new LunTanAdapter(getChildFragmentManager(),fragments,stringList);
////        luntanAdapter.setFragments(fragments);
//        // 给ViewPager设置适配器
//        viewPager.setAdapter(luntanAdapter);
//        // TabLayout 指示器 (记得自己手动创建4个Fragment,注意是 app包下的Fragment 还是 V4包下的 Fragment)
////        tabLayout.addTab(tabLayout.newTab().setText("生活"));
////        tabLayout.addTab(tabLayout.newTab().setText("电脑"));
////        tabLayout.addTab(tabLayout.newTab().setText("工业"));
////        tabLayout.addTab(tabLayout.newTab().setText("科技"));
//        // 使用 TabLayout 和 ViewPager 相关联
//        tabLayout.setupWithViewPager(viewPager);
        // TabLayout指示器添加文本

    }


    @OnClick({R.id.tabLayout, R.id.viewPager, R.id.tv_fatie,R.id.tv_xuanze_yuanqu})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.tv_fatie:
                Intent intent = new Intent(getContext(), FaTieActivity.class);
                startActivity(intent);
                break;
            case R.id.tabLayout:
                break;
            case R.id.viewPager:
                break;
//            选择园区
            case R.id.tv_xuanze_yuanqu:
                OptionsPickerView pvOptions = new  OptionsPickerView.Builder(getContext(), new OptionsPickerView.OnOptionsSelectListener() {
                    @Override
                    public void onOptionsSelect(int options1, int option2, int options3 ,View v) {
//                        //返回的分别是三个级别的选中位置
                        String tx = list22.get(options1);
                        yuanquid = list11.get(options1);
                        tvXuanzeYuanqu.setText(tx);

                        yuanquid = huoQuYuanQuLieBiaoEntity.getResponse().get(options1).getParkId();
                        fragments.get(viewPager.getCurrentItem()).initLunTanLieBiaoHttp(yuanquid, lunTanEntity.getResponse().get(viewPager.getCurrentItem()).getId());
                    }
                })
                        .setSubmitText("确定")//确定按钮文字
                        .setCancelText("取消")//取消按钮文字
                        .setTitleText("选择园区")//标题
                        .setSubCalSize(14)//确定和取消文字大小
                        .setTitleSize(16)//标题文字大小
                        .setTitleColor(Color.BLACK)//标题文字颜色
                        .setSubmitColor(Color.RED)//确定按钮文字颜色
                        .setLineSpacingMultiplier(3.0f)//设置两横线之间的间隔倍数
                        .setCancelColor(Color.parseColor("#AEAAAA"))//取消按钮文字颜色
                        .setTitleBgColor(Color.parseColor("#f5f5f5"))//标题背景颜色 Night mode
                        .setBgColor(Color.parseColor("#ffffff"))//滚轮背景颜色 Night mode
                        .setContentTextSize(16)//滚轮文字大小
                        .setLinkage(false)//设置是否联动，默认true
                        .setLabels("", "", "")//设置选择的三级单位
                        .isCenterLabel(false) //是否只显示中间选中项的label文字，false则每项item全部都带有label。
                        .setCyclic(false, false, false)//循环与否
                        .setSelectOptions(0, 0, 0)  //设置默认选中项
                        .setOutSideCancelable(false)//点击外部dismiss default true
                        .isDialog(false)//是否显示为对话框样式
                        .build();

                pvOptions.setPicker(list22);//添加数据源
                pvOptions.show();

                break;
        }
    }


}
