package com.wanyangproject.fragment;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.google.gson.Gson;
import com.wanyangproject.R;
import com.wanyangproject.adapter.ChanYeJinRongAdapter;
import com.wanyangproject.entity.ChanYeJinRongEntity;
import com.wanyangproject.entity.LunTanLieBiaoEntity;
import com.wanyangproject.shouye.ChanYeJinRongXiangQingActivity;
import com.wanyangproject.shouye.YuanQuJieShaoActivity;
import com.wanyangproject.utils.ContractUtils;
import com.zhy.http.okhttp.OkHttpUtils;
import com.zhy.http.okhttp.callback.StringCallback;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import okhttp3.Call;

/**
 * Created by 甜甜 on 2018/8/9.
 */

public class ChanYeJinRongFragment extends Fragment {
    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    private ChanYeJinRongAdapter chanYeJinRongAdapter;
    private ChanYeJinRongEntity chanYeJinRongEntity;
    private ProgressDialog progressDialog;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.chanye_jinrong_fragment, container, false);
        ButterKnife.bind(this, view);
//        产业金融的网络请求
        initChanYeJinRongHttp();
        return view;
    }

//    产业金融的网络请求
    private void initChanYeJinRongHttp() {
        progressDialog = new ProgressDialog(getContext());
        progressDialog.setTitle("提示");
        progressDialog.setMessage("请等待...");
        progressDialog.setCancelable(false);
        progressDialog.show();

        OkHttpUtils.post()
                .url(ContractUtils.LOGIN_URL+"news/getList")
                .addHeader("token",ContractUtils.getTOKEN(getContext()))
                .addParams("parkId",ContractUtils.getParkId(getContext()))
                .addParams("nTypeId","2")//nTypeId产业金融传2   nTypeId智能制造传1
                .build()
                .execute(new StringCallback() {
                    @Override
                    public void onError(Call call, Exception e, int id) {
                        System.out.println(e+"    产业金融eeee");
                        progressDialog.dismiss();
                    }

                    @Override
                    public void onResponse(String response, int id) {
                        progressDialog.dismiss();
                        ContractUtils.Code500(getContext(),response);
                        System.out.println(response+"      产业金融的网络请求");
                        if(response.indexOf("200") != -1){
                            Gson gson = new Gson();
                            chanYeJinRongEntity = gson.fromJson(response, ChanYeJinRongEntity.class);

                            List<ChanYeJinRongEntity.ResponseBean> response1 = chanYeJinRongEntity.getResponse();
                            for(ChanYeJinRongEntity.ResponseBean bean : response1){
                                if(bean.getImgArr().size() == 0){
                                    bean.setItemType(0);
                                }else if(bean.getImgArr().size() == 1){
                                    bean.setItemType(1);
                                }else if(bean.getImgArr().size() == 2){
                                    bean.setItemType(2);
                                }else if(bean.getImgArr().size() == 3){
                                    bean.setItemType(3);
                                }else if(bean.getImgArr().size() == 6){
                                    bean.setItemType(6);
                                }else if(bean.getImgArr().size() >= 9){
                                    bean.setItemType(9);
                                }
                            }

                            chanYeJinRongAdapter = new ChanYeJinRongAdapter(chanYeJinRongEntity.getResponse());
                            LinearLayoutManager manager = new LinearLayoutManager(getContext());
                            recyclerView.setLayoutManager(manager);
                            recyclerView.setAdapter(chanYeJinRongAdapter);

                            chanYeJinRongAdapter.setOnItemChildClickListener(new BaseQuickAdapter.OnItemChildClickListener() {
                                @Override
                                public void onItemChildClick(BaseQuickAdapter adapter, View view, int position) {
                                    Intent intent = new Intent(getContext(), ChanYeJinRongXiangQingActivity.class);
                                    intent.putExtra("wenzhangid",chanYeJinRongEntity.getResponse().get(position).getId());
                                    System.out.println(chanYeJinRongEntity.getResponse().get(position).getId()+"         zzzz产业金融传送的id");
                                    startActivity(intent);
                                }
                            });
                        }


                    }
                });
    }
}
