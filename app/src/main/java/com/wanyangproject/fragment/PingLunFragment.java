package com.wanyangproject.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.wanyangproject.R;
import com.wanyangproject.adapter.ShangJiaPingLunAdapter;
import com.wanyangproject.entity.ShangJiaPingFenEntity;
import com.wanyangproject.utils.ContractUtils;
import com.zhy.http.okhttp.OkHttpUtils;
import com.zhy.http.okhttp.callback.StringCallback;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.Call;

/**
 * Created by 甜甜 on 2018/7/20.
 */
//   外卖评论
public class PingLunFragment extends Fragment {

    @BindView(R.id.tv_pingfen)
    TextView tvPingfen;
    @BindView(R.id.image_one)
    ImageView imageOne;
    @BindView(R.id.image_two)
    ImageView imageTwo;
    @BindView(R.id.image_three)
    ImageView imageThree;
    @BindView(R.id.image_four)
    ImageView imageFour;
    @BindView(R.id.image_five)
    ImageView imageFive;
    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    @BindView(R.id.tv_pingfen_number)
    TextView tvPingfenNumber;
    private ShangJiaPingLunAdapter shangJiaPingLunAdapter;
    private ShangJiaPingFenEntity shangJiaPingFenEntity;
    private String shopid;

    public String getShopid() {
        return shopid;
    }

    public void setShopid(String shopid) {
        this.shopid = shopid;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.pinglun_fragment, container, false);
        ButterKnife.bind(this, view);

//        商家评分的网络请求
        initShangJiaPingFenHttp();

        return view;
    }

    //    商家评分的网络请求
    private void initShangJiaPingFenHttp() {
        OkHttpUtils.post()
                .url(ContractUtils.LOGIN_URL + "shop/evaluatelist")
                .addHeader("token", ContractUtils.getTOKEN(getContext()))
                .addParams("parkId", ContractUtils.getParkId(getContext()))
                .addParams("shopId", shopid)
                .build()
                .execute(new StringCallback() {
                    @Override
                    public void onError(Call call, Exception e, int id) {
                        System.out.println(e + "      商家评分eee");
                    }

                    @Override
                    public void onResponse(String response, int id) {
                        System.out.println(response + "     商家评分的网络请求");
                        ContractUtils.Code500shibai(getContext(),response);
                        if (response.indexOf("200") != -1) {
                            Gson gson = new Gson();
                            shangJiaPingFenEntity = gson.fromJson(response, ShangJiaPingFenEntity.class);
                            float ping = 0;
                            if(shangJiaPingFenEntity.getResponse().getPing() != null){
                                ping = Float.parseFloat(shangJiaPingFenEntity.getResponse().getPing());
                            }

                         //        分数
                                tvPingfenNumber.setText(ping+"");

                                if(ping >= 1.0){
                                    imageOne.setImageResource(R.drawable.xing_true);
                                    imageTwo.setImageResource(R.drawable.xing_false);
                                    imageThree.setImageResource(R.drawable.xing_false);
                                    imageFour.setImageResource(R.drawable.xing_false);
                                    imageFive.setImageResource(R.drawable.xing_false);
                                }
                                if(ping >= 2.0){
                                    imageOne.setImageResource(R.drawable.xing_true);
                                    imageTwo.setImageResource(R.drawable.xing_true);
                                    imageThree.setImageResource(R.drawable.xing_false);
                                    imageFour.setImageResource(R.drawable.xing_false);
                                    imageFive.setImageResource(R.drawable.xing_false);
                                }
                                if(ping >= 3.0){
                                    imageOne.setImageResource(R.drawable.xing_true);
                                    imageTwo.setImageResource(R.drawable.xing_true);
                                    imageThree.setImageResource(R.drawable.xing_true);
                                    imageFour.setImageResource(R.drawable.xing_false);
                                    imageFive.setImageResource(R.drawable.xing_false);
                                }
                                if(ping >= 4.0){
                                    imageOne.setImageResource(R.drawable.xing_true);
                                    imageTwo.setImageResource(R.drawable.xing_true);
                                    imageThree.setImageResource(R.drawable.xing_true);
                                    imageFour.setImageResource(R.drawable.xing_true);
                                    imageFive.setImageResource(R.drawable.xing_false);
                                }
                                if(ping >= 5.0){
                                    imageOne.setImageResource(R.drawable.xing_true);
                                    imageTwo.setImageResource(R.drawable.xing_true);
                                    imageThree.setImageResource(R.drawable.xing_true);
                                    imageFour.setImageResource(R.drawable.xing_true);
                                    imageFive.setImageResource(R.drawable.xing_true);
                                }



//                            if(shangJiaPingFenEntity.getResponse().getList().get() == null){
//
//                            }else{
//                                if(shangJiaPingFenEntity.getService().equals("1")){
//                                    imageOne.setImageResource(R.drawable.xing_true);
//                                    imageTwo.setImageResource(R.drawable.xing_false);
//                                    imageThree.setImageResource(R.drawable.xing_false);
//                                    imageFour.setImageResource(R.drawable.xing_false);
//                                    imageFive.setImageResource(R.drawable.xing_false);
//                                }
//                                if(shangJiaPingFenEntity.getService().equals("2")){
//                                    imageOne.setImageResource(R.drawable.xing_true);
//                                    imageTwo.setImageResource(R.drawable.xing_true);
//                                    imageThree.setImageResource(R.drawable.xing_false);
//                                    imageFour.setImageResource(R.drawable.xing_false);
//                                    imageFive.setImageResource(R.drawable.xing_false);
//                                }
//                                if(shangJiaPingFenEntity.getService().equals("3")){
//                                    imageOne.setImageResource(R.drawable.xing_true);
//                                    imageTwo.setImageResource(R.drawable.xing_true);
//                                    imageThree.setImageResource(R.drawable.xing_true);
//                                    imageFour.setImageResource(R.drawable.xing_false);
//                                    imageFive.setImageResource(R.drawable.xing_false);
//                                }
//                                if(shangJiaPingFenEntity.getService().equals("4")){
//                                    imageOne.setImageResource(R.drawable.xing_true);
//                                    imageTwo.setImageResource(R.drawable.xing_true);
//                                    imageThree.setImageResource(R.drawable.xing_true);
//                                    imageFour.setImageResource(R.drawable.xing_true);
//                                    imageFive.setImageResource(R.drawable.xing_false);
//                                }
//                                if(shangJiaPingFenEntity.getService().equals("5")){
//                                    imageOne.setImageResource(R.drawable.xing_true);
//                                    imageTwo.setImageResource(R.drawable.xing_true);
//                                    imageThree.setImageResource(R.drawable.xing_true);
//                                    imageFour.setImageResource(R.drawable.xing_true);
//                                    imageFive.setImageResource(R.drawable.xing_true);
//                                }
//                            }

                            shangJiaPingLunAdapter = new ShangJiaPingLunAdapter(getContext(), shangJiaPingFenEntity.getResponse().getList());
                            LinearLayoutManager manager = new LinearLayoutManager(getContext());
                            recyclerView.setLayoutManager(manager);
                            recyclerView.setAdapter(shangJiaPingLunAdapter);


                        }


                        }
//                    }
                });
    }


    @OnClick({R.id.tv_pingfen, R.id.image_one, R.id.image_two, R.id.image_three, R.id.image_four, R.id.image_five})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.tv_pingfen:
                break;
            case R.id.image_one:
//                isOK(0);
                break;
            case R.id.image_two:
//                isOK(1);
                break;
            case R.id.image_three:
//                isOK(2);
                break;
            case R.id.image_four:
//                isOK(3);
                break;
            case R.id.image_five:
//                isOK(4);
                break;
        }
    }

    private void isOK(int ttt) {
        ArrayList<ImageView> list = new ArrayList();
        list.add(imageOne);
        list.add(imageTwo);
        list.add(imageThree);
        list.add(imageFour);
        list.add(imageFive);

        for (int i = 0; i < list.size(); i++) {
            ImageView sss = list.get(i);
            if (i <= ttt) {
                sss.setImageResource(R.drawable.xing_true);
            } else {
                sss.setImageResource(R.drawable.xing_false);
            }
        }
    }


}
