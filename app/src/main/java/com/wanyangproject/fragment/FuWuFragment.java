package com.wanyangproject.fragment;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.google.gson.Gson;
import com.wanyangproject.R;
import com.wanyangproject.activity.HomeActivity;
import com.wanyangproject.activity.WenZhangXiangQingActivity;
import com.wanyangproject.adapter.BannerAdapter;
import com.wanyangproject.entity.SheZhiYuanQuNameEntity;
import com.wanyangproject.entity.ShouYeBannerEntity;
import com.wanyangproject.entity.XiTongXiaoXiEntity;
import com.wanyangproject.entity.XiTongXiaoXiWeiDuGeShuEntity;
import com.wanyangproject.fuwuactivity.FangKeDengJiActivity;
import com.wanyangproject.fuwuactivity.FuWuMessageActivity;
import com.wanyangproject.fuwuactivity.FuWuShenQingJiLuActivity;
import com.wanyangproject.fuwuactivity.QiYeFuWuActivity;
import com.wanyangproject.fuwuactivity.WuYeFuWuActivity;
import com.wanyangproject.fuwuactivity.WuYeFuWuLieBiaoActivity;
import com.wanyangproject.fuwuactivity.YuanQuTongZhiActivity;
import com.wanyangproject.my.MyBangDingActivity;
import com.wanyangproject.my.MyShouCangActivity;
import com.wanyangproject.my.QuanBuActivity;
import com.wanyangproject.my.RuZhuShenQingActivity;
import com.wanyangproject.my.SettingActivity;
import com.wanyangproject.shouye.ChanYeJinRongActivity;
import com.wanyangproject.shouye.QiuZhiZhaoPinActivity;
import com.wanyangproject.shouye.ShouHuoXinXiActivity;
import com.wanyangproject.shouye.YuanQuQiYeActivity;
import com.wanyangproject.shouye.YuanQuShengHuoActivity;
import com.wanyangproject.shouye.YuanQuShouCeActivity;
import com.wanyangproject.shouye.YuanQuZhaoShangActivity;
import com.wanyangproject.shouye.ZhengCeFaGuiActivity;
import com.wanyangproject.shouye.ZhiNengZhiZaoActivity;
import com.wanyangproject.utils.ContractUtils;
import com.wanyangproject.utils.FuWuTiaoZhuFragment;
import com.wanyangproject.utils.TiaoZhuanFragment;
import com.youth.banner.loader.ImageLoader;
import com.zhy.http.okhttp.OkHttpUtils;
import com.zhy.http.okhttp.callback.StringCallback;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.Call;

/**
 * Created by 甜甜 on 2018/7/11.
 */
//   服务界面
public class FuWuFragment extends Fragment implements ViewPager.OnPageChangeListener {


    @BindView(R.id.image_wuye_fuwu)
    ImageView imageWuyeFuwu;
    @BindView(R.id.image_qiye_fuwu)
    ImageView imageQiyeFuwu;
    @BindView(R.id.image_yuanqu_tongzhi)
    ImageView imageYuanquTongzhi;
    @BindView(R.id.image_fangke_dengji)
    ImageView imageFangkeDengji;
    //    @BindView(R.id.img_one)
//    ImageView imgOne;
//    @BindView(R.id.img_two)
//    ImageView imgTwo;
//    @BindView(R.id.img_three)
//    ImageView imgThree;
    @BindView(R.id.tv_jilu)
    TextView tvJilu;
    @BindView(R.id.image_xiaoxi)
    ImageView imageXiaoxi;
    //    @BindView(R.id.banner)
//    Banner banner;
    @BindView(R.id.tv_xiaoxi_number)
    TextView tvXiaoxiNumber;
    @BindView(R.id.viewPager)
    ViewPager viewPager;
    //    private ViewPager viewPager;
    private TiaoZhuanFragment tiaoZhuanFragment;
    private FuWuTiaoZhuFragment fuWuTiaoZhuFragment;

    private List<View> list = new ArrayList<>();

    private Boolean isOk = true;//    private Handler handler = new Handler() {

    private XiTongXiaoXiEntity xiTongXiaoXiEntity;
    private XiTongXiaoXiWeiDuGeShuEntity xiTongXiaoXiWeiDuGeShuEntity;
    private ShouYeBannerEntity shouYeBannerEntity;
    private BannerAdapter1 adapter1;
    private LinearLayoutManager manager;
    @BindView(R.id.recyclerView_banner)
    RecyclerView recyclerViewBanner;
    private int pos;//下标




    private Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            switch (msg.what) {
                case 1:
                    viewPager.setCurrentItem(msg.arg1);
                    break;
                case 2:
                    viewPager.setCurrentItem(msg.arg1);
                    break;
            }
            if (adapter1 != null) {
                adapter1.notifyDataSetChanged();
            }
        }
    };


//        @Override
//        public void handleMessage(Message msg) {
//            super.handleMessage(msg);
//            switch (msg.what) {
//                case 1:
//                    viewPager.setCurrentItem(msg.arg1);
//                    break;
//                case 2:
//                    viewPager.setCurrentItem(msg.arg1);
//                    break;
//            }
//        }
//    };


    public void setFuWuTiaoZhuFragment(FuWuTiaoZhuFragment fuWuTiaoZhuFragment) {
        this.fuWuTiaoZhuFragment = fuWuTiaoZhuFragment;
    }

    public void setTiaoZhuanFragment(TiaoZhuanFragment tiaoZhuanFragment) {
        this.tiaoZhuanFragment = tiaoZhuanFragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_fuwu, container, false);
//        viewPager = view.findViewById(R.id.viewPager);

//        View viewOne = LayoutInflater.from(getContext()).inflate(R.layout.item_one, null);
//        View viewTwo = LayoutInflater.from(getContext()).inflate(R.layout.item_two, null);
//        View viewThree = LayoutInflater.from(getContext()).inflate(R.layout.item_three, null);
//
//        list.add(viewOne);
//        list.add(viewTwo);
//        list.add(viewThree);
//
//        MyAdapter adapter = new MyAdapter(list);
//        viewPager.setAdapter(adapter);
//
        initThread();
//
//        viewPager.addOnPageChangeListener(this);

        ButterKnife.bind(this, view);
        viewPager.addOnPageChangeListener(this);

        //        首页banner图的网络请求
        initBannerHttp();
        //        首页系统消息的网络请求
        initXiTongXiaoXiHttp();


        //   用户未读消息的个数的网络请求
        initWeiDuXiaoXiHttp();

        return view;
    }


    @Override
    public void onResume() {
        super.onResume();
        //        设置园区接口
        initSheZhiYuanQuHttp();
    }

    //    设置园区接口
    private void initSheZhiYuanQuHttp() {
        OkHttpUtils.post()
                .url(ContractUtils.LOGIN_URL+"user/setDefaultPark")
                .addHeader("token",ContractUtils.getTOKEN(getContext()))
                .addParams("parkId",ContractUtils.getParkId(getContext()))
                .build()
                .execute(new StringCallback() {
                    @Override
                    public void onError(Call call, Exception e, int id) {

                    }

                    @Override
                    public void onResponse(String response, int id) {
                        System.out.println(response+"               设置园区接口");
                        if(response.indexOf("200") != -1){
                            Gson gson = new Gson();
                            SheZhiYuanQuNameEntity sheZhiYuanQuNameEntity = gson.fromJson(response, SheZhiYuanQuNameEntity.class);
                            ContractUtils.setParkId(sheZhiYuanQuNameEntity.getResponse().getParkInfo().getParkId());
                            ContractUtils.setPhone(sheZhiYuanQuNameEntity.getResponse().getUsername());
                            ContractUtils.setYuanquname(sheZhiYuanQuNameEntity.getResponse().getParkInfo().getParkName());
                            ContractUtils.setTypeId(sheZhiYuanQuNameEntity.getResponse().getTypeId());
                            ContractUtils.setTypeId22(sheZhiYuanQuNameEntity.getResponse().getTypeId2());
                        }
                    }
                });
    }













    //    首页系统消息的网络请求
    private void initXiTongXiaoXiHttp() {
        OkHttpUtils.post()
                .url(ContractUtils.LOGIN_URL + "life/message")
                .addHeader("token", ContractUtils.getTOKEN(getContext()))
                .addParams("parkId", ContractUtils.getParkId(getContext()))
                .addParams("page", "1")
                .build()
                .execute(new StringCallback() {
                    @Override
                    public void onError(Call call, Exception e, int id) {

                    }

                    @Override
                    public void onResponse(String response, int id) {
                        ContractUtils.Code500(getContext(), response);
                        System.out.println(response + "       首页系统消息的网络请求");
                        Gson gson = new Gson();
                        xiTongXiaoXiEntity = gson.fromJson(response, XiTongXiaoXiEntity.class);


                    }
                });
    }


    //    用户未读消息的个数的网络请求
    private void initWeiDuXiaoXiHttp() {
        OkHttpUtils.post()
                .url(ContractUtils.LOGIN_URL + "life/messagege")
                .addHeader("token", ContractUtils.getTOKEN(getContext()))
                .addParams("parkId", ContractUtils.getParkId(getContext()))
                .build()
                .execute(new StringCallback() {
                    @Override
                    public void onError(Call call, Exception e, int id) {

                    }

                    @Override
                    public void onResponse(String response, int id) {
                        ContractUtils.Code500(getContext(), response);
                        System.out.println(response + "         未读消息个数的网络请求");
                        Gson gson = new Gson();
                        xiTongXiaoXiWeiDuGeShuEntity = gson.fromJson(response, XiTongXiaoXiWeiDuGeShuEntity.class);
                        Integer number = 0;
                        if (response.indexOf("200") != -1) {
                            number = Integer.parseInt(xiTongXiaoXiWeiDuGeShuEntity.getResponse().getGe());
                            if (number > 0) {
                                tvXiaoxiNumber.setVisibility(View.VISIBLE);
                                tvXiaoxiNumber.setText(xiTongXiaoXiWeiDuGeShuEntity.getResponse().getGe());
                            } else if (number <= 0) {
                                tvXiaoxiNumber.setVisibility(View.GONE);
                            }
                        }
                    }
                });
    }


    //    首页banner图的网络请求
    private void initBannerHttp() {
        final ProgressDialog progressDialog = new ProgressDialog(getContext());
        progressDialog.setTitle("提示");
        progressDialog.setMessage("请等待...");
        progressDialog.setCancelable(false);
        progressDialog.show();
        OkHttpUtils.post()
                .url(ContractUtils.LOGIN_URL + "news/getBanner")
                .addHeader("token", ContractUtils.getTOKEN(getContext()))
                .addParams("parkId", ContractUtils.getParkId(getContext()))
                .addParams("typeId", "4")
                .build()
                .execute(new StringCallback() {
                    @Override
                    public void onError(Call call, Exception e, int id) {
                        progressDialog.dismiss();
                    }

                    @Override
                    public void onResponse(String response, int id) {
                        progressDialog.dismiss();
                        ContractUtils.Code500(getContext(), response);
                        System.out.println(response + "      首页banner图的网络请求");

                        if (response.indexOf("200") != -1) {
                            Gson gson = new Gson();
                            shouYeBannerEntity = gson.fromJson(response, ShouYeBannerEntity.class);
                            list.clear();

                            for (int i = 0; i < shouYeBannerEntity.getResponse().size(); i++) {
                                View view = LayoutInflater.from(getContext()).inflate(R.layout.item_one, null);
                                TextView textView = view.findViewById(R.id.tv_wenzi);
                                textView.setText(shouYeBannerEntity.getResponse().get(i).getId());
                                Glide.with(getContext()).load(shouYeBannerEntity.getResponse().get(i).getImage()).into((ImageView) view.findViewById(R.id.viewpager_image));
                                view.setTag(shouYeBannerEntity.getResponse().get(i).getId());
                                list.add(view);


                                view.findViewById(R.id.viewpager_image).setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
////                                        TextView textView = view.findViewById(R.id.tv_wenzi);
////                                        String trim = textView.getText().toString().trim();
////                                        System.out.println(textView.getId()+"         text");

//                                        System.out.println(pos+"           pos");
//                                        Intent intent = new Intent(getContext(), ZiXunXiangQingActivity.class);
//                                        intent.putExtra("id",shouYeBannerEntity.getResponse().get(pos).getId());
//                                        startActivity(intent);

                                        if (shouYeBannerEntity.getResponse().get(viewPager.getCurrentItem()).getHrefType().equals("0")) {
                                            Intent intenttongzhi = new Intent(getContext(), YuanQuTongZhiActivity.class);
                                            intenttongzhi.putExtra("zhi","zhi");
                                            startActivity(intenttongzhi);

//                                            Intent intent1 = new Intent(getContext(), ChanYeJinRongActivity.class);
//                                            startActivity(intent1);
                                        } else if (shouYeBannerEntity.getResponse().get(viewPager.getCurrentItem()).getHrefType().equals("1")) {
                                            Intent intent1 = new Intent(getContext(), YuanQuShouCeActivity.class);
                                            startActivity(intent1);
                                        } else if (shouYeBannerEntity.getResponse().get(viewPager.getCurrentItem()).getHrefType().equals("2")) {
                                            Intent intent1 = new Intent(getContext(), YuanQuQiYeActivity.class);
                                            startActivity(intent1);
                                        } else if (shouYeBannerEntity.getResponse().get(viewPager.getCurrentItem()).getHrefType().equals("3")) {
                                            Intent intent1 = new Intent(getContext(), ZhengCeFaGuiActivity.class);
                                            startActivity(intent1);
                                        } else if (shouYeBannerEntity.getResponse().get(viewPager.getCurrentItem()).getHrefType().equals("4")) {
                                            Intent intent1 = new Intent(getContext(), ZhiNengZhiZaoActivity.class);
                                            startActivity(intent1);
                                        } else if (shouYeBannerEntity.getResponse().get(viewPager.getCurrentItem()).getHrefType().equals("5")) {
                                            Intent intent1 = new Intent(getContext(), YuanQuZhaoShangActivity.class);
                                            startActivity(intent1);
                                        } else if (shouYeBannerEntity.getResponse().get(viewPager.getCurrentItem()).getHrefType().equals("6")) {
                                            if (tiaoZhuanFragment != null) {
                                                tiaoZhuanFragment.onClick();
                                            }
                                        } else if (shouYeBannerEntity.getResponse().get(viewPager.getCurrentItem()).getHrefType().equals("7")) {
                                            if (fuWuTiaoZhuFragment != null) {
                                                fuWuTiaoZhuFragment.onClick1();
                                            }
                                        } else if (shouYeBannerEntity.getResponse().get(viewPager.getCurrentItem()).getHrefType().equals("8")) {
                                            Intent intent = new Intent(getContext(), HomeActivity.class);
                                            startActivity(intent);
                                        } else if (shouYeBannerEntity.getResponse().get(viewPager.getCurrentItem()).getHrefType().equals("9")) {
                                            if (shouYeBannerEntity.getResponse().get(viewPager.getCurrentItem()).getContent() != null) {
                                                Intent intent = new Intent(getContext(), WenZhangXiangQingActivity.class);
                                                intent.putExtra("content", shouYeBannerEntity.getResponse().get(viewPager.getCurrentItem()).getContent());
                                                startActivity(intent);
                                            }
                                        } else if (shouYeBannerEntity.getResponse().get(viewPager.getCurrentItem()).getHrefType().equals("10")) {
                                            if (shouYeBannerEntity.getResponse().get(viewPager.getCurrentItem()).getContent() != null) {
                                                Intent intent = new Intent(getContext(), YuanQuShengHuoActivity.class);
                                                startActivity(intent);
                                            }
                                        } else if (shouYeBannerEntity.getResponse().get(viewPager.getCurrentItem()).getHrefType().equals("11")) {
                                            if (shouYeBannerEntity.getResponse().get(viewPager.getCurrentItem()).getContent() != null) {
                                                return;
                                            }
                                        } else if (shouYeBannerEntity.getResponse().get(viewPager.getCurrentItem()).getHrefType().equals("12")) {
                                            if (shouYeBannerEntity.getResponse().get(viewPager.getCurrentItem()).getContent() != null) {
                                                Intent intent = new Intent(getContext(), SettingActivity.class);
                                                startActivity(intent);
                                            }
                                        } else if (shouYeBannerEntity.getResponse().get(viewPager.getCurrentItem()).getHrefType().equals("13")) {
                                            if (shouYeBannerEntity.getResponse().get(viewPager.getCurrentItem()).getContent() != null) {
                                                Intent intent = new Intent(getContext(), QiuZhiZhaoPinActivity.class);
                                                startActivity(intent);
                                            }
                                        } else if (shouYeBannerEntity.getResponse().get(viewPager.getCurrentItem()).getHrefType().equals("14")) {
                                            if (shouYeBannerEntity.getResponse().get(viewPager.getCurrentItem()).getContent() != null) {
                                                Intent intent = new Intent(getContext(), ChanYeJinRongActivity.class);
                                                startActivity(intent);
                                            }
                                        } else if (shouYeBannerEntity.getResponse().get(viewPager.getCurrentItem()).getHrefType().equals("15")) {
                                            if (shouYeBannerEntity.getResponse().get(viewPager.getCurrentItem()).getContent() != null) {
                                                Intent intent = new Intent(getContext(), YuanQuTongZhiActivity.class);
                                                startActivity(intent);
                                            }
                                        } else if (shouYeBannerEntity.getResponse().get(viewPager.getCurrentItem()).getHrefType().equals("16")) {
                                            if (shouYeBannerEntity.getResponse().get(viewPager.getCurrentItem()).getContent() != null) {
                                                Intent intent = new Intent(getContext(), FangKeDengJiActivity.class);
                                                startActivity(intent);
                                            }
                                        } else if (shouYeBannerEntity.getResponse().get(viewPager.getCurrentItem()).getHrefType().equals("17")) {
                                            if (shouYeBannerEntity.getResponse().get(viewPager.getCurrentItem()).getContent() != null) {
                                                Intent intent = new Intent(getContext(), QuanBuActivity.class);
                                                intent.putExtra("daizhifu", "0");
                                                startActivity(intent);
                                            }
                                        } else if (shouYeBannerEntity.getResponse().get(viewPager.getCurrentItem()).getHrefType().equals("18")) {
                                            if (shouYeBannerEntity.getResponse().get(viewPager.getCurrentItem()).getContent() != null) {
                                                Intent intent = new Intent(getContext(), MyShouCangActivity.class);
                                                startActivity(intent);
                                            }
                                        } else if (shouYeBannerEntity.getResponse().get(viewPager.getCurrentItem()).getHrefType().equals("19")) {
                                            if (shouYeBannerEntity.getResponse().get(viewPager.getCurrentItem()).getContent() != null) {
                                                Intent intent = new Intent(getContext(), ShouHuoXinXiActivity.class);
                                                startActivity(intent);
                                            }
                                        } else if (shouYeBannerEntity.getResponse().get(viewPager.getCurrentItem()).getHrefType().equals("20")) {
                                            if (shouYeBannerEntity.getResponse().get(viewPager.getCurrentItem()).getContent() != null) {
                                                Intent intent = new Intent(getContext(), RuZhuShenQingActivity.class);
                                                startActivity(intent);
                                            }
                                        } else if (shouYeBannerEntity.getResponse().get(viewPager.getCurrentItem()).getHrefType().equals("21")) {
                                            if (shouYeBannerEntity.getResponse().get(viewPager.getCurrentItem()).getContent() != null) {
                                                Intent intent = new Intent(getContext(), MyBangDingActivity.class);
                                                startActivity(intent);
                                            }
                                        }
                                    }
                                });

                            }


                            BannerAdapter adapter = new BannerAdapter(getContext(), list);
                            viewPager.setAdapter(adapter);
                            adapter1 = new BannerAdapter1();
                            manager = new LinearLayoutManager(getContext());
                            manager.setOrientation(LinearLayoutManager.HORIZONTAL);
                            recyclerViewBanner.setLayoutManager(manager);
                            recyclerViewBanner.setAdapter(adapter1);







//                            ArrayList<String> list = new ArrayList<String>();
//                            for (int i = 0; i < shouYeBannerEntity.getResponse().size(); i++) {
//                                ShouYeBannerEntity.ResponseBean responseBean = shouYeBannerEntity.getResponse().get(i);
//                                list.add(responseBean.getImage());
//                            }

//                            banner.setBannerStyle(BannerConfig.CIRCLE_INDICATOR);
////                            banner.setBannerStyle(BannerConfig.NUM_INDICATOR);
//                            banner.setImageLoader(new MyImageLoader());
//                            banner.setImages(list);
//                            banner.setDelayTime(2000);
////                            BannerConfig.CENTER
////                            banner.setIndicatorGravity(BannerConfig.TITLE_HEIGHT);
//                            banner.setIndicatorGravity(BannerConfig.CENTER);
//                            banner.start();
//
//                            banner.setOnBannerListener(new OnBannerListener() {
//                                @Override
//                                public void OnBannerClick(int position) {
////                                    Intent intent = new Intent(getContext(), ZiXunXiangQingActivity.class);
////                                    intent.putExtra("id",shouYeBannerEntity.getResponse().get(position).getId());
////                                    startActivity(intent);
//
//                                    if (shouYeBannerEntity.getResponse().get(position).getHrefType().equals("0")){
//                                        Intent intent1 = new Intent(getContext(),ChanYeJinRongActivity.class);
//                                        startActivity(intent1);
//                                    }else if(shouYeBannerEntity.getResponse().get(position).getHrefType().equals("1")){
//                                        Intent intent1 = new Intent(getContext(),YuanQuShouCeActivity.class);
//                                        startActivity(intent1);
//                                    }else if(shouYeBannerEntity.getResponse().get(position).getHrefType().equals("2")){
//                                        Intent intent1 = new Intent(getContext(),YuanQuQiYeActivity.class);
//                                        startActivity(intent1);
//                                    }else if(shouYeBannerEntity.getResponse().get(position).getHrefType().equals("3")){
//                                        Intent intent1 = new Intent(getContext(),ZhengCeFaGuiActivity.class);
//                                        startActivity(intent1);
//                                    }else if(shouYeBannerEntity.getResponse().get(position).getHrefType().equals("4")){
//                                        Intent intent1 = new Intent(getContext(),ZhiNengZhiZaoActivity.class);
//                                        startActivity(intent1);
//                                    }else if(shouYeBannerEntity.getResponse().get(position).getHrefType().equals("5")){
//                                        Intent intent1 = new Intent(getContext(),YuanQuZhaoShangActivity.class);
//                                        startActivity(intent1);
//                                    }else if(shouYeBannerEntity.getResponse().get(position).getHrefType().equals("6")) {
//                                        if (tiaoZhuanFragment != null) {
//                                            tiaoZhuanFragment.onClick();
//                                    }
//                                    } else if (shouYeBannerEntity.getResponse().get(position).getHrefType().equals("7")) {
//                                        if (fuWuTiaoZhuFragment != null) {
//                                            fuWuTiaoZhuFragment.onClick1();
//                                        }
//                                    } else if (shouYeBannerEntity.getResponse().get(position).getHrefType().equals("8")) {
//                                        Intent intent = new Intent(getContext(), HomeActivity.class);
//                                        startActivity(intent);
//                                    } else if(shouYeBannerEntity.getResponse().get(position).getHrefType().equals("9")){
//                                        if(shouYeBannerEntity.getResponse().get(position).getContent() != null){
//                                            Intent intent = new Intent(getActivity(), WenZhangXiangQingActivity.class);
//                                            intent.putExtra("content",shouYeBannerEntity.getResponse().get(position).getContent());
//                                            startActivity(intent);
//                                        }
//                                    }else if(shouYeBannerEntity.getResponse().get(position).getHrefType().equals("10")){
//                                        if(shouYeBannerEntity.getResponse().get(position).getContent() != null){
//                                            Intent intent = new Intent(getContext(), YuanQuShengHuoActivity.class);
//                                            startActivity(intent);
//                                        }
//                                    }else if(shouYeBannerEntity.getResponse().get(position).getHrefType().equals("11")){
//                                        if(shouYeBannerEntity.getResponse().get(position).getContent() != null){
//                                            return;
//                                        }
//                                    }else if(shouYeBannerEntity.getResponse().get(position).getHrefType().equals("12")){
//                                        if(shouYeBannerEntity.getResponse().get(position).getContent() != null){
//                                            Intent intent = new Intent(getContext(),SettingActivity.class);
//                                            startActivity(intent);
//                                        }
//                                    }else if(shouYeBannerEntity.getResponse().get(position).getHrefType().equals("13")){
//                                        if(shouYeBannerEntity.getResponse().get(position).getContent() != null){
//                                            Intent intent = new Intent(getContext(),QiuZhiZhaoPinActivity.class);
//                                            startActivity(intent);
//                                        }
//                                    }else if(shouYeBannerEntity.getResponse().get(position).getHrefType().equals("14")){
//                                        if(shouYeBannerEntity.getResponse().get(position).getContent() != null){
//                                            Intent intent = new Intent(getContext(),ChanYeJinRongActivity.class);
//                                            startActivity(intent);
//                                        }
//                                    }else if(shouYeBannerEntity.getResponse().get(position).getHrefType().equals("15")){
//                                        if(shouYeBannerEntity.getResponse().get(position).getContent() != null){
//                                            Intent intent = new Intent(getContext(),YuanQuTongZhiActivity.class);
//                                            startActivity(intent);
//                                        }
//                                    }else if(shouYeBannerEntity.getResponse().get(position).getHrefType().equals("16")){
//                                        if(shouYeBannerEntity.getResponse().get(position).getContent() != null){
//                                            Intent intent = new Intent(getContext(),FangKeDengJiActivity.class);
//                                            startActivity(intent);
//                                        }
//                                    }else if(shouYeBannerEntity.getResponse().get(position).getHrefType().equals("17")){
//                                        if(shouYeBannerEntity.getResponse().get(position).getContent() != null){
//                                            Intent intent = new Intent(getContext(),QuanBuActivity.class);
//                                            intent.putExtra("daizhifu","0");
//                                            startActivity(intent);
//                                        }
//                                    }else if(shouYeBannerEntity.getResponse().get(position).getHrefType().equals("18")){
//                                        if(shouYeBannerEntity.getResponse().get(position).getContent() != null){
//                                            Intent intent = new Intent(getContext(),MyShouCangActivity.class);
//                                            startActivity(intent);
//                                        }
//                                    }else if(shouYeBannerEntity.getResponse().get(position).getHrefType().equals("19")){
//                                        if(shouYeBannerEntity.getResponse().get(position).getContent() != null){
//                                            Intent intent = new Intent(getContext(),ShouHuoXinXiActivity.class);
//                                            startActivity(intent);
//                                        }
//                                    }else if(shouYeBannerEntity.getResponse().get(position).getHrefType().equals("20")){
//                                        if(shouYeBannerEntity.getResponse().get(position).getContent() != null){
//                                            Intent intent = new Intent(getContext(),RuZhuShenQingActivity.class);
//                                            startActivity(intent);
//                                        }
//                                    }else if(shouYeBannerEntity.getResponse().get(position).getHrefType().equals("21")){
//                                        if(shouYeBannerEntity.getResponse().get(position).getContent() != null){
//                                            Intent intent = new Intent(getContext(),MyBangDingActivity.class);
//                                            startActivity(intent);
//                                        }
//                                    }
//                                    }
//                            });
//                            banner.start();

                        }
                    }
                });
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {
        pos = position;

        if (adapter1!=null){
            adapter1.notifyDataSetChanged();
        }
    }


    @Override
    public void onPageScrollStateChanged(int state) {

    }

    public class MyImageLoader extends ImageLoader {
        @Override
        public void displayImage(Context context, Object path, ImageView imageView) {
            Glide.with(context).load(path).into(imageView);
        }
    }






    class BannerAdapter1 extends RecyclerView.Adapter<BannerAdapter1.ViewHolder> {

        @Override
        public BannerAdapter1.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(getContext()).inflate(R.layout.item_banner_dian, parent, false);
            BannerAdapter1.ViewHolder viewHolder = new BannerAdapter1.ViewHolder(view);
            return viewHolder;
        }

        @Override
        public void onBindViewHolder(BannerAdapter1.ViewHolder holder, int position) {
            if (viewPager.getCurrentItem() == position) {
                holder.img_two.setImageResource(R.drawable.yuanhong);
            } else {
                holder.img_two.setImageResource(R.drawable.yuanhui);
            }
        }

        @Override
        public int getItemCount() {
            return list.size();
        }


        public class ViewHolder extends RecyclerView.ViewHolder {
            private ImageView img_two;

            public ViewHolder(View itemView) {
                super(itemView);
                img_two = itemView.findViewById(R.id.img_two);
            }
        }
    }



    private void initThread() {
        new Thread() {
            @Override
            public void run() {
                super.run();
                try {
                    while (isOk) {
                        Thread.sleep(3000);
                        Message message = Message.obtain();
                        int currentItem = viewPager.getCurrentItem();
                        if (currentItem == 2) {
                            currentItem = 0;
                            message.what = 1;
                            message.arg1 = currentItem;
                            handler.sendMessage(message);
                        } else {
                            message.what = 2;
                            message.arg1 = currentItem + 1;
                            handler.sendMessage(message);
                        }
                    }
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }.start();
    }


    @OnClick({R.id.tv_jilu, R.id.image_xiaoxi, R.id.image_wuye_fuwu, R.id.image_qiye_fuwu, R.id.image_yuanqu_tongzhi, R.id.image_fangke_dengji})
    public void onViewClicked(View view) {
        switch (view.getId()) {
//            申请记录
            case R.id.tv_jilu:
                Intent intentjilu = new Intent(getContext(), FuWuShenQingJiLuActivity.class);
                intentjilu.putExtra("wode", "fuwu");
                startActivity(intentjilu);
                break;
//            消息
            case R.id.image_xiaoxi:
                Intent intent = new Intent(getContext(), FuWuMessageActivity.class);
                startActivity(intent);
                break;
//            物业服务
            case R.id.image_wuye_fuwu:
                Intent intentwuye = new Intent(getContext(), WuYeFuWuLieBiaoActivity.class);
//                Intent intentwuye = new Intent(getContext(), WuYeFuWuActivity.class);
                startActivity(intentwuye);
                break;
//            企业服务
            case R.id.image_qiye_fuwu:
                Intent intentqiye = new Intent(getContext(), QiYeFuWuActivity.class);
                startActivity(intentqiye);
                break;
//            园区通知
            case R.id.image_yuanqu_tongzhi:
                Intent intenttongzhi = new Intent(getContext(), YuanQuTongZhiActivity.class);
                startActivity(intenttongzhi);
                break;
//            访客登记
            case R.id.image_fangke_dengji:
                Intent intentfangke = new Intent(getContext(), FangKeDengJiActivity.class);
                startActivity(intentfangke);
                break;
        }
    }


//    @Override
//    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
//
//    }
//
//    @Override
//    public void onPageSelected(int position) {
//        clearState();
//        switch (position) {
//            case 0:
//                imgOne.setImageResource(R.drawable.radio_true);
//                break;
//            case 1:
//                imgTwo.setImageResource(R.drawable.radio_true);
//                break;
//            case 2:
//                imgThree.setImageResource(R.drawable.radio_true);
//                break;
//        }
//    }
//
//    @Override
//    public void onPageScrollStateChanged(int state) {
//
//    }
//
//    public void clearState() {
//        imgOne.setImageResource(R.drawable.radio_false);
//        imgTwo.setImageResource(R.drawable.radio_false);
//        imgThree.setImageResource(R.drawable.radio_false);
//    }
}
