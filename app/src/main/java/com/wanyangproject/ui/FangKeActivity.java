package com.wanyangproject.ui;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;

import com.wanyangproject.R;
import com.wanyangproject.adapter.VisitorAdapter;
import com.wanyangproject.base.BaseActivity;
import com.wanyangproject.model.TestModel;
import com.wanyangproject.net.util.ProgressSubscriber;
import com.wanyangproject.net.util.SwipeRefreshUtil;
import com.wanyangproject.widget.recyclerview.IRecyclerSwipe;
import com.wanyangproject.widget.recyclerview.refresh.OnLoadMoreListener;
import com.wanyangproject.widget.recyclerview.refresh.RecyclerViewWithFooter;

import java.util.List;

public class FangKeActivity extends BaseActivity implements IRecyclerSwipe, SwipeRefreshLayout.OnRefreshListener, OnLoadMoreListener {

    private SwipeRefreshLayout swipeRefresh;
//    private RecyclerViewWithFooter swipeRecycler;
    private RecyclerView swipeRecycler;
    private int page = 0;
    private final int COUNT = 10;
    private boolean isRefresh = false;
    private VisitorAdapter visitorAdapter;

    public static void start(Context context) {
        context.startActivity(new Intent(context, FangKeActivity.class));
    }

    @Override
    public int getContentResId() {
        return R.layout.activity_company_join;
    }

    @Override
    public void init() {

        if (Build.VERSION.SDK_INT >= 21) {
            View dview = getWindow().getDecorView();
            dview.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN | View.SYSTEM_UI_FLAG_LAYOUT_STABLE);
            getWindow().setStatusBarColor(Color.TRANSPARENT);
        }

        ImageButton barBack = (ImageButton) findViewById(R.id.bar_back);
        TextView barTitle = (TextView) findViewById(R.id.bar_title);
        TextView barRight = (TextView) findViewById(R.id.bar_right);
        barBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        barTitle.setText("访客管理");
        barRight.setText("添加访客");

        swipeRefresh = (SwipeRefreshLayout) findViewById(R.id.swipe_refresh);
        swipeRecycler = (RecyclerView) findViewById(R.id.swipe_recycler);

        SwipeRefreshUtil.setColors(swipeRefresh);

        swipeRecycler.setLayoutManager(new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false));
        visitorAdapter = new VisitorAdapter(mContext, null);
        swipeRecycler.setAdapter(visitorAdapter);

        swipeRefresh.setOnRefreshListener(this);
//        swipeRecycler.setOnLoadMoreListener(this);

        getData(0);
    }

    private void getData(int page) {
//        mHttpUtil.getData(new ProgressSubscriber<TestModel>(mContext, this) {
//            @Override
//            public void next(TestModel testModel) {
//                List<TestModel.ResultsBean> results = testModel.getResults();
//                if (results != null) {
//                    int size = results.size();
//                    if (size > 0) {
//                        if (visitorAdapter != null) {
//                            if (isRefresh) {
//                                visitorAdapter.setData(results);
//                            } else {
//                                visitorAdapter.addData(results);
//                            }
//                        }
//                        if (size < COUNT) {
//                            setNoMore();
//                        }
//                    } else {
//                        setEnd();
//                    }
//                } else {
//                    setEnd();
//                }
//
//            }
//        }, COUNT, page);
    }

    private void setEnd() {
        if (visitorAdapter != null) {
            visitorAdapter.clearData();
            visitorAdapter.notifyDataSetChanged();
//            swipeRecycler.setEnd("获取数据为空");
        }
    }

    @Override
    public void showItemFail(String msg) {

    }

    @Override
    public void hideSwipeLoading() {
        swipeRefresh.setRefreshing(false);
    }

    @Override
    public void setNoMore() {
//        swipeRecycler.setEnd("没有更多数据");
    }

    @Override
    public void onRefresh() {
        isRefresh = true;
        page = 0;
        getData(page);
    }

    @Override
    public void onLoadMore() {
        isRefresh = false;
        page++;
        getData(page);
    }
}