package com.wanyangproject.ui;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;


import com.wanyangproject.R;
import com.wanyangproject.adapter.RecruitAdapter;
import com.wanyangproject.base.BaseActivity;

import java.util.ArrayList;
import java.util.List;

public class ZhaoPinActivity extends BaseActivity {

    public static void start(Context context) {
        context.startActivity(new Intent(context, ZhaoPinActivity.class));
    }

    @Override
    public int getContentResId() {
        return R.layout.recycler_view;
    }

    @Override
    public void init() {

        if (Build.VERSION.SDK_INT >= 21) {
            View dview = getWindow().getDecorView();
            dview.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN | View.SYSTEM_UI_FLAG_LAYOUT_STABLE);
            getWindow().setStatusBarColor(Color.TRANSPARENT);
        }


        ImageButton barBack = (ImageButton) findViewById(R.id.bar_back);
        TextView barTitle = (TextView) findViewById(R.id.bar_title);
        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.recycler_view);

        barBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        barTitle.setText("求职招聘");

        List<String> recruitList = new ArrayList<>();
        recruitList.add("服务员");
        recruitList.add("Android开发工程师");
        recruitList.add("公司前台");
        recruitList.add("保洁");
        recruitList.add("快餐配送员");
        recruitList.add("保安");
        recruitList.add("秘书");
        recruitList.add("项目经理");
        recruitList.add("产品经理");

        recyclerView.setLayoutManager(new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false));
        recyclerView.setAdapter(new RecruitAdapter(mContext, recruitList));
        recyclerView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(ZhaoPinActivity.this, JobDetailsActivity.class));
            }
        });
    }
}