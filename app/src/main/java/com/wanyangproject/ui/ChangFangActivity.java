package com.wanyangproject.ui;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;


import com.wanyangproject.R;
import com.wanyangproject.adapter.HouseAdapter;
import com.wanyangproject.base.BaseActivity;
import com.wanyangproject.model.HouseModel;

import java.util.ArrayList;
import java.util.List;

public class ChangFangActivity extends BaseActivity {

    public static void start(Context context) {
        context.startActivity(new Intent(context, ChangFangActivity.class));
    }

    @Override
    public int getContentResId() {
        return R.layout.recycler_view;
    }




    @Override
    public void init() {

        if (Build.VERSION.SDK_INT >= 21) {
            View dview = getWindow().getDecorView();
            dview.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN | View.SYSTEM_UI_FLAG_LAYOUT_STABLE);
            getWindow().setStatusBarColor(Color.TRANSPARENT);
        }

        ImageButton barBack = (ImageButton) findViewById(R.id.bar_back);
        TextView barTitle = (TextView) findViewById(R.id.bar_title);
        ImageButton barSearch = (ImageButton) findViewById(R.id.bar_search);
        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.recycler_view);

        barBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        barTitle.setText("厂房管理");
        barSearch.setImageResource(R.mipmap.sousuo);
        barSearch.setVisibility(View.VISIBLE);

        List<HouseModel> houseList = new ArrayList<>();
        HouseModel house1 = new HouseModel();
        house1.setName("龙芯电子 F07#3-401");
        house1.setFloor("F07#3");
        house1.setDoor("401");
        house1.setBlock("F-07");
        houseList.add(house1);
        HouseModel house2 = new HouseModel();
        house2.setName("龙芯电子 F07#4-402");
        house2.setFloor("F07#4");
        house2.setDoor("402");
        house2.setBlock("F-07");
        houseList.add(house2);

        recyclerView.setLayoutManager(new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false));
        recyclerView.setAdapter(new HouseAdapter(mContext, houseList));
    }
}