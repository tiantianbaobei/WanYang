package com.wanyangproject.ui;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.view.View;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.wanyangproject.R;
import com.wanyangproject.base.BaseActivity;


public class CompanyMgrActivity extends BaseActivity implements View.OnClickListener {

    public static void start(Context context) {
        context.startActivity(new Intent(context, CompanyMgrActivity.class));
    }

    @Override
    public int getContentResId() {
        return R.layout.activity_company_mgr;
    }

    @Override
    public void init() {

        if (Build.VERSION.SDK_INT >= 21) {
            View dview = getWindow().getDecorView();
            dview.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN | View.SYSTEM_UI_FLAG_LAYOUT_STABLE);
            getWindow().setStatusBarColor(Color.TRANSPARENT);
        }

        ImageButton barBack = (ImageButton) findViewById(R.id.bar_back);
        TextView barTitle = (TextView) findViewById(R.id.bar_title);
        barBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        barTitle.setText("企业管理");

        RelativeLayout companyInfo = (RelativeLayout) findViewById(R.id.company_info);
        companyInfo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                CompanyDetailActivity.start(mContext);
            }
        });

        TextView companyMgrSd = (TextView) findViewById(R.id.company_mgr_sd);
        TextView companyMgrFk = (TextView) findViewById(R.id.company_mgr_fk);
        TextView companyMgrCl = (TextView) findViewById(R.id.company_mgr_cl);
        TextView companyMgrCf = (TextView) findViewById(R.id.company_mgr_cf);
        TextView companyMgrSs = (TextView) findViewById(R.id.company_mgr_ss);
        TextView companyMgrZp = (TextView) findViewById(R.id.company_mgr_zp);
        companyMgrSd.setOnClickListener(this);
        companyMgrFk.setOnClickListener(this);
        companyMgrCl.setOnClickListener(this);
        companyMgrCf.setOnClickListener(this);
        companyMgrSs.setOnClickListener(this);
        companyMgrZp.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.company_mgr_sd:
                ShuiDianActivity.start(mContext);
                break;
            case R.id.company_mgr_fk:
                FangKeActivity.start(mContext);
                break;
            case R.id.company_mgr_cl:
                CheLiangActivity.start(mContext);
                break;
            case R.id.company_mgr_cf:
                ChangFangActivity.start(mContext);
                break;
            case R.id.company_mgr_ss:
                DormMgrActivity.start(mContext);
                break;
            case R.id.company_mgr_zp:
                ZhaoPinActivity.start(mContext);
                break;
        }
    }
}