package com.wanyangproject.ui;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

import com.wanyangproject.R;
import com.wanyangproject.base.BaseActivity;
import com.wanyangproject.shouye.ZhiFuActivity;


public class OneCardActivity extends BaseActivity implements View.OnClickListener {

    private Button oneCard50;
    private Button oneCard100;
    private Button oneCard200;
    private Button btn_queren;

    public static void start(Context context) {
        context.startActivity(new Intent(context, OneCardActivity.class));
    }

    @Override
    public int getContentResId() {
        return R.layout.activity_one_card;
    }

    @Override
    public void init() {

        if (Build.VERSION.SDK_INT >= 21) {
            View dview = getWindow().getDecorView();
            dview.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN | View.SYSTEM_UI_FLAG_LAYOUT_STABLE);
            getWindow().setStatusBarColor(Color.TRANSPARENT);
        }

        ImageButton barBack = (ImageButton) findViewById(R.id.bar_back);
        TextView barTitle = (TextView) findViewById(R.id.bar_title);
        TextView barRight = (TextView) findViewById(R.id.bar_right);
        btn_queren = (Button) findViewById(R.id.btn_queren);
        btn_queren.setOnClickListener(this);
        barBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        barTitle.setText("一卡通");
        barRight.setText("充值记录");
        barRight.setVisibility(View.VISIBLE);

        oneCard50 = (Button) findViewById(R.id.one_card_50);
        oneCard100 = (Button) findViewById(R.id.one_card_100);
        oneCard200 = (Button) findViewById(R.id.one_card_200);
        oneCard50.setOnClickListener(this);
        oneCard100.setOnClickListener(this);
        oneCard200.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.one_card_50:
                setFastRecharge(oneCard50, true);
                setFastRecharge(oneCard100, false);
                setFastRecharge(oneCard200, false);
                break;
            case R.id.one_card_100:
                setFastRecharge(oneCard50, false);
                setFastRecharge(oneCard100, true);
                setFastRecharge(oneCard200, false);
                break;
            case R.id.one_card_200:
                setFastRecharge(oneCard50, false);
                setFastRecharge(oneCard100, false);
                setFastRecharge(oneCard200, true);
                break;
            case R.id.btn_queren:
                startActivity(new Intent(OneCardActivity.this, ZhiFuActivity.class));
                break;
        }
    }

    private void setFastRecharge(Button button, boolean isChecked) {
        if (isChecked) {
            button.setTextColor(getResources().getColor(R.color.white));
            button.setBackgroundResource(R.drawable.shape_red_bg);
        } else {
            button.setTextColor(getResources().getColor(R.color.red));
            button.setBackgroundResource(R.drawable.shape_red_border);
        }
    }
}
