package com.wanyangproject.ui;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewCompat;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.TextView;


import com.wanyangproject.R;
import com.wanyangproject.adapter.TabsPagerAdapter;
import com.wanyangproject.base.BaseActivity;
import com.wanyangproject.net.util.DensityUtil;
import com.wanyangproject.net.util.FragmentUtil;
import com.wanyangproject.net.util.ScreenUtil;
import com.wanyangproject.widget.tabs.TabsIndicator;

import java.util.List;

public class    CollectActivity extends BaseActivity implements ViewPager.OnPageChangeListener {

    private RelativeLayout indicatorLine;
    private View indicator;
    private List<Fragment> collectFragment;

    public static void start(Context context) {
        Intent intent = new Intent(context, CollectActivity.class);
        context.startActivity(intent);
    }

    @Override
    public int getContentResId() {
        return R.layout.activity_collect;
    }

    @Override
    public void init() {
        if (Build.VERSION.SDK_INT >= 21) {
            View dview = getWindow().getDecorView();
            dview.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN | View.SYSTEM_UI_FLAG_LAYOUT_STABLE);
            getWindow().setStatusBarColor(Color.TRANSPARENT);
        }


        ImageButton barBack = (ImageButton) findViewById(R.id.bar_back);
        TextView barTitle = (TextView) findViewById(R.id.bar_title);
        TabsIndicator collectTabs = (TabsIndicator) findViewById(R.id.collect_tabs);
        ViewPager collectPager = (ViewPager) findViewById(R.id.collect_pager);
        indicatorLine = (RelativeLayout) findViewById(R.id.indicator_line);
        indicator = findViewById(R.id.indicator);
        collectFragment = FragmentUtil.getCollectFragment();
        collectPager.setAdapter(new TabsPagerAdapter(getSupportFragmentManager(), collectFragment));
        collectPager.addOnPageChangeListener(this);
        collectPager.setOffscreenPageLimit(4);
        collectTabs.setViewPager(collectPager);
        barTitle.setText("我的收藏");
        barBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        initIndicator();
    }

    private void initIndicator() {
        indicatorLine.getLayoutParams().width = ScreenUtil.getScreenWidth(mContext) / collectFragment.size();
        indicatorLine.requestLayout();
        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT
        );
        int interval = DensityUtil.dp2px(mContext, 35);
        params.setMargins(interval, 0, interval, 0);
        indicator.setLayoutParams(params);
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
        float offsetX = positionOffset * indicatorLine.getWidth();
        int startX = position * indicatorLine.getWidth();
        float translationX = startX + offsetX;
        ViewCompat.setTranslationX(indicatorLine, translationX);
    }

    @Override
    public void onPageSelected(int position) {

    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }
}
