package com.wanyangproject.ui;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;

import com.wanyangproject.R;
import com.wanyangproject.adapter.EmployMgrAdapter;
import com.wanyangproject.base.BaseActivity;
import com.wanyangproject.model.EmployModel;
import com.wanyangproject.widget.suspensionindexbar.IndexBar;

import java.util.ArrayList;
import java.util.List;

public class EmployMgrActivity extends BaseActivity {

    private RecyclerView employRecycler;
    private IndexBar employIndexBar;
    private TextView employHintBar;

    public static void start(Context context) {
        context.startActivity(new Intent(context, EmployMgrActivity.class));
    }

    @Override
    public int getContentResId() {
        return R.layout.activity_employ_mgr;
    }

    @Override
    public void init() {

        if (Build.VERSION.SDK_INT >= 21) {
            View dview = getWindow().getDecorView();
            dview.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN | View.SYSTEM_UI_FLAG_LAYOUT_STABLE);
            getWindow().setStatusBarColor(Color.TRANSPARENT);
        }

        ImageButton barBack = (ImageButton) findViewById(R.id.bar_back);
        TextView barTitle = (TextView) findViewById(R.id.bar_title);
        ImageButton barSearch = (ImageButton) findViewById(R.id.bar_search);
        barBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        barTitle.setText("员工管理");
        barSearch.setVisibility(View.VISIBLE);

        employRecycler = (RecyclerView) findViewById(R.id.employ_recycler);
        employIndexBar = (IndexBar) findViewById(R.id.employ_index_bar);
        employHintBar = (TextView) findViewById(R.id.employ_hint_bar);

        getData();
    }

    private void getData() {
        List<EmployModel> results = new ArrayList<>();
        results.add(new EmployModel("陈希鹏", "男"));
        results.add(new EmployModel("白洪锐", "男"));
        results.add(new EmployModel("颜双双", "女"));
        results.add(new EmployModel("叶信斌", "男"));
        results.add(new EmployModel("王威", "男"));
        results.add(new EmployModel("胡丹", "男"));
        results.add(new EmployModel("汪丽", "女"));
        results.add(new EmployModel("冯先红", "女"));
        results.add(new EmployModel("汪华伟", "男"));
        results.add(new EmployModel("秦周", "女"));
        results.add(new EmployModel("周兰平", "女"));
        results.add(new EmployModel("段徽畅", "男"));
        results.add(new EmployModel("全超", "男"));
        results.add(new EmployModel("沙汉泼", "男"));
        results.add(new EmployModel("易丹", "女"));
        results.add(new EmployModel("罗虹", "男"));
        results.add(new EmployModel("李景", "男"));
        results.add(new EmployModel("金李", "男"));
        results.add(new EmployModel("文小念", "男"));
        results.add(new EmployModel("张森龙", "男"));
        results.add(new EmployModel("王石华", "男"));
        results.add(new EmployModel("熊余婷", "女"));
        results.add(new EmployModel("刘宁", "女"));
        results.add(new EmployModel("陈聪", "女"));
        results.add(new EmployModel("林龙聪", "男"));
        results.add(new EmployModel("吴文波", "男"));
        results.add(new EmployModel("蔡正旺", "男"));
        results.add(new EmployModel("邹全武", "男"));
        results.add(new EmployModel("位俊杰", "女"));
        results.add(new EmployModel("夏廷芳", "女"));
        results.add(new EmployModel("刘敏", "女"));
        results.add(new EmployModel("张娟", "女"));
        results.add(new EmployModel("杨昌柳", "女"));
        results.add(new EmployModel("艾涛", "男"));
        results.add(new EmployModel("杨棚", "男"));
        results.add(new EmployModel("杨涛", "男"));
        results.add(new EmployModel("陆兰珍", "女"));
        results.add(new EmployModel("邹洪波", "男"));
        LinearLayoutManager layoutManager = new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false);
        employRecycler.setLayoutManager(layoutManager);
        EmployMgrAdapter employMgrAdapter = new EmployMgrAdapter(mContext, results);
        employRecycler.setAdapter(employMgrAdapter);
        employIndexBar.setmPressedShowTextView(employHintBar)
                .setNeedRealIndex(true)
                .setmLayoutManager(layoutManager);
        employIndexBar.getDataHelper().sortSourceDatas(results);
        employIndexBar.setmSourceDatas(results).invalidate();
    }
}