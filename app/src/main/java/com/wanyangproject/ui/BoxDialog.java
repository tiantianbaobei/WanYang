package com.wanyangproject.ui;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.view.View;
import android.widget.Button;


import com.wanyangproject.R;
import com.wanyangproject.base.BaseActivity;
import com.wanyangproject.model.EventModel;
import com.wanyangproject.net.util.Constant;
import com.wanyangproject.utils.ContractUtils;

import org.greenrobot.eventbus.EventBus;

public class BoxDialog extends BaseActivity implements View.OnClickListener {

    public static void start(Context context) {
        context.startActivity(new Intent(context, BoxDialog.class));
    }

    @Override
    public int getContentResId() {
        return R.layout.box_dialog;
    }


    @Override
    public void init() {

//        游客
        findViewById(R.id.role_tourist).setOnClickListener(this);
//        企业
        findViewById(R.id.role_company).setOnClickListener(this);
//        员工
        findViewById(R.id.role_employ).setOnClickListener(this);
//        门卫
        findViewById(R.id.role_door).setOnClickListener(this);
//        商家
        findViewById(R.id.btn_shangjia).setOnClickListener(this);
//        物业
        findViewById(R.id.btn_wuye).setOnClickListener(this);



    }

    @Override
    protected void onResume() {
        super.onResume();
        initView();
    }


//    暂时 哈哈jiangtiantian 放开
    private void initView() {

        Button youke = (Button) findViewById(R.id.role_tourist);
        Button twoshenfen = (Button) findViewById(R.id.role_company);
//        typeId: 0 游客 1 普通员工 2企业用户 3商家 4 门卫 5 物业
        if(ContractUtils.getTypeId(this).equals("0")){
            youke.setText("游客");
            System.out.println("游客游客游客游客游客");
        }else if(ContractUtils.getTypeId(this).equals("1")){
            youke.setText("普通员工");
            System.out.println("普通员工普通员工普通员工普通员工普通员工");
        }else if(ContractUtils.getTypeId(this).equals("2")){
            youke.setText("企业用户");
            System.out.println("企业用户企业用户企业用户企业用户企业用户");
        }else if(ContractUtils.getTypeId(this).equals("3")){
            youke.setText("商家");
            System.out.println("商家商家商家商家商家商家");
        }else if(ContractUtils.getTypeId(this).equals("4")){
            youke.setText("门卫");
            System.out.println("门卫门卫门卫门卫门卫门卫门卫门卫");
        }else if(ContractUtils.getTypeId(this).equals("5")){
            youke.setText("物业");
            System.out.println("物业物业物业物业物业物业物业物业");
        }
//
////        暂时 哈哈jiangtiantian 放开
        if(ContractUtils.getTypeId22(this).equals("0")){
            twoshenfen.setText("游客");
            System.out.println("2222游客游客游客游客");
        }else if(ContractUtils.getTypeId22(this).equals("1")){
            twoshenfen.setText("普通员工");
            System.out.println("22222普通员工普通员工普通员工");
        }else if(ContractUtils.getTypeId22(this).equals("2")){
            twoshenfen.setText("企业用户");
            System.out.println("222企业用户企业用户企业用户");
        }else if(ContractUtils.getTypeId22(this).equals("3")){
            twoshenfen.setText("商家");
            System.out.println("2222商家商家商家商家");
        }else if(ContractUtils.getTypeId22(this).equals("4")){
            twoshenfen.setText("门卫");
            System.out.println("2222门卫门卫门卫门卫");
        }else if(ContractUtils.getTypeId22(this).equals("5")){
            twoshenfen.setText("物业");
            System.out.println("2222物业物业物业物业");
        }
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {
//            游客
            case R.id.role_tourist:
//                暂时 哈哈jiangtiantian 放开
//                   typeId: 0 游客 1 普通员工 2企业用户 3商家 4 门卫 5 物业
                if(ContractUtils.getTypeId(this).equals("0")){
                    restart(Constant.ROLE_TOURIST);
                }else if(ContractUtils.getTypeId(this).equals("1")){
                    restart(Constant.ROLE_EMPLOY);
                }else if(ContractUtils.getTypeId(this).equals("2")){
                    restart(Constant.ROLE_COMPANY);
                }else if(ContractUtils.getTypeId(this).equals("3")){
                    restart(Constant.BTN_SHANGJIA);
                }else if(ContractUtils.getTypeId(this).equals("4")){
                    restart(Constant.ROLE_DOOR);
                }else if(ContractUtils.getTypeId(this).equals("5")){
                    restart(Constant.BTN_WUYE);
                }


//                restart(Constant.ROLE_TOURIST);
                break;
//            企业
            case R.id.role_company:
//                暂时 哈哈jiangtiantian 放开
                if(ContractUtils.getTypeId22(this).equals("0")){
                    restart(Constant.ROLE_TOURIST);
                }else if(ContractUtils.getTypeId22(this).equals("1")){
                    restart(Constant.ROLE_EMPLOY);
                }else if(ContractUtils.getTypeId22(this).equals("2")){
                    restart(Constant.ROLE_COMPANY);
                }else if(ContractUtils.getTypeId22(this).equals("3")){
                    restart(Constant.BTN_SHANGJIA);
                }else if(ContractUtils.getTypeId22(this).equals("4")){
                    restart(Constant.ROLE_DOOR);
                }else if(ContractUtils.getTypeId22(this).equals("5")){
                    restart(Constant.BTN_WUYE);
                }


//                restart(Constant.ROLE_COMPANY);
                break;





//////            员工
            case R.id.role_employ:
//                restart(Constant.ROLE_EMPLOY);
                break;
//            门卫
            case R.id.role_door:
//                restart(Constant.ROLE_DOOR);
                break;
//            商家
            case R.id.btn_shangjia:
//                restart(Constant.BTN_SHANGJIA);
                break;
//            物业
            case R.id.btn_wuye:
//                restart(Constant.BTN_WUYE);
                break;
        }
    }

    private void restart(int roleIndex) {
        EventModel eventModel = new EventModel();
        eventModel.setInteger(roleIndex);
        eventModel.setString(Constant.ROLE_INDEX);
        EventBus.getDefault().post(eventModel);
        mCache.put(Constant.ROLE_INDEX, String.valueOf(roleIndex));
        finish();
    }
}
