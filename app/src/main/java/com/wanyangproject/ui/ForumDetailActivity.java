package com.wanyangproject.ui;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.support.annotation.IdRes;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;


import com.wanyangproject.R;
import com.wanyangproject.adapter.CommentAdapter;
import com.wanyangproject.base.BaseActivity;
import com.wanyangproject.model.ForumModel;

import java.util.ArrayList;
import java.util.List;

public class ForumDetailActivity extends BaseActivity implements RadioGroup.OnCheckedChangeListener {

    private RadioGroup forumDetailRg;
    private RecyclerView forumDetailRecycler;
    private List<ForumModel> forumList;
    private CommentAdapter commentAdapter;
    private RadioButton forumDetailComment;
    private RadioButton forumDetailLike;

    public static void start(Context context) {
        Intent intent = new Intent(context, ForumDetailActivity.class);
        context.startActivity(intent);
    }

    @Override
    public int getContentResId() {
        return R.layout.activity_forum_detail;
    }

    @Override
    public void init() {
        if (Build.VERSION.SDK_INT >= 21) {
            View dview = getWindow().getDecorView();
            dview.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN | View.SYSTEM_UI_FLAG_LAYOUT_STABLE);
            getWindow().setStatusBarColor(Color.TRANSPARENT);
        }

        ImageButton barBack = (ImageButton) findViewById(R.id.bar_back);
        TextView barTitle = (TextView) findViewById(R.id.bar_title);
        ImageButton barSearch = (ImageButton) findViewById(R.id.bar_search);
        ImageView barDel = (ImageView) findViewById(R.id.bar_del);
        barBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        barTitle.setText("帖子详情");
        barSearch.setVisibility(View.VISIBLE);
        barSearch.setImageResource(R.mipmap.zhuanfa);
        barDel.setVisibility(View.VISIBLE);

        forumDetailRg = (RadioGroup) findViewById(R.id.forum_detail_rg);
        forumDetailComment = (RadioButton) findViewById(R.id.forum_detail_comment);
        forumDetailLike = (RadioButton) findViewById(R.id.forum_detail_like);
        forumDetailRecycler = (RecyclerView) findViewById(R.id.forum_detail_recycler);

        forumDetailRg.setOnCheckedChangeListener(this);

        initData();
    }

    public void initData() {
        forumList = new ArrayList<>();
        ForumModel model1 = new ForumModel();
        model1.setResId(R.drawable.touxiang4);
        model1.setNick("爱生活的喵耳朵");
        model1.setTime("4-18 21:34");
        model1.setContent("好漂亮~");
        List<ForumModel.Data> dataList = new ArrayList<>();
        ForumModel.Data data1 = new ForumModel.Data();
        data1.setNick("园区王先生");
        data1.setContent("谢谢");
        dataList.add(data1);
        ForumModel.Data data2 = new ForumModel.Data();
        data2.setNick("爱生活的喵耳朵");
        data2.setContent("好想看看~");
        dataList.add(data2);
        model1.setData(dataList);
        forumList.add(model1);
        ForumModel model2 = new ForumModel();
        model2.setResId(R.drawable.touxiang);
        model2.setNick("爱生活");
        model2.setTime("4-18 21:34");
        model2.setContent("好漂亮~");
        forumList.add(model2);

        forumDetailRecycler.setLayoutManager(new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false));
        commentAdapter = new CommentAdapter(mContext, null);
        forumDetailRecycler.setAdapter(commentAdapter);
        setData(0);
    }

    private void setData(int comment) {
        if (commentAdapter != null) {
            commentAdapter.setData(forumList);
            Drawable drawable = getResources().getDrawable(R.mipmap.xiahuaxian);
            drawable.setBounds(0, 0, drawable.getMinimumWidth(), drawable.getMinimumHeight());
            if (comment == 0) {
                forumDetailComment.setCompoundDrawables(null, null, null, drawable);
                forumDetailLike.setCompoundDrawables(null, null, null, null);
            } else {
                forumDetailComment.setCompoundDrawables(null, null, null, null);
                forumDetailLike.setCompoundDrawables(null, null, null, drawable);
            }
        }
    }

    @Override
    public void onCheckedChanged(RadioGroup radioGroup, @IdRes int resId) {
        switch (resId) {
            case R.id.forum_detail_comment:
                setData(0);
                break;
            case R.id.forum_detail_like:
                setData(1);
                break;
        }
    }
}