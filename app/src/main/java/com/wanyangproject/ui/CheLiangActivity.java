package com.wanyangproject.ui;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;


import com.wanyangproject.R;
import com.wanyangproject.adapter.CarAdapter;
import com.wanyangproject.base.BaseActivity;
import com.wanyangproject.model.CarModel;

import java.util.ArrayList;
import java.util.List;

public class CheLiangActivity extends BaseActivity {

    public static void start(Context context) {
        context.startActivity(new Intent(context, CheLiangActivity.class));
    }

    @Override
    public int getContentResId() {
        return R.layout.recycler_view;
    }

    @Override
    public void init() {

        if (Build.VERSION.SDK_INT >= 21) {
            View dview = getWindow().getDecorView();
            dview.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN | View.SYSTEM_UI_FLAG_LAYOUT_STABLE);
            getWindow().setStatusBarColor(Color.TRANSPARENT);
        }

        ImageButton barBack = (ImageButton) findViewById(R.id.bar_back);
        TextView barTitle = (TextView) findViewById(R.id.bar_title);
        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.recycler_view);

        barBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        barTitle.setText("车辆管理");

        List<CarModel> carList = new ArrayList<>();
        CarModel car1 = new CarModel();
        car1.setName("吴文文");
        car1.setTel("18958944932");
        car1.setNum("浙C91S00");
        carList.add(car1);
        CarModel car2 = new CarModel();
        car2.setName("吴文文");
        car2.setTel("18958944932");
        car2.setNum("浙CU1N55");
        carList.add(car2);
        CarModel car3 = new CarModel();
        car3.setName("吴文文");
        car3.setTel("18958944932");
        car3.setNum("浙C8895D");
        carList.add(car3);

        recyclerView.setLayoutManager(new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false));
        recyclerView.setAdapter(new CarAdapter(mContext, carList));
    }
}