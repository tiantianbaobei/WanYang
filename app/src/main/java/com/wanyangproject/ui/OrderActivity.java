package com.wanyangproject.ui;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewCompat;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.TextView;


import com.wanyangproject.R;
import com.wanyangproject.adapter.OrderAdapter;
import com.wanyangproject.adapter.TabsPagerAdapter;
import com.wanyangproject.base.BaseActivity;
import com.wanyangproject.net.util.Constant;
import com.wanyangproject.net.util.DensityUtil;
import com.wanyangproject.net.util.FragmentUtil;
import com.wanyangproject.net.util.ScreenUtil;
import com.wanyangproject.widget.tabs.TabsIndicator;

import java.util.List;

public class OrderActivity extends BaseActivity implements ViewPager.OnPageChangeListener {

    private RelativeLayout indicatorLine;
    private View indicator;
    private List<Fragment> orderFragment;
    private ViewPager orderPager;
    private OrderAdapter orderAdapter;

    public static void start(Context context, int order) {
        Intent intent = new Intent(context, OrderActivity.class);
        intent.putExtra(Constant.ORDER_INDEX, order);
        context.startActivity(intent);
    }

    @Override
    public int getContentResId() {
        return R.layout.activity_order;
    }

    @Override
    public void init() {
        if (Build.VERSION.SDK_INT >= 21) {
            View dview = getWindow().getDecorView();
            dview.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN | View.SYSTEM_UI_FLAG_LAYOUT_STABLE);
            getWindow().setStatusBarColor(Color.TRANSPARENT);
        }

        int orderIndex = getIntent().getIntExtra(Constant.ORDER_INDEX, Constant.ORDER_ALL);

        ImageButton barBack = (ImageButton) findViewById(R.id.bar_back);
        TextView barTitle = (TextView) findViewById(R.id.bar_title);
        TabsIndicator orderTabs = (TabsIndicator) findViewById(R.id.order_tabs);
        orderPager = (ViewPager) findViewById(R.id.order_pager);
        indicatorLine = (RelativeLayout) findViewById(R.id.indicator_line);
        indicator = findViewById(R.id.indicator);
        orderFragment = FragmentUtil.getOrderFragment();
        orderPager.addOnPageChangeListener(this);
        orderPager.setOffscreenPageLimit(5);
        orderPager.setAdapter(new TabsPagerAdapter(getSupportFragmentManager(), orderFragment));
        orderTabs.setViewPager(orderPager);
        orderTabs.setTabCurrenItem(orderIndex);
        barTitle.setText("我的订单");
        barBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        initIndicator();
    }

    private void initIndicator() {
        indicatorLine.getLayoutParams().width = ScreenUtil.getScreenWidth(mContext) / orderFragment.size();
        indicatorLine.requestLayout();
        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT
        );
        int interval = DensityUtil.dp2px(mContext, 30);
        params.setMargins(interval, 0, interval, 0);
        indicator.setLayoutParams(params);
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
        float offsetX = positionOffset * indicatorLine.getWidth();
        int startX = position * indicatorLine.getWidth();
        float translationX = startX + offsetX;
        ViewCompat.setTranslationX(indicatorLine, translationX);
    }

    @Override
    public void onPageSelected(int position) {

    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }
}
