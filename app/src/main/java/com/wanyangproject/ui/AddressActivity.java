package com.wanyangproject.ui;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;


import com.wanyangproject.R;
import com.wanyangproject.adapter.AddressAdapter;
import com.wanyangproject.base.BaseActivity;
import com.wanyangproject.model.AddressModel;
import com.wanyangproject.widget.recyclerview.DividerItemDecoration;

import java.util.ArrayList;
import java.util.List;

public class AddressActivity extends BaseActivity {

    public static void start(Context context) {
        context.startActivity(new Intent(context, AddressActivity.class));
    }

    @Override
    public int getContentResId() {
        return R.layout.activity_address;

    }

    @Override
    public void init() {

        if (Build.VERSION.SDK_INT >= 21) {
            View dview = getWindow().getDecorView();
            dview.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN | View.SYSTEM_UI_FLAG_LAYOUT_STABLE);
            getWindow().setStatusBarColor(Color.TRANSPARENT);
        }

        ImageButton barBack = (ImageButton) findViewById(R.id.bar_back);
        TextView barTitle = (TextView) findViewById(R.id.bar_title);
        ImageButton barSearch = (ImageButton) findViewById(R.id.bar_search);
        barBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        barTitle.setText("收货信息");
        barSearch.setVisibility(View.VISIBLE);
        barSearch.setImageResource(R.mipmap.tianjia2);

        RecyclerView addressRecycler = (RecyclerView) findViewById(R.id.address_recycler);

        List<AddressModel> results = new ArrayList<>();
        results.add(new AddressModel("李先生", "18510622262", "北京市东城区西直门外大街西环广场T3-9B4", true));
        results.add(new AddressModel("王先生", "18510622262", "北京市东城区西直门外大街西环广场T3-9B4", false));
        results.add(new AddressModel("张女士", "18510622262", "北京市东城区西直门外大街西环广场T3-9B4", false));
        addressRecycler.setLayoutManager(new LinearLayoutManager(mContext));
        addressRecycler.setAdapter(new AddressAdapter(mContext, results));
        addressRecycler.addItemDecoration(new DividerItemDecoration(mContext, DividerItemDecoration.VERTICAL_LIST));
    }
}