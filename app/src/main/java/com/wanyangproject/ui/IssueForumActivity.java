package com.wanyangproject.ui;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.view.View;

import com.wanyangproject.R;
import com.wanyangproject.base.BaseActivity;


public class IssueForumActivity extends BaseActivity {

    public static void start(Context context) {
        Intent intent = new Intent(context, IssueForumActivity.class);
        context.startActivity(intent);
    }

    @Override
    public int getContentResId() {
        return R.layout.activity_issue_forum;
    }

    @Override
    public void init() {

        if (Build.VERSION.SDK_INT >= 21) {
            View dview = getWindow().getDecorView();
            dview.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN | View.SYSTEM_UI_FLAG_LAYOUT_STABLE);
            getWindow().setStatusBarColor(Color.TRANSPARENT);
        }

        findViewById(R.id.issue_forum_back).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }
}
