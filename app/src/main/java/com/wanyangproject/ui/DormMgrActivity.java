package com.wanyangproject.ui;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;

import com.wanyangproject.R;
import com.wanyangproject.adapter.DormMgrAdapter;
import com.wanyangproject.base.BaseActivity;
import com.wanyangproject.model.HouseModel;

import java.util.ArrayList;
import java.util.List;

public class DormMgrActivity extends BaseActivity {

    public static void start(Context context) {
        context.startActivity(new Intent(context, DormMgrActivity.class));
    }

    @Override
    public int getContentResId() {
        return R.layout.recycler_view;
    }

    @Override
    public void init() {

        if (Build.VERSION.SDK_INT >= 21) {
            View dview = getWindow().getDecorView();
            dview.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN | View.SYSTEM_UI_FLAG_LAYOUT_STABLE);
            getWindow().setStatusBarColor(Color.TRANSPARENT);
        }

        ImageButton barBack = (ImageButton) findViewById(R.id.bar_back);
        TextView barTitle = (TextView) findViewById(R.id.bar_title);
        ImageButton barSearch = (ImageButton) findViewById(R.id.bar_search);
        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.recycler_view);

        barBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        barTitle.setText("宿舍管理");
        barSearch.setImageResource(R.mipmap.sousuo);
        barSearch.setVisibility(View.VISIBLE);

        List<HouseModel> houseList = new ArrayList<>();
        HouseModel house1 = new HouseModel();
        house1.setName("734寝室");
        house1.setFloor("F07#宿舍楼");
        house1.setDoor("734");
        house1.setBlock("F-07");
        houseList.add(house1);
        HouseModel house2 = new HouseModel();
        house2.setName("735寝室");
        house2.setFloor("F07#宿舍楼");
        house2.setDoor("735");
        house2.setBlock("F-07");
        houseList.add(house2);
        HouseModel house3 = new HouseModel();
        house3.setName("737寝室");
        house3.setFloor("F07#宿舍楼");
        house3.setDoor("737");
        house3.setBlock("F-07");
        houseList.add(house3);
        HouseModel house4 = new HouseModel();
        house4.setName("738寝室");
        house4.setFloor("F07#宿舍楼");
        house4.setDoor("738");
        house4.setBlock("F-07");
        houseList.add(house4);
        HouseModel house5 = new HouseModel();
        house5.setName("739寝室");
        house5.setFloor("F07#宿舍楼");
        house5.setDoor("739");
        house5.setBlock("F-07");
        houseList.add(house5);

        recyclerView.setLayoutManager(new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false));
        recyclerView.setAdapter(new DormMgrAdapter(mContext, houseList));
    }
}