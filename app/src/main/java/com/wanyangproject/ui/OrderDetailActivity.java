package com.wanyangproject.ui;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.wanyangproject.R;
import com.wanyangproject.base.BaseActivity;
import com.wanyangproject.net.util.SpannableUtil;


public class OrderDetailActivity extends BaseActivity {

    private ImageButton barBack;
    private TextView barTitle;

    public static void start(Context context, String state) {
        Intent intent = new Intent(context, OrderDetailActivity.class);
        intent.putExtra("state", state);
        context.startActivity(intent);
    }

    @Override
    public int getContentResId() {
        return R.layout.activity_order_detail;
    }

    @Override
    public void init() {

        if (Build.VERSION.SDK_INT >= 21) {
            View dview = getWindow().getDecorView();
            dview.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN | View.SYSTEM_UI_FLAG_LAYOUT_STABLE);
            getWindow().setStatusBarColor(Color.TRANSPARENT);
        }

        String state = getIntent().getStringExtra("state");

        barBack = (ImageButton) findViewById(R.id.bar_back);
        barTitle = (TextView) findViewById(R.id.bar_title);
        TextView orderDetailAddress = (TextView) findViewById(R.id.order_detail_address);
        Button orderDetailBtn = (Button) findViewById(R.id.order_detail_btn);
        LinearLayout orderDetailBottom = (LinearLayout) findViewById(R.id.order_detail_bottom);

        barBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        barTitle.setText("订单详情");

        String defaultAddress = "[默认] 收货地址: 北京北京市海淀区上地街道中关村软件园，国际软件大厦，9号楼";
        SpannableUtil.colorSpan(orderDetailAddress, defaultAddress);

        if (state == null) {
            orderDetailBottom.setVisibility(View.GONE);
        } else {
            orderDetailBottom.setVisibility(View.VISIBLE);
            orderDetailBtn.setText(state);
        }
    }
}