package com.wanyangproject.model;

public class AddressModel {

    private String nick;
    private String phone;
    private String address;
    private boolean def;

    public AddressModel(String nick, String phone, String address, boolean def) {
        this.nick = nick;
        this.phone = phone;
        this.address = address;
        this.def = def;
    }

    public String getNick() {
        return nick;
    }

    public void setNick(String nick) {
        this.nick = nick;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public boolean isDef() {
        return def;
    }

    public void setDef(boolean def) {
        this.def = def;
    }
}
