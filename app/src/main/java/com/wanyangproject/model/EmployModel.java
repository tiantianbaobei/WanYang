package com.wanyangproject.model;


import com.wanyangproject.widget.suspensionindexbar.BaseIndexPinyinBean;

public class EmployModel extends BaseIndexPinyinBean {

    private String name;
    private String six;

    public EmployModel(String name, String six) {
        this.name = name;
        this.six = six;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSix() {
        return six;
    }

    public void setSix(String six) {
        this.six = six;
    }

    @Override
    public String getTarget() {
        return name;
    }
}
