package com.wanyangproject.widget.scrollview;

public interface OnScrollChangedListener {
    void onScrollChanged(MonitorScrollView scroll, int x, int y, int oldx, int oldy);
}
