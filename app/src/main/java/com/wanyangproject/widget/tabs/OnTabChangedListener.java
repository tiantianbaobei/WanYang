package com.wanyangproject.widget.tabs;

public interface OnTabChangedListener {
    void onTabChanged(int tabNum);
}
