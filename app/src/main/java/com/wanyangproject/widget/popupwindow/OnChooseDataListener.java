package com.wanyangproject.widget.popupwindow;

public interface OnChooseDataListener {
    void onChooseData(String data);
}
