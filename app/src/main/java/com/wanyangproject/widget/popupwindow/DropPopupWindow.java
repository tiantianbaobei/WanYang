package com.wanyangproject.widget.popupwindow;

import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.PopupWindow;

import com.wanyangproject.net.util.DensityUtil;


public abstract class DropPopupWindow extends PopupWindow {

    protected Context mContext;
    protected View rootView;
    protected View mAnchor;

    public DropPopupWindow(Context context, View anchor) {
        mContext = context;
        mAnchor = anchor;
        initPopupWindow();
    }

    private void initPopupWindow() {
        rootView = initView();
        this.setContentView(rootView);
        this.setOutsideTouchable(true);
        this.setWidth(DensityUtil.dp2px(mContext, viewDpWidth()));
        this.setHeight(DensityUtil.dp2px(mContext, viewDpHeight()));
        this.setFocusable(true);
        this.setBackgroundDrawable(new ColorDrawable(0x00000000));
        int offX = DensityUtil.dp2px(mContext, offsetDpX());
        int offY = DensityUtil.dp2px(mContext, offsetDpY());
        this.showAsDropDown(mAnchor, -offX, -offY);

        /**事务处理*/
        init();
    }

    private View initView() {
        LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(layoutResId(), null);
        return view;
    }

    public <T> T findViewById(int id) {
        T view = (T) rootView.findViewById(id);
        return view;
    }

    public void myDismiss() {
        dismiss();
    }

    public abstract int layoutResId();
    public abstract int viewDpWidth();
    public abstract int viewDpHeight();
    public abstract int offsetDpX();
    public abstract int offsetDpY();
    public abstract void init();
}