package com.wanyangproject.widget.recyclerview;

import android.view.View;

public class RecyclerListener implements View.OnClickListener, View.OnLongClickListener {

    private int mPosition;
    private Object mHolder;
    private OnClickListener mOnClickListener;
    private OnLongClickListener mOnLongClickListener;
    public static final int MIN_CLICK_DELAY_TIME = 2000;
    private long lastClickTime = 0;

    public RecyclerListener(int position, Object holder) {
        this.mPosition = position;
        this.mHolder = holder;
    }

    @Override
    public void onClick(View v) {
        if (mOnClickListener != null) {
//            long currentTime = Calendar.getInstance().getTimeInMillis();
//            if (currentTime - lastClickTime > MIN_CLICK_DELAY_TIME) {
//                lastClickTime = currentTime;
                mOnClickListener.onClick(v, mPosition, mHolder);
//            } else {
//                Toast.show(MyApplication.getContext(), R.string.do_not_often);
//            }
        }
    }

    public interface OnClickListener<T> {
        void onClick(View v, int position, T holder);
    }

    public void setOnClickListener(OnClickListener mOnClickListener) {
        this.mOnClickListener = mOnClickListener;
    }

    @Override
    public boolean onLongClick(View v) {
        if (mOnLongClickListener != null) {
            mOnLongClickListener.onLongClick(v, mPosition, mHolder);
        }
        return true;
    }

    public interface OnLongClickListener<T> {
        void onLongClick(View v, int position, T holder);
    }

    public void setOnLongClickListener(OnLongClickListener mOnLongClickListener) {
        this.mOnLongClickListener = mOnLongClickListener;
    }
}
