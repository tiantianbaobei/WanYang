package com.wanyangproject.widget.recyclerview.refresh;

import android.view.View;
import android.view.ViewGroup;

public abstract class EmptyItem {

    CharSequence mEmptyText;
    int mEmptyIconRes = -1;

    abstract View onCreateView(ViewGroup parent);

    abstract void onBindData(View view);
}
