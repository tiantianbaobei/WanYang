package com.wanyangproject.widget.recyclerview;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;


import com.wanyangproject.net.HttpUtil;
import com.wanyangproject.net.util.AppSession;
import com.wanyangproject.net.util.ToastUtil;

import java.util.ArrayList;
import java.util.List;

public abstract class RecyclerAdapter<T> extends RecyclerView.Adapter {

    protected Context mContext;
    protected List<T> mData;
    private int layoutId;
    private View mView;
    private int maxSize = 0;
    protected String mType;
    protected AppSession mSession;
    protected HttpUtil mHttpUtil;


    public void addData(List<T> data) {
        if (data != null) {
            mData.addAll(data);
        }
        notifyDataSetChanged();
    }

    public void setData(List<T> data) {
        mData.clear();
        addData(data);
    }

    public void clearData() {
        if (mData != null) {
            mData.clear();
        }
    }

    public List<T> getData() {
        return mData;
    }

    public T getItem(int position) {
        if (position > -1 && null != mData && mData.size() > position) {
            return mData.get(position);
        }
        return null;
    }

    public void addItem(int position, T data) {
        mData.add(position, data);
        notifyItemInserted(position);
        notifyItemRangeChanged(position, mData.size() - position);
        ToastUtil.show(mContext, "添加成功!");
    }

    public void removeItem(int position) {
        if (null != mData && mData.size() > position && position > -1) {
            mData.remove(position);
            notifyItemRemoved(position);
            notifyItemRangeChanged(position, mData.size());
            ToastUtil.show(mContext, "删除成功!");
        }
    }

    public RecyclerAdapter(Context context, List<T> data, int layoutId) {
        this.mContext = context;
        this.mData = data == null ? (List<T>) new ArrayList<>() : data;
        this.layoutId = layoutId;
        mSession = AppSession.getInstance(mContext);
//        mHttpUtil = HttpUtil.getInstance();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return RecyclerHolder.getHolder(mContext, parent, layoutId);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int position) {
        if (viewHolder instanceof RecyclerHolder) {
            RecyclerHolder holder = (RecyclerHolder) viewHolder;
            holder.position = position;
            T model = mData.get(position);
            convert(holder, model);
        }
    }

    public abstract void convert(RecyclerHolder recyclerHolder, T t);

    @Override
    public int getItemCount() {
        if (mData != null && mData.size() > 0) {
            if (maxSize == 0) {
                return mData.size();
            } else {
                return mData.size() > maxSize ? maxSize : mData.size();
            }
        } else {
            return 0;
        }
    }

    public void setMaxSize(int size) {
        maxSize = size;
    }

    public void setConsumptionType(String type){
        mType=type;
    }
}
