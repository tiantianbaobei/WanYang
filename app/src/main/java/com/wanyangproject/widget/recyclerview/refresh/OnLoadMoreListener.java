package com.wanyangproject.widget.recyclerview.refresh;

public interface OnLoadMoreListener {
    void onLoadMore();
}
